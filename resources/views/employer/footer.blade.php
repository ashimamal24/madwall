@php
$code = '';
@endphp
@if(Auth::user())
    @php$code = Auth::user()->email_verification_code;@endphp
@endif


<!-- footer section  -->
<footer class="footer-main">
	<div class="container">
    	<div class="row">
        	<div class="col-md-3 col-sm-4">
            		<div class="footer-logo">
               	    <a href="{{url('')}}"><img src="{{ url('public/employer/images/logo.png') }}" alt=""/></a>
                    </div>
            </div>
            <div class="col-md-9 col-sm-8">
            	<div class="footer-support-social">
                	<ul>
                    	<!--<li><a target="_blank" href="https://www.facebook.com/MadWall-Employment-150017992231257/"><i class="fa fa-facebook"></i></a></li>-->
                    	<li><a target="_blank" href="https://www.facebook.com/MadWallEmployment/"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/madwall.employment/"><i class="fa fa-instagram"></i></a></li>
                        <li><a target="_blank" href="https://www.linkedin.com/company-beta/11115309/"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="footer-nav">
                <ul>
                	<!-- <li><a href="#">Contact us</a></li> -->
                  <!-- <li><a href="javascript:void(0)" data-toggle="modal" data-target="#contact">Contact us</a></li> -->
                  <li><a href="{{url('/employer/contact')}}">Contact MadWall</a></li>
                    <li><a href="{{ url('terms-condtion') }}" target="_blank">Terms And Conditions</a></li>
                    <li><a href="{{ url('privacy-policy') }}" target="_blank">Privacy Policy</a></li>
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#doc">Documentation</a></li>
                </ul>
                </div>
                <div class="footer-copyright">
                Copyright reserved by <span>MadWall Inc</span>
                </div>
            </div>
        </div>
    </div>


<!--forgot popup-->



<div class="modal fade signup forgot" id="waitlistuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
        <p><b>You have been not approved by admin yet.</b></p>
       
      </div>
      <div class="modal-footer">
        <button class="submit-button" type="button" data-dismiss="modal">Ok</button>
        <button type="button" class="cancel-button" data-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade signup forgot" id="blockuser_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
        <p><b>You have been blocked by administrator. Please contact them through contact us form</b></p>       
      </div>
      <div class="modal-footer">
        <button class="submit-button" type="button" data-dismiss="modal">Ok</button>
        <button type="button" class="cancel-button" data-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade signup forgot" id="verifyAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
		<p><b>Please verify your Email first in order to Post A Job</b></p>
       
      </div>
      <div class="modal-footer">
        <button class="submit-button" type="button" id="verifyaccount">Verify Now</button>
       
		<button type="button" class="cancel-button" data-dismiss="modal">Verify Later</button><!-- ---- updated : 27july2017, for text changes ----- -->
      </div>
    </div>
  </div>
</div>
	<div class="modal fade signup forgot" id="doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <h4 class="modal-title">Documents</h4>
                </div>
                <div class="modal-body" style="overflow-y: scroll; height: 435px;">
                    <div class="documents">
                        <ul>
                            @foreach($documetation as $document)
                            <li><div class="doc_img"><i class="{{$document->extension}}" aria-hidden="true"></i>
                                </div> <h6>{{$document->doc_name}}</h6>
                                <a href="{{Url('public/documentation/').'/'.$document->doc_name}}" download><div class="button_download" ><button>Download</button></div></a>
                            </li>
                            @endforeach
                            
                            
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
					<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    
    
   @include('models.contactus') 
    @include('employer.promo.tutorial_screen')
</footer>
<!-- footer section end -->
<script src="{{ asset('public/employer/js/jquery-1.11.3.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/page.jumpToData().js" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.main.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/wow.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.malihu.PageScroll2id.js') }}" type="text/javascript"></script>

<!-- <script src="{{ asset('public/employer/js/path.js') }}" type="text/javascript"></script> -->
<script type="text/javascript">
var path = '{{ url("/") }}/';
var adminname = "employer";
</script>
<script src="{{asset('public/employer/js/bootbox.min.js')}}" type="text/javascript"></script>  
<script src="{{asset('public/employer/js/loader.js')}}" type="text/javascript"></script> 
<script src="{{asset('public/employer/js/jquery.validate.min.js')}}" type="text/javascript"></script>    






<script type="text/javascript">
   wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
</script>
<script type="text/javascript">
    setTimeout(function() {
        $('.flashMessage').fadeOut('slow');
    }, 10000);
    setTimeout(function() {
        $('.alert-danger').fadeOut('slow');
    }, 10000);
</script>
<script type="text/javascript">
</script>    
@yield('js')
<script src="{{ asset('public/admin/js/moment-timezone.js')}}"></script>
<script type="text/javascript">
$(document).on('click','.waitlistuser',function(){
    $('#waitlistuser').modal('show');
});
$(document).on('click','.verifyemail',function(){

    $('#verifyAccount').modal('show');
    
});


$(document).ready(function() {
     
		$("#owl-tut").owlCarousel({
     
			navigation : true, // Show next and prev buttons
			slideSpeed : 300,

			items : 1, 
			paginationSpeed : 400,
			singleItem:true,
			autoHeight: false,
			lazyLoad : false,
     
		});
		
		
		
		if(getCookie('tutorial_view') != 'yes'){
			$('#tut_modal').modal('show');
			setCookie("tutorial_view",'yes',365);
		}
		
		
		
		
     
    });

/*
DESC : to check if user profile is approved or not
*/
$(document).on('click','.post_a_job',function(){
	$.ajax({
		url: path+'employer/check-employer',
		type: 'post',
		dataType: 'json',
		data: '_token='+$('meta[name="csrf-token"]').attr('content'),
		beforeSend:function(){
			Loader();
		},
		success: function(data) {
			if(data.success == true){
				if(data.block_user == true){
					if(data.block_type == 'blockuser'){
						$('#blockuser_modal').modal('show');
					}
					if(data.block_type == 'verifyemail'){
						$('#verifyAccount').modal('show');
					}
					if(data.block_type == 'waitlistuser'){
						$('#waitlistuser').modal('show');
					}
				}else{
					window.location.href=data.url;	//redirect user to post a new job page
				}
			}
		},
		error: function(error) { 
			RemoveLoader();
			window.location.reload(true);
		},
		complete: function() { 
			RemoveLoader();			
		}
    });
});






$(document).on('click','#verifyaccount',function(){
    Loader();
    var code = "{{$code}}"
    //window.location=path+'employer/logout/'+;
    $.ajax({
    url: path+'employer/verfiylink',
    type: 'post',
    dataType: 'json',
    data: '_token='+$('meta[name="csrf-token"]').attr('content'),
    beforeSend:function(){
    //Loader();
    },
    success: function(data) {
    //console.log('sudccess');
    window.location.href=data.url;
    },
    error: function(error) { 
    },
    complete: function() { //RemoveLoader() 
    }
    });
})

var timezone = moment.tz.guess();
//alert(timezone);
document.cookie = "timezone="+timezone; 
$('#timezone').val(moment.tz.guess()); 

$(document).on('click','#logout',function(){
   
	$('.bootbox-close-button').hide();
    bootbox.confirm({
		closeButton: false,
                        message: 'Are you sure you want to logout?',
                        buttons: {
                            'cancel': {
                                label: 'No',
                            },
                            'confirm': {
                                label: 'Yes',
                            }
                        },
                        callback: function(result) {
                            if (result) {
                                Loader();
                                 window.location=path+'employer/logout';
                            }
                        }
                    });
});

function notification()
{

  $.ajax({
    url: path+'employer/all-notification',
    type: 'post',
    dataType: 'json',
    data: '_token='+$('meta[name="csrf-token"]').attr('content'),
    beforeSend : function(){
		//RemoveLoader();
	},
    success: function(data) {
        $('.bell_div').html(data.content);
    },
    error: function(error) { 
    
    },
    complete: function() { 
    
    }
    });
}


notification();

setInterval(function(){
	notification();
},10000);


$(document).on('click','.visit_nofity',function(){
	var elem = $(this);
  $.ajax({
    url: path+'employer/update-notification',
    type: 'post',
    dataType: 'json',
    data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&notify='+$('.notify-'+$(this).attr('data-value')).val(),
    success: function(data) {
		//redirect to their specific location
        window.location = path+data.redirect_uri;
    },
    error: function(error) { 
    
    }
  });
});
</script>
<script  src="{{ asset('public/employer/js/timezone.js') }}" type="text/javascript"></script>
<script  src="{{ asset('public/employer/js/cookies.js') }}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/contactus.js')}}" type="text/javascript"></script> 
<script type='text/javascript'>


var tz = jstz.determine(); // Determines the time zone of the browser client
var test =  tz.name(); 

	if(test != '' && test != undefined){
		var delete_cookie = function(name) {
			document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		};

		delete_cookie('client_timezone');
		setCookie("client_timezone",test,365);
	}
		



  $(document).on('click','.approvedUser',function(){
    $.ajax({
    url: path+'employer/check-user',
    type: 'post',
    dataType: 'json',
    data: '_token='+$('meta[name="csrf-token"]').attr('content'),
    beforeSend:function(){
  //  Loader();
    },
    success: function(data) {
        window.location.reload();
    },
    error: function(error) { 
    },
    complete: function() { //RemoveLoader() 
    }
    });
  });
  
  
  
  
  /*
	Added on : 29 july 2017
	Added by : shivani, Debut infotech
	DESC : to auto refresh page after 5 minutes of inactivity
	*/
    var time = new Date().getTime();
	$(document.body).bind("mousemove keypress", function(e) {
		time = new Date().getTime();
	});

	function refresh() {
		if(new Date().getTime() - time >= 600000) 
			window.location.reload(true);
		else 
			setTimeout(refresh, 10000);
	}
	setTimeout(refresh, 10000);
  
  
  
  
  
</script>


<?php
if(Session::has('timezone')){
    Session::put('timezone',$_COOKIE['timezone']);
}

?>

</body>

</html>
