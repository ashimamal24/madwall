<?php 
$start_date = date('Y-m-d H:i:s',strtotime($job_post->start_date));
$end_date = date('Y-m-d H:i:s',strtotime($job_post->end_date));
$jobId = Crypt::encrypt($job_post->_id);
$shiftsData = [];
foreach($job_post->jobshifts AS $k => $row){
	$shiftsData[] = ['start_date'=>date('H:i',strtotime($row['start_date'])),'end_date'=>date('H:i',strtotime($row['end_date']))];
}

$jobUserData = $job_post->toArray();

?>


@extends('employer.layout')
@section('css')
	<link rel="stylesheet" href="{{ asset('public/employer/css/datepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{asset('public/employer/js/date-timepicker/jquery.timepicker.css')}}" />
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css"/>
	<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
	<style>
		.remove_margin{
			margin-top : 0px !important;
		}
		.filter_reset_divClear{
			filter_reset_divClear
		}
		.dataTables_wrapper .dataTables_paginate {
		    float: left !important;
		}	

button.close {
    font-size: 18px !important;
    padding: 0 5px 3px !important;
}
	</style>
@endsection
@section('title')
	{{$title}}
@endsection

@section('content')
	<div class="white_inner_sec wow fadeInUp">
		@include('flash::message')
		<div class="job_detail_sec">
			<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
				<div class="job_detail_left_sec">
					<h2>{{ ucwords($job_post->title) }}</h2>
					<span class="job_time">
						{{ date('jS F ',strtotime($start_date)) }} • {{ date('jS F Y',strtotime($end_date)) }}
					</span>
					<p style="word-wrap:break-word;">
				
						{!!html_entity_decode($job_post->description)!!}
					</p>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
				<div class="job_time_loc">
					<div class="hours_working">
						${{ number_format($job_post->salary_per_hour,2) }}/hr
					</div>
					<div class="location_sec width_100">
						<span>
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</span>
						<div class="text_job">
							<b>{{ $job_post->address }}</b>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="location_sec">

						<div class="text_job">
							<b>
								@include('employer.promo.shift_data',['processData'=>$job_post->shifts])
							</b>
						</div>
					</div>
					<div class="location_sec">
						<div class="text_job">
							Workers Required :  <b>{{ $job_post->number_of_worker_required }}</b>
						</div>
					</div>
					<div class="clearfix"></div>
					
					@if(!empty($job_post->lunch_hour))
						<!-- --- Lunch Hours --- -->
						<div class="location_sec">
							<div class="text_job">
								Lunch Hours :  <b>{{ floatval(number_format(($job_post->lunch_hour/60),2)) }} hours</b>
							</div>
						</div>
						<div class="clearfix"></div>
					@endif
					
					
					<div class="location_sec">
						<div class="text_job">
							Category : <b>{{ ucwords($job_post->category[0]['name']) }}</b>
						</div>
					</div>
					<div class="location_sec">
						<div class="text_job">
							Sub-Category : <b>{{ ucwords($job_post->subcategory[0]['name']) }}</b>
						</div>
					</div>
					<!-- --- physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->physical_requirement))
					<div class="location_sec width_100">
						<div class="text_job">Physical Requirements : 
						
						<b>{!!html_entity_decode($job_post->physical_requirement)!!}</b>
						
						
						
						</div>
					</div><div class="clearfix"></div>
					@endif
					<!-- physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->safety_hazards))
					<div class="location_sec width_100">
						<div class="text_job">Safety Hazards : 						
							<b>{!!html_entity_decode($job_post->safety_hazards)!!}</b>							
						</div>						
					</div>
					<div class="clearfix"></div>
					@endif
					<!-- --- physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->contact_name))
					<div class="location_sec width_100">
						<div class="text_job">
							Contact Name : <b>{{$job_post->contact_name}}</b>
						</div>						
					</div>
					<div class="clearfix"></div>
					@endif
					<!-- --- physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->meeting_location))
					<div class="location_sec width_100">
						<div class="text_job">Meeting Location : 
							<b>{{$job_post->meeting_location}}</b>
						</div>    
					</div>
					<div class="clearfix"></div>
					@endif

					<div class="location_sec width_100">
						<div class="text_job">Skills : </div>
						<div class="tag_align_right">		
							@foreach($job_post->skill AS $key => $value)			
								<div class="">&nbsp;<b>{{ $value['name'] }}</b></div>
							@endforeach
						</div>
					</div>
					<div class="clearfix"></div>

				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="worked_hr_and_amnt">
					@php
						$dates = current($shiftsData);
						$start= Carbon\Carbon::parse($dates['start_date']);
						$end= Carbon\Carbon::parse($dates['end_date']);
						$totalDuration = $end->diff($start)->format('%H.%I');
						
						$totalHours = ($job_post->total_work_hours);
					@endphp
					<span>
						Total Hours Worked: <b> {{$totalHours}} @if($totalHours > 1) hrs @else hr @endif</b>
					</span>
					<span>Amount:
						<b>
						@php
							
							$price = ($job_post->salary_per_hour * $totalHours); 
							$totalCommision = $job_post->category_commision;
							
							$commision = (($price * $totalCommision)/100);
						@endphp
						${{number_format($price + $commision,2)}}
						</b>
					</span>
				</div>
			</div>
		</div>
		<!-- -------------- Candidate Listing ------------ -->
		<div class="hired_workers_profile">
				
				<h6>Hired Workers</h6>
				</div>
		<div class="job_detail_table">
			
			<!-- ----- SEARCH JOBSEEKER FORM ------ -->
			<div class="row">
				{!! Form::open(['url'=>'employer/historydetail/jobapplylisting','onsubmit'=>'return false','id'=>'filter_form']) !!}
					{!! Form::hidden('jobvalue',$jobId) !!}
					<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
						<div class="number_list">
							<!-- ---- success/error message here ---- -->
							<div class="msg"></div>
						</div>
					</div>
				
                <div class="select-job-view">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="jobdetailsearch">
								<!-- ---- search field --- -->
								<div class="search_input remove_margin select-job-de">
									{!! Form::text('job_name',null,['placeholder'=>'Search by Candidate name','id'=>'job_name']) !!}<span><i class="fa fa-search search" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
						<!-- ---- select no. of record to be shown --- -->
						<div class="col-md-2 col-sm-4 col-xs-12 select-job-de1">
							{!! Form::select('records',[10=>10,15=>15,20=>20,30=>30,40=>40],null,['class'=>'customselect selectrecords','data-jcf'=>'{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}']) !!}
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="filter_reset_divClear">								
								<button class="reset" id="reset_search" type="button">Clear</button><!-- for text changes -->
								<button class="export_candidate_list" id="export_candidate_list" type="button">Export</button><!-- for text changes -->
							</div>
						</div>
                        </div>
				
					{!! Form::close() !!}
				</div>
				
				<!-- ---- jobseekers list will display ---- -->
				<div class="table_job_detail jobdata"></div>
			</div>

		</div>
		

		<div class="buttons">
			@if(count($job_post->jobapplied))
				<button class="rate_worker_btn" data-toggle="modal" data-target="#Rehiring">Rate Workers</button>
			@endif
			<a href="{{ url('employer/post_job/'.$jobId) }}">Repost This Job</a>
		</div>
	</div>
	{!! Form::close() !!}
</div>

	<div class="modal fade signup forgot rate_worker" id="Rehiring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">        
					<h4 class="modal-title" id="myModalLabel">Rate workers</h4>
				</div>
				@if($rateUserCount == 0)
					<div class="modal-body">
						<p>You have rated all of the workers hired for this job</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="submit-button" data-dismiss="modal" aria-label="Close">Ok</button>
					</div>
				@else
				<div class="modal-body" style="overflow-y: scroll; height: 435px;">
					<div class="rehiring_div">
						<ul>
							{!! Form::open(['id'=>'ratinguser','url'=>'employer/job-rating']) !!}
								<input type="hidden" name="value" value="{{$jobId}}">
							@if(count($applicants))
								@foreach($applicants AS $user_rate)	
									@if(!empty($user_rate['applyjobuser']))					
									<?php
										$rateUser = $user_rate['applyjobuser'];		
										$userId = Crypt::encrypt($rateUser['_id']); 						
									?>	 
									<li>  								
										<input type="hidden" name="u_ids[]" value="{{ $userId }}">					   
										<div class="col-sm-6 col-xs-12">									 
											@if(isset($rateUser['image']) && !empty($rateUser['image']))
												<div class="rehir_pic"><img src="{{$rateUser['image']}}" /></div>
											@else
											<div class="rehir_pic"><img src="{{url('public/employer/images/profile_avatar.png')}}"/></div>
											@endif
											<p>{{ $rateUser['first_name'] }} {{ $rateUser['last_name'] }}</p>
										</div>
										<div class="col-sm-6 col-xs-12">
											<div class="name_rating">
												<div class="rating_rehire pull-right">
													<div class="stars stars-example-fontawesome starratecont intervewirateFeild">
														<select id="example-fontawesome" name="rating[]" class="ratingstar ratingstar-{{$k}}" data-value="{{$k}}">
															<option value="">0</option>	
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
														</select> 
													</div>
												</div>
											</div>
										</div>
									</li>
									@endif									  
								@endforeach
							@else    
								<div class="col-sm-12 col-xs-12">     
									<div>
										<p>No workers hired for this job</p>
										<br><br><br>
									</div>
								</div>   
							@endif
						</ul>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="submit-button" id="addRating">Submit</button>
					<button type="button" class="cancel-button" data-dismiss="modal" aria-label="Close">Cancel</button>
				</div>
			{!! Form::close() !!} 
			</div>
		@endif
		</div>
	</div>
</div>
	
	@include('models.error_popup')
	@include('models.jobseeker_docs',$users)
	@include('employer.promo.work_history_popup')
	
@endsection

@section('js')
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<script src="{{ asset('public/employer/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- --------- timepicker js ------------ -->
<script type="text/javascript" src="{{asset('public/employer/js/date-timepicker/jquery.timepicker.js')}}"></script>
<script src="{{ asset('public/employer/js/jquery.barrating.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/jquery.raty.js') }}" type="text/javascript"></script>
<!-- --------- common js ------------ --> 
<script src="{{ asset('public/employer/js/common.js') }}" type="text/javascript"></script>
<!-- --------- employer js ------------ --> 
<script src="{{ asset('public/employer/js/employer_jobs.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(e) {
		
		$('#datepicker').datepicker({
			maxViewMode: 0,	   
		});
		
		$('.ratingstar').barrating({
			theme: 'fontawesome-stars',
			showSelectedRating: false
		});
		
		//to display jobseeker list
		var url = $('#filter_form').attr('action');
		listjobApply(url);
		
	});
	
$("#DisableJobAppAction").on("hidden.bs.modal", function () {
		$('#disable_message_here').empty();
	});

//delete shift
$(document).on('click','.delete-shift',function(){
	var url = $(this).attr('action'); 
	bootbox.confirm("Are you want to remove selected shift?", function(result) {
		if(result){
			Loader();
		    $.ajax({
		            url: url,
		            type: 'post',
		            dataType: 'json',
		            data:  {"_token": "{{ csrf_token() }}"},
		            beforeSend:function(){
		                Loader();
		            },
		            success: function(data) {
		            	RemoveLoader();
						window.location.reload();
		            },
		            error: function(error) {
		              RemoveLoader();
		              $('span.error_msg').hide();
		              $('span.error_msgg').hide();
		              /*var result_errors = error.responseJSON;
		              if(result_errors)
		              {
		                 $.each(result_errors, function(i,obj)
		                 {
		                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);

		                    if(i == 'token_mismatch')
		                        window.location.reload();
		                 }) 
		              }*/

		            },
		            complete: function() { //alert('b nn'); 
		            }
		          });		
		}
        
    });
	
});
	
$(document).on('click','#addRating',function(){
	
	//check if any user is selected or not
	var selectedUser = $('.br-widget').find('.br-selected').length;
	if(selectedUser == 0){
		$('#disable_message_here').html("Please select atleast one User for rating!");
		$('#DisableJobAppAction').modal('show');
		return false;
	}
	
	
    Loader();
    $.ajax({
            url: $('form#ratinguser').attr('action'),
            type: 'post',
            dataType: 'json',
            data: $('form#ratinguser').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
            	RemoveLoader();
				window.location.reload();
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              /*var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);

                    if(i == 'token_mismatch')
                        window.location.reload();
                 }) 
              }*/

            },
            complete: function() { //alert('b nn'); 
            }
          });
    

});

//to display jobseeker list and to filter jobseeker data
function listjobApply(url)
{
	var record =  '';
	if($('.selectrecords').val()!='' && $('.selectrecords').val() != undefined)
	{
		record = $('.selectrecords').val();
	}
	var filter_data = $('#filter_form').serialize();
	//Loader();
	$.ajax({
		type : 'post',
		url : url,
		data : filter_data,
		dataType : 'html',
		beforesend:function(){
			//Loader();
		},
		success : function(data){
			//RemoveLoader();
			$('.jobdata').empty().html(data);
			$('.msg').html($('#message').val());
		},
		error : function(data,ajaxOptions, thrownError){
			//RemoveLoader();

			var errors = jQuery.parseJSON(data.responseText);
			if(errors.success==false){
				new PNotify({
					type: 'error',
					title: Error,
					text: 'Something went wrong!!!'
				});
			}
			
		}
	});
}

		
</script>
@endsection
