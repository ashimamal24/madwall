@php $total_records=$total_page=$display_record=0;@endphp
@if(count($jobData))

			@php $total_records=$jobCount;@endphp
			@php $j=($jobData->currentPage() - 1) * $jobData->perPage() + 1;@endphp
			@php $k=($jobData->currentPage()) * $jobData->perPage();@endphp
			@php $display_record=$j.'-'.$k; @endphp
			@php $total_page=$jobData->lastPage(); 


			@endphp
@endif				
{{--*/use \Carbon\Carbon;/*--}}
			
<div class="number_list"> <!-- Showing $display_record total of  count($total_records) ? $total_records : '0'  jobs -->
	<div class="pull-right form_inputs col-md-3">
		<select class="customselect pull-right selectrecords"  data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
			<option value="">Jobs Displayed</option><!-- ---- updated : 27july2017, for text changes ----- -->
			<option value="10" <?php if($records==10){ echo 'selected'; } ?>>10</option>
			<option value="15" <?php if($records==15){ echo 'selected'; } ?>>15</option>
			<option value="20" <?php if($records==20){  echo 'selected'; } ?>>20</option>
			<option value="30" <?php if($records==30){ echo 'selected'; } ?>>30</option>
			<option value="40" <?php if($records==40){ echo 'selected'; } ?>>40</option>
		</select>
	</div>
</div>
	<div class="white_inner_sec ">
	<table width="100%">
	<tr><th class="greycolor text-center">Job Id</th><!-- ---- updated : 27july2017, for text changes ----- -->
	<th class="text-center">Job Name</th>
	<th class="text-center">Hiring Method</th>
	<th class="text-center">Category</th>
	<th class="text-center">Status</th>
	<th class="text-center">Total Applicants</th><!-- ---- updated : 27july2017, for text changes ----- -->
	<th class="text-center">Total Hired</th><!-- ---- updated : 27july2017, for text changes ----- -->
	<th class="text-center">Start date</th>
	<th class="text-center">End date</th>
	</tr>
		@if(count($jobData))
			
			@php $i= ($jobData->currentPage() - 1) * $jobData->perPage() + 1; 

			
			@endphp
			@foreach($jobData AS $k => $value)
				@php $jobId = Crypt::encrypt($value->_id); @endphp
				<tr>
				<td data-list-label="Sr. no" class="greycolor text-center">{{$value->job_id}}
				</td>
				<td data-list-label="Job Name" class="text-center"><a href="jobdetail/{{$jobId}}" class="textleft">{{ ucfirst($value->title) }} </a>
					<?php //echo '<br>for '.humanTiming(strtotime($value->created_at)).' ago'; 
					?>
				</td>
				
				<td data-list-label="Hiring Method" class="text-center">
					@if($value->job_published_type == 0)
						Manual
					@elseif($value->job_published_type == 1)
						Automatic
					@else
						Rehire
					@endif
				</td>
				
				
				<td data-list-label="Category" class="text-center">{{ ucfirst($value['category'][0]['name']) }}</td>
				<td data-list-label="Status" class="text-center">
					@if($value->job_status==1)
						<span  class=" color_orange">{{'Active'}}</span>
					@elseif($value->job_status==2)
						{{'Filled'}}	
					@elseif($value->job_status==3)	
						{{'In Progress'}}
					@elseif($value->job_status==4)	
						<span  class=" color_green ">{{'Completed'}}</span>						
					@endif	
				</td>
				<td data-list-label="Total Applicants" class="text-center">
					@if($value->total_applied)
						{{$value->total_applied}}
					@else
						{{'0'}}	
					@endif	
				 </td>
				<td data-list-label="Total Hired" class="text-center">
					@if($value->total_hired)
						{{$value->total_hired}}
					@else
						{{'0'}}	
					@endif	
				 </td>
				<td data-list-label="Start date" class="text-center">

				@php 
				/*$d= (string)$value->shifts[0]['start_date'];
				$utcdatetime = new \MongoDB\BSON\UTCDateTime($d); 
				$datetime  = $utcdatetime->toDateTime();
				$start = $datetime->format('Y-m-d');*/
				@endphp
				 {{ date('F d, Y',strtotime($value->start_date)) }} 

				</td>
				<td data-list-label="End date" class="text-center">{{ date('F d, Y',strtotime($value->end_date)) }} </td>
				</tr>
			<?php $i += 1;?>
			@endforeach
		@else
			<tr>
				<td class="text-center text-danger" colspan="9">
					{{$msg}}
				</td>
			</tr>	
		@endif	


	</table>
	<div class="pull-right pagination-common" data-ref="dashboard">
			<?php echo $jobData->render(); ?>
		</div> 

<?php
	/*******Need to common*******/

	function humanTiming ($time)
	{
	    $time = time() - $time; // to get the time since that moment
	    $time = ($time<1)? 1 : $time;
	    $tokens = array (
	        31536000 => 'year',
	        2592000 => 'month',
	        604800 => 'week',
	        86400 => 'day',
	        3600 => 'hour',
	        60 => 'minute',
	        1 => 'second'
	    );

	    foreach ($tokens as $unit => $text) {
	        if ($time < $unit) continue;
	        $numberOfUnits = floor($time / $unit);
	        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
	    }
	}
?>

