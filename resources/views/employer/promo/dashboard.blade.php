@section('css')
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
@endsection
@section('title')
{{$title}}
@endsection
@extends('employer.layout')
@section('content')
<div class="white_inner_sec">
@include('flash::message')
	{!! Form::open(['url'=>'employer/jobdata','id'=>'filter_form']) !!}
	{!! Form::hidden('url','jobdata',['class'=>'url']) !!}	
	{!! Form::hidden('timezone',null,['id'=>'timezone']) !!}	
	<div class="search_input">
		<!-- ------------- search by keyword ----------- -->
		{!! Form::text('job_name',null,['placeholder'=>'Search by Job name','id'=>'jobname']) !!}
		<span><i class="fa fa-search" aria-hidden="true"></i></span>
	</div>
	<div class="category_input job_category">
		<!-- ------------- search by category ----------- -->
		{!! Form::select('category',[''=>'Category']+$category,null,['class'=>'customselect',  'id'=>'category']) !!}
	</div>
	<div class="category_input">
		<!-- ------------- search by status ----------- -->
		{!! Form::select('status',[''=>'Status',1=>'Active',2=>'Filled',3=>'In Progress',4=>'Completed'],null,['class'=>'customselect']) !!}
	</div>
	<div class="filter_reset_div">
		<button class="jobsearch" type="button">Search</button>
		<button class="reset" type="button">Clear</button>
		<button class="exportActiveJobs" type="button">Export</button><!-- ---- export results to csv ----- -->
	</div>
	{!! Form::close() !!}
</div>

	<div class="table_dashboard wow fadeInUp jobdata">
				
	</div>
</div>
@endsection
@section('js')
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<script src="{{asset('public/employer/js/loader.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/bootbox.min.js')}}" type="text/javascript"></script> 
<!-- --------- common js ------------ --> 
<script src="{{ asset('public/employer/js/common.js') }}" type="text/javascript"></script>
<script type="text/javascript">
 	 wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
	$(document).ready(function(){
		var url = $('#filter_form').attr('action');
		postJob(url);

	});

	$(document).on('click', '.pagination a', function (e) 
	 {
		e.preventDefault();
		
		var url=$(this).attr('href');

		postJob(url);
	 });

	function postJob(url,csv_export)
	{
		var record =  '';
		if($('.selectrecords').val()!='' && $('.selectrecords').val() != undefined)
		{
			record = $('.selectrecords').val();
		}
		var filter_data = $('#filter_form').serialize();
		Loader();
		$.ajax({
					type : 'get',
					url : url,
					data : filter_data+'&records='+record+'&csv_export='+csv_export,
					dataType : 'html',
					beforesend:function(){
						Loader();
					},
					success : function(data){
						RemoveLoader();
						$('.jobdata').empty().html(data);
					},
					error : function(data,ajaxOptions, thrownError){
						RemoveLoader();

						var errors = jQuery.parseJSON(data.responseText);
						if(errors.success==false){
							new PNotify({
								type: 'error',
								title: Error,
								text: 'Something went wrong!!!'
							});
						}
						
					}
				});
	}

	/*
	DESC : to filter results as per the applied filters.
	*/
	$(document).on('click','.jobsearch',function(){
		var url = $('#filter_form').attr('action');
		postJob(url);
	});
	
	/*
	DESC : to export results to csv.
	*/
	$(document).on('click','.exportActiveJobs',function(){
		export_csv(path+'employer/export-active-jobs','filter_form');
	});
	
	/*
	DESC : select no. of records to be shown.
	*/
	$(document).on('change','.selectrecords',function(){
		var url = $('#filter_form').attr('action');
		postJob(url);
	});


	$(document).on('change','#jobrating',function(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		Loader();
		$.ajax({

				type : 'post',
				url : $('.rating').attr('action'),
				data : 'value='+$(this).data('jobvalue')+'&rating='+$(this).val()+'&_token='+CSRF_TOKEN,
				dataType : 'html',
				beforesend:function(){
					Loader();
				},
				success : function(data){
					window.location.reload();
				},
				error : function(data,ajaxOptions, thrownError){
					RemoveLoader();

					var errors = jQuery.parseJSON(data.responseText);
					if(errors.success==false){
						new PNotify({
							type: 'error',
							title: Error,
							text: 'Something went wrong!!!'
						});
					}
				}	
		});
	})

	$(document).on('click','.reset',function(){
		$('#filter_form')[0].reset();
		var url = $('#filter_form').attr('action');
		postJob(url,false);
	});
 </script> 
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
var token = $('meta[name="csrf-token"]').attr('content');
	Loader();
	$.ajax({
		type     : 'POST',
		url		:	path+'employer/employer-jobs',
		data		:	{'_token':token},
		datatype : 'json',
		success : function(data) 
		{
			RemoveLoader();
			 $("#jobname").autocomplete({
				source: data.location,
			});
		},
		error: function(data) {
			RemoveLoader();
		}
	});	
</script>
@endsection
