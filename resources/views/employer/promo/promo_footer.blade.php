
<!-- footer section  -->
<footer class="footer-main">
	<div class="container">
    	<div class="row">
        	<div class="col-md-3 col-sm-4">
            		<div class="footer-logo">
               	    <a href="{{url('')}}"><img src="{{asset('public/employer/images/logo.png')}}" alt=""/></a>
                    </div>
            </div>
            <div class="col-md-9 col-sm-8">
            	<div class="footer-support-social">
                	<ul>
                    	<!--<li><a target="_blank" href="https://www.facebook.com/MadWall-Employment-150017992231257/"><i class="fa fa-facebook"></i></a></li>-->
                    	<li><a target="_blank" href="https://www.facebook.com/MadWallEmployment/"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/madwall.employment/"><i class="fa fa-instagram"></i></a></li>
                        <li><a target="_blank" href="https://www.linkedin.com/company-beta/11115309/"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="footer-nav">
                <ul>
                	<!-- <li><a href="#">Contact us</a></li> -->
                  <li><a href="#" data-toggle="modal" data-target="#contact">Contact us</a></li>
                    <li><a href="{{url('/about-us')}}">About MadWall</a></li>
                    <li><a href="{{url('/terms-condtion')}}">Terms And Conditions</a></li>
                    <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                    <li><a id="openDocuemnt" href="javascript:void(0)" data-toggle="modal" data-target="#doc">Documentation</a></li>
                </ul>
                </div>
                <div class="footer-copyright">
                Copyright reserved by <span>MadWall Inc</span>
                </div>
            </div>
        </div>
    </div>

<!--forgot popup-->
<div class="modal fade signup forgot" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
        <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
      </div>
    {!! Form::open(['id'=>'forgotpassword','url'=>'employer/forgotpassword']) !!}  
      <div class="modal-body">
       <div class="form_signup">
       <div class="row">
       <div class="col-md-12">
       <div class="form_grp">
       <!-- <input type="text" placeholder="E-mail"> -->
       {!! Form::text('email',null,['placeholder'=>'E-mail']) !!}
       <span class="error_msgg" style="display:none;"></span>
       </div>
       </div>
       </div>
       
       </div>
       
      </div>
      <div class="modal-footer">
        <button type="submit" class="submit-button">Submit</button>
        <button type="button" class="cancel-button" data-dismiss="modal" aria-label="Close">Cancel</button>
        
    {!! Form::close() !!}
      </div>
    
    </div>
  </div>
</div>

	
	
	<!-- ---- call helper function to get documentation details ---- -->
	<?php
	$document = Helper::doc_promo_footer();
	$industries = Helper::indstry_promo_footer();
	
	?>
	<!-- ----------------------------------------------------------- -->
	
	
	
    <div class="modal fade signup forgot" id="doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Documents</h4>
                </div>
                <div class="modal-body">
                    <div class="documents">
                        <ul>
                            @foreach($document as $document)
                            <li><div class="doc_img"><i class="{{$document->extension}}" aria-hidden="true"></i>
                                </div> <h6>{{$document->general_file_name}}</h6>
                                <a href="{{$document->general_file_url}}" download><div class="button_download" ><button>Download</button></div></a>
                            </li>
                            @endforeach
                            
                            
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
					<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    <!-- ----------------- 4 aug 2017, shivani - to display file upload error messages via modal popup instead of bootbox alert----- -->
    <div class="modal fade FileUploadError" id="FileUploadError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <div class="modal-body">
                    <p id="file_error_message_here"></p>
                </div>
                <div class="modal-footer">
					<button type="button" class="submit-button" data-dismiss="modal" aria-label="Close">OK</button>
					<button type="button" class="cancel-button" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div id="SeeMore" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">More Details</h4>
      </div>
      <div class="modal-body">

        <div class="Detailspopup">
          <label>Name</label> <span class="ValueDetail">Richard Anderson</span>
        </div>
        <div class="Detailspopup">
          <label>Current Position</label> <span class="ValueDetail">Order Picker</span>
        </div>
        <div class="Detailspopup">
          <label>Location</label> <span class="ValueDetail">Milton</span>
        </div>
        <div class="Detailspopup">
          <label>Member Since</label> <span class="ValueDetail">June 2018</span>
        </div>
        <div class="Detailspopup">
          <label>Field Of Interest</label> <span class="ValueDetail">Operations</span>
        </div>
        <div class="Detailspopup">
          <label>Where You see yourself in 5 years</label> <span class="ValueDetail">Manager</span>
        </div>
        <div class="Detailspopup">
          <label>Skills</label> <span class="ValueDetail">Attention to Details</span>
        </div>
        <div class="Detailspopup brdrbtmzero">
          <label>Certifications</label> <span class="ValueDetail">Fall Arrest</span>
        </div>
      </div>
  
    </div>

  </div>
</div>

@include('models.contactus')
</footer>
<!-- footer section end -->
</div>


<?php
if(isset($openDocument) && $openDocument == true){
	?>
	<script>
	
	$(document).ready(function(){
		$('#openDocuemnt').click();
	});
	
	</script>
	<?php
}
?>






<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0Ucux0_UPErLjpzmwKqvnaD7yot5J08&amp;libraries=places">
    </script>

<script src="{{asset('public/employer/js/jquery.geocomplete.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/jquery.validate.min.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/admin/js/chosen.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/loader.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/bootbox.min.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/jquery.maskedinput.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/contactus.js')}}" type="text/javascript"></script>    
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>

<script src="{{asset('public/employer/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script  src="{{ asset('public/employer/js/cookies.js') }}" type="text/javascript"></script>
<script type="text/javascript">
var path = '{{ url("/") }}/';
var adminname = "employer";
</script>
<script>
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);

</script>
<script type="text/javascript">

  $(function(){
        $("#location").geocomplete({
          details: ".pickLocation",
          types: ["geocode", "establishment"],
        }).bind("geocode:result", function(event, result){
            var coor = result.geometry.location.lat();
            $('#key').val(1);
            //$('#pickup-location').val(coor); 
            $('.pickLocation').find('span.error_msgg').hide();  
          }).bind("blur", function(event, results){
            setTimeout(function(){
              if($('#key').val() == '')
              {
                $('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid company address.');
                $('#lat').val('');
                $('#lng').val('');
              }
              $('#key').val('');
            },1000);
            
          }) 
        ;
      });
</script>
<script type="text/javascript">

    var industry_detail=<?php echo json_encode($industries); ?>;
    //~ Object.keys(industry_detail).forEach(function(key) {
        //~ if( industry_detail[key].name ){
            //~ $('#data').append("<div class='col-md-4'><div class='inner_serves_full'><div class='img_div_serves'><img src="+window.location.href+"uploads/"+industry_detail[key].image+"></div><div class='description-div'><h6>"+industry_detail[key].name+"</h6><p id='indstry-description'>"+industry_detail[key].description+"</p></div></div></div>");
        //~ }
    //~ });

   wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
    
</script>

<script type="text/javascript">
jQuery("form.employer_login_form").validate({
    rules: {
      "email":{
         required:true 
      },
      "password":{
         required:true 
      },
    },
    messages: {
        "email":{
          required:"Please enter your registered Email."
        },
        "password":{
          required:"Please enter your Password."
        },
    },
    errorElement:'span',
    errorClass:'error_msg errormsges',
    submitHandler: function(form) {
      Loader();
      form.submit();
      //$('form.employer_login_form').submit();
    /*$.ajax({
            url: $('form.employer_login_form').attr('action'),
            type: 'post',
            dataType: 'json',
            data: $('form.employer_login_form').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
              if(data.status==1)
              {
                window.location.href=data.message;
              }
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.field-icon').find('span.error_msgg').slideDown(400).html(obj);
                 }) 
              }

            },
            complete: function() { //RemoveLoader() 
              }
          });*/
    }
});


$(document).on('click','#registrationEmployer',function(){
    if($('select[name="industry_type[]"]').val()==null)
    {
		$('.industrytype').find('span.error_msgg').slideDown(400).html("Please select at least one value in type of industries field.");
		$('.industrytype').find('span.error_msgg').show();
    }
    else
    {
		$('.industrytype').find('span.error_msgg').hide();
    }
    if($('#location').val())
    {
		if($('#lat').val() == '' || $('#lng').val() == '')
			$('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid location.'); 
    }

    
    if($('#phone_number').val() == '')
    {
		$('.phonenumber').find('span.error_msgg').slideDown(400).html('Please enter phone number.');
       //$('.companycontact').find('span.error_msgg').slideDown(400).html('Please enter contact number.'); 
    }
    else
    {
		$('.phonenumber').find('span.error_msgg').hide();
		$('.companycontact').find('span.error_msgg').hide();  
    }	

    $('#register').submit();
});




	/*
	DESC : to add new rule for password : 1 number, 1 special character, 1 alphabet
	9 aug 2017, shivani
	*/
	$.validator.addMethod("pwcheck",
		function(value, element) {
			return /^((?=(.*[0-9]){1,})(?=(.*[a-z]){0,})(?=(.*[A-Z]){1,})(?=(.*\W+){1,})(?=\S)).{6,25}$/.test(value);
		});




  jQuery("#register").validate({
    
        // Specify the validation rules
        rules: {
            "company_name": {
              required:true,
            },
            "first_name": {
              required:true,
            },
            "last_name": {
              required:true,
            },
            "email": {
              required:true,
              email : true
            },
            "email_confirmation": {
              required:true,
              equalTo: "#email_company"
            },
            "country_code": {
              required:true,
            },
            "company_code": {
              required:true,
            },
            "password": {
              required:true,
              pwcheck:true,
              minlength: 6,
              maxlength: 25,
            },
            "password_confirmation": {
              required:true,
              equalTo: "#password_register"
            },
            "company_address": {
              required:true,
            },
            "number_worker": {
              //required:true,
              number: true
            },
            "user_image": {
              //required:true,
            },
            "company_description":{
              //required:true,
              minlength : 10,
              maxlength : 10000
            },
            "agree":{
              required:true,
            }
    },
        
        // Specify the validation error messages
        messages: {
           "company_name": {
                required: "Please enter Company Name",
            },
            "first_name": {
                required: "Please enter Contact First Name",
            },
            "last_name": {
                required: "Please enter Contact Last Name ",
            },
            "email": {
                required: "Please enter your email ",
            },
            "password": {
                required: "Please enter password ",
                pwcheck : "Password must contain at least one uppercase letter, one lowercase letter, a number, a special character, and must be at least six characters long.",
            },
            "password_confirmation": {
                required: "Please enter confirm password ",
                equalTo: "Passwords do not match",
            },
            "email_confirmation": {
                required: "Please enter confirm email ",
                equalTo: "Emails do not match",
            },
            "company_address": {
                required: "Please enter company address ",
            },
            "number_worker": {
                required: "Please select at least one value in number of workers’ field. ",
            },
            
            "user_image":{
              required: "Please select images.",
            },
            "company_description":{
              required:"Please enter company description",
            },
            "country_code":{
              required:"Please enter country code",
            },
            "company_code":{
              required:"Please enter country code",
            },
            "agree":{
              required:"Please agree to the Terms and Conditions and Privacy Policy.",
            }
        },
        errorElement:'span',
        errorClass:'error_msg',
        errorPlacement: function(error, element) {
           // alert(element.attr("name"));
            $(element).parent('.form_grp').find('span.error_msgg').slideDown(400).html(error);
            if(element.attr("name") == "user_image"){
              $(element).closest('.form_grp').parent('.form-field').find('span.error_msgg').slideDown(400).html(error);
            }

            /*if(element.attr("name") == "phone"){
                $(element).closest('.country_codes').parent('.phonenumber').find('span.error_msgg').slideDown(400).html(error);
            }

            if(element.attr("name") == "company_contact"){
                $(element).closest('.country_codes').parent('.companycontact').find('span.error_msgg').slideDown(400).html(error);
            }*/

            if(element.attr("name") == "agree"){
                $(element).closest('span').parent('.agree').find('span.error_msgg').slideDown(400).html(error);
            }
			
            
        },

        submitHandler: function(form,e) {
			$('span.error_msg').hide();
            $('span.error_msgg').hide();
          /*$('form#register').submit();*/
          var form_data = new FormData($('form#register')[0]);
			
          $.ajax({
            url: $('form#register').attr('action'),
            type: 'post',
            dataType: 'json',
            data: form_data,
            contentType: false,
            processData: false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                Loader();
                $('#registration').prop( "disabled", true );
            },
            success: function(data) {
              if(data.status==1)
              {
                //$('#msg').html('<div class="alert alert-success"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</strong>.</div>');
                //setTimeout(function(){
                  $('#myModal').hide();
                  $('form#register')[0].reset();
                  window.location.reload();
                //},1000);

              }
              if(data.status==0)
              {
                $('#registration').prop( "disabled", false );
                $('#msg').html('<div class="alert alert-danger"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.errors+'</strong>.</div>');
              }
            },
            error: function(error) { 

              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              $('#registration').prop( "disabled", false );
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
            
                   	
                    if(i == 'industry_type')
                      $('.industrytype').find('span.error_msgg').slideDown(400).html(obj);

                  	if(i == 'country_code')
                      $('.phonenumber').find('span.error_msgg').slideDown(400).html(obj);

                  	if(i == 'company_code')
                      $('.companycontact').find('span.error_msgg').slideDown(400).html(obj);

                  	

                    if(i == 'agree')
                      $('.agree').find('span.error_msgg').slideDown(400).html(obj);  

                    if(i=='company_description')
                      $('.description').find('span.error_msgg').slideDown(400).html(obj);

                      if(i == "phone")
                         $('.phonenumber').find('span.error_msgg').slideDown(400).html(obj);
                      

                      if(i == "company_contact")
                        $('.companycontact').find('span.error_msgg').slideDown(400).html(obj);
                          
                      if(i == 'token_mismatch')
                        window.location.reload();

                 }) 
              }

            },
            complete: function() { RemoveLoader() }
          }); 
        }
    });


$(document).on('change','#user_image',function(e){

        var fileExtension = ['png','jpg','jpeg'];
        var file = $(this)[0].files;
        var type = file[0].type;
       
        // Check File Size
        if( file[0].size >5242880 ) {
            //bootbox.alert("Please upload a valid certificate within a max range of 5 MB");
            //updated : 4 aug 2017, shivani - to display error via popup instead of bootbox alert
            $('#file_error_message_here').html("Please upload a valid company logo within a max range of 5 MB");
            $(this).val('');
            $('#FileUploadError').modal('show');
           
        //   alert("Please upload a valid certificate within a max range of 5 MB");
            return false;
        }

        // Check File Type
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
           // bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            //updated : 4 aug 2017, shivani - to display error via popup instead of bootbox alert
            $('#file_error_message_here').html("Only "+fileExtension.join(', ')+ " format is allowed ");

              $(this).val('');

            $('#FileUploadError').modal('show');
			       
            
            
          //alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            return false;
        }
        Loader();
        $(this).closest('.certificate').find('.certificate_name').val(file[0].name);
        firebase_multiple_upload( file, type, $(this) );
    });
    
    
    
    //4 aug 2017, shivani- to resolve page scroll issue when an error is there for file uploads
    $("#FileUploadError").on("hidden.bs.modal", function () {
		//hide the image if already shown any.
		$("#preview_img").hide();
		$('#file_error_message_here').empty();
		$('body').addClass('modal-open');
	});
	//4 aug 2017, shivani- to remove all errors and inputs empty when signup modal is shown.
    $("#myModal").on("shown.bs.modal", function () {
		$(".has-error").empty().hide();
		$('input[name="email"]').val('');
		$('input[name="password"]').val('');
		$('input[name="password_confirmation"]').val('');
	});
    
    
    
    function firebase_multiple_upload( file,type ,reference ){

        var timestamp=Date.now();
        var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
        var metadata = {
            contentType: type,
        };
        
        var blob_image=new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
           // reference.closest('.certificate').find('.firebase_url').val(snapshot.downloadURL);
           $('#image').val(snapshot.downloadURL);
           //4 aug 2017, shivani - to preview img when it is uploaded
           $("#preview_img").find('img').removeAttr('src').attr('src',snapshot.downloadURL);
           $("#preview_img").show();
           
            //certificate.push(snapshot.downloadURL);
            //certificate['url'] = snapshot.downloadURL;
            removeLoader()
        }).catch(function(error) {
            console.log('firebase error occured:'+error);
            RemoveLoader();
        });
    }


    jQuery("form#forgotpassword").validate({
    rules: {
      "email":{
         required:true 
      }
    },
    messages: {
        "email":{
          required:"Please enter your registered Email."
        }
    },
    errorElement:'span',
    errorClass:'error_msg errormsges',
    submitHandler: function(form) {
      
      Loader();
    $.ajax({
            url: $('form#forgotpassword').attr('action'),
            type: 'post',
            dataType: 'json',
            data: $('form#forgotpassword').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
              $('form#forgotpassword')[0].reset();
              $('#forgot').modal('hide');
              window.location.reload();
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);

                    if(i == 'token_mismatch')
                        window.location.reload();
                 }) 
              }

            },
            complete: function() { //alert('b nn'); 
            }
          });
    }
});
</script>
<script type="text/javascript">
   $(".chosen-select").chosen({
        placeholder_text_single: "option",
        no_results_text: "Oops, nothing found!"
      });
   $(document).ready(function(){
      $('#multiselect_chosen').removeAttr('style');
   });

   $("#phone").intlTelInput({
      // allowDropdown: false,
       autoHideDialCode: false,
     // autoPlaceholder: "off",
      // dropdownContainer: "body",
       excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // initialCountry: "auto",
       nationalMode: false,
       	initialCountry: "ca",
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "public/employer/js/utils.js"
    });

    $("#company_code").intlTelInput({
       autoHideDialCode: false,
       nationalMode: false,
       	initialCountry: "ca",
       	excludeCountries: ["us"],
      utilsScript: "public/employer/js/utils.js"
    });


    $("#contact_number").mask("(999)-999-9999");
    $("#phone_number").mask("(999)-999-9999");
$('.default').removeAttr( 'style' );
  //  $('#location').keypress(function(e) {
     // alert(e.which);
  /*if (e.which == 8) {
      $('#lat').val('');
      $('#lng').val('');
  }*/
//});
   
</script>







<script>
	/*
	Added on : 29 july 2017
	Added by : shivani, Debut infotech
	DESC : to auto refresh page after 5 minutes of inactivity
	*/
     var time = new Date().getTime();
     $(document.body).bind("mousemove keypress", function(e) {
         time = new Date().getTime();
     });

     function refresh() {
         if(new Date().getTime() - time >= 600000) 
             window.location.reload(true);
         else 
             setTimeout(refresh, 10000);
     }
     
    function hide_message() {
        $('.flashMessage').empty().fadeOut();
    }
    
    
    /*
    Function to check notifications after every 5seconds
    */ 
	function check_notifications(){
		$.ajax({
            url: path+'employer/check-notification',
            type: 'post',
            dataType: 'json',
            data: $('form#forgotpassword').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
              $('form#forgotpassword')[0].reset();
              $('#forgot').modal('hide');
              window.location.reload();
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
                    if(i == 'token_mismatch')
                        window.location.reload();
                 }) 
              }
            },
            complete: function() { //alert('b nn'); 
            }
        });
	}

	setTimeout(refresh, 10000);
	//to hide success and error message after 3 seconds
	setTimeout(hide_message, 10000);

	//to check notifications
	setTimeout(check_notifications,5000);
</script>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '114032705934595'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=114032705934595&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->



</body>
</html>
