				 
@if(!empty($processData))
<div class="location_sec" style="padding:0px;width:100%;margin-bottom:27px;">
	<span><i class="fa fa-clock-o" aria-hidden="true"></i></span> Selected Job Shift Date and Time 
	<div class="text_job" style="width:446px;font-weight:bold; <?php if(count($processData) > 4){ ?>overflow-y: scroll; height: 131px;<?php } ?>">
	@foreach($processData as $shft)
		@php 
		
		$start_date = Carbon\Carbon::parse($shft['start_date'])->format('d-M l'); 
		$start_time = Carbon\Carbon::parse($shft['start_date'])->format('H:i'); 
		$end_time = Carbon\Carbon::parse($shft['end_date'])->format('H:i'); 
		$end_date = Carbon\Carbon::parse($shft['end_date'])->format('d-M l'); 
		
		@endphp
		<p>{{$start_date}} , {{$start_time}} - {{$end_date}} , {{$end_time}}</p>		
	@endforeach
	</div>						
</div>
@endif
