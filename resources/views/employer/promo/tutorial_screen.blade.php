<!--tutorial-->
    <div class="modal fade tutorial_pop" id="tut_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<div class="tut_pop_inner">            
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
								<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
								<li data-target="#carousel-example-generic" data-slide-to="1"></li>
								<li data-target="#carousel-example-generic" data-slide-to="2"></li>
							</ol>
							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
								<div class="item active">
									<img src="{{asset('public/employer/images/tut1.png')}}" alt=""/>
									<p> View and filter all jobs by category or status of the job. Displayed information includes the hiring method used to fill the posting, number of applicants, and the status of the job.</p>
								</div>
								<div class="item">
									<img src="{{asset('public/employer/images/tut2.png')}}" alt=""/>
									<p>Shows jobs which have already been completed, with additional information such as the method of hiring used, number of applicants, as well as the start and end date of the job.</p>
								</div>
								<div class="item">
									<img src="{{asset('public/employer/images/tut3.png')}}" alt=""/>
									<p>Fill in details regarding your required posting, which MadWall workers will be able to see on their app. Select from either Automatic, Manual or Rehire methods of hiring.</p>
								</div>
							</div>
							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>          
			</div>
		</div>
    </div>
    <!--tutorial end-->
