<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MadWall| Reset Password</title>
<link href="{{ asset('public/employer/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('public/employer/css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('public/employer/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/employer/css/owl.carousel.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
<link rel="shortcut icon" href="{{ url('public/logos/favicon.ico') }}"/>

<script src="{{ asset('public/employer/js/jquery-1.11.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.main.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/wow.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.malihu.PageScroll2id.js') }}" type="text/javascript"></script>
</head>

<body>
<div class="loader-section">

<div class="cssload-container">
	<div class="cssload-whirlpool"></div>
</div>
</div>

<div class="wrapper">

<div class="main">
<!-- Header section -->
<header class="header-main reset_password wow fadeIn ">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 colo-sm-3">
<div class="logo"><a href="{{ url('/') }}"><img src="{{ asset('images/logo_dash.png') }}" alt=""></a></div>
</div>

</div>
<div class="inner_banner_sec">
<div class="row">
<h1>Reset <b>Password</b></h1>
</div>
</div>
</div>
</header>

<!-- Hero section -->


<div class="main_sub_sec">
<div class="reset_sec">
<div class="container">
<div class="col-md-offset-3 col-md-6">
{!! Form::open(['url'=>'employer/reset']) !!}

{!! Form::hidden('token',$token) !!}
{!! Form::hidden('email',$email) !!}
<div class="reset_pswrd_sec">
<div class="form_inner_sec_reset">

<div class="col-md-12"><div class="field_forms"><div class="label_form"><label>Enter new password</label></div><div class="form_inputs"><!-- <input type="text"> -->
  {!! Form::password('password',['placeholder'=>'Password']) !!}

  @if ($errors->has('password'))
        <span class="error_msgg">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
</div></div></div>
<div class="col-md-12"><div class="field_forms"><div class="label_form"><label>Confirm new password</label></div><div class="form_inputs"><!-- <input type="text"> -->
  
  {!! Form::password('password_confirmation',['placeholder'=>'Confirm password']) !!}
  @if ($errors->has('password_confirmation'))
      <span class="error_msgg">
          <strong>{{ $errors->first('password_confirmation') }}</strong>
      </span>
  @endif
</div></div></div>
</div>
<div class="date_time_div_post_job">
<div class="buttons">
<button type="submit">Submit</button>
</div>
</div>
</div>
{!! Form::close() !!}
</div>
</div>
</div>
</div>

</div>

<!-- footer section  -->
<footer class="footer-main">
	<div class="container">
    	<div class="row">
        	<div class="col-md-3 col-sm-4">
            		<div class="footer-logo">
               	    <img src="images/logo.png" alt=""/>
                    </div>
            </div>
            <div class="col-md-9 col-sm-8">
            	<div class="footer-support-social">
                	<ul>
                    	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="footer-nav">
                <ul>
                	<li><a href="#">Contact us</a></li>
                    <li><a href="#">Term &amp; condition</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
                </div>
                <div class="footer-copyright">
                Copyright reserved by <span>MadWall</span>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade signup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign up</h4>
      </div>
      <div class="modal-body">
       <div class="form_signup">
       <div class="row">
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Company Name">
       </div>
       </div>
       </div>
       <div class="row">
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Email">
       </div>
       </div>
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Phone no">
       </div>
       </div>
       </div>
       <div class="row">
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Email">
       </div>
       </div>
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Phone no">
       </div>
       </div>
       </div>
       <div class="row">
       <div class="col-md-6">
       <div class="form_grp">
       <input type="password" placeholder="Password">
       </div>
       </div>
       <div class="col-md-6">
       <div class="form_grp">
       <input type="password" placeholder="Confirm Password">
       </div>
       </div>
       </div>
       </div>
       <div class="form_signup">
       <div class="row">
       <div class="col-md-6">
       <div class="form_grp">
      <select class="customselect"  data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
     <option>Type of Industry</option>
       <option>Type of Industry</option>
         <option>Type of Industry</option>
           <option>Type of Industry</option>
      </select>
       </div>
       </div>
       <div class="col-md-6">
       <div class="form_grp">
      
      <select class="customselect"  data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
     <option>No. of Workers.</option>
       <option>No. of Workers.</option>
         <option>No. of Workers.</option>
           <option>No. of Workers.</option>
      </select>
  
       </div>
       </div>
       </div>
       <div class="row">
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Contact Name">
       </div>
       </div>
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Contact No.">
       </div>
       </div>
       </div>
       <div class="row">
       <div class="col-md-6">
       <div class="form_grp">
       <input type="text" placeholder="Location">
       </div>
       </div>
       
       </div>
       <div class="row">
       <div class="col-md-12">
       <div class="form_grp">
       <input type="checkbox" id="signup_check" class="customCheckbox"> <label for="signup_check">I agree to the <a href="#">term & conditions</a> and <a href="#">privacy policy</a></label>
       </div>
       </div>
       
       </div>
       </div>
      </div>
      <div class="modal-footer signup_ftr">
        <button type="submit">Submit</button>
  
      </div>
    </div>
  </div>
</div>

<!--forgot popup-->
<div class="modal fade signup forgot" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
      </div>
      <div class="modal-body">
       <div class="form_signup">
       <div class="row">
       <div class="col-md-12">
       <div class="form_grp">
       <input type="text" placeholder="E-mail">
       </div>
       </div>
       </div>
       
       </div>
       
      </div>
      <div class="modal-footer signup_ftr ">
        <button type="submit">Submit</button>
  
      </div>
    </div>
  </div>
</div>
</footer>
<!-- footer section end -->
</div>

<script type="text/javascript">

   wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
</script>


</body>

</html>
