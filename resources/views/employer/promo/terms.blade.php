
@extends('employer.layout')
@section('css')

<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
@endsection
@section('title')
{{$title}}
@endsection
@section('content')
<div class="white_inner_sec wow fadeInUp">

<div class="row">
<div class="col-md-12">
	<h3>{{$title}}</h3>
<p>@if(count($cmspage) > 0)
{!!html_entity_decode($cmspage[0]['content'])!!}
  @endif</p>
</div>
</div>

</div>

</div>
@endsection
@section('js')
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<script src="{{asset('public/employer/js/loader.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/bootbox.min.js')}}" type="text/javascript"></script> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endsection
