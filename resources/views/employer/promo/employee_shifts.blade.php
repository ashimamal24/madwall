

@if($err_message == false && !empty($shiftDetails))
@foreach($shiftDetails as $app)
<input type="hidden" value="{{$app->total_hours_worked}}" class="total_hours"/>
<input type="hidden" value="{{$app->jobseeker_id}}" name="jobseeker_id"/>

<?php
		$first_shift = $job_startShiftDate;
		while($first_shift->format('l') != "Sunday"){
			$first_shift->subDays(1);
		}

		//print_r($first_shift->format('l'));die();
		$last_shift =  $job_endShiftDate;//Carbon\Carbon::parse($app->app_shift->sortBy('start_date')->last()->start_date);
		while($last_shift->format('l') != "Saturday"){
			
			$last_shift->addDays(1);

		}

		?>



		@if($job_status->job_status == 3)

			@if (\Carbon\Carbon::now() > $first_shift && \Carbon\Carbon::now() < $job_endShiftDate)

	      	 		<input type="hidden" value="{{\Carbon\Carbon::parse($last_shift)->format('l - F d, Y')}}" name="current_date"/>
		
			@endif


		@endif
		<?php
		//print_r($last_shift->format('l y/m/d'));die();
		$arr = [];
		//print_r($first_shift);print_r($last_shift);die();
		while($first_shift <= $last_shift->EndOfDay()){
			$arr[]= $first_shift->copy();
			$first_shift->addDays(1);
		}
	/*	print_r($arr);
		//~ print_r($appArr);
		 die();*/


		$appArr = [];
		
		foreach($app->app_shift as $shift){

			$appArr[] =	Carbon\Carbon::parse($shift->start_date);
		}
		$app_id = $app->app_shift->first()->app_id;
		$job_id = $app->app_shift->first()->job_id;

		foreach ($arr as $key => $value) {
			foreach ($appArr as $key1 => $value1) {
				if($value->format('y/m/d') == $value1->format('y/m/d')){
					$arr[$key] =$value1;
				}
			}
		}
		//~ print_r($app->app_shift->sortBy('start_date')->toArray());
		/* print_r($arr);
		//~ print_r($appArr);
		 die();*/
?>
<table class="table table-bordered">
	<input type="hidden" name="app_id" value="{{$app_id}}" />
	<input type="hidden" name="job_id" value="{{$job_id}}" />
<tr>
	<td colspan="5" align="center"><h4>{{$name}}</h4></td>
</tr>

</table>
<table id="tbl" class="table table-bordered">
<thead>
<tr>
	<th>Date and Day</th>
	<th>Start Time</th>
	<th>End Time</th>
	<th>Lunch Hours</th>
	<th>Total Hours</th>
</tr>
</thead>
<tbody><!-- @php $i=0; @endphp -->
@foreach($arr as $array)
	@if(in_array($array,$appArr))
	<!-- @php $i++; @endphp -->
		<tr>
			<td>{{Carbon\Carbon::parse($app->app_shift->where('start_date',$array)->first()->start_date)->format('l - F d, Y')}}</td>
			<td><input size="4" class="form-control work_hours start_{{$app->app_shift->where('start_date',$array)->first()->_id}}" 
							name="shiftstart[{{$app->app_shift->where('start_date',$array)->first()->_id}}]" data-id="{{$app->app_shift->where('start_date',$array)->first()->_id}}" value="{{Carbon\Carbon::parse($app->app_shift->where('start_date',$array)->first()->start_date)->format('H:i')}}" type="text"></td>
			<td><input size="4" class="form-control work_hours end_{{$app->app_shift->where('start_date',$array)->first()->_id}}" 
							name="shiftend[{{$app->app_shift->where('start_date',$array)->first()->_id}}]" data-id="{{$app->app_shift->where('start_date',$array)->first()->_id}}" value="{{Carbon\Carbon::parse($app->app_shift->where('start_date',$array)->first()->end_date)->format('H:i')}}" type="text"></td>
			<td>{{$app->app_shift->where('start_date',$array)->first()->lunch_hour}}</td>
			<td><div class="form_inputs total_hour_column ttotal_{{$app->app_shift->where('start_date',$array)->first()->_id}}" data-total="{{$app->app_shift->where('start_date',$array)->first()->total_hours_worked}}">{{$app->app_shift->where('start_date',$array)->first()->total_hours_worked}} <i class="fa fa-trash-o pull-right delete-shift" action="{{url('employer/delete-job')}}/{{$app->app_shift->where('start_date',$array)->first()->id}}" action-admin="{{url('admin/delete-job')}}/{{$app->app_shift->where('start_date',$array)->first()->id}}" style="font-size:16px;color:red;cursor: pointer;"></i></div></td>
		</tr>
	@else
		<tr>
			<td class="date_time" date="{{$array}}">{{Carbon\Carbon::parse($array)->format('l - F d, Y')}}</td>
			<td class="start_shift">N/A</td>
			<td class="end_shift">N/A</td>
			<td class="lunch_time">N/A</td>
			<td><a href="#" class="edit-job">Add Shift</a></td>
		</tr>
	@endif
	
@endforeach
</tbody>
</table>
@endforeach

@else
<!-- 
<input type="hidden" value="$app->total_hours_worked" class="total_hours"/>
<input type="hidden" value="$app->jobseeker_id" name="jobseeker_id"/> -->
<input type="hidden" value="{{$employee_id}}" name="jobseeker_id"/>
<?php
		$first_shift = $job_startShiftDate;
		while($first_shift->format('l') != "Sunday"){
			$first_shift->subDays(1);
		}

		//print_r($first_shift->format('l'));die();
		$last_shift =  $job_endShiftDate;//Carbon\Carbon::parse($app->app_shift->sortBy('start_date')->last()->start_date);
		while($last_shift->format('l') != "Saturday"){
			
			$last_shift->addDays(1);

		}

		while($first_shift <= $last_shift->EndOfDay()){
			$arr[]= $first_shift->copy();
			$first_shift->addDays(1);
		}
		?>
		<table class="table table-bordered">
			<input type="hidden" name="app_id" value="{{$job_app_id}}" />
			<input type="hidden" name="job_id" value="{{$job_id}}" />
		<tr>
			<td colspan="5" align="center"><h4>{{$name}}</h4></td>
		</tr>

		</table>
		<table id="tbl" class="table table-bordered">
		<thead>
		<tr>
			<th>Date and Day</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th>Lunch Hours</th>
			<th>Total Hours</th>
		</tr>
		</thead>
		<tbody><!-- @php $i=0; @endphp -->
		@foreach($arr as $array)
		<tr>
			<td class="date_time" date="{{$array}}">{{Carbon\Carbon::parse($array)->format('l - F d, Y')}}</td>
			<td class="start_shift">N/A</td>
			<td class="end_shift">N/A</td>
			<td class="lunch_time">N/A</td>
			<td><a href="#" class="edit-job">Add Shift</a></td>
		</tr>
        @endforeach 
      </tbody>
    </table>		

@endif

<table style="display: none"><tr><td class="hidden_div"></td></tr></table>
