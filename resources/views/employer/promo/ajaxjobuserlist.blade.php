@php $total_records=$total_page=$display_record='';@endphp
@if(count($jobData))
	@php $total_records=$jobCount;@endphp
	@php $j=($jobData->currentPage() - 1) * $jobData->perPage() + 1;@endphp
	@php $k=($jobData->currentPage()) * $jobData->perPage();@endphp
	@php $display_record=$j.'-'.$k; @endphp
	@php $total_page=$jobData->lastPage(); @endphp
@endif				

<div class="table_dashboard">
	<table width="100%">
		<tr>
			<th class="greycolor text-center">Number</th>
			<th class="text-center">Candidate Name</th>
			<th class="text-center">Certifications</th>
			<th class="text-center">Download Resume</th>
			<th class="text-center">Rating</th>
			<th class="text-center">Status</th>
			<th class="text-center">Total Hours Worked</th>
			<th class="text-center">Action</th>
			<th class="text-center">Timesheets</th>
		</tr>

		@if(count($jobData))
			@php 		
				$total = count($total_records) ? $total_records : '0';

				$i= ($jobData->currentPage() - 1) * $jobData->perPage() + 1;
			@endphp
			
			@foreach($jobData AS $k =>$value)
			
				@if(!empty($value->applyjobuser))
					@php $applicationId = Crypt::encrypt($value->_id); 
					@endphp
					<tr>
						<td data-list-label="Sr. no" class="greycolor text-center">{{$i}}</td>
						<!-- ----- Candidate name ------ -->
						<td data-list-label="Candidate Name" class="text-center">
							
							<!-- ------ 18 aug 2017, shivani - to display dummy image if jobseeker image is not available ----- -->
							<div class="candi_img">
								<?php
									if(!empty($value->applyjobuser->image)){
										$url = $value->applyjobuser->image;
									}else{
										$url = url('public/employer/images/profile_avatar.png')	;
									}
								
								?>
								
								<img height="50" width="50" src="{{$url}}" alt=""/>
							</div>
							
							
							
						<div class="name_candi">{{$value->applyjobuser->first_name}}  {{$value->applyjobuser->last_name}}</div></td>
						
						
						
						<!-- -------- Added : 28 aug 2017, shivani - to list down candidate certifications ------- -->
						<td data-list-label="Certifications" class="text-center">
							
							
							
							@if(!empty($value->applyjobuser->additional_documents))
								
								<a href="javascript:void(0);" title="Check Certifications" class="check_jobseeker_docs" data-id="{{$value->applyjobuser->_id}}"><i class="fa fa-eye color_blue" aria-hidden="true"></i> </a>
							@else
								N/A
							@endif
							
							
							
						</td>
						
						
						<!-- ----- CV URL ----- -->
						
						<td data-list-label="Download CV" class="text-center">
							@if(!empty($value->applyjobuser->cv_url))
								<a href="{{$value->applyjobuser->cv_url}}" target="_blank">
									<i class="fa fa-download" aria-hidden="true"></i>
								</a>
							@else
								N/A
							@endif
							
						</td>
						<!--- ----- Rating ------ -->
						<td data-list-label="Rating" class="text-center color_orange">						
<!--
							<p class="text-notification">
-->
								
								@if(!empty($value->applyjobuser->rating))
								<strong>
									<?php
										echo $rating = Helper::user_rating($value->applyjobuser->rating);
									?>
									
								</strong> 
								@else
								N/A
								@endif
<!--
							</p>
-->
						</td>
						<!--- ----- Status ------ -->
						<td data-list-label="Status" class="text-center">						
							<?php
								$status = 'N/A';
								if($value->job_status == 5){
									$status = 'Cancelled';
								}
								else if($value->job_status == 7){
									$status = 'Removed';
								}
								else if($value->job_status == 4){
									$status = 'Complete';
								}else if($value->job_status == 1 || $value->job_status == 2){
									$status = 'Hired';
								}else{
									$status = 'Pending';
								}
							?>
							{{$status}}
							
						</td>
						<!--- ----- Total Hours Worked ------ -->
						<td data-list-label="Rating" class="text-center">						
							<?php
								$totalHoursWorked = 0;
								if(!empty($value->total_hours_worked)){
									$totalHoursWorked = $value->total_hours_worked;
								}
								
							?>
							{{$totalHoursWorked}}
						</td>
						
						<!-- ----- Action ---- -->
						<td  data-list-label="Action" class="text-center actions">
							@if(($value->job_type==1 && $value->job_status==1) || ($value->job_type==1 && $value->job_status==2) || ($value->job_type==2 && $value->job_status==2) || ($value->job_type==2 && $value->job_status==1))
								
								<!-- --------- hired ----------- -->
								<a href="javascript:void(0);" title="hired"><i class="fa fa-check color_green" aria-hidden="true"></i></a>
								&nbsp;
								<!-- --------- cancel employee ----------- -->
								<a href="javascript:void(0);" class="cancel_employee" data-apply_value="{{$applicationId}}" title="cancel employee" data-confirm-message="Are you sure, you want to cancel this employee?"><i class="fa fa-trash color_red" aria-hidden="true"></i></a>
								
								
							@elseif($value->job_type==2 && $value->job_status==0)						
								{{'N/A'}}
							@elseif($value->job_type==0)
								@if($value->job_status==0)
									<?php
										$timeLeft = Helper::check_difference_in_hours($value->job->start_date);
									?>
									
									<!-- ----- if max workers are already hired ------ -->
										@if($value->job->number_of_worker_required == $value->job->total_hired)
											<!-- ----- accept candidate button (Disabled)  ----- -->
											<a href="javascript:void(0);" data-apply_value="{{$applicationId}}" class="accept_max_hired" data-type="accepted" data-confirm-message="Sorry, your job vacancies are full">
												<i class="fa fa-thumbs-up color_green" aria-hidden="true"></i>
											</a>
											<!-- ----- decline candidate button  (Disabled) ----- -->
											<a href="javascript:void(0);" data-apply_value="{{$applicationId}}" class="declined_max_hired" data-type="rejected" data-confirm-message="Sorry, your requirement for maximum workers has already been fulfilled">
												<i class="fa fa-thumbs-down color_red" aria-hidden="true"></i>
											</a>
										@else
											<!-- ----- accept candidate button ----- -->
											<a href="javascript:void(0);" data-apply_value="{{$applicationId}}" class="accept" data-type="accepted" data-confirm-message="Are you sure you want to accept the candidate?">
												<i class="fa fa-thumbs-up color_green" aria-hidden="true"></i>
											</a>
											<!-- ----- decline candidate button ----- -->
											<a href="javascript:void(0);" data-apply_value="{{$applicationId}}" class="declined" data-type="rejected" data-confirm-message="Are you sure you want to decline the candidate?">
												<i class="fa fa-thumbs-down color_red" aria-hidden="true"></i>
											</a>
										@endif

								@elseif($value->job_status==1)
									<!-- --------- hired ----------- -->
									<a href="javascript:void(0);" title="hired"><i class="fa fa-check color_green" aria-hidden="true"></i></a>
									&nbsp;
									<!-- --------- cancel employee ----------- -->
									<a href="javascript:void(0);" class="cancel_employee" data-apply_value="{{$applicationId}}" title="cancel employee" data-confirm-message="Are you sure, you want to cancel this employee?"><i class="fa fa-trash color_red" aria-hidden="true"></i></a>
								@else
									<a href="javascript:void(0);" title="Cancelled/Removed From the job"><i class="fa fa-times color_red" aria-hidden="true"></i></a>
								@endif
							@endif	
							
						</td>
						<td align="center">
							<!-- ------ update hours button, if any of the shift has been completed -->
							@if($value->app_shift)
								<?php $displayShift = false; 
								foreach($value->app_shift as $shft){
									if(\Carbon\Carbon::parse($shft['end_date']) < \Carbon\Carbon::now() ){
										$displayShift = true;
									}
								}
								
								if($displayShift == true){
								?>								
									<a href="javascript:void(0);" title="view/update employee work hours" class="check_shift_details" data-jobseeker="{{$value->jobseeker_id}}" data-job="{{$value->job_id}}"><i class="fa fa-eye color_blue" aria-hidden="true"></i></a>
								<?php 
								} else{
									echo '-';
								}
								?>
							@else
								-
							@endif
						</td>
					</tr>
					@php $i += 1; @endphp
				@endif
			@endforeach
		@else
			<tr>
				<td class="text-center text-danger" colspan="8">
					No records to display.
				</td>
			</tr>	
		@endif
		{!! Form::hidden('records',$records,['class'=>'records']) !!}
	</table>
	<div class="pull-right pagination-common" data-ref="dashboard">
		<?php echo $jobData->render(); ?>
	</div> 
</div>
