@section('css')

<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
@endsection
@section('title')
{{$title}}
@endsection
@extends('employer.layout')
@section('content')
@if(!empty($user->company_description))
<div class="white_inner_sec wow fadeInUp">

<div class="row">
<div class="col-md-12">
@php 
//print_r($user);
@endphp


<p>{{ $user->company_description }}</p>


</div>
</div>

</div>
@endif
<div class="white_inner_sec wow fadeInUp">


@include('flash::message')

{!! Form::open(['id'=>'about','url'=>'employer/contact-us']) !!}   

@if(Auth::check())
	{!! Form::hidden('name',Auth::user()->first_name.' '.Auth::user()->last_name,['placeholder'=>'Name']) !!}
	{!! Form::hidden('req_frm','employer',['placeholder'=>'Name']) !!}
@else
	{!! Form::text('name',null,['placeholder'=>'Name', 'id'=>'name', 'maxlength'=>'25' ]) !!}
@endif

@if(Auth::check())
	{!! Form::hidden('email',Auth::user()->email,['placeholder'=>'Name']) !!}
@else
	{!! Form::text('email',null,['placeholder'=>'E-mail', 'id'=>'email']) !!}
@endif




<div class="field_forms"><div class="label_form"><label>Please use the below text box to contact us </label></div>
<div class="form_grp">
{!! Form::textarea('content',null,['rows'=>2,'placeholder'=>'Write your message', 'id'=>'content']) !!}
<span class="error_msgg" style="display:none;"></span>
</div></div>
<div class="date_time_div_post_job">

<div class="buttons">
<!-- <button>Submit</button> -->

<button type="button" id="aboutmadwall">Submit</button>
</div>
</div>
{!! Form::close() !!}

</div>
@endsection
@section('js')
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>  
<script src="{{asset('public/employer/js/jquery.validate.min.js')}}" type="text/javascript"></script>    
<script type="text/javascript">
	$(document).on('click','#aboutmadwall',function(){
		$.ajax({
            url: $('form#about').attr('action'),
            type: 'post',
            dataType: 'json',
            data: $('form#about').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
              if(data.status==1)
              {
                window.location.reload();
              }
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msgg').hide();
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('textarea[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
                 }) 
              }

            },
            complete: function() { //RemoveLoader() 
              }
          });
	});
	
</script>
@endsection
