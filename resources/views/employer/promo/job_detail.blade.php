@extends('employer.layout')

<?php 
//date_default_timezone_set($_COOKIE['client_timezone']);
$job_detail = Helper::getDates($job_post->shifts);
$start_date = date('Y-m-d H:i:s',strtotime($job_post->start_date));
$end_date = date('Y-m-d',strtotime($job_post->end_date));
$shiftsData = [];
function getDates($dates)
{
    $data = '';
    foreach($dates AS $k => $row)
    {
        $data[] = date('Y-m-d H:i:s',strtotime($row['start_date'])); 
    }
    return $data;
}

$jobId = Crypt::encrypt($job_post->_id);
foreach($job_post->jobshifts AS $k => $row){
	$shiftsData[] = ['start_date'=>date('H:i',strtotime($row['start_date'])),'end_date'=>date('H:i',strtotime($row['end_date']))];
}

?>

@section('css')
	<link rel="stylesheet" href="{{ asset('public/employer/css/datepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{asset('public/employer/js/date-timepicker/jquery.timepicker.css')}}" />
	<link href="{{ asset( 'public/admin/css/bootstrap-datetimepicker.css' ) }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
	<style>
		.remove_margin{
			margin-top : 0px !important;
		}
		.filter_reset_divClear{
			filter_reset_divClear
		}
	</style>
@endsection
@section('title')
	{{$title}}
@endsection

@section('content')
	<div class="white_inner_sec wow fadeInUp">
		@include('flash::message')
		<div class="job_detail_sec">
			<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
				<div class="job_detail_left_sec">
					<!-- --- job title --- -->
					<h2>{{ ucwords($job_post->title) }}</h2>
					<!-- --- job start/end time --- -->
					<span class="job_time">
						{{ date('jS F ',strtotime($start_date)) }} • {{ date('jS F Y',strtotime($end_date)) }}
					</span>
					<!-- --- job description --- -->
					<p style="word-wrap:break-word;">						
						{!!html_entity_decode($job_post->description)!!}
					</p>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="job_time_loc">
					<!-- --- salary --- -->
					<div class="hours_working">
						${{ number_format($job_post->salary_per_hour,2) }}/hr
					</div>
					<!-- --- address --- -->
					<div class="location_sec width_100">
						<span>
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</span>
						<b>{{ $job_post->address }}</b>
					</div>
					<div class="clearfix"></div>
					<div class="location_sec">

						<!-- --- shift start/end time --- -->
						<div class="text_job">
							<b>
					
								@include('employer.promo.shift_data',['processData'=>$job_post->shifts])
								
								
							</b>
						</div>
					</div>
					<div class="clearfix"></div>
					@if(!empty($job_post->lunch_hour))
						<!-- --- Lunch Hours --- -->
						<div class="location_sec">
							<div class="text_job">
								Lunch Hours :  <b>{{ floatval(number_format(($job_post->lunch_hour/60),2)) }} hours</b>
							</div>
						</div>
						<div class="clearfix"></div>
					@endif
						
					<!-- --- no. of workers required --- -->
					<div class="location_sec">
						<div class="text_job">
							Workers required :  <b>{{ $job_post->number_of_worker_required }}</b>
						</div>
					</div>
					<div class="clearfix"></div>
					<!-- --- Category --- -->
					<div class="location_sec">
						<div class="text_job">Category : <b>{{ ucwords($job_post->category[0]['name']) }}</b></div>
					</div>
					<div class="clearfix"></div>
					<!-- --- Sub-Category --- -->
					<div class="location_sec">
						<div class="text_job">Sub-Category : <b>{{ ucwords($job_post->subcategory[0]['name']) }}</b></div>
					</div>
					<div class="clearfix"></div>
					
					<!-- ------- method to hire the remaining employees ------- -->
					@if($job_post->job_published_type == 2)
					<div class="location_sec" style="padding-right:62px;">
						<div class="text_job">Method to hire remaining employees : 
							<b>
								@if($job_post->rehire_type == 1)
									Automatic
								@else
									Manual
								@endif
							</b>
						</div>
					</div>
					<div class="clearfix"></div>
					@endif

					<!-- --- to display physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->physical_requirement))
					<div class="location_sec width_100">
						<div class="text_job">Physical Requirements : <b>{!!html_entity_decode($job_post->physical_requirement)!!}
						</b></div>
					</div><div class="clearfix"></div>
					@endif
					<!-- --- to display physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->safety_hazards))
					<div class="location_sec width_100">
						<div class="text_job">Safety Hazards : <b> {!!html_entity_decode($job_post->safety_hazards)!!} </b></div>
						
					</div><div class="clearfix"></div>
					@endif
					<!-- --- to display physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->contact_name))
					<div class="location_sec width_100">
						<div class="text_job">Contact Name : 
						
								<b>{{$job_post->contact_name}}</b></div>
						
					</div><div class="clearfix"></div>
					@endif
					<!-- --- to display physical req. and other fields on job detail page --- -->
					@if(!empty($job_post->meeting_location))
					<div class="location_sec width_100">
						<div class="text_job">Meeting Location : 
							<b>{{$job_post->meeting_location}}</b>
						</div>    
					</div>
					<div class="clearfix"></div>
					@endif
					<!-- --- Skills --- -->
					<div class="location_sec width_100">
						<div class="text_job">Skills : </div>
						<div class="tag_align_right">
							@foreach($job_post->skill AS $key => $value)
								<div class="">&nbsp;<b>{{ $value['name'] }}</b></div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<!-- --- job dates --- -->
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="calender_div">
					<div class="formfieldset" id="datepicker"></div>
				</div>
			</div>
		</div>

		<div class="job_detail_table">
			<!-- ----- SEARCH JOBSEEKER FORM ------ -->
			<div class="row">
				{!! Form::open(['url'=>'employer/jobdetail/jobapplylisting','onsubmit'=>'return false','id'=>'filter_form']) !!}
					{!! Form::hidden('jobvalue',$jobId) !!}
					<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
						<div class="number_list">
							<!-- ---- success/error message here ---- -->
							<div class="msg"></div>
						</div>
					</div>
				
                <div class="select-job-view">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="jobdetailsearch">
								<!-- ---- search field --- -->
								<div class="search_input remove_margin select-job-de">
									{!! Form::text('job_name',null,['placeholder'=>'Search by Candidate name','id'=>'job_name']) !!}<span><i class="fa fa-search search" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
						<!-- ---- select no. of record to be shown --- -->
						<div class="col-md-2 col-sm-4 col-xs-12 select-job-de1">
							{!! Form::select('records',[10=>10,15=>15,20=>20,30=>30,40=>40],null,['class'=>'customselect selectrecords','data-jcf'=>'{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}']) !!}
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="filter_reset_divClear">								
								<button class="reset" id="reset_search" type="button">Clear</button><!-- for text changes -->
								<button class="export_candidate_list" id="export_candidate_list" type="button">Export</button><!-- for text changes -->
							</div>
						</div>
                        </div>
				
					{!! Form::close() !!}
				</div>
				
				<!-- ---- jobseekers list will display ---- -->
				<div class="table_job_detail jobdata"></div>
			</div>

			<div class="buttons">
				<!-- ---- Edit job button ---- -->				
				@if($job_edit['edit'] == true)
					<a href="{{ url('employer/editjob/'.$jobId) }}" class="jobedit">Edit job</a>
				@endif
				
				
				<!-- ----- Display candidate list for rehire job ----- -->
				@if($job_post->job_published_type == 2)
				<input type="hidden" id="rehire_job_id" value="{{Crypt::encrypt($job_post->_id)}}">
					<a href="javascript:void(0)" id="display_jobseeker_list" data-id="{{$job_post->_id}}">Invite Employees</a>
				@endif
				
				
				<!-- ---- Delete job button --- -->
				<button href="#deletejob" data-toggle="modal" data-target="#deletejob" type="button" class="delete jobdelete" data-job="{{$jobId}}">Delete job</button>
			</div>
		</div>

@include('models.delete_job')


<!-- --------- to display message when employer accept/decline job application before 24 hours --------- -->

@include('models.error_popup')


@include('models.emp_modals')

<!-- ---------------------------------------------------------------------------------------------------- -->



<!-- ------------- Invite more candidates for rehire type job --------------------- -->
@include('models.rehire',$users)
@include('models.cancel_employee',$users)
@include('models.jobseeker_docs',$users)
@include('employer.promo.work_history_popup')

<!-- ------------------------------------------------------------------------------ -->



@endsection
 
@php	
$datepicker_date = implode(',', $job_detail); 

@endphp

@section('css')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{ asset('public/employer/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- ---------------- datetimepicker -------------------- -->
<script src="{{ asset( 'public/admin/js/bootstrap-datetimepicker.js') }}"></script>
<!-- <script src="{{ asset('public/employer/js/path.js') }}" type="text/javascript"></script>-->
<script src="{{asset('public/employer/js/loader.js')}}" type="text/javascript"></script> 
<script src="{{asset('public/employer/js/bootbox.min.js')}}" type="text/javascript"></script>   
<!-- --------- datepicker js ------------ -->
<script src="{{ asset('public/employer/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- --------- timepicker js ------------ -->
<script type="text/javascript" src="{{asset('public/employer/js/date-timepicker/jquery.timepicker.js')}}"></script>
<!-- --------- common js ------------ --> 
<script src="{{ asset('public/employer/js/common.js') }}" type="text/javascript"></script>
<!-- --------- employer js ------------ --> 
<script src="{{ asset('public/employer/js/employer_jobs.js') }}" type="text/javascript"></script>

<script type="application/javascript">
		
var rateArray = "<?php echo $datepicker_date ?>";

 var d = rateArray.split(',');
 
 
 
 var manualDates = [];
 for(i = 0; i<d.length; i++)
 {
 	manualDates.push(new Date(d[i]));
 }
 


$(document).ready(function () {	
	var token = $('meta[name="csrf-token"]').attr('content');
	//Loader();
	var jobId = "{{Crypt::encrypt($job_post->_id)}}";	
	/*
	DESC : when invite jobseekers button is clicked.
	29 aug 2017, shivani
	*/
	$('#display_jobseeker_list').click(function(){
		$("body").scrollTop()
		//start loader
		$('#rehire_fr_job').modal('show');
	});
	$("#DisableJobAppAction").on("hidden.bs.modal", function () {
		$('#disable_message_here').empty();
	});
	
    $("#datepicker").datepicker(
      	'setDates',manualDates
    );
    
    //to display jobseeker list
	var url = $('#filter_form').attr('action');
    listjobApply(url);
    
	//when job delete button is clicked (get confirmation from user - display modal)
	$(document).on('click','.jobdelete',function(){
		$('#deletejob').modal('show');
	});
	
	//to delete job
	$(document).on('click','#deletejobs',function(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			url:path+'employer/deletejob',
			type:'post',
			data:'value='+$('.jobdelete').data('job')+'&_token='+CSRF_TOKEN,
			dataType:'json',
			beforeSend:function(){
				Loader();
			},
			success:function(data){
				if(data.status){
					window.location.href=data.url;
				}else{
					window.location.reload(true);
				}
			},
			error:function(errors){
				console.log(errors);
			},
			complete:function(){
				RemoveLoader();
			}
		});
	});

});

</script>
<script src="{{ asset( 'public/employer/js/job_detail.js' ) }}" type="text/javascript"></script>
@endsection
