@extends('employer.layout')
@section('css')
<link rel="stylesheet" href="{{ asset('public/employer/css/datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">

<link rel="stylesheet" type="text/css" href="{{asset('public/employer/js/date-timepicker/jquery.timepicker.css')}}" />

<link rel="stylesheet" href="{{ asset('public/employer/text-editor/src/richtext.min.css') }}">

<!-- -------------- toggle css --------------- -->
<link href="{{ asset('public/employer/bootstrap-toggle-master/css/bootstrap-toggle.css') }}" rel="stylesheet">

<link  href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
<style>
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
  .toggle.ios .toggle-handle { border-radius: 20px; }
</style>
@endsection
@section('title')
{{$title}}
@endsection

@section('content')

@php $serverdate = date('Y-m-d'); 
$lat = '';
$lng = '';
$startTime = '';
$endTime = '';
$id = '';
if(count($jobs)){
	$lat = $jobs->location['lat'];
	$lng = $jobs->location['lng'];
	$startTime = date('H:i',strtotime($jobs->start_date));
	$endTime = date('H:i',strtotime($jobs->end_date));
	$id = \Crypt::encrypt($jobs->_id);
}

@endphp
<div class="white_inner_sec wow fadeInUp">
	@include('flash::message')
	{!! Form::model($jobs,['url'=>'employer/savejob','id'=>'addjob']) !!}

	@if(isset($repost_id) && !empty($repost_id))
		{!! Form::hidden('page','repost') !!}
		{!! Form::hidden('repost_id',$repost_id) !!}
	@else
		{!! Form::hidden('page','add') !!}
	@endif

{!! Form::hidden('timezone',null,['id'=>'timezone']) !!}
{!! Form::hidden('process_id',null,['id'=>'process_id']) !!}

<!-- ----------- job post form ---------------- -->
	@include('/employer/promo/job_post_form')
	
	<div class="date_time_div_post_job">
		
		
		<div class="buttons">
			<button type="button" id="set_jobTime" data-status="1">Set Shift</button>
		</div>
	</div>
<!-- ----------- ----------- -------------- ------ -->
<div class="date_time_div_post_job">
	<div class="buttons">
		<button type="button" id="automatichiring" data-status="1">Automatic Hiring</button>
		<button type="button" id="manulhiring" data-status="0">Manual Hiring</button>
		<button type="button" id="rehirejob" data-status="2" data-value="{{ Crypt::encrypt(1) }}">Rehire Employees</button>
	</div>
</div>

<input type="hidden" name="dates" id="dates">
<input type="hidden" name="formatted_dates" id="formatted_dates">
<input type="hidden" name="job_published_type" id="job_published_type">

{!! Form::close() !!}
</div>

<!-- -------- modal popup to confirm job posting from user ------------ -->
<div class="modal fade" id="job_postConfirm" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p id="jobPost_confirmMessage"></p>       
			</div>
			<div class="modal-footer">
				<button class="submit-button" type="button" id="hiring">Hire</button>
				<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!-- -------- modal popup to set strt/end Time for job shifts (selected dates) ------------ -->
<div class="modal fade" id="set_shiftTime" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
			<!-- --------- modal header ---------- --> 
			<div class="modal-header"><h4>Set Job Shift Time</h4></div>   
			
			<div class="alert" id="show_err" style="display:none"></div>			
			<!-- --------- modal body ----------- --> 
			<div class="modal-body" id="append_shiftFieldHere" style="overflow-y: scroll; height: 435px;">
				<!-- ---------- shift start/end fields will append here --------- -->
			</div>
			<!-- --------- modal footer ------- -->
			<div class="modal-footer">
				<button class="submit-button" type="button" id="time_selected">Ok</button>
				<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>



@include('models.rehire',$users)
@include('models.emp_modals')

@endsection
@section('js')
<!-- --------- moment js ------------ -->
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<!-- --------- datepicker js ------------ -->
<script src="{{ asset('public/employer/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- --------- timepicker js ------------ -->
<script type="text/javascript" src="{{asset('public/employer/js/date-timepicker/jquery.timepicker.js')}}"></script>
<!-- --------- text editor js ------------ -->
<script src="{{ asset('public/employer/text-editor/src/jquery.richtext.min.js') }}" type="text/javascript"></script>
<!-- --------- chosen js ------------ -->
<script src="{{asset('public/admin/js/chosen.js')}}" type="text/javascript"></script> 
<!-- --------- google address api ------------ -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0Ucux0_UPErLjpzmwKqvnaD7yot5J08&amp;libraries=places"> </script>
<!-- --------- geocomplete js ------------ -->
<script src="{{asset('public/employer/js/jquery.geocomplete.js')}}" type="text/javascript"></script>  
<!-- --------- validate js ------------ -->  
<script src="{{asset('public/employer/js/jquery.validate.min.js')}}" type="text/javascript"></script>  
<!-- --------- toggle ---------- -->
<script src="{{asset('public/employer/bootstrap-toggle-master/js/bootstrap-toggle.js')}}"></script>
<!-- --------- common js ------------ --> 
<script src="{{ asset('public/employer/js/common.js') }}" type="text/javascript"></script>
<!-- --------- employer job js --------- --> 
<script src="{{ asset('public/employer/js/employer_jobs.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/create_job.js') }}" type="text/javascript"></script>

@endsection
