<!-- -------- modal popup to confirm job posting from user ------------ -->
	<div class="modal fade" id="employee_shiftDetail" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">  
				<div class="modal-header">
					<h4>Employee Timesheets</h4>
				</div>   
				<div class="alert" id="show_shift_err" style="display:none"></div> 
				<div class="modal-body" id="displayShiftHours" style="">
					
				</div>
				<div class="modal-footer">
					<button class="submit-button btn btn-primary displayshftdetail" type="button" id="updateHours">Save and Submit</button>
					<button class="cancel-button btn displayshftdetail" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
					<button class="submit-button btn btn-primary ok_close" type="button" class="close" data-dismiss="modal" aria-label="Close">Ok</button>
				</div>
			</div>
		</div>
	</div>
