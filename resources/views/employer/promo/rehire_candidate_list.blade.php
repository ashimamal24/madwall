@if(count($users))
            <ul>      
            @foreach($users AS $key => $value)
              <li>
                  <div class="rehir_pic"><img src="{{ $value->image }}" /></div>
                  <div class="name_rating"> <p>{{ $value->first_name }} {{ $value->last_name }}</p>
                  
                  {{$value->email}}
                  
                  <div class="rating_rehire">
                          @if( ( $value->rating > 1 ) && ( $value->rating < 1.5 ) )
                              <i class="fa fa-star"></i>
                          @elseif( ( $value->rating > 1.5 ) && ( $value->rating <= 2 || $value->rating < 2.5 ) )
                              <i class="fa fa-star"></i><i class="fa fa-star"></i>  
                          @elseif( $value->rating == 1.5 )
                              <i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>  
                          @elseif( ( $value->rating > 2.5 )&& ( $value->rating <= 3 || $value->rating < 3.5 ) )
                              <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  
                          @elseif( $value->rating == 2.5 )
                              <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>  
                          @elseif( ( $value->rating > 3.5 ) && ( $value->rating <= 4 || $value->rating < 4.5 ) )
                              <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  
                          @elseif( $value->rating >= 4 && $value->rating < 4.5)
                              <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>  
                          @elseif( $value->rating == 4.5 )
                              <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>  
                          @else
                              <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  
                          @endif
                      </div>
                      <div class="check_box_rating">
                         
                          {!! Form::checkbox('offer[]',$value->_id,null,['id'=>$value->_id]) !!}
                            <label for="{{$value->_id}}"><i class="fa fa-check" aria-hidden="true"></i>
                          </label>
                      </div>
                  </div>
              </li>
             
            @endforeach
            </ul>
            {!! Form::hidden('rehirevalue',null,['id'=>'rehirevalue']) !!}
          @else
			<!-- ----------- updated : 28 july 2017, text updates ------- -->
            <p class="nodatafound">No Qualified candidates, Please select another form of hiring</p>
          @endif 
       
       
       </div>
       
      </div>
      <div class="modal-footer ">
      @if(count($users))
        <button class="submit-button" type="submit" id="selectedUser">Submit</button>
        <button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
        
      @else
        <button class="submit-button" type="button" class="close" data-dismiss="modal" aria-label="Close">OK</button>
      @endif 
