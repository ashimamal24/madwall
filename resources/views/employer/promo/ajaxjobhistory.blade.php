@php $total_records=$total_page=$display_record=0;@endphp
@if(count($jobData))

			@php $total_records=$jobCount;@endphp
			@php $j=($jobData->currentPage() - 1) * $jobData->perPage() + 1;@endphp
			@php $k=($jobData->currentPage()) * $jobData->perPage();@endphp
			@php $display_record=$j.'-'.$k; @endphp
			@php $total_page=$jobData->lastPage(); 


			@endphp
@endif				
{{--*/use \Carbon\Carbon;/*--}}			
<div class="number_list"><!-- Showing $display_record  of  count($total_records) ? $total_records : '0'  jobs -->
	<div class="pull-right form_inputs col-md-3">
		<select class="customselect pull-right selectrecords" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
			<option value="">Jobs Displayed</option><!-- ---- updated : 27july2017, for text changes ----- -->
			<option value="10" <?php if($records==10){ echo 'selected'; } ?>>10</option>
			<option value="15" <?php if($records==15){ echo 'selected'; } ?>>15</option>
			<option value="20" <?php if($records==20){  echo 'selected'; } ?>>20</option>
			<option value="30" <?php if($records==30){ echo 'selected'; } ?>>30</option>
			<option value="40" <?php if($records==40){ echo 'selected'; } ?>>40</option>
		</select>
	</div>
</div>
	<div class="white_inner_sec ">
	<table width="100%">
		<!-- ---- updated : 27july2017, for text changes ----- -->
<tr><th class="greycolor text-center">Job Id</th>
<th class="text-center">Job Name</th>
<th class="text-center">Hiring Method</th>
<th class="text-center">Applicants</th>
<th class="text-center">Wage</th>
<th class="text-center">Commision(%)</th>
<!--<th class="text-center">Number of Days</th>-->
<th class="text-center">Sub-Total</th>
<th class="text-center">Total</th> <!-- ---------- end here(27july2017) -------- -->
<th class="text-center">Status</th> 
<th class="text-center">Start Date</th>
<th class="text-center">End Date</th>
</tr>
@if(count($jobData))
			
			@php $i= ($jobData->currentPage() - 1) * $jobData->perPage() + 1; 

			@endphp
			@foreach($jobData AS $k => $value)
				@php $jobId = Crypt::encrypt($value->_id); @endphp
				<tr>
				<td data-list-label="Sr. no" class="greycolor text-center">{{$value->job_id}}
				</td>
				<td data-list-label="Job Name" class="text-center"><a href="history-detail/{{$jobId}}" class="textleft">{{ ucfirst($value->title) }} </a>
					<?php //echo '<br>for '.humanTiming(strtotime($value->created_at)).' ago'; 
					?>
				</td>
				<td data-list-label="Hiring Method" class="text-center">
					@if($value->job_published_type == 0)
						Manual
					@elseif($value->job_published_type == 1)
						Automatic
					@else
						Rehire
					@endif
				</td>
				
				<td data-list-label="Applicants" class="text-center">
					@if($value->total_hired)
						{{$value->total_hired}}
					@else
						{{'0'}}	
					@endif	
				 	
				</td>
				<td data-list-label="Wage" class="text-center ">
					${{number_format($value->salary_per_hour,2)}}	
				</td>
				
				<td data-list-label="Commision(%)" class="text-center ">
					 @php
						
						$dates = array();
						foreach($value->shifts AS $key => $val)
						{
							$dates[] = ['start_date'=>$val['start_date'],'end_date'=>$val['end_date']];
						}
						
						$dates = current($dates);
						
						$start= Carbon\Carbon::parse($dates['start_date']);
						$end= Carbon\Carbon::parse($dates['end_date']);
						$totalDuration = $end->diff($start)->format('%H.%I');
					    //$price = (count($value->shifts) * $value->total_hired * $value->salary_per_hour * $totalDuration); 
					    $price = ($value->salary_per_hour * $value->total_work_hours); 
					    
					    
					    $totalCommision = $value->category[0]['commision'];
					    if(isset($value->category_commision) && !empty($value->category_commision)){
							$totalCommision = $value->category_commision;
						}
					    
					    $commision = (($price * $totalCommision)/100);
					@endphp
					
					{{$totalCommision}}	
				</td>
				<!--<td data-list-label="Number of Days" class="text-center">
					count($value->shifts)	
				</td>-->
				<td data-list-label="Sub-Total" class="text-center">
						

						
						
						 ${{$price}}
				</td>
				
				<td data-list-label="Total" class="text-center ">
					${{number_format($price + $commision,2)}}
				</td>
				<!-- ------- job status ------ -->
				<td data-list-label="Status" class="text-center ">
					
					
					@if($value->is_deleted == true)
						Deleted
					@else
						@if($value->job_status==1)
							<span  class=" color_orange">{{'Active'}}</span>
						@elseif($value->job_status==2)
							{{'Filled'}}	
						@elseif($value->job_status==3)	
							{{'Progress'}}
						@elseif($value->job_status==4)	
							<span  class=" color_green ">{{'Completed'}}</span>
						@endif

					@endif
					
					
					
					
					
				</td>
				<!-- ------------- 4 aug 2017 , shivani (to display job start/end date) --------- -->
				<td data-list-label="Start date" class="text-center">
					{{ date('F d, Y',strtotime($value->start_date)) }} 
				</td>
				<td data-list-label="End date" class="text-center">{{ date('F d, Y',strtotime($value->end_date)) }} </td>
				
				</tr>
			<?php $i += 1;?>
			@endforeach
		@else
			<tr>
				<td class="text-center text-danger" colspan="11">
					{{$msg}}
				</td>
			</tr>	
		@endif


</table>
<div class="pull-right pagination-common" data-ref="dashboard">
		<?php echo $jobData->render(); ?>
</div> 



