<?php
if(!isset($class)){
	$class = 'abc';
}
?>
<div class="row">
	<div class="col-md-4">
		<!-- ----------- job name ---------- -->
		<div class="field_forms">
			<div class="label_form">
				<label>Job Name</label>

			</div>
			<div class="form_inputs">
				{!! Form::text('title',null,['Placeholder'=>'Job Name','id'=>'title','class'=>$class,'maxlength'=>'150']) !!}
				<span class="error_msgg" style="display:none;"></span>
			</div>
		</div>
		<!-- ----------- job category ---------- -->
        <div class="field_forms">
			<div class="label_form">
				<label>Job Category</label>
			</div>
			@if(isset($commission) && is_array($commission) && !empty($commission)) 
				<div class="commission">
					<span><b>Commission : </b></span>
					<span id="commision"><b>{{$commission['commision']}} </b></span> %
				</div>
			@else
				<div class="commission" style="display:none">
					<span><b>Commission : </b></span>
					<span id="commision"><b></b></span> %
				</div>
			@endif
			<div class="form_inputs job_category">
				{!! Form::select('job_category',[''=>'Please select category']+$category,null,['class'=>'customselect subcategory', 'id'=>'category' ]) !!}
				<span class="cat_error" style="display:none;"></span>
				<span class="error_msgg" style="display:none;"></span>
			</div>
		</div>
		<!-- ----------- job sub category ---------- -->
        <div class="field_forms">
			<div class="label_form">
				<label>Job Sub-Category</label>
			</div>
			<div class="form_inputs job_subcategory">
				{!! Form::select('job_subcategory',[''=>'Please select Sub-Category']+$subcategory_All,null,['class'=>'customselect ','id'=>'subcategory']) !!}
				<span class="subcat_error" style="display:none;"></span>
				<span class="error_msgg" style="display:none;"></span>
			</div>
		</div>  
		<!-- ----------- job skills ---------- -->   
		<div class="field_forms">
			<div class="label_form">
				<label>Job Skills</label>
			</div>
			
			<!-- ------------- Skills here ------------- -->
			
			<div class="form_inputs skills">
				{!! Form::select('skills[]',[]+$skill_All,null,['class'=>'chosen-select skills check_skills_selected','multiple'=>'multiple','id'=>'multiselect','data-placeholder'=>'Select some Skills']) !!}
				<span class="error_msgg" style="display:none;"></span>
				<span class="skill_error" style="display:none;"></span>
			</div>
		</div>
		
		<!-- ------------ strength indicator -------------- -->
		<div class="field_forms strength_indicatorDiv" style="display:none;">
			<div class="label_form">
				<label>Job strength Indicator</label>
			</div>
			
			<!-- ------------- Skills here ------------- -->
			<div class="strength_indicator" style="display:none;position: absolute; right: 20px;">
				
			</div>
		</div>
	</div>
  
    <div class="col-md-8">	
		<!-- ----------- job description ---------- -->	
		<div class="field_forms">
			<div class="label_form">
				<label>Job Description</label>
			</div>
			<div class="form_inputs description">
				{!! Form::textarea('description',null,['placeholder'=>'Example: We are looking for a skilled Machine Operator to set up, maintain and operate machinery','class'=>$class,'id'=>'description','id'=>'description','rows'=>2]) !!}
				<span class="error_msgg" style="display:none;"></span>
			</div>
		</div>
		<!-- ----------- physical requirement ---------- -->
		<div class="field_forms">
			<div class="label_form">
				<label>Physical Requirements</label>
			</div>
			<div class="form_inputs">
				{!! Form::textarea('physical_requirement',null,['placeholder'=>'Example: Standing for long periods of time','rows'=>2,'id'=>'physical_requirement','class'=>$class]) !!}
				<span class="error_msgg" style="display:none;"></span>
			</div>
		</div>
		<!-- ----------- safety hazards ---------- -->
		<div class="field_forms">
			<div class="label_form">
				<label>Safety Hazards</label>
			</div>
			<div class="form_inputs">
				{!! Form::textarea('safety_hazards',null,['placeholder'=>'Example: Exposed moving parts','rows'=>2,'id'=>'safety_hazards','class'=>$class]) !!}
				<span class="error_msgg" style="display:none;"></span>
			</div>
		</div>
    </div>
</div>



<div class="date_time_div_post_job">
	
	<div class="row">
		<div class="col-md-6 col-xs-12 col-sm-6">
			<div class="row">
				<div class="col-md-6" id="show_selectedDates_here">
					@if(!empty($jobs->shifts) && $repost == false)
						@include('employer.promo.shift_data',['processData'=>$jobs->shifts])
					@endif
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	<div class="row">
		<div class="col-md-6 col-xs-12 col-sm-6">
			
			<!-- -------------- unpaid lunch hours --------------- -->
			<div class="row">
				<div class="col-md-6">
					<!-- ----------- Hourly wage ---------- -->
					<div class="label_form">
						<label>Unpaid lunch</label>
					</div>
						@if(!empty($jobs->lunch_hour))
							{!! Form::checkbox('unpaid_lunch',true,true,['id'=>'unpaid_lunch','data-toggle'=>'toggle','data-style'=>'ios']) !!}
						@else
							{!! Form::checkbox('unpaid_lunch',true,null,['id'=>'unpaid_lunch','data-toggle'=>'toggle','data-style'=>'ios']) !!}
						@endif		
				</div>
				<!-- ----- lunch Hours ------- -->
				@if(!empty($jobs->lunch_hour))
					<div class="col-md-6" id="lunch_hours_div">
				@else
					<div class="col-md-6" id="lunch_hours_div" style="display:none">
				@endif	
				
					<div class="field_forms">
						<div class="label_form">
							<label>Lunch hours</label>
						</div>
						<div class="form_inputs">
							
							{!! Form::select('lunch_hour',[''=>'Please select hours','15'=>'15 minutes','30'=>'30 minutes','60'=>'1 hour','90'=>'1 and half hour','120'=>'2 hours'],null,['class'=>'customselect', 'id'=>'lunch_hour' ]) !!}
							<span class="lunch_hr_err error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<!-- ----------- job start time ---------- -->
					<div class="field_forms">
						<div class="label_form">
							<label>Start Time</label>
						</div>
						<div class="form_inputs">
							<?php
								$start = $end = null;
								if(!empty($jobs->start_time) && !empty($jobs->end_time)){
									$start = $jobs->start_time;
									$end = $jobs->end_time;									
								}
							?>
							{!! Form::text('start_date',$start,['class'=>'form-control','id'=>'start_date','placeholder'=>'Start time','onkeydown'=>'return false']) !!}
							<span class="error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<!-- ----------- job end time ---------- -->
					<div class="field_forms">
						<div class="label_form">
							<label>End Time</label>
						</div>
						<div class="form_inputs">
							{!! Form::text('end_date',$end,['class'=>'form-control','id'=>'end_date','placeholder'=>'End time','onkeydown'=>'return false']) !!}
							<span class="error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<!-- ----------- Hourly wage ---------- -->
					<div class="field_forms">
						<div class="label_form">
							<label>Hourly wage</label>
						</div>
						<div class="form_inputs salary_div_dol">
							<div class="dolar_sign">$</div>
							{!! Form::text('salary_per_hour',null,['id'=>'salary_per_hour','class'=>$class]) !!}
							<span class="error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<!-- ----------- workers required ---------- -->
					<div class="field_forms">
						<div class="label_form">
							<label>Workers Required</label>
						</div>
						<div class="form_inputs">
							{!! Form::number('number_of_worker_required',null,['placeholder'=>'Worker Required','id'=>'number_of_worker_required','class'=>$class,'min'=>'1','max'=>'2500']) !!}
							<span class="error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<!-- --------- Contact Name ------------ -->
				<div class="col-md-6">
					<div class="field_forms">
						<div class="label_form">
							<label>Contact Name</label>
						</div>
						<div class="form_inputs">
							{!! Form::text('contact_name',null,['placeholder'=>'Contact Name','class'=>$class,'id'=>'contact_name']) !!}
							<span class="error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>
				<!-- --------- Meeting Location ------------ -->
				<div class="col-md-6">
					<div class="field_forms">
						<div class="label_form">
							<label>Meeting Location</label>
						</div>
						<div class="form_inputs">
							{!! Form::text('meeting_location',null,['placeholder'=>'Meeting Location','class'=>$class,'id'=>'meeting_location']) !!}
							<span class="error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>
			</div>
			<!-- ----------- job Address ---------- -->
			<div class="row">
				<div class="col-md-12">
					<div class="field_forms">
						<div class="label_form">
							<label>Job Address</label>
						</div>
						<div class="form_inputs pickLocation">
							{!! Form::text('address',null,['id'=>'location','class'=>$class]) !!}
							{!! Form::hidden('lat',$lat,['id'=>'lat']) !!}
							{!! Form::hidden('lng',$lng,['id'=>'lng']) !!}
							{!! Form::hidden('key',null,['id'=>'key']) !!}
							<span class="error_msgg" style="display:none;"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5 col-sm-6 col-xs-12 pull-right">
			<div class="calender_div">
				<div class="formfieldset" id="datepicker"></div>
				<span class="error_msgg" style="display:none;"></span>
				<span for="person_required" class="error_msg errormsge date_err" style="display:none"></span>
			</div>
		</div>
	</div>
</div>
