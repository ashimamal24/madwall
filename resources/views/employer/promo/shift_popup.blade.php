<!-- -------- modal popup to set strt/end Time for job shifts (selected dates) ------------ -->
<div class="modal fade" id="set_shiftTime" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
			<!-- --------- modal header ---------- --> 
			<div class="modal-header"><h4>Set Job Shift Time</h4></div>   
			
			<div class="alert" id="show_err" style="display:none"></div>			
			<!-- --------- modal body ----------- --> 
			<div class="modal-body" id="append_shiftFieldHere" style="overflow-y: scroll; height: 435px;">
				<!-- ---------- shift start/end fields will append here --------- -->
			</div>
			<!-- --------- modal footer ------- -->
			<div class="modal-footer">
				<button class="submit-button" type="button" id="time_selected">Ok</button>
				<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>
