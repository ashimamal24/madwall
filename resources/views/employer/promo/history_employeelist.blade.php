@php $total_records=$total_page=$display_record='';@endphp
@if(count($jobData))
	@php $total_records=$jobCount;@endphp
	@php $j=($jobData->currentPage() - 1) * $jobData->perPage() + 1;@endphp
	@php $k=($jobData->currentPage()) * $jobData->perPage();@endphp
	@php $display_record=$j.'-'.$k; @endphp
	@php $total_page=$jobData->lastPage(); @endphp
@endif				

<div class="table_dashboard">
	<table width="100%">
		<tr>
			<th class="greycolor text-center">Number</th>
			<th class="text-center">Candidate Name</th>
			<th class="text-center">Certifications</th>
			<th class="text-center">Download Resume</th>
			<th class="text-center">Rating</th>
			<th class="text-center">Status</th>
			<th class="text-center">Total Hours Worked</th>
			<th class="text-center">Timesheets</th>
		</tr>

		@if(count($jobData))
			@php 		
				$total = count($total_records) ? $total_records : '0';

				$i= ($jobData->currentPage() - 1) * $jobData->perPage() + 1;
			@endphp
			
			@foreach($jobData AS $k =>$value)
			
				@if(!empty($value->applyjobuser))
					@php $applicationId = Crypt::encrypt($value->_id); 
					@endphp
					<tr>
						<td data-list-label="Sr. no" class="greycolor text-center">{{$i}}</td>
						
						<td data-list-label="Candidate Name" class="text-center">
							
							<!-- ------ to display dummy image if jobseeker image is not available ----- -->
							<div class="candi_img">
								<?php
									if(!empty($value->applyjobuser->image)){
										$url = $value->applyjobuser->image;
									}else{
										$url = url('public/employer/images/profile_avatar.png')	;
									}
								?>
								<img height="50" width="50" src="{{$url}}" alt=""/>
							</div>
							<!-- ----- Candidate name ------ -->	
							<div class="name_candi">{{$value->applyjobuser->first_name}}  {{$value->applyjobuser->last_name}}</div>
						</td>
						
						<!-- -------- to list down candidate certifications ------- -->
						<td data-list-label="Certifications" class="text-center">
							@if(!empty($value->applyjobuser->additional_documents))
								<a href="javascript:void(0);" title="Check Certifications" class="check_jobseeker_docs" data-id="{{$value->applyjobuser->_id}}"><i class="fa fa-eye color_blue" aria-hidden="true"></i> </a>
							@else
								N/A
							@endif
						</td>
							
						<!-- ----- CV URL ----- -->
						
						<td data-list-label="Download CV" class="text-center">
							@if(!empty($value->applyjobuser->cv_url))
								<a href="{{$value->applyjobuser->cv_url}}" target="_blank">
									<i class="fa fa-download" aria-hidden="true"></i>
								</a>
							@else
								N/A
							@endif
							
						</td>
						<!--- ----- Rating ------ -->
						<td data-list-label="Rating" class="text-center color_orange">						
							<p class="text-notification">								
								<?php
								if( isset( $value->rating ) && $value->rating = true ) {
									$jobRating = Helper::job_rating_for_user($value->jobseeker_id,$value->job_id); 
									if(!empty($jobRating)){														
										?>
										<strong>
										<?php
											echo $rating = Helper::user_rating($jobRating); 
										?>
										</strong> 
										<?php
									}else{
										echo $rating = 'Not rated yet';
									}
								} else {
									echo $rating = 'Not rated yet';
								}
								?>
							</p>							
						</td>
						
						<!-- ----- Employee Status ----- -->
						<td data-list-label="Status" class="text-center">
							<?php
								$status = 'Complete';
								if($value->job_status == 5){
									$status = 'Cancelled';
								}
								if($value->job_status == 7){
									$status = 'Removed';
								}
							?>
							{{$status}}
						</td>
						
						<!-- ------ total Hours Worked ---- -->
						<td data-list-label="Total Hours Worked" class="text-center">
							<?php
								$totalHoursWorked = 0;
								if(!empty($value->total_hours_worked)){
									$totalHoursWorked = $value->total_hours_worked;
								}
								
							?>
							{{$totalHoursWorked}}
						</td>
						
						<!-- ----- Action ---- -->
						<td  data-list-label="Action" class="text-center actions">
							<a href="javascript:void(0);" title="view/update employee work hours" class="check_shift_details" data-jobseeker="{{$value->jobseeker_id}}" data-job="{{$value->job_id}}"><i class="fa fa-eye color_blue" aria-hidden="true"></i></a>
						</td>
					</tr>
					@php $i += 1; @endphp
				@endif
			@endforeach
		@else
			<tr>
				<td class="text-center text-danger" colspan="8">
					No workers hired for this job
				</td>
			</tr>	
		@endif
		{!! Form::hidden('records',$records,['class'=>'records']) !!}
	</table>
	<div class="pull-right pagination-common" data-ref="dashboard">
		<?php echo $jobData->render(); ?>
	</div> 
</div>
