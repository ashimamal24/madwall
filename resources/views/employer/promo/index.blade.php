<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="MadWall is Your Local Innovative On-Demand Staffing Agency. Register today and watch your job fill up in minutes and connect with workers who meet your needs!">
    <title>MadWall | Home</title>
    <link href="{{asset('public/employer/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/employer/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('public/employer/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/employer/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('public/employer/css/owl.theme.default.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/employer/css/developer.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
    <link href="{{asset('public/employer/css/intlTelInput.css')}}" rel="stylesheet">
    <!-- <link  href="{{asset('public/employer/css/demo.css')}}" rel="stylesheet"> -->
    <link href="{{asset('public/employer/css/main.css')}}" rel="stylesheet">
    <script src="{{asset('public/employer/js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/employer/js/bootstrap.js')}}" type="text/javascript"></script>
    <!--<script src="{{asset('public/employer/js/owl.carousel.min.js')}}" type="text/javascript"></script>-->
    <script src="{{asset('public/employer/js/jquery.main.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/employer/js/wow.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/employer/js/jquery.malihu.PageScroll2id.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/employer/js/intlTelInput.js')}}" type="text/javascript"></script>
    <link rel="shortcut icon" href="{{ url('public/logos/favicon.ico') }}" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109836395-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-109836395-1');

        $(document).ready(function() {

                // Calculate number of Slides
                var totalItems = $('.item').length;

                // If there is only three slides
                if (totalItems <= 3) {
                // Set loop option variable to false
                var isLooped = false;
                // Set nav option variable to false
                var isNav = false;
                } 
                else {
                // Set loop option variable to true
                var isLooped = true;


                // Set loop option variable to true
                var isNav = true;
                }

            $('.owl-carousel1').owlCarousel({


                loop: isLooped,
                nav: true,
                items: 4,
                autoplay: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                },
                slideBy: 3,
                loop: isLooped,
            });
            $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
            $(".owl-next").html('<i class="fa fa-chevron-right"></i>');
        });
    </script>
   

</head>

<body>
    <div class="loader-section">

        <div class="cssload-container">
            <div class="cssload-whirlpool"></div>
        </div>
    </div>

    <div class="wrapper">

        <div class="main">
            <!-- Header section -->
            <header class="header-main wow fadeIn " id="top_page_banner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 colo-sm-3">
                            <div class="logo">
                                <a href="{{url('/')}}"><img src="{{asset('public/employer/images/logo.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 colo-sm-9">
                            <a href="javascript:void(0)" class="drop-opener pull-right">
                                <span></span>
                                <span></span>
                                <em class="border"></em>

                            </a>
                            <div class="menu_bar">
                                <div class="navSection">
                                    <div class="nav-holder" id="menu-drop">
                                        <ul>
                                            <li><a href="#how-jobseeker">How To apply</a></li>
                                            <li><a href="#how-hire">How To hire</a></li>
                                            <li><a href="{{url('/about-us')}}">About MadWall</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#contact">Contact us</a></li>
                                            <li><a href="{{url('/blog')}}">Blog</a></li>
                                            @if(Auth::user())

                                            <li class="navbtn">
                                                <ul>
                                                    <li>
                                                        <a href="{{ url('employer/dashboard') }}">Dashboard</a></li>
                                            </li>
                 

                                            </ul>
                                            <li class="navbtn"><a href="{{ url('employer/logout') }}">Logout</a></li>
                                            @else
                                            <li class="navbtn"><a href="#" data-toggle="modal" data-target="#myModal">Sign up</a></li>
                                            @endif

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <header class="header-main header-scroll wow fadeIn ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 colo-sm-3">
                            <div class="logo">
                                <a href="#top_page_banner"><img src="{{asset('public/employer/images/logo.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 colo-sm-9">
                            <a href="#" class="drop-opener pull-right">
                                <span></span>
                                <span></span>
                                <em class="border"></em>

                            </a>
                            <div class="menu_bar">
                                <div class="navSection">
                                    <div class="nav-holder" id="menu-drop">
                                        <ul>
                                            <li><a href="#how-jobseeker">How To Apply</a></li>
                                            <li><a href="#how-hire">How To Hire</a></li>
                                            <li><a href="{{url('/about-us')}}">About MadWall</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#contact">Contact us</a></li>
                                            <li><a href="{{url('/blog')}}">Blog</a></li>
                                            <!-- <li class="navbtn"><a href="#" data-toggle="modal" data-target="#myModal" >Sign up</a></li> -->
                                            @if(Auth::user())

                                            <li class="navbtn">
                                                <ul>
                                                    <li>
                                                        <a href="{{ url('employer/dashboard') }}">Dashboard</a></li>
                                            </li>
                                            </ul>
                                            <li class="navbtn"><a href="{{ url('employer/logout') }}">Logout</a></li>
                                            @else
                                            <li class="navbtn"><a href="#" data-toggle="modal" data-target="#myModal">Sign up</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- Hero section -->
            <div class="hero-section">
                @include('flash::message')
                <div class="container">
                    <div class="hero-section-text wow fadeInLeft">Madwall Staffing
                        <br>
                        <span><b>On-Demand</b> Staffing</span> 
                        </div>

                    <div class="hero-box-block-main wow slideInRight">
                        <div class="hero-box-block">
                            <div class="hero-box-block-top">
                                <div class="hero-box-block-text"><span>Need Workers?</span></div>

                            </div>
                            @if(!Auth::user())
                            <div class="hero-box-block-bot">
                                <!--        <div class="hero-box-block-title"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><span>Need Workers?</span></a> </div>-->
                                {{ Form::open(array('url' => 'employer/login','method'=>'POST','class'=>'employer_login_form','data-ajax'=>false)) }}
                                <div class="hero-box-block-form">
                                    <div class="hero-box-worker-title">Sign-in</div>
                                    <div class="fieldset-form">
                                        <div class="fieldset-field field-icon">

                                            {!! Form::text('email', Cookie::has('email') ? Cookie::get('email') : old('email'),['maxlength'=>'100','placeholder'=>'Email']) !!}
                                            <span class="field-icon-set">
                    <i class="fa fa-envelope"></i>
                </span>
                                            <span class="error_msgg" style="display:none;"></span>
                                            <div class="@if ($errors->has('email')) has-error @endif">
                                                @if ($errors->has('email'))
                                                <p class="text-danger">{{ $errors->first('email') }}</p> @endif
                                            </div>
                                        </div>
                                        <div class="fieldset-field field-icon">

                                            <!-- {!! Form::password('password',['maxlength'=>'100','placeholder'=>'Password'],Cookie::has('password') ? Cookie::get('email') : '') !!} -->
                                            <input type="password" name="password" maxlength=100 value="<?php echo Cookie::has('password') ? Cookie::get('password') : ''; ?>" placeholder="Password">

                                            <span class="field-icon-set">
                    <i class="fa fa-lock"></i>
                </span>
                                            <span class="error_msgg" style="display:none;"></span>
                                            <div class="@if ($errors->has('password')) has-error @endif">
                                                @if ($errors->has('password'))
                                                <p class="text-danger">{{ $errors->first('password') }}</p> @endif
                                            </div>
                                        </div>
                                        <div class="fieldset-field fieldlinklogin">
                                            <!-- <a data-toggle="modal" data-target="#">Forgot password?</a> -->
                                            <label class="fake-check" for="remember">
                                                <!-- <input type="checkbox" checked="" id="agree"> -->
                                                <input id="remember" name="remember" type="checkbox" class="customCheckbox" <?php if(Cookie::has( 'email') && Cookie::has( 'password')) echo "checked"; ?>>
                                                <span><i class="icon-check-small"></i></span>
                                                <label for="remember">Remember Me</label>
                                            </label>
                                            <a data-toggle="modal" data-target="#forgot" class="pull-right">Forgot password?</a>

                                        </div>
                                        <div class="fieldset-field">

                                            {!!Form::button('Login',array('class'=>'login-btn-home','type'=>'submit'))!!}

                                        </div>

                                        <div class="loginregiteruserlink">No Account? <a href="#" data-toggle="modal" data-target="#myModal">Sign up</a>

                                        </div>
                                        <div class="text-center need-box">
                                            <span class="label-2">Need a Job?</span> Download MadWall App from links below.
                                            <span>

                </span>
                                            <a href="https://play.google.com/store/apps/details?id=com.madwall" class="store-play-link">
                                                <img src="{{asset('public/employer/images/favicon_v2.ico')}}" width="20" height="20">Available on the Play Store</span>
                                            </a>

                                            <a href="https://itunes.apple.com/ca/app/madwall-staffing/id1378825478?mt=8" class="store-play-link">
                                                <img src="{{asset('public/employer/images/apple-logo.svg')}}" width="20" height="20">Available on the App Store</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="hero-section-img">
                    <div class="container">
<h1>{{$titles['title']}}</h1></div>
                </div>

            </div>
            <!-- Hero section  end-->
            <div class="clearfix"></div>
            <!-- second section -->
            <div class="clients-logo-section">
                <div class="container">
                    <div class="section-title wow slideInUp"> Industries <span>that  MadWall</span> Serves </div>
                    <div class="clearfix"></div>
                    <div class="madwall_serves client-sec">
                        <div class="owl-carousel owl-carousel1 owl-theme owl-loaded">
                            @foreach($industryContent as $val)
                            <div class="item">
                                <h4>{{ucfirst($val->name)}}</h4>
                                <img src="{{$val->image}}" class="ser-img">
                                <div class="client-box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="{{$val->user_image}}" class="client-img">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="client-desc">
                                                <h5>{{ucfirst($val->user_name)}}</h5>
                                                <h6>Member Since: June 2018</h6>
                                                <p>Order Picker</p>
                                                <a  data-toggle="modal" data-target="#SeeMore" class="seemorebtn" href="#">See More</a>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-9">
                                            <h5>{{ucfirst($val->user_name)}}</h5>
                                            <p class="default_{{$val->_id}}">{{substr(strip_tags($val->description),0,30)}}</p>
                                            <div class="text_{{$val->_id}}" style="display: none;">{!! $val->description !!}
                                            </div>
                                            <div class="more text_change_{{$val->_id}}" data-id="{{$val->_id}}"
                                             style="color:#0b4cc4;"><span>show more</span></div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>

            <div class="hire-step-section highlight-section" id="how-hire">
                <div class="container">
                <div class="section-title wow slideInUp">
                           Highlights
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                        <div class="col-md-4">
                                <div class="TopShadowBox">
                                    <div class="high-box">
                                    <img src="{{asset('public/employer/images/calendar.png')}}" class="img img-fluid">
                                     </div>
                                     <h4>Online Timesheets</h4>
                                     <p>Enter and Validate Worker Hours Online with ease.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="TopShadowBox">
                                    <div class="high-box">
                                    <img src="{{asset('public/employer/images/certificate.png')}}" class="img img-fluid">
                                     </div>
                                     <h4>Job Seeker Details and Certifications</h4>
                                     <p>See all relevant information of the Worker, from CV to all Licences and Certificates.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="TopShadowBox">
                                    <div class="high-box">
                                    <img src="{{asset('public/employer/images/hire.png')}}" class="img img-fluid">
                                     </div>
                                     <h4>Hire Immediately or Screen Further.</h4>
                                     <p>Either hire immediately from our Worker Pool using our Automatic Hiring, or screen further using our Manual Hire! Like your past Worker? Use our Re-Hire Function!</p>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                        <div class="col-md-4">
                                <div class="TopShadowBox">
                                    <div class="high-box">
                                    <img src="{{asset('public/employer/images/money.png')}}" class="img img-fluid">
                                     </div>
                                     <h4>Save Money!</h4>
                                     <p>Not only will you save money on your Hiring and Screening process. You only pay when our Worker’s preform your requested Tasks!</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="TopShadowBox">
                                    <div class="high-box">
                                    <img src="{{asset('public/employer/images/clock.png')}}" class="img img-fluid">
                                     </div>
                                     <h4>Find Workers Faster!</h4>
                                     <p>Find Worker’s Faster 40% faster compared to Traditional Staffing Agencies.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="TopShadowBox">
                                    <div class="high-box">
                                    <img src="{{asset('public/employer/images/worker.png')}}" class="img img-fluid">
                                     </div>
                                     <h4>Long-Term Hire or Temporary Worker’s</h4>
                                     <p>If you are impressed with our Worker’s you can hire them on to your team with ease! Want to further evaluate a Worker’s ability? Feel free to use our Temporary services to put our Workers to the test!</p>
                                </div>
                            </div>
                           
                 
                           
                            </div>
                            <div class="row">
                           
                 
                           
                            </div>
                        </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <!-- third section -->
            <div class="hire-step-section" id="how-hire">
                <div class="container">
                    <div class="">
                        <div class="section-title wow slideInUp">
                            How It Works as a <span>Client</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-sm-4 wow fadeInUp">
                                <div class="hire-step-inner">
                                    <div class="hire-step-title">Register as a Client</div>
                                    <div class="hire-step-text">Select the industry you wish to hire from</div>
                                </div>
                            </div>

                            <div class="col-sm-4 wow fadeInUp">
                                <div class="hire-step-inner">
                                    <div class="hire-step-title">Request Worker</div>
                                    <div class="hire-step-text">Briefly fill a job description and post instantly</div>
                                </div>
                            </div>

                            <div class="col-sm-4 wow fadeInUp">
                                <div class="hire-step-inner">
                                    <div class="hire-step-title">Hire Workers</div>
                                    <div class="hire-step-text">3 different methods to hire workers gives you more control and flexability</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hire-sction-slide wow slideInUp">
                        <img src="{{asset('public/employer/images/web-slide.png')}}" alt="" />
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- fourth section -->

        </div>
    </div>
    <!-- fourth section end -->
    </div>
    <div class="clearfix"></div>

    <div class="container" id="how-jobseeker">
        <div class="row">
            <div class="col-md-6">
                <div class="mobile-slide-screen-main">
                    <img src="{{asset('public/employer/images/mob-slide1.png')}}" alt="" class="mobile-slide-screen-1 wow fadeIn">
                    <img src="{{asset('public/employer/images/mob-slide2.png')}}" alt="" class="mobile-slide-screen-2 wow fadeIn" data-wow-delay="0.8s" data-wow-duration="1s">
                    <img src="{{asset('public/employer/images/mob-slide3.png')}}" alt="" class="mobile-slide-screen-3 wow fadeIn" data-wow-delay="1.5s" data-wow-duration="1s">
                </div>
            </div>
            <div class="col-md-6">
                <div class="jobseeker-step-main">
                    <div class="section-title wow slideInUp">
                        How It Works as a <span>Jobseeker</span>
                    </div>
                    <ul>
                        <li class=" wow fadeInRight">
                            <div class="jobseeker-step-count">1</div>
                            <div class="jobseeker-step-title">Download &amp;
                                <br><span>Register for free</span></div>
                            <div class="jobseeker-step-text">Simply download the App and register at absolutely no cost</div>
                        </li>
                        <li class=" wow fadeInRight">
                            <div class="jobseeker-step-count">2</div>
                            <div class="jobseeker-step-title">Apply to Jobs &amp;
                                <br><span>Get Hired</span></div>
                            <div class="jobseeker-step-text">Search for a job that best fits your schedule, with your desired wage </div>
                        </li>
                        <li class=" wow fadeInRight">
                            <div class="jobseeker-step-count">3</div>
                            <div class="jobseeker-step-title"><span>Get Paid</span></div>
                            <div class="jobseeker-step-text">Instantly check earnings and get paid weekly! </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade signup" id="myModal" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Sign up</h4>
                </div>
                <div class="modal-body">
                    <div id="msg"></div>
                    {!! Form::open(['url'=>'auth/employe/register','id'=>'register']); !!}
                    <div class="form_signup">
                        <h5>Log-in Credentials</h5>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form_grp">
                                    {!! Form::text('company_name',null,['placeholder' => 'Company Name', 'maxlength'=>'50']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('company_name')) has-error @endif">
                                        @if ($errors->has('company_name'))
                                        <p class="text-danger">{{ $errors->first('company_name') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- --------- email --------- -->
                            <div class="col-md-6">
                                <div class="form_grp">
                                    {!! Form::text('email',null,['placeholder' => 'Email','id'=>'email_company','maxlength'=>'40']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('email')) has-error @endif">
                                        @if ($errors->has('email'))
                                        <p class="text-danger">{{ $errors->first('email') }}</p> @endif
                                    </div>
                                </div>
                            </div>

                            <!-- ------------ confirm email ------------ -->

                            <div class="col-md-6">
                                <div class="form_grp">
                                    {!! Form::text('email_confirmation',null,['placeholder' => 'Confirm Email','maxlength'=>'40']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('email')) has-error @endif">
                                        @if ($errors->has('email_confirmation'))
                                        <p class="text-danger">{{ $errors->first('email_confirmation') }}</p> @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <!-- ---------- password ----------- -->
                            <div class="col-md-6">
                                <div class="form_grp">
                                    {!! Form::password('password',['placeholder' => 'Password','id'=>'password_register']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('password')) has-error @endif">
                                        @if ($errors->has('password'))
                                        <p class="text-danger">{{ $errors->first('password') }}</p> @endif
                                    </div>
                                </div>
                            </div>
                            <!-- -------- confirm password ----------- -->
                            <div class="col-md-6">
                                <div class="form_grp">
                                    {!! Form::password('password_confirmation',['placeholder' => 'Confirm Password']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('password_confirmation')) has-error @endif">
                                        @if ($errors->has('password_confirmation'))
                                        <p class="text-danger">{{ $errors->first('password_confirmation') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form_signup">
                        <h5>Company Details</h5>
                        <br>
                        <div class="row">
                            <!-- --------- first name ------ -->
                            <div class="col-md-6">
                                <div class="form_grp">
                                    {!! Form::text('first_name',null,['placeholder' => 'Contact First Name', 'maxlength'=>'20']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('first_name')) has-error @endif">
                                        @if ($errors->has('first_name'))
                                        <p class="text-danger">{{ $errors->first('first_name') }}</p> @endif
                                    </div>
                                </div>
                            </div>
                            <!-- ------- last name ------- -->
                            <div class="col-md-6">
                                <div class="form_grp">
                                    {!! Form::text('last_name',null,['placeholder' => 'Contact Last Name', 'maxlength'=>'20']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('last_name')) has-error @endif">
                                        @if ($errors->has('last_name'))
                                        <p class="text-danger">{{ $errors->first('last_name') }}</p> @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- --------- company contact ---------- -->

                            <div class="col-md-6 phonenumber">
                                <div class="country_codes">
                                    <div class="form_grp number_add">
                                        {!! Form::text('country_code',null,['placeholder' => 'Country Code','id'=>'phone','onkeydown'=>'return false']) !!}
                                    </div>

                                    <div class="form_grp number_cus">
                                        {!! Form::text('phone',null,['placeholder' => 'Contact Phone Number','id'=>'phone_number']) !!}
                                    </div>
                                </div>
                                <span class="error_msgg" style="display:none;"></span>
                                <div class="@if ($errors->has('phone')) has-error @endif">
                                    @if ($errors->has('phone'))
                                    <p class="text-danger">{{ $errors->first('phone') }}</p> @endif
                                </div>
                            </div>

                            <!--<div class="col-md-6 companycontact">
                            <div class="country_codes">
                                <div class="form_grp number_add">
                                    {!! Form::text('company_code',null,['placeholder' => 'Contry code','id'=>'company_code','onkeydown'=>'return false']) !!}
                                </div>
                                <div class="form_grp number_cus">
                                    {!! Form::text('company_contact',null,['placeholder' => 'Contact number','id'=>'contact_number']) !!}
                                </div>
                            </div>
                            <span class="error_msgg" style="display:none;"></span>
                            <div class="@if ($errors->has('company_contact')) has-error @endif">   
                                @if ($errors->has('company_contact')) <p class="text-danger">{{ $errors->first('company_contact') }}</p> @endif
                            </div>
                        </div>-->
                            <!-- -------- company address ---------- -->
                            <div class="col-md-6">
                                <div class="form_grp pickLocation">
                                    {!! Form::text('company_address',null,['id'=>'location','placeholder'=>'Company Address']) !!} {!! Form::hidden('lat',null,['id'=>'lat']) !!} {!! Form::hidden('lng',null,['id'=>'lng']) !!} {!! Form::hidden('key',null,['id'=>'key']) !!} {!! Form::hidden('register_key',Session::has('session_key')?Session::get('session_key'):0,['id'=>'key']) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('company_address')) has-error @endif">
                                        @if ($errors->has('company_address'))
                                        <p class="text-danger">{{ $errors->first('company_address') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- ----------- industry ----------- -->
                            <div class="col-md-6  industry">
                                <div class="form_grp industrytype">
                                    {!! Form::select('industry_type[]',$industry,null,['class'=>'chosen-select','multiple'=>'multiple','id'=>'multiselect','style'=>'width:350px;','data-placeholder'=>"Type of Industries"]) !!}
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('industry_type')) has-error @endif">
                                        @if ($errors->has('industry_type'))
                                        <p class="text-danger">{{ $errors->first('industry_type') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- ----------- number of workers required --------- -->
                            <!--<div class="col-md-6">
                            <div class="form_grp">
                                 Form::text('number_worker',null,['id'=>'number_worker', 'placeholder'=>'Number of Workers', 'maxlength' => '10']) 
                                <span class="error_msgg" style="display:none;"></span>
                                <div class="if ($errors->has('number_worker')) has-error endif">   
                                    if ($errors->has('number_worker')) 
                                        <p class="text-danger"> $errors->first('number_worker') </p> 
                                    endif
                                </div>
                            </div>
                        </div> -->
                        </div>

                        <div class="row">
                            <!-- --------- terms and conditions -------- -->
                            <div class="col-md-12">
                                <div class="form_grp agree">
                                    {!! Form::checkbox('agree',true,false,['class'=>'customCheckbox','id'=>'signup_check']) !!}
                                    <label for="signup_check">I agree to the <a href="{{ url('terms-condtion') }}" target="_blank">Terms and Conditions</a> and <a href="{{ url('privacy-policy') }}" target="_blank">Privacy Policy</a></label>
                                    <span class="error_msgg" style="display:none;"></span>
                                    <div class="@if ($errors->has('agree')) has-error @endif">
                                        @if ($errors->has('agree'))
                                        <p class="text-danger">{{ $errors->first('agree') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer signup_ftr">
                    <button type="button" id="registrationEmployer">Submit</button>
                </div>
                {!! Form::close(); !!}
            </div>
        </div>
    </div>


    @include('employer.promo.promo_footer')

<script type="text/javascript">
    $(document).on('click', '.more', function(){
      var text = $(this).text();
      var Class = $(this).attr('data-id');
      $(".default_"+Class).toggle();
      $(".text_"+Class).toggle(); 
      $('.text_change_'+Class).text($(this).text() == 'show more' ? 'show less' : 'show more');
    });
</script>


