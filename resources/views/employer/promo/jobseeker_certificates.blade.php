@if( count( $candidate_details['additional_documents'] ) )
	@foreach( $candidate_details['additional_documents'] as $key => $additional_documents )
		<div class="col-md-12 tiles">
			<div class="documents">
				<ul>					
					@if( isset( $additional_documents['url'] ) )
						<li>
							<div class="doc_img">
								<i class="fa fa-briefcase" aria-hidden="true"></i>
							</div> 
							<h6>{{$additional_documents['name']}}</h6>
							
							<?php
								if(isset($additional_documents['description']) && !empty($additional_documents['description'])){
									?>
									{{$additional_documents['description']}}
									<?php
								}
							?>
							
							<a title="download" target="_blank" class="btn" href="{{ $additional_documents['url'] }}">
								<div class="button_download">
								<i class="fa fa-download" aria-hidden="true"></i>	
								</div>
							</a>
						</li>						
					@endif				
				</ul>
			</div>
		</div>
	@endforeach
@else
	No certificate provided yet.
@endif
