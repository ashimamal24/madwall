
@extends('employer.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
<link  href="{{asset('public/employer/css/intlTelInput.css')}}" rel="stylesheet">

@endsection


@section('title')
	Company Profile
@endsection

@section('heading')
	Company Profile
@endsection
	
@section('content')
<div class="white_inner_sec wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
    <div class="job_detail_sec">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
         @include('flash::message')
            <div class="company_profile">
                <div class="company_img">
					<?php
					if(Auth::user()){
						if(!empty($user['image'])){
							$url = $user['image'];
						}else{
							$url = url('public/employer/images/profile_avatar.png')	;
						}
						
					}
					else
						$url = url('public/employer/images/profile_avatar.png')	;
					?>
					
					<img src="{{$url}}">
					
					
					
                    <div class="edit_pic"><a href="#changeLogo" data-toggle="modal" data-target="#changeLogo"><i class="fa fa-pencil" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        
        <a href="#editform" data-toggle="modal" data-target="#editform"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;update profile</a>
        <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
         
            <div class="job_detail_left_sec">
                <h2>{{$user['company_name']}}</h2>
                <span class="job_time">{{$user['email']}}</span>
                <p>@if(isset( $user['company_description'] ) ){{$user['company_description']}}@endif</p>
                <div class="job_time_loc">
                    <div class="location_sec width_100"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        <div class="text_job"><b>{{$user['location']}}</b></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="location_sec"><span><i class="fa fa-mobile" aria-hidden="true"></i></span>
                        <div class="text_job"><b>{{$user['company_code']}}_{{$user['company_contact']}}</b></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="location_sec">
                        <div class="text_job">Number of persons: <b>{{$user['number_worker']}}</b></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="location_sec">
                        <div class="text_job">Industries - </div>
                        @foreach($user['industry'] as $industry )
                            <div class="jobtag">{{$industry['name']}}</div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="flash-message">
           
        </div>
    </div>

    <div class="job_detail_table">
        <!-- <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <div class="number_list">Showing 1 - 6 of 50 jobs</div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="jobdetailsearch">
                    <div class="search_input">
                        <input placeholder="Search by Job name" type="text"><span><i class="fa fa-search" aria-hidden="true"></i></span></div>
                </div>
            </div>
        </div> -->

        <div class="table_job_detail">
            <div class="table_dashboard">
                <div class="company_info_table">
                    <div class="heading_row headingoftb">
                        <div class="width33company_info brdr_btm">Industry</div>
                        <div class="width33company_info brdr_btm">Job Category</div>
                        <div class="width33company_info brdr_btm">Comission</div>
                    </div>
                    @foreach( $industries as $industry )
                        <div class="heading_row company-job-catrgory">
                        <div class="width33company_info company-job-catrgory-name same-height bg_grey same-height-left" style="height: 105px;"><span>{{ $industry['industry_name']}}</span></div>
                        <div class="width66company_info same-height same-height-right" style="height: 105px;">
                            <div class="sub_data">
                                <div class="width50company_info ">
                                    {{ $industry['name']}}
                                </div>
                                <div class="width50company_info">
                                    {{ $industry['commision']}}%
                                </div>
                            </div>  
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="buttons">
            <button data-toggle="modal" data-target="#forgot">Change Email</button>
            <button data-toggle="modal" data-target="#change_pass">Change Password</button>
        </div>
    </div>
</div>

<!-- ------------------ Change password popup ------------------- -->
<div class="modal fade signup forgot" id="change_pass" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title">Change Password</h4>
            </div>
            
			@include('errors.user_error')
			
			<!-- -------- display error messages here -------- -->
				<div class="alert" id="password_messages" style="display:none"></div>
			<!-- --------------------------------------------- -->
			
			{{ Form::model( $user =new App\Model\User, array( 'url' => url('employer/change-password'), 'id'=> 'employer-change-password' )) }}
            
            <div class="modal-body">
                <div class="form_signup">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form_grp old_password">
				            	{{ Form::password( 'old_password', array( 'id' => 'old_password', 'placeholder' => 'Enter Old Password' ) )}}
                                <label class="help-block"></label>
                                <span class="error_msgg" style="display:none;"></span>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form_grp password">
				                {{ Form::password( 'password', array( 'id' =>'password', 'placeholder' => 'Enter New Password' ) )}}
                                <span class="error_msgg" style="display:none;"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form_grp password_confirmation">
					        	{{ Form::password( 'password_confirmation', array( 'id' => 'password_confirmation', 'placeholder' => 'Retype New Password' ) )}}
                                <span class="error_msgg" style="display:none;"></span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
		      	{{ Form::submit('Submit', [ 'id'=>'submit-button','class'=>'submit-button' ]) }}
		      	<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<!-- -------------------- Change email popup ------------------- -->
<div class="modal fade signup forgot" id="forgot" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title">Change Email</h4>
            </div>
            
			<!-- -------- display error messages here -------- -->
				<div class="alert" id="email_messages" style="display:none"></div>
			<!-- --------------------------------------------- -->
			
			{{ Form::model( $user =new App\Model\User, array( 'url' => url('employer/change-email'), 'id'=> 'employer-change-email' )) }}
            
            <div class="modal-body">
                <div class="form_signup">
                    <div class="row">
                        <div class="col-md-12">
							<!-- ------------- old email ----------- -->
                            {{Auth::user()->email}}
                        </div>
                        <div class="col-md-12">
                            <div class="form_grp new_email">
								{!! Form::text('new_email',null,['maxlength'=>'100','id'=>'new_email','placeholder'=>'Enter New Email']) !!}
				                <label class="help-block"></label>
                                <span class="error_msgg" style="display:none;"></span>
                            </div>
                            

                        </div>
                        
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
		      	{{ Form::submit('Submit', [ 'id'=>'submit-email-button','class'=>'submit-button' ]) }}
		      	<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- --------------- end change email popup here ---------------- -->




<!-- --------------- edit company detail popup ------------------ -->

<div class="modal fade signup" id="editform" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>-->
				<h4 class="modal-title" id="myModalLabel">Update Profile</h4>
			</div>
			<div class="modal-body">
				<div id="msg"></div>
				{{ Form::model( $user =Auth::user(), array( 'url'=>'/employer/update-company-profile', 'id'=> 'update-company-profile' )) }}
    
				<div class="form_signup">
					
     
					<div class="row">
						
						<div class="col-md-6">
							<!-- ------------ company name ----------- -->
							<div class="form_grp">
								{!! Form::text('company_name',null,['placeholder' => 'Company Name']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('company_name')) has-error @endif">   
								  @if ($errors->has('company_name')) <p class="text-danger">{{ $errors->first('company_name') }}</p> @endif
								</div>
							</div>
						</div>
						
						<div class="col-md-6 phonenumber">
							<!-- ------------ country code ----------- -->
							<div class="country_codes">
								<div class="form_grp number_add">
									{!! Form::text('country_code',null,['placeholder' => 'Country Code','id'=>'phone','onkeydown'=>'return false']) !!}
								</div> 
								<!-- ------------ company phone number ----------- -->
								<div class="form_grp number_cus">
									{!! Form::text('phone',null,['placeholder' => 'Company Phone Number','id'=>'phone_number']) !!}
								</div>
							</div>
							<span class="error_msgg" style="display:none;"></span>
							<div class="@if ($errors->has('phone')) has-error @endif">   
								@if ($errors->has('phone')) <p class="text-danger">{{ $errors->first('phone') }}</p> @endif
							</div>
						</div>
					</div>
				</div>
				<div class="form_signup">
					<div class="row">
						<div class="col-md-6  industry">
							
							<!-- ------------ Industry type ----------- -->
							<div class="form_grp industrytype">
								{!! Form::select('industry_type[]',$indstry,$selected_indstry,['class'=>'chosen-select','multiple'=>'multiple','id'=>'multiselect','style'=>'width:350px;','data-placeholder'=>"Type of Industry"]) !!}
								
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('industry_type')) has-error @endif">   
									@if ($errors->has('industry_type')) <p class="text-danger">{{ $errors->first('industry_type') }}</p> @endif
								</div>
							</div>
							
							<!-- <span class="cat_error" style="display:none;"></span> -->
						</div>
						<div class="col-md-6">
							<div class="form_grp">
								<!-- ------------ number of workers ----------- -->
								{!! Form::text('number_worker',null,['id'=>'number_worker','placeholder'=>'Number of workers']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('number_worker')) has-error @endif">   
									@if ($errors->has('number_worker')) <p class="text-danger">{{ $errors->first('number_worker') }}</p> @endif
								</div>
							</div>
						</div>
					</div>
					<div class="row">       
						<div class="col-md-6">
							<!-- ------------ first name ----------- -->
							<div class="form_grp">
								{!! Form::text('first_name',null,['placeholder' => 'Contact First Name']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('first_name')) has-error @endif">   
									@if ($errors->has('first_name')) <p class="text-danger">{{ $errors->first('first_name') }}</p> @endif
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- ------------ last name ----------- -->
							<div class="form_grp">
								{!! Form::text('last_name',null,['placeholder' => 'Contact Last Name']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('last_name')) has-error @endif">   
									@if ($errors->has('last_name')) <p class="text-danger">{{ $errors->first('last_name') }}</p> @endif
								</div>
							</div>
						</div>       
					</div>
					<div class="row">
						<div class="col-md-6 companycontact">
							<div class="country_codes">
								<!-- ------------ company code ----------- -->
								<div class="form_grp number_add">
									{!! Form::text('company_code',null,['placeholder' => 'Contry code','id'=>'company_code','onkeydown'=>'return false']) !!}
									<!-- <span class="error_msgg" style="display:none;"></span> -->
								</div>
								<!-- ------------ company contact ----------- -->
								<div class="form_grp number_cus">
									{!! Form::text('company_contact',null,['placeholder' => 'Contact number','id'=>'contact_number']) !!}
								</div>
							</div>
							<span class="error_msgg" style="display:none;"></span>
							<div class="@if ($errors->has('company_contact')) has-error @endif">   
								@if ($errors->has('company_contact')) <p class="text-danger">{{ $errors->first('company_contact') }}</p> @endif
							</div>
						</div>
						<div class="col-md-6">
							<!-- ------------ company address ----------- -->
							<div class="form_grp pickLocation">
							   {!! Form::text('company_address',null,['id'=>'location','placeholder'=>'Location']) !!}
							   {!! Form::hidden('lat',null,['id'=>'lat']) !!}
							   {!! Form::hidden('lng',null,['id'=>'lng']) !!}
							   @if(!empty(Auth::user()->company_address))
								{!! Form::hidden('key',1,['id'=>'key']) !!}
							   @else
								{!! Form::hidden('key',null,['id'=>'key']) !!}
							   @endif
							   
							   {!! Form::hidden('register_key',Session::has('session_key')?Session::get('session_key'):0,['id'=>'key']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('company_address')) has-error @endif">   
									@if ($errors->has('company_address')) <p class="text-danger">{{ $errors->first('company_address') }}</p> @endif
								</div>
							</div>
						</div>       
					</div>
					<div class="row">
						<div class="col-md-6">
							<!-- ------------ company description ----------- -->
							<div class="form_grp description">
								{!! Form::textarea('company_description',null,['placeholder'=>'Enter Description']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('email')) has-error @endif">   
									@if ($errors->has('company_description')) <p class="text-danger">{{ $errors->first('company_description') }}</p> @endif
								</div>
							</div>
						</div>
					</div>
				</div>      
			</div>
			<!-- -------------- submit form -------------- -->
			<div class="modal-footer">
				<button type="submit" class="submit-button" id="updateEmployer">Submit</button>
				<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>       
		{!! Form::close() !!} 
    </div>
  </div>
</div>


<!-- ------------ end edit company detail popup ------------- -->

<!-- -------------- change company logo modal popup ----------------- -->
	
	
<div class="modal fade signup changeLogoModal" id="changeLogo" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">Change Email</h4>
            </div>
            
			<!-- -------- display error messages here -------- -->
				<div class="alert" id="logo_messages" style="display:none"></div>
			<!-- --------------------------------------------- -->
			
			{{ Form::model( $user =new App\Model\User, array( 'url' => url('employer/change-logo'), 'id'=> 'employer-change-logo' )) }}
            
            <div class="modal-body">
                <div class="form_signup">
                    <div class="row">
                        <div class="col-md-12">
							
                        </div>
                        <div class="col-md-12">
                            <div class="form_grp">
								<div class="img_browse_btn">
									Company logo
									{!! Form::file('user_image',array('id'=>'user_image','placeholder'=>'Logo')) !!}
									{!! Form::hidden('image',null,array('id'=>'image')) !!}
								 </div>
								 <br>
								 <div id="user_image_display"><img height="100" width="100" src="{{Auth::user()->image}}"></div>

								<span class="error_msgg" style="display:none;"></span>
								
								<div class="@if ($errors->has('image')) has-error @endif">   
									@if ($errors->has('image')) <p class="text-danger">{{ $errors->first('image') }}</p> @endif
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
		      	{{ Form::submit('Submit', [ 'id'=>'submit-logo-button','class'=>'submit-button' ]) }}
		      	<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


@endsection
@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/employer/js/additional-methods.min.js') }}"></script>
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<script src="{{ asset('public/admin/js/chosen.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.maskedinput.js')}}" type="text/javascript"></script>    
<script src="{{ asset('public/employer/js/contactus.js')}}" type="text/javascript"></script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0Ucux0_UPErLjpzmwKqvnaD7yot5J08&amp;libraries=places"></script>
<script src="{{asset('public/employer/js/jquery.geocomplete.js')}}" type="text/javascript"></script>  


<script src="{{asset('public/employer/js/intlTelInput.js')}}" type="text/javascript"></script>


<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>

<script>
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);

</script>






<script type="text/javascript">
	
	
	$(document).ready(function(){
		$("#contact_number").mask("(999)-999-9999");
		$("#phone_number").mask("(999)-999-9999");
		
		$("#phone").intlTelInput({
		  // allowDropdown: false,
		   autoHideDialCode: false,
		 // autoPlaceholder: "off",
		  // dropdownContainer: "body",
		  // excludeCountries: ["us"],
		  // formatOnDisplay: false,
		  // geoIpLookup: function(callback) {
		  //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
		  //     var countryCode = (resp && resp.country) ? resp.country : "";
		  //     callback(countryCode);
		  //   });
		  // },
		  // initialCountry: "auto",
		   nationalMode: false,
		   initialCountry: "ca",
		  // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
		  // placeholderNumberType: "MOBILE",
		  // preferredCountries: ['cn', 'jp'],
		  // separateDialCode: true,
		  utilsScript: "public/employer/js/utils.js"
		});

		$("#company_code").intlTelInput({
		   autoHideDialCode: false,
		   nationalMode: false,
		  utilsScript: "public/employer/js/utils.js"
		});
		
		
			$("#location").geocomplete({
				details: ".pickLocation",
				types: ["geocode", "establishment"],
			}).bind("geocode:result", function(event, result){
				var coor = result.geometry.location.lat();
				$('#key').val(1);
				$('.pickLocation').find('span.error_msgg').hide();  
			}).bind("blur", function(event, results){
				setTimeout(function(){
					if($('#key').val() == '')
					{
						$('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid location.');
						$('#lat').val('');
						$('#lng').val('');
					}
					//$('#key').val('');
				},1000);				
			});
	});
	
	
	
	
    $( "#employer-change-password" ).validate({
        rules: {
            "old_password":{
                required:true, 
            },
            "password":{
                required:true, 
            },
            "password_confirmation":{
                equalTo:"#password", 
            },
        },
        messages: {
            "old_password":{
              required:"Please enter old password."
            },
            "password":{
              required:"Please enter new password."
            },
            "password_confirmation":{
              equalTo:"Password do not match."
            },
        },
        errorElement:'span',
        errorClass:'error_msg',
        submitHandler: function(form) {
            $("#employer-change-password").ajaxSubmit({
                method      : 'post',
                beforeSend  : function() {
                    Loader();
                    $('#password_messages').removeClass('alert-danger').hide();
                    $('#password_messages').removeClass('alert-success').hide();
                },
                url : path+'employer/change-password',
                success     : function(data) {
					RemoveLoader();
					if(data.success == true){
						$('#password_messages').addClass('alert-success').html(data.message).fadeIn('slow').delay(3000).fadeOut();
						//redirect to logout page, so we can re-authenticate user with new password.
						setTimeout(function(){ window.location = path+'employer/logout'; }, 3000);
					}else{
						$('#password_messages').addClass('alert-danger').html(data.message).fadeIn('slow').delay(3000).fadeOut();
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    RemoveLoader();
                    $( "#employer-change-password .form-grp" ).removeClass( "has-error" );
                    $( ".help-block" ).hide();
                    $.each( xhr.responseJSON, function( i, obj ) {
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        
                        if( i=='old_password' ){
                            $('.old_password').find('span.error_msgg').slideDown(400).html(obj);
                        }

                        if( i=='password' ){
                            $('.password').find('span.error_msgg').slideDown(400).html(obj);
                        }

                        if( i=='password_confirmation' ){
                            $('.password_confirmation').find('span.error_msgg').slideDown(400).html(obj);                           
                        }
                        
                        
                        
                    });
                }
            });
        }
    });
    
    
    /*
    DESC : when  user email is updated
    */
    $( "#employer-change-email" ).validate({
        rules: {
            "new_email":{
                required:true,
            }
        },
        messages: {
            "new_email":{
              required:"Please enter new email.",
            }
        },
        errorElement:'span',
        errorClass:'error_msg',
        submitHandler: function(form) {
            $("#employer-change-email").ajaxSubmit({
                method      : 'post',
                beforeSend  : function() {
                    Loader();
                    $('#email_messages').removeClass('alert-danger').hide();
                    $('#email_messages').removeClass('alert-success').hide();
                },
                url : path+'employer/change-email',
                success     : function(data) {
					RemoveLoader();
					if(data.success == true){
						$('#email_messages').addClass('alert-success').html(data.message).fadeIn('slow').delay(3000).fadeOut();
						//redirect to logout page, so we can re-authenticate user with new password.
						setTimeout(function(){ window.location = path+'employer/logout'; }, 3000);
					}else{
						$('#email_messages').addClass('alert-danger').html(data.message).fadeIn('slow').delay(3000).fadeOut();
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    RemoveLoader();
                    $( "#employer-change-email .form-grp" ).removeClass( "has-error" );
                    $( ".help-block" ).hide();
                    $.each( xhr.responseJSON, function( i, obj ) {
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        if( i=='new_email' ){
                            $('.new_email').find('span.error_msgg').slideDown(400).html(obj);
                        }
                    });
                }
            });
        }
    });
    /*
    DESC : when  user email is updated
    */
    $( "#employer-change-logo" ).validate({
        rules: {
            "user_image":{
                required:true,
            }
        },
        messages: {
            "user_image":{
              required:"Please select an image.",
            }
        },
        errorElement:'span',
        errorClass:'error_msg',
        submitHandler: function(form) {
            $("#employer-change-logo").ajaxSubmit({
                method      : 'post',
                beforeSend  : function() {
                    Loader();
                    $('#logo_messages').removeClass('alert-danger').hide();
                    $('#logo_messages').removeClass('alert-success').hide();
                },
                url : path+'employer/change-logo',
                success     : function(data) {
					RemoveLoader();
					if(data.success == true){
						$('#logo_messages').addClass('alert-success').html(data.message).fadeIn('slow').delay(3000).fadeOut();
						//redirect to logout page, so we can re-authenticate user with new password.
						setTimeout(function(){ window.location = path+'employer/company-profile'; }, 3000);
					}else{
						$('#logo_messages').addClass('alert-danger').html(data.message).fadeIn('slow').delay(3000).fadeOut();
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    RemoveLoader();
                    $( "#employer-change-logo .form-grp" ).removeClass( "has-error" );
                    $( ".help-block" ).hide();
                    $.each( xhr.responseJSON, function( i, obj ) {
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        if( i=='image' ){
                            $('.image').find('span.error_msgg').slideDown(400).html(obj);
                        }
                    });
                }
            });
        }
    });
    
    /*
    DESC : when  user email is updated
    */
    $( "#update-company-profile" ).validate({
        rules: {
           "company_name": {
              required:true,
            },
            "first_name": {
              required:true,
            },
            "last_name": {
              required:true,
            },
            "country_code": {
              required:true,
            },
            "company_code": {
              required:true,
            },
            "password": {
              required:true,
            },
            "password_confirmation": {
              required:true,
            },
            "company_address": {
              required:true,
            },
            "number_worker": {
              required:true,
            },
            "user_image": {
              //required:true,
            },
            "company_description":{
              required:true,
            },
            "agree":{
              required:true,
            }
        },
        messages: {
            "company_name": {
                required: "Please enter Company Name",
            },
            "first_name": {
                required: "Please enter Contact First Name",
            },
            "last_name": {
                required: "Please enter Contact Last Name ",
            },
            "email": {
                required: "Please enter your email ",
            },
            "password": {
                required: "Please enter password ",
            },
            "password_confirmation": {
                required: "Please enter confirm password ",
            },
            "company_address": {
                required: "Please enter location ",
            },
            "number_worker": {
                required: "Please select at least one value in number of workers’ field. ",
            },
            "user_image":{
              required: "Please select images.",
            },
            "company_description":{
              required:"Please enter company description",
            },
            "country_code":{
              required:"Please enter country code",
            },
            "company_code":{
              required:"Please enter country code",
            },
        },
        errorElement:'span',
        errorClass:'error_msg',
        errorPlacement: function(error, element) {
           // alert(element.attr("name"));
            $(element).parent('.form_grp').find('span.error_msgg').slideDown(400).html(error);
            if(element.attr("name") == "user_image"){
              $(element).closest('.form_grp').parent('.form-field').find('span.error_msgg').slideDown(400).html(error);
            }

            if(element.attr("name") == "agree"){
                $(element).closest('span').parent('.agree').find('span.error_msgg').slideDown(400).html(error);
            }
        },
        submitHandler: function(form,e) {
          var form_data = new FormData($('form#update-company-profile')[0]);
          $.ajax({
            url: path+'employer/update-company-profile',
            type: 'post',
            dataType: 'json',
            data: form_data,
            contentType: false,
            processData: false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                Loader();
                //$('#updateEmployer').prop( "disabled", true );
            },
            success: function(data) {
				if(data.success==true)
				{
					$('#editform').hide();
					$('form#update-company-profile')[0].reset();
					window.location.reload();                  
				}else{
					//$('#updateEmployer').prop( "disabled", false );
					$('#msg').html('<div class="alert alert-danger"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</strong>.</div>');
				}
            },
            error: function(error) { 
				RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
             // $('#updateEmployer').prop( "disabled", false );
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
                    if(i == 'industry_type')
                      $('.industrytype').find('span.error_msgg').slideDown(400).html(obj);

                    if(i=='company_description')
                      $('.description').find('span.error_msgg').slideDown(400).html(obj);

                      if(i == "phone")
                         $('.phonenumber').find('span.error_msgg').slideDown(400).html(obj);
                      

                      if(i == "company_contact")
                        $('.companycontact').find('span.error_msgg').slideDown(400).html(obj);
                          
                      if(i == 'token_mismatch')
                        window.location.reload();

                 }) 
              }

            },


            complete: function() { RemoveLoader(); }
          }); 
        }
    });
    
    
    
    $(document).on('change','#user_image',function(e){

        var fileExtension = ['pdf','png','jpg'];
        var file = $(this)[0].files;
        var type = file[0].type;
       
        // Check File Size
        if( file[0].size >5242880 ) {
            bootbox.alert("Please upload a valid image within a max range of 5 MB");
            return false;
        }

        // Check File Type
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            return false;
        }
        Loader();
        $('#image').val(file[0].name);
        firebase_multiple_upload( file, type, $(this) );
    });
    function firebase_multiple_upload( file,type ,reference ){
        var timestamp=Date.now();
        var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
        var metadata = {
            contentType: type,
        };
        var blob_image=new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
           $('#image').val(snapshot.downloadURL);
           //display new image,
           $("#user_image_display").find('img').removeAttr('src').attr('src',snapshot.downloadURL);
            RemoveLoader();
        }).catch(function(error) {
            alert('firebase error occured:'+error);
            RemoveLoader();
        });
        RemoveLoader();
    }
    
</script>

@endsection
