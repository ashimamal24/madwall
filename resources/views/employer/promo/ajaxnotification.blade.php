<a id="clickNotification" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	<i class="fa fa-bell-o" aria-hidden="true"></i>
</a>
	@if($not_count)
	<span>
		{{ $not_count }}
	</span>
	@endif
@if(count($notification))
		<ul class="dropdown-menu" aria-labelledby="dLabel">
		@foreach($notification AS $k=> $notify)
			@php $notifyId = Crypt::encrypt($notify->_id) @endphp
			<li class="visit_nofity" data-value="{{$k}}">{{$notify->title}}
				{!! Form::hidden('notity',$notify->_id,['class'=>"notify-$k"]) !!}
				<div class="time_noti">{{ date('F d, Y h:i',strtotime($notify->created_at)) }}</div>
			</li>
		@endforeach
		</ul>
@else
	<ul class="dropdown-menu" aria-labelledby="dLabel">
		<li>No Notifications</li>
	</ul>
@endif
