@section('css')
<link rel="stylesheet" href="{{ asset('public/employer/css/datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">
<link rel="stylesheet" href="{{ asset('public/employer/css/timepicker.css') }}">
<link  href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
<style>
#submit-button {
    background: #079835 none repeat scroll 0 0;
    border: medium none;
    border-radius: 5px;
    color: #fff;
    float: left;
    font-size: 16px;
    font-weight: 500;
    margin-left: 20px;
    padding: 10px 30px;
    text-align: center;
}
</style>
@endsection
@extends('employer.layout')

@section('title')
	Change Password
@endsection

@section('heading')
	Change Password 
@endsection

@section('content')

	<div class="col-md-offset-3 col-md-6">
	    <div class="reset_pswrd_sec">
	    	<div class="form_inner_sec_reset">
			   <!--  <div class="col-md-6"> -->
			        <!-- <div class="box box-primary"> -->
			            <div class="flash-message">
			                @include('flash::message')
			            </div>
			            @include('errors.user_error')
			           
			            {{ Form::model( $user =new App\Model\User, array( 'url' => url('employer/change-password') )) }}
			                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			                <div class="col-md-12">
			                	<div class="field_forms">
				                    <div class="label_form">
				                    	{{ Form::label( 'old_password', 'Old Password: *' ) }}
				                    </div>
				                    <div class="form_inputs">
				                    	{{ Form::password( 'old_password', array( 'class' =>'form-control', 'placeholder' => 'Enter  Old Password' ) )}}
				                    </div>
				                </div>
			                </div>
			                <div class="col-md-12">
			                	<div class="field_forms">
			                		<div class="label_form">
				                    	{{ Form::label( 'password', 'Password *' ) }}
									</div>
									<div class="form_inputs">		                        
				                        {{ Form::password( 'password', array( 'class' =>'form-control', 'placeholder' => 'Enter New Password' ) )}}
				                    </div>
			                	</div>
			                </div>
			                <div class="col-md-12">
			                	<div class="field_forms">
				                    <div class="label_form">
				                    	{{ Form::label( 'password_confirmation', 'Confirm Password *' ) }}
				                	</div>
					                <div class="form_inputs">
					           			{{ Form::password( 'password_confirmation', array( 'class' =>'form-control', 'placeholder' => 'Retype New Password' ) )}}
					                </div>
			                	</div>
			                </div>
			                <div class="date_time_div_post_job">
			                    <div class="buttons">
			                   	{{ Form::submit('Submit', [ 'id'=>'submit-button' ]) }}
			                    </div>
								<!-- <a class="btn default" href="{{ url( 'admin/dashboard' ) }}">Cancel</a> -->
			               	</div>
			           {{ Form::close() }}
			       <!--  </div> -->
			    <!-- </div> -->
		    </div>
		</div>
	</div>
@endsection