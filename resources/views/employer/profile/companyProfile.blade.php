
@extends('employer.layout')

@section('css')
<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">
<link  href="{{ asset('public/employer/css/main.css') }}" rel="stylesheet">
<link  href="{{asset('public/employer/css/intlTelInput.css')}}" rel="stylesheet">
<link  href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
<style>



.chosen-container-multi .chosen-choices {
    background-color: #fff;
    background-image: linear-gradient(#eeeeee 1%, #ffffff 15%);
    border: 1px solid #aaa;
    cursor: text;
    height: 53px;
    margin: 0;
    overflow: hidden;
    padding: 0 5px;
    position: relative;
    width: 100%;
}

span.description_time {
    color: #0b4cc4;
    float: left;
    font-size: 14px;
    font-weight: 500;
    width: 3%;
}


button.change_profile_btn {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #0b4cc4;
    color: #0b4cc4;
}




</style>

@endsection


@section('title')
	Company Profile
@endsection

@section('heading')
	Company Profile
@endsection
	
@section('content')
<div class="white_inner_sec wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
    <div class="job_detail_sec">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
         @include('flash::message')
            <div class="company_profile">
                <div class="company_img">
					<?php
					if(Auth::user()){
						if(!empty($user['image'])){
							$url = $user['image'];
						}else{
							$url = url('public/employer/images/profile_avatar.png')	;
						}
					}
					else
						$url = url('public/employer/images/profile_avatar.png')	;
					?>
					
					<img src="{{$url}}">
					
                    <div class="edit_pic"><a href="#changeLogo" data-toggle="modal" data-target="#changeLogo"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="job_detail_left_sec">
				<div class="row">
				
				<div class="cmpny_txtcntr">
				<div class="col-lg-7 col-md-7 col-sm-7 ">
                <h2>{{$user['company_name']}}</h2>
                <span class="job_time"><i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp;{{$user['email']}}</span>
               
               
            <span class="job_time">
					@if(isset( $user['company_description'] ) ) 
						<i class="fa fa-file-text-o" aria-hidden="true"></i>
						 {{$user['company_description']}} 
					@endif
			</span>
				</div>
                </div>
				<div class="col-lg-5 col-md-5 col-sm-5 "><div class="buttons">
			<button class="change_profile_btn" data-toggle="modal" data-target="#editform"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Update Profile</button>
		</div></div>
				</div>
				<div class="clearfix"></div>
                <div class="job_time_loc">
					<div class="location_sec width_100"><span><i class="fa fa-user" aria-hidden="true"></i></span>
                        <div class="text_job">  <b>{{$user['first_name']}} {{$user['last_name']}}</b></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="location_sec width_100"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        <div class="text_job"><b>{{$user['location']}}</b></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="location_sec width_100"><span><i class="fa fa-mobile" aria-hidden="true"></i></span>
                        <div class="text_job">
							
							<b>
								
								<?php
									/******* 17 aug 2017, shivani - to display employer contact number ******/
									
									$phone = 'N/A';

									if( isset($user['company_code'] ) && !empty($user['company_contact']) && $user['company_contact'] != null ) {
										$phone =$user['company_code'] ." ". $user['company_contact'];
									}
									if( isset($user['country_code'] ) &&  !empty($user['phone'])) {
										$phone = $user['country_code'] ." ".$user['phone'];
									}
								?>
								
								
								
								{{ $phone }}
								
							
							
							</b></div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <!-- <div class="location_sec width_100"><span><i class="fa fa-group" aria-hidden="true"></i></span>
                        <div class="text_job">Number of Workers : <b>
							if(!empty($user['number_worker']))
							$user['number_worker']
							else
							N/A
							endif
							
							</b></div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="location_sec width_100"><span><i class="fa fa-industry" aria-hidden="true"></i></span>
                        <div class="text_job">Industries : </div>
                        @foreach($user['industry'] as $industry )
                            <div class="jobtag">{{$industry['name']}}</div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="flash-message">
           
        </div>
    </div>

    <div class="job_detail_table">
        <!-- <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <div class="number_list">Showing 1 - 6 of 50 jobs</div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="jobdetailsearch">
                    <div class="search_input">
                        <input placeholder="Search by Job name" type="text"><span><i class="fa fa-search" aria-hidden="true"></i></span></div>
                </div>
            </div>
        </div> -->

        <div class="table_job_detail">
            <div class="table_dashboard">
                <div class="company_info_table">
                    <div class="heading_row headingoftb">
                        <div class="width33company_info brdr_btm">Industry</div>
                        <div class="width33company_info brdr_btm">Job Category</div>
                        <div class="width33company_info brdr_btm">Commission</div>
                    </div>
                  
                    <?php $ind_group=[]; ?>
                    @foreach( $industries as $industry )
                        <div class="heading_row company-job-catrgory">
							<div class="width33company_info company-job-catrgory-name same-height bg_grey same-height-left" style="height: 105px;">
								@if(!in_array($industry['industry_name'],$ind_group))
									<span>{{ $industry['industry_name']}}</span>
								@else
									<span></span>
								@endif
							</div>
							
							<div class="width66company_info same-height same-height-right" style="height: 105px;border-bottom:none;">
								<div class="sub_data">
									<div class="width50company_info ">
										{{ $industry['name']}}
									</div>
									<div class="width50company_info">
										<?php
											$commision = $industry['commision'];
										?>
										@if(isset($user['category_details']) && !empty($user['category_details']))
											@foreach($user['category_details'] as $catDtl)
												@if($catDtl['_id'] == $industry['_id'])
													@if(!empty($catDtl['extra']))
														<?php $commision += $catDtl['extra']; ?>
													@elseif(!empty($catDtl['discount']))
													<?php $commision -= $catDtl['discount']; ?>
													@else
													<?php $commision += 0; ?>
													@endif
												@endif
											@endforeach
										@endif
										
										{{ $commision }}%
									</div>
								</div>  
							</div>
						</div>
						<?php $ind_group[] = $industry['industry_name']; ?>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="buttons">
            <button class="change_email_btn" data-toggle="modal" data-target="#forgot">Change Email</button>
            <button data-toggle="modal" data-target="#change_pass">Change Password</button>
        </div>
    </div>
</div>

<!-- ------------------ Change password popup ------------------- -->
<div class="modal fade signup forgot" id="change_pass" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:500px">
            <div class="modal-header">
               <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title">Change Password</h4>
            </div>
            
			@include('errors.user_error')
			
			<!-- -------- display error messages here -------- -->
				<div class="alert" id="password_messages" style="display:none"></div>
			<!-- --------------------------------------------- -->
			
			{{ Form::model( $user =new App\Model\User, array( 'url' => url('employer/change-password'), 'id'=> 'employer-change-password' )) }}
            
            <div class="modal-body">
                <div class="form_signup">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form_grp old_password">
				            	{{ Form::password( 'old_password', array( 'id' => 'old_password', 'placeholder' => 'Enter Old Password' ) )}}
                                <label class="help-block"></label>
                                <span class="error_msgg" style="display:none;"></span>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form_grp password">
				                {{ Form::password( 'password', array( 'id' =>'password', 'placeholder' => 'Enter New Password' ) )}}
                                <span class="error_msgg" style="display:none;"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form_grp password_confirmation">
					        	{{ Form::password( 'password_confirmation', array( 'id' => 'password_confirmation', 'placeholder' => 'Retype New Password' ) )}}
                                <span class="error_msgg" style="display:none;"></span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
		      	{{ Form::submit('Submit', [ 'id'=>'submit-button','class'=>'submit-button' ]) }}
		      	<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<!-- -------------------- Change email popup ------------------- -->
<div class="modal fade signup forgot" id="forgot" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title">Change Email</h4>
            </div>
            
			<!-- -------- display error messages here -------- -->
				<div class="alert" id="email_messages" style="display:none"></div>
			<!-- --------------------------------------------- -->
			
			{{ Form::model( $user =new App\Model\User, array( 'url' => url('employer/change-email'), 'id'=> 'employer-change-email' )) }}
            
            <div class="modal-body">
                <div class="form_signup">
                    <div class="row">
                        <div class="col-md-12">
							<div class="label_form">
								<label>Old Email</label>
							</div>
							<!-- ------------- old email ----------- -->
							<div class="form_grp">								
								<input type="text" name="older_email" value="{{Auth::user()->email}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
							<div class="label_form">
								<label>New Email</label>
							</div>
                            <div class="form_grp new_email">
								{!! Form::text('new_email',null,['maxlength'=>'100','id'=>'new_email','placeholder'=>'Enter New Email']) !!}
				                <label class="help-block"></label>
                                <span class="error_msgg" style="display:none;"></span>
                            </div>
                            

                        </div>
                        
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
		      	{{ Form::submit('Submit', [ 'id'=>'submit-email-button','class'=>'submit-button' ]) }}
		      	<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- --------------- end change email popup here ---------------- -->




<!-- --------------- edit company detail popup ------------------ -->

<div class="modal fade signup" id="editform" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>-->
				<h4 class="modal-title" id="myModalLabel">Update Profile</h4>
			</div>
			<div class="modal-body">
				<div id="msg"></div>
				{{ Form::model( $user =Auth::user(), array( 'url'=>'/employer/update-company-profile', 'id'=> 'update-company-profile' )) }}
    
				<div class="form_signup">
					
					<div class="row">
						
						<div class="col-md-6">
							<!-- ------------ company name ----------- -->
							<div class="label_form">
								<label>Company Name</label>
							</div>
							<div class="form_grp">
								{!! Form::text('company_name',null,['placeholder' => 'Company Name']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('company_name')) has-error @endif">   
								  @if ($errors->has('company_name')) <p class="text-danger">{{ $errors->first('company_name') }}</p> @endif
								</div>
							</div>
						</div>
						
						<div class="col-md-6 phonenumber">
							<div class="label_form">
								<label>Contact Phone Number</label>
							</div>
							<!-- ------------ country code ----------- -->
							<div class="country_codes">
								<div class="form_grp number_add">
									{!! Form::text('country_code',null,['placeholder' => 'Country Code','id'=>'phone','onkeydown'=>'return false']) !!}
								</div> 
								<!-- ------------ company phone number ----------- -->
								<div class="form_grp number_cus">
									{!! Form::text('phone',null,['placeholder' => 'Contact Phone Number','id'=>'phone_number']) !!}
								</div>
							</div>
							<span class="error_msgg" style="display:none;"></span>
							<div class="@if ($errors->has('phone')) has-error @endif">   
								@if ($errors->has('phone')) <p class="text-danger">{{ $errors->first('phone') }}</p> @endif
							</div>
						</div>
					</div>
				</div>
				<div class="form_signup">
					
					<div class="row">       
						<div class="col-md-6">
							<!-- ------------ first name ----------- -->
							<div class="label_form">
								<label>Contact First Name</label>
							</div>
							<div class="form_grp">
								{!! Form::text('first_name',null,['placeholder' => 'Contact First Name']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('first_name')) has-error @endif">   
									@if ($errors->has('first_name')) <p class="text-danger">{{ $errors->first('first_name') }}</p> @endif
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- ------------ last name ----------- -->
							<div class="label_form">
								<label>Contact Last Name</label>
							</div>
							<div class="form_grp">
								{!! Form::text('last_name',null,['placeholder' => 'Contact Last Name']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('last_name')) has-error @endif">   
									@if ($errors->has('last_name')) <p class="text-danger">{{ $errors->first('last_name') }}</p> @endif
								</div>
							</div>
						</div>       
					</div>
					<div class="row">
						<div class="col-md-6  industry">
							<div class="label_form">
								<label>Type of Industries</label>
							</div>
							<!-- ------------ Industry type ----------- -->
							<div class="form_grp industrytype">
								{!! Form::select('industry_type[]',$indstry,$selected_indstry,['class'=>'chosen-select','multiple'=>'multiple','id'=>'multiselect','style'=>'width:350px;','data-placeholder'=>"Type of Industries"]) !!}
								
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('industry_type')) has-error @endif">   
									@if ($errors->has('industry_type')) <p class="text-danger">{{ $errors->first('industry_type') }}</p> @endif
								</div>
							</div>
							
							<!-- <span class="cat_error" style="display:none;"></span> -->
						</div>
						<div class="col-md-6">
							<!-- ------------ company address ----------- -->
							<div class="label_form">
								<label>Company Address</label>
							</div>
							<div class="form_grp pickLocation">
							   {!! Form::text('company_address',null,['id'=>'location','placeholder'=>'Company Address']) !!}
							   {!! Form::hidden('lat',null,['id'=>'lat']) !!}
							   {!! Form::hidden('lng',null,['id'=>'lng']) !!}
							   @if(!empty(Auth::user()->company_address))
								{!! Form::hidden('key',1,['id'=>'key']) !!}
							   @else
								{!! Form::hidden('key',null,['id'=>'key']) !!}
							   @endif
							   
							   {!! Form::hidden('register_key',Session::has('session_key')?Session::get('session_key'):0,['id'=>'key']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('company_address')) has-error @endif">   
									@if ($errors->has('company_address')) <p class="text-danger">{{ $errors->first('company_address') }}</p> @endif
								</div>
							</div>
						</div>       
					</div>
					
					
					<div class="row">
						
						<!-- <div class="col-md-6">
							<div class="label_form">
									<label>Number of Workers</label>
								</div>
							<div class="form_grp">								
								<!-- ------------ number of workers ----------- 
								 Form::text('number_worker',null,['id'=>'number_worker','placeholder'=>'Number of Workers']) 
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('number_worker')) has-error @endif">   
									if ($errors->has('number_worker')) <p class="text-danger"> $errors->first('number_worker') </p> endif
								</div>
							</div>
						</div> -->
						
						<div class="col-md-6">
							<div class="label_form">
								<label>Company Description</label>
							</div>
							<!-- ------------ company description ----------- -->
							<div class="form_grp description">
								{!! Form::textarea('company_description',null,['placeholder'=>'Describe your company']) !!}
								<span class="error_msgg" style="display:none;"></span>
								<div class="@if ($errors->has('email')) has-error @endif">   
									@if ($errors->has('company_description')) <p class="text-danger">{{ $errors->first('company_description') }}</p> @endif
								</div>
							</div>
						</div>
						
					</div>
					
				</div>      
			</div>
			<!-- -------------- submit form -------------- -->
			<div class="modal-footer">
				<button type="submit" class="submit-button" id="updateEmployer">Submit</button>
				<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>       
		{!! Form::close() !!} 
    </div>
  </div>
</div>


<!-- ------------ end edit company detail popup ------------- -->

<!-- -------------- change company logo modal popup ----------------- -->
	
	
<div class="modal fade changeLogoModal" id="changeLogo" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:500px">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">Change Company Logo</h4>
            </div>
            
			<!-- -------- display error messages here -------- -->
				<div class="alert" id="logo_messages" style="display:none"></div>
			<!-- --------------------------------------------- -->
			
			{{ Form::model( $user =new App\Model\User, array( 'url' => url('employer/change-logo'), 'id'=> 'employer-change-logo' )) }}
            
            <div class="modal-body">
                <div class="form_signup">
                    <div class="row">
                        <div class="col-md-12">
							
                        </div>
                        <div class="col-md-12">
                            <div class="form_grp">
								<div class="img_browse_btn">
									Company logo
									{!! Form::file('user_image',array('id'=>'user_image','placeholder'=>'Logo')) !!}
									{!! Form::hidden('image',null,array('id'=>'image')) !!}
								 </div>
								 <br>
								<div id="user_image_display">
									 @php
										$url = '';
										if(Auth::user()){
											if(!empty(Auth::user()->image)){
												$url = Auth::user()->image;
											}else{
												$url = url('public/employer/images/profile_avatar.png')	;
											}
											
										}
										else
											$url = url('public/employer/images/profile_avatar.png')	;
									@endphp
									 
									<img height="100" width="100" src="{{$url}}">
									 
									
									 
								</div>

								<span class="error_msgg" style="display:none;"></span>
								
								<div class="@if ($errors->has('image')) has-error @endif">   
									@if ($errors->has('image')) <p class="text-danger">{{ $errors->first('image') }}</p> @endif
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
		      	
		      	<button class="submit-button" id="submit-logo-button" type="submit">Submit</button>
		      	<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@include('models.error_popup')

@endsection
@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/employer/js/additional-methods.min.js') }}"></script>
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<script src="{{ asset('public/admin/js/chosen.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.maskedinput.js')}}" type="text/javascript"></script>    
<script src="{{ asset('public/employer/js/contactus.js')}}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0Ucux0_UPErLjpzmwKqvnaD7yot5J08&amp;libraries=places"></script>
<script src="{{asset('public/employer/js/jquery.geocomplete.js')}}" type="text/javascript"></script>  


<script src="{{asset('public/employer/js/intlTelInput.js')}}" type="text/javascript"></script>


<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>

<script>
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);

</script>






<script type="text/javascript">
	
   
	$(".chosen-select").chosen({
		placeholder_text_single: "option",
		no_results_text: "Oops, nothing found!"
	});
	
	$(document).ready(function(){
		
		$(document).ready(function(){
			$('#multiselect_chosen').removeAttr('style');
		});
		
		
		
		$("#contact_number").mask("(999)-999-9999");
		$("#phone_number").mask("(999)-999-9999");
		
		$("#phone").intlTelInput({
		  // allowDropdown: false,
		   autoHideDialCode: false,
		 // autoPlaceholder: "off",
		  // dropdownContainer: "body",
		  // excludeCountries: ["us"],
		  // formatOnDisplay: false,
		  // geoIpLookup: function(callback) {
		  //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
		  //     var countryCode = (resp && resp.country) ? resp.country : "";
		  //     callback(countryCode);
		  //   });
		  // },
		   initialCountry: "ca",
		   nationalMode: false,
		   excludeCountries: ["us"],
		  // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
		  // placeholderNumberType: "MOBILE",
		  // preferredCountries: ['cn', 'jp'],
		  // separateDialCode: true,
		  utilsScript: "public/employer/js/utils.js"
		});

		$("#company_code").intlTelInput({
		   autoHideDialCode: false,
		   nationalMode: false,
		   initialCountry: "ca",
		   nationalMode: false,
		  utilsScript: "public/employer/js/utils.js"
		});
		
		
			$("#location").geocomplete({
				details: ".pickLocation",
				types: ["geocode", "establishment"],
			}).bind("geocode:result", function(event, result){
				var coor = result.geometry.location.lat();
				$('#key').val(1);
				$('.pickLocation').find('span.error_msgg').hide();  
			}).bind("blur", function(event, results){
				setTimeout(function(){
					if($('#key').val() == '')
					{
						$('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid company address.');
						$('#lat').val('');
						$('#lng').val('');
					}
					$('#key').val('');
				},1000);				
			});
	});
	
	
	/*
	DESC : to add new rule for password : 1 number, 1 special character, 1 alphabet
	9 aug 2017, shivani
	*/
	$.validator.addMethod("pwcheck",
		function(value, element) {
			return /^((?=(.*[0-9]){1,})(?=(.*[a-z]){0,})(?=(.*[A-Z]){1,})(?=(.*\W+){1,})(?=\S)).{6,25}$/.test(value);
		});
	
    $( "#employer-change-password" ).validate({
        rules: {
            "old_password":{
                required:true, 
            },
            "password":{
                required:true, 
                pwcheck:true,
				minlength: 6,
				maxlength: 25,
            },
            "password_confirmation":{
                equalTo:"#password", 
            },
        },
        messages: {
            "old_password":{
              required:"Please enter old password.",
            },
            "password":{
              required:"Please enter new password.",
              pwcheck : "Password must contain at least one uppercase letter, one lowercase letter, a number, a special character, and must be at least six characters long.",
            },
            "password_confirmation":{
              equalTo:"Passwords do not match.",
            },
        },
        errorElement:'span',
        errorClass:'error_msg',
        submitHandler: function(form) {
            $("#employer-change-password").ajaxSubmit({
                method      : 'post',
                beforeSend  : function() {
                    Loader();
                    $('#password_messages').removeClass('alert-danger').hide();
                    $('#password_messages').removeClass('alert-success').hide();
                },
                url : path+'employer/change-password',
                success     : function(data) {
					RemoveLoader();
					if(data.success == true){
						$('#password_messages').addClass('alert-success').html(data.message).fadeIn('slow').delay(3000).fadeOut();
						//redirect to logout page, so we can re-authenticate user with new password.
						setTimeout(function(){ window.location = path+'employer/logout'; }, 3000);
						//setTimeout(function(){ window.location.reload(); }, 3000);
					}else{
						$('#password_messages').addClass('alert-danger').html(data.message).fadeIn('slow').delay(3000).fadeOut();
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    RemoveLoader();
                    $( "#employer-change-password .form-grp" ).removeClass( "has-error" );
                    $( ".help-block" ).hide();
                    $.each( xhr.responseJSON, function( i, obj ) {
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        
                        if( i=='old_password' ){
                            $('.old_password').find('span.error_msgg').slideDown(400).html(obj);
                        }

                        if( i=='password' ){
                            $('.password').find('span.error_msgg').slideDown(400).html(obj);
                        }

                        if( i=='password_confirmation' ){
                            $('.password_confirmation').find('span.error_msgg').slideDown(400).html(obj);                           
                        }
                        
                    });
                }
            });
        }
    });
    
    
    /*
    DESC : when  user email is updated
    */
    $( "#employer-change-email" ).validate({
        rules: {
            "new_email":{
                required:true,
                email:true,
            }
        },
        messages: {
            "new_email":{
              required:"Please specify your email address",
              email:"Please specify a valid email address",
            }
        },
        errorElement:'span',
        errorClass:'error_msg',
        submitHandler: function(form) {
            $("#employer-change-email").ajaxSubmit({
                method      : 'post',
                beforeSend  : function() {
                    Loader();
                    $('#email_messages').removeClass('alert-danger').hide();
                    $('#email_messages').removeClass('alert-success').hide();
                },
                url : path+'employer/change-email',
                success     : function(data) {
					RemoveLoader();
					if(data.success == true){
						$('#email_messages').addClass('alert-success').html(data.message).fadeIn('slow').delay(3000).fadeOut();
						//redirect to logout page, so we can re-authenticate user with new password.
						//setTimeout(function(){ window.location = path+'employer/logout'; }, 3000);
						setTimeout(function(){ window.location.reload(); }, 3000);
					}else{
						$('#email_messages').addClass('alert-danger').html(data.message).fadeIn('slow').delay(3000).fadeOut();
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    RemoveLoader();
                    $( "#employer-change-email .form-grp" ).removeClass( "has-error" );
                    $( ".help-block" ).hide();
                    $.each( xhr.responseJSON, function( i, obj ) {
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        if( i=='new_email' ){
                            $('.new_email').find('span.error_msgg').slideDown(400).html(obj);
                        }
                    });
                }
            });
        }
    });
    
    
    /*
    remove error messages and clear input fields on modal hide.
    */
    $('#forgot,#change_pass').on('hidden.bs.modal', function(){
		$('#new_email,#password,#password_confirmation').val('');
		
		$('span.error_msg').empty().hide();
	});
    
    
    
    
    /*
    DESC : when  user email is updated
    */
    $( "#employer-change-logo" ).validate({
        rules: {
            "user_image":{
                required:true,
            }
        },
        messages: {
            "user_image":{
              required:"Please select an image.",
            }
        },
        errorElement:'span',
        errorClass:'error_msg',
        submitHandler: function(form) {
            $("#employer-change-logo").ajaxSubmit({
                method      : 'post',
                beforeSend  : function() {
                    Loader();
                    $('#logo_messages').removeClass('alert-danger').hide();
                    $('#logo_messages').removeClass('alert-success').hide();
                },
                url : path+'employer/change-logo',
                success     : function(data) {
					RemoveLoader();
					if(data.success == true){
						$('#logo_messages').addClass('alert-success').html(data.message).fadeIn('slow').delay(3000).fadeOut();
						//redirect to logout page, so we can re-authenticate user with new password.
						setTimeout(function(){ window.location = path+'employer/company-profile'; }, 3000);
					}else{
						$('#logo_messages').addClass('alert-danger').html(data.message).fadeIn('slow').delay(3000).fadeOut();
					}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    RemoveLoader();
                    $( "#employer-change-logo .form-grp" ).removeClass( "has-error" );
                    $( ".help-block" ).hide();
                    $.each( xhr.responseJSON, function( i, obj ) {
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'input[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').addClass('has-error');
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-grp').find('label.help-block').slideDown(400).html(obj);
                        if( i=='image' ){
                            $('.image').find('span.error_msgg').slideDown(400).html(obj);
                        }
                    });
                }
            });
        }
    });
    
    /*
    DESC : when  user email is updated
    */
    $( "#update-company-profile" ).validate({
        rules: {
           "company_name": {
              required:true,
            },
            "first_name": {
              required:true,
            },
            "last_name": {
              required:true,
            },
            "country_code": {
              required:true,
            },
            "company_code": {
              required:true,
            },
            "password": {
              required:true,
            },
            "password_confirmation": {
              required:true,
            },
            "company_address": {
              required:true,
            },
            "number_worker": {
              //required:true,
            },
            "user_image": {
              //required:true,
            },
            "company_description":{
              //required:true,
            },
            "agree":{
              required:true,
            }
        },
        messages: {
            "company_name": {
                required: "Please enter Company Name",
            },
            "first_name": {
                required: "Please enter Contact First Name",
            },
            "last_name": {
                required: "Please enter Contact Last Name ",
            },
            "email": {
                required: "Please specify your email address",
            },
            "password": {
                required: "Please enter password ",
            },
            "password_confirmation": {
                required: "Please enter confirm password ",
            },
            "company_address": {
                required: "Please enter Company Address ",
            },
            "number_worker": {
               // required: "Please select at least one value in number of workers’ field. ",
            },
            "user_image":{
              required: "Please select images.",
            },
            "company_description":{
              //required:"Please enter company description",
            },
            "country_code":{
              required:"Please enter country code",
            },
            "company_code":{
              required:"Please enter country code",
            },
        },
        errorElement:'span',
        errorClass:'error_msg',
        errorPlacement: function(error, element) {
           // alert(element.attr("name"));
            $(element).parent('.form_grp').find('span.error_msgg').slideDown(400).html(error);
            if(element.attr("name") == "user_image"){
              $(element).closest('.form_grp').parent('.form-field').find('span.error_msgg').slideDown(400).html(error);
            }

            if(element.attr("name") == "agree"){
                $(element).closest('span').parent('.agree').find('span.error_msgg').slideDown(400).html(error);
            }
        },
        submitHandler: function(form,e) {
			var err = false;
			if($('select[name="industry_type[]"]').val()==null)
			{
				$('.industrytype').find('span.error_msgg').slideDown(400).html("Please select at least one value in type of industries field.");
				$('.industrytype').find('span.error_msgg').show();
				err = true;
			}
			else
			{
				$('.industrytype').find('span.error_msgg').hide();
			}
			if($('#location').val())
			{
				if($('#lat').val() == '' || $('#lng').val() == ''){
					$('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid location.'); 
					err = true;
				}
			}

			
			if($('#phone_number').val() == '')
			{
				$('.phonenumber').find('span.error_msgg').slideDown(400).html('Please enter phone number.');
				err = true;
			   //$('.companycontact').find('span.error_msgg').slideDown(400).html('Please enter contact number.'); 
			}
			else
			{
				$('.phonenumber').find('span.error_msgg').hide();
			}	
			
			
			
			
			if(err == false){
				var form_data = new FormData($('form#update-company-profile')[0]);
				  $.ajax({
					url: path+'employer/update-company-profile',
					type: 'post',
					dataType: 'json',
					data: form_data,
					contentType: false,
					processData: false,
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					beforeSend:function(){
						Loader();
						//$('#updateEmployer').prop( "disabled", true );
					},
					success: function(data) {
						if(data.success==true)
						{
							$('#editform').hide();
							$('form#update-company-profile')[0].reset();
							window.location.reload();                  
						}else{
							//$('#updateEmployer').prop( "disabled", false );
							$('#msg').html('<div class="alert alert-danger"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</strong>.</div>');
						}
					},
					error: function(error) { 
						RemoveLoader();
					  $('span.error_msg').hide();
					  $('span.error_msgg').hide();
					 // $('#updateEmployer').prop( "disabled", false );
					  var result_errors = error.responseJSON;
					  if(result_errors)
					  {
						 $.each(result_errors, function(i,obj)
						 {
							$('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
							if(i == 'industry_type')
							  $('.industrytype').find('span.error_msgg').slideDown(400).html(obj);

							if(i=='company_description')
							  $('.description').find('span.error_msgg').slideDown(400).html(obj);

							  if(i == "phone")
								 $('.phonenumber').find('span.error_msgg').slideDown(400).html(obj);
							  

							  if(i == "company_contact")
								$('.companycontact').find('span.error_msgg').slideDown(400).html(obj);
								  
							  if(i == 'token_mismatch')
								window.location.reload();
						 }) 
					  }
					},
					complete: function() { RemoveLoader(); }
				  });
			}
        }
    });
 
     $(document).on('change','#user_image',function(e){
        var fileExtension = ['jpeg','png','jpg'];
        var file = $(this)[0].files;
        var type = file[0].type;
       
        // Check File Size
        if( file[0].size >5242880 ) {
			$('.error_msg').empty().hide();            
            $('#logo_messages').addClass('alert-danger').html("Please upload a valid company image within a max range of 5 MB").fadeIn('slow').delay(3000).fadeOut();
            $('#user_image').val('');
            return false;
        }

        // Check File Type
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			$('.error_msg').empty().hide();
            //bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            
            // $('#disable_message_here').html();
            $('#logo_messages').addClass('alert-danger').html("Only "+fileExtension.join(', ')+ " format is allowed ").fadeIn('slow').delay(3000).fadeOut();
            $('#user_image').val('');
            return false;
        }
        
        
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var image = new Image();
            image.onload = function (imageEvent) {
                // Resize the image
                var orig_canvas = document.createElement('canvas'),
				max_size = 544,// TODO : pull max size from a site config
				width = image.width,
				height = image.height;
                //canvas width and height should be same as image is having
                orig_canvas.width = width;
                orig_canvas.height = height;
                //paste the image to the canvas i.e.. having same height and width
                orig_canvas.getContext('2d').drawImage(image, 0, 0);
                //get small resiged image into equal dimensions
                var canvas = getResizedCanvas(orig_canvas,500,500);
                var dataUrl = canvas.toDataURL('image/jpeg');
                var resizedImage = dataURLToBlob(dataUrl);
                $.event.trigger({
                    type: "imageResized",
                    blob: resizedImage,
                    url: dataUrl
                });
            }
            image.src = readerEvent.target.result;
        }
        reader.readAsDataURL(file[0]);
    });
    
    
    $(document).on("imageResized", function (event) {
		if (event.blob && event.url) {
			Loader();
			firebase_multiple_upload(event.blob);
		}
	});
	
	
	function getResizedCanvas(canvas,newWidth,newHeight) {
		var tmpCanvas = document.createElement('canvas');
		tmpCanvas.width = newWidth;
		tmpCanvas.height = newHeight;

		var ctx = tmpCanvas.getContext('2d');
		ctx.drawImage(canvas,0,0,canvas.width,canvas.height,0,0,newWidth,newHeight);

		return tmpCanvas;
	}
    
    
    
    var dataURLToBlob = function(dataURL) {
		var BASE64_MARKER = ';base64,';
		if (dataURL.indexOf(BASE64_MARKER) == -1) {
			var parts = dataURL.split(',');
			var contentType = parts[0].split(':')[1];
			var raw = parts[1];

			return new Blob([raw], {type: contentType});
		}

		var parts = dataURL.split(BASE64_MARKER);
		var contentType = parts[0].split(':')[1];
		var raw = window.atob(parts[1]);
		var rawLength = raw.length;

		var uInt8Array = new Uint8Array(rawLength);

		for (var i = 0; i < rawLength; ++i) {
			uInt8Array[i] = raw.charCodeAt(i);
		}

		return new Blob([uInt8Array], {type: contentType});
	}
    
    
    
    
    
    function firebase_multiple_upload(file){
        var timestamp=Date.now();
        
        var file_name = "{{$user['company_name']}}";
       // var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
        var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file_name); //creating firebase image reference
        var metadata = {
            contentType: 'image/jpeg',
        };
        var blob_image=    file;//new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
           $('#image').val(snapshot.downloadURL);
           //display new image,
           $("#user_image_display").find('img').removeAttr('src').attr('src',snapshot.downloadURL);
            RemoveLoader();
        }).catch(function(error) {
            alert('firebase error occured:'+error);
            RemoveLoader();
        });
        RemoveLoader();
    }
    
    
</script>

@endsection
