
@php 
$notify = Helper::getAllNotification();
$active = isset($active)?$active:'';
@endphp
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="MadWall is Your Local Innovative On-Demand Staffing Agency. Register today and watch your job fill up in minutes and connect with workers who meet your needs!">
<meta name="csrf-token" content="{!! csrf_token() !!}" />
<title>MadWall | @yield('title')</title>
<link href="{{ asset('public/employer/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('public/employer/css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('public/employer/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/employer/css/owl.carousel.css') }}" rel="stylesheet">
<link rel="shortcut icon" href="{{ url('public/logos/favicon.ico') }}"/>

<style>

.modal-footer button {
  float:right;
  margin-left: 10px;
}

</style>





@yield('css')
</head>

<body>
<div class="loader-section">

<div class="cssload-container">
	<div class="cssload-whirlpool"></div>
</div>
</div>

<div class="wrapper">

<div class="main">
<!-- Header section -->
<header class="header-main wow fadeIn dashboradbanner ">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="logo"><a href="{{url('')}}"><img src="{{ url('public/employer/images/logo_dash.png') }}" alt=""></a></div>
<a href="#" class="drop-opener pull-right">
									<span></span>
									<span></span>
									<em class="border"></em>
                                   
								</a>
</div>
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
	
	<!-- ----------------- Notifications Here ------------------ -->
	<div class="notification_div">
		<div class="bell_div dropdown">
			<a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bell-o" aria-hidden="true"></i>
			</a>
			<span id="notification_count"></span>
			<ul class="dropdown-menu" aria-labelledby="dLabel" id="display_notifications_here"></ul>
		</div>
	</div>
	<!-- --------------------------- --------------------- ---------- -->
	
@php
$url = '';
	if(Auth::user()){
		if(!empty(Auth::user()->image)){
			$url = Auth::user()->image;
		}else{
			$url = url('public/employer/images/profile_avatar.png')	;
		}
		
	}
	else
		$url = url('public/employer/images/profile_avatar.png')	;
@endphp
<div class="profile_pic_name"><div class="company_name">Welcome <b>@if(Auth::user())  {{ Auth::user()->company_name }} @endif</b></div>
<div class="name_of_profile"><a href="{{url('/employer/company-profile')}}"><img src="{{ $url }}" /></a></div>
<div class="drop_profile dropdown"><a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down" aria-hidden="true"></i>
</a>
<ul class="dropdown-menu profile_sec_menu" aria-labelledby="dLabel">



   <!-- <li>
		<a href="{{ url('employer/view-profile') }}">
		<i class="icon-user"></i> Edit My Profile </a>
	</li>

	<li>
		<a href="{{ url( 'employer/view-change-password' ) }}">
		<i class="icon-lock"></i> Change Password </a>
	</li> -->
<li><a href="javascript:void(0);" id="logout">Logout</a></li>


  </ul>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="inner_banner_sec">
<div class="row">
<h1><b>{{$title}}</b></h1>
<div class="col-lg-12 col-md-12 colo-sm-12">
 
<div class="menu_bar">
<div class="navSection">
            <div class="nav-holder" id="menu-drop">
<ul>
<!-- <li><a href="{{ url('') }}">Home</a></li> -->
<li class="{{ ($active == 'dashboard')?'active':''}}"><a href="{{ url('employer/dashboard') }}">Active Jobs</a></li>
<li class="{{ ($active == 'history')?'active':''}}"><a href="{{ url('employer/jobhistory') }}"> Jobs History</a></li>
<li class="{{ ($active == 'company-profile')?'active':''}}"><a href="{{ url( 'employer/company-profile' ) }}"> Company Profile</a></li>
<li class="{{ ($active == 'about')?'active':''}}"><a href="{{ url( 'employer/contact' ) }}"> Contact MadWall</a></li>
</ul>
</div>
</div>

</div>
</div>
</div>
@php 
	$url = 'javascript:void(0)';
	$class = '';
@endphp
<!-- ---------- updated : 3 aug 2017, shivani -- to check user status dynamically on click of post job button -------- -->
<a href="{{ $url }}" class="post_a_job"><span>+</span><div class="text_post_btn">Request Worker</div></a>
</div>
</div>
</header>
