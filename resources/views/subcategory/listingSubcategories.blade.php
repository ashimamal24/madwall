@extends('admin.layout')
@section('title')
	SubCategories	
@endsection
@section('content')
<h3 class="page-title">
Subcategory
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-subcategories' ) }}">Manage Subcategory</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-body">
					<div class="table-container">
						<input type="hidden" name="action" value="filter-subcategories"/>
						<div class="table-actions-wrapper">
							<span>
							</span>
						<a href="{{url('admin/add-subcategories')}}" class="btn blue btn-sm pull-right">Add Subcategory</a>
						</div>
						<div class="table-custom table-responsive">
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax_for_subcategory">
							<thead>
							<tr role="row" class="heading">
								<th width="5%">No.</th>
								<th width="15%">Name</th>
								<th width="15%">Category</th>
								<th width="10%">Active/Inactive</th>
								<th width="15%">Actions</th>
							</tr>
							<tr role="row" class="filter">
								<td></td>
								<td><input type="text" class="form-control form-filter input-sm" name="name" id="approved_planname" autocomplete="off"></td>
								
								<td></td>
								<td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
								<td>
									<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
									<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>
								</td>
							</tr>
							</thead>
						</table>
					</div>
					</div>
				</div>
			</div>
		</div><!-- End: life time stats -->
	</div><!-- END PAGE CONTENT-->
	@include('subcategory.popupSubcategory')
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwsubcategory.js') }}"></script>
<script>


</script>
<script>

jQuery(document).ready(function() {

	/***********user ajax view *******/
	$(document).on("click", "#view", function () {
        var url_for_user_view = adminname+'/view-subcategories';
        var catId = $(this).attr("catId");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: catId,_token:token},
            dataType: "JSON",
			success: function (result) {
				if ( result.status == 'error' ) {
				    bootbox.alert('some problem occur try again.....');
				}else {                   
					
					if(result.reslutset.name){
						$('#name').text(result.reslutset.name);
				   	} else{
				   		$('#name').text('N/A');
				   	}

					if(result.reslutset.category_object[0].name){
						$('#category_name').text(result.reslutset.category_object[0].name);
					} else{
				    	$('#category_name').text('N/A');
				   	}

				   	if(result.skills){
						$('#skills').text(result.skills);
				   	} else{
				   		$('#skills').text('N/A');
				   	}
				   	
				   	if(result.mandatory_skills){
						$('#mandatory_skills').text(result.mandatory_skills);
				   	} else{
				   		$('#mandatory_skills').text('N/A');
				   	}
					
					if(result.reslutset.description){
						$('#description').text(result.reslutset.description);
					} else {
						$('#description').text('N/A');
					}
				   $('#myModal').modal('show');
				}
			}
        });
    });
	/***********user ajax view ends here*******/
	
	$(document).on("click", "#deletesubcategory", function () {
        
        //var deleteLink = path+$(this).attr('deleteLink');
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";
        bootbox.confirm("Are you sure you want to delete?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/delete-subcategory/'+id,
	         		type: "POST",
	         		data: {id: id,_token:token},
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/list-subcategories';
						}
						if( data.success == false ){
							window.location = path+'admin/list-subcategories';
						}
					},
	         	});
            }
        });
    });
    
	
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		 bootbox.confirm('Are you sure you want to  '+(status == true ? "Inactive" : "Active")+' this item ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/lock-category',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					complete: function(){
						
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage('Status changed');
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});

	TableAjax.init();
	TableAjax.update();
});
</script>

@endsection
