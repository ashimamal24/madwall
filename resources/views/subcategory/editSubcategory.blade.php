@extends('admin.layout')
@section('title')
	Edit Sub-Category
@endsection
@section('heading')
	Edit Subcategory
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/chosen.css' ) }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Subcategory
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_subcategories, ['method' => 'POST','url' => '/admin/edit-subcategory','id'=>'editsubcategory']) }}
				@include('subcategory.subcategoryform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit', array('id'=>'action') ) }}

			{{ Form::hidden( 'idedit', $edit_subcategories['_id'], ['id'=>'idedit'] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script src="{{ asset( 'public/admin/js/chosen.js') }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {
	$("#mandatory_skills").chosen();
	$("#skills").chosen();
	$('#industry_id').chosen();
	$('#category_id').chosen();
	/* 	=========================
		Send Ajax to add Category
		=========================
	*/
	
	$('#addsubcategory').click(function(){
		var name = $('#name').val();
		var description = $('#description').val();
		var category_id = $('#category_id').val();
		var category_name = $('#category_id option:selected').text(); 
		var industry_id = $('#industry_id option:selected').val(); 
		var status = $('#status').val();
		var token ="{{ csrf_token() }}"; 
		var skills = []; 
            $('#skills :selected').each(function(i, selected){ 
                skills[i] = $(selected).val();
        });
		var mandatory_skills = []; 
            $('#mandatory_skills :selected').each(function(i, selected){ 
                mandatory_skills[i] = $(selected).val();
        });
		var id = $('#idedit').val();
		var action = $('#action').val();
		$.ajax({
			dataType: 'json',
			method:'post',
			url: path+'admin/edit-subcategory/'+id,
			data 		: { name:name, description:description, category_id:category_id, category_name:category_name, _token:token, skills:skills,mandatory_skills:mandatory_skills,industry_id:industry_id, status:status,action:action,idedit:id },
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				if( data.success == true ){
					window.location = path+'admin/list-subcategories';
				}
				if( data.success == false ){
					window.location = path+'admin/list-subcategories';
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#editsubcategory .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					
					if( i=='category_id' ){
						$('.category_id').addClass('has-error');
						$('.category_id' ).find('label.help-block').slideDown(400).html(obj);
					}

					if( i=='skills' ){
						$('.skills').addClass('has-error');
						$('.skills' ).find('label.help-block').slideDown(400).html(obj);
					}
										
					if( i=='mandatory_skills' ){
						$('.mandatory_skills').addClass('has-error');
						$('.mandatory_skills' ).find('label.help-block').slideDown(400).html(obj);
					}
					
					if( i=='status' ){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
					if( i=='industry_id' ){
						$('.industry_id').addClass('has-error');
						$('.industry_id').find('label.help-block').slideDown(400).html(obj);
					}
					
				});	
			}
		});
	});

	/* 	==================================================================
		Select Subcategory on based on change from Category to subcategory 
		==================================================================
	*/
	$('#type').on('change', function(){
        var val = $(this).val();
        if( val && val == 'subcategory' ) {
        	$( '#parent_id' ).prop( 'disabled', false );
        } else {
        	$( '#parent_id' ).prop( 'disabled', true );
        }
    });

    if( $('#type').val() == 'subcategory' ){
		$( '#parent_id' ).prop( 'disabled', false );
	}

	/* 	========================================================
		Fetch Industry nam ein hidden feild to store in database 
		========================================================
	*/
	$(document).on('change','#industry_id',function(){
		
		//var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		var industry_id = $(this).val();
		
		$.ajax({
			url:path+'admin/industry-category',
			type:'get',
			data:'industry_id='+industry_id,
			dataType:'json',
			beforeSend:function(){
				addLoader();
			},
			success:function(data){
				
				removeLoader();
				$('#category_id').html('');
				var data1 = '<option value="">Please select category</option>';
				if(data.category)
				{
					$.each(data.category, function(index, element) {
					   data1 +='<option value="'+element._id+'">'+element.name+'</option>';
					});
				}
				$('#category_id').html(data1);
				//jcf.getInstance($('#category_id')).refresh();
				//$('#multiselect').html('');
				$('#category_id').trigger("chosen:updated");
			},
			error:function(errors){
				removeLoader();
			},
			complete:function(){
				removeLoader();
			}
		});
	});

});
</script>

@endsection	
