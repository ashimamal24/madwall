<div class="form-body">
    <div class="row">
		
		<!-- ------------ select Industry -------------- -->
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group industry_id">
                {{ Form::label( 'industry_id', 'Select Industry: ',['class' => 'control-label'] ) }} <span class="star">*</span>

                @if(count($industries)) 
                {{ Form::select( 'industry_id', array_replace([''=>'Select'],$industries), null, ['class' => 'form-control','id'=>'industry_id'] ) }}
                @else
                    {{ Form::select( 'industry_id', [''=>'No Industry'], null, ['class' => 'form-control','id'=>'category_id'] ) }}
                @endif
                <label class="help-block"></label>
            </div>
        </div>
        
        <!-- ------------ select Category -------------- -->
        <div class="col-md-4">
            <div class="form-group category_id">
                {{ Form::label( 'category_id', 'Select Category: ',['class' => 'control-label'] ) }} <span class="star">*</span>

                @if(count($categories)) 
                {{ Form::select( 'category_id', array_replace([''=>'Please select category'],$categories), null, ['class' => 'form-control','id'=>'category_id'] ) }}
                @else
                    {{ Form::select( 'category_id', [''=>'No Category'], null, ['class' => 'form-control','id'=>'category_id'] ) }}
                @endif
                <label class="help-block"></label>
            </div>
        </div>
    </div>

    <div class="row">
		<!-- ----------- Select mandatory skills ------------ -->
		<div class="col-md-offset-2 col-md-4">
            <div class="form-group mandatory_skills">
            
                {{ Form::label('mandatory_skills', 'Mandatory Skills: ',['class' => 'control-label'] ) }}
               
                {{ Form::select('mandatory_skills[]', $skills, null   , ['multiple' => 'multiple','class' => 'form-control','id'=>'mandatory_skills']) }}
                
                
                <label class="help-block"></label>
            </div>
        </div>
		<!-- ---------- select optional skills ----------- -->
		<div class="col-md-4">
            <div class="form-group skills">
            
                {{ Form::label('skills', 'Optional Skills: ',['class' => 'control-label'] ) }}  
               
                {{ Form::select('skills[]', $skills, null   , ['multiple' => 'multiple','class' => 'form-control','id'=>'skills']) }}
                
                
                <label class="help-block"></label>
            </div>
        </div>
    </div>
    
    
    
    <div class="row">
		<!-- ---------- subcategory name ---------- -->
		<div class="col-md-offset-2 col-md-4">
			<div class="form-group">
				{{ Form::label('name', 'Name: ',['class' => 'control-label'] ) }} <span class="star">*</span> 
				{{ Form::text( 'name' , null, ['class' => 'form-control', 'maxlength'=> '40' ] ) }}
				<label class="help-block"></label>
			</div>
		</div>
        <!-- --------- status ---------- -->
        <div class="col-md-4">
            <div class="form-group status">
                 {{ Form::label('status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span>                
                 @if(isset($edit_subcategories['status']))
                    @if($edit_subcategories['status'] == 0)
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),0,['class' => 'form-control']) !!}
                    @else
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),1,['class' => 'form-control']) !!}
                    @endif
                @else
                    {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) !!}
                @endif
                <label class="help-block"></label>
            </div>
        </div>
    </div>    
    <!-- --------- description -------------- -->
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label('description', 'Description: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::textarea('description',null,['class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
                <label class="help-block"></label>
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="col-sm-4"></div>
    <div class="col-sm-6">
        {{ Form::button( $submitButtonText, ['id'=>'addsubcategory','class' => 'btn btn-primary']) }}
        {{ Html::link( 'admin/list-subcategories', 'Cancel', array( 'class' => 'btn btn-primary' ))}}
    <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
