<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>View Industry Details</b></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>Name: </th>
                            <td id="name"></td>
                        </tr>


                        <tr>
                            <th>Logo: </th>
                            <td>
                                <img id="logo"  src="" class="img-circle" width="50" height="50"/>
                            </td>
                        </tr>
                       
                        <tr>
                            <th>Description: </th>
                            <td id="description"></td>
                        </tr>


                        <tr>
                            <th>User Name: </th>
                            <td id="user_name"></td>
                        </tr>

                        <tr>
                            <th>User image: </th>
                            <td>
                                <img id="user_image" src="" class="img-circle"  width="50" height="50" />
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>