<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>New registration</title>
	<meta name="new message" />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
<style type="text/css">
#outlook a{padding:0;}

.ReadMsgBody{width:100%;} 
.ExternalClass{width:100%;}
ol li {margin-bottom:15px;}
	
img{height:auto; line-height:100%; outline:none; text-decoration:none;}
	
p {margin: 1em 0;text-align:center}

h1, h2, h3, h4, h5, h6 {color:#404040 ; font-family: 'PT Sans', 'Helvetica Neue',Helvetica,Arial,sans-serif;}
	
table td {border-collapse:collapse;}
	
.yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: rgb(64,64,64); text-decoration: none ; border-bottom: none ; background: none ;}
	
.im {color:#404040;}

		
@media screen and (max-device-width: 480px), screen and (max-width: 480px) { 
		td[class=emailcolsplit]{
			width:100%!important; 
			float:left!important;
			text-align:center !important;
			padding-left:0!important;
			max-width:430px !important;
		}

}
</style>

</head>

<body style="width:100%; margin:0; padding:0;background-color:none">
<!-- header -->
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10">&nbsp;</td>
  </tr>
   <tr>
    <td  style="color: #fff; font-weight:bold; text-align:center; font-size: 34px; line-height:2em;"><img border="0" alt="{{ config('app.APP_NAME') }}" src="{{ asset('public/employer/images/logo.png') }}") width="120"></td>
  </tr>
  
  
</table>
	<!-- header end -->
