<style type="text/css">
    .error{ color: #B71C1C; }
#userActions{
    display: table-cell;
    height: 150px;
    width: 10% ;
    vertical-align: middle;
    text-align: center;
    color: #37474F;
    background-color: #FFFFFF;
    border: solid 2px #333333;
    border-radius: 10px;
}
#userActions input{
    width: 150px;
    margin: auto;
}
#imgPrime{ display: none; }
</style>

<div class="form-body">
    <div class="row">
       
        <div class="col-md-12">
            
            <div class="form-group industry_id">
               
                {{ Form::label( 'blog_cat_id', 'Select Category: ',['class' => 'control-label'] ) }} <span class="star">*</span>
              
                @if(count($blogCategories)) 
                {{ Form::select('_id', array_replace([''=>'Select'],$blogCategories), null, ['class' => 'form-control','id'=>'blog_cat_id'] ) }}
                @else
                    {{ Form::select( 'blog_cat_id', [''=>'No Industry'], null, ['class' => 'form-control','id'=>'category_id'] ) }}
                @endif
                 
           
                <label class="help-block"></label>

            
             
            </div>

            <div class="form-group">
                {{ Form::label( 'title', 'Title of Blog: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'title',null,['class' => 'form-control']) }}
                <label class="help-block"></label>
            </div>

           


            <div class="form-group">
                {{ Form::label('blog_description', 'Blog Content: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::textarea('blog_description',null,['id'=>'description1', 'class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
                <label class="help-block"></label>
            </div>
            
            <div class="form-group">
                {{ Form::label( 'author_name', 'Author Name: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'author_name',null,['class' => 'form-control']) }}
                <label class="help-block"></label>
            </div>


            <div class="form-group">
                {{ Form::label('author_description', 'Author Info: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::textarea('author_description',null,['id'=>'description2', 'class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
                <label class="help-block"></label>
            </div>


            <div class="form-group">
                <div id="userActions">
                <p>Drag &amp; Drop Image</p>
                <input type="file" id="fileUpload" /><br/>
                <img id="imgPrime" src="" alt="uploaded image placeholder" />
                </div>
                
            </div>
            

             <div id="dropzone">
                <form class="dropzone needsclick" id="demo-upload" action="/upload">
                  <div class="dz-message needsclick">    
                    Drop files here or click to upload.<BR>
                    <SPAN class="note needsclick">(This is just a demo dropzone. Selected 
                    files are <STRONG>not</STRONG> actually uploaded.)</SPAN>
                  </div>
                </form>
              </div>
          
        
           
        </div>
    </div>

</div>

<div class="box-footer">
    <div class="col-md-4"></div>
    <div class="col-sm-8">
        {{ Form::button( $submitButtonText, ['id'=>'addblog','class' => 'btn btn-primary']) }}
        {{ Html::link('admin/list-wage', 'Cancel', array( 'class' => 'btn btn-primary' ))}}   
    <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
