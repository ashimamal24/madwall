@extends('admin.layout')
@section('title')
   @lang('admin.breadcrumbs.breadcrumb.add_post')

@endsection
@section('css')

<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" type="text/css" rel="stylesheet"/>



@endsection
@section('heading') 
   @lang('admin.breadcrumbs.breadcrumb.add_post')
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Add Blog
            </div>
        </div>
        <div class="portlet-body form">
            @include('errors.user_error')
            @include('flash::message')
            @if (count($errors) > 0)
            @endif
            {{ Form::open(array( 'method' => 'POST','url' => '/admin/add-blog','id'=>'addblogform')) }}
                @include('admin-blogs.blogform',['submitButtonText' => 'Add Blog'])
            {{ Form::close() }}
        </div>  
    </div>
</div>

@endsection 
@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/blog-editor-settings.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js" type="text/javascript"></script>

<script>


   

jQuery(document).ready(function() {


    $('#addblog').click(function(){
        var formData = new FormData($('#addblogform')[0]);
        $.ajax({
            processData: false,
            contentType: false,
            method      : 'post',
            cache  :false,
            url         : path+'admin/blogs/store',
            data        : formData,
            beforeSend  : function() {
                addLoader();
            },
            success     : function(data) {
                window.location = path+'admin/blogs/list';
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#addcategoryform .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                
                /* Start each loop*/
                $.each( xhr.responseJSON, function( i, obj ) {
                
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);

                    if( i=='type' ){
                        $('.type').addClass('has-error');
                        $('.type').find('label.help-block').slideDown(400).html(obj);
                    }
                    if( i=='industry_id' ){
                        $('.industry').addClass('has-error');
                        $('.industry' ).find('label.help-block').slideDown(400).html(obj);
                    }
                    
                    if( i=='status' ){
                        $('.status').addClass('has-error');
                        $('.status').find('label.help-block').slideDown(400).html(obj);
                    }
                }); /* End each loop */
            } /* End error() */
        });
    });


    

    $('#blog_cat_id').change(function(){
        $('#blog_cat_name').val($('#blog_cat_id').find(":selected").text() );
    });
    

});
var dropzone = new Dropzone('#demo-upload', {
  previewTemplate: document.querySelector('#preview-template').innerHTML,
  parallelUploads: 2,
  thumbnailHeight: 120,
  thumbnailWidth: 120,
  maxFilesize: 3,
  filesizeBase: 1000,
  thumbnail: function(file, dataUrl) {
    if (file.previewElement) {
      file.previewElement.classList.remove("dz-file-preview");
      var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
      for (var i = 0; i < images.length; i++) {
        var thumbnailElement = images[i];
        thumbnailElement.alt = file.name;
        thumbnailElement.src = dataUrl;
      }
      setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
    }
  }

});


// Now fake the file upload, since GitHub does not handle file uploads
// and returns a 404

var minSteps = 6,
    maxSteps = 60,
    timeBetweenSteps = 100,
    bytesPerStep = 100000;

dropzone.uploadFiles = function(files) {
  var self = this;

  for (var i = 0; i < files.length; i++) {

    var file = files[i];
    totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

    for (var step = 0; step < totalSteps; step++) {
      var duration = timeBetweenSteps * (step + 1);
      setTimeout(function(file, totalSteps, step) {
        return function() {
          file.upload = {
            progress: 100 * (step + 1) / totalSteps,
            total: file.size,
            bytesSent: (step + 1) * file.size / totalSteps
          };

          self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
          if (file.upload.progress == 100) {
            file.status = Dropzone.SUCCESS;
            self.emit("success", file, 'success', null);
            self.emit("complete", file);
            self.processQueue();
            //document.getElementsByClassName("dz-success-mark").style.opacity = "1";
          }
        };
      }(file, totalSteps, step), duration);
    }
  }
}

</script>

@endsection 
