@extends('admin.layout')
@section('title')
	Blogs	
@endsection
@section('content')
<h3 class="page-title">
Blogs
</h3><br>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/blogs/list') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/blogs/list' ) }}">Manage Blogs</a>
		</li>
	</ul>
</div>

@include('flash::message')
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-body">
				<div class="table-container">
						<input type="hidden" name="action" value="blogs/filter-blogs"/>
						<div class="table-actions-wrapper">
							<span>
							</span>
						<a href="{{url('admin/blogs/add')}}" class="btn blue btn-sm pull-right">Add Blog</a>
						</div>
						<div class="table-custom table-responsive">
							<table class="table table-striped table-bordered table-hover"  id="blogs">
								<thead>
								<tr role="row" class="heading">
									<th width="1%">No.</th>
									<th width="10%">Blog Heading</th>
									<th width="10%">Author Name</th>
									<th width="10%">Active/Inactive</th>
									<th width="10%">Actions</th>
								</tr>
								<tr role="row" class="filter">
									<td></td>
									<td><input type="text" class="form-control form-filter input-sm" name="name" id="" autocomplete="off"></td>
									<td></td>
									
									<td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
									<td>
										<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
										<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
									</td>
								</tr>
								</thead>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END PAGE CONTENT-->

@endsection
@section('js')
<script src="{{ asset('public/admin/js/mwblogs.js') }}"></script>
<script>
jQuery(document).ready(function() {
   
	/** Delete **/
	$(document).on( "click", "#deleteblogcategory", function () {
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";

        bootbox.confirm("Are you sure you want to delete?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/blog-category/delete/'+id,
	         		type: "POST",
	         		 data: {id: id,_token:token},
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/blog-category/list';
						}
						if( data.success == false ){
							window.location = path+'admin/blog-category/list';
						}
					},
	         	});
            }
        });
    });
    
   
    $(document).on( "click", "#blockunblockblogcategory", function () {
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";

        bootbox.confirm("Are you sure you want to delete?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/blog-category/delete/'+id,
	         		type: "POST",
	         		 data: {id: id,_token:token},
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/blog-category/list';
						}
						if( data.success == false ){
							window.location = path+'admin/blog-category/list';
						}
					},
	         	});
            }
        });
    });

    
    
   /** Change Ststus **/
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to  '+(status == true ? "Inactive" : "Active")+' this item ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/blog-category',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage('Status changed');
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});
	
	
	TableAjax.init();
	TableAjax.update();
	

});
</script>

@endsection
