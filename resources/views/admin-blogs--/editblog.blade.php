@extends('admin.layout')
@section('title')
	Edit Blog 
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
<style>

.img_show_div img {
    float: left;
    position: static;
}

.img_show_div{
	width: 100%;
	float: right;
}

</style>
@endsection
@section('heading')
	Edit Blog 
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Blog 
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $editBlog, ['method' => 'POST','url' => '/admin/edit-blog','id'=>'editblog']) }}
				@include('admin-blogs.blogform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			
			{{ Form::hidden( 'idedit', $editBlog['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/blog-editor-settings.js') }}" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {     
	$('#addblog').click(function(){
	var formData = new FormData($('#editblog')[0]);
	var id = $('#idedit').val();
	$.ajax({
		dataType: 'json',
		method:'post',
		processData: false,
		contentType: false,
		url: path+'admin/blogs/update/'+id,
		data: formData,
		beforeSend : function() {
			addLoader();
		},
		
		success  : function(data) {
			if( data.success == true ){
				window.location = path+'admin/blogs/list';
			}
			if( data.success == false ){
				window.location = path+'admin/blogs/list';
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			removeLoader();
			
			$("#editblogcategory .form-group").removeClass("has-error");
			$(".help-block").hide();
			$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
	});
	
	});

});
</script>

@endsection	
