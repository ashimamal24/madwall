@extends('admin.layout')
@section('title')
	Edit FAQ
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
@endsection
@section('heading')
	Edit FAQ
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit FAQ
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_faq, ['method' => 'POST','url' => '/admin/edit-faq','id'=>'editfaq']) }}
				@include('faq.faqForm',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			
			{{ Form::hidden( 'idedit', $edit_faq['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<!-- <script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/faq-editor-settings.js') }}" type="text/javascript"></script>
 -->
<script>
jQuery(document).ready(function() {     
	$('#addfaq').click(function(){
	var formData = new FormData($('#editfaq')[0]);
	var id = $('#idedit').val();
	$.ajax({
		dataType: 'json',
		method:'post',
		processData: false,
		contentType: false,
		url: path+'admin/edit-faq/'+id,
		data: formData,
		beforeSend : function() {
			addLoader();
		},
		
		success  : function(data) {
			if( data.success == true ){
				window.location = path+'admin/list-faqs';
			}
			if( data.success == false ){
				window.location = path+'admin/list-faqs';
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			removeLoader();
			
			$("#editfag .form-group").removeClass("has-error");
			$(".help-block").hide();
			$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
	});
	
	});

});
</script>

@endsection	
