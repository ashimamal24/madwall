<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>View FAQ Details</b></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Question: </th>
                                <td id="faqs"></td>
                            </tr>
                            <tr>
                                <th>Answer: </th>
                                <td id="answere"></td>
                            </tr> 
                        </tbody>
                </div>
                </table>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn green" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>