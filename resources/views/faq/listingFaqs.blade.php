@extends('admin.layout')
@section('title')
	FAQ	
@endsection
@section('content')
<h3 class="page-title">
FAQ
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-fags' ) }}">Manage FAQ</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-body">
				<div class="table-container">
						<input type="hidden" name="action" value="filter-faq"/>
						<div class="table-actions-wrapper">
							<span>
							</span>
						<a href="{{url('admin/add-faq')}}" class="btn blue btn-sm pull-right">Add FAQ</a>
						</div>
						<div class="table-custom table-responsive">
							<table class="table table-striped table-bordered table-hover"  id="datatable_ajax_for_faq">
								<thead>
								<tr role="row" class="heading">
									<th width="5%">No.</th>
									<th width="5%">Question</th>
									<th width="5%">Answer</th>
									<th width="10%">Active/Inactive</th>
									<th width="10%">Actions</th>
								</tr>
								<tr role="row" class="filter">
									<td></td>
									<td><input type="text" class="form-control form-filter input-sm" name="faqs" id="skillname" autocomplete="off"></td>
									<td></td>
									<td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
									<td>
										<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
										<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
									</td>
								</tr>
								</thead>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END PAGE CONTENT-->
	@include('faq.popupFaq')
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwfaq.js') }}"></script>
<script>
jQuery(document).ready(function() {

	$(document).on("click", "#view", function () {
        /***********user ajax view *******/
        var url_for_user_view = adminname+'/view-faq';
        var faqId = $(this).attr("faqId");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: faqId,_token:token},
            dataType: "JSON",
			success: function (result) {
				if ( result.status == 'error' ) {
				    bootbox.alert('some problem occur try again.....');
				} else {                   
				  
					if(result.reslutset.faqs){
						$('#faqs').text(result.reslutset.faqs);
				   	} else{
				   		$('#faqs').text('NA');
				   	}
				  
					if(result.reslutset.answer){
						$('#answere').text(result.reslutset.answer);
					} else {
						$('#answere').text('NA');
					}
				   $('#myModal').modal('show');
				}
			}
        });
        /***********user ajax view ends here*******/

    });
	
	/** Delete Skill **/

	$(document).on( "click", "#deletefaq", function () {
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";

        bootbox.confirm("Are you sure you want to delete?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/delete-faq/'+id,
	         		type: "POST",
	         		 data: {id: id,_token:token},
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/list-faqs';
						}
						if( data.success == false ){
							window.location = path+'admin/list-faqs';
						}
					},
	         	});
            }
        });
    });
    
   /** Change Ststus **/
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to  '+(status == true ? "Inactive" : "Active")+' this item ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage('Status changed');
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});
	
	
	TableAjax.init();
	TableAjax.update();
	

});
</script>

@endsection
