@extends('admin.layout')
@section('title')
	Add FAQ
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
@endsection
@section('heading')
	Add FAQ
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Add FAQ
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			@if (count($errors) > 0)
		    @endif
			{{ Form::open(array( 'method' => 'POST','url' => '/admin/add-faq','id'=>'addfaqform')) }}
				@include('faq.faqForm',['submitButtonText' => 'Add FAQ'])
		    {{ Form::close() }}
		</div>	
	</div>
</div>

@endsection	
@section('js')
<!-- <script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/faq-editor-settings.js') }}" type="text/javascript"></script> -->


<script>

jQuery(document).ready(function() {     
	$('#addfaq').click(function(){
		var formData = new FormData($('#addfaqform')[0]);
		$.ajax({
			dataType: 'json',
			method:'post',
			processData: false,
			contentType: false,
			url: path+'admin/add-faq',
			data: formData,
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				window.location = path+'admin/list-faqs';
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#addskillform .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
						$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
						$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
						$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
						$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
						
						if(i=='status'){
							$('.status').addClass('has-error');
							$('.status').find('label.help-block').slideDown(400).html(obj);
						}
					});
				}
		});
	});
});
</script>

@endsection	
