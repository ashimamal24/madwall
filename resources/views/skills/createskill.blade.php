@extends('admin.layout')
@section('title')
	Add Skill
@endsection
@section('heading')
	Add Skill
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Add Skill
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			@if (count($errors) > 0)
		    @endif
			{{ Form::open(array( 'method' => 'POST','url' => '/admin/add-skill','id'=>'addskillform')) }}
				@include('skills.skillform',['submitButtonText' => 'Add Skill'])
		    {{ Form::close() }}
		</div>	
	</div>
</div>

@endsection	
@section('js')
<script>
jQuery(document).ready(function() {     
	$('#addskill').click(function(){
		var formData = new FormData($('#addskillform')[0]);
		$.ajax({
			dataType: 'json',
			method:'post',
			processData: false,
			contentType: false,
			url: path+'admin/add-skill',
			data: formData,
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				window.location = path+'admin/list-skills';
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#addskillform .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
						$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
						$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
						$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
						$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
						
						if(i=='status'){
							$('.status').addClass('has-error');
							$('.status').find('label.help-block').slideDown(400).html(obj);
						}
					});
				}
		});
	});
});
</script>

@endsection	
