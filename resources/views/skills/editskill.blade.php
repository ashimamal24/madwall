@extends('admin.layout')
@section('title')
	Edit Skill
@endsection
@section('heading')
	Edit Skill
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Skill
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_skill, ['method' => 'POST','url' => '/admin/edit-skill','id'=>'editskill']) }}
				@include('skills.skillform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			
			{{ Form::hidden( 'idedit', $edit_skill['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script>
jQuery(document).ready(function() {     
	$('#addskill').click(function(){
	var formData = new FormData($('#editskill')[0]);
	var id = $('#idedit').val();
	$.ajax({
		dataType: 'json',
		method:'post',
		processData: false,
		contentType: false,
		url: path+'admin/edit-skill/'+id,
		data: formData,
		beforeSend : function() {
			addLoader();
		},
		
		success  : function(data) {
			if( data.success == true ){
				window.location = path+'admin/list-skills';
			}
			if( data.success == false ){
				window.location = path+'admin/list-skills';
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			removeLoader();
			
			$("#editskill .form-group").removeClass("has-error");
			$(".help-block").hide();
			$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
	});
	
	});

});
</script>

@endsection	
