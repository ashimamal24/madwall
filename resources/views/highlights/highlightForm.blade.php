<div class="form-body">


    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group industry">
                {{ Form::label('title', 'Title: ',['class' => 'control-label'] ) }} <span class="star">*</span> 
                {{ Form::text( 'title' , null, ['class' => 'form-control', 'maxlength'=> '40' ] ) }}
  
                <label class="help-block"></label>
            </div>
        </div>
       
        
    </div>

    <div class="row">
        <div class="col-md-offset-2 col-md-4">
           <div class="form-group">
                {{ Form::label('descriptions', 'Description: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::textarea('descriptions',null,['id'=>'descriptions', 'class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
                <label class="help-block"></label>
            </div>
        </div>
    
    </div>

    
</div>

<div class="box-footer">
    <div class="col-sm-4"></div>
    <div class="col-sm-6">
        {{ Form::button( $submitButtonText, ['id'=>'addhighlight','class' => 'btn btn-primary']) }}
        {{ Html::link( 'admin/highlights/list', 'Cancel', array( 'class' => 'btn btn-primary' ))}}
    <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
