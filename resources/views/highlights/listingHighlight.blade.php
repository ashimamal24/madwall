@extends('admin.layout')
@section('title')
	@lang('Admin/breadcrumbs.highlight')
@endsection
@section('content')
<h3 class="page-title">
@lang('Admin/breadcrumbs.highlight')
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/highlights/list' ) }}">
			@lang('Admin/breadcrumbs.highlight')</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-body">
					<div class="table-container">
						<input type="hidden" name="action" value="highlights/filter-highlight"/>
						<div class="table-actions-wrapper">
							<span>
							</span>
						<!--<a href="{{url('admin/highlights/add')}}" class="btn blue btn-sm pull-right">Add Highlight</a>-->
						</div>
						<div class="table-custom table-responsive">
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax_for_industry_content">
							<thead>
							<tr role="row" class="heading">
								<th width="5%">No.</th>
								<th width="40%">Title</th>
								<th width="40%">Description</th>
								
								<th width="10px;">Actions</th>
							</tr>
						
							</thead>
						</table>
					</div>
					</div>
				</div>
			</div>
		</div><!-- End: life time stats -->
	</div><!-- END PAGE CONTENT-->
	@include('highlights.popupHighlight')
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwhighlight.js') }}"></script>

<script>
jQuery(document).ready(function() {


	/***********user ajax view *******/
	$(document).on("click", "#view", function () {

        var url_for_user_view = adminname+'/highlights/view';
        var indusd_id = $(this).attr("indusd_id");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: indusd_id,_token:token},
            dataType: "JSON",
			success: function (result) {
			if ( result.status == 'error' ) {
			    bootbox.alert('some problem occur try again.....');
			}else {                   
				
				if(result.reslutset.title){
					$('#title').text(result.reslutset.title);
			   	} else{
			   		$('#title').text('NA');
			   	}

				
				if(result.reslutset.descriptions){
					$('#descriptions').html(result.reslutset.descriptions);
				} else {
					$('#descriptions').text('NA');
				}
               

			   $('#myModal').modal('show');
			}
			}
        });
    });
	/***********user ajax view ends here*******/
	

    $(document).on( "click", "#delete", function () {
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";

        bootbox.confirm("Are you sure you want to delete?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/highlights/delete/'+id,
	         		type: "POST",
	         		 data: {id: id,_token:token},
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/highlights/list';
						}
						if( data.success == false ){
							window.location = path+'admin/highlights/list';
						}
					},
	         	});
            }
        });
    });
	
  /** Change Ststus **/
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to  '+(status == true ? "Inactive" : "Active")+' this item ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/industries-content/change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage(json.message);
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});

	TableAjax.init();
	TableAjax.update();
});
</script>

@endsection
