@extends('admin.layout')
@section('title')
	@lang('Admin/breadcrumbs.edit_title')
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>

@endsection
@section('heading')
	@lang('Admin/breadcrumbs.edit_title')<br/><br/>
@endsection
@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/titles/list' ) }}">
			@lang('Admin/breadcrumbs.title')</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a>
			@lang('Admin/breadcrumbs.edit_title')</a>
		</li>
	</ul>
</div>
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Title
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_title, ['method' => 'POST', 'file'=> 'true', 'url' => '/admin/edit-title','id'=>'edittitleform']) }}
				@include('titles.titleForm',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
	
			{{ Form::hidden( 'idedit', $edit_title['_id'], ['id'=>'idedit' ]  ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/industry-content-editor-settings.js') }}" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>

<script>
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);

</script>

<script>
jQuery(document).ready(function() {


	$(document).on('change','#image,#user_image',function(e){
		 var imgId =$(this).attr('id');
		var fileExtension = ['jpeg','png','jpg'];
        var file = $(this)[0].files;
        var type = file[0].type;
       
        // Check File Size
        if( file[0].size >5242880 ) {
			bootbox.alert("Please upload a valid company image within a max range of 5 MB");
            $('#'+imgId).val('');           
            return false;
        }

        // Check File Type
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			         
            bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            $('#'+imgId).val('');
            return false;
        }
		addLoader();
		
		if(imgId =='image'){
		 $('.img_show_divE').remove();
        }

        if(imgId =='user_image'){
           $('.img_show_divE2').remove();
        }

		var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var image = new Image();
            image.onload = function (imageEvent) {
                // Resize the image
                var orig_canvas = document.createElement('canvas'),
				max_size = 544,// TODO : pull max size from a site config
				width = image.width,
				height = image.height;
                //canvas width and height should be same as image is having
                orig_canvas.width = width;
                orig_canvas.height = height;
                //paste the image to the canvas i.e.. having same height and width
                orig_canvas.getContext('2d').drawImage(image, 0, 0);
                //get small resiged image into equal dimensions
                var canvas = getResizedCanvas(orig_canvas,382,382);
                var dataUrl = canvas.toDataURL('image/jpeg');
                var resizedImage = dataURLToBlob(dataUrl);
                $.event.trigger({
                    type: "imageResized",
                    blob: resizedImage,
                    imageId:imgId,
                    url: dataUrl
                });
            }
            image.src = readerEvent.target.result;
        }
        reader.readAsDataURL(file[0]);
		
        removeLoader();
    });


	/*$( "#clear-preview" ).click(function(e){
		$( '.industry-image-preview' ).hide();
		$(this).hide();
		$('#pic').val("");
		$('.industry-image-preview').attr("src","");
		$('#image').val("");
		$('.file-name').val('');
	});*/

	  
	$( "#clear-preview-image" ).click(function(e){
		$( '.industry-image-preview' ).hide();
		$(this).hide();
		$('#pic').val("");
		$('.industry-image-preview').attr("src","");
		$('#image').val("");
		$('.file_url').find('.file-name').val('');
           
	});

	$( "#clear-preview-user-image" ).click(function(e){
		$( '.industry-user-image-preview' ).hide();
		$(this).hide();
		$('#pic').val("");
		$('.industry-user-image-preview').attr("src","");
		$('#image').val("");
		$('.user_image_file_url').find('.file-name').val('');

	});

	$('#addtitle').click(function(){
		var id=$('#idedit').val();
		$("#edittitleform").ajaxSubmit( {
			url: path+'admin/titles/update/'+id,
		//	contentType: false,
          //  processData: false,
			method:'post',
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				window.location = path+'admin/titles/list';
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#addindustryform .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
							$('.status').addClass('has-error');
							$('.status').find('label.help-block').slideDown(400).html(obj);
						}
					if(i=='image'){
						$('.file_url').addClass('has-error');
						$('.file_url').find('label.help-block').slideDown(400).html(obj);
					}

				    if(i=='user_image'){
					$('.user_image_file_url').addClass('has-error');
					$('.user_image_file_url').find('label.help-block').slideDown(400).html(obj);
				    }
				});
			}
        });
	});
	
	
	/*********************** IMAGE RESIZE FUNCTIONS ********************** */	 
    $(document).on("imageResized", function (event) {
		if (event.blob && event.url) {
			addLoader();
			firebase_multiple_upload(event.blob,event.imageId);
		}
	});
	
	
	function getResizedCanvas(canvas,newWidth,newHeight) {
		var tmpCanvas = document.createElement('canvas');
		tmpCanvas.width = newWidth;
		tmpCanvas.height = newHeight;

		var ctx = tmpCanvas.getContext('2d');
		//ctx.drawImage(canvas,0,0,canvas.width,canvas.height,0,0,newWidth,newHeight);
        var imgData=ctx.getImageData(0,0,canvas.width,canvas.height);
		var data=imgData.data;
		for(var i=0;i<data.length;i+=4){
		    if(data[i+3]<255){
		        data[i]=255;
		        data[i+1]=255;
		        data[i+2]=255;
		        data[i+3]=255;
		    }
		}
		ctx.putImageData(imgData,0,0);
		ctx.drawImage(canvas,0,0,canvas.width,canvas.height,0,0,newWidth,newHeight);
		return tmpCanvas;
	}
    
    
    
    var dataURLToBlob = function(dataURL) {
		var BASE64_MARKER = ';base64,';
		if (dataURL.indexOf(BASE64_MARKER) == -1) {
			var parts = dataURL.split(',');
			var contentType = parts[0].split(':')[1];
			var raw = parts[1];

			return new Blob([raw], {type: contentType});
		}

		var parts = dataURL.split(BASE64_MARKER);
		var contentType = parts[0].split(':')[1];
		var raw = window.atob(parts[1]);
		var rawLength = raw.length;

		var uInt8Array = new Uint8Array(rawLength);

		for (var i = 0; i < rawLength; ++i) {
			uInt8Array[i] = raw.charCodeAt(i);
		}

		return new Blob([uInt8Array], {type: contentType});
	}
    
    function firebase_multiple_upload(file,imageId){
        var timestamp=Date.now();
        
        var file_name = "Madwall_industry_image";
       // var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
        var storageRef = firebase.storage().ref('industry_image/'+timestamp+'_'+file_name); //creating firebase image reference
        var metadata = {
            contentType: 'image/jpeg',
        };
        var blob_image=    file;//new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
           
           //~ $('#image').val('');
           //~ $('#image').val(snapshot.downloadURL);
           //display new image,
          // $("#user_image_display").find('img').removeAttr('src').attr('src',snapshot.downloadURL);
           

            if(imageId =='image')
            {
           $( '.industry-image-preview' ).removeAttr('src').attr('src',snapshot.downloadURL);
			$('.industry-image-preview').show();
			$('#clear-preview-image').show();
			
			//var file = $(this)[0].files;
			$('.file_url').find('.file-name').val(snapshot.downloadURL);



		   }

		    if(imageId =='user_image')
			{
               	$( '.industry-user-image-preview' ).removeAttr('src').attr('src',snapshot.downloadURL);
				$('.industry-user-image-preview').show();
				$('#clear-preview-user-image').show();
				//var file = $(this)[0].files;
				$('.user_image_file_url').find('.file-name').val(snapshot.downloadURL);

				removeLoader();

			}
           
            removeLoader();
        }).catch(function(error) {
            alert('firebase error occured:'+error);
            removeLoader();
        });
        removeLoader();
    }

});
</script>

@endsection	
