@extends('emails.template') 
@section('content')
<p style="text-align:center">Dear User,</p>

<p style="text-align:center">

	We have received a password reset request for your account at {{ config('app.name') }}.<br> Please click the link below to reset your password :
	<br>
	<a href="{{$actionUrl}}" target="_blank" rel="noopener noreferrer" class="x_button x_button-blue" style="font-family:Avenir,Helvetica,sans-serif; border-radius:3px; color:#FFF; display:inline-block; text-decoration:none; background-color:#3097D1; border-top:10px solid #3097D1; border-right:18px solid #3097D1; border-bottom:10px solid #3097D1; border-left:18px solid #3097D1">Reset Password</a>
</p>



@stop
