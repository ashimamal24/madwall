@extends('admin.layout')
@section('title')
  Approved or Decline
@endsection
@section('css')

@endsection
@section('content')

	
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="caption" style="float:right">
                <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/view-job-detail/'.$jobseekers['job_id'])}}"> Back </a>
            </div>
           
            <div class="portlet-body util-btn-margin-bottom-5">
                {{ Form::open(array( 'method' => 'POST','url' => '/admin/edit-hour-rating','id'=>'edit-hour-rating-form' )) }}
	                <div class="row">
				        <div class="col-md-offset-2 col-md-4">
				        	@include('flash::message')
				            <div class="form-group">
				                <label for="total_hours_worked" class="control-label">Working Hours: </label> <span class="star">*</span> 
								@if(isset($jobseekers['total_hours_worked']))
                                    {{ Form::text("total_hours_worked",$jobseekers['total_hours_worked'],['class' => 'form-control proofs-value', 'maxlength'=> '2']) }}
                                @else
                                    {{ Form::text("total_hours_worked",null, ['class' => 'form-control proofs-value', 'maxlength'=> '2']) }}

                                @endif
				                <label class="help-block"></label>
				            </div>
				        </div>
				    </div>

				    <div class="row">
				        <div class="col-md-offset-2  col-md-4">
				            <div class="form-group status">
				               <label for="rating" class="control-label">Rating: </label> <span class="star">*</span>
								@if(isset($jobseekers['rating']))
                                    {{ Form::text("rating",$jobseekers['rating'],['class' => 'form-control proofs-value', 'maxlength'=> '4']) }}
                                    @else
                                    <?php
										$rating = null;
										if(isset($jobRating) && !empty($jobRating)){
											$rating = $jobRating;
										}
                                    ?>
                                    {{ Form::text("rating",$rating ,['class' => 'form-control proofs-value', 'maxlength'=> '4']) }}
                                @endif
				                <label class="help-block"></label>
				            </div>
				        </div>
				    </div>
				    <div class="row">
				        <div class="col-md-offset-2  col-md-4">
				            <div class="form-group status">
								{{ Form::button( 'Save', [ 'id'=>'edit-hours-rating-button','class' => 'btn btn-primary']) }}
				                <label class="help-block"></label>
				            </div>
				        </div>
				    </div>
                {{ Form::hidden( 'jobappid', $jobseekers['_id'],['id'=>'jobappid'] ) }}
                {{ Form::close() }}
            </div>
        </div>
    </div> 
</div>
@endsection 
@section('js')

<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>

<script>

jQuery(document).ready(function() {
    /* Approve Waiting Jobseeker*/

    $('#edit-hours-rating-button').click(function(){
 		var jobappid = $('#jobappid').val();
 		//alert(jobappid);
 		//return ;
        $("#edit-hour-rating-form").ajaxSubmit(
        {
            method      : 'post',
            beforeSend  : function() {
                addLoader();
            },
            url : path+'admin/edit-hour-rating',
            success     : function(data) {
                window.location = path+'admin/view-job-detail/jobseeker-hours-rating/'+jobappid;
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#addcategoryform .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                var j=0; 
                $.each( xhr.responseJSON, function( i, obj ) {
                
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                                
                    if( i=='category_id' ){
                        $('.category_id').addClass('has-error');
                        $('.category_id' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    if( i=='skills' ){
                        $('.skills').addClass('has-error');
                        $('.skills' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    
                    if(i.indexOf("certificate_file_url") >= 0){
                        var result = i.split('.');
                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').addClass('has-error');
                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').find('label.help-block').slideDown(400).html(obj);
                    } 
                });
            }
        });
    });
    
   
});
</script>

@endsection 
