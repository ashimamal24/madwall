@extends('admin.layout')
@section('title')
  Job Detail
@endsection 
@section('css')
<link href="{{ asset( 'public/admin/css/jquery-ui.css' ) }}" rel="stylesheet" type="text/css"/>
<!--<link href="{{ asset( 'public/admin/css/bootstrap-datetimepicker.css' ) }}" rel="stylesheet" type="text/css"/>-->

<link rel="stylesheet" type="text/css" href="{{asset('public/employer/js/date-timepicker/jquery.timepicker.css')}}" />
<link href="{{ asset( 'public/admin/css/admin_job.css' ) }}" rel="stylesheet" type="text/css"/>
<style>

</style>
@endsection
@section('content')
<h3 class="block" style="text-align:center">
<b><u>View Details</u></b>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="caption" style="float:right">
                <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/list-jobs')}}"> Back </a>
            </div>
            <div class="portlet-body util-btn-margin-bottom-5">
				@include('flash::message')
                <div class="tab-pane" id="tab_1">
                    <div class="portlet-body form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-4" style="width: 39%;">
                                        @php $skills =''; $subcat=''; @endphp
                                        <span style="color:#228B22; font-size:20px">{{ $job_data['title'] }}</span><br>
                                        <br>
                                        <span>Posted on {{Carbon\Carbon::parse($job_data['created_at'])->format('d')}}th {{Carbon\Carbon::parse($job_data['start_date'])->format('F')}}, {{Carbon\Carbon::parse($job_data['start_date'])->format('Y')}} By {{$employer['first_name']}} {{$employer['last_name']}}</span><br>
                                        <br>
                                        <span><b>Category :</b> {{ $job_data ['category'][0]['name'] }}</span><br>
                                        <br>
                                        <span><b>Subcategory :</b>@foreach ( $job_data['subcategory'] as $subcategory )
                                             @php $subcat.= $subcategory['name'].','; @endphp
                                             @endforeach
                                             {{trim($subcat,',')}}
                                             </span><br>
                                        <br>
                                        <span><b>Skills:</b> @foreach( $job_data['skill'] as $skill )
							                	@php $skills .= $skill['name'].",";@endphp
							                 @endforeach
							                 {{ trim($skills,',') }}
							            </span><br>
                                        <br>
                                        <span><b>Workers Required :</b> {{$job_data ['number_of_worker_required']}}  </span><br>
                                        <br>
                                        <!-- ------- method to hire remaining employees ----- -->
                                        @if($job_data ['job_published_type'] == 2)
                                        <span>
											<b>Method to hire remaining employees :</b> 
											@if($job_data['rehire_type'] == 1)
												Automatic
											@else
												Manual
											@endif
										</span><br>
                                        <br>
                                        @endif
                                        
                                        <!-- <span><b>Shift Time : </b>{{Carbon\Carbon::parse($job_data['start_date'])->format('d')}} th {{Carbon\Carbon::parse($job_data['start_date'])->format('M')}} - {{Carbon\Carbon::parse($job_data['end_date'])->format('d')}}th {{Carbon\Carbon::parse($job_data['end_date'])->format('M')}}</span><br>
                                        <span>{{Carbon\Carbon::parse($job_data['start_date'])->format('g:i A')}} - {{Carbon\Carbon::parse($job_data['end_date'])->format('g:i A')}}</span> -->
                                        
                                        @include('employer.promo.shift_data',['processData'=>$job_data['shifts']])
                                        
                                        
                                    </div>
                                    <div class="col-md-4" style="width: 28%;"> 
                                       
                                        <span><b>Job Description :</b> @if(isset($job_data ['description']))
											{!!html_entity_decode($job_data ['description'])!!}
                                        @else N/A @endif</span><br>
                                        <br>
                                        <span><b>Physical Requirement :</b> @if(isset($job_data ['physical_requirement']))
											{!!html_entity_decode($job_data ['physical_requirement'])!!}
                                        @else N/A @endif</span><br>
                                        <br>
                                        <!-- --------- Job Address ------------ -->
                                        <span><b>Job Address:</b> 
							                 {{ $job_data['address'] }}
							            </span><br>
                                        <br>
							            <!-- ---------- Meeting Location -------- -->
                                        <span><b>Meeting Location :</b> @if(isset($job_data ['meeting_location'])){{ ucwords($job_data ['meeting_location']) }}@else N/A @endif</span><br>
                                        <br>
                                        <!-- ----------- Safety hazards ----------- -->
                                        <span><b>Safety Hazards :</b> @if(isset($job_data ['safety_hazards']))
                                         {!!html_entity_decode($job_data ['safety_hazards'])!!}
                                       
                                        @else N/A @endif</span><br>
                                        <br>
                                        <span><b>Contact Name :</b> @if(isset($job_data ['contact_name'])){{ ucwords($job_data ['contact_name']) }}@else N/A @endif</span><br>
                                       
                                    </div>
                                    <div class="col-md-4" style="width: 33%;">
										<!-- ------------ Total hours worked ---------------- -->
                                        <span> <b>Total Hours Worked</b> :
                                        @php $total_hours = 0; @endphp
                                        <?php 
											if(isset($job_data ['total_work_hours'])){
                                                $total_hours = $job_data ['total_work_hours']; 
                                            } else {
                                                $total_hours = 0;
                                            }
                                        ?>
                                         {{$total_hours}}</span>
                                        <br>
                                        <br>
                                        <!-- -------------- Wage ------------ -->
                                        <span><b> Wage </b> : ${{number_format($job_data['salary_per_hour'],2)}}	</span>
                                        <br>
                                        <br>
                                        <!-- ------------ Commision --------------- -->
                                        <span><b> Comission (%) </b> : 
                                        <?php
                                        
											$price = ($job_data['salary_per_hour']  * $total_hours); 
											//$totalCommision = $commission['commision'];
											$totalCommision = $job_data['category_commision'];
											$commision = (($price * $totalCommision)/100);
                                        ?>
                                        {{$totalCommision}}%</span>
                                        <br>
                                        <br>
                                        <!-- ------------ Sub total --------------- -->
                                        <span><b> Sub-Total </b> : ${{$price}}</span>
                                        <br>
                                        <br>
                                        <!-- ------------ Total --------------- -->
                                        <span><b> Total </b> :
                                        
                                        ${{number_format($price + $commision,2)}} </span>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ------------- edit/delete job button ------------ -->
                        <br>
                        <div class="row">
							<div class="col-md-12">	
								<div class="col-md-12">		
									<div class="col-md-6">	
										@if($job_data['job_status'] != 4 && $job_data['job_status'] != 7)
										@if($job_edit['edit'] == true)
											<a href="{{ url('admin/editjob/'.$job_data['_id'])}}" class="btn btn-primary" style="">Edit Job</a>
										@endif
											
											<a href="javascript:void(0)" id="deleteJob" class="btn btn-primary" style="">Delete Job</a>
											<!-- ----- Display candidate list for rehire job ----- -->
											@if($job_data['job_published_type'] == 2)
												<input type="hidden" id="rehire_job_id" value="{{Crypt::encrypt($job_data['_id'])}}">
												<a style="margin-left:5px" class="btn btn-primary" href="javascript:void(0)" id="display_jobseeker_list" data-id="{{$job_data['_id']}}">Invite Employees</a>
											@endif
										@else
											<a style="margin-left:5px" class="btn btn-primary" href="{{url('admin/post-new-job/'.Crypt::encrypt($job_data['_id']))}}">Repost this job</a>
										@endif
										
									</div>
								</div>
							</div>
						</div>
                    </div>  
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-body">
                <div class="table-container">
					<input type="hidden" name="action" value="filter-job-detail/{{$job_data['_id']}}"/>
					<div class="table-actions-wrapper">
						<span>
						</span>
					</div>
					<div class="table-custom table-responsive">

						<table class="table table-striped table-bordered table-hover"  id="datatable_ajax_for_job_detail">
							<thead>
							<tr role="row" class="heading">
								<th width="10%">No.</th>
								<th width="10%">Profile Picture</th>
								<th width="10%">Candidate Name</th>
								<th class="10%">Certifications</th>
								<th class="10%">Download Resume</th>
								<th width="10%">Phone Number</th>
								<th width="10%">Number of hours worked </th>
								<th width="10%">Rating</th>
								<th width="30%">Action</th>
								<th width="30%">Timesheets</th>
								
							</tr>
							
							</thead>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- -------- modal popup to update rating from admin ------------ -->
	<div class="modal fade" id="updateApplicantRating" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">  
				<div class="modal-header">
					<h4>Employee Rating</h4>
				</div>   
				<div class="alert" id="rating_err" style="display:none"></div> 
				<div class="modal-body">
					<div class="row">			
						<div class="col-md-6">
							<div class="field_forms">
								<div class="label_form">
									<label>Rating</label>
								</div>
								
							</div>
						</div>					
						<div class="col-md-6">
							<div class="field_forms">
								<div class="form_inputs">
									<input id="applicant_id" name="applicant_id" value="" type="hidden">
									<input class="form-control" id="applicant_rating" name="applicant_rating" value="" type="text">
									<span class="error_msgg" style="display:none;"></span>
								</div>
							</div>
						</div>
					
					</div>
				</div>
				<div class="modal-footer">
					<button class="submit-button btn btn-primary" type="button" id="updateRatingjob">Save</button>					
					<button class="submit-button btn btn-primary" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
				</div>
			</div>
		</div>
	</div>





@include('models.delete_job')
@include('models.error_popup')
@include('models.rehire',$users)
@include('models.cancel_employee')
@include('models.jobseeker_docs')
@include('employer.promo.work_history_popup')
@endsection 
@section('js')
<script src="{{ asset( 'public/admin/js/mwjobdetail.js') }}"></script>
<!--<script src="{{ asset( 'public/admin/js/bootstrap-datepicker.js' ) }}" type="text/javascript"></script>-->
<!-- --------- datepicker js ------------ -->
<script src="{{ asset('public/employer/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- --------- timepicker js ------------ -->
<script type="text/javascript" src="{{asset('public/employer/js/date-timepicker/jquery.timepicker.js')}}"></script>
<script src="{{ asset( 'public/employer/js/employer_jobs.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/employer/js/job_detail.js' ) }}" type="text/javascript"></script>
<script>
$(document).ready(function(){
	
	$('.checker').removeClass('checker');
        
    $('.start_date').datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
		onClose: function(dateText, inst) { 
			$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
		} 
    });

 $('start_date').datepicker()
    .on('changeDate', function(ev){
           $('.datepicker').hide();
    });

    ///////////////////// code for datepickers start  ////////////////////////////////
            
    function formatDate(date) {
        var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return [year,month,day].join('-');
    }
    
    $('#registered_start_date').datepicker().on('changeDate', function(ev){
        var selected_date = new Date(ev.dates);
        var date = formatDate(selected_date);
        $('.datepicker').hide();
        $('.filter-submit').click();  
    });
    TableAjax.init();
    TableAjax.update();
    
    
    
    $(document).on('click','#deleteJob',function(){
		bootbox.confirm('Are you sure you want to delete this job',function(result){
			if(result){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				$.ajax({
					url:path+adminname+'/deletejob',
					type:'post',
					data:'value={{$job_data["_id"]}}&_token='+CSRF_TOKEN,
					dataType:'json',
					beforeSend:function(){
						Loader();
					},
					success:function(data){
						window.location.href=data.url;
					},
					error:function(errors){
						console.log(errors);
					},
					complete:function(){
						RemoveLoader()();
					}
				});
			}
		});
	});
	
	$('#display_jobseeker_list').click(function(){
		//$('#rehire_fr_job').modal('show');
	});

});
//delete shift
$(document).on('click','.delete-shift',function(){
	var url = $(this).attr('action-admin'); 
	bootbox.confirm("Are you want to remove selected shift?", function(result) {
		if(result){
			Loader();
		    $.ajax({
		            url: url,
		            type: 'post',
		            dataType: 'json',
		            data:  {"_token": "{{ csrf_token() }}"},
		            beforeSend:function(){
		                Loader();
		            },
		            success: function(data) {
		            	RemoveLoader();
						window.location.reload();
		            },
		            error: function(error) { 
		              RemoveLoader();
		              $('span.error_msg').hide();
		              $('span.error_msgg').hide();
		              /*var result_errors = error.responseJSON;
		              if(result_errors)
		              {
		                 $.each(result_errors, function(i,obj)
		                 {
		                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);

		                    if(i == 'token_mismatch')
		                        window.location.reload();
		                 }) 
		              }*/

		            },
		            complete: function() { //alert('b nn'); 
		            }
		          });		
		}
        
    });
	
});
</script>
@endsection 
