@extends('admin.layout')
@section('title')
  Post a new job
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('public/employer/css/datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('public/employer/css/developer.css') }}">

<link rel="stylesheet" type="text/css" href="{{asset('public/employer/js/date-timepicker/jquery.timepicker.css')}}" />

<!-- -------------- toggle css --------------- -->
<link href="{{ asset('public/employer/bootstrap-toggle-master/css/bootstrap-toggle.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('public/employer/text-editor/src/richtext.min.css') }}">
<link href="{{ asset( 'public/admin/css/chosen.css' ) }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset( 'public/admin/css/admin_job.css' ) }}" rel="stylesheet" type="text/css"/>

@endsection
@section('heading')
Post a job
@endsection
@section('content')

@php $serverdate = date('Y-m-d'); 
$lat = '';
$lng = '';
$startTime = '';
$endTime = '';
$id = '';
if(count($jobs)){
	$lat = $jobs->location['lat'];
	$lng = $jobs->location['lng'];
	$startTime = date('H:i',strtotime($jobs->start_date));
	$endTime = date('H:i',strtotime($jobs->end_date));
	$id = \Crypt::encrypt($jobs->_id);
}

@endphp

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="caption" style="float:right">
                <a style="background-color:#C23F44" class="btn btn-primary" href="{{url('admin/list-jobs')}}"> Back </a>
            </div>
           
            <div class="portlet-body util-btn-margin-bottom-5">
				@include('flash::message')
				{!! Form::model($jobs,['url'=>'admin/savejob','id'=>'addjob']) !!}

				@if(isset($repost_id) && !empty($repost_id))
					{!! Form::hidden('page','repost') !!}
					{!! Form::hidden('repost_id',$repost_id) !!}
				@else
					{!! Form::hidden('page','add') !!}
				@endif

				{!! Form::hidden('timezone',null,['id'=>'timezone']) !!}
				{!! Form::hidden('process_id',null,['id'=>'process_id']) !!}
				
				<!-- ----------- select employer -------------- -->
				<div class="row">
					<div class="col-md-4">
						<!-- ----------- job name ---------- -->
						<div class="field_forms">
							<div class="label_form">
								<label>Select Employer</label>

							</div>
							<div class="form_inputs">
								<?php
									$selectedEmployer = null;
									if(!empty($jobs->user_id)){
										$selectedEmployer = $jobs->user_id;
									}
								?>
								{!! Form::select('employer_id',[''=>'Please select employer']+$employers,$selectedEmployer,['class'=>'employer', 'id'=>'employer_id' ]) !!}
								
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
					</div>
				</div>

				<!-- ----------- job post form ---------------- -->
					@include('/employer/promo/job_post_form',['class'=>'form-control'])
					
					<div class="date_time_div_post_job">
						<div class="buttons">
							<button class="btn btn-primary" type="button" id="set_jobTime" data-status="1">Set Shift</button>
						</div>
					</div>
				<!-- ----------- ----------- -------------- ------ -->
				<div class="date_time_div_post_job">
					<div class="buttons">
						<br>
						<button class="btn btn-primary" type="button" id="automatichiring" data-status="1">Automatic Hiring</button>
						<button class="btn btn-primary" type="button" id="manulhiring" data-status="0">Manual Hiring</button>
						<button class="btn btn-primary" type="button" id="rehirejob" data-status="2" data-value="{{ Crypt::encrypt(1) }}">Rehire Employees</button>
					</div>
				</div>

				<input type="hidden" name="dates" id="dates">
				<input type="hidden" name="formatted_dates" id="formatted_dates">
				<input type="hidden" name="job_published_type" id="job_published_type">

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@include('models.emp_modals')

@endsection
	
@section('js')
<!-- --------- moment js ------------ -->
<script src="{{ asset('public/employer/js/moment.min.js') }}"></script>
<!-- --------- datepicker js ------------ -->
<script src="{{ asset('public/employer/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- --------- timepicker js ------------ -->
<script type="text/javascript" src="{{asset('public/employer/js/date-timepicker/jquery.timepicker.js')}}"></script>
<!-- --------- text editor js ------------ -->
<script src="{{ asset('public/employer/text-editor/src/jquery.richtext.min.js') }}" type="text/javascript"></script>
<!-- --------- chosen js ------------ -->
<script src="{{asset('public/admin/js/chosen.js')}}" type="text/javascript"></script> 
<!-- --------- google address api ------------ -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0Ucux0_UPErLjpzmwKqvnaD7yot5J08&amp;libraries=places"> </script>
<!-- --------- geocomplete js ------------ -->
<script src="{{asset('public/employer/js/jquery.geocomplete.js')}}" type="text/javascript"></script>  
<!-- --------- validate js ------------ -->  
<script src="{{asset('public/employer/js/jquery.validate.min.js')}}" type="text/javascript"></script>  
<!-- --------- toggle ---------- -->
<script src="{{asset('public/employer/bootstrap-toggle-master/js/bootstrap-toggle.js')}}"></script>
<!-- --------- common js ------------ --> 
<script src="{{ asset('public/employer/js/common.js') }}" type="text/javascript"></script>
<!-- --------- employer job js --------- --> 
<script src="{{ asset('public/employer/js/employer_jobs.js') }}" type="text/javascript"></script>

<!-- --------- createjob js --------- -->
<script src="{{ asset('public/employer/js/create_job.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>

<script>

	$(document).ready(function(){
		$('.checker').removeClass('checker');
		$('#category').chosen();
		$('#lunch_hour').chosen();
		$('#subcategory').chosen();
		$('#employer_id').chosen();
		
		//when employer is selected, auto-populate categories on behalf of employer selected
		$(document).on('change','#employer_id',function(){
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			$.ajax({
				url:path+'admin/employer-categories',
				type:'post',
				data:'employer_id='+$('#employer_id').val()+'&_token='+CSRF_TOKEN,
				dataType:'json',
				beforeSend:function(){
					Loader();
				},
				success:function(data){
					var data1 = '<option value="">Please select category</option>';
					if(data.category_data)
					{
						$.each(data.category_data, function(index, element) {
							data1 +='<option value="'+element._id+'"';							
							data1 += '>'+element.name+'</option>';
						});
					}
					$('#category').html(data1);
					//$('#multiselect option').prop('selected', true);
					$('#category').trigger("chosen:updated");
					$('#subcategory').html('');
					$('#subcategory').trigger("chosen:updated");
					$('#multiselect').html('');
					
					$('#multiselect').trigger("chosen:updated");
					//$('#rehire_fr_job').empty().html(data.user_list);
					$('#employer_id').parent('.form_inputs').find('.error_msgg').empty().hide();
					RemoveLoader();
				},
				error:function(errors){
					RemoveLoader();
				}
			});

		});
		
		
		
		
	});

</script>

@endsection 
