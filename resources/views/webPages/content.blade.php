<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MadWall | @if(count($cmspage) > 0)
					{{ucwords( $cmspage[0]['name'] )}}
					@endif</title>
	<link href="{{asset('public/employer/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('public/employer/css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('public/employer/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('public/employer/css/owl.carousel.css')}}" rel="stylesheet">
	<link  href="{{asset('public/employer/css/developer.css')}}" rel="stylesheet">
	<link  href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
	<link  href="{{asset('public/employer/css/intlTelInput.css')}}" rel="stylesheet">
	<!-- <link  href="{{asset('public/employer/css/demo.css')}}" rel="stylesheet"> -->
	<link  href="{{asset('public/employer/css/main.css')}}" rel="stylesheet">
	<script src="{{asset('public/employer/js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/bootstrap.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/owl.carousel.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/jquery.main.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/wow.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/jquery.malihu.PageScroll2id.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/intlTelInput.js')}}" type="text/javascript"></script>
	<link rel="shortcut icon" href="{{ url('public/logos/favicon.ico') }}"/>
	</head>
<body>
	<div class="loader-section">
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
	</div>
<div class="wrapper">
<div class="main"> 
<!-- Header section -->
	<!--<header class="header-main reset_password wow fadeIn ">-->
		<header class="header-main wow fadeIn dashboradbanner blog-head ">
		
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 colo-sm-3">
				<div class="logo"><a href="{{url('/')}}">
				<a href="{{url('/')}}"><img src="{{asset('public/employer/images/logo_dash.png')}}" alt=""></a>
				<!--             {{ Html::image('public/webpages/images/logo_dash.png', 'alt' ) }} -->
				</a></div>
				</div>
				<div class="col-lg-9 col-md-9 colo-sm-9"> <a href="#" class="drop-opener pull-right"> <span></span> <span></span> <em class="border"></em> </a>
					<div class="menu_bar">
						<div class="navSection">
							<div class="nav-holder" id="menu-drop">
							<!-- <ul>
							<li><a href="#how-jobseeker">How to apply</a></li>
							<li><a href="#how-hire">How to hire</a></li>
							<li><a href="{{url('/about-us')}}">About Us</a></li>
							<li><a href="#" data-toggle="modal" data-target="#contact">Contact us</a></li> 
							@if(Auth::user())

							<li class="navbtn">
							<ul>
							<li>
							<a href="{{ url('employer/dashboard') }}">Dashboard</a></li>  
							</li>
							</ul>
							<li class="navbtn"><a href="{{ url('employer/logout') }}">Logout</a></li>  
							@else
							<li class="navbtn"><a href="#" data-toggle="modal" data-target="#myModal">Sign up</a></li>
							@endif 
							</ul> -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="inner_banner_sec">
				<div class="row">
					<h1><b>@if(count($cmspage) > 0)
					{{ucwords( $cmspage[0]['name'] )}}
					@endif</b></h1>
				</div>
			</div>
		</div>
	</header>
	<!-- <header class="header-main wow fadeIn " id="top_page_banner">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 colo-sm-3">
					<div class="col-lg-9 col-md-9 colo-sm-9">
						<a href="javascript:void(0)" class="drop-opener pull-right">
						<span></span>
						<em class="border"></em>
						</a>
						<div class="menu_bar">
							<div class="navSection">
								<div class="nav-holder" id="menu-drop">
									<ul>
										@if(Auth::user())
										<li class="navbtn">
										<ul>
										<li>
										<a href="{{ url('employer/dashboard') }}">Dashboard</a></li>  
										</li>
										</ul>
										<li class="navbtn"><a href="{{ url('employer/logout') }}">Logout</a></li>  
										@else
										<!-- <li class="navbtn"><a href="#" data-toggle="modal" data-target="#myModal">Sign up</a></li> -->
										@endif
									<!-- </ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header> -->
	<div class="main_sub_sec">
		<div class="container">
		<div class="white_inner_sec wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
		<div class="row">
		<div class="col-md-12" id="content">
		<p>
			{!!html_entity_decode($cmspage[0]['content'])!!}
		</p>

		</div>
		</div>
		</div>
		</div>
	</div>
</div>

<!-- END CONTENT -->
<!-- footer section  -->
<footer class="footer-main">
<!-- <div class="container">
<div class="row">
<div class="col-md-3 col-sm-4">
<div class="footer-logo">
{{ Html::image('public/webpages/images/logo.png', 'alt' ) }}
</div>
</div>
<div class="col-md-9 col-sm-8">
<div class="footer-support-social">
<ul>
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
</ul>
</div>
<div class="footer-nav">
<ul>
@if(count($cmspage) > 0)
<li><a href="{{url('/about-us')}}">About us</a></li>
@endif
@if(count($cmspage) > 0)
<li><a href="{{url('/terms-condtion')}}">Terms &amp; conditions</a></li>
@endif
@if(count($cmspage) > 0)
<li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
@endif
</ul>
</div>
<div class="footer-copyright"> Copyright reserved by <span>MadWall</span> </div>
</div>
</div>
</div> -->
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-4">
<div class="footer-logo">
<a href="{{url('/')}}"> <img src="{{asset('public/employer/images/logo.png')}}" alt=""/></a>
</div>
</div>
<div class="col-md-9 col-sm-8">
<div class="footer-support-social">
<ul>
<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
<li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
</ul>
</div>
<div class="footer-nav">
<ul>
<li><a href="#" data-toggle="modal" data-target="#contact" >Contact us</a></li>

<li><a href="{{url('/about-us')}}">About MadWall</a></li>
<li><a href="{{url('/terms-condtion')}}">Terms &amp; Conditions</a></li>
<li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
<li><a href="#" data-toggle="modal" data-target="#doc">Documentation</a></li>
</ul>
</div>
<div class="footer-copyright">
Copyright reserved by <span>MadWall Inc</span>
</div>
</div>
</div>
</div>
<div class="modal fade signup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Sign up</h4>
</div>
<div class="modal-body">
<div class="form_signup">
<div class="row">
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Company Name">
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Email">
</div>
</div>
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Phone no">
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Email">
</div>
</div>
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Phone no">
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form_grp">
<input type="password" placeholder="Password">
</div>
</div>
<div class="col-md-6">
<div class="form_grp">
<input type="password" placeholder="Confirm Password">
</div>
</div>
</div>
</div>
<div class="form_signup">
<div class="row">
<div class="col-md-6">
<div class="form_grp">
<select class="customselect"  data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
<option>Type of Industry</option>
<option>Type of Industry</option>
<option>Type of Industry</option>
<option>Type of Industry</option>
</select>
</div>
</div>
<div class="col-md-6">
<div class="form_grp">
<select class="customselect"  data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
<option>No. of Workers.</option>
<option>No. of Workers.</option>
<option>No. of Workers.</option>
<option>No. of Workers.</option>
</select>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Contact Name">
</div>
</div>
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Contact No.">
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form_grp">
<input type="text" placeholder="Location">
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form_grp">
<input type="checkbox" id="signup_check" class="customCheckbox">
<label for="signup_check">I agree to the <a href="#">term & conditions</a> and <a href="#">privacy policy</a></label>
</div>
</div>
</div>
</div>
</div>
<div class="modal-footer signup_ftr">
<button type="submit">Submit</button>
</div>
</div>
</div>
</div>

<!--forgot popup-->
<div class="modal fade signup forgot" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
</div>
<div class="modal-body">
<div class="form_signup">
<div class="row">
<div class="col-md-12">
<div class="form_grp">
<input type="text" placeholder="E-mail">
</div>
</div>
</div>
</div>
</div>
<div class="modal-footer signup_ftr ">
<button type="submit">Submit</button>
</div>
</div>
</div>
</div>
<div class="modal fade signup forgot" id="doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title">Documents</h4>
</div>
<div class="modal-body">
<div class="documents">
<ul>
@foreach($documetation as $document)
<li><div class="doc_img"><i class="{{$document->extension}}" aria-hidden="true"></i>
</div> <h6>{{$document->doc_name}}</h6>
<a href="{{Url('public/documentation/').'/'.$document->doc_name}}" download><div class="button_download" ><button>Download</button></div></a>
</li>
@endforeach
</ul>
</div>
</div>
<div class="modal-footer signup_ftr">

</div>
</div>
</div>
</div>
</footer>
@include('models.contactus')
<!-- footer section end --> 
</div>
<script src="{{asset('public/employer/js/loader.js')}}" type="text/javascript"></script>    

	<script src="{{asset('public/employer/js/contactus.js')}}" type="text/javascript"></script>    

	<script type="text/javascript">

	wow = new WOW(
	{
		animateClass: 'animated',
		offset:       100,
		callback:     function(box) {
		console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
		}
	}
	);
	wow.init();
	</script>
	<script type="text/javascript">
	//var slots=<?php echo json_encode($cmspage[0]['content']); ?>;
   	//$('#content').html(slots);
	</script>
</body>
</html>
