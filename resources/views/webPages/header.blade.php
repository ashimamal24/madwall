<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MadWall | Home</title>
<link href="{{asset('public/employer/css/animate.css')}}" rel="stylesheet">
<link href="{{asset('public/employer/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('public/employer/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('public/employer/css/owl.carousel.css')}}" rel="stylesheet">
<link  href="{{asset('public/employer/css/developer.css')}}" rel="stylesheet">
<link  href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
<link  href="{{asset('public/employer/css/intlTelInput.css')}}" rel="stylesheet">
<!-- <link  href="{{asset('public/employer/css/demo.css')}}" rel="stylesheet"> -->
<link  href="{{asset('public/employer/css/main.css')}}" rel="stylesheet">
<script src="{{asset('public/employer/js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/jquery.main.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/wow.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/jquery.malihu.PageScroll2id.js')}}" type="text/javascript"></script>
<script src="{{asset('public/employer/js/intlTelInput.js')}}" type="text/javascript"></script>
<link rel="shortcut icon" href="{{ url('public/logos/favicon.ico') }}"/>
</head>

<body>
<div class="loader-section">

<div class="cssload-container">
  <div class="cssload-whirlpool"></div>
</div>
</div>

<div class="wrapper">

<div class="main">
<!-- Header section -->
<header class="header-main wow fadeIn " id="top_page_banner">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 colo-sm-3">
<div class="logo"><a href="{{url('/')}}"><img src="{{asset('public/employer/images/logo.png')}}" alt=""></a></div>
</div>
<div class="col-lg-9 col-md-9 colo-sm-9">
 <a href="javascript:void(0)" class="drop-opener pull-right">
                  <span></span>
                  <span></span>
                  <em class="border"></em>
                                   
                </a>
<div class="menu_bar">
<div class="navSection">
            <div class="nav-holder" id="menu-drop">
<ul>
<li><a href="#how-jobseeker">How to apply</a></li>
<li><a href="#how-hire">How to hire</a></li>
<li><a href="{{url('/about-us')}}">About Us</a></li>
<li><a href="#" data-toggle="modal" data-target="#contact">Contact us</a></li>
	
@if(Auth::user())
  
  <li class="navbtn">
  <ul>
    <li>
      <a href="{{ url('employer/dashboard') }}">Dashboard</a></li>  
    </li>
  </ul>
  <li class="navbtn"><a href="{{ url('employer/logout') }}">Logout</a></li>  
@else
  <li class="navbtn"><a href="#" data-toggle="modal" data-target="#myModal">Sign up</a></li>
@endif

</ul>
</div>
</div>

</div>
</div>
</div>
</div>
<div class="inner_banner_sec">
          <div class="row">

            <h1><b>@if(count($cmspage) > 0)
              {{ucwords( $cmspage[0]['name'] )}}
            @endif</b></h1>
          </div>
        </div>
        
</header>
        
