  <!-- footer section  -->
  <footer class="footer-main">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-4">
          <div class="footer-logo">
            {{ Html::image('public/webpages/images/logo.png', 'alt' ) }}
             </div>
        </div>
        <div class="col-md-9 col-sm-8">
          <div class="footer-support-social">
            <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
          <div class="footer-nav">
            <ul>
                @if(count($cmspage) > 0)
                    <li><a href="{{url('/about-us')}}">About us</a></li>
                @endif
                @if(count($terms) > 0)
                    <li><a href="{{url('/terms-condtion')}}">Term &amp; condition</a></li>
                @endif
                @if(count($cmspage) > 0)
                    <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                @endif
            </ul>
          </div>
          <div class="footer-copyright"> Copyright reserved by <span>MadWall</span> </div>
        </div>
      </div>
    </div>
    <div class="modal fade signup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Sign up</h4>
          </div>
          <div class="modal-body">
            <div class="form_signup">
              <div class="row">
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Company Name">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Email">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Phone no">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Email">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Phone no">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="password" placeholder="Password">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="password" placeholder="Confirm Password">
                  </div>
                </div>
              </div>
            </div>
            <div class="form_signup">
              <div class="row">
                <div class="col-md-6">
                  <div class="form_grp">
                    <select class="customselect"  data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
                      <option>Type of Industry</option>
                      <option>Type of Industry</option>
                      <option>Type of Industry</option>
                      <option>Type of Industry</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form_grp">
                    <select class="customselect"  data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
                      <option>No. of Workers.</option>
                      <option>No. of Workers.</option>
                      <option>No. of Workers.</option>
                      <option>No. of Workers.</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Contact Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Contact No.">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form_grp">
                    <input type="text" placeholder="Location">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form_grp">
                    <input type="checkbox" id="signup_check" class="customCheckbox">
                    <label for="signup_check">I agree to the <a href="#">term & conditions</a> and <a href="#">privacy policy</a></label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer signup_ftr">
            <button type="submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
    
    <!--forgot popup-->
    <div class="modal fade signup forgot" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
          </div>
          <div class="modal-body">
            <div class="form_signup">
              <div class="row">
                <div class="col-md-12">
                  <div class="form_grp">
                    <input type="text" placeholder="E-mail">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer signup_ftr ">
            <button type="submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- footer section end --> 
</div>
<script type="text/javascript">

   wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
</script>
</body>
</html>