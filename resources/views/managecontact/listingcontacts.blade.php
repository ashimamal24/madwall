@extends('admin.layout')
@section('title')
	Contact Us Queries	
@endsection
@section('content')
<h3 class="page-title">
Contact Us Queries
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-contacts' ) }}">Manage Contact Us Queries</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-body">
				<div class="table-container">
						<input type="hidden" name="action" value="filter-contacts"/>
						<div class="table-custom table-responsive">
							<table class="table table-striped table-bordered table-hover"  id="datatable_ajax_for_contacts">
								<thead>
								<tr role="row" class="heading">
									<th width="5%">No.</th>
									<th width="15%">Name</th>
									<th width="15%">Email</th>
									<th width="15%">Mobile Number</th>
									<th width="15%">Subject</th>
									<th width="15%">Message</th>
									<th width="15%">Attachment</th>
									<th width="25%">Date</th>
									<th width="25%">Request Status</th>
									<th width="25%">From</th>
							<!-- 		<th width="10%">Active/Inactive</th> -->
									<th width="15%">Actions</th>
								</tr>
								<tr role="row" class="filter">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										
									</td>
									<td>{{ Form::select('request_status', array(''=>'--select--','pending' => 'Pending', 'closed' => 'Closed' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
									
									<td>{{ Form::select('messagefrom', array(''=>'--select--','mobile' => 'Employees', 'website' => 'Employers', 'guest' => 'General' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
									<!-- <td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td> -->
									
									<td>
										<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
										<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
									</td>
								</tr>
								</thead>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END PAGE CONTENT-->
	@include('managecontact.popupcontact')
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwcontacts.js') }}"></script>
<script src="{{ asset( 'public/admin/js/bootstrap-datepicker.js' ) }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {

	$(document).on("click", "#view", function () {
        /***********user ajax view *******/
        var url_for_user_view = adminname+'/view-contact';
        var contactId = $(this).attr("contactId");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: contactId,_token:token},
            dataType: "JSON",
			success: function (result) {
				if ( result.status == 'error' ) {
				    bootbox.alert('some problem occur try again.....');
				} else {                   
				  
					if(result.username){
						$('#name').text(result.username);
				   	} else{
				   		$('#name').text('N/A');
				   	}

				   	if(result.guest){
						$('#name').text(result.reslutset.name);
						$('#email').text(result.reslutset.email);
				   	}

				   	if(result.email){
						$('#email').text(result.email);
				   	}
				   	
				   	if(result.reslutset._id){
						$('#query_id').val(result.reslutset._id);
				   	}else{
						$('#query_id').val('');
					}
				   	
				   	if(result.reslutset.subject){
						$('#subject').text(result.reslutset.subject);
				   	} else{
				   		$('#subject').text('N/A');
				   	}
				   	
				   	
				   	if(result.phone){
						$('#phone').text(result.phone);
				   	} else{
				   		$('#phone').text('N/A');
				   	}
				  
					if(result.reslutset.content){
						$('#content').text(result.reslutset.content);
					} else {
						$('#content').text('N/A');
					}

					if(result.reslutset.file_url){
						$('#file_url').html('<a href='+result.reslutset.file_url+'>'+result.reslutset.file_name+'</a>');
					} else {
						$('#file_url').text('N/A');
					}

					if(result.reslutset.type){
						switch (result.reslutset.type) {
						    case "mobile":
						        $('#from').text("Employee");
						        break;
						   case "website":
						        $('#from').text("Employer");
						        break;
						    case "guest":
						    	$('#from').text("General");
						        break;
						    default:
						        $('#from').text('NA');
						}
					} else {
						$('#from').text('NA');
					}
				   $('#myModal').modal('show');
				}
			}
        });
        /***********user ajax view ends here*******/

    });
	
	$('.start_date').datepicker( {
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months",
      onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            
        }    
	});

 $('start_date').datepicker()
    .on('changeDate', function(ev){
		   $('.datepicker').hide();
	  });


	$(document).on('click','#mark_closed',function(){
		var contactId = $('#query_id').val();
		$.ajax({
            url: path+'admin/mark-closed',
            type: "GET",
            data: {id: contactId},
            dataType: "JSON",
			success: function (result) {
				if(result.success == true){
					showSuccessMessage(result.message);					
				}else{
					showErrorMessage(result.message);
				}
				TableAjax.refresh();
				$('#myModal').modal('hide');
			},
			error : function(err){
				showErrorMessage('Something went wrong!!');
				TableAjax.refresh();
			}
		});
	});
	
	$('#myModal').on("hidden.bs.modal", function () {
		$('#query_id').val('');
	});


	  /////////////////////    code for datepickers start    ////////////////////////////////
			
	function formatDate(date) {
	    var d = new Date(date),
		month = '' + (d.getMonth() + 1),
	    day = '' + d.getDate(),
	    year = d.getFullYear();
	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	    return [year,month,day].join('-');
	}
	
	$('#registered_start_date').datepicker().on('changeDate', function(ev){
		var selected_date = new Date(ev.dates);
		var date = formatDate(selected_date);
		$('.datepicker').hide();
		$('.filter-submit').click();  
    });
      
	
             
   ////////////////////    code for timepicker end    ////////////////////////////////////

	
	TableAjax.init();
	TableAjax.update();
	

});
</script>

@endsection
