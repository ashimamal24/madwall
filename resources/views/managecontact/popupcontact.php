<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>View Contact Details</b></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Name: </th>
                                <td id="name"></td>
                            </tr>
                            <tr>
                                <th>Email: </th>
                                <td id="email"></td>
                            </tr>
                            <tr>
                                <th>Mobile Number: </th>
                                <td id="phone"></td>
                            </tr>
                            <tr>
                                <th>Subject: </th>
                                <td id="subject"></td>
                            </tr>
                            <tr>
                                <th>Message: </th>
                                <td id="content"></td>
                            </tr>
                             <tr>
                                <th>Attachment: </th>
                                <td id="file_url"></td>
                            </tr> 
                            <tr>
                                <th>From: </th>
                                <td id="from"></td>
                            </tr> 
                        </tbody>
                </div>
                </table>
            </div>
            <div class="modal-footer">
				<input type="hidden" name="query_id" id="query_id" value="">
                <button type="button" class="btn green" id="mark_closed">Close Query</button>
            </div>
        </div>
    </div>
</div>
