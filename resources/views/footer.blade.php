</div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="{{ asset( 'public/admin/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery-ui.min.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery-migrate.min.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/bootstrap.min.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery.blockui.min.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery.cokie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery.uniform.min.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/bootstrap-switch.min.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/moment.min.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/moment-timezone.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/metronic.js') }}" type="text/javascript"></script>

<script src="{{ asset( 'public/admin/js/waitMe.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/waitMe.min.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/quick-sidebar.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/demo.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery.fancybox.pack.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/wysihtml5-0.3.0.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/bootstrap-wysihtml5.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery.ui.widget.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/tmpl.min.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/load-image.min.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/canvas-to-blob.min.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/jquery.blueimp-gallery.min.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/jquery.iframe-transport.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/jquery.sparkline.min.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/inbox.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/index.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/bootbox.min.js' ) }}"></script>
<script src="{{ asset( 'public/admin/js/pnotify.all.min.js')}}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/jquery.dataTables.min.js' )}}" type="text/javascript" ></script>
<script src="{{ asset( 'public/admin/js/dataTables.bootstrap.js')}}" type="text/javascript" ></script>

<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/page.jumpToData().js" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/datatable.js' ) }}"></script>

<script src="{{ asset( 'public/admin/js/dataTables.tableTools.min.js' )}}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/dataTables.colReorder.min.js' )}}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/loader.js')}}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/common.js')}}" type="text/javascript"></script>
<script  src="{{ asset('public/employer/js/timezone.js') }}" type="text/javascript"></script>
<script  src="{{ asset('public/employer/js/cookies.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/custom.js' ) }}"></script>
<!--<script src="{{ asset( 'public/admin/js/path.js' ) }}"></script>-->
<script type="text/javascript">
var path = '{{ url("/") }}/';
var adminname = "admin";
</script>

<script>
	

	
	
	
	
jQuery(document).ready(function() {       
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
	$('#logout').click( function( e ){
		e.preventDefault();
		bootbox.confirm({
    	message: "Are you sure you want to logout?",
	    buttons: {
	        confirm: {
	            label: 'Yes',
	            className: 'btn-success'
	        },
	        cancel: {
	            label: 'No',
	            className: 'btn-danger'
	        }
	    },
	    callback: function (result) {
	    	if( result ){
				$('#logout-form').submit();
	    	}
	    }
});

	});
	
   	//TableAdvanced.init();

});

</script>

<script type="text/javascript">
	$(document).ready(function(){
        setTimeout(function() {
            $('.flashMessage').fadeOut('slow');
        }, 3000);

        setTimeout(function() {
        	$('.alert-danger').fadeOut('slow');
    	}, 3000);
    	
    });
	/*
	Added on : 29 july 2017
	Added by : shivani, Debut infotech
	DESC : to auto refresh page after 5 minutes of inactivity
	*/
    var time = new Date().getTime();
	$(document.body).bind("mousemove keypress", function(e) {
		time = new Date().getTime();
	});

	function refresh() {
		if(new Date().getTime() - time >= 600000) 
			window.location.reload(true);
		else 
			setTimeout(refresh, 10000);
	}
	setTimeout(refresh, 10000);
</script>
<script type="text/javascript">
setInterval(function(){
     //ajax call
    var url_for_user_view = adminname+'/notification';
    var token ="{{csrf_token()}}";
    var notification_data =[];
    $.ajax({
    	url: path+url_for_user_view,
    	type: 'get',
        data: {_token:token},
        beforeSend : function(){
			//removeLoader();
		},
        success: function(data) {
        	
			$('.badge-default').html(data.not_count);
			$('.bold').html(data.not_count+' Pending');
        	var notification = data.notification;
        		if(notification.length >0 ){
        			
        			notification.forEach(function(notification_alert, i) {
					for (var key in notification_alert){
						
						if(notification_alert.target==1){ //target=1 upload docs
							notification_data.push('<li ><a class="test" href="'+path+'admin/edit-approved-employee/' + notification_alert.from + '" data-attribute='+notification_alert._id+ '>'+notification_alert.title+'</a></li>');
						}
						if(notification_alert.target==2){ //target=1 upload docs
							notification_data.push('<li ><a class="test" href="'+path+'admin/list-employerwaitlist" data-attribute='+notification_alert._id+ '>'+notification_alert.title+'</a></li>');
						}
						if(notification_alert.target==12){ //target=12 upload new cv by candidate
							notification_data.push('<li ><a class="test" href="'+path+'admin/list-approvedemployee" data-attribute='+notification_alert._id+ '>'+notification_alert.title+'</a></li>');
						}
						if(notification_alert.target==3){ //target=3 contact us
							notification_data.push('<li ><a class="test" href="'+path+'admin/list-contacts" data-attribute='+notification_alert._id+ '>'+notification_alert.title+'</a></li>');
						}
						if(notification_alert.target==4){ //target=4 New jobseeker components
							notification_data.push('<li ><a class="test" href="'+path+'admin/list-waitingemployee" data-attribute='+notification_alert._id+ '>'+notification_alert.title+'</a></li>');							
						}
						if(notification_alert.target==5){ //target=5 jobseeker request for addtional time slot
							notification_data.push('<li ><a class="test" href="'+path+'admin/assign-time-slot/' + notification_alert.from + '" data-attribute='+notification_alert._id+ '>'+notification_alert.title+'</a></li>');
						}
					}
						jQuery.unique(notification_data);
						$('.scroller').html(notification_data);
					});
        		} else {
        			$('.scroller').html('<li class="no-notification">No Notification</li>');
        		}
	    },
    }); //data passed to controller
 },3000);
</script>
<script type="text/javascript">
	var url_for_user_view = adminname+'/check-notification';
	var token ="{{csrf_token()}}";
	$('.scroller').on('click', 'li a', function(e) {
    	var id = $(this).data("attribute");
		$.ajax({
			url: path+url_for_user_view,
			type: 'post',
			data: {_token:token,id:id},
			success: function(data) {
				console.log(data);
			}
    	});              
	});
	
	
		var tz = jstz.determine(); // Determines the time zone of the browser client
var test =  tz.name(); 
setCookie("client_timezone",test,365);
	
	
	
</script>

@yield('js')
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
