<div class="form-body">
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label( 'name', 'Name: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'name',null,['class' => 'form-control', 'maxlength'=> '40']) }}
                <label class="help-block"></label>
            </div>
        </div>
        
    </div>
    <div class="row">
        
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group file_url">
                {{ Form::label( 'file_url', 'File: ',['class' => 'control-label']) }} <span class="star">*</span>
                
                @if( isset ($edit_ginfo['general_file_name']) )
                
                <p class="hidden-file-name-readonly">Uploaded File : <a href={{$edit_ginfo['general_file_url']}}>{{ $edit_ginfo['general_file_name'] }}</a></p>
                @endif

                {{ Form::file( 'file_url', $attributes = array( 'class' => 'form-group', 'id' =>'file_url', 'accept'=> 'application/pdf' )) }}
                {{ Form::hidden('general_file_name',null,[ 'class' => 'hidden-file-name' ]) }}
                {{ Form::hidden('general_file_url',null,[ 'id' => 'hidden-file-url' ]) }}
              <label class="help-block"></label>
            </div>
        </div>
    </div>

    <div class="row">
         
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group status">
                {{ Form::label( 'status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span>
                @if(isset($edit_ginfo['status']))
                    @if($edit_ginfo['status'] == 0)
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),0,['class' => 'form-control']) !!}
                    @else
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),1,['class' => 'form-control']) !!}
                    @endif
                @else
                    {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) !!}
                @endif
              <label class="help-block"></label>
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
	
	 <div class="row">
		 <div class="col-md-offset-2 col-md-4" style="padding-left: 22px;">
			 
				{{ Form::button( $submitButtonText, ['id'=>'addginfo','class' => 'btn btn-primary']) }}
				{{ Html::link('admin/list-generalinfo', 'Cancel', array( 'class' => 'btn btn-primary' ))}}   
			<br><br>
			
		</div>
	</div>
	
	
     
    
    <div class="clearfix"></div>
</div>
<!-- /.col -->
