@extends('admin.layout')
@section('title')
	Edit App Document
@endsection

@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
@endsection

@section('heading')
	Edit App Document
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit App Document
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_ginfo, ['method' => 'POST','url' => '/admin/edit-generalinfo','id'=>'editginfo']) }}
				@include('generalinfo.generalinfoForm',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			
			{{ Form::hidden( 'idedit', $edit_ginfo['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')

<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>
<script>
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);
</script>

<script>
jQuery(document).ready(function() {   
	
	$(document).on('change','#file_url',function(e){
        
        var fileExtension = ['pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
        	return false;
        }

        addLoader();
        var file = $(this)[0].files;
        var type = file[0].type;
        $(this).closest('.file_url').find('.hidden-file-name-readonly').html("Uploaded File: "+file[0].name);
        $(this).closest('.file_url').find('.hidden-file-name').val(file[0].name);
        firebase_file_upload( file, type );
        
    });
    function firebase_file_upload( file,type ){
        var timestamp=Date.now();
        var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name);
        var metadata = {
            contentType: type,
        };
        
        var blob_image=new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
            $('#hidden-file-url').val(snapshot.downloadURL);
            removeLoader()
        }).catch(function(error) {
            removeLoader()
            console.log('firebase error occured:'+error);
            removeLoader();
        });
    }

	$('#addginfo').click(function(){
	var formData = new FormData($('#editginfo')[0]);
	var id = $('#idedit').val();
	$.ajax({
		dataType: 'json',
		method:'post',
		processData: false,
		contentType: false,
		url: path+'admin/edit-generalinfo/'+id,
		data: formData,
		beforeSend : function() {
			addLoader();
		},
		
		success  : function(data) {
			if( data.success == true ){
				window.location = path+'admin/list-generalinfo';
			}
			if( data.success == false ){
				window.location = path+'admin/list-generalinfo';
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			removeLoader();
			
			$("#editcms .form-group").removeClass("has-error");
			$(".help-block").hide();
			$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}

					if(i=='file_url'){
						$('.file_url').addClass('has-error');
						$('.file_url').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
	});
	
	});

});
</script>

@endsection	
