@extends('blogs.layouts.layout')
@section('title', 'Blog')
@section('content')
  
        <div class="dashboard_sec about">
          <div class="white_inner_sec wow fadeInUp">
            <div class="blog-tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                 
                  <li class="nav-item active">
                    <a class="nav-link dynamic_tabs" id="home-tab" data-toggle="tab"
                     data-value="all" href="javascript:void(0)" role="tab" aria-controls="home" aria-selected="true">All</a>
                  </li>
       
                  @foreach($blogCategory as $k=>$category)

                   <li class="nav-item" data-id="{{$category->_id}}">
                    <a class="nav-link dynamic_tabs" id="profile-tab"
                    data-id="{{$category->_id}}"  data-toggle="tab" href="javascript:void(0)" 
                      role="tab" aria-controls="profile" aria-selected="false">{{ucfirst($category->name)}}</a>
                   </li>

                  @endforeach

                </ul>
                   <span class="list-view"><i class="fa fa-list" title="List View"></i></span>
            </div>

            <div id="tag_container">
                @include('blogs.blog')
            </div>  
        </div>
      </div>
   
@endsection

@section('scripts')

<script type="text/javascript">

   $(document).ready(function(){
      $(".dynamic_tabs").click(function(e){
        e.preventDefault();
        $(".list-view .fa.fa-th-large").removeClass("fa-th-large");
        $(".list-view .fa.fa-list").removeClass("fa-list");
        $(".list-view .fa").addClass("fa-list");
         var category =$(this).attr("data-id");
         var page =1;
         getData(page,category);
      });

          
      $(document).on('click','#btn-more',function(e){
        e.preventDefault();
       //var id = $(this).data('id');
        var id = $(this).attr('data-id'); 
        var ref_this = $(".nav-tabs").find(".active");
        var category = ref_this.data("id");
     
        $("#btn-more").html("Loading....");
          $.ajax({
            url: 'load-post',
            method : "get",
            data : {id:id,category:category},
            dataType : "text",
            success : function (data){
              if(data != ''){

                $('#remove-row').remove();
                $("#load-data").append(data);
                var clName = $('.blog-class').attr('class').split(' ')[1];
                // alert(clName);
                $(".blog-content .blog-class").toggleClass(clName);
                $(".blog-content .blog-class").addClass(clName);

                if(clName =='col-md-12')
                {
                 $(".blog-box .blogi-img").addClass("change-image-grid-view");
                }else{
                  $(".blog-box .blogi-img").removeClass("change-image-grid-view");
                }

              }else{
                $('#btn-more').html("No More Blogs");
              }
            }
          });
      });  
   });

  function getData(page,category){  
    if(page && category !=""){
        $.ajax(
        {
          url: '?',
          data:{page:page,category:category},
          type: "get",
          datatype: "html"
        })
        .done(function(data)
        {
          $("#tag_container").empty().html(data);
           // location.hash = page;
        })

        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
          alert('No response from server');
        });
      }
  }
</script>

@endsection