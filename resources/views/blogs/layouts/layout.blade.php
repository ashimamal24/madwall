<!DOCTYPE html>
<html>
@include('blogs.layouts.head')
	<body>
		<div class="loader-section">
		  <div class="cssload-container">
		    <div class="cssload-whirlpool"></div>
		  </div>
		</div>
		<div class="wrapper">
		    <div class="main"> 
			    <!-- Header section -->
			    @include('blogs.layouts.header')
			    <!--content  -->
				<div class="main_sub_sec">
				    <div class="container">
			           @yield('content')
			        </div>
			    </div>
		    </div>
		</div>

		<div class="clearfix"></div>
		<!-- footer section  -->
		@include('blogs.layouts.footer')

		<script type="text/javascript">

			$.ajaxSetup({
			headers:
			{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
			});
			
		   wow = new WOW(
		      {
		        animateClass: 'animated',
		        offset:       100,
		        callback:     function(box) {
		          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
		        }
		      }
		    );
		    wow.init();

		</script>


<!-- copy -->

<script>
     	
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);

</script>
<script type="text/javascript">

  $(function(){
        $("#location").geocomplete({
          details: ".pickLocation",
          types: ["geocode", "establishment"],
        }).bind("geocode:result", function(event, result){
            var coor = result.geometry.location.lat();
            $('#key').val(1);
            //$('#pickup-location').val(coor); 
            $('.pickLocation').find('span.error_msgg').hide();  
          }).bind("blur", function(event, results){
            setTimeout(function(){
              if($('#key').val() == '')
              {
                $('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid company address.');
                $('#lat').val('');
                $('#lng').val('');
              }
              $('#key').val('');
            },1000);
            
          }) 
        ;
      });
</script>
<script type="text/javascript">

    var industry_detail=<?php echo json_encode($industries); ?>;
    //~ Object.keys(industry_detail).forEach(function(key) {
        //~ if( industry_detail[key].name ){
            //~ $('#data').append("<div class='col-md-4'><div class='inner_serves_full'><div class='img_div_serves'><img src="+window.location.href+"uploads/"+industry_detail[key].image+"></div><div class='description-div'><h6>"+industry_detail[key].name+"</h6><p id='indstry-description'>"+industry_detail[key].description+"</p></div></div></div>");
        //~ }
    //~ });

   wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
    
</script>

<script type="text/javascript">
jQuery("form.employer_login_form").validate({
    rules: {
      "email":{
         required:true 
      },
      "password":{
         required:true 
      },
    },
    messages: {
        "email":{
          required:"Please enter your registered Email."
        },
        "password":{
          required:"Please enter your Password."
        },
    },
    errorElement:'span',
    errorClass:'error_msg errormsges',
    submitHandler: function(form) {
      Loader();
      form.submit();
      //$('form.employer_login_form').submit();
    /*$.ajax({
            url: $('form.employer_login_form').attr('action'),
            type: 'post',
            dataType: 'json',
            data: $('form.employer_login_form').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
              if(data.status==1)
              {
                window.location.href=data.message;
              }
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.field-icon').find('span.error_msgg').slideDown(400).html(obj);
                 }) 
              }

            },
            complete: function() { //RemoveLoader() 
              }
          });*/
    }
});


$(document).on('click','#registrationEmployer',function(){
    if($('select[name="industry_type[]"]').val()==null)
    {
		$('.industrytype').find('span.error_msgg').slideDown(400).html("Please select at least one value in type of industries field.");
		$('.industrytype').find('span.error_msgg').show();
    }
    else
    {
		$('.industrytype').find('span.error_msgg').hide();
    }
    if($('#location').val())
    {
		if($('#lat').val() == '' || $('#lng').val() == '')
			$('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid location.'); 
    }

    
    if($('#phone_number').val() == '')
    {
		$('.phonenumber').find('span.error_msgg').slideDown(400).html('Please enter phone number.');
       //$('.companycontact').find('span.error_msgg').slideDown(400).html('Please enter contact number.'); 
    }
    else
    {
		$('.phonenumber').find('span.error_msgg').hide();
		$('.companycontact').find('span.error_msgg').hide();  
    }	

    $('#register').submit();
});




	/*
	DESC : to add new rule for password : 1 number, 1 special character, 1 alphabet
	9 aug 2017, shivani
	*/
	$.validator.addMethod("pwcheck",
		function(value, element) {
			return /^((?=(.*[0-9]){1,})(?=(.*[a-z]){0,})(?=(.*[A-Z]){1,})(?=(.*\W+){1,})(?=\S)).{6,25}$/.test(value);
		});




  jQuery("#register").validate({
    
        // Specify the validation rules
        rules: {
            "company_name": {
              required:true,
            },
            "first_name": {
              required:true,
            },
            "last_name": {
              required:true,
            },
            "email": {
              required:true,
              email : true
            },
            "email_confirmation": {
              required:true,
              equalTo: "#email_company"
            },
            "country_code": {
              required:true,
            },
            "company_code": {
              required:true,
            },
            "password": {
              required:true,
              pwcheck:true,
              minlength: 6,
              maxlength: 25,
            },
            "password_confirmation": {
              required:true,
              equalTo: "#password_register"
            },
            "company_address": {
              required:true,
            },
            "number_worker": {
              //required:true,
              number: true
            },
            "user_image": {
              //required:true,
            },
            "company_description":{
              //required:true,
              minlength : 10,
              maxlength : 10000
            },
            "agree":{
              required:true,
            }
    },
        
        // Specify the validation error messages
        messages: {
           "company_name": {
                required: "Please enter Company Name",
            },
            "first_name": {
                required: "Please enter Contact First Name",
            },
            "last_name": {
                required: "Please enter Contact Last Name ",
            },
            "email": {
                required: "Please enter your email ",
            },
            "password": {
                required: "Please enter password ",
                pwcheck : "Password must contain at least one uppercase letter, one lowercase letter, a number, a special character, and must be at least six characters long.",
            },
            "password_confirmation": {
                required: "Please enter confirm password ",
                equalTo: "Passwords do not match",
            },
            "email_confirmation": {
                required: "Please enter confirm email ",
                equalTo: "Emails do not match",
            },
            "company_address": {
                required: "Please enter company address ",
            },
            "number_worker": {
                required: "Please select at least one value in number of workers’ field. ",
            },
            
            "user_image":{
              required: "Please select images.",
            },
            "company_description":{
              required:"Please enter company description",
            },
            "country_code":{
              required:"Please enter country code",
            },
            "company_code":{
              required:"Please enter country code",
            },
            "agree":{
              required:"Please agree to the Terms and Conditions and Privacy Policy.",
            }
        },
        errorElement:'span',
        errorClass:'error_msg',
        errorPlacement: function(error, element) {
           // alert(element.attr("name"));
            $(element).parent('.form_grp').find('span.error_msgg').slideDown(400).html(error);
            if(element.attr("name") == "user_image"){
              $(element).closest('.form_grp').parent('.form-field').find('span.error_msgg').slideDown(400).html(error);
            }

            /*if(element.attr("name") == "phone"){
                $(element).closest('.country_codes').parent('.phonenumber').find('span.error_msgg').slideDown(400).html(error);
            }

            if(element.attr("name") == "company_contact"){
                $(element).closest('.country_codes').parent('.companycontact').find('span.error_msgg').slideDown(400).html(error);
            }*/

            if(element.attr("name") == "agree"){
                $(element).closest('span').parent('.agree').find('span.error_msgg').slideDown(400).html(error);
            }
			
            
        },

        submitHandler: function(form,e) {
			$('span.error_msg').hide();
            $('span.error_msgg').hide();
          /*$('form#register').submit();*/
          var form_data = new FormData($('form#register')[0]);
			
          $.ajax({
            url: $('form#register').attr('action'),
            type: 'post',
            dataType: 'json',
            data: form_data,
            contentType: false,
            processData: false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                Loader();
                $('#registration').prop( "disabled", true );
            },
            success: function(data) {
              if(data.status==1)
              {
                //$('#msg').html('<div class="alert alert-success"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</strong>.</div>');
                //setTimeout(function(){
                  $('#myModal').hide();
                  $('form#register')[0].reset();
                  window.location.reload();
                //},1000);

              }
              if(data.status==0)
              {
                $('#registration').prop( "disabled", false );
                $('#msg').html('<div class="alert alert-danger"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.errors+'</strong>.</div>');
              }
            },
            error: function(error) { 

              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              $('#registration').prop( "disabled", false );
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
            
                   	
                    if(i == 'industry_type')
                      $('.industrytype').find('span.error_msgg').slideDown(400).html(obj);

                  	if(i == 'country_code')
                      $('.phonenumber').find('span.error_msgg').slideDown(400).html(obj);

                  	if(i == 'company_code')
                      $('.companycontact').find('span.error_msgg').slideDown(400).html(obj);

                  	

                    if(i == 'agree')
                      $('.agree').find('span.error_msgg').slideDown(400).html(obj);  

                    if(i=='company_description')
                      $('.description').find('span.error_msgg').slideDown(400).html(obj);

                      if(i == "phone")
                         $('.phonenumber').find('span.error_msgg').slideDown(400).html(obj);
                      

                      if(i == "company_contact")
                        $('.companycontact').find('span.error_msgg').slideDown(400).html(obj);
                          
                      if(i == 'token_mismatch')
                        window.location.reload();

                 }) 
              }

            },
            complete: function() { RemoveLoader() }
          }); 
        }
    });


$(document).on('change','#user_image',function(e){

        var fileExtension = ['png','jpg','jpeg'];
        var file = $(this)[0].files;
        var type = file[0].type;
       
        // Check File Size
        if( file[0].size >5242880 ) {
            //bootbox.alert("Please upload a valid certificate within a max range of 5 MB");
            //updated : 4 aug 2017, shivani - to display error via popup instead of bootbox alert
            $('#file_error_message_here').html("Please upload a valid company logo within a max range of 5 MB");
            $(this).val('');
            $('#FileUploadError').modal('show');
           
        //   alert("Please upload a valid certificate within a max range of 5 MB");
            return false;
        }

        // Check File Type
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
           // bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            //updated : 4 aug 2017, shivani - to display error via popup instead of bootbox alert
            $('#file_error_message_here').html("Only "+fileExtension.join(', ')+ " format is allowed ");

              $(this).val('');

            $('#FileUploadError').modal('show');
			       
            
            
          //alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            return false;
        }
        Loader();
        $(this).closest('.certificate').find('.certificate_name').val(file[0].name);
        firebase_multiple_upload( file, type, $(this) );
    });
    
    
    
    //4 aug 2017, shivani- to resolve page scroll issue when an error is there for file uploads
    $("#FileUploadError").on("hidden.bs.modal", function () {
		//hide the image if already shown any.
		$("#preview_img").hide();
		$('#file_error_message_here').empty();
		$('body').addClass('modal-open');
	});
	//4 aug 2017, shivani- to remove all errors and inputs empty when signup modal is shown.
    $("#myModal").on("shown.bs.modal", function () {
		$(".has-error").empty().hide();
		$('input[name="email"]').val('');
		$('input[name="password"]').val('');
		$('input[name="password_confirmation"]').val('');
	});
    
    
    
    function firebase_multiple_upload( file,type ,reference ){

        var timestamp=Date.now();
        var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
        var metadata = {
            contentType: type,
        };
        
        var blob_image=new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
           // reference.closest('.certificate').find('.firebase_url').val(snapshot.downloadURL);
           $('#image').val(snapshot.downloadURL);
           //4 aug 2017, shivani - to preview img when it is uploaded
           $("#preview_img").find('img').removeAttr('src').attr('src',snapshot.downloadURL);
           $("#preview_img").show();
           
            //certificate.push(snapshot.downloadURL);
            //certificate['url'] = snapshot.downloadURL;
            removeLoader()
        }).catch(function(error) {
            console.log('firebase error occured:'+error);
            RemoveLoader();
        });
    }


    jQuery("form#forgotpassword").validate({
    rules: {
      "email":{
         required:true 
      }
    },
    messages: {
        "email":{
          required:"Please enter your registered Email."
        }
    },
    errorElement:'span',
    errorClass:'error_msg errormsges',
    submitHandler: function(form) {
      
      Loader();
    $.ajax({
            url: $('form#forgotpassword').attr('action'),
            type: 'post',
            dataType: 'json',
            data: $('form#forgotpassword').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
              $('form#forgotpassword')[0].reset();
              $('#forgot').modal('hide');
              window.location.reload();
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);

                    if(i == 'token_mismatch')
                        window.location.reload();
                 }) 
              }

            },
            complete: function() { //alert('b nn'); 
            }
          });
    }
});
</script>
<script type="text/javascript">
   $(".chosen-select").chosen({
        placeholder_text_single: "option",
        no_results_text: "Oops, nothing found!"
      });
   $(document).ready(function(){
      $('#multiselect_chosen').removeAttr('style');
   });

   $("#phone").intlTelInput({
      // allowDropdown: false,
       autoHideDialCode: false,
     // autoPlaceholder: "off",
      // dropdownContainer: "body",
       excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // initialCountry: "auto",
       nationalMode: false,
       	initialCountry: "ca",
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "public/employer/js/utils.js"
    });

    $("#company_code").intlTelInput({
       autoHideDialCode: false,
       nationalMode: false,
       	initialCountry: "ca",
       	excludeCountries: ["us"],
      utilsScript: "public/employer/js/utils.js"
    });


    $("#contact_number").mask("(999)-999-9999");
    $("#phone_number").mask("(999)-999-9999");
$('.default').removeAttr( 'style' );
  //  $('#location').keypress(function(e) {
     // alert(e.which);
  /*if (e.which == 8) {
      $('#lat').val('');
      $('#lng').val('');
  }*/
//});
   
</script>







<script>
	/*
	Added on : 29 july 2017
	Added by : shivani, Debut infotech
	DESC : to auto refresh page after 5 minutes of inactivity
	*/
     var time = new Date().getTime();
     $(document.body).bind("mousemove keypress", function(e) {
         time = new Date().getTime();
     });

     function refresh() {
         if(new Date().getTime() - time >= 600000) 
             window.location.reload(true);
         else 
             setTimeout(refresh, 10000);
     }
     
    function hide_message() {
        $('.flashMessage').empty().fadeOut();
    }
    
    
    /*
    Function to check notifications after every 5seconds
    */ 
	function check_notifications(){
		$.ajax({
            url: path+'employer/check-notification',
            type: 'post',
            dataType: 'json',
            data: $('form#forgotpassword').serialize(),
            beforeSend:function(){
                Loader();
            },
            success: function(data) {
              $('form#forgotpassword')[0].reset();
              $('#forgot').modal('hide');
              window.location.reload();
            },
            error: function(error) { 
              RemoveLoader();
              $('span.error_msg').hide();
              $('span.error_msgg').hide();
              var result_errors = error.responseJSON;
              if(result_errors)
              {
                 $.each(result_errors, function(i,obj)
                 {
                    $('input[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
                    if(i == 'token_mismatch')
                        window.location.reload();
                 }) 
              }
            },
            complete: function() { //alert('b nn'); 
            }
        });
	}

	setTimeout(refresh, 10000);
	//to hide success and error message after 3 seconds
	setTimeout(hide_message, 10000);

	//to check notifications
	setTimeout(check_notifications,5000);
</script>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '114032705934595'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=114032705934595&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

     </script>
		<!--  -->

		@yield ('scripts')

	</body>
</html>

