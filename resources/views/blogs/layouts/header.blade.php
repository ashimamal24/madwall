<header class="header-main wow fadeIn dashboradbanner blog-head">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="logo"><a href="{{url('/')}}"><img src="{{url('/public/employer/images/logo_dash.png')}}" alt=""></a></div>
            <a href="#" class="drop-opener pull-right"> <span></span> <span></span> <em class="border"></em> </a> </div>
         <div class="col-lg-9 col-md-9 colo-sm-9"> <a href="#" class="drop-opener pull-right"> <span></span> <span></span> <em class="border"></em> </a>
            <div class="menu_bar">
              <div class="navSection">
                <div class="nav-holder" id="menu-drop">
                  <ul>
                    <li><a href="{{url('/')}}" class="">How to apply</a></li>
                    <li><a href="{{url('/#how-hire')}}" class="">How to hire</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#contact">Contact us</a></li>


                    <li><a href="{{url('/blog')}}" class="mPS2id-highlight">Blog</a></li>

                    <li class="navbtn"><a href="#" data-toggle="modal" data-target="#myModal">Sign up</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="inner_banner_sec">
          <div class="row">
            <h1><b>Blog</b></h1>
          </div>
       </div>
      </div>
    </header>


    <div class="modal fade signup" id="myModal" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign up</h4>
      </div>
      <div class="modal-body">
        <div id="msg"></div>
        {!! Form::open(['url'=>'auth/employe/register','id'=>'register']); !!}
        <div class="form_signup">
          <h5>Log-in Credentials</h5><br>
          <div class="row">
            <div class="col-md-6">
              <div class="form_grp">
                {!! Form::text('company_name',null,['placeholder' => 'Company Name', 'maxlength'=>'50']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('company_name')) has-error @endif">   
                  @if ($errors->has('company_name')) 
                    <p class="text-danger">{{ $errors->first('company_name') }}</p> 
                  @endif
                </div>
              </div>
            </div>
          </div>
      
          <div class="row">
            <!-- --------- email --------- -->
            <div class="col-md-6">
              <div class="form_grp">
                {!! Form::text('email',null,['placeholder' => 'Email','id'=>'email_company','maxlength'=>'40']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('email')) has-error @endif">   
                  @if ($errors->has('email')) <p class="text-danger">{{ $errors->first('email') }}</p> @endif
                </div>
              </div>
            </div>
            
            
            <!-- ------------ confirm email ------------ -->
            
            <div class="col-md-6">
              <div class="form_grp">
                {!! Form::text('email_confirmation',null,['placeholder' => 'Confirm Email','maxlength'=>'40']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('email')) has-error @endif">   
                  @if ($errors->has('email_confirmation')) <p class="text-danger">{{ $errors->first('email_confirmation') }}</p> @endif
                </div>
              </div>
            </div>

          </div>
          
          
          
          <div class="row">
            <!-- ---------- password ----------- -->
            <div class="col-md-6">
              <div class="form_grp">
                {!! Form::password('password',['placeholder' => 'Password','id'=>'password_register']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('password')) has-error @endif">   
                  @if ($errors->has('password')) <p class="text-danger">{{ $errors->first('password') }}</p> @endif
                </div>
              </div>
            </div>
            <!-- -------- confirm password ----------- -->
            <div class="col-md-6">
              <div class="form_grp">
                {!! Form::password('password_confirmation',['placeholder' => 'Confirm Password']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('password_confirmation')) has-error @endif">   
                  @if ($errors->has('password_confirmation')) 
                    <p class="text-danger">{{ $errors->first('password_confirmation') }}</p> 
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form_signup">
          <h5>Company Details</h5><br>
          <div class="row">       
            <!-- --------- first name ------ -->
            <div class="col-md-6">
              <div class="form_grp">
                {!! Form::text('first_name',null,['placeholder' => 'Contact First Name', 'maxlength'=>'20']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('first_name')) has-error @endif">   
                  @if ($errors->has('first_name')) <p class="text-danger">{{ $errors->first('first_name') }}</p> @endif
                </div>
              </div>
            </div>
            <!-- ------- last name ------- -->
            <div class="col-md-6">
              <div class="form_grp">
                {!! Form::text('last_name',null,['placeholder' => 'Contact Last Name', 'maxlength'=>'20']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('last_name')) has-error @endif">   
                  @if ($errors->has('last_name')) <p class="text-danger">{{ $errors->first('last_name') }}</p> @endif
                </div>
              </div>
            </div>       
          </div>
          
          <div class="row">
            <!-- --------- company contact ---------- -->
            
            
            <div class="col-md-6 phonenumber">
              <div class="country_codes">
                <div class="form_grp number_add">
                  {!! Form::text('country_code',null,['placeholder' => 'Country Code','id'=>'phone','onkeydown'=>'return false']) !!}
                </div> 

                <div class="form_grp number_cus">
                  {!! Form::text('phone',null,['placeholder' => 'Contact Phone Number','id'=>'phone_number']) !!}
                </div>
              </div>
              <span class="error_msgg" style="display:none;"></span>
              <div class="@if ($errors->has('phone')) has-error @endif">   
                @if ($errors->has('phone')) <p class="text-danger">{{ $errors->first('phone') }}</p> @endif
              </div>
            </div>
            
            <!--<div class="col-md-6 companycontact">
              <div class="country_codes">
                <div class="form_grp number_add">
                  {!! Form::text('company_code',null,['placeholder' => 'Contry code','id'=>'company_code','onkeydown'=>'return false']) !!}
                </div>
                <div class="form_grp number_cus">
                  {!! Form::text('company_contact',null,['placeholder' => 'Contact number','id'=>'contact_number']) !!}
                </div>
              </div>
              <span class="error_msgg" style="display:none;"></span>
              <div class="@if ($errors->has('company_contact')) has-error @endif">   
                @if ($errors->has('company_contact')) <p class="text-danger">{{ $errors->first('company_contact') }}</p> @endif
              </div>
            </div>-->
            <!-- -------- company address ---------- -->
            <div class="col-md-6">
              <div class="form_grp pickLocation">
                 {!! Form::text('company_address',null,['id'=>'location','placeholder'=>'Company Address']) !!}
                 {!! Form::hidden('lat',null,['id'=>'lat']) !!}
                 {!! Form::hidden('lng',null,['id'=>'lng']) !!}
                 {!! Form::hidden('key',null,['id'=>'key']) !!}
                 {!! Form::hidden('register_key',Session::has('session_key')?Session::get('session_key'):0,['id'=>'key']) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('company_address')) has-error @endif">   
                  @if ($errors->has('company_address')) 
                    <p class="text-danger">{{ $errors->first('company_address') }}</p> 
                  @endif
                </div>
              </div>
            </div>       
          </div>
          
          
          <div class="row">
            <!-- ----------- industry ----------- -->
            <div class="col-md-6  industry">
              <div class="form_grp industrytype">
                {!! Form::select('industry_type[]',$industry,null,['class'=>'chosen-select','multiple'=>'multiple','id'=>'multiselect','style'=>'width:350px;','data-placeholder'=>"Type of Industries"]) !!}
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('industry_type')) has-error @endif">   
                  @if ($errors->has('industry_type')) 
                    <p class="text-danger">{{ $errors->first('industry_type') }}</p>
                  @endif
                </div>
              </div>
            </div>
            <!-- ----------- number of workers required --------- -->
            <!--<div class="col-md-6">
              <div class="form_grp">
                 Form::text('number_worker',null,['id'=>'number_worker', 'placeholder'=>'Number of Workers', 'maxlength' => '10']) 
                <span class="error_msgg" style="display:none;"></span>
                <div class="if ($errors->has('number_worker')) has-error endif">   
                  if ($errors->has('number_worker')) 
                    <p class="text-danger"> $errors->first('number_worker') </p> 
                  endif
                </div>
              </div>
            </div> -->
          </div>
          
          
          
          <div class="row">
            <!-- --------- terms and conditions -------- -->
            <div class="col-md-12">
              <div class="form_grp agree">
                {!! Form::checkbox('agree',true,false,['class'=>'customCheckbox','id'=>'signup_check']) !!} <label for="signup_check">I agree to the <a href="{{ url('terms-condtion') }}" target="_blank">Terms and Conditions</a> and <a href="{{ url('privacy-policy') }}" target="_blank">Privacy Policy</a></label>
                <span class="error_msgg" style="display:none;"></span>
                <div class="@if ($errors->has('agree')) has-error @endif">   
                  @if ($errors->has('agree')) 
                    <p class="text-danger">{{ $errors->first('agree') }}</p> 
                  @endif
                </div>
              </div>
            </div>       
          </div>
        </div>      
      </div>
      <div class="modal-footer signup_ftr">
        <button type="button" id="registrationEmployer">Submit</button>
      </div>       
      {!! Form::close(); !!} 
    </div>
  </div>
</div>