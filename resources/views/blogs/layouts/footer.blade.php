<footer class="footer-main">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4">
        <div class="footer-logo"> <a href="{{url('/')}}"><img src="{{url('/public/employer/images/logo.png')}}" alt=""/> </a></div>
      </div>
      <div class="col-md-9 col-sm-8">
        <div class="footer-support-social">
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
        <div class="footer-nav">
          <ul>
            <li><a href="#" data-toggle="modal" data-target="#contact">Contact us</a></li>
            <li><a href="{{url('/about-us')}}">About MadWall</a></li>
            <li><a href="{{url('/terms-condtion')}}">Term &amp; condition</a></li>
            <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
            <li><a href="javascript:void(0)" data-toggle="modal" data-target="#doc">Documentation</a></li>
            
          </ul>
        </div>
        <div class="footer-copyright"> Copyright reserved by <span>MadWall</span> </div>
      </div>
    </div>
  </div>
  <div class="modal fade signup forgot" id="automatic_hiring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <!--<h4 class="modal-title" id="myModalLabel">Forgot Password</h4>--> 
        </div>
        <div class="modal-body">
          <div class="form_signup">
            <div class="row">
              <div class="col-md-12">
                <div class="form_grp">
                  <p>Are you sureyou want to post this  job <br>
                    with <b>automatic hiring ?</b></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer signup_ftr ">
          <button type="submit">Hiring</button>
          <button type="submit" class="cancel_btn">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade signup forgot" id="manul_hiring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <!--<h4 class="modal-title" id="myModalLabel">Forgot Password</h4>--> 
        </div>
        <div class="modal-body">
          <div class="form_signup">
            <div class="row">
              <div class="col-md-12">
                <div class="form_grp">
                  <p>Are you sureyou want to post this  job <br>
                    with <b>manual hiring ?</b></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer signup_ftr ">
          <button type="submit">Hiring</button>
          <button type="submit" class="cancel_btn">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade signup forgot" id="Rehiring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Rehire for Job</h4>
        </div>
        <div class="modal-body">
          <div class="rehiring_div">
            <ul>
              <li>
                <div class="rehir_pic"><img src="images/img_rehire.jpg"  alt=""/></div>
                <div class="name_rating">
                  <p>John deo</p>
                  <div class="rating_rehire"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </div>
                  <div class="check_box_rating">
                    <input type="checkbox" id="rate_check">
                    <label for="rate_check"><i class="fa fa-check" aria-hidden="true"></i> </label>
                  </div>
                </div>
              </li>
              <li>
                <div class="rehir_pic"><img src="images/img_rehire.jpg" alt="" /></div>
                <div class="name_rating">
                  <p>John deo</p>
                  <div class="rating_rehire"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </div>
                  <div class="check_box_rating">
                    <input type="checkbox" id="rate_check1">
                    <label for="rate_check1"><i class="fa fa-check" aria-hidden="true"></i> </label>
                  </div>
                </div>
              </li>
              <li>
                <div class="rehir_pic"><img src="images/img_rehire.jpg"  alt=""/></div>
                <div class="name_rating">
                  <p>John deo</p>
                  <div class="rating_rehire"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </div>
                  <div class="check_box_rating">
                    <input type="checkbox" id="rate_check2">
                    <label for="rate_check2"><i class="fa fa-check" aria-hidden="true"></i> </label>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="modal-footer signup_ftr ">
          <button type="submit">reHire</button>
        </div>
      </div>
    </div>
  </div>
  <!--forgot popup--> 
  @include('models.contactus')

  <!-- ---- call helper function to get documentation details ---- -->
  <?php
  $documetation = Helper::doc_promo_footer();
  $industries = Helper::indstry_promo_footer();
  
  ?>
  <!-- ----------------------------------------------------------- -->
    <div class="modal fade signup forgot" id="doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Documents</h4>
                </div>
                <div class="modal-body">
                    <div class="documents">
                        <ul>
                            @foreach($documetation as $document)
                            <li><div class="doc_img"><i class="{{$document->extension}}" aria-hidden="true"></i>
                                </div> <h6>{{$document->doc_name}}</h6>
                                <a href="{{Url('public/documentation/').'/'.$document->doc_name}}" download><div class="button_download" ><button>Download</button></div></a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
          <button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php
if(isset($openDocument) && $openDocument == true){
?>
<script>

$(document).ready(function(){
$('#openDocuemnt').click();
});

</script>
<?php
}
?>

</footer>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0Ucux0_UPErLjpzmwKqvnaD7yot5J08&amp;libraries=places">
    </script>

<script src="{{asset('public/employer/js/jquery.geocomplete.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/jquery.validate.min.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/admin/js/chosen.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/loader.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/bootbox.min.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/jquery.maskedinput.js')}}" type="text/javascript"></script>    
<script src="{{asset('public/employer/js/contactus.js')}}" type="text/javascript"></script>    
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>

<script src="{{asset('public/employer/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script  src="{{ asset('public/employer/js/cookies.js') }}" type="text/javascript"></script>
<script  src="{{ asset('public/employer/js/timezone.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  var tz = jstz.determine(); // Determines the time zone of the browser client
var test =  tz.name(); 
setCookie("client_timezone",test,365);
</script>