
    <head>
       <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MadWall | @yield('title')</title>
        <link href="{{asset('public/employer/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('public/employer/css/bootstrap.css')}}" rel="stylesheet">

<link href="{{asset('public/employer/css/owl.theme.default.min.css')}}" rel="stylesheet">
<link  href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
<link  href="{{asset('public/employer/css/intlTelInput.css')}}" rel="stylesheet">

        <link href="{{asset('public/employer/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/employer/css/owl.carousel.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('public/employer/css/datepicker.css')}}">
        <link  href="{{asset('public/employer/css/main.css')}}" rel="stylesheet">
         <link  href="{{asset('public/employer/css/developer.css')}}" rel="stylesheet">
        
        <script src="{{asset('public/employer/js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/employer/js/owl.carousel.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/employer/js/bootstrap.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/employer/js/jquery.main.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/employer/js/wow.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/employer/js/jquery.malihu.PageScroll2id.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/employer/js/moment.min.js')}}"></script>


        <script src="{{asset('public/employer/js/intlTelInput.js')}}" type="text/javascript"></script>
       <link rel="shortcut icon" href="{{ url('public/logos/favicon.ico') }}"/>

        <script src="{{asset('public/employer/js/bootstrap-datepicker.js')}}" type="text/javascript"></script> 
        <script>
            $(document).ready(function(e) {
              $('#datepicker').datepicker({
                maxViewMode: 0,
              });
            });
        </script>
    </head>




