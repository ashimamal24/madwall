@if(!empty($blogDetail->comments))
     @foreach($blogDetail->comments as $k=>$comment)
     @if(!empty($comment))
     <div class="comment-box">
        <h4>{{ucwords($comment['first_name'])}} {{ucwords($comment['last_name'])}} <span>
          {{\Carbon\Carbon::parse($comment['created_at'])->format('d') . ' ' . 
          \Carbon\Carbon::parse($comment['created_at'])->format('M') . ' ' . 
          \Carbon\Carbon::parse($comment['created_at'])->format('Y')}}
          | {{ \Carbon\Carbon::parse($comment['created_at'])->format('g:i A')}}
      </span></h4>
        <p>{{$comment['comment']}}</p>
     </div>
          
        @if($comment->replies->count() < 5)
        <a href="javascript:void(0)" class="reply" data-id="reply_{{$comment['_id']}}"><span id="change_text_reply_{{$comment['_id']}}">Reply</span></a> <!-- | <a href="javascript:void(0)" class="cancel" data-id="reply_{{$comment['_id']}}">Cancel</a> -->
        @endif

              <form action="#" method="post" class="comment-form comment-form-reply" id="reply_{{$comment['_id']}}" style="display: none;">
               <p class="user-label comment-label">LEAVE A REPLY</p>
               <div class="row">

                  <div class="col-md-12 form-comment-{{$comment['_id']}}">
                     <p class="comment-form-comment">
                        <label for="comment">Comment <span aria-required="true">*</span></label>
                        <textarea  name="comment" rows="4" aria-required="true" class="form-control"></textarea>
                         <label class="help-block"></label>
                     </p>
                  </div>
                  <div class="col-md-6 form-comment-{{$comment['_id']}}">
                     <p class="comment-form-author">
                        <label for="name">First Name <span aria-required="true">*</span></label>
                        <input  name="first_name" type="text" size="30" aria-required="true" class="form-control">
                        <label class="help-block"></label>
                     </p>
                  </div>
                  <div class="col-md-6 form-comment-{{$comment['_id']}}">
                     <p class="comment-form-author">
                        <label>Last Name <span class="required" aria-required="true">*</span></label>
                        <input name="last_name" type="text" size="30" aria-required="true" class="form-control">
                        <label class="help-block"></label>
                     </p>
                  </div>
                  <div class="col-md-12">
                     <p class="form-submit">
                        <input type="button" name="submit" 
                          data-id={{$comment['_id']}}
                        value="Reply" class="button primary add_reply">
                     </p>
                  </div>
               </div>
            </form>
         
        @if($comment->replies)
        <div class="reply_last_{{$comment['id']}}">
          <!-- reply view -->
            @include('blogs.replies', ['replies' => $comment->replies])
          <!-- reply view -->
        </div>
        @endif
        <div class="clearfix"></div>
     @endif
  @endforeach
@endif