
@foreach($replies as $k=>$replay)
	<div class="comment-box-reply comment-box">

      <h4>{{ucwords($replay->first_name)}} {{ucwords($replay->last_name)}}<span>
          {{\Carbon\Carbon::parse($replay->created_at)->format('d') . ' ' . 
          \Carbon\Carbon::parse($replay->created_at)->format('M') . ' ' . 
          \Carbon\Carbon::parse($replay->created_at)->format('Y')}}
          | {{ \Carbon\Carbon::parse($replay->created_at)->format('g:i A')}}
      </span></h4>
        <p>{{$replay->reply}}</p>
     </div>
<!-- 
	</div> -->
@endforeach
