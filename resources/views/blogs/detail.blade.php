@extends('blogs.layouts.layout')
@section('title')
{{$blogDetail->title}}
@endsection
@section('content')
<div class="dashboard_sec about">
<ol class="breadcrumb blog-bread">
   <li class="breadcrumb-item"><a href="{{url('/blog')}}">Blog</a></li>
   <li class="breadcrumb-item active">{{$blogDetail->title}}</li>
</ol>
<div class="white_inner_sec wow fadeInUp blog-inner">
   <div class="blog-detail">
      <div class="blog-box">
         <h4>{{$blogDetail->title}}</h4>
         <h6> <i class="fa fa-calendar"></i>&nbsp; 
            {{\Carbon\Carbon::parse($blogDetail->created_at)->format('dS') . ' ' . 
            \Carbon\Carbon::parse($blogDetail->created_at)->format('M') . ' ' . 
            \Carbon\Carbon::parse($blogDetail->created_at)->format('Y')}}
         </h6>
         <div class="blogi-img">
            @if($blogDetail->blog_image)
            <img src="{{url('public/blog_images')}}/{{$blogDetail->blog_image}}" class="img img-responsive">
            @else
            <img src="{{url('public/blog_images/default.png')}}" class="img img-responsive">
            @endif
         </div>
         <div class="blogi-info">
            <p>{!!$blogDetail->blog_description!!}</p>
         </div>
         <p class="user-label">WRITTEN BY</p>
         <div class="blog-user">
            <div class="user-img">
               @if($blogDetail->author_image)
               <img src="{{url('/public/author_images')}}/{{$blogDetail->author_image}}" class="img img-responsive">
               @else
               <img src="{{url('public/blog_images/default.png')}}" 
                  class="img img-responsive">
               @endif
            </div>
            <div class="user-info">
               <h4>{{$blogDetail->author_name}}</h4>
               <p>{!!$blogDetail->author_description!!}</p>
            </div>
         </div>
         <form action="#" method="post" class="comment-form" id="comment_form">
            <p class="user-label comment-label">LEAVE A COMMENT</p>
            <div class="row">
               <input type="hidden" name="blog_id" id="blog_id" value="{{$blogDetail->_id}}"/> 
               <div class="col-md-12 form">
                  <p class="comment-form-comment">
                     <label for="comment">Comment <span aria-required="true">*</span></label>
                     <textarea id="comment" name="comment" rows="8" aria-required="true" class="form-control"></textarea>
                     <label class="help-block"></label>
                  </p>
               </div>
               <div class="col-md-6 form">
                  <p class="comment-form-author">
                     <label for="name">First Name <span aria-required="true">*</span></label>
                     <input id="name" name="first_name" type="text" size="30" aria-required="true" class="form-control">
                     <label class="help-block"></label>
                  </p>
               </div>
               <div class="col-md-6 form">
                  <p class="comment-form-author">
                     <label for="last">Last Name <span class="required" aria-required="true">*</span></label>
                     <input id="last" name="last_name" type="text" size="30" aria-required="true" class="form-control">
                     <label class="help-block"></label>
                  </p>
               </div>
               <div class="col-md-12">
                  <p class="form-submit">
                     <input type="button" name="submit" id="add_comment" class="button primary" value="Post Comment">
                  </p>
               </div>
            </div>
         </form>
         <span id="comment_flase_message"></span>
         <div class="blog-comment">
            <h3 id="comment_count">
               @if($blogDetail->comments_count()>0) 
               <span id="cmtcount">{{$blogDetail->comments_count()}}</span>  Comment(s)
               @else
               <span id="cmtcount"></span> <span id="cmtcount_message"></span>
               @endif 
            </h3>
            <div id="post-data">
               @include('blogs.comments')
            </div>
         </div>
         @if($blogDetail->comments_count()>0)
         <center>
            <div class="ajax-load text-center blog-comment">
            </div>
         </center>
         @endif
      </div>
   </div>
</div>
<!-- Model for reply limit -->
<div id="myModalReplyLimit" class="modal fade"  data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- dialog body -->
         <div class="modal-body">
            <center>
               <h4 id="replyLimitMessage"></h4>
            </center>
         </div>
         <!-- dialog buttons -->
         <div class="modal-footer"><button type="button" class="btn btn-primary modal_limit_button">OK</button></div>
      </div>
   </div>
</div>
<!--  -->
@endsection
@section('scripts')
<script type="text/javascript">
   var page = 1;
   $(window).scroll(function() {
       if($(window).scrollTop() + $(window).height() >= $(document).height()) {
          page++;
          loadMoreData(page);
       }
   });
   
   
  function loadMoreData(page){
    $.ajax(
          {
            url: '?page=' + page,
            type: "get",
            beforeSend: function(){
              $('.ajax-load').show();
            }
          })
          .done(function(data){
            if(data.html == ""){
              $('.ajax-load').html("No more comments found");
                return;
              }

              $('.ajax-load').hide();
              $("#post-data").append(data.html);
           })
           .fail(function(jqXHR, ajaxOptions, thrownError){
                alert('server not responding...');
          });
   }
   
   
   var path = '{{ url("/") }}/';
   
   $('#add_comment').click(function(){
   
      var blogId = $("#blog_id").val();
      var formData = new FormData($('#comment_form')[0]);
        
      $("#add_comment").attr("disabled", true);
      $("#add_comment").val("Sending...");
   
      $.ajax({
             processData: false,
             contentType: false,
             method      : 'post',
             cache  :false,
             url         : path+'blog/post_comment/'+blogId,
             data        : formData,
             beforeSend  : function() {
                // addLoader();
             },
             success     : (data)=> {
             
               $('#comment_form')[0].reset();
               $("#add_comment").attr("disabled", false);
               $("#add_comment").val("Post Comment");
              
              if(data.status ==true){
                 var str = $('#cmtcount').text();
                 if (str =='') {
                  $('#cmtcount').text('1');
                  $('#cmtcount_message').text('Comment(s)');
                 }else{
                  var newCmtCount = parseInt(str)+1;
                  $('#cmtcount').text(newCmtCount);
                 }
   
                  
                $('#post-data').fadeOut(1000, function () {
                
                  var html = '<div class="comment-box"><h4 style="text-transform: capitalize;">'+data.data.first_name+' '+data.data.last_name+'<span>'+data.data.date+'</span></h4><p>'+data.data.comment+'</p></div>'
                 
                  html += '<a href="javascript:void(0)" class="reply" data-id="reply_'+data.data._id+'"><span id="change_text_reply_'+data.data._id+'">Reply</span></a>';
                  
                  html += '<form action="#" method="post" class="comment-form comment-form-reply" id="reply_'+data.data._id+'" style="display: none;"><p class="user-label comment-label">LEAVE A REPLY</p><div class="row"><div class="col-md-12 form-comment-'+data.data._id+'"><p class="comment-form-comment"><label for="comment">Comment</label><textarea  name="comment" rows="4" aria-required="true" class="form-control"></textarea><label class="help-block"></label></p></div><div class="col-md-6 form-comment-'+data.data._id+'"><p class="comment-form-author"><label for="name">First Name <span aria-required="true">*</span></label><input  name="first_name" type="text" size="30" aria-required="true" class="form-control"><label class="help-block"></label></p></div><div class="col-md-6 form-comment-'+data.data._id+'"><p class="comment-form-author"><label>Last Name <span class="required" aria-required="true">*</span></label><input name="last_name" type="text" size="30" aria-required="true" class="form-control"><label class="help-block"></label></p></div><div class="col-md-12"><p class="form-submit"><input type="button" name="submit" data-id="'+data.data._id+'" value="Reply" class="button primary add_reply"></p></div></div></form><div class="reply_last_'+data.data._id+'"></div><div class="clearfix"></div>';
   
                   $('#post-data').prepend(html);        
                   $(this).fadeIn(1000);
   
                 });
   
                 $('html,body').animate({
                  scrollTop: $('#post-data').offset().top},
                  'slow',function(){
                 });
   
               }else{
                 alert('something went wrong please try again.');
               }
             },
             error       : function(xhr, ajaxOptions, thrownError) {
                // removeLoader();
                 $("#add_comment").attr("disabled", false);
                 $("#add_comment").val("Post Comment");
                 $( "#comment_form " ).removeClass( "has-error" );
                 $( ".help-block" ).hide();
                 
                 /* Start each loop*/
                 $.each( xhr.responseJSON, function( i, obj ) {
                 
                     $( 'input[name="'+i+'"]' ).closest( '.form').addClass('has-error');
                     $( 'input[name="'+i+'"]' ).closest( '.form').find('label.help-block').slideDown(400).html(obj);
                     $( 'textarea[name="'+i+'"]' ).closest( '.form').addClass('has-error');
                     $( 'textarea[name="'+i+'"]' ).closest( '.form').find('label.help-block').slideDown(400).html(obj);                           
                 
                 });  /* End each loop */
             } /* End error() */
         });
     });
   

  $(document).on('click', '.modal_limit_button', function(){
    $("#myModalReplyLimit").modal('hide');
    location.reload();
   
  })
   
   
  $(document).on('click', '.add_reply', function(){
    
    $formId = $(this).attr('data-id');
    var commentId = $(this).attr('data-id');
    var formData = new FormData($('#reply_'+$formId)[0]);
    $(".add_reply").attr("disabled", true);
    $(".add_reply").val("Sending...");
   
    $.ajax({
      processData: false,
      contentType: false,
      method      : 'post',
      cache  :false,
      url         : path+'blog/comment_reply/'+commentId,
      data        : formData,
      beforeSend  : function() {
                // addLoader();
      },
      success     : (data)=> {
        if(data.status ==true){ 
          if(data.message ==='Success'){
            $('#reply_'+$formId)[0].reset();
            $('#reply_'+$formId).hide();
            $('#change_text_reply_'+$formId).text('Reply');
            $(".add_reply").attr("disabled", false);
            $(".add_reply").val("Reply");

            $('#post-data').fadeOut(1000, function () {
            
              $(".reply_last_"+commentId).append('<div class="comment-box-reply comment-box"><h4 style="text-transform: capitalize;">'+data.data.first_name+' '+data.data.last_name+'<span>'+data.data.date+'</span></h4><p>'+data.data.reply+'</p></div>');
                   $(this).fadeIn(1000);
            });
   
            $('html,body').animate({
              scrollTop: $('.reply_last_'+commentId+' .comment-box-reply').offset().top},
              'slow',function(){
                   // alert('here');
            });
          }else{
            $('#reply_'+$formId)[0].reset();
            $('#reply_'+$formId).hide();
            $('#change_text_reply_'+$formId).remove();
            $('#myModalReplyLimit').modal('show')
            $('#replyLimitMessage').text('Sorry, maximum limit for the replies has been reached.');
          }
        }else{
          alert('something went wrong please try again.');
        }
      }, 
      error : (xhr, ajaxOptions, thrownError)=> {
        $(".add_reply").attr("disabled", false);
        $(".add_reply").val("Reply");
        // removeLoader();
        $( ".comment_form " ).removeClass( "has-error" );
        $( ".help-block" ).hide();
                 
         /* Start each loop with arrow function*/
         $.each( xhr.responseJSON,( i, obj )=> {
           
             var id = $(this).attr('data-id');
             $( 'input[name="'+i+'"]' ).closest('.form-comment-'+id).addClass('has-error');
             $( 'input[name="'+i+'"]' ).closest('.form-comment-'+id).find('label.help-block').slideDown(400).html(obj);
             $( 'textarea[name="'+i+'"]' ).closest('.form-comment-'+id).addClass('has-error');
             $( 'textarea[name="'+i+'"]' ).closest('.form-comment-'+id).find('label.help-block').slideDown(400).html(obj);                           
          });  /* End each loop */
      } /* End error() */
    });
  });
   
   
  //Show and hide reply form
  $(document).on('click', '.reply', function(){
     var formID = $(this).attr('data-id');
     if($(this).text() =='Reply'){
       var changeText = 'Cancel';
     }else{
       var changeText = 'Reply';
     }
     $('#change_text_'+formID).text(changeText);
     $("#"+formID).toggle('slow');
   })
    
</script>
@endsection