<div class="tab-content blog-content" id="myTabContent" >
<div class="tab-pane fade active in" id="home" role="tabpanel" aria-labelledby="home-tab">
   <div class="row" id="load-data">
      @if(!$blog->isEmpty())
      @foreach ($blog as $val)
      <div class="blog-class col-md-4">
         <div class="blog-box">
            @if($val->blog_image)
            <div class="blog-page blogi-img" style="background:url({{url('public/blog_images')}}/{{$val->blog_image}}) no-repeat center;">
               @else
               <div class="blog-page blogi-img" style="background:url({{url('public/blog_images/default.png')}}) no-repeat center;"> 
                  @endif
                  @if($val->blog_image)
                  <img src="{{url('public/blog_images')}}/{{$val->blog_image}}" class="img img-responsive">
                  @else
                  <img src="{{url('public/blog_images/default.png')}}" class="img img-responsive">
                  @endif 
               </div>
               <div class="clearfix"></div>
               <div class="blogi-info">
                  <h4>
                     <a href="{{url('/blog/details')}}/{{$val->_id}}">{{str_word_count(strip_tags($val->title)) >5? substr(strip_tags($val->title),0,30)."....":$val->title}}</a>
                  </h4>
                  <h6> <i class="fa fa-calendar"></i>&nbsp;
                     {{\Carbon\Carbon::parse($val->created_at)->format('dS') . ' ' . 
                     \Carbon\Carbon::parse($val->created_at)->format('M') . ' ' . 
                     \Carbon\Carbon::parse($val->created_at)->format('Y')}}
                  </h6>
                  <p>{!!str_word_count(strip_tags($val->blog_description)) >50? substr(strip_tags($val->blog_description),0,200)."....":$val->blog_description; !!}</p>
                  <a class="btn btn-primary" href="{{url('/blog/details')}}/{{$val->_id}}">Read More</a>
               </div>
            </div>
         </div>
         @endforeach
         @if(count($blog) >=3)
         <div class="clearfix"></div>
         <div id="remove-row" class="text-center">
            <button id="btn-more" data-id="{{ $val->_id }}" class="btn btn-primary nounderline mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" > Load More </button>
         </div>
         @endif
         @else
         <center>
            <p>Blog not found.</p>
         </center>
         @endif  
      </div>
   </div>
</div>