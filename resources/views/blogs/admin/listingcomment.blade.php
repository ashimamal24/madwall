@extends('admin.layout')
@section('title')
	Comments	
@endsection
@section('content')
<h3 class="page-title">
Comments
</h3><br>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/blogs/list' ) }}">@lang('Admin/breadcrumbs.blog')</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a>@lang('Admin/breadcrumbs.comments')</a>
		</li>
	</ul>
</div>

@include('flash::message')
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-body">
				<div class="table-container">

						<input type="hidden" name="action" value="blogs/filter-comments/{{ Request::segment(4) }}">
					
						<div class="table-custom table-responsive">
							<table class="table table-striped table-bordered table-hover"  id="comments">
								<thead>
								<tr role="row" class="heading">
									<th>No.</th>
									<th>UserName</th>
									<th style="width: 570px;">Comments</th>
									<th>Date</th>
									<th style="width: 100px;">Actions</th>
								</tr>
								<tr role="row" class="filter">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								</thead>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END PAGE CONTENT-->


<!--  -->


	<div class="modal fade" id="replyModal" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">  
				<div class="modal-header">
					<h4>Comment Reply</h4>
				</div>   
				
               <form id="reply-form">

				<div class="modal-body">
					<div class="row">							
						<div class="col-md-12">
							<div class="field_forms">
								<div class="form_inputs">
									  <div class="form-group">
										<input id="comment_id" name="comment_id" value="" type="hidden">
										<!-- <input class="form-control" id="applicant_rating" name="applicant_rating" value="" type="text"> -->
										<textarea placeholder="Reply..." rows="7" cols="68" name="reply"></textarea>
										 <label class="help-block"></label>
										</span>
									</div>
								   
								</div>
							</div>
						</div>
					
					</div>
				</div>
				<div class="modal-footer">
					<button class="submit-button btn btn-primary" type="button" id="reply-comment">Save</button>					
					<button class="submit-button btn btn-primary" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
				</div>

			  </form>

			</div>
		</div>
	</div>

@endsection
@section('js')
<script src="{{ asset('public/admin/js/mwcomments.js') }}"></script>
<script>

var segment = "{{request()->segment(4)}}";

jQuery(document).ready(function() {

	$(document).on("click", ".reply", function () {
		var commentId = $(this).attr('data-id');
        $("#comment_id").val(commentId);
	    $('#replyModal').modal('show'); 	
    });

    $(document).on("click", "#reply-comment", function () {
	     var formData = new FormData($('#reply-form')[0]);
	      formData.append('_token', '{{csrf_token()}}');
	 
        $.ajax({
            processData: false,
            contentType: false,
            method      : 'post',
            cache  :false,
            url         : path+'admin/blogs/comment_reply',
            data        : formData,
            beforeSend  : function() {
                addLoader();
            },
            success     : function(data) {
               window.location = path+'admin/blogs/comment-list/'+segment;
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#reply-comment .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                
                /* Start each loop*/
                $.each( xhr.responseJSON, function( i, obj ) {
                     
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                   
                }); /* End each loop */
            } /* End error() */
        });
    });


   
	/** Delete **/
	$(document).on( "click", "#delete-comment", function () {
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";

        bootbox.confirm("Are you sure you want to delete?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/blogs/comment_delete/'+id,
	         		type: "POST",
	         		 data: {id: id,_token:token},
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/blogs/comment-list/'+segment;
						}
						if( data.success == false ){
							window.location = path+'admin/blogs/comment-list/'+segment;
						}
					},
	         	});
            }
        });
    });
    
	TableAjax.init();
	TableAjax.update();
	
});
</script>

@endsection
