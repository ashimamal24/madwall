<div class="form-body">
   <div class="row">
      <div class="col-md-12">
         <div class="col-md-6">
            <div class="form-group category">
               {{ Form::label( 'blog_cat_id', 'Select Category: ',['class' => 'control-label'] ) }} <span class="star">*</span>
               @if(count($blogCategories)) 
               {{ Form::select('blog_cat_id', array_replace([''=>'Select'],$blogCategories), null, ['class' => 'form-control','id'=>''] ) }}
               @else
               {{ Form::select( 'blog_cat_id', [''=>'No Industry'], null, ['class' => 'form-control','id'=>''] ) }}
               @endif
               <label class="help-block"></label>
            </div>
         </div>
         <div class="col-md-6">
            <div class="form-group">
               {{ Form::label( 'title', 'Title of Blog: ',['class' => 'control-label']) }} <span class="star">*</span> 
               {{ Form::text( 'title',null,['class' => 'form-control']) }}
               <label class="help-block"></label>
            </div>
         </div>
          <div class="clearfix"></div>
         <div class="col-md-6">
            <div class="form-group">
               {{ Form::label( 'title', 'Blog Image: ',['class' => 'control-label']) }} 
               <div class="dropzone" id="blog_image"></div>
               {{ Form::hidden( 'blog_image',null,['class' => 'form-control','id'=>'blog_image_name']) }}
               <input type="hidden" name="is_blog_image" id="is_blog_image" value="">
            </div>
         </div>
      
         <div class="col-md-12">
            <div class="form-group">
               {{ Form::label('blog_description', 'Blog Content: ',['class' => 'control-label']) }} <span class="star">*</span>
               {{ Form::textarea('blog_description',null,['id'=>'description1', 'class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
               <label class="help-block"></label>
            </div>
         </div>
         <div class="col-md-6">
            <div class="form-group">
               {{ Form::label( 'author_name', 'Author Name: ',['class' => 'control-label']) }} <span class="star">*</span> 
               {{ Form::text( 'author_name',null,['class' => 'form-control']) }}
               <label class="help-block"></label>
            </div>
         </div>
         <div class="col-md-12">
            <div class="form-group">
               {{ Form::label('author_description', 'Author Info: ',['class' => 'control-label']) }} <span class="star">*</span>
               {{ Form::textarea('author_description',null,['id'=>'description2', 'class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
               <label class="help-block"></label>
            </div>
         </div>
         <div class="col-md-6">
            <div class="form-group">
               {{ Form::label( 'title', 'Author Image: ',['class' => 'control-label']) }} 
               <div class="dropzone" id="author_image"></div>
               {{ Form::hidden( 'author_image',null,['class' => 'form-control','id'=>'author_image_name']) }}
               <input type="hidden" name="is_author_image" id="is_author_image" value="">
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="box-footer">
   <div class="col-md-4"></div>
   <div class="col-sm-8">
      {{ Form::button( $submitButtonText, ['id'=>'addblog','class' => 'btn btn-primary']) }}
      {{ Html::link('admin/blogs/list', 'Cancel', array( 'class' => 'btn btn-primary' ))}}   
      <br> <br>
   </div>
   <div class="clearfix"></div>
</div>
<!-- /.col -->