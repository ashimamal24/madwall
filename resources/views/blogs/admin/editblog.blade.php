@extends('admin.layout')
 @section('title')
@lang('Admin/breadcrumbs.edit_blog')
@endsection('title')
@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
<link href="https://unpkg.com/dropzone/dist/dropzone.css" rel="stylesheet"/>
<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>

@endsection
@section('heading')
	@lang('Admin/breadcrumbs.edit_blog') <br/><br/>
@endsection
@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="{{ url( 'admin/blogs/list') }}">Dashboard</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="{{ url( 'admin/blogs/list' ) }}">@lang('Admin/breadcrumbs.blog')</a>
       <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a>@lang('Admin/breadcrumbs.edit_blog')</a>
    </li>
  </ul>
</div>
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Blog 
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $editBlog, ['method' => 'POST','url' => '/admin/edit-blog','id'=>'editblog']) }}
				@include('blogs.admin.blogform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			
			{{ Form::hidden( 'idedit', $editBlog['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/blog-editor-settings.js') }}" type="text/javascript"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.3/cropper.js"></script>
<script src="https://unpkg.com/dropzone"></script>


<script>
jQuery(document).ready(function() {     
	$('#addblog').click(function(){
	var formData = new FormData($('#editblog')[0]);
	var id = $('#idedit').val();
	$.ajax({
		dataType: 'json',
		method:'post',
		processData: false,
		contentType: false,
		url: path+'admin/blogs/update/'+id,
		data: formData,
		beforeSend : function() {
			addLoader();
		},
		
		success  : function(data) {
			if( data.success == true ){
				window.location = path+'admin/blogs/list';
			}
			if( data.success == false ){
				window.location = path+'admin/blogs/list';
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			removeLoader();
			
			$("#editblogcategory .form-group").removeClass("has-error");
			$(".help-block").hide();
			$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
				

          if(i=='blog_cat_id'){
            $('.category').addClass('has-error');
            $('.category').find('label.help-block').slideDown(400).html(obj);
          }

				});
			}
	});
	
	});

});

// blog_image
Dropzone.autoDiscover = false;
var c = 0;
var blogImageCropped = false;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


var myBlogImageDropzone = new Dropzone(
  //id of drop zone element 1
  '#blog_image', { 
      //autoProcessQueue: false,
      acceptedFiles: 'image/*',
      maxFilesize: 2, // MB
      dictDefaultMessage: 'Drag an image here to upload, or click to select one',
      url: path+'admin/blogs/save_blog_image',
       headers: {
              'x-csrf-token': CSRF_TOKEN,
          }, 

      init: function() { 

        var imgName =$("#blog_image_name").val();
        if(imgName !=""){
          var imgPath = path+"{{'public/blog_images/'}}"; 
          var mockFile = { name: imgName, size: 12345 };
          var myDropzone = this;
          myDropzone.options.addedfile.call(myDropzone, mockFile);
          myDropzone.options.thumbnail.call(myDropzone, mockFile, imgPath+imgName);
         }
      },
      success: function(file, response){
        if(response.status !='success'){
        alert('Something went wrong,please try again.');
        myBlogImageDropzone.removeFile(file);
        console.log(response);
        return false;
        }
      },
      error: function(file, response){
        alert(response);
        myBlogImageDropzone.removeFile(file);
        console.log(response)
        return false;
      }
  }
);


myBlogImageDropzone.on('addedfile', function(file) {
  if (!blogImageCropped) {
    myBlogImageDropzone.removeFile(file);
    cropper(file);
  } else {
   blogImageCropped = false;
   var previewURL = URL.createObjectURL(file);
   var dzPreview = $(file.previewElement).find('img');
   //console.log('sssssss',dzPreview);
   dzPreview.attr("src", previewURL);
    $('#is_blog_image').val('1');
    $('#blog_image').find('div.dz-preview.dz-image-preview').css("display", "none");
  }
  myBlogImageDropzone.processQueue();
});


var cropper = function(file) {
    var fileName = file.name;
    var loadedFilePath = getSrcImageFromBlob(file);
    // @formatter:off
    var modalTemplate =
      '<div class="modal fade" tabindex="-1" role="dialog">' +
      '<div class="modal-dialog" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>' +
      '</div>' +
      '<div class="modal-body">' +
      '<div class="cropper-container">' +
      '<img id="img-' + ++c + '" src="' + loadedFilePath + '" data-vertical-flip="false" data-horizontal-flip="false">' +
      '</div>' +
      '</div>' +
      '<div class="modal-footer">' +
      '<button type="button" class="btn btn-warning rotate-left"><span class="fa fa-rotate-left"></span></button>' +
      '<button type="button" class="btn btn-warning rotate-right"><span class="fa fa-rotate-right"></span></button>' +
      '<button type="button" class="btn btn-warning scale-x" data-value="-1"><span class="fa fa-arrows-h"></span></button>' +
      '<button type="button" class="btn btn-warning scale-y" data-value="-1"><span class="fa fa-arrows-v"></span></button>' +
      '<button type="button" class="btn btn-warning reset"><span class="fa fa-refresh"></span></button>' +
      '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
      '<button type="button" class="btn btn-primary crop-upload">Crop & upload</button>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>';
    // @formatter:on

  var $cropperModal = $(modalTemplate);

  $cropperModal.modal('show').on("shown.bs.modal", function() {

      var $image = $('#img-' + c);
      var cropper = $image.cropper({
           // aspectRatio: 16 / 9,
            autoCropArea: 1,
            movable: false,
            cropBoxResizable: true,
            minContainerWidth: 100,
            minContainerheight: 100,
            zoom: function (e) {
               var canvasData = $(this).cropper('getCanvasData');
               var cropBoxData = $(this).cropper('getCropBoxData');
  
               //when zoom out lessthn 1 prevent zoom out
              if (e.ratio < 2) { // Zoom out
                if (canvasData.width < cropBoxData.width || canvasData.height < cropBoxData.height) {
                  e.preventDefault(); // Prevent zoom out
                }
              }
           },
      }).on('hidden.bs.modal', function() {
          $image.cropper('destroy');
      });

  $cropperModal.on('click', '.crop-upload', function() {
          // get cropped image data
          myBlogImageDropzone.removeAllFiles();
          $image.cropper('getCroppedCanvas', {
        /*  width: 300,
          height: 300,*/

          }).toBlob(function(blob) {
            var croppedFile = blobToFile(blob, fileName);
            croppedFile.accepted = true;
            var files = myBlogImageDropzone.getAcceptedFiles();
           // myDropzone.removeFile(files);
            blogImageCropped = true;
             myBlogImageDropzone.processQueue();
             myBlogImageDropzone.addFile(croppedFile);
             $cropperModal.modal('hide');
             myBlogImageDropzone.processQueue();

          });
      }).on('click', '.rotate-right', function() {
          $image.cropper('rotate', 90);
      }).on('click', '.rotate-left', function() {
          $image.cropper('rotate', -90);
        })
        .on('click', '.reset', function() {
          $image.cropper('reset');
        })
        .on('click', '.scale-x', function() {
          if (!$image.data('horizontal-flip')) {
            $image.cropper('scale', -1, 1);
            $image.data('horizontal-flip', true);
          } else {
            $image.cropper('scale', 1, 1);
            $image.data('horizontal-flip', false);
          }
        })
        .on('click', '.scale-y', function() {
          if (!$image.data('vertical-flip')) {
            $image.cropper('scale', 1, -1);
            $image.data('vertical-flip', true);
          } else {
            $image.cropper('scale', 1, 1);
            $image.data('vertical-flip', false);
          }
        });

    });
  
  };


  function getSrcImageFromBlob(blob) {
    var urlCreator = window.URL || window.webkitURL;
    return urlCreator.createObjectURL(blob);
  }

  function blobToFile(theBlob, fileName) {
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
  }




//author_image
Dropzone.autoDiscover = true;
var c = 0;
var myAuthorImagecropped = false;

var myAuthorImageDropzone = new Dropzone('#author_image', {
     // autoProcessQueue: false,
      acceptedFiles: 'image/*',
      maxFilesize: 2, // MB
      dictDefaultMessage: 'Drag an image here to upload, or click to select one',
      url: path+'admin/blogs/save_author_image',
       headers: {
              'x-csrf-token': CSRF_TOKEN,
          },

      init: function() { 
        var imgName =$("#author_image_name").val();
      if(imgName !=""){
       
        var imgPath = path+"{{'public/author_images/'}}"; 
        var mockFile = { name: imgName, size: 12345 };
        var myDropzone = this;
        myDropzone.options.addedfile.call(myDropzone, mockFile);
        myDropzone.options.thumbnail.call(myDropzone, mockFile, imgPath+imgName);
      }

      },
       success: function(file, response){
        if(response.status !='success'){
        alert('Something went wrong,please try again.');
        myAuthorImageDropzone.removeFile(file);
        console.log(response);
        return false;
        }
      },
      error: function(file, response){
        alert(response);
        myAuthorImageDropzone.removeFile(file);
        console.log(response)
        return false;

      }


});



myAuthorImageDropzone.on('addedfile', function(file) {
        if (!myAuthorImagecropped) {
          myAuthorImageDropzone.removeFile(file);
          cropper2(file);
        } else {
         myAuthorImagecropped = false;
         var previewURL = URL.createObjectURL(file);
         //var dzPreview = $(file.previewElement).find('file');
          var dzPreview = $(file.previewElement).find('img');
           dzPreview.attr("src", previewURL);
           $('#is_author_image').val('1');
           $('#author_image').find('div.dz-preview.dz-image-preview').css("display", "none");
        }
        myAuthorImageDropzone.processQueue();
   });



var cropper2 = function(file) {
    var fileName = file.name;
    var loadedFilePath = getSrcImageFromBlob(file);
    // @formatter:off

       var modalTemplate =
      '<div class="modal fade" tabindex="-1" role="dialog">' +
      '<div class="modal-dialog" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>' +
      '</div>' +
      '<div class="modal-body">' +
      '<div class="cropper-container">' +
      '<img id="img-' + ++c + '" src="' + loadedFilePath + '" data-vertical-flip="false" data-horizontal-flip="false">' +
      '</div>' +
      '</div>' +
      '<div class="modal-footer">' +
      '<button type="button" class="btn btn-warning rotate-left"><span class="fa fa-rotate-left"></span></button>' +
      '<button type="button" class="btn btn-warning rotate-right"><span class="fa fa-rotate-right"></span></button>' +
      '<button type="button" class="btn btn-warning scale-x" data-value="-1"><span class="fa fa-arrows-h"></span></button>' +
      '<button type="button" class="btn btn-warning scale-y" data-value="-1"><span class="fa fa-arrows-v"></span></button>' +
      '<button type="button" class="btn btn-warning reset"><span class="fa fa-refresh"></span></button>' +
      '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
      '<button type="button" class="btn btn-primary crop-upload">Crop & upload</button>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>';
    // @formatter:on

  var $cropperModal2 = $(modalTemplate);


  $cropperModal2.modal('show').on("shown.bs.modal", function() {

      var $image = $('#img-' + c);
      console.log($image);
      var cropper2 = $image.cropper({
           // aspectRatio: 16 / 9,
          autoCropArea: 1,
          movable: false,
          cropBoxResizable: true,
          minContainerWidth: 100,
          minContainerheight: 100,
          zoom: function (e) {
               var canvasData = $(this).cropper('getCanvasData');
               var cropBoxData = $(this).cropper('getCropBoxData');
    
               //when zoom out lessthn 1 prevent zoom out
              if (e.ratio < 2) { // Zoom out
                if (canvasData.width < cropBoxData.width || canvasData.height < cropBoxData.height) {
                  e.preventDefault(); // Prevent zoom out
                }
              }
           },
        })
        .on('hidden.bs.modal', function() {
          $image.cropper2('destroy');
        });


  $cropperModal2.on('click', '.crop-upload', function() {
     
          // get cropped image data
          myAuthorImageDropzone.removeAllFiles();
          $image.cropper('getCroppedCanvas', {
        /*  width: 300,
          height: 300,*/


          }).toBlob(function(blob) {
            var croppedFile = blobToFile(blob, fileName);
            croppedFile.accepted = true;
            var files = myAuthorImageDropzone.getAcceptedFiles();
          
           // myDropzone.removeFile(files);
            myAuthorImagecropped = true;
             myAuthorImageDropzone.processQueue();
             myAuthorImageDropzone.addFile(croppedFile);
             $cropperModal2.modal('hide');
             myAuthorImageDropzone.processQueue();

          });
        })   .on('click', '.rotate-right', function() {
          $image.cropper('rotate', 90);
        })
        .on('click', '.rotate-left', function() {
          $image.cropper('rotate', -90);
        })
        .on('click', '.reset', function() {
          $image.cropper('reset');
        })
        .on('click', '.scale-x', function() {
          if (!$image.data('horizontal-flip')) {
            $image.cropper('scale', -1, 1);
            $image.data('horizontal-flip', true);
          } else {
            $image.cropper('scale', 1, 1);
            $image.data('horizontal-flip', false);
          }
        })
        .on('click', '.scale-y', function() {
          if (!$image.data('vertical-flip')) {
            $image.cropper('scale', 1, -1);
            $image.data('vertical-flip', true);
          } else {
            $image.cropper('scale', 1, 1);
            $image.data('vertical-flip', false);
          }
        });

    });
  
  };

</script>

@endsection	
