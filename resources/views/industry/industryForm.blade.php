<div class="form-body">
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group industry">
				{{ Form::label('name', 'Name: ',['class' => 'control-label'] ) }} <span class="star">*</span> 
                {{ Form::text( 'name' , null, ['class' => 'form-control', 'maxlength'=> '40' ] ) }}

               
                <label class="help-block"></label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group file_url">
                {{ Form::label( 'image', 'File: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::file( 'image', $attributes = array( 'class' => 'form-group', 'id' =>'image' )) }}
                {{ Form::hidden('file_name',null,[ 'class' => 'file-name' ]) }}
                <label class="help-block"></label>
                
            </div>
            <div>
            <div class="img_show_divE">
				@if( isset ($edit_industry['image']) )
				
					@if(file_exists('uploads/'.$edit_industry['image']))
						<img src="{{ asset('uploads/'.$edit_industry['image']) }}" width="100" height="100" class="img-circle industry-image-preview remove_this" alt="User Image"/>
					@else
						<img src="{{$edit_industry['image']}}" width="100" height="100" class="img-circle industry-image-preview remove_this" alt="User Image"/>
					@endif
					<img src="" width="100" height="100" style="display:none" class="img-circle industry-image-preview" alt="User Image"/>
                @endif
				</div>
                
                
                <div class="img_show_div">
					<img src="" width="100" height="100" style="display:none" class="img-circle industry-image-preview" alt="User Image"/>
                </div>
                <br><br>
                <div class="img_clear_btn">					 
					<a class="btn btn-primary" id="clear-preview" style="display:none" >Clear</a>
                </div>
				
            </div>
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-offset-2 col-md-4">
           <div class="form-group">
                {{ Form::label('description', 'Description: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::textarea('description',null,['id'=>'description', 'class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
                <label class="help-block"></label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group status">
               {{ Form::label('status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span>
                @if(isset($edit_industry['status']))
                    @if($edit_industry['status'] == 0)
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),0,['class' => 'form-control']) !!}
                    @else
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),1,['class' => 'form-control']) !!}
                    @endif
                @else
                    {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) !!}
                @endif
            <label class="help-block"></label>
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="col-sm-4"></div>
    <div class="col-sm-6">
        {{ Form::button( $submitButtonText, ['id'=>'addindustry','class' => 'btn btn-primary']) }}
        {{ Html::link( 'admin/list-industries', 'Cancel', array( 'class' => 'btn btn-primary' ))}}
    <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
