@extends('admin.layout')
@section('title')
    Add Blog Category    
@endsection
@section('heading')
    Add Blog Category<br/><br/>
@endsection
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{ url( 'admin/blog-category/list' ) }}">@lang('Admin/breadcrumbs.blog_category')</a>
             <i class="fa fa-angle-right"></i>
        </li>
         <li>
            <a>@lang('Admin/breadcrumbs.add_blog_category')</a>
        </li>
    </ul>
</div>

<div class="tab-pane" id="tab_1">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Add Category
            </div>
        </div>
        <div class="portlet-body form">
            @include('errors.user_error')
            @include('flash::message')
            @if (count($errors) > 0)
            @endif
            {{ Form::open(array( 'method' => 'POST','url' => '/admin/add-category','id'=>'addblogcategoryform')) }}
                @include('blog-category.blogcategoryform',['submitButtonText' => 'Add Blog Category'])
            {{ Form::close() }}
        </div>  
    </div>
</div>

@endsection 
@section('js')
<script>
jQuery(document).ready(function() {     
    $('#addblogcategory').click(function(){
        var formData = new FormData($('#addblogcategoryform')[0]);
        $.ajax({
            processData: false,
            contentType: false,
            method      : 'post',
            cache  :false,
            url         : path+'admin/blog-category/store',
            data        : formData,
            beforeSend  : function() {
                addLoader();
            },
            success     : function(data) {
                window.location = path+'admin/blog-category/list';
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#addcategoryform .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                
                /* Start each loop*/
                $.each( xhr.responseJSON, function( i, obj ) {
                
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);

                    if( i=='type' ){
                        $('.type').addClass('has-error');
                        $('.type').find('label.help-block').slideDown(400).html(obj);
                    }
                    if( i=='industry_id' ){
                        $('.industry').addClass('has-error');
                        $('.industry' ).find('label.help-block').slideDown(400).html(obj);
                    }
                    
                    if( i=='status' ){
                        $('.status').addClass('has-error');
                        $('.status').find('label.help-block').slideDown(400).html(obj);
                    }
                }); /* End each loop */
            } /* End error() */
        });
    });


    

    $('#industry_id').change(function(){
        $('#industry_name').val($('#industry_id').find(":selected").text() );
    });
    

});
</script>

@endsection 
