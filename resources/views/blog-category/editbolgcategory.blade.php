@extends('admin.layout')
@section('title')
	@lang('Admin/breadcrumbs.edit_blog_category')
@endsection
@section('heading')
	@lang('Admin/breadcrumbs.edit_blog_category')<br/><br/>
@endsection
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{ url( 'admin/blog-category/list' ) }}">@lang('Admin/breadcrumbs.blog_category')</a>
             <i class="fa fa-angle-right"></i>
        </li>
         <li>
            <a>@lang('Admin/breadcrumbs.edit_blog_category')</a>
        </li>
    </ul>
</div>
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Blog Category
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $editBlogCategory, ['method' => 'POST','url' => '/admin/edit-skill','id'=>'editblogcategory']) }}
				@include('blog-category.blogcategoryform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			
			{{ Form::hidden( 'idedit', $editBlogCategory['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script>
jQuery(document).ready(function() {     
	$('#addblogcategory').click(function(){
	var formData = new FormData($('#editblogcategory')[0]);
	var id = $('#idedit').val();
	$.ajax({
		dataType: 'json',
		method:'post',
		processData: false,
		contentType: false,
		url: path+'admin/blog-category/update/'+id,
		data: formData,
		beforeSend : function() {
			addLoader();
		},
		
		success  : function(data) {
			if( data.success == true ){
				window.location = path+'admin/blog-category/list';
			}
			if( data.success == false ){
				window.location = path+'admin/blog-category/list';
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			removeLoader();
			
			$("#editblogcategory .form-group").removeClass("has-error");
			$(".help-block").hide();
			$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
	});
	
	});

});
</script>

@endsection	
