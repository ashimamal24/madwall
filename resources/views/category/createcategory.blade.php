@extends('admin.layout')
@section('title')
	Add Category	
@endsection
@section('heading')
	Add Category
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Add Category
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			@if (count($errors) > 0)
		    @endif
			{{ Form::open(array( 'method' => 'POST','url' => '/admin/add-category','id'=>'addcategoryform')) }}
				@include('category.categoryform',['submitButtonText' => 'Add Category'])
		    {{ Form::close() }}
		</div>	
	</div>
</div>

@endsection	
@section('js')
<script>
jQuery(document).ready(function() {     
	$('#addcategory').click(function(){
		var formData = new FormData($('#addcategoryform')[0]);
		$.ajax({
			processData: false,
    		contentType: false,
			method 		: 'post',
			cache  :false,
			url			: path+'admin/add-category',
			data 		: formData,
			beforeSend	: function() {
				addLoader();
			},
			success		: function(data) {
				window.location = path+'admin/list-categories';
			},
			error 		: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$( "#addcategoryform .form-group" ).removeClass( "has-error" );
				$( ".help-block" ).hide();
				
				/* Start each loop*/
				$.each( xhr.responseJSON, function( i, obj ) {
				
					$( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
					$( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
					$( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
					$( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);

					if( i=='type' ){
						$('.type').addClass('has-error');
						$('.type').find('label.help-block').slideDown(400).html(obj);
					}
					if( i=='industry_id' ){
						$('.industry').addClass('has-error');
						$('.industry' ).find('label.help-block').slideDown(400).html(obj);
					}
					
					if( i=='status' ){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				}); /* End each loop */
			} /* End error() */
		});
	});


	

	$('#industry_id').change(function(){
		$('#industry_name').val($('#industry_id').find(":selected").text() );
	});
	

});
</script>

@endsection	
