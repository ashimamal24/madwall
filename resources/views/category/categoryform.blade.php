<div class="form-body">
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group industry">
                {{ Form::label( 'industry', 'Select Industry: ',['class' => 'control-label'] ) }} <span class="star">*</span>
                {{ Form::hidden( 'industry_name' , null, ['class' => 'form-control' , 'id' => 'industry_name' ] ) }}

                @if(count($industries)) 
                {{ Form::select( 'industry_id', array_replace([''=>'Select'],$industries), null, ['class' => 'form-control','id'=>'industry_id'] ) }}
                @else
                    {{ Form::select( 'industry_id', [''=>'No Industry'], null, ['class' => 'form-control','id'=>'industry_id'] ) }}
                @endif
                <label class="help-block"></label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {{ Form::label('commision', 'Commission %: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text('commision',null,['class' => 'form-control','id'=>'commission', 'maxlength'=> '3']) }}
                <label class="help-block"></label>
            </div>
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label('name', 'Name: ',['class' => 'control-label'] ) }} <span class="star">*</span> 
                {{ Form::text( 'name' , null, ['class' => 'form-control', 'maxlength'=> '40' ] ) }}
                <label class="help-block"></label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group status">
               {{ Form::label('status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span>
                @if(isset($edit_categories['status']))
                    @if($edit_categories['status'] == 0)
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),0,['class' => 'form-control']) !!}
                    @else
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),1,['class' => 'form-control']) !!}
                    @endif
                @else
                    {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) !!}
                @endif
            <label class="help-block"></label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label('description', 'Description: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::textarea('description',null,['class' => 'form-control short_textarea', 'maxlength'=> '200' ]) }}
                <label class="help-block"></label>
            </div>
        </div>
        
        
        
    </div>

</div>

<div class="box-footer">
    <div class="col-sm-4"></div>
    <div class="col-sm-6">
        {{ Form::button( $submitButtonText, ['id'=>'addcategory','class' => 'btn btn-primary']) }}
        {{ Html::link( 'admin/list-categories', 'Cancel', array( 'class' => 'btn btn-primary' ))}}
    <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
