@extends('admin.layout')
@section('title')
	Edit Category
@endsection
@section('heading')
	Edit Category
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Category
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_categories, ['method' => 'POST','url' => '/admin/edit-category','id'=>'editcategory']) }}
				@include('category.categoryform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action', 'edit' ) }}
	
			{{ Form::hidden( 'idedit', $edit_categories['_id'], ['id'=>'idedit' ]  ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script>
jQuery(document).ready(function() {

	/* 	=========================
		Send Ajax to add Category
		=========================
	*/

	$('#addcategory').click(function(){
		var formData = new FormData($('#editcategory')[0]);
		var id = $('#idedit').val();
		$.ajax({
			dataType: 'json',
			method:'post',
			processData: false,
			contentType: false,
			url: path+'admin/edit-category/'+id,
			data: formData,
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				if( data.success == true ){
					window.location = path+'admin/list-categories';
				}
				if( data.success == false ){
					window.location = path+'admin/list-categories';
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#editcategory .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='type'){
						$('.type').addClass('has-error');
						$('.type').find('label.help-block').slideDown(400).html(obj);
					}
					if(i=='industry_id'){
						$('.industry').addClass('has-error');
						$('.industry').find('label.help-block').slideDown(400).html(obj);
					}
					if(i=='parent_id'){
						$('.parent_id').addClass('has-error');
						$('.parent_id').find('label.help-block').slideDown(400).html(obj);
					}
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});	
			}
		});
	});

	/* 	==================================================================
		Select Subcategory on based on change from Category to subcategory 
		==================================================================
	*/
	$('#type').on('change', function(){
        var val = $(this).val();
        if( val && val == 'subcategory' ) {
        	$( '#parent_id' ).prop( 'disabled', false );
        } else {
        	$( '#parent_id' ).prop( 'disabled', true );
        }
    });

    if( $('#type').val() == 'subcategory' ){
		$( '#parent_id' ).prop( 'disabled', false );
	}

	/* 	========================================================
		Fetch Industry nam ein hidden feild to store in database 
		========================================================
	*/
	$( '#industry_id' ).change(function(){
		$('#industry_name').val($('#industry_id').find(":selected").text() );
	});

});
</script>

@endsection	
