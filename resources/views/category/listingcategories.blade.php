@extends('admin.layout')
@section('title')
	Categories	
@endsection
@section('content')
<h3 class="page-title">
Categories
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-categories' ) }}">Manage Category</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-body">
					<div class="table-container">
						<input type="hidden" name="action" value="filter-data"/>
						<div class="table-actions-wrapper">
							<span>
							</span>
						<a href="{{url('admin/add-category')}}" class="btn blue btn-sm pull-right">Add Category</a>
						</div>
						<div class="table-custom table-responsive">
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax_for_category">
							<thead>
							<tr role="row" class="heading">
								<th width="5%">No.</th>
								<th width="15%">Name</th>
								<th width="15%">Commission</th>
								<th width="10%">Industry</th>
								<th width="10%">Active/Inactive</th>
								<th width="15%">Actions</th>
							</tr>
							<tr role="row" class="filter">
								<td></td>
								<td><input type="text" class="form-control form-filter input-sm" name="name" id="approved_planname" autocomplete="off"></td>
								<td></td>
								<td>{{ Form::select('industry_name', array(''=>'--select--','IT' => 'IT', 'Banking' => 'Banking' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
								<td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
								<td>
									<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
									<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>
								</td>
							</tr>
							</thead>
						</table>
					</div>
					</div>
				</div>
			</div>
		</div><!-- End: life time stats -->
	</div><!-- END PAGE CONTENT-->
	@include('category.popupcategory')
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwcategory.js') }}"></script>
<script>
jQuery(document).ready(function() {


	/***********user ajax view *******/
	$(document).on("click", "#view", function () {
        var url_for_user_view = adminname+'/view-category';
        var catId = $(this).attr("catId");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: catId,_token:token},
            dataType: "JSON",
			success: function (result) {
			if ( result.status == 'error' ) {
			    bootbox.alert('some problem occur try again.....');
			}else {                   
				
				if(result.reslutset.name){
					$('#name').text(result.reslutset.name);
			   	} else{
			   		$('#name').text('NA');
			   	}


				if(result.reslutset.commision){
					$('#commision').text(result.reslutset.commision);
				} else{
			    	$('#commision').text('NA');
			   	}
			    if(result.reslutset.industry_name){
					$('#industry').text(result.reslutset.industry_name);
				} else {
					$('#industry_name').text('NA');
				}

				
				if(result.reslutset.categoryrelation ){
					$('#category').text(result.reslutset.categoryrelation.name);
				} else {
					$('#category').text('NA');
				}
				
				if(result.reslutset.description){
					$('#description').text(result.reslutset.description);
				} else {
					$('#description').text('NA');
				}
				
				if(result.subcategory){
					$('#subcategory').text(result.subcategory);
			   	} else{
			   		$('#subcategory').text('NA');
			   	}
			   $('#myModal').modal('show');
			}
			}
        });
    });
	/***********user ajax view ends here*******/
	
	$(document).on("click", "#deletecategory", function () {
        
        var deleteLink = path+$(this).attr('deleteLink');
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";
        bootbox.confirm("This action will delete category and all its associated subcategories. Do you want to proceed?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/delete/'+id,
	         		type: "POST",
	         		data: {id: id,_token:token},
	         		success  : function(data) {
	         				
	         			
						if( data.success == true ){
							window.location = path+'admin/list-categories';
						}
						if( data.success == false ){
							window.location = path+'admin/list-categories';
						}
						/*if( data.countsubcategory > 0 ){

							bootbox.confirm("This will delete the category and sub-category associated with it.Are you sure?", 
								function (result) {
								if(result){
									$.ajax({
									url : path+'admin/delete-releted-subcategory/'+data.catid,
									type:"POST",
									data:{id: data.catid,_token:token },
									success : function( catdata ){
										if( catdata.success == true ){
											window.location = path+'admin/list-categories';
										}
										if( catdata.success == false ){
											window.location = path+'admin/list-categories';
										}
									}
								});
								}
								
							});
						}*/

					},
	         	});
            }
        });
    });
    
	
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to  '+(status == true ? "Inactive" : "Active")+' this item ?', function (result) {
            if (result) {
                $.ajax({
						url: path+'admin/lock-category',
						data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
						dataType: 'json',
						type: 'post',
						beforeSend: function(){
							$this.html('<i class="fa fa-spin fa-spinner"></i>');
						},
						complete: function(){
							
						},
						success: function(json){
							if ( json.success ) {
								showSuccessMessage('Status changed');
								TableAjax.refresh();
							} else if (json.exception_message) {
								showErrorMessage('Something went wrong!!');
								TableAjax.refresh();
							}
						},
						error : function(xhr, ajaxOptions, thrownError) {
							showErrorMessage('Something went wrong!!');
						}
				});
            }
        });
	});
	
	/*$('.table-bordered').dataTable( {
	    "language": {
    	  "emptyTable": "No Categories."
    	}
    } );*/

	TableAjax.init();
	TableAjax.update();
});
</script>

@endsection
