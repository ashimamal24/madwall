<!DOCTYPE html>
<html>
<head>
	<title>JQuery HTML5 QR Code Scanner using Instascan JS Example - ItSolutionStuff.com</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
</head>
<body>
  
    <h1>JQuery HTML5 QR Code Scanner using Instascan JS Example - ItSolutionStuff.com</h1>
  <button class="btn btn-primary rotate1"><i>rotate1</i></button>  
    <video id="preview"></video>
<button class="btn btn-primary rotate"><i>rotate</i></button>
    <script type="text/javascript">
      cameraLength = 0;
      var scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
      
      Instascan.Camera.getCameras().then(function (cameras) {
          cameraLength = cameras.length;        
        	if (cameras.length > 0) {
          		scanner.start(cameras[0]);
        	} else {
          	console.error('No cameras found.');
        	}
      	}).catch(function (e) {
        	console.error(e);
      	});
      var currentCamera =1;
      $(document).on('click','.rotate1',function(){
        alert(currentCamera);
      });
      $(document).on('click','.rotate',function(){
        
        scanner.stop().then(function(){
            scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
        scanner.addListener('scan', function (content) {
        alert(content);
      });
        Instascan.Camera.getCameras().then(function (cameras) {
          
            if(cameraLength  == currentCamera){
              currentCamera =1;
            }else{
              ++currentCamera;
            }

              scanner.start(cameras[currentCamera-1]);
          
        
        }).catch(function (e) {
          alert(e);
        });
        });
        
      });
    </script>
   
</body>
</html>