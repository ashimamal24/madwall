
<table align="center" width="100%" style="font-family:Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', serif" cellpadding="0" cellspacing="0">
	<!-- ---------- Employee name ----------- -->
  <tr>
    <td style="text-align:center; background:#ededed; font-size:24px; color:#11111; font-weight:bold; border-bottom:1px solid #949494; padding:20px;"> {{ $users['first_name'] }} {{ $users['last_name'] }} </td>
  </tr>
  <!-- ---------- Employee name ----------- -->
  <tr>
    <td style="font-size:20px; color:#11111; padding-top:10px;">{{ $users['first_name'] }} {{ $users['last_name'] }}</td>
  </tr>
  <!-- ---------- Employee Email ----------- -->
  <tr>
    <td style="font-size:20px; color:#11111; padding-top:10px;">{{ $users['email'] }}</td>
  </tr>
  <!-- ---------- Employee Phone ----------- -->
  <tr>
    <td style="font-size:20px; color:#11111; padding-top:10px;">{{ $users['country_code'] }}-{{ $users['phone'] }}</td>
  </tr>
  <!-- ---------- Employee Address ----------- -->
  <tr>
    <td style="font-size:20px; color:#11111; padding-top:10px; padding-bottom:10px; padding-bottom:30px;"># {{ $users['address'] }}</td>
  </tr>
  <!-- ---------- Employee DOB ----------- -->
  <tr>
    <td style="font-size:20px; color:#11111; padding-top:10px;">{{ Carbon\Carbon::parse($users['dob'])->format('d')}} {{ Carbon\Carbon::parse($users['dob'])->format('M') }}, {{ Carbon\Carbon::parse($users['dob'])->format('Y') }}</td>
  </tr>
  <!-- ---------- Employee SIN Number ----------- -->
  <tr>
    <td style="font-size:20px; color:#11111; padding-top:10px;">@if(!empty($users['sin_number'])) {{ wordwrap($users['sin_number'], 3 , ' ' , true )}} @endif</td>
  </tr>
  <!-- ---------- Employee Image ----------- -->
  <tr>
	<td style="font-size:20px; color:#11111; padding-top:10px;">
		@if(isset($users['image']))
			<img src="{{$users['image']}}" class="img-rounded" height="120" width="140" />
		@else
			No image
		@endif
	</td>
  </tr>
  
  
	<!-- ---------- work/total hours worked and earnings history ----------- -->
	<tr>
		<td style="text-align:center;  font-size:24px; color:#11111; font-weight:bold; border-bottom:1px solid #949494; padding:20px; background:#ededed">Work History </td>
	</tr>
	<tr>
		<td style="font-size:18px;">
			<table width="100%"  cellpadding="0" cellspacing="0">
				<tr>
					<th style="padding:10px 0px;">Total Jobs</th>
					<th>Hours Worked</th>
					<th>Earned</th>
				</tr>
				<tr>
					<td style="padding:10px 0px;">{{$completed_jobs}}</td>
					<td>{{$total_working_hour}}</td>
					<td>{{$total_earning}}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

