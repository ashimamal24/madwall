<!-- Modal Header -->
<div class="modal-header">
	<button type="button" class="close" 
	   data-dismiss="modal">
		   <span aria-hidden="true">&times;</span>
		   <span class="sr-only">Close</span>
	</button>
	<h4 class="modal-title" id="myModalLabel">
		Employee CV
	</h4>
</div>
	
<!-- Modal Body -->
<div class="modal-body" id="employee_cv_list">
	@if(!empty($userDetail))
		<div class="table-responsive">
			<table class="table table-bordered">
				<tbody>
					<tr>
						@if(!empty($userDetail['new_cv'])) <td>Old CV</td> @endif
						
						@if(empty($userDetail['cv_name']))
							<td>Not available</td><td align="center">N/A</td>
						@else
							<td>{{$userDetail['cv_name']}}</td><td align="center"><a target="_blank" href="{{$userDetail['cv_url']}}"><span style="color:#4985CB" title="Download New CV" class="fa fa-download" aria-hidden="true"></span></a></td>
						@endif
						
					</tr>
					@if(!empty($userDetail['new_cv']))	
						
						<tr>
							@if(!empty($userDetail['new_cv'])) <td>New CV</td> @endif
							<td>{{$userDetail['new_cv_name']}}</td><td align="center"><a target="_blank" href="{{$userDetail['new_cv']}}"><span style="color:#4985CB" title="Download New CV" class="fa fa-download" aria-hidden="true"></span></a></td>
						</tr>	
						<tr>
							@if(!empty($userDetail['new_cv'])) 
							<td colspan="3" style="text-align:right"> 
							@else
							<td colspan="2" style="text-align:right">
							@endif
								<button type="button" data-id="{{$userDetail['_id']}}" class="btn btn-primary" id="approve_new_cv">Approve CV</button>
								<button type="button" data-id="{{$userDetail['_id']}}" class="btn btn-primary" id="decline_new_cv">Decline CV</button>
							</td>
						</tr>				
					@endif	
				</tbody>			
			</table>
		</div>
	@else
		No CVs available
	@endif
</div> 


