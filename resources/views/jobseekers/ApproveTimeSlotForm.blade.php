<div class="form-body">
    <div class="row">
         <!--Candidate proofs-->
        <div class="col-md-12">
            <div class="col-md-6 cv-border tiles">
                @foreach ( $detail['id_proofs'] as $key => $proofs )
                    <div class="col-sm-4">
                        <h6><strong>{{ $proofs['name'] }}</strong></h6>
                        <a href="{{$proofs['url']}}" target="_blank">
                            <div class="tile bg-blue">
                                <div class="tile-body">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                            </div>
                        </a>
                        <div class="tile-object required-proof">
                            <div class="number">
                                {{ Form::text("proofs_value[$key]",$proofs['value'],['class' => 'form-control proofs-value', 'maxlength'=> '25' ]) }}
                                {{ Form::hidden('proofs_url[]',$proofs['url'],['class' => 'form-control proofs-url']) }}
                                {{ Form::hidden('proofs_name[]',$proofs['name'],['class' => 'form-control proofs-name']) }}
                                <label class="help-block"></label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <a class="btn red-sunglo" target="_blank" href="{{$proofs['url']}}"><i class="fa fa-download "></i> Download</a>
                    </div>
                @endforeach
                <div class="col-sm-4 certificate">
                    @if(isset($detail['cv_url']))
                        <h6><strong>Candidate Resume</strong></h6>
                            <a target="_blank" href="{{$detail['cv_url']}}">
                            <div class="tile bg-blue">
                                <div class="tile-body">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                            </div>
                        </a>
                        <p class="cv-name">{{$detail['cv_name']}}</p>
                        <a class="btn red-sunglo" target="_blank" href="{{$detail['cv_url']}}"><i class="fa fa-download "></i> Download</a>
                        {{ Form::file( 'cv_url', $attributes = array( 'class' => 'form-group', 'id' =>'cv_url' )) }}
                        {{ Form::hidden('hidden_cv_name',$detail['cv_name'],['class' => 'hidden-cv-name',  'id' => 'hidden-cv-name' ]) }}
                        {{ Form::hidden('hidden_cv_url',$detail['cv_url'],[ 'id' => 'hidden-cv-url' ]) }}
                    @endif             
                </div>
            </div>
            <div class="col-md-6 tiles">
                <div class="col-sm-6">
                    <p>% obtained in Madwall Quiz :  <span style="float:right; position: absolute;"><strong>
                    @if(isset($detail['madwall_quiz_answer']))  
                     {{$detail['madwall_quiz_answer']['over_all_percentage']}}</strong></span> </p>
                    <p>% obtained in WIMIS Quiz :  <span style="float:right; position: absolute;"><strong>{{$detail['madwall_health_answer']['over_all_percentage']}}</strong></span></p> 
                    @endif
                </div>
            </div>
            <div class="col-md-6 tiles">
                <div class="col-sm-6 sin_number">
                    <p><b>SIN :</b><span style="float:right; position: absolute;"> </span>{{ Form::text("sin_number",$detail['sin_number'],[ 'id' => 'sin_number', 'class'=> 'form-control', 'maxlength'=>'9'  ]) }} </p>
                    <label class="help-block"></label>
                </div>
            </div>
        </div>
        <div class="expertise col-md-12">


             <div class="col-md-6 tiles">
                             
                                @if(isset($detail['downloadEmplyoeeContract']))    
                                <div class="col-sm-5">
                                        <h5><strong>Employee Agreement</strong></h5>
                                        <a target="_blank" href="{{$detail['downloadEmplyoeeContract']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['downloadEmplyoeeContract']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                 @if(isset($detail['downloadEmergencyContact']))    
                                <div class="col-sm-5">
                                        <h5><strong>Emergency Contact</strong></h5>
                                        <a target="_blank" href="{{$detail['downloadEmergencyContact']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['downloadEmergencyContact']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                 @if(isset($detail['downloadDirectDeposit']))    
                                <div class="col-sm-5">
                                        <h5><strong>Direct Deposit</strong></h5>
                                        <a target="_blank" href="{{$detail['downloadDirectDeposit']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['downloadDirectDeposit']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 
                            
                        </div>


            <h4><b><u>Degree/Diploma Certificate</u></b></h4><br>
                <div class="col-md-3 certificate certificate-validation">
                    {{ Form::file( 'certificate_file_url[0]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate0' )) }}
                    <p class="certificate-name"></p>
                    {{ Form::textarea('certificate_description[0]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
                    {{ Form::hidden( 'certificate_hidden_url[0]', null,array( 'class' => 'firebase_url'  )) }}
                    {{ Form::hidden( 'certificate_hidden_name[0]', null,array( 'class' => 'certificate_name'  )) }}
                    <label class="help-block"></label>
                </div>
                <div class="col-md-3 certificate certificate-validation">
                    {{ Form::file( 'certificate_file_url[1]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate1' )) }}
                    <p class="certificate-name"></p>
                    {{ Form::textarea('certificate_description[1]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
                    {{ Form::hidden( 'certificate_hidden_url[1]', null,array( 'class' => 'form-group firebase_url'  )) }}
                    {{ Form::hidden( 'certificate_hidden_name[1]', null,array( 'class' => 'certificate_name'  )) }}
                    <label class="help-block"></label>
                </div>
                <div class="col-md-3 certificate certificate-validation">
                    {{ Form::file( 'certificate_file_url[2]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate2' )) }}
                    <p class="certificate-name"></p>
                    {{ Form::textarea('certificate_description[2]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
                    {{ Form::hidden( 'certificate_hidden_url[2]', null,array( 'class' => 'form-group firebase_url'  )) }}
                    {{ Form::hidden( 'certificate_hidden_name[2]', null,array( 'class' => 'certificate_name'  )) }}
                    <label class="help-block"></label>
                </div>
                <div class="col-md-3 certificate certificate-validation">
                    {{ Form::file( 'certificate_file_url[3]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate3' )) }}
                    <p class="certificate-name"></p>
                    {{ Form::textarea('certificate_description[3]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
                    {{ Form::hidden( 'certificate_hidden_url[3]', null,array( 'class' => 'form-group firebase_url'  )) }}
                    {{ Form::hidden( 'certificate_hidden_name[3]', null,array( 'class' => 'certificate_name'  )) }}
                    <label class="help-block"></label>
                </div>
                <div class="col-md-3 certificate certificate-validation">
                    {{ Form::file( 'certificate_file_url[4]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate4' )) }}
                    <p class="certificate-name"></p>
                    {{ Form::textarea('certificate_description[4]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
                    {{ Form::hidden( 'certificate_hidden_url[4]', null,array( 'class' => 'form-group firebase_url'  )) }}
                    {{ Form::hidden( 'certificate_hidden_name[4]', null,array( 'class' => 'certificate_name'  )) }}
                    <label class="help-block"></label>
                </div>
        </div>
        
        <div  class="expertise col-md-12">
            <h4><b><u>Expertise</u></b></h4><br><br>
            <!-- ------------ select Industry -------------- -->
			<div class="col-md-4">
				<div class="form-group industry_id">
					{{ Form::label( 'industry_id', 'Select Industry: ',['class' => 'control-label'] ) }} <span class="star">*</span>

					@if(count($industries)) 
					{{ Form::select( 'industry_id[]', array_replace($industries), null, ['class' => 'form-control','id'=>'industry_id','multiple' => 'multiple'] ) }}
					@else
						{{ Form::select( 'industry_id[]', [''=>'No Industry'], null, ['class' => 'form-control','id'=>'industry_id','multiple' => 'multiple'] ) }}
					@endif
					<label class="help-block"></label>
				</div>
			</div>
        </div>
        
        <div  class="col-md-12">
            
			<div class="col-md-3">
				<div class="form-group category_id">
					{{ Form::label('category_id', 'Category: ',['class' => 'control-label']) }} <span class="star">*</span>
					@if( count( $categories ) )
					{{ Form::hidden( 'category_name', null,array( 'class' => 'category_hidden_name' , 'id' => 'category_hidden_name'  )) }}
						<!-- ------ , $categories -------- -->
						{{ Form::select('category_id[]',[], null, ['multiple'=>'multiple','class' => 'category-ajax form-control','id'=>'category_id']) }}
					@else
						{{ Form::select('category',[''=>'No Category'], null, ['class' => 'form-control','id'=>'parent_id' ]) }}
					@endif
					<label class="help-block"></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group subcategories">
					{{ Form::label('subcategories', 'Subcategory: ',['class' => 'control-label']) }} <span class="star">*</span>
						{{ Form::select('subcategories[]', [],null, array('multiple'=>'multiple' , 'class'=>'subcategory-ajax form-control', 'id' => 'subcategory')) }}
					<label class="help-block"></label>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group skills">
					{{ Form::label('skills', 'Skill: ',['class' => 'control-label']) }} <span class="star">*</span>
					{{ Form::select('skills[]', [] , null, ['multiple' => 'multiple','class' => 'form-control','id'=>'skills']) }}
					{{ Form::hidden( 'skills_name[]', null,array( 'class' => 'skill-name'  )) }}
					<label class="help-block"></label>
				</div>
			</div>
		</div>
		
		
		
    </div>
</div>
<div id="selected-time-slot" style="margin-top:20px">

@if(isset($detail['timeslots']) && !empty($detail['timeslots']))	
	@foreach($detail['timeslots'] as $dtl)
		@if($dtl['accepted'] == 1)
			<h3 class='header-text-success text-success'>
				<b>The slot selected by user is {{Carbon\Carbon::parse($dtl['start_time'])->format('d M H:i A')}} To {{Carbon\Carbon::parse($dtl['end_time'])->format('H:i A')}}</b>
			</h3>
		@endif
	@endforeach
@endif

</div>


<div class="box-footer">
    <div class="col-sm-6" style="float:right">
        <a class="btn btn-small btn-custom" href="{{URL::to('admin/htmltopdfview/' . $detail['_id'] )}}">Export to Pdf</a>
        {{ Form::button( $submitButtonText, [ 'id'=>'approve-waiting-jobseeker','class' => 'btn green']) }}
        
        @if($submitDeclineText != 'No')
        {{ Form::button( $submitDeclineText, [ 'id'=>'decline','class' => 'btn red']) }}
        @endif
      
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
