@extends('admin.layout')

@section('title')
	Declined Employees
@endsection

@section('content')
<h3 class="page-title">
Blocked Employees
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-blockedemployee' ) }}">Declined Employees</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-body">
					<div class="table-custom table-container">
						<input type="hidden" name="action" value="filter-declined-user"/>
						
						<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax_for_block_employee">
							<thead>
							<tr role="row" class="heading">
								<th width="5%">No.</th>
								<th width="10%">Name</th>
								<th width="10%">Email</th>
								<th width="10%">Mobile Number</th>
								<th width="10%">Address</th>
								<th width="10%">SIN</th>
								<th width="20%">Registered Date</th>								
								
								<th width="10%">Action</th>
							</tr>
							<tr role="row" class="filter">
								<td></td>
								<td><input type="text" class="form-control form-filter input-sm" name="name" id="approved_planname" autocomplete="off"></td>
								<td><input type="text" class="form-control form-filter input-sm" name="email" autocomplete="off"/></td>
								<td><input type="text" class="form-control form-filter input-sm" name="phone_num" autocomplete="off"/></td>
								<td></td>
								<td></td>
								
								<td>
									<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd" id="registered_start_date">
										<input type="text" class="form-control form-filter input-sm" readonly name="requested_at_from" placeholder="From">
										<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd" id="plan_end_date">
										<input type="text" class="form-control form-filter input-sm" readonly name="requested_at_to" placeholder="To">
										<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</td>							
								
								<td>
									<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
									<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
								</td>
							</tr>
							</thead>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End: life time stats -->
	</div><!-- END PAGE CONTENT-->
	@include('jobseekers.popupblockuser')
	
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwblockjobseekers.js') }}"></script>
<script src="{{ asset( 'public/admin/js/bootstrap-datepicker.js' ) }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {
 
 /***********user ajax view *******/
	$(document).on("click", "#view", function () {
        var url_for_user_view = adminname+'/view-block-employe';
        var userId = $(this).attr("userId");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: userId,_token:token},
            dataType: "JSON",
			success: function (result) {
				if ( result.status == 'error' ) {
				    bootbox.alert('some problem occur try again.....');
				} else {                   
				  
					if(result.reslutset.first_name){
						$('#name').text(result.reslutset.first_name +' '+ result.reslutset.last_name );
				   	} else{
				   		$('#name').text('N/A');
				   	}

					if(result.reslutset.email){
						$('#email').text(result.reslutset.email);
					} else{
				    	$('#email').text('N/A');
				   	}

				   	if(result.reslutset.country_code != '' && result.reslutset.phone != ''){
						$('#mob').text(result.reslutset.country_code +' '+ result.reslutset.phone);
					} else{
				    	$('#mob').text('N/A');
				   	}

				   	if(result.reslutset.address){
						$('#address').text(result.reslutset.address);
					} else{
				    	$('#address').text('N/A');
				   	}

				   	if(result.reslutset.sin_number){
						$('#sin').text(result.reslutset.sin_number);
					} else{
				    	$('#sin').text('N/A');
				   	}

				   $('#myModal').modal('show'); 	
				}
			}
        });
    });
	
$('.start_date').datepicker( {
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months",
      onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            
        } 
});

 $('start_date').datepicker()
      .on('changeDate', function(ev){
		   $('.datepicker').hide();
	  });



	  /////////////////////    code for datepickers start    ////////////////////////////////
			
	function formatDate(date) {
	    var d = new Date(date),
		month = '' + (d.getMonth() + 1),
	    day = '' + d.getDate(),
	    year = d.getFullYear();
	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	    return [year,month,day].join('-');
	}
	
	$('#registered_start_date').datepicker().on('changeDate', function(ev){
		var selected_date = new Date(ev.dates);
		var date = formatDate(selected_date);
		$('#plan_end_date').datepicker('setStartDate',date);
		$('.datepicker').hide();
		$('.filter-submit').click();  
    });
      
	$('#plan_end_date').datepicker().on('changeDate', function(ev){
		var selected_date = new Date(ev.dates);
		var date = formatDate(selected_date);
		$('#registered_start_date').datepicker('setEndDate', date);
		$('.datepicker').hide();
		$('.filter-submit').click();  
	});		
             
   ////////////////////    code for timepicker end    ////////////////////////////////////


   /** Change Ststus **/
   	$(document).on('click','#change-common-status', function(){
		var $this = $(this);

		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to put this employee in waiting list.', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/undelined-employee',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						window.location = path+'admin/list-waitingemployee';
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});
	
	TableAjax.init();
	TableAjax.update();
});
</script>

@endsection
