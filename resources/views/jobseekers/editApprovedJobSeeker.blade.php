@extends('admin.layout')
@section('title')
  Approved or Decline
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/chosen.css' ) }}" rel="stylesheet" type="text/css"/>
<style>
.expertise{
    border-top-style: solid;
    border-top-width: 1px;
}
.form-group.certificate_upload {
    float: left;
    overflow-wrap: break-word;
    width: 100%;
}
.certificate-name {
    float: left;
    overflow-wrap: break-word;
    width: 100%;
}
.work-history{
	padding :5px;
}
.btn-custom{
  background: white none repeat scroll 0 0;
    border: 1px solid #3379B5;
    color: #3379B5;
}
.btn-custom:hover{
  color: #3379B5;
}

</style>
@endsection
@section('content')
<h3 class="block" style="text-align:center">
 <b><u>View Details</u></b>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="caption" style="float:right">
                <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/list-approvedemployee')}}"> Back </a>
            </div>
                                <div class="portlet-body util-btn-margin-bottom-5">
                <div class="tab-pane" id="tab_1">
                    <div class="portlet-body form">
                        <div class="row">
                            <div class="col-md-12">
                            	<div class="col-md-12">
                            		
                                		<div class="well col-md-10">
                                            <span style="color:#228B22; font-size:20px">{{$edit_approved_jobseeker['first_name']}} {{$edit_approved_jobseeker['last_name']}}</span><br>
                                            <span><i class="glyphicon glyphicon-envelope"></i> {{ $edit_approved_jobseeker['email']}}</span><br>
                                            <span><i class="fa fa-phone"></i> {{ $edit_approved_jobseeker['country_code'] }}-{{ $edit_approved_jobseeker['phone'] }}</span><br>
                                            <span># {{ $edit_approved_jobseeker['address']}}</span><br>
                                            <span>{{ Carbon\Carbon::parse($edit_approved_jobseeker['dob'])->format('d')}} {{ Carbon\Carbon::parse($edit_approved_jobseeker['dob'])->format('M') }}, {{ Carbon\Carbon::parse($edit_approved_jobseeker['dob'])->format('Y') }}</span><br>
                                            <span>{{ $edit_approved_jobseeker['sin_number']}}</span>
                                    	</div>
                            		  		<div class="well col-md-2">
                                                @if(isset($edit_approved_jobseeker['image']))
                                                    <img src="{{$edit_approved_jobseeker['image']}}" class="img-rounded" height="120" width="140" />
                                                @else
                                                    {{ Html::image('public/admin/img/user_male2-512.png', 'a picture', array( "height"=>"120", "width"=>"120" )) }}
                                                @endif
                                            </div>
                                    	
                            	</div>
                                
                                {{ Form::open(array( 'method' => 'POST','url' => '/admin/edit-approve-waitlisting-jobseeker-data','id'=>'edit-approve-waitlisting-form', 'files'=>true, 'accept'=> 'application/pdf' )) }}
                               
                                <div class="col-md-12">
              <div class="form-body">
                  <div class="row">
                       <!--Candidate proofs-->
                      <div class="col-md-12">
                          <div class="col-md-6 tiles required-proof">
                            
                            @if( isset( $edit_approved_jobseeker['id_proofs'] ) )
                              @foreach ( $edit_approved_jobseeker['id_proofs'] as $key => $proofs )
                                  <div class="col-sm-4">
                                      <h6><strong>{{ $proofs['name'] }}</strong></h6>
                                        
                                      <a target="_blank" href="{{$proofs['url']}}" target="_blank">
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                      </a>

                                        <div class="tile-object">
                                            <div class="number">
                                                {{ Form::text("proofs_value[$key]",$proofs['value'],['class' => 'form-control proofs-value', 'maxlength'=> '25']) }}
                                                {{ Form::hidden('proofs_url[]',$proofs['url'],['class' => 'form-control proofs-url']) }}
                                                {{ Form::hidden('proofs_name[]',$proofs['name'],['class' => 'form-control proofs-name']) }}
                                                <label class="help-block"></label>
                                            </div>
                                        </div>
                                      <div class="clearfix"></div>
                                      <a target="_blank" class="btn red-sunglo" target="_blank" href="{{$proofs['url']}}">Download</a>
                                  </div>
                              @endforeach
                            @endif

                              
                              <div class="col-sm-4 certificate">
                                  <h6><strong>Candidate Resume</strong></h6>
                                  <a target="_blank" href="{{$edit_approved_jobseeker['cv_url']}}"><div class="tile bg-blue">
                                      <div class="tile-body">
                                          <i class="fa fa-briefcase"></i>
                                      </div>
                                  </div></a>
                                  <p class="cv-name">{{$edit_approved_jobseeker['cv_name']}}</p>

                                  <a target="_blank" class="btn red-sunglo" href="{{$edit_approved_jobseeker['cv_url']}}"><i class="fa fa-download "></i>Download</a>
                                  {{ Form::file( 'cv_url', $attributes = array( 'class' => 'form-group', 'id' =>'cv_url' )) }}
                                  {{ Form::hidden('hidden_cv_name',$edit_approved_jobseeker['cv_name'],['class' => 'hidden-cv-name',  'id' => 'hidden-cv-name' ]) }}
                                  {{ Form::hidden('hidden_cv_url',$edit_approved_jobseeker['cv_url'],[ 'id' => 'hidden-cv-url' ]) }}
                              </div>
                        
                          </div>

                          <div class="col-md-6 tiles">
                              <div class="col-sm-6 rating">
                                	<p> <strong> Average Rating</strong> {{ Form::text('rating',$edit_approved_jobseeker['rating'],['class' => 'form-control proofs-value', 'maxlength'=> '4',]) }}</p> 
                                	<label class="help-block"></label>
                              		
                              </div>
                          </div>
                      </div>


                         <!--Employee  Contract-->
                      <div class="col-md-12">
                          <div class="col-md-6 tiles required-proof">
                            
                             
                                @if(isset($edit_approved_jobseeker['downloadEmplyoeeContract']))    
                                <div class="col-sm-4">
                                        <h5><strong>Employee Agreement</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['downloadEmplyoeeContract']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['downloadEmplyoeeContract']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                 @if(isset($edit_approved_jobseeker['downloadEmergencyContact']))    
                                <div class="col-sm-4">
                                        <h5><strong>Emergency Contact</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['downloadEmergencyContact']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['downloadEmergencyContact']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                 @if(isset($edit_approved_jobseeker['downloadDirectDeposit']))    
                                <div class="col-sm-4">
                                       <br><h5><strong>Direct Deposit</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['downloadDirectDeposit']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['downloadDirectDeposit']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                @if(isset($edit_approved_jobseeker['opentokVideoDownloadUrl']))    
                                <div class="col-sm-4">
                                       <br><h5><strong>Download Video</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['opentokVideoDownloadUrl']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['opentokVideoDownloadUrl']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 

                         
                          </div>

                      </div>

                      	<div class="expertise col-md-12">
                        @php $database_key = array(); @endphp
                         	<h4><b><u>Degree/Diploma Certificate</u></b></h3><br>
							@if( count( $edit_approved_jobseeker['additional_documents'] ) )
                                
									@foreach( $edit_approved_jobseeker['additional_documents'] as $key => $additional_documents )
									<?php $database_key[] = $key; ?>
                                    <div class="col-md-3 certificate tiles certificate-validation">
										@if( isset( $additional_documents['url'] ) )
                                            <a target="_blank" href="{{ $additional_documents['url'] }}">
                                                <div class="tile bg-blue">
                                                    <div class="tile-body">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                </div>
                                            </a>

											<!-- <img src="{{ $additional_documents['url'] }}" width="50" height="50" class="img-circle preview4" alt=""/> -->
										@endif

										{{ Form::file( "certificate_file_url[$key]",  array( 'class' => 'form-group certificate_upload' )) }}
											<p class="certificate-name">{{$additional_documents['name']}}</p>
                                            <a target="_blank" class="btn red-sunglo" href="{{ $additional_documents['url'] }}">Download</a>
											{{ Form::textarea("certificate_description[$key]",$additional_documents['description'],['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
										

											{{ Form::hidden( "certificate_hidden_url[$key]", $additional_documents['url'],array( 'class' => 'form-group firebase_url'  )) }}

											{{ Form::hidden( "certificate_hidden_name[$key]", $additional_documents['name'],array( 'class' => 'certificate_name'  )) }}
										<label class="help-block"></label>
									</div>
								@endforeach
                                
								<div class="col-md-12">
									@for($i = 0; $i<=4 ; $i++ )
                                        @if (! in_array($i, $database_key))
                                            <div class="col-md-4 certificate certificate-validation">
                                                {{ Form::file( "certificate_file_url[$i]", $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate1' )) }}
                                                <p class="certificate-name"></p>
                                                
                                                {{ Form::textarea("certificate_description[$i]",null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
                                                {{ Form::hidden( "certificate_hidden_url[$i]", null,array( 'class' => 'firebase_url'  )) }}
                                                {{ Form::hidden( "certificate_hidden_name[$i]", null,array( 'class' => 'certificate_name'  )) }}
                                                <label class="help-block"></label>
                                            </div>
                                        @endif
									@endfor
								</div>
							@else
								<div  class="col-md-12">
						            <div class="col-md-4 certificate certificate-validation">
						                {{ Form::file( 'certificate_file_url[0]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate1' )) }}
						                <p class="certificate-name"></p>
                                        {{ Form::textarea('certificate_description[0]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
						                {{ Form::hidden( 'certificate_hidden_url[0]', null,array( 'class' => 'firebase_url'  )) }}
						                {{ Form::hidden( 'certificate_hidden_name[0]', null,array( 'class' => 'certificate_name'  )) }}
						                <label class="help-block"></label>
                                    </div>
						            <div class="col-md-4 certificate certificate-validation">
						                {{ Form::file( 'certificate_file_url[1]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate2' )) }}
						                <p class="certificate-name"></p>
                                        {{ Form::textarea('certificate_description[1]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
						                {{ Form::hidden( 'certificate_hidden_url[1]', null,array( 'class' => 'form-group firebase_url'  )) }}
						                {{ Form::hidden( 'certificate_hidden_name[1]', null,array( 'class' => 'certificate_name'  )) }}
						                <label class="help-block"></label>
                                    </div>
						            <div class="col-md-4 certificate certificate-validation">
						                {{ Form::file( 'certificate_file_url[2]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate3' )) }}
						                <p class="certificate-name"></p>
                                        {{ Form::textarea('certificate_description[2]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
						                {{ Form::hidden( 'certificate_hidden_url[2]', null,array( 'class' => 'form-group firebase_url'  )) }}
						                {{ Form::hidden( 'certificate_hidden_name[2]', null,array( 'class' => 'certificate_name'  )) }}
						                <label class="help-block"></label>
                                    </div>
								</div>
								<div class="clearfix"></div>
					            <div  class="col-md-12">
					                <br>
					                <div class="col-md-3 certificate certificate-validation">
						                {{ Form::file( 'certificate_file_url[3]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate4' )) }}
						                <p class="certificate-name"></p>
                                        {{ Form::textarea('certificate_description[3]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
						                {{ Form::hidden( 'certificate_hidden_url[3]', null,array( 'class' => 'form-group firebase_url'  )) }}
						                {{ Form::hidden( 'certificate_hidden_name[3]', null,array( 'class' => 'certificate_name'  )) }}
						                <label class="help-block"></label>
                                    </div>
					                <div class="col-md-3 certificate certificate-validation">
						                {{ Form::file( 'certificate_file_url[4]', $attributes = array( 'class' => 'form-group certificate_upload', 'id' =>'certificate5' )) }}
						                <p class="certificate-name"></p>
                                        {{ Form::textarea('certificate_description[4]',null,['class' => 'form-control short_textarea description', 'maxlength'=> '50', 'placeholder'=>'Description' ]) }}
						                {{ Form::hidden( 'certificate_hidden_url[4]', null,array( 'class' => 'form-group firebase_url'  )) }}
						                {{ Form::hidden( 'certificate_hidden_name[4]', null,array( 'class' => 'certificate_name'  )) }}
						                <label class="help-block"></label>
                                    </div>
					            </div>
							@endif
						</div>
							@if(isset($edit_approved_jobseeker['other_documents']))
								<div class="expertise tiles col-md-12">
								<h4><b><u>Other Documents</u></b></h4>
									@foreach( $edit_approved_jobseeker['other_documents'] as $otherdocument )
										<div class="col-sm-3">
											<h6><strong>{{$otherdocument['name']}}</strong></h6>
											<a target="_blank" href="{{$otherdocument['url']}}" target="_blank">
												<div class="tile bg-blue">
													<div class="tile-body">
														<i class="fa fa-briefcase"></i>
													</div>
												</div>
											</a>
										</div>
									@endforeach
								</div>
							@endif
							<div class="expertise work-history col-md-12">
								<h4><b><u>Work History</u></b></h4>
								<div class="col-lg-3">
				                    <div class="dashboard-stat blue-madison">
				                        <div class="visual">
				                            <i class="fa fa-bar-chart-o"></i>
				                        </div>
				                        <div class="details">
				                            <div style="font-size: 21px" class="number">
				                                <span>Total Jobs</span><br> {{$completed_jobs}}
				                        	</div>
				                        </div>
				                    </div>
				                </div>
				                <div class="col-lg-3">
				                    <div class="dashboard-stat blue-madison">
				                        <div class="visual">
				                            <i class="fa fa-bar-chart-o"></i>
				                        </div>
				                        <div class="details">
				                            <div style="font-size: 21px" class="number">
				                                <span>Hours Worked</span><br> {{$total_working_hour}}
				                        	</div>
				                        </div>
				                    </div>
				                </div>
				                <div class="col-lg-3">
				                    <div class="dashboard-stat blue-madison">
				                        <div class="visual">
				                            <i class="fa fa-bar-chart-o"></i>
				                        </div>
				                        <div class="details">
				                            <div style="font-size: 21px" class="number">
				                                <span>Earned</span><br> {{$total_earning}}
				                        	</div>
				                        </div>
				                    </div>
				                </div>
							</div>
							<div class="expertise col-md-12">

                            <h4><b><u>Expertise</u></b></h4><br>
								<!-- ------------ select Industry -------------- -->
								<div class="col-md-4">
									<div class="form-group industry_id">
										{{ Form::label( 'industry_id', 'Select Industry: ',['class' => 'control-label'] ) }} <span class="star">*</span>

										@if(count($industries)) 
										{{ Form::select( 'industry_id[]', array_replace([''=>'Select'],$industries), $edit_approved_jobseeker['industry_id'], ['class' => 'form-control','id'=>'industry_id','multiple' => 'multiple'] ) }}
										@else
											{{ Form::select( 'industry_id[]', [''=>'No Industry'], null, ['class' => 'form-control','id'=>'industry_id','multiple' => 'multiple'] ) }}
										@endif
										<label class="help-block"></label>
									</div>
								</div>
                            </div>
							
                        <div class="col-md-12">

<!--
                            <h4><b><u>Expertise</u></b></h4><br>
-->
                            <div class="col-md-4">
                                <div class="form-group category_id">
                                    
                                    {{ Form::label('category_id', 'Category: ',['class' => 'control-label']) }} <span class="star">*</span>
                                    
                                    
                                    
                                      <select multiple="multiple" class="category-ajax form-control" id="category_id" name="category_id[]">
											<option value="">Please select category</option>
											
										
										<?php
											if(count($allcategories)){
												$optcategory = [];
												foreach($allcategories as $ind){
													if(!in_array($ind['industry_name'],$optcategory)){
														if(count($optcategory)){
															?></optgroup><optgroup label="<?php echo $ind['industry_name']; ?>">
															<?php
														}else{ ?>
															<optgroup label="<?php echo $ind['industry_name']; ?>"><?php
														}
													}
													$optcategory[] = $ind['industry_name'];
													?>
													<option <?php if(in_array($ind['_id'],$edit_approved_jobseeker['category_id'])){ echo "selected"; } ?> value="<?php echo $ind['_id']; ?>"><?php echo $ind['name']; ?></option>
													<?php
												}
												?>
												
												<?php
											}
										?>
                                    </select>
                                    
                                    
                                   
                                    <label class="help-block"></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group subcategories">
                                    {{ Form::label('subcategories', 'Subcategory: ',['class' => 'control-label']) }} <span class="star">*</span>
                                    
                                    
                                    
                                    <select multiple="multiple" class="subcategory-ajax form-control" id="subcategory" name="subcategories[]">
											<option value="">Please select subcategory</option>
											
										
										<?php
											if(count($sub_categories)){
												$optsubcategory = [];
												foreach($sub_categories as $sub){
													if(!in_array($sub['category_name'],$optsubcategory)){
														if(count($optsubcategory)){
															?></optgroup><optgroup label="<?php echo $sub['category_name']; ?>">
															<?php
														}else{ ?>
															<optgroup label="<?php echo $sub['category_name']; ?>"><?php
														}
													}
													$optsubcategory[] = $sub['category_name'];
													?>
													<option <?php if(in_array($sub['_id'],$selected_subcategory)){ echo "selected"; } ?> value="<?php echo $sub['_id']; ?>"><?php echo $sub['name']; ?></option>
													<?php
												}
												?>
												
												<?php
											}
										?>
                                    </select>
                                    
                                    
                                    
                                  
                                    <label class="help-block"></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group skills">
                                    {{ Form::label('skills', 'Skill: ',['class' => 'control-label']) }} <span class="star">*</span>
                                    {{ Form::select('skills[]', $allskills, $selected_skill, ['multiple' => 'multiple','class' => 'form-control','id'=>'skills']) }}
                                    <label class="help-block"></label>
                                </div>
                            </div>
                        </div>
                  </div>
              </div>

              <div class="box-footer">
                  <div class="col-sm-6" style="float:right">                              
					<a class="btn btn-custom " href="{{URL::to('admin/htmltopdfview/' . $edit_approved_jobseeker['_id'] )}}">Export to Pdf</a>
                      {{ Form::button( 'Save', [ 'id'=>'edit-approve-user','class' => 'btn btn-primary']) }}

                  </div>
                  <div class="clearfix"></div>
              </div>
                                </div>
                                {{ Form::hidden( 'userid', $edit_approved_jobseeker['_id'],['id'=>'userid'] ) }}
                                {{ Form::hidden( 'action','edit' ) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div> 
</div>
    
<?php //echo $edit_approved_jobseeker['approved'];?>
@if( $edit_approved_jobseeker['approved'] == 1 )
    <div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Reason For Block
                </h4>
            </div>
            <!-- Modal Body -->
        <div class="modal-body">
            
          {{ Form::open(array( 'method' => 'POST','url' => '/admin/block-employee','id'=>'block-user-form', 'class' => 'form-horizontal', 'role' => 'form')) }}
            <div class="form-group reason_for_block_usr">
              <div class="col-sm-12">
              {{ Form::textarea('reason_for_block_usr',null, ['class' => 'form-control description', 'maxlength'=> '500', 'placeholder'=>'Reason' ]) }}
              </div>
              <div style="margin-left:15px;">
                <label class="help-block"></label>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-8 col-sm-10">
              {{ Form::button( 'Submit', [ 'id'=>'block-user-reason','class' => 'btn btn-primary']) }}
              <button type="button" class="btn btn-primary"
                        data-dismiss="modal">
                            Close
                </button>
              </div>
            </div>
                    {{ Form::hidden( 'userid', $edit_approved_jobseeker['_id'],['id'=>'userid'] ) }}
          {{ Form::close() }}
        </div>      
        </div>
    </div>
</div>
@endif

@endsection 
@section('js')

<script src="{{ asset( 'public/admin/js/chosen.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>

<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>

<!-- Leave out Storage -->
<!-- <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script> -->
<script>
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);

</script>

<script>

jQuery(document).ready(function() {
    /* Approve Waiting Jobseeker*/
    $('#skills').chosen();
    $('#industry_id').chosen();
    $('#subcategory').chosen();
	$('#category_id').chosen();
        
        $(document).on('change','#cv_url',function(e){
            var file = $(this)[0].files;
            
            
            if( file[0].size > 1000000 ) {
                bootbox.alert("Please upload a valid resume file within a range of 1 MB maximum");
                return false;
            }
            // Cehck File Extension
            var fileExtension = ['pdf','doc','docx'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
                return false;
            }

            addLoader();
            
            var type = file[0].type;
            $(this).closest('.certificate').find('.hidden-cv-name').val(file[0].name);
            //$(this).closest('.certificate').find('.cv-name').text(file[0].name);
            firebase_cv_upload( file, type );
            //removeLoader();
        });

        $(document).on('change','.certificate_upload',function(e){
            var file = $(this)[0].files;
            if( file[0].size > 26214400 ) {
                bootbox.alert("Please upload a valid certificate within a max range of 25 MB");
                return false;
            }

            var fileExtension = ['pdf','doc','docx', 'png', 'jpeg', 'jpg'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
                return false;
            }
            addLoader();
            
            var type = file[0].type;
            $(this).closest('.certificate').find('.certificate_name').val(file[0].name);
            //$(this).closest('.certificate').find('.certificate-name').html(file[0].name);
            firebase_multiple_upload( file, type, $(this) );
        });
        

        function firebase_multiple_upload( file,type,reference ){
            var timestamp=Date.now();
            var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
            var metadata = {
                contentType: type,
            };
            
            var blob_image=new Blob(file, { "type" : type });
            storageRef.put(blob_image).then(function(snapshot) {
                reference.closest('.certificate').find('.firebase_url').val(snapshot.downloadURL);
                removeLoader()
            }).catch(function(error) {
                removeLoader()
                console.log('firebase error occured:'+error);
                removeLoader();
            });
        }

        function firebase_cv_upload( file,type ){
            var timestamp=Date.now();
            var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
            var metadata = {
                contentType: type,
            };
            
            var blob_image=new Blob(file, { "type" : type });
            storageRef.put(blob_image).then(function(snapshot) {
                $('#hidden-cv-url').val(snapshot.downloadURL);
                removeLoader()
            }).catch(function(error) {
                removeLoader()
                console.log('firebase error occured:'+error);
                removeLoader();
            });
        }
        
        
        
        //~ /*
        //~ DESC : Autoload categories when industry is selected.
        //~ */
        //~ $(document).on('change','#industry_id',function(){
			//~ //var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			//~ var industry_id = $(this).val();
			//~ $.ajax({
				//~ url:path+'admin/industry-category',
				//~ type:'get',
				//~ data:'industry_id='+industry_id,
				//~ dataType:'json',
				//~ beforeSend:function(){
					//~ addLoader();
				//~ },
				//~ success:function(data){
					//~ removeLoader();
					//~ $('#subcategory').html('').trigger('chosen:updated');
					//~ $('#skills').html('').trigger('chosen:updated');
					//~ $('#category_id').html('');
					//~ var data1 = '<option value="">Please select category</option>';
					//~ var optgroup = [];
					//~ if(data.category)
					//~ {
						
						//~ $.each(data.category, function(index, element) {
							
							//~ if(optgroup.indexOf(element.industry_name) == -1){
								
								//~ if(optgroup.length){
									//~ data1 +='</optgroup><optgroup label="'+element.industry_name+'">';
									
								//~ }else{
									//~ data1 +='<optgroup label="'+element.industry_name+'">';
								//~ }
							//~ }
						   //~ optgroup.push(element.industry_name);
						   //~ data1 +='<option value="'+element._id+'">'+element.name+'</option>';						   
						//~ });
					//~ }
					
					//~ $('#category_id').html(data1);
					//~ //jcf.getInstance($('#category_id')).refresh();
					//~ //$('#multiselect').html('');
					//~ $('#category_id').trigger("chosen:updated");
				//~ },
				//~ error:function(errors){
					//~ removeLoader();
				//~ },
				//~ complete:function(){
					//~ removeLoader();
				//~ }
			//~ });
			
		//~ });
        
        $('#category_id').on('change', function(){
            $('#category_hidden_name').val($('option:selected',this).text());
        });
		
		//~ /*
		//~ Auto load subcategories when categories are selected
		//~ */
        //~ $('.category-ajax').on('change', function(){
            //~ $('#subcategory').val('').trigger('chosen:updated');
            //~ $('#skills').val('').trigger('chosen:updated');
			//~ var catid = $(this).val();
			//~ if(catid) {
				//~ $.ajax({
					//~ url : path+'admin/select-subcategory/'+catid,
					//~ type: "GET",
					//~ dataType: "json",
					//~ success:function(data) {
						//~ var optgroup = [];
						//~ var subcategory ='';
						//~ $.each(data, function(key, value) {
							//~ if(optgroup.indexOf(value.category_name) == -1){
								
								//~ if(optgroup.length){
									//~ subcategory +='</optgroup><optgroup label="'+value.category_name+'">';
									
								//~ }else{
									//~ subcategory +='<optgroup label="'+value.category_name+'">';
								//~ }
							//~ }
						   //~ optgroup.push(value.category_name);
							//~ subcategory+='<option value="' + value._id + '">' + value.name + '</option>';
						//~ });
						//~ $("#subcategory").html(subcategory);
						//~ $("#subcategory").trigger("chosen:updated");
						//~ $('#subcategory').chosen({ width: "95%" });
					//~ }
				//~ });
			//~ }else{
				//~ $('#subcategory').val('').trigger('chosen:updated');
			//~ }
		//~ });

    //~ $('.subcategory-ajax').on('change', function(){
        //~ var subCategory_id = $(this).val();
        //~ if(subCategory_id) {
            //~ $.ajax({
                //~ url : path+'admin/select-skills/'+subCategory_id,
                //~ type: "GET",
                //~ dataType: "json",
                //~ success:function(data) {
                    //~ var skills ='';
                    //~ data.forEach(function(data) {
                        //~ skills+='<option value="' + data.id + '">' + data.name + '</option>';
                    //~ });
                    
                    
                    
                    //~ if(data.skills)
					//~ {
						//~ $.each(data.skills, function(index, element) {
							
							//~ skills +='<option value="'+element._id+'"';
							//~ if(data.mandatory){
								
								//~ if($.inArray(element._id, data.mandatory) != -1){
									//~ skills +='selected';
								//~ }
							//~ }
							
							//~ skills += '>'+element.name+'</option>';
						//~ });
					//~ }
                    
                    
                    
                    
                    
                    
                    //~ $("#skills").html(skills);
                    //~ //$('#skills option').prop('selected', true);
                    //~ $("#skills").trigger("chosen:updated");
                    //~ $('#skills').chosen({ width: "95%" });              
                //~ }
            //~ });
        //~ }else{
            //~ $('#skills').val('').trigger('chosen:updated');
        //~ }
    //~ });

    $('#edit-approve-user').click(function(){
        
        $("#edit-approve-waitlisting-form").ajaxSubmit(
        {
            method      : 'post',
            beforeSend  : function() {
                addLoader();
            },
            url : path+'admin/edit-approve-waitlisting-jobseeker-data',
            success     : function(data) {
                window.location = path+'admin/list-approvedemployee';
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#addcategoryform .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                var j=0; 
                $.each( xhr.responseJSON, function( i, obj ) {
                
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    
                    if(i.indexOf("proofs_value") >= 0)  {
                        
                        var result = i.split('.');
                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.number').addClass('has-error');
                        
                        if( obj == "The proofs value.0 field is required." || obj == "The proofs value.1 field is required."){ 
                            $("input[name='"+result[0]+"["+result[1]+"]']").closest('.number').find('label.help-block').slideDown(400).html('Please specify ID proof number');
                        }

                        if( obj == "The proofs value.0 may only contain letters and numbers." || obj == "The proofs value.1 may only contain letters and numbers."){ 
                            $("input[name='"+result[0]+"["+result[1]+"]']").closest('.number').find('label.help-block').slideDown(400).html('Proof number must be alphanumeric');
                        }
                    }
                    

                    if(i.indexOf("certificate_description") >= 0)  {
                        var result = i.split('.');
                        $("textarea[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').addClass('has-error');
                        if( obj == "The certificate description.0 field is required." || obj == "The certificate description.1 field is required." || obj == "The certificate description.2 field is required." ||  obj == "The certificate description.3 field is required." ||  obj == "The certificate description.4 field is required."){
                            $("textarea[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').find('label.help-block').slideDown(400).html('Please specify the description');
                        }
                        if( obj == "The certificate description.0 must be at least 5 characters." || obj == "The certificate description.1 must be at least 5 characters." || obj == "The certificate description.2 must be at least 5 characters." || obj == "The certificate description.3 must be at least 5 characters." ||  obj == "The certificate description.4 must be at least 5 characters."){
                            $("textarea[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').find('label.help-block').slideDown(400).html('Please specify the description between 5 – 50 characters only');
                        }
                    }

                  
                    if( i=='category_id' ){
                        $('.category_id').addClass('has-error');
                        $('.category_id' ).find('label.help-block').slideDown(400).html(obj);
                    }
                    
                    if( i=='industry_id' ){
                        $('.industry_id').addClass('has-error');
                        $('.industry_id' ).find('label.help-block').slideDown(400).html(obj);
                    }


                    if( i=='skills' ){
                        $('.skills').addClass('has-error');
                        $('.skills' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    if( i=='rating' ){
                        $('.rating').addClass('has-error');
                        $('.rating' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    if( i=='subcategories' ){
                        $('.subcategories').addClass('has-error');
                        $('.subcategories' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    if(i.indexOf("certificate_file_url") >= 0){
                        var result = i.split('.');
                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').addClass('has-error');
                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').find('label.help-block').slideDown(400).html(obj);
                    } 
                });
            }
        });
    });
    
    $('#block-user-reason ').click(function(){  
          var token = "{{ csrf_token() }}";
          var id = $('#userid').val();
          $.ajax({
            method      : 'post',
            url : path+'admin/block-employee',
            //data: { id:id, _token: token, skills:skills, category_id: cat_id, file:file1 },
            data: $("#block-user-form").serialize(),
            beforeSend  : function() {
                addLoader();
            },
            success     : function(data) {
                window.location = path+'admin/list-approvedemployee';
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#block-user-form .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                var j=0; 
                $.each( xhr.responseJSON, function( i, obj ) {
                
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);

                    if( i=='reason_for_block_usr' ){
                        $('.reason_for_block_usr').addClass('has-error');
                        $('.reason_for_block_usr' ).find('label.help-block').slideDown(400).html(obj);
                    }

                });
            }
        });
    });


    $('#unblock-user').click( function(){
        bootbox.confirm({
            message: "Are you sure to unblock this user ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if( result ){
                    var token = "{{ csrf_token() }}";
                    var id = $('#userid').val();
                    $.ajax({
                        method      : 'post',
                        url : path+'admin/unblock-employee',
                        data: $("#edit-approve-waitlisting-form").serialize(),
                        beforeSend  : function() {
                            addLoader();
                        },
                        success     : function(data) {
                            window.location = path+'admin/list-approvedemployee';
                        },
                        error       : function(xhr, ajaxOptions, thrownError) {
                            removeLoader();
                            $( "#block-user-form .form-group" ).removeClass( "has-error" );
                            $( ".help-block" ).hide(); 
                        }  
                    });
                }
            }
        });
    });
    
});
</script>
<script src="{{ asset('public/admin/js/approve_candidate.js') }}" type="text/javascript"></script>
@endsection 
