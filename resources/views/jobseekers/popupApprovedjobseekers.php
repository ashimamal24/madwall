<div class="modal fade" id="approved-jobseeker" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>First Name: </th>
                            <td id="firstname"></td>
                        </tr>
                        <tr>
                            <th>Last Name: </th>
                            <td id="lastname"></td>
                        </tr>
                        <tr>
                            <th>Email: </th>
                            <td id="email"></td>
                        </tr>
                        <tr>
                            <th>Phone Number: </th>
                            <td id="phone"></td>
                        </tr>
                        <tr>
                            <th>Address: </th>
                            <td id="address"></td>
                        </tr>
                        <tr>
                            <th>Sin: </th>
                            <td id="sin"></td>
                        </tr>
                       <!--  <tr>
                            <th>Profile Status: </th>
                            <td id="profile-status"></td>
                        </tr> -->
                         <tr>
                            <th>Date: </th>
                            <td id="date"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn green" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>