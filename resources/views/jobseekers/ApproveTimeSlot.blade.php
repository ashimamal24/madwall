@extends('admin.layout')
@section('title')
	Approved or Decline
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/chosen.css' ) }}" rel="stylesheet" type="text/css"/>
<style>
.expertise{
    border-top-style: solid;
    border-top-width: 1px;
}
.form-group.certificate_upload {
    float: left;
    overflow-wrap: break-word;
    width: 100%;
}
.btn-custom{
  background: white none repeat scroll 0 0;
    border: 1px solid #3379B5;
    color: #3379B5;
}
.btn-custom:hover{
  color: #3379B5;
}
/*.header-text-success{
    color: #008000;
}
.btn.green{
    background-color: #008000;
}
.btn.red{
    background-color:#FF0000;
}*/

.certificate-name {
    float: left;
    overflow-wrap: break-word;
    width: 100%;
}

#skills_chosen {
    width: 425px!important;
}
</style>
@endsection
@section('content')
<h3 class="block" style="text-align:center">
 <b><u>View Details </u></b>
</h3>
<div class="tab-content">
    <div class="tab-pane active" id="tab_1_1">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue ">
                    <div class="caption" style="float:right">
                        <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/list-waitingemployee')}}"> Back </a>
                    </div>
                    
                    <div class="portlet-body util-btn-margin-bottom-5">
                        <div class="tab-pane" id="tab_1">
                            <div class="portlet-body form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="well col-md-10">
                                            <span style="color:#228B22; font-size:20px">{{ $detail['first_name']}} {{ $detail['last_name']}}</span><br>
                                            <span><i class="glyphicon glyphicon-envelope"></i> {{ $detail['email']}}</span><br>
                                            <span><i class="fa fa-phone"></i>  {{ $detail['country_code'] }}-{{ $detail['phone']}}</span><br>
                                            <span> # {{ $detail['address']}}</span><br>
                                            <span>{{ Carbon\Carbon::parse($detail['dob'])->format('d')}} {{ Carbon\Carbon::parse($detail['dob'])->format('M') }}, {{ Carbon\Carbon::parse($detail['dob'])->format('Y') }}</span><br>
                                        </div>
                                        <div class="col-md-2">
                                            @if( isset( $detail['image'] ) )
                                                <img src="{{$detail['image']}}" class="img-rounded" height="120" width="140" />
                                            @else
                                                {{ Html::image('public/admin/img/user_male2-512.png', 'a picture', array( "height"=>"120", "width"=>"140" )) }}
                                            @endif
                                        </div>
                                       
                                        <div class="col-md-12">
                                        {{ Form::open(array( 'method' => 'POST','url' => '/admin/approve-waitlisting-jobseeker-data','id'=>'approve-waitlisting-form', 'files'=>true)) }}
											<?php $declineText = 'Decline'; ?>
											@if($detail['status'] == false && $detail['approved'] == 2)
											
												<?php $declineText = 'No'; ?>
											@endif
                                        
                                        
                                            @include('jobseekers.ApproveTimeSlotForm',['submitButtonText' => 'Approve','submitDeclineText' => $declineText ])
                                            {{ Form::hidden( 'userid', $detail['_id'],['id'=>'userid'] ) }}
                                        {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
@endsection	
@section('js')

<script src="{{ asset('public/admin/js/chosen.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script>

<!-- Leave out Storage -->
<!-- <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-storage.js"></script> -->
<script>
    var config = {
        apiKey: "AIzaSyAzQDQ4EldRySSHdDixmUhL9trZzec4ZfI",
        authDomain: "madwalll-a5b4f.firebaseapp.com",
        databaseURL: "https://madwalll-a5b4f.firebaseio.com",
        projectId: "madwalll-a5b4f",
        storageBucket: "madwalll-a5b4f.appspot.com",
        messagingSenderId: "277872430975"
    };
    var defaultApp = firebase.initializeApp(config);

</script>

<script>
jQuery(document).ready(function() {
    /* Approve Waiting Jobseeker*/
    $('#skills').chosen();
    $('#subcategory').chosen();
    $('#category_id').chosen();
    $('#industry_id').chosen();
        
    $(document).on('change','#cv_url',function(e){
        
        var file = $(this)[0].files;
        // Check File Size
        if( file[0].size > 1000000 ) {
            bootbox.alert("Please upload a valid resume within a range of 1 MB maximum");
            return false;
        }

        // Check File Type  
        var fileExtension = ['pdf','doc','docx'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            return false;
        }

        addLoader();
        
        var type = file[0].type;
        $(this).closest('.certificate').find('.hidden-cv-name').val(file[0].name);
       // $(this).closest('.certificate').find('.cv-name').text(file[0].name);
        firebase_cv_upload( file, type );
        //removeLoader();
    });

    $(document).on('change','.certificate_upload',function(e){
        var fileExtension = ['pdf','doc','docx', 'png', 'jpeg', 'jpg'];
        var file = $(this)[0].files;
        var type = file[0].type;
       
        // Check File Size
        if( file[0].size >5242880 ) {
            bootbox.alert("Please upload a valid certificate within a max range of 5 MB");
            return false;
        }

        // Check File Type
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            bootbox.alert("Only "+fileExtension.join(', ')+ " format is allowed ");
            return false;
        }
        addLoader();
        $(this).closest('.certificate').find('.certificate_name').val(file[0].name);
        //$(this).closest('.certificate').find('.certificate-name').html(file[0].name);
        
        firebase_multiple_upload( file, type, $(this) );
    });
        

    function firebase_multiple_upload( file,type ,reference ){
        var timestamp=Date.now();
        var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
        var metadata = {
            contentType: type,
        };
        
        var blob_image=new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
            reference.closest('.certificate').find('.firebase_url').val(snapshot.downloadURL);
            //certificate.push(snapshot.downloadURL);
            //certificate['url'] = snapshot.downloadURL;
            removeLoader()
        }).catch(function(error) {
            removeLoader()
            console.log('firebase error occured:'+error);
            removeLoader();
        });
    }

    function firebase_cv_upload( file,type ){
        var timestamp=Date.now();
        var storageRef = firebase.storage().ref('certificate/'+timestamp+'_'+file[0].name); //creating firebase image reference
        var metadata = {
            contentType: type,
        };
        
        var blob_image=new Blob(file, { "type" : type });
        storageRef.put(blob_image).then(function(snapshot) {
            $('#hidden-cv-url').val(snapshot.downloadURL);
            removeLoader()
        }).catch(function(error) {
            removeLoader()
            console.log('firebase error occured:'+error);
            removeLoader();
        });
    }

    //~ $('#category_id').on('change', function(){
        //~ $('#category_hidden_name').val($('option:selected',this).text());
    //~ });

    //~ $('.category-ajax').on('change', function(){
        //~ var catid = $(this).val();
        //~ $('#skills').val('').trigger('chosen:updated');
        //~ $('#subcategories').val('').trigger('chosen:updated');
        //~ if(catid) {
            //~ $.ajax({
                //~ url : path+'admin/select-subcategory/'+catid,
                //~ type: "GET",
                //~ dataType: "json",
                //~ success:function(data) {
                    
                    //~ var subcategory ='';
                    //~ $.each(data, function(key, value) {
                        //~ subcategory+='<option value="' + key + '">' + value + '</option>';
                    //~ });
                    //~ $("#subcategories").html(subcategory);
                    //~ $("#subcategories").trigger("chosen:updated");
                    //~ $('#subcategories').chosen({ width: "95%" });
                //~ }
            //~ });
        //~ }else{
            //~ $('#subcategories').val('').trigger('chosen:updated');
        //~ }
    //~ });

    //~ $('.subcategory-ajax').on('change', function(){
        //~ var subCategory_id = $(this).val();
        //~ if(subCategory_id) {
            //~ $.ajax({
                //~ url : path+'admin/select-skills/'+subCategory_id,
                //~ type: "GET",
                //~ dataType: "json",
                //~ success:function(data) {
                    //~ $('#skills').val('').trigger('chosen:updated');
                    //~ var skills ='';
                    //~ if(data.skills)
					//~ {
						//~ $.each(data.skills, function(index, element) {
							
							//~ skills +='<option value="'+element._id+'"';
							//~ if(data.mandatory){
								
								//~ if($.inArray(element._id, data.mandatory) != -1){
									
									//~ skills +='selected';
								//~ }
							//~ }
							
							//~ skills += '>'+element.name+'</option>';
						//~ });
					//~ }
                    
                    //~ $("#skills").html(skills);
                    //~ //$('#skills option').prop('selected', true);
                    //~ $("#skills").trigger("chosen:updated");
                    //~ $('#skills').chosen({ width: "95%" });     
                   //~ // alert("I am done");         
                //~ }
            //~ });
        //~ }else{
            //~ $('#skills').val('').trigger('chosen:updated');
        //~ }
    //~ });
    

    

    $('#approve-waiting-jobseeker').click(function(){
        
        $("#approve-waitlisting-form").ajaxSubmit(
            {
                type: 'post',
                beforeSend  : function() {
                    addLoader();
                },
                url : path+'admin/approve-waitlisting-jobseeker-data',
                success     : function(data) {
                    window.location = path+'admin/list-approvedemployee';
                },
                error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#addcategoryform .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                $.each( xhr.responseJSON, function( i, obj ) {
                
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'file[name="'+i+'"]').closest('.form-group').addClass('has-error');
                    $( 'file[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);                    
                    if(i.indexOf("proofs_value") >= 0)  {
                        var result = i.split('.');
                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.number').addClass('has-error');
                        
                        if( obj == "The proofs value.0 field is required." || obj == "The proofs value.1 field is required."){ 
                            $("input[name='"+result[0]+"["+result[1]+"]']").closest('.number').find('label.help-block').slideDown(400).html('Please specify ID proof number');
                        }

                        if( obj == "The proofs value.0 may only contain letters and numbers." || obj == "The proofs value.1 may only contain letters and numbers."){ 
                            $("input[name='"+result[0]+"["+result[1]+"]']").closest('.number').find('label.help-block').slideDown(400).html('Proof number must be alphanumeric');
                        }
                    }
                    
                    if(i.indexOf("certificate_description") >= 0)  {
                        var result = i.split('.');
                        $("textarea[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').addClass('has-error');
                        if( obj == "The certificate description.0 field is required." || obj == "The certificate description.1 field is required." || obj == "The certificate description.2 field is required." ||  obj == "The certificate description.3 field is required." ||  obj == "The certificate description.4 field is required."){
                            $("textarea[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').find('label.help-block').slideDown(400).html('Please specify the description');
                        }
                        if( obj == "The certificate description.0 must be at least 5 characters." || obj == "The certificate description.1 must be at least 5 characters." || obj == "The certificate description.2 must be at least 5 characters." || obj == "The certificate description.3 must be at least 5 characters." ||  obj == "The certificate description.4 must be at least 5 characters."){
                            $("textarea[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').find('label.help-block').slideDown(400).html('Please specify the description between 5 – 50 characters only');
                        }
                    }

                    if( i=='category_id' ){
                        $('.category_id').addClass('has-error');
                        $('.category_id' ).find('label.help-block').slideDown(400).html(obj);
                    }
                    
                    if( i=='industry_id' ){
                        $('.industry_id').addClass('has-error');
                        $('.industry_id' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    if( i=='subcategories' ){
                        $('.subcategories').addClass('has-error');
                        $('.subcategories' ).find('label.help-block').slideDown(400).html(obj);
                    }
                    
                    if( i=='skills' ){
                        $('.skills').addClass('has-error');
                        $('.skills' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    if( i=='sin_number' ){
                        $('.sin_number').addClass('has-error');
                        $('.sin_number' ).find('label.help-block').slideDown(400).html(obj);
                    }

                    if(i.indexOf("certificate_file_url") >= 0){
                        var result = i.split('.');

                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').addClass('has-error');
                        $("input[name='"+result[0]+"["+result[1]+"]']").closest('.certificate-validation').find('label.help-block').slideDown(400).html('Upload the corresponding certificate');
                    }  
                });
            }
        }
        )
       
    });

        /* Decline User */
    $('#decline').click(function(){
        var token = "{{ csrf_token() }}";
        var id = $('#userid').val();
        $.ajax({
            dataType    : 'json',
            method      : 'post',
            url : path+'admin/decline-waitlisting-jobseeker',
            data: { id:id, _token: token },
            beforeSend  : function() {
                addLoader();
            },
            success     : function(data) {
                window.location = path+'admin/list-waitingemployee';
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#addcategoryform .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
            } /* End error() */
        });
    });

    

    var slots=<?php echo json_encode($detail['timeslots']); ?>;
    if( slots.length > 0 ){
        getSelectedTimeSlot();
    }
    function getSelectedTimeSlot(){
        var slots_user=<?php echo json_encode($detail['timeslots']); ?>;
        Object.keys(slots_user).forEach(function(key) {
            if( slots_user[key].accepted ){
               // $('#selected-time-slot').append("<h3 class='header-text-success text-success'><b> The slot selected by user is "+moment.utc(slots_user[key].start_time).local().format('D')+ " "+ moment.utc(slots_user[key].start_time).local().format('MMM')+" "+ moment.utc(slots_user[key].start_time).local().format('h:mm A')+ " to "+ moment.utc(slots_user[key].end_time).local().format('h:mm A')+ "</b></h3>");
            }
        });
    }

});
</script>
<script src="{{ asset('public/admin/js/approve_candidate.js') }}" type="text/javascript"></script>
@endsection	
