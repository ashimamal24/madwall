<div class="form-body">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group">
					
					<input type="hidden" id="first_start_time_slote_hidden" value="">
					<input type="hidden" id="first_end_time_slote_hidden" value="">
                    {{ Form::label( 'first_start_time_slot', 'First Time Slot: ',['class' => 'control-label']) }} <span class="star">*</span> 
                    <label class="help-block"></label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class='input-group date' id='first_start_time_slote'>
						{{ Form::text( 'first_start_time_slot',null,['class' => 'form-control', 'placeholder'=> 'Start Date and Time' ]) }}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
                    <label class="help-block"></label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class='input-group date' id='first_end_time_slote'>
						{{ Form::text( 'first_end_time_slot',null,['class' => 'form-control', 'placeholder'=> 'End Date and Time' ]) }}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<label class="help-block"></label>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group">
					<input type="hidden" id="second_start_time_slote_hidden" value="">
					<input type="hidden" id="second_end_time_slote_hidden" value="">
                    {{ Form::label( 'second_start_time_slot', 'Second Time Slot: ',['class' => 'control-label']) }} <span class="star">*</span> 
                    <label class="help-block"></label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class='input-group date' id='second_start_time_slote'>
						{{ Form::text( 'second_start_time_slot',null,['class' => 'form-control', 'placeholder'=> 'Start Date and Time' ]) }}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
                    <label class="help-block"></label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class='input-group date' id='second_end_time_slote'>
						{{ Form::text( 'second_end_time_slot',null,['class' => 'form-control', 'placeholder'=> 'End Date and Time' ]) }}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<label class="help-block"></label>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group">
					<input type="hidden" id="third_start_time_slote_hidden" value="">
					<input type="hidden" id="third_end_time_slote_hidden" value="">
                    {{ Form::label( 'third_start_time_slot', 'Third Time Slot: ',['class' => 'control-label']) }} <span class="star">*</span> 
                    <label class="help-block"></label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class='input-group date' id='third_start_time_slote'>
						{{ Form::text( 'third_start_time_slot',null,['class' => 'form-control', 'placeholder'=> 'Start Date and Time' ]) }}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
                    <label class="help-block"></label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class='input-group date' id='third_end_time_slote'>
						{{ Form::text( 'third_end_time_slot',null,['class' => 'form-control', 'placeholder'=> 'End Date and Time' ]) }}

						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<label class="help-block"></label>
                </div>
            </div>
            <input type="hidden" name="timezone" id="timezone" />
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="col-sm-12">
        {{ Form::button( $submitButtonText, [ 'id'=>'add_time_slots','class' => 'btn btn-primary']) }}
        {{ Html::link('admin/list-waitingemployee', 'Cancel', array( 'class' => 'btn btn-primary', 'style' =>'color:#ffffff' ))}}   
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
