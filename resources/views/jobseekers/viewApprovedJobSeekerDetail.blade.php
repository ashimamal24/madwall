@extends('admin.layout')
@section('title')
  Approved or Decline
@endsection
@section('css')
<style>
  .number > span {
    float: left;
   /* overflow-wrap: break-word; */
    width: 100%;
}
.expertise{
    border-top-style: solid;
    border-top-width: 1px;
    margin-top: 10px;
}
.proof-border{
    border-bottom-style: solid;
    border-bottom-width: 1px;
}

.certificate-name {
    float: left;
    overflow-wrap: break-word;
    width: 100%;
}

</style>
@endsection
@section('content')
<h3 class="block" style="text-align:center">
 <b><u>View Details </u></b>
</h3>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue ">
            <div class="caption" style="float:right">
                <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/list-approvedemployee')}}"> Back </a>
            </div>
            <div class="portlet-body util-btn-margin-bottom-5">
                <div class="tab-pane" id="tab_1">
                    <div class="portlet-body form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="well col-md-10">
                                        <span style="color:#228B22; font-size:20px">{{ $edit_approved_jobseeker['first_name']}} {{ $edit_approved_jobseeker['last_name']}}</span><br>
                                        <span><i class="glyphicon glyphicon-envelope"></i> {{ $edit_approved_jobseeker['email']}}</span><br>
                                        <span><i class="fa fa-phone"></i> {{ $edit_approved_jobseeker['country_code'] }}-{{ $edit_approved_jobseeker['phone'] }}</span><br>
                                        <span># {{ $edit_approved_jobseeker['address']}}</span><br>
                                        <span>{{ Carbon\Carbon::parse($edit_approved_jobseeker['dob'])->format('d')}} {{ Carbon\Carbon::parse($edit_approved_jobseeker['dob'])->format('M') }}, {{ Carbon\Carbon::parse($edit_approved_jobseeker['dob'])->format('Y') }}</span><br>
                                        <span>{{ $edit_approved_jobseeker['sin_number']}}</span>
                                    </div>
                                    <div class="well col-md-2">
                                        @if(isset($edit_approved_jobseeker['image']))
                                        <img src="{{$edit_approved_jobseeker['image']}}" class="img-rounded" height="120" width="140" />
                                        @else
                                            {{ Html::image('public/admin/img/user_male2-512.png', 'a picture', array( "height"=>"120", "width"=>"140" )) }}
                                        @endif
                                    </div>  
                                </div>
                                <div class="col-md-12">
                <div class="form-body">
                    <div class="row">
                       <!--Candidate proofs-->
                        <div class="proof-border col-md-12">
                            <div class="col-md-6 tiles">
                                @if( isset( $edit_approved_jobseeker['id_proofs'] ) )
                                    @foreach ( $edit_approved_jobseeker['id_proofs'] as $key => $proofs )
                                    <div class="col-sm-5">
                                        <h6><strong>{{ $proofs['name'] }}</strong></h6>

                                        <a href="{{$proofs['url']}}" target="_blank" >
                                        <div class="tile bg-blue">
                                        <div class="tile-body">
                                        <i class="fa fa-briefcase"></i>
                                        </div>
                                        </div>
                                        </a>
                                        <div class="tile-object">
                                        <div class="number">
                                        <span>{{ $proofs['value'] }}</span>
                                        </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <a class="btn red-sunglo" target="_blank" href="{{$proofs['url']}}"><i class="fa fa-download "></i> Download</a>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="col-sm-5 certificate">
                                    @if(isset($edit_approved_jobseeker['cv_url']))
                                        <h6><strong>Candidate Resume</strong></h6>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['cv_url']}}">
                                            <div class="tile bg-blue">
                                                <div class="tile-body"><i class="fa fa-briefcase"></i></div>
                                            </div>
                                        </a>
                                        <div class="tile-object">
                                        <div class="number">
                                        <span>{{$edit_approved_jobseeker['cv_name']}}</span></div></div><div class="clearfix"></div>
                                        <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['cv_url']}}"><i class="fa fa-download "></i> Download</a>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 tiles">
                                <div class="col-sm-4">
                                    <strong> Average Rating</strong> 
                                </div>
                                <div class="col-sm-4">
                                    <p class="start-color"><strong>
                                        @if( ( $edit_approved_jobseeker['rating'] > 1 ) && ( $edit_approved_jobseeker['rating'] < 1.5 ) )
                                            <i class="fa fa-star"></i> ( {{$edit_approved_jobseeker['rating']}} )
                                        @elseif( ( $edit_approved_jobseeker['rating'] > 1.5 ) && ( $edit_approved_jobseeker['rating'] <= 2 || $edit_approved_jobseeker['rating'] < 2.5 ) )
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i> ( {{$edit_approved_jobseeker['rating']}} )
                                        @elseif( $edit_approved_jobseeker['rating'] == 1.5 )
                                            <i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>  ( {{$edit_approved_jobseeker['rating']}} )
                                        @elseif( ( $edit_approved_jobseeker['rating'] > 2.5 )&& ( $edit_approved_jobseeker['rating'] <= 3 || $edit_approved_jobseeker['rating'] < 3.5 ) )
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  ( {{$edit_approved_jobseeker['rating']}} )
                                        @elseif( $edit_approved_jobseeker['rating'] == 2.5 )
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>  ( {{$edit_approved_jobseeker['rating']}} )
                                        @elseif( ( $edit_approved_jobseeker['rating'] > 3.5 ) && ( $edit_approved_jobseeker['rating'] <= 4 || $edit_approved_jobseeker['rating'] < 4.5 ) )
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  ( {{$edit_approved_jobseeker['rating']}} )
                                        @elseif( $edit_approved_jobseeker['rating'] == 3.5 )
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i> ( {{$edit_approved_jobseeker['rating']}} )
                                        @elseif( $edit_approved_jobseeker['rating'] == 4.5 )
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>  ( {{$edit_approved_jobseeker['rating']}} )
                                        @else
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  ( {{$edit_approved_jobseeker['rating']}} )
                                        @endif
                                    </strong> </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 tiles">
                             
                                @if(isset($edit_approved_jobseeker['downloadEmplyoeeContract']))    
                                <div class="col-sm-5">
                                        <h5><strong>Employee Agreement</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['downloadEmplyoeeContract']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['downloadEmplyoeeContract']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                 @if(isset($edit_approved_jobseeker['downloadEmergencyContact']))    
                                <div class="col-sm-5">
                                        <h5><strong>Emergency Contact</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['downloadEmergencyContact']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['downloadEmergencyContact']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                 @if(isset($edit_approved_jobseeker['downloadDirectDeposit']))    
                                <div class="col-sm-5">
                                        <h5><strong>Direct Deposit</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['downloadDirectDeposit']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['downloadDirectDeposit']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                @if(isset($edit_approved_jobseeker['opentokVideoDownloadUrl']))    
                                <div class="col-sm-5">
                                        <h5><strong>Download Video</strong></h5>
                                        <a target="_blank" href="{{$edit_approved_jobseeker['opentokVideoDownloadUrl']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$edit_approved_jobseeker['opentokVideoDownloadUrl']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 
                            
                        </div>


                        <div  class="col-md-12">
                            <h4><b><u>Degree/Diploma Certificate</b></u></h3><br>
                            @if( $edit_approved_jobseeker['additional_documents'] )
                                @foreach( $edit_approved_jobseeker['additional_documents'] as $key => $additional_documents )
                                    <div class="col-md-2 tiles">
                                        @if( isset( $additional_documents['url'] ) )
                                            <a target="_blank" href="{{ $additional_documents['url'] }}">
                                                <div class="tile bg-blue">
                                                    <div class="tile-body">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                </div>
                                            </a>
                                            <p class="certificate-name">{{$additional_documents['name']}}</p>
                                            
                                            <a target="_blank" class="btn red-sunglo" href="{{ $additional_documents['url'] }}">Download</a>
                                        @endif
                                            {{ Form::textarea("certificate_description[$key]",$additional_documents['description'],['class' => 'form-control short_textarea description', 'readonly'=>'true' ]) }}
                                    </div>
                                @endforeach
                            @else
                                No certificate provided yet.
                            @endif
                        </div>
                        <div class="expertise col-md-12">
                            <h4><b><u>Expertise</u></b></h4><br>
                            <div class="col-md-3">
                                @if(isset( $edit_approved_jobseeker['industry'] ) )
                                    <b>Industry:</b> 
                                    
                                    @php $indstry ='';@endphp 
									@foreach( $edit_approved_jobseeker['industry'] as $k => $ind )
										@php $indstry .= $ind['name'].",";@endphp
									@endforeach
									{{ trim($indstry,',') }} 
                                    
                                    <!-- $edit_approved_jobseeker['category_object'][0]['name'] -->
                                @endif
                            </div>
                            
                            
                            <div class="col-md-3">
                                @if(isset( $edit_approved_jobseeker['category_object'] ) )
                                    <b>Category:</b> 
                                    
                                    @php $cat ='';@endphp 
									@foreach( $edit_approved_jobseeker['category_object'] as $k => $categories )
										@php $cat .= $categories['name'].",";@endphp
									@endforeach
									{{ trim($cat,',') }} 
                                    
                                    <!-- $edit_approved_jobseeker['category_object'][0]['name'] -->
                                @endif
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    @if(isset($edit_approved_jobseeker['subcategories']))
                                        <b>Subcategories:</b>
                                         @php $subcat ='';@endphp 
                                            @foreach( $edit_approved_jobseeker['subcategories'] as $key => $subcategories )
                                                @php $subcat .= $subcategories['name'].",";@endphp
                                            @endforeach
                                            {{ trim($subcat,',') }} 
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group skills">
                                    @if(isset($edit_approved_jobseeker['skills']))
                                        <b>Skills:</b> 
                                        @php $skill ='';@endphp
                                        @foreach( $edit_approved_jobseeker['skills'] as $skills )
                                           @php $skill .= $skills['name'].",";@endphp
                                        @endforeach 
                                        {{ trim($skill,',') }}    
                                    @endif                             
                                </div>
                            </div> 
                        </div>
                        @if(isset($edit_approved_jobseeker['other_documents']))
                                <div class="expertise tiles col-md-12">
                                <h4><b><u>Other Documents</u></b></h4>
                                    @foreach( $edit_approved_jobseeker['other_documents'] as $otherdocument )
                                        <div class="col-sm-3">
                                            <h6><strong>{{$otherdocument['name']}}</strong></h6>
                                            <a target="_blank" href="{{$otherdocument['url']}}" target="_blank">
                                                <div class="tile bg-blue">
                                                    <div class="tile-body">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="expertise work-history col-md-12">
                                <h4><b><u>Work History</u></b></h4>
                                <div class="col-lg-3">
                                    <div class="dashboard-stat blue-madison">
                                        <div class="visual">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="details">
                                            <div style="font-size: 21px" class="number">
                                                <span>Total Jobs</span><br> {{$completed_jobs}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="dashboard-stat blue-madison">
                                        <div class="visual">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="details">
                                            <div style="font-size: 21px" class="number">
                                                <span>Hours Worked</span><br> {{$total_working_hour}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="dashboard-stat blue-madison">
                                        <div class="visual">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="details">
                                            <div style="font-size: 21px" class="number">
                                                <span>Earned</span><br> {{$total_earning}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div> 
</div>

@endsection 
@section('js')
@endsection 
