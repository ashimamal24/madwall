@extends('admin.layout')
@section('title')
	Employee Details
@endsection
@section('css')

<link href="{{ asset( 'public/admin/css/jquery-ui.css' ) }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset( 'public/admin/css/bootstrap-datetimepicker.css' ) }}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .header-text-success{
        color: #008000;
    }
    .header-text-failure{
        color:#FF0000;
    }
    .proof-detail{
        border-right-style: solid;
        border-right:1px double #000000;
        padding-bottom: 10px;
    }
    #flip{
        font-weight: bold;    
        text-align: center;
    }
</style>
@endsection

@section('content')
<h3 class="block" style="text-align:center">
 <b><u>View Details</u></b>
</h3>

<div class="row">
    <div class="col-md-4">
        <div class="well">
        <span style="color:#228B22; font-size:20px">{{ $detail['first_name']}} {{ $detail['last_name']}}</span><br>
        <span><i class="glyphicon glyphicon-envelope"></i> {{ $detail['email']}}</span><br>
        <span><i class="fa fa-phone"></i>  {{ $detail['country_code'] }}-{{ $detail['phone']}}</span><br>
    </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
	
			
            <div class="caption" style="float:right">
                    <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/list-waitingemployee')}}"> Back </a>
            </div>
		
		<div class="portlet-body form">
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Step-1</a></li>
                    <li><a href="#tabs-2">Step 2</a></li>
                    <li><a href="#tabs-3">Step 3</a></li>
                    <li><a href="#tabs-4">Step 4</a></li>
                </ul>
                <div id="tabs-1">
                    <div class="row">
                        <div class="col-md-offset-2  col-md-2">
                           
                        </div>
                        <div class="col-md-4">
                            <h3><strong><u>Basic Details</u></strong></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3  col-md-3">
                            <div class="form-group">
                                <label class="control-label"><strong>Address: </strong></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <span>@if(isset($detail['address'])) # {{ $detail['address']}}@else Not Provided Yet @endif</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-3">
                            <div class="form-group">
                                <label class="control-label"><strong>DOB: </strong></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <span>
									@if(isset($detail['dob'])){{ Carbon\Carbon::parse($detail['dob'])->format('d') }} 
										{{Carbon\Carbon::parse($detail['dob'])->format('F')}}, {{Carbon\Carbon::parse($detail['dob'])->format('Y') }}
									@else 
										Not Provided Yet  
									@endif</span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-offset-3  col-md-3">
                            <div class="form-group">
                                <label class="control-label"><strong>SIN:</strong> </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <span>@if(isset($detail['sin_number'])){{ wordwrap($detail['sin_number'],3,' ', true)}} @else Not Provided Yet @endif</span>
                            </div>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-offset-3  col-md-3">
                            <div class="form-group">
                                <label class="control-label"><strong>Where did you here about us:</strong> </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <span>@if(isset($detail['source'])){{ $detail['source']}}@else Not Provided Yet @endif</span>
                            </div>
                        </div>
                    </div>
                    @if(isset($detail['id_proofs']) && count($detail['id_proofs']))

                    <div class="row row portlet light bordered form-fit">
                        <div class="col-md-12">
                            <div class="col-md-6 tiles proof-detail">
                                    @foreach ( $detail['id_proofs'] as $proofs )
                                        <div class="col-sm-5">
                                            <h5><strong>{{ $proofs['name'] }}</strong></h5>
                                            @if(isset($proofs['url']))
                                                <a target="_blank" href="{{$proofs['url']}}">
                                                    <div class="tile bg-blue">
                                                        <div class="tile-body">
                                                            <i class="fa fa-briefcase"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                @else
                                                <a target="_blank" href="#">
                                                    <div class="tile bg-blue">
                                                        <div class="tile-body">
                                                            <i class="fa fa-briefcase"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                            @endif
                                            @if(isset($proofs['value']))
                                                <div class="tile-object">
                                                    <div class="name">
                                                        <p>{{ $proofs['value'] }}</p>
                                                    </div>
                                                </div>
                                                @else
                                                 <div class="tile-object">
                                                    <div class="name">
                                                        <p></p>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            <div class="clearfix"></div>
                                            @if(isset($proofs['url']))
                                                <a target="_blank" class="btn red-sunglo" href="{{$proofs['url']}}"><i class="fa fa-download "></i> Download</a>
                                            @else
                                                <a target="_blank" class="btn red-sunglo" href="#"><i class="fa fa-download "></i> Download</a>
                                            @endif
                                        </div>
                                    @endforeach

                                
                            </div>


                            <div class="col-md-6 tiles">
                                @if(isset($detail['cv_url']))
                                    <div class="col-sm-5">
                                        <h5><strong>Candidate CV</strong></h5>
                                        <a target="_blank" href="{{$detail['cv_url']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['cv_url']}}"><i class="fa fa-download "></i> Download</a>
                                    </div>
                                @endif 

                                 @if(isset($detail['downloadEmplyoeeContract']))    
                                <div class="col-sm-5">
                                        <h5><strong>Employee Agreement</strong></h5>
                                        <a target="_blank" href="{{$detail['downloadEmplyoeeContract']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['downloadEmplyoeeContract']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 

                                 @if(isset($detail['downloadEmergencyContact']))    
                                <div class="col-sm-5">
                                        <h5><strong>Emergency Contact</strong></h5>
                                        <a target="_blank" href="{{$detail['downloadEmergencyContact']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['downloadEmergencyContact']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                 @if(isset($detail['downloadDirectDeposit']))    
                                <div class="col-sm-5">
                                        <h5><strong>Direct Deposit</strong></h5>
                                        <a target="_blank" href="{{$detail['downloadDirectDeposit']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['downloadDirectDeposit']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 


                                @if(isset($detail['opentokVideoDownloadUrl']))    
                                <div class="col-sm-5">
                                        <h5><strong>Download Video</strong></h5>
                                        <a target="_blank" href="{{$detail['opentokVideoDownloadUrl']}}">
                                        
                                        <div class="tile bg-blue">
                                            <div class="tile-body">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                        </div>
                                        </a>
                                    <a class="btn red-sunglo" target="_blank" href="{{$detail['opentokVideoDownloadUrl']}}"><i class="fa fa-download "></i> Download</a>
                                </div>
                                @endif 
                                


                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div id="tabs-2">
                    <div class="row">
                        <div class="col-md-offset-2  col-md-2">
                           
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><strong><u>MadWall Quiz</u></strong></h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group">
                        @if( isset($detail['madwall_quiz_answer']) )
                            @foreach( $detail['madwall_quiz_answer']['quiz'] as $key => $mw_quiz_answer )
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                            <a class="accordion-toggle" >
                                                <b>Question {{$key+1}}:</b> {{ $mw_quiz_answer['question'] }} </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <b>Answer:</b> {{ $mw_quiz_answer['answer'] }}
                                                @if( $mw_quiz_answer['quiz_result'] )
                                                    <i class="glyphicon glyphicon-ok" style="color: #008000; float: right;" aria-hidden="true"></i>
                                                @else
                                                    <i class="glyphicon glyphicon-remove" style="color:#FF0000; float: right;" aria-hidden="true"></i>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            <br>
                            <div class="col-md-offset-2 col-md-10">
                                <div class="col-md-6"><b>Quiz % </b></div>
                                <div classs="col-md-6">
                                    @if($detail['madwall_quiz_answer']['over_all_percentage'] >= 50)
                                        <span class="header-text-success text-success"><b>{{ $detail['madwall_quiz_answer']['over_all_percentage'] }} %</b></span>
                                    @else
                                        <span class="header-text-failure text-danger"><b>{{ $detail['madwall_quiz_answer']['over_all_percentage'] }} %</b></span>
                                    @endif
                                </div>
                            </div>
                            @if( $detail['madwall_quiz_answer']['over_all_percentage'] >= 50 )
                                <div class="col-md-offset-3 header-text-success text-success"><h3><b>Madwall Handbook Quiz Cleared!</b></h3></div>
                            @else
                                <div class="col-md-offset-3 header-text-failure text-danger"><h3><b>MadWall Quiz not cleared yet</b></h3></div>
                            @endif
                        @else
                            <p>Not Performed Yet</p>
                        @endif
                    </div>
                </div>
                <div id="tabs-3">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-2">
                           
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><strong><u>Health & Safety Quiz</u></strong></h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group">
                        @if(isset($detail['madwall_health_answer']) )
                            @foreach( $detail['madwall_health_answer']['quiz'] as $key => $mw_health_answer )
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                            <a class="accordion-toggle">
                                            <b>Question {{$key+1}}:</b> {{ $mw_health_answer['question'] }} </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                               <b>Answer:</b> {{ $mw_health_answer['answer'] }}
                                                @if( $mw_health_answer['quiz_result'] )
                                                    <i class="glyphicon glyphicon-ok" style="color: #008000; float: right;" aria-hidden="true"></i>
                                                @else
                                                    <i class="glyphicon glyphicon-remove" style="color:#FF0000; float: right;" aria-hidden="true"></i>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            <br>
                            <div class="col-md-offset-2 col-md-10">
                                <div class="col-md-6"><b>Quiz % </b></div>
                                <div classs="col-md-6">
                                    @if( $detail['madwall_health_answer']['over_all_percentage'] >= 80 )
                                        <span class="header-text-success text-success"><b>{{$detail['madwall_health_answer']['over_all_percentage']}} %</b></span>
                                    @else
                                        <span class="header-text-failure text-danger"><b>{{$detail['madwall_health_answer']['over_all_percentage']}} %</b></span>
                                    @endif
                                </div>
                            </div>
                            @if( $detail['madwall_health_answer']['over_all_percentage'] >= 80 )
                                <div class="col-md-offset-3 header-text-success text-success"><h3><b>Health &amp; Safety Quiz cleared.</b></h3></div>
                            @else
                                <div class="col-md-offset-3 header-text-failure text-danger"><h3><b>Health & Safety Quiz not cleared yet.</b></h3></div>
                            @endif
                        @else
                            <p>Not Performed Yet</p>
                        @endif
                    </div>
                </div>


                <div id="tabs-4">
                    @include('flash::message')
                    @if($detail['profile_complete'] >= 6 )

                        @if(count($detail['timeslots']))
                            <div id="data">
								<?php
									if(!empty($detail['timeslots'])){
										
										
										foreach($detail['timeslots'] as $key => $timeSlot){
											if($key==0){
												$slot = "First"; 
											}
											if($key==1){
												$slot = "Second"; 
											}
											if($key==2){
												$slot = "Third"; 
											}
											
											?>
											<div class='form-body'>
												<div class='row'>
													<div class='col-md-12'>
														<div class='col-md-4'>
															<div class='form-group'>
																<strong><?php echo $slot ?> Time Slot:</strong>
																</div>
															</div>
															<div class='col-md-4'>
																<div class='form-group'>
																	<div class='input-group time-slots-start'>					{{Carbon\Carbon::parse($detail['timeslots'][$key]['start_time'])->format('Y-M-d h:i A')}}
																		
																	</div>
																</div>
															</div>
															<div class='col-md-4'>
																<div class='form-group'>
																	<div class='input-group'>		{{Carbon\Carbon::parse($detail['timeslots'][$key]['end_time'])->format('Y-M-d h:i A')}}
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<?php
										}
									}

								?>
                                <div id='flip'><a style='color: #0b4cc4;'>Resend Time Slots <br><i id='icon' class="fa fa-angle-double-down fa-lg"></i></a></div>
                                <div id='panel'>
                                    <div class="row">
                                    <div class="col-md-offset-2  col-md-2"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h3><strong><u>Set up a Conference Call</u></strong></h3>
                                        </div>
                                    </div>
                                    </div>
                                    {{ Form::open(array( 'method' => 'POST','url' => '/admin/save-time-slot','id'=>'assigntimeslotform')) }}
                                    @include('jobseekers.jobseekerform',['submitButtonText' => 'Submit'])
                                    {{ Form::hidden( 'userid', $detail['_id'],['id'=>'userid'] ) }}
                                    {{ Form::close() }}
                                </div>
                            </div>
                           
                        @else
                            <div class="row">
                                <div class="col-md-offset-2  col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h3><strong><u>Set up a Conference Call</u></strong></h3>
                                    </div>
                                </div>
                            </div>
                            {{ Form::open(array( 'method' => 'POST','url' => '/admin/save-time-slot','id'=>'assigntimeslotform')) }}
                            @include('jobseekers.jobseekerform',['submitButtonText' => 'Submit'])
                        {{ Form::hidden( 'userid', $detail['_id'],['id'=>'userid'] ) }}
                        {{ Form::close() }}
                        @endif
                    @else
                  
                        <p>Sorry, Employee has not cleared the previous steps yet.</p>
                    @endif
                   	
                </div>
            </div>
        </div>	
	</div>
</div>

@endsection	
@section('js')
<script src="{{ asset( 'public/admin/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset( 'public/admin/js/timing_slots.js') }}"></script>
<script>

jQuery(document).ready(function() {

    //jQuery("#panel").slideUp();
    //$( "#panel" ).hide();
    jQuery("#flip").click(function(){
       $(this).find('#icon').toggleClass('fa-angle-double-up fa-angle-double-down')
        jQuery("#panel").slideToggle("slow");
    });
   

    
    var slots=<?php echo json_encode($detail['timeslots']); ?>;
   
    if( slots.length > 0 ){
        createStep4();
    }
    function createStep4(){
        var slots_user=<?php echo json_encode($detail['timeslots']); ?>;
        Object.keys(slots_user).forEach(function(key) {
           // console.log(key, slots_user[key].start_time);
        // console.log(moment.utc(slots_user[key].start_time).local().format('YY MM DD h:m A'));    
           // console.log( moment.utc(slots_user[key].start_time).local().format('YY MM DD h:m A')) );
            
            var slot=""
            if(key==0){
                var slot = "First"; 
            }
            if(key==1){
                var slot = "Second"; 
            }
            if(key==2){
                var slot = "Third"; 
            }
            //$('#data').append("<div class='form-body'><div class='row'><div class='col-md-12'><div class='col-md-4'><div class='form-group'><strong>"+slot+" Time Slot:</strong></div></div><div class='col-md-4'><div class='form-group'><div class='input-group time-slots-start'>"+moment.utc(slots_user[key].start_time).local().format('YYYY-MM-DD hh:mm A')+"</div></div></div><div class='col-md-4'><div class='form-group'><div class='input-group'>"+moment.utc(slots_user[key].end_time).local().format('YYYY-MM-DD hh:mm A')+"</div></div></div></div></div></div>");

        });

    }


    var profile_complete=<?php echo json_encode($detail['profile_complete']); ?>;
    var slot_requested_additional=<?php echo json_encode( $detail['slot_requested_additional']); ?>;
    $( "#tabs" ).tabs();
    if( profile_complete == 1 || profile_complete == 4 ){
        $("#tabs").tabs("option", "active", 0 );
    }

    if( profile_complete == 5 ){
        $("#tabs").tabs("option", "active", 1 );
    }

    if( profile_complete == 6 ){
        $("#tabs").tabs("option", "active", 2 );
    }

    if( slots.length > 0 || slot_requested_additional ){
         
    }

   if(getUrlParameter('tabs') == 3){
        $("#tabs").tabs("option", "active", 3 );
    }

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };



    $('#timezone').val(moment.tz.guess());
    $('#add_time_slots').click(function(){
        var first_slot_start    =  $('#first_start_time_slote').find("input").val();
        var first_slot_end      =  $('#first_end_time_slote').find("input").val();
        var second_slot_start   =  $('#second_start_time_slote').find("input").val();
        var second_slot_end     =  $('#second_end_time_slote').find("input").val();
        var third_slot_start    =  $('#third_start_time_slote').find("input").val();
        var third_slot_end      =  $('#third_end_time_slote').find("input").val();
        var timezone      =  $('#timezone').val();

       if(first_slot_start != '' && first_slot_end != '' && second_slot_start != '' && second_slot_end != '' && third_slot_start != '' && third_slot_end != '') {
            var all_timing_slots = {};
            all_timing_slots[0] = {};
            all_timing_slots[0]['start_time']=first_slot_start;
            all_timing_slots[0]['end_time']=first_slot_end;
            all_timing_slots[1] = {};
            all_timing_slots[1]['start_time']=second_slot_start;
            all_timing_slots[1]['end_time']=second_slot_end;
            all_timing_slots[2] = {};
            all_timing_slots[2]['start_time']=third_slot_start;
            all_timing_slots[2]['end_time']=third_slot_end;
            var token ="{{ csrf_token() }}"; 

            var id = $('#userid').val();
            $.ajax({
                dataType    : 'json',
                method      : 'post',
                url : path+'admin/save-time-slot',
                data: { id:id, _token: token,timezone:timezone,slots:all_timing_slots },
                beforeSend  : function() {
                    addLoader();
                },
                success     : function(data) {
                    window.location = path+'admin/assign-time-slot/'+id+'?tabs=3';
                },
                error       : function(xhr, ajaxOptions, thrownError) {
                    removeLoader();
                    $( "#addcategoryform .form-group" ).removeClass( "has-error" );
                    $( ".help-block" ).hide();
                    
                    /* Start each loop*/
                    $.each( xhr.responseJSON, function( i, obj ) {
                    
                        $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                        $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                        $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);

                        
                    });
                } /* End error() */
            });

        } else {
            bootbox.alert( 'Please specify all 3 time slots' );
        }
        
    });
});
</script>

@endsection	
