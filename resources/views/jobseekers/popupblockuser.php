<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>View Blocked User</b></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>Name: </th>
                            <td id="name"></td>
                        </tr>
                        <tr>
                            <th>Email : </th>
                            <td id="email"></td>
                        </tr>
                        
                        <tr>
                            <th>Mobile Number: </th>
                            <td id="mob"></td>
                        </tr>
                        
                        <tr>
                            <th>Address: </th>
                            <td id="address"></td>
                        </tr>

                        <tr>
                            <th>SIN: </th>
                            <td id="sin"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn green" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>