@extends('admin.layout')
@section('title')
	
@endsection
@section('heading')
	Add Commission
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Add Commission
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
		 	@if (count($errors) > 0)
	    	@endif
			{{ Form::open(array( 'method' => 'POST','url' => '/admin/add-commission','id'=>'addcommissionform')) }}
				@include('commission.commissionform',['submitButtonText' => 'Add Commission'])
	    	{{ Form::close() }}
		</div>	
	</div>
</div>

@endsection	
@section('js')
<script>
jQuery(document).ready(function() {     
	$('#addcommission').click(function(){
		var formData = new FormData($('#addcommissionform')[0]);
		$.ajax({
			dataType: 'json',
			method:'post',
			processData: false,
			contentType: false,
			url: path+'admin/add-commission',
			data: formData,
			beforeSend : function() {
				//addLoader();
			},
			success  : function(data) {
				window.location = path+'admin/list-commissions';
			},
			error: function(xhr, ajaxOptions, thrownError) {
				//removeLoader();
				$("#addcommissionform .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='category_id'){
						$('.category_id').addClass('has-error');
						$('.category_id').find('label.help-block').slideDown(400).html(obj);
					}
					
				});
			}
		});
	});

	$('#category_id').change(function(){
		$('#category_name').val($('#category_id').find(":selected").text() );
	});

});
</script>

@endsection	
