<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Commission Details</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>Category: </th>
                            <td id="category_name"></td>
                        </tr>
                        <tr>
                            <th>Commission: </th>
                            <td id="commission_amount"></td>
                        </tr> 
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn green" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>