@extends('admin.layout')
@section('title')
	Edit Commission
@endsection
@section('heading')
	Edit Commission
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Commission
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_commission, ['method' => 'POST','url' => '/admin/edit-commission','id'=>'editcommission']) }}
				@include('commission.commissionform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			{{ Form::hidden( 'idedit', $edit_commission['_id'] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script>
jQuery(document).ready(function() {     
	$('#addcommission').click(function(){
		var formData = new FormData($('#editcommission')[0]);
		var id = formData.get('idedit');
		$.ajax({
			dataType	: 'json',
			method		: 'post',
			processData	: false,
			contentType	: false,
			url			: path+'admin/edit-commission/'+id,
			data 		: formData,
			
			beforeSend	: function() {
				addLoader();
			},
			
			success		: function(data) {
				if( data.success == true ){
					window.location = path+'admin/list-commissions';
					$('.alert-succes').fadeOut('fast');
            		
				}
				if( data.success == false ){
					window.location = path+'admin/list-commissions';
				}
			},

			error 		: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				
				$("#editcommission .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					if(i=='category_id'){
						$('.category_id').addClass('has-error');
						$('.category_id').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
		});
	});


	$('#category_id').change(function(){
		$('#category_name').val($('#category_id').find(":selected").text() );
	});
});
</script>

@endsection