<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group category_id">
                {{ Form::label( 'category_id', 'Category: ',['class' => 'control-label'] ) }} <span class="star">*</span>
                {{ Form::hidden( 'category_name' , null, ['class' => 'form-control' , 'id' => 'category_name' ] ) }}
                @if(count( $categories )) 
                    {{ Form::select( 'category_id', array_replace([ ''=>'Select' ], $categories ), null, ['class' => 'form-control','id'=>'category_id'] ) }}
                @else
                    {{ Form::select( 'category_id', [''=>'No Category added by admin'], null, ['class' => 'form-control','id'=>'category_id'] ) }}
                @endif
                <label class="help-block"></label>
            </div>
        </div>    
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label( 'commission_amount', 'Commission Amount: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'commission_amount',null,['class' => 'form-control', 'maxlength'=> '15']) }}
                <label class="help-block"></label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group status">
                {{ Form::label('status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) }} 
                <label class="help-block"></label>
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="col-sm-12">
        {{ Form::button( $submitButtonText, ['id'=>'addcommission','class' => 'btn btn-primary']) }}
        {{ Html::link('admin/list-commissions', 'Cancel', array( 'class' => 'btn btn-primary' ))}}   
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
