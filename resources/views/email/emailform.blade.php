<div class="form-body">
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label( 'name', 'Title: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'name',null,['class' => 'form-control', 'maxlength'=> '40']) }}
                <label class="help-block"></label>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-group status">
                {{ Form::label('status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span>
                @if(isset($edit_emails['status']))
                    @if($edit_emails['status'] == 0)
                        {{ Form::select('status', array('1' => 'Active', '0' => 'Inactive'),0,['class' => 'form-control']) }}
                    @else
                        {{ Form::select('status', array('1' => 'Active', '0' => 'Inactive'),1,['class' => 'form-control']) }}
                    @endif
                @else
                    {{ Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) }}
                @endif
                
                <label class="help-block"></label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label( 'subject', 'Subject: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'subject',null,['class' => 'form-control', 'maxlength'=> '40']) }}
                <label class="help-block"></label>
            </div>
        </div>
         <div class="col-md-4">
            <div class="form-group status">
                {{ Form::label('attributes', 'Attributes: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::select('attributes', array('@name@' => '@name@', '@username@' => '@username@','@otp@' => '@otp@', '@link@' => '@link@','@company@' => '@company@', ),null,['multiple' => 'multiple' ,'class' => 'form-control attrubute']) }}
                   
                
                <label class="help-block"></label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label('content', 'Content: ',['class' => 'control-label',]) }} <span class="star">*</span>
                {{ Form::textarea('content',null,[ 'id'=>'emailcontent', 'maxlength'=> '200' , 'class' => 'form-control']) }}
                <label class="help-block"></label>
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
        {{ Form::button( $submitButtonText, [ 'id'=>'addemail','class' => 'btn btn-primary']) }}
        {{ Html::link('admin/list-emails', 'Cancel', array( 'class' => 'btn btn-primary' ))}}   
     <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
