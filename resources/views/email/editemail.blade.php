@extends('admin.layout')
@section('title')
	Edit Email
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
@endsection
@section('heading')
	Edit Email
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Email
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_emails, ['method' => 'POST','url' => '/admin/edit-email','id'=>'editemail']) }}
				@include('email.emailform',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			{{ Form::hidden( 'idedit', $edit_emails['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')

 <script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<!-- <script src="//cdn.ckeditor.com/4.7.1/basic/ckeditor.js"></script>
 --><script>
$(document).ready(function() {
	
	/*CKEDITOR.replace( 'emailcontent',
         {
          customConfig : 'config.js',
          toolbar: [
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
			{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
			
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
			{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
			{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
			
			{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
			{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
			{ name: 'others', items: [ '-' ] },
			{ name: 'about', items: [ 'About' ] }
		],
    })*/

    $('.attrubute').change( function(){
		 
		var emailcontent = $('#textarea').val();
		$('#textarea').append( emailcontent + $(this).val() );
    	
    });

	$('#addemail').click(function(){
		
	    /*for (instance in CKEDITOR.instances) {
	        CKEDITOR.instances[instance].updateElement();
	    }*/
		var id = $('#idedit').val();
		$("#editemail").ajaxSubmit({
			method:'post',
			dataType: 'json',
			url: path+'admin/edit-email/'+id,
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				if( data.success == true ){
					window.location = path+'admin/list-emails';
				}
				if( data.success == false ){
					window.location = path+'admin/list-emails';
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#editcategory .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('file[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('file[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
				});		
			}
        });		
	});

});
</script>
<script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/editor-settings.js') }}" type="text/javascript"></script>
@endsection	
