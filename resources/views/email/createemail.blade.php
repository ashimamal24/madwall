@extends('admin.layout')
@section('title')
	Add Email
@endsection

@section('css')
<link href="{{ asset( 'public/admin/css/editor.css' ) }}" type="text/css" rel="stylesheet"/>
@endsection

@section('heading')
	Add Email
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Add Email
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			@if (count($errors) > 0)
			@endif
			{{ Form::open(array( 'method' => 'POST','url' => '/admin/add-email','id'=>'addemailform')) }}
				@include('email.emailform',['submitButtonText' => 'Add Email'])
		    {{ Form::close() }}
		</div>	
	</div>
</div>

@endsection	
@section('js')
<script src="{{ asset( 'public/admin/js/editor.js' ) }}"></script>
<script src="{{ asset('public/admin/js/editor-settings.js') }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {     
	
	$('#addemail').click(function(){
		var formData = new FormData($('#addemailform')[0]);
		 
		$.ajax({
			dataType 	: 'json',
			method		:'post',
			processData	: false,
			contentType	: false,
			url			: path+'admin/add-email',
			data 		: formData,

			beforeSend	: function() {
				addLoader();
			},

			success		: function(data) {
				window.location = path+'admin/list-emails';
			},

			error 		: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#addemailform .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('file[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('file[name="'+i+'"]').closest('.form-group').find('label.help-block');slideDown(400).html(obj);
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
		});
	});
});
</script>
@endsection	
