
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MadWall | Error</title>
	<link href="{{asset('public/employer/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('public/employer/css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('public/employer/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('public/employer/css/owl.carousel.css')}}" rel="stylesheet">
	<link  href="{{asset('public/employer/css/developer.css')}}" rel="stylesheet">
	<link  href="{{asset('public/admin/css/chosen.css')}}" rel="stylesheet">
	<link  href="{{asset('public/employer/css/intlTelInput.css')}}" rel="stylesheet">
	<!-- <link  href="{{asset('public/employer/css/demo.css')}}" rel="stylesheet"> -->
	<link  href="{{asset('public/employer/css/main.css')}}" rel="stylesheet">
	<script src="{{asset('public/employer/js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/bootstrap.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/owl.carousel.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/jquery.main.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/wow.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/jquery.malihu.PageScroll2id.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/employer/js/intlTelInput.js')}}" type="text/javascript"></script>
	<link rel="shortcut icon" href="{{ url('public/logos/favicon.ico') }}"/>
	</head>
<body>
	<div class="loader-section">
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
	</div>
<div class="wrapper">
	<div class="main"> 
		<!-- Header section -->
		<header class="header-main reset_password wow fadeIn ">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 colo-sm-3">
					<div class="logo"><a href="{{url('/')}}">
					<a href="{{url('/')}}"><img src="{{asset('public/employer/images/logo_dash.png')}}" alt=""></a>
					</a></div>
					</div>
					<div class="col-lg-9 col-md-9 colo-sm-9"> <a href="#" class="drop-opener pull-right"> <span></span> <span></span> <em class="border"></em> </a>
						<div class="menu_bar">
							<div class="navSection">
								<div class="nav-holder" id="menu-drop">
								
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="inner_banner_sec">
					<div class="row">
						<h1><b>Error</b></h1>
					</div>
				</div>
			</div>
		</header>
		<!-- --------------- check reset token is valid or not ------------- -->
		
		
		
		
		
		<!-- --------------- end check here ---------- -->
		<div class="main_sub_sec">
			
				<div class="reset_sec">
					<div class="container">
						<div class="col-md-offset-3 col-md-6">
							
								<h4>{{$message}}</h4>
							
						</div>
					</div>
				</div>
			<!-- endif -->
		</div>
	</div>


<!-- END CONTENT -->
@include('employer.promo.promo_footer')
<!-- footer section end --> 
</div>
	
	<script type="text/javascript">

	wow = new WOW(
	{
		animateClass: 'animated',
		offset:       100,
		callback:     function(box) {
		console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
		}
	}
	);
	wow.init();
	
	
	
	
	
	</script>
	
</body>
</html>

