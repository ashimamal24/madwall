<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>View CMS Details</b></h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; height: 435px;">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Name: </th>
                                <td id="name"></td>
                            </tr>
                            <tr>
                                <th>Description: </th>
                                <td id="content"></td>
                            </tr>
                            <tr>
                                <th>Type: </th>
                                <td id="type"></td>
                            </tr>                            
                        </tbody>
                    </table>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
