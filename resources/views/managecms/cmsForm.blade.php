<div class="form-body">
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label( 'name', 'Name: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'name',null,['class' => 'form-control', 'maxlength'=> '40']) }}
                <label class="help-block"></label>
            </div>
           
        </div>
        <!-- <div class="col-md-6">
            <div class="form-group file_url">
                {{ Form::label( 'file_url', 'File: ',['class' => 'control-label']) }} <span class="star">*</span>
                
                @if( isset ($edit_cms['pdf_file_name']) )
                
                <p class="hidden-file-name-readonly">Uploaded File : {{ $edit_cms['pdf_file_name'] }}</p>
                @endif

                {{ Form::file( 'file_url', $attributes = array( 'class' => 'form-group', 'id' =>'file_url' )) }}
                {{ Form::hidden('pdf_file_name',null,[ 'class' => 'hidden-file-name' ]) }}
                {{ Form::hidden('pdf_file_url',null,[ 'id' => 'hidden-file-url' ]) }}
              <label class="help-block"></label>
            </div>
        </div> -->
        <div class="col-md-4">
             <div class="form-group">
                {{ Form::label('alias', 'Alias: ',['class' => 'control-label',]) }} <span class="star">*</span>
                @if( isset ($edit_cms['alias']) )
                {{ Form::text('alias',null,['class' => 'form-control','readonly' => 'true' ]) }}
                @else
                {{ Form::text('alias',null,['class' => 'form-control', 'maxlength'=> '40']) }}
                @endif
                <label class="help-block"></label>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label('content', 'Description: ',['class' => 'control-label']) }} <span class="star">*</span>
                {{ Form::textarea('content',null,[ 'id'=>'content', 'maxlength'=> '200' , 'class' => 'form-control ckeditor']) }}
                <label class="help-block"></label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
               {{ Form::label('type', 'Type: ',['class' => 'control-label']) }} <span class="star">*</span>
                @if(isset($edit_cms['type']))
                    @if($edit_cms['type'] == 'web')
                        {{ Form::select('type', array('web' => 'Web', 'mobile' => 'Api', 'both' => 'Both'),'web',['class' => 'form-control', 'disabled' => 'true' ]) }}
                    @elseif($edit_cms['type'] == 'mobile')
                        {{ Form::select('type', array('web' => 'Web', 'mobile' => 'Api', 'both' => 'Both'),'mobile',['class' => 'form-control', 'disabled' => 'true' ]) }}
                    @else
                        {{ Form::select('type', array('web' => 'Web', 'mobile' => 'Api', 'both' => 'Both'),'both',['class' => 'form-control', 'disabled' => 'true']) }}

                    @endif
                @else
                    {{ Form::select('type', array('web' => 'Web', 'mobile' => 'Api', 'both' => 'Both'),null,['class' => 'form-control']) }}
                @endif
               
                <label class="help-block"></label>
            </div>
        </div>
        <!-- <div class="col-md-4">
            <div class="form-group status">
                {{ Form::label( 'status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span>
                @if(isset($edit_cms['status']))
                    @if($edit_cms['status'] == 0)
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),0,['class' => 'form-control']) !!}
                    @else
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),1,['class' => 'form-control']) !!}
                    @endif
                @else
                    {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) !!}
                @endif
              <label class="help-block"></label>
            </div>
        </div> -->
    </div>
</div>

<div class="box-footer">
     <div class="col-md-4"></div>
    <div class="col-sm-8">
        {{ Form::button( $submitButtonText, ['id'=>'addcms','class' => 'btn btn-primary']) }}
        {{ Html::link('admin/list-cms', 'Cancel', array( 'class' => 'btn btn-primary' ))}}   
    <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
