<p style="float:right;margin-top:-32px">
	<b>Payment paid between {{Carbon\Carbon::parse($showing_date)->format('M-d-Y') }} to {{Carbon\Carbon::parse($end_date)->format('M-d-Y') }}</b>
</p>

<table class="table table-striped table-bordered table-hover"  id="datatable_ajax_for_recive_payment">
    <thead>
        <tr role="row" class="heading">
            <th width="1%">No.</th>
            <th width="10%">Employer Name</th>
            <th width="10%">Job Name</th>
            <th width="10%">Number of Persons </th>
            <th width="10%">Total Hours</th>
            <th width="10%">Salary Per Hour</th>
            <th width="10%">Commision</th>
            <th width="10%">Total Amount</th>
            <th width="10%">Total Payable Amount</th>
        </tr>
    </thead>

    @if($jobData->count())
    @php
        $commission = 0;
    @endphp

    @foreach($jobData as $job)
    <tr role="row" class="filter">
        <td>{{ $job->jobwithuser->mwuserid }}</td>
        <td>{{ $job->jobwithuser->first_name }} {{ $job->jobwithuser->last_name }}</td>
        <td>{{ $job->title }}</td>
        <td>{{ $job->total_hired }}</td>
        <td>{{ $job->total_work_hours }}</td>
        <td>${{ number_format($job->salary_per_hour, 2) }}</td>
        <td>
            @php
            if($job->category_commision && !empty($job->category_commision)) {
                $commission = $job->category_commision;
            }
            else {
                $commission = $base['category'][0]['commision'];
            }
            @endphp
            {{ $commission }}%
        </td>
        @php
            $total_amount = $job->total_work_hours * $job->salary_per_hour;
        @endphp
        <td>${{ number_format($total_amount, 2) }}</td>
        <td>${{ number_format($total_amount+(($total_amount*$commission)/100), 2) }}</td>
    </tr>
    @endforeach
    @else
    <tr>
        <td colspan="9"><span id="no-data"><b>No results to display</b></span></td>
    </tr>
    @endif
</table>

<div class="pull-right pagination-common" data-ref="dashboard">
    <?php echo $jobData->render(); ?>
</div> 
