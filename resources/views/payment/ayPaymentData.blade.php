<p style="float:right;margin-top:-32px">
	<b>Payment paid between {{Carbon\Carbon::parse($showing_date)->format('M-d-Y') }} to {{Carbon\Carbon::parse($end_date)->format('M-d-Y') }}</b>
</p>

<table class="table table-striped table-bordered table-hover" id="datatable_ajax_for_recive_payment">
	<thead>
		<tr role="row" class="heading">
			<th width="1%">No.</th>
			<th width="10%">Employee Name</th>
			<th width="10%">Job Name</th>
			<th width="10%">Employer Name</th>
			<th width="10%">Number of Days</th>
			<th width="10%">Total Hours</th>
			<th width="10%">Salary Per Hour</th>
			<th width="10%">Total Amount</th>
		</tr>
	</thead>
	@if($jobData->count())
	@php
		$i = 1;
		$referals_recived = [];
	@endphp

	@foreach($jobData as $application)
	<tr role="row" class="filter">
		<td>{{ $application->userjobapplied->mwuserid }}</td>
		<td>{{ $application->userjobapplied->first_name }} {{ $application->userjobapplied->last_name }}</td>
		<td>{{ $application->job->title }}</td>
		<td>{{ $application->employerjobs->first_name }} {{ $application->employerjobs->last_name }}</td>
		<td>{{ $application->app_shift->count() }}</td>
		<td>{{ $application->total_hours_worked }}</td>
		<td>${{ number_format($application->job->salary_per_hour, 2) }}</td>
		<td>${{ number_format($application->total_hours_worked * $application->job->salary_per_hour, 2) }}</td>
	</tr>

	@php
		//check jobseeker's referrals and calculate their referal Amount
		$jobseekerReferals = Helper::check_referals($application->userjobapplied->_id);
	@endphp
	
	@if(!in_array($application->userjobapplied->_id, $referals_recived) && $jobseekerReferals['earned_amount'] != 0)
		@php
			$referals_recived[] = $application->userjobapplied->_id;
		@endphp
		
		<tr role="row" class="filter">
			<td>{{ $application->userjobapplied->mwuserid }}</td>
			<td>{{ $application->userjobapplied->first_name }} {{ $application->userjobapplied->last_name }}</td>
			<td>Referral Amount</td>
			<td>--</td>
			<td>--</td>
			<td>--</td>
			<td>--</td>
			<td>${{$jobseekerReferals['earned_amount']}}</td>
		</tr>
	@endif

	@php
		$i++;
	@endphp

	@endforeach
	@else
		<tr>
			<td colspan="8">
				<span id="no-data"><b>No results to display</b></span>
			</td>
		</tr>
	@endif
</table>

<div class="pull-right pagination-common" data-ref="dashboard">
    <?php echo $jobData->render(); ?>
</div> 
