@extends('admin.layout')
@section('title')
    Payments To pay
@endsection
@section('css')
<link href="{{ asset( 'public/admin/css/jquery-ui.css' ) }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset( 'public/admin/css/bootstrap-datetimepicker.css' ) }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

<h3 class="page-title">
	Payments To pay
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url('admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/receive-payment-view' ) }}">Payments To pay</a>
		</li>
	</ul>
</div>



<div class="box blue">
    <div class="portlet-body form">
        @include('flash::message')
            <div class="row">
                <div class="col-md-offset-2  col-md-2"></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <h3><strong><u>Select Timeframe</u></strong></h3>
                    </div>
                </div>
            </div>
        {{ Form::open(array( 'method' => 'POST','url' => '/admin/payment-to-pay','id'=>'pay-payment')) }}
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-offset-2 col-md-4">
                            <div class="form-group">
                                <div class='input-group date'>
                                    {{ Form::text( 'start_date',null,['id'=>'start_date', 'class' => 'from_date form-control', 'placeholder'=> 'Start Date' ]) }}
                                    <!-- <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span> -->
                                </div>
                                <label class="help-block"></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class='input-group date'>
                                    {{ Form::text( 'select_end_date',null,['id'=>'end_date','class' => 'to_date form-control', 'placeholder'=> 'End Date', 'disabled' => 'true' ]) }}
                                    {{ Form::hidden( 'end_date',null,['id'=>'end_date_selected','class' => 'to_date form-control', 'placeholder'=> 'End Date' ]) }}
                                   
                                </div>
                                <label class="help-block"></label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="timezone" id="timezone" />
                </div>
                
                
            </div>

            <div class="box-footer">
                <div class="col-md-offset-5 col-sm-1">
                    {{ Form::button( 'GO', [ 'id'=>'go','class' => 'btn btn-primary']) }}
                </div>
                <div id="exportcsv-div-xx" class="col-sm-2">
                    {{ Form::button( 'Export CSV', [ 'id'=>'export-csv','class' => 'btn btn-primary']) }}
                </div>
                <div class="clearfix"></div>
                <br>
            </div>
            <div class="row" id="search">
                    <div class="col-md-12">
                        <div class="col-md-offset-2 col-md-3">
                            <div class="form-group">
                                {{ Form::text('emp_name',null,['class'=>'from_date form-control','placeholder'=>'Search by Employee Name','id'=>'emp_name']) }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class='input-group date'>
                                {{ Form::select('records',[10=>10,15=>15,20=>20,30=>30,40=>40,'100'=>'All'],10,['id'=>'records','class'=>'form-control customselect selectrecords','data-jcf'=>'{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}']) }}
                            </div>
                        </div>
                        
                    </div>
                </div>
        {{ Form::close() }}
    </div>
    <div class="jobdata"></div>
</div>
@endsection 
@section('js')
<script src="{{ asset( 'public/admin/js/bootstrap-datetimepicker.js') }}"></script> -->
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/employer/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/employer/js/additional-methods.min.js') }}"></script>
<script>

jQuery(document).ready(function() {
     $("#start_date").datepicker({
        showOn: 'button',
        buttonText: 'Show Date',
        buttonImageOnly: true,
        buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#end_date").datepicker("option","minDate", selected)
        }
    });
    $("#end_date").datepicker({
        showOn: 'button',
        buttonText: 'Show Date',
        buttonImageOnly: true,
        buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
        numberOfMonths: 1,
        onSelect: function(selected) {
           $("#start_date").datepicker("option","maxDate", selected)
        }
    });

    $('#go').click(function(){
        $('#end_date_selected').val($('#end_date').val());
        if(!$("#start_date").val() && !$("#end_date").val()){
            bootbox.alert('Please specify both start and end date');
            return false;
        }
        if(!$("#start_date").val()){
            bootbox.alert('Please specify start  date');
            return false;
        }
        if(!$("#end_date").val()){
            bootbox.alert('Please specify end date');
            return false;
        }

        $("#pay-payment").ajaxSubmit({
            method      : 'post',
            dataType : 'html',
            beforeSend  : function() {
               addLoader();
            },
            url : path+'admin/payment-to-pay',
            success     : function(data) {
                removeLoader();

                $('#search').show();
                $('#export-csv').show();
                $('.jobdata').empty().html(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
               removeLoader();
            }
        });
    });
    $('#export-csv').click(function(){
        exportcsv('export-payable-csv');
    });


    $('#records').change(function(){
        $('#go').click();
    });
    
    
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var url=$(this).attr('href');
        listpayment(url);
     });
    
    $('#emp_name').keyup(function(e) {
        e.preventDefault();
        listpayment($('#pay-payment').attr('action'));
    });
  
    function listpayment(url) {
        var filter_data = $('#pay-payment').serialize();
       // addLoader();
        $.ajax({
            type : 'get',
            url : url,
            data : filter_data,
            dataType : 'html',
            beforesend:function(){
               // Loader();
            },
            success : function(data){
               // removeLoader();
                $('.jobdata').empty().html(data);
                $('.msg').html($('#message').val());
            },
            error : function(data,ajaxOptions, thrownError){
               // removeLoader();
            }
        });
    }

    function exportcsv(url) {
        var filter_data = $('#pay-payment').serialize();
        addLoader();
        $.ajax({
            type : 'get',
            url : url,
            data : filter_data,
            dataType : 'json',
            beforesend:function(){
                Loader();
            },
            success : function(data){
                removeLoader();
                if(data.status == true){
                    window.location.href = path+data.url;
                }else{
					bootbox.alert(data.message);
				}
            },
            error : function(data,ajaxOptions, thrownError){
                removeLoader();
            }
        });
    }

});
</script>
@endsection
