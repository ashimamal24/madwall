@extends('admin.layout')
@section('title')
	Edit Wage
@endsection
@section('heading')
	Edit Wage
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Edit Wage
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			{{ Form::model( $edit_wage, ['method' => 'POST','url' => '/admin/edit-wage','id'=>'editwage']) }}
				@include('wage.wageForm',['submitButtonText' => 'Update'])
			{{ Form::hidden( 'action','edit' ) }}
			{{ Form::hidden( 'idedit', $edit_wage['_id'], ['id'=>'idedit' ] ) }}
			{{ Form::close() }}
    	</div>	
	</div>
</div>
	
	
@endsection	

@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {
	$('#addwage').click(function(){
		var id = $('#idedit').val();
		$("#editwage").ajaxSubmit({
			method:'post',
			url: path+'admin/edit-wage/'+id,
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				window.location = path+'admin/list-wage';
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#editwage .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}
		});
	});
	
	/*
	to resolve crash while submiting form on press enter.
	*/
	$('#editwage').submit(function(){
		return false;
	});
});    
	

</script>

@endsection	
