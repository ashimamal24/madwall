@extends('admin.layout')
@section('title')
	Wage	
@endsection
@section('content')
<h3 class="page-title">
Wage
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-wage' ) }}">Manage Wage</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-body">
				<div class="table-container">
						<input type="hidden" name="action" value="filter-wage"/>
						<div class="table-actions-wrapper">
							<span>
							</span>
					
						</div>
						<div class="table-custom table-responsive">
							<table class="table table-striped table-bordered table-hover"  id="datatable_ajax_for_wage">
								<thead>
								<tr role="row" class="heading">
									<th width="1%">No.</th>
									<th width="10%">Amount</th>
									<th width="10%">Active/Inactive</th>
									<th width="10%">Actions</th>
								</tr>
								<tr role="row" class="filter">
									<td></td>
									<td></td>
									<td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
									<td>
										<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
										<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
									</td>
								</tr>
								</thead>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END PAGE CONTENT-->
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwwage.js') }}"></script>
<script>
jQuery(document).ready(function() {

    
   /** Change Ststus **/
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to  '+(status == true ? "Inactive" : "Active")+' this item ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage('Status changed');
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});
	
	
	TableAjax.init();
	TableAjax.update();
	

});
</script>

@endsection
