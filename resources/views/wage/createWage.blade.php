@extends('admin.layout')
@section('title')
	Add Wage
@endsection
@section('heading')
	Add Wage
@endsection
@section('content')
<div class="tab-pane" id="tab_1">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Add Wage
			</div>
		</div>
		<div class="portlet-body form">
			@include('errors.user_error')
			@include('flash::message')
			@if (count($errors) > 0)
		    @endif
			{{ Form::open(array( 'method' => 'POST','url' => '/admin/add-wage','id'=>'addwageform')) }}
				@include('wage.wageForm',['submitButtonText' => 'Add Wage'])
		    {{ Form::close() }}
		</div>	
	</div>
</div>

@endsection
@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {

	$('#addwage').click(function(){
		$("#addwageform").ajaxSubmit( {
			url: path+'admin/add-wage',
			method:'post',
			beforeSend : function() {
				addLoader();
			},
			success  : function(data) {
				window.location = path+'admin/add-wage';
			},
			error: function(xhr, ajaxOptions, thrownError) {
				removeLoader();
				$("#addwageform .form-group").removeClass("has-error");
				$(".help-block").hide();
				$.each(xhr.responseJSON, function(i, obj) {
					$('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					$('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
					$('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
					
					if(i=='status'){
						$('.status').addClass('has-error');
						$('.status').find('label.help-block').slideDown(400).html(obj);
					}
				});
			}

        });
	});
});
</script>

@endsection	
