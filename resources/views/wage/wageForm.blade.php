<div class="form-body">
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group">
                {{ Form::label( 'min_amount', 'Minimum Wage: ',['class' => 'control-label']) }} <span class="star">*</span> 
                {{ Form::text( 'min_amount',null,['class' => 'form-control', 'maxlength'=> '5']) }}
                <label class="help-block"></label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group status">
                {{ Form::label( 'status', 'Status: ',['class' => 'control-label']) }} <span class="star">*</span> 
                @if(isset($edit_wage['status']))
                    @if($edit_wage['status'] == 0)
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),0,['class' => 'form-control']) !!}
                    @else
                        {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),1,['class' => 'form-control']) !!}
                    @endif
                @else
                    {!! Form::select('status', array('1' => 'Active', '0' => 'Inactive'),null,['class' => 'form-control']) !!}
                @endif
              <label class="help-block"></label>
            </div>
        </div>
    </div>

</div>

<div class="box-footer">
    <div class="col-md-4"></div>
    <div class="col-sm-8">
        {{ Form::button( $submitButtonText, ['id'=>'addwage','class' => 'btn btn-primary']) }}
        {{ Html::link('admin/list-wage', 'Cancel', array( 'class' => 'btn btn-primary' ))}}   
    <br> <br>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.col -->
