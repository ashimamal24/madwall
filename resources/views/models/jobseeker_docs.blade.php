<div class="modal fade" data-backdrop="static" data-keyboard="false" id="jobseekerDocsPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
			<div class="modal-header">
				<h3>Degree/Diploma Certifications</h3>
			</div>     
			<div class="modal-body" style="overflow-y: scroll; height: 435px;">
				<div class="form_signup">
					<div class="row">
						<div class="col-md-12">
							<div class="form_grp" id="display_docs_here">
								
							</div>
						</div>
					</div>       
				</div>       
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
