<div class="modal fade" id="DisableJobAppAction" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p id="disable_message_here"></p>       
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
