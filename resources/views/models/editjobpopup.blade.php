<div class="modal fade signup forgot editjobpopup" id="manul_hiring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
       <p>Are you sure you want to edit this job?</p>
       
      </div>
      <div class="modal-footer">
        <button class="submit-button" type="button" id="editJobs">Save</button>
        <button type="button" class="cancel-button" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
