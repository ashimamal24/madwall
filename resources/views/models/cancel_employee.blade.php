<div class="modal fade" id="cancelEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p id="cancel_employee_message_here"></p>
				<form method="get" id="cancelEmployeeForm">
					<input type="hidden" name="cancel_employee_id" value="" id="cancel_employee_id">
				</form>       
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" id="cancelEmployeeYes" type="button">Yes</button>
				<button class="cancel-button btn" type="button" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
