<div class="modal fade signup forgot gurrntte" id="manul_hiring_be_advc" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
		<div class="modal-body">
			<p>Please be advised MadWall cannot guarantee that the worker(s) requested for this job will be able to make it to the job site on the specified time. Please update your timesheets accordingly.</p>
		   {!! Form::hidden('dur_days',null,['id'=>'dur_days']) !!}
		   {!! Form::hidden('dur_hours',null,['id'=>'dur_hours']) !!}
		   {!! Form::hidden('dur_cur_date',null,['id'=>'dur_cur_date']) !!}
		   {!! Form::hidden('dur_current_date',null,['id'=>'dur_current_date']) !!}
		</div>
      <div class="modal-footer">
        <button class="submit-button" type="button" id="confirm_dur_message">OK</button>
		<button class="cancel-button" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
    </div>
  </div>
</div>
