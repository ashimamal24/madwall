<div class="modal fade signup forgot" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Contact us</h4>
			</div>

			{!! Form::open(['id'=>'usercontactus','url'=>'employer/contact-us']) !!}    
				<div class="modal-body">
					<div id="contactmsg"></div>
					<div class="form_signup">
						<div class="row">
								<div class="col-md-12" style="<?php if(Auth::check()){ ?> display:none <?php } ?>">
									<div class="form_grp">
										@if(Auth::check())
												{!! Form::hidden('name',Auth::user()->first_name.' '.Auth::user()->last_name,['placeholder'=>'Name']) !!}
											@else
												{!! Form::text('name',null,['placeholder'=>'Name', 'id'=>'name', 'maxlength'=>'25' ]) !!}
										@endif
										<span class="error_msgg" style="display:none;"></span>
									</div>
								</div>
								<div class="col-md-12" style="<?php if(Auth::check()){ ?> display:none <?php } ?>">
									<div class="form_grp">
										@if(Auth::check())
												{!! Form::hidden('email',Auth::user()->email,['placeholder'=>'Name']) !!}
											@else
												{!! Form::text('email',null,['placeholder'=>'E-mail', 'id'=>'email']) !!}
										@endif
										<span class="error_msgg" style="display:none;"></span>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form_grp">
										{!! Form::textarea('content',null,['rows'=>2,'placeholder'=>'Message', 'id'=>'content']) !!}
										<span class="error_msgg" style="display:none;"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="modal-footer signup_ftr">
					{!! Form::button('Submit',['type'=>'submit','class'=>'contact-us']) !!}
				</div>
			{!! Form::close() !!}    
		</div>
	</div>
</div>
