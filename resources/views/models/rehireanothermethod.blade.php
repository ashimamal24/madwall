<div class="modal fade signup forgot rehireanothermethod" id="manul_hiring_rehr_anthr" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
		<p id="rehire_err_msg">Sorry, Rehire job can not be posted within 12 hours prior to job start time!.</p>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="submit-button" data-dismiss="modal">Ok</button>
        <button type="button" class="cancel-button" data-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
    </div>
  </div>
</div>
