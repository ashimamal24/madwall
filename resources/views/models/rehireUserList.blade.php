<div class="modal-dialog " role="document">
	<div class="modal-content">
		<div class="modal-header">        
			<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->        
			<h4 class="modal-title" id="myModalLabel">Rehire</h4>
		</div>      
		<div class="alert" id="rehire_error_messages" style="display:none"></div>      
		<div class="modal-body" style="overflow-y: scroll; height: 435px;">
			<div class="rehiring_div">
				@if(count($users))
					<ul>   
						<?php $matched = 0; ?>   
						@foreach($users AS $key => $value)
						
						<?php
						$userSkill= [];
						foreach($value->skills as $skl){
							$userSkill[] = $skl['_id'];
						}
						
						//@array_intersect('search_parameter','other array from which first one will be searched')
						if(count(array_intersect($jobSkills,$userSkill)) == count($jobSkills)){
							$matched = 1;
						?>
							<li>                 
								<div class="candi_img">
									<?php
										if(!empty($value->image)){
											$url = $value->image;
										}else{
											$url = url('public/employer/images/profile_avatar.png')	;
										}
									?>					
									<img height="50" width="50" src="{{$url}}" alt=""/>
								</div>                  
								<div class="name_rating"> <p>{{ $value->first_name }} {{ $value->last_name }} ({{$value->email}})</p>
									<div class="rating_rehire">                         
									<?php
										echo $rating = Helper::user_rating($value->rating);
									?>
									</div>
									@if(isset($admin) && $admin == true)
										<div class="admin_rehire">
									@else
										<div class="check_box_rating">
									@endif                       
										@if(isset($invitedUsers) && !empty($invitedUsers))
											@if(in_array($value->_id,$invitedUsers))
												
												{!! Form::checkbox('offer[]',$value->_id,true,['id'=>$value->_id,'class'=>'rehire_checkbox']) !!}
											@else
												{!! Form::checkbox('offer[]',$value->_id,null,['id'=>$value->_id,'class'=>'rehire_checkbox']) !!}
											@endif
										@else
											{!! Form::checkbox('offer[]',$value->_id,null,['id'=>$value->_id,'class'=>'rehire_checkbox']) !!}
										@endif
										@if(isset($admin) && $admin == false)
											<label for="{{$value->_id}}"><i class="fa fa-check" aria-hidden="true"></i></label>
										@endif
									</div>
								</div>
							</li>
							
						<?php							
						}
						?>
						
						@endforeach
						
						<?php 
							if($matched == 0){
								?>
							<p class="nodatafound">No employees found matching with job skills</p>
								<?php
							}
						?>
					</ul>
					 <!-- ---------- select job type to hire remaining employees ---------- -->
						<style>
							span.form_inputs_label{ width:auto; float:left; padding-right:10px; }
							span.form_inputs_label input[type='radio']{position: relative; top:3px;}
						</style>
						<div class="field_forms" id="rehire_remaining">
							<div class="label_form">
								<label>Select another job method to hire remaining employees(If any) :</label>
							</div>
							<div class="form_inputs skills">
								<span class="form_inputs_label">
									<input type="radio" name="rehire_type" value="1"> Automatic
								</span>
								<span class="form_inputs_label">	
									<input type="radio" name="rehire_type" value="0"> Manual 
								</span>	
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
						
					<!-- ------------- ------------- -------------- -------------- --------- -->
					{!! Form::hidden('rehirevalue',null,['id'=>'rehirevalue']) !!}
				@else
				<!-- ----------- updated : 28 july 2017, text updates ------- -->
				<p class="nodatafound">No qualified candidates have been previously rehired - please select another form of hiring</p>
				@endif 
			</div>       
		</div>
		<div class="modal-footer ">
			@if(count($users) && $matched == 1)
				<button class="submit-button btn btn-primary" type="submit" id="selectedUser">Submit</button>
				<button class="cancel-button btn" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			@else
				<button class="submit-button btn btn-primary" type="button" class="close" data-dismiss="modal" aria-label="Close">OK</button>
			@endif 
		</div>
	</div>
</div>
