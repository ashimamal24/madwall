<div class="modal fade signup forgot" id="rehire_fr_job" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        
        <h4 class="modal-title" id="myModalLabel">Rehire</h4>
      </div>
      
      <div class="alert" id="rehire_error_messages" style="display:none"></div>
      
      <div class="modal-body" style="overflow-y: scroll; height: 435px;">
      <div class="rehiring_div">
         @if(count($users))
            <ul>      
            @foreach($users AS $key => $value)
              <li>
                 
                <div class="candi_img">
					<?php
						if(!empty($value->image)){
							$url = $value->image;
						}else{
							$url = url('public/employer/images/profile_avatar.png')	;
						}
					?>
					
					<img height="50" width="50" src="{{$url}}" alt=""/>
				</div>
                  
                  <div class="name_rating"> <p>{{ $value->first_name }} {{ $value->last_name }} ({{$value->email}})</p><div class="rating_rehire">
                         
                          <?php
								echo $rating = Helper::user_rating($value->rating);
							?>
                      </div>
                      @if(isset($admin) && $admin == true)
						<div class="admin_rehire">
                      @else
						 <div class="check_box_rating">
                      @endif
                     
                         
                        @if(isset($user_ids) && !empty($user_ids))
							@if(in_array($value->_id,$user_ids))
								
								{!! Form::checkbox('offer[]',$value->_id,true,['id'=>$value->_id]) !!}
							@else
								{!! Form::checkbox('offer[]',$value->_id,null,['id'=>$value->_id]) !!}
							@endif
						@else
							{!! Form::checkbox('offer[]',$value->_id,null,['id'=>$value->_id]) !!}
                        @endif
                         
                         
                          @if(!isset($admin))
							<label for="{{$value->_id}}"><i class="fa fa-check" aria-hidden="true"></i></label>
						@endif
                      </div>
                  </div>
              </li>
            @endforeach
            
            
            <!-- ---------- select job type to hire remaining employees ---------- -->
            
            <div class="field_forms">
				<div class="label_form">
					<label>Select job type</label>
				</div>
				<div class="form_inputs skills">
					<input type="radio" name="rehire_type" value="1"> &nbsp; Automatic 
					<input type="radio" name="rehire_type" value="0"> &nbsp; Manual 
					<span class="error_msgg" style="display:none;"></span>
				</div>
			</div>
            
            <!-- ------------- ------------- -------------- -------------- --------- -->
            
            
            </ul>
            {!! Form::hidden('rehirevalue',null,['id'=>'rehirevalue']) !!}
          @else
			<!-- ----------- updated : 28 july 2017, text updates ------- -->
            <p class="nodatafound">No qualified candidates have been previously rehired - please select another form of hiring</p>
          @endif 
       
       
       </div>
       
      </div>
      <div class="modal-footer ">
      @if(count($users))
        <button class="submit-button btn btn-primary" type="submit" id="selectedUser">Submit</button>
        <button class="cancel-button btn" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
        
      @else
        <button class="submit-button btn btn-primary" type="button" class="close" data-dismiss="modal" aria-label="Close">OK</button>
      @endif 

      </div>
    </div>
  </div>
</div>
