
<!-- -------- modal popup to confirm job posting from user ------------ -->
<div class="modal fade" id="job_postConfirm" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p id="jobPost_confirmMessage"></p>       
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" id="hiring">Hire</button>
				<button class="cancel-button btn" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!-- -------- modal popup to set strt/end Time for job shifts (selected dates) ------------ -->
<div class="modal fade" id="set_shiftTime"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
			<!-- --------- modal header ---------- --> 
			<div class="modal-header"><h4>Set Job Shift Time</h4></div>   
			
			<div class="alert" id="show_err" style="display:none"></div>			
			<!-- --------- modal body ----------- --> 
			<div class="modal-body" id="append_shiftFieldHere" style="overflow-y: scroll; height: 435px;">
				<!-- ---------- shift start/end fields will append here --------- -->
			</div>
			<!-- --------- modal footer ------- -->
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" id="time_selected">Ok</button>
				<button class="cancel-button btn" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- ---------------- Rehire user select error popup ---------------- -->
<div class="modal fade signup forgot rehireuserselect" id="manul_hiring_rehr_usrslct" data-backdrop="static" data-keyboard="false"  role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p>Please choose at least one user!!.</p>       
			</div>
			<div class="modal-footer">
				<button type="button" class="submit-button btn btn-primary" data-dismiss="modal">Ok</button>
				<button type="button" class="cancel-button btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
		</div>
	</div>
</div>

<!-- -------------- rehire another method ------------------- -->

<div class="modal fade signup forgot rehireanothermethod" id="manul_hiring_rehr_anthr"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p id="rehire_err_msg">Sorry, Rehire job can not be posted within 12 hours prior to job start time!.</p>       
			</div>
			<div class="modal-footer">
				<button type="button" class="submit-button btn btn-primary" data-dismiss="modal">Ok</button>
				<button type="button" class="cancel-button btn" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- ------------- hours modal --------------- -->

<div class="modal fade signup forgot hoursmodel" data-backdrop="static" data-keyboard="false" id="manul_hiring_hrs_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>        
			</div>
			<div class="modal-body">
				<p>Please choose new timeslots or current dates which have not already passed.</p>        
			</div>
			<div class="modal-footer signup_ftr ">
				<button type="button" data-dismiss="modal" class="btn">Ok</button>
			</div>
		</div>
	</div>
</div>

<!-- -------------- gurntte modal ---------------- -->

<div class="modal fade signup forgot gurrntte" id="manul_hiring_be_advc" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
      
			<div class="modal-body">
				<p>Please be advised MadWall cannot guarantee that the worker(s) requested for this job will be able to make it to the job site on the specified time. Please update your timesheets accordingly.</p>
			   {!! Form::hidden('dur_days',null,['id'=>'dur_days']) !!}
			   {!! Form::hidden('dur_hours',null,['id'=>'dur_hours']) !!}
			   {!! Form::hidden('dur_cur_date',null,['id'=>'dur_cur_date']) !!}
			   {!! Form::hidden('dur_current_date',null,['id'=>'dur_current_date']) !!}
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" id="confirm_dur_message">OK</button>
				<button class="cancel-button btn close" type="button" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- -------- error popup ----------- -->

<div class="modal fade" id="DisableJobAppAction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p id="disable_message_here"></p>       
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>


<!-- ---------- edit job popup ------------- -->
<div class="modal fade signup forgot editjobpopup" data-backdrop="static" data-keyboard="false" id="manul_hiring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p>Are you sure you want to edit this job?</p>       
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" id="editJobs">Save</button>
				<button type="button" class="cancel-button btn" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- ------ Delete Job Modal ------ -->
<div class="modal fade signup forgot" id="deletejob" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">      
			<div class="modal-body">
				<p>Are you sure you want to delete this job ?</p>      
			</div>
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" id="deletejobs">Delete</button>        
				<button type="button" class="cancel-button btn" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade signup forgot" id="rehire_fr_job"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	
</div>


<!-- ----------------- Modal to assign interview timeslot before accepting an employee for a job ------------------- -->

<div class="modal fade" id="set_intrvwTime"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
			<!-- --------- modal header ---------- --> 
			<div class="modal-header"><h4>Hire Employee</h4></div>   
			
			<div class="alert" id="show_intrvw_err" style="display:none"></div>			
			<!-- --------- modal body ----------- --> 
			<div class="modal-body" id="three_timeslots_here">
				
				<!-- --------- confirmation from employer ------------- -->
				<div class="row" style="margin-bottom: 15px;">
					<div class="col-md-12" style="padding-bottom:15px">
						Are you sure you want to hire this employee?
						<br><br>
						<input type="checkbox" name="assign_time_slot" value='1' id="assign_time_slot"> 
						Do you want to take an interview before hiring this employee.?
						
						<input type="hidden" name="application_itrvw_id" id="application_itrvw_id" value="">
					</div>
				</div>
				<!-- -------- Interview Time slot 1 --------- -->
				<div class="row display_intrvwtime_slot">
					<div class="col-md-4">
						<label class="center_align_label">
							Interview Timeslot 1
						</label>
					</div>
					<div class="col-md-4">
						<div class="field_forms">
							<div class="label_form">
								<label>Start Time</label>
							</div>
							<div class="form_inputs">
								<input class="intervw_time form-control" type="text" id="intrvw_one_start" name="start_one" value="" onkeydown="return false">
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="field_forms">
							<div class="label_form">
								<label>End Time</label>
							</div>
							<div class="form_inputs">
								<input readonly class="form-control" type="text" id="intrvw_one_end" name="end_one" value="" onkeydown="return false">
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
					</div>
				</div>
				<!-- -------- Interview Time slot 2 --------- -->
				<div class="row display_intrvwtime_slot">
					<div class="col-md-4">
						<label class="center_align_label">
							Interview Timeslot 2
						</label>
					</div>
					<!-- --- intrvw start time ------- -->
					<div class="col-md-4">
						<div class="field_forms">
							<div class="label_form">
								<label>Start Time</label>
							</div>
							<div class="form_inputs">
								<input class="intervw_time form-control" id="intrvw_two_start" type="text" name="start_two" value="" onkeydown="return false">
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
					</div>
					<!-- --- intrvw end time ------- -->
					<div class="col-md-4">
						<div class="field_forms">
							<div class="label_form">
								<label>End Time</label>
							</div>
							<div class="form_inputs">
								<input readonly class="form-control" type="text" id="intrvw_two_end" name="end_two" value="" onkeydown="return false">
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
					</div>
				</div>
				<!-- -------- Interview Time slot 3 --------- -->
				<div class="row display_intrvwtime_slot">
					<div class="col-md-4">
						<label class="center_align_label">
							Interview Timeslot 3
						</label>
					</div>
					<!-- --- intrvw start time ------- -->
					<div class="col-md-4">
						<div class="field_forms">
							<div class="label_form">
								<label>Start Time</label>
							</div>
							<div class="form_inputs">
								<input class="intervw_time form-control" type="text" id="intrvw_three_start" name="start_three" value="" onkeydown="return false">
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
					</div>
					<!-- --- intrvw end time ------- -->
					<div class="col-md-4">
						<div class="field_forms">
							<div class="label_form">
								<label>End Time</label>
							</div>
							<div class="form_inputs">
								<input readonly class="form-control" type="text" id="intrvw_three_end" name="end_three" value="" onkeydown="return false">
								<span class="error_msgg" style="display:none;"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- --------- modal footer ------- -->
			<div class="modal-footer">
				<button class="submit-button btn btn-primary" type="button" id="hire_employee">Ok</button>
				<button class="cancel-button btn" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
			</div>
		</div>
	</div>
</div>


