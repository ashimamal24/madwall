<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Document Details</b></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Name: </th>
                                <td id="name"></td>
                            </tr>
                             <tr>
                                <th>File Name: </th>
                                <td id="file-name"></td>
                            </tr>
                           
                           
                           
                            
                        </tbody>
                </div>
                </table>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn green" data-dismiss="modal">Close</button>
                
                <a target="_blank" href="javascript:void(0)" id="download_app_file" class="btn blue">Download</a>
            </div>
        </div>
    </div>
</div>
