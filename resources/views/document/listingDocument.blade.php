@extends('admin.layout')
@section('title')
	Manage Documents	
@endsection
@section('content')
<h3 class="page-title">
Manage Documents
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-document' ) }}">Manage Documents</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-body">
				<div class="table-container">
						<input type="hidden" name="action" value="filter-document"/>
						<div class="table-actions-wrapper">
							<span>
							</span>
						<a href="{{url('admin/add-document')}}" class="btn blue btn-sm pull-right">Add Document</a>
						</div>
						<div class="table-custom table-responsive">
							<table class="table table-striped table-bordered table-hover"  id="datatable_ajax_for_cms">
								<thead>
								<tr role="row" class="heading">
									<th width="5%">No.</th>
									<th width="5%">Name</th>
									<th width="5%">File Name</th>
									<th width="10%">Active/Inactive</th>
									<th width="10%">Actions</th>
								</tr>
								<tr role="row" class="filter">
									<td></td>
									<td><input type="text" class="form-control form-filter input-sm" name="name" id="skillname" autocomplete="off"></td>
									<td></td>
									<td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
									<td>
										<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
										<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
									</td>
								</tr>
								</thead>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END PAGE CONTENT-->
	@include('document.popupDocument')
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwdocument.js') }}"></script>
<script>
jQuery(document).ready(function() {

	$(document).on("click", "#view", function () {
        /***********user ajax view *******/
        var url_for_user_view = adminname+'/view-document';
        var ginfoId = $(this).attr("ginfoId");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: ginfoId,_token:token},
            dataType: "JSON",
			success: function (result) {
				if ( result.status == 'error' ) {
				    bootbox.alert('some problem occur try again.....');
				} else {                   
				  
					if(result.reslutset.name){
						$('#name').text(result.reslutset.name);
				   	} else{
				   		$('#name').text('NA');
				   	}
				  
					
					if(result.reslutset.general_file_name){
						$('#file-name').text(result.reslutset.general_file_name);
					} else {
						$('#file-name').text('NA');
					}
					
					
					if(result.reslutset.general_file_url){
						$('#download_app_file').removeAttr("href");
						$('#download_app_file').attr("href",result.reslutset.general_file_url);
						
					} else {
						$('#download_app_file').removeAttr("href");
						$('#download_app_file').attr("href",'javascript:void(0)');
					}

					
				   $('#myModal').modal('show');
				}
			}
        });
        /***********user ajax view ends here*******/

    });
	
	/** Delete Skill **/

	$(document).on( "click", "#deleteginfo", function () {
        var id = $(this).attr('data-id');
        var token ="{{csrf_token()}}";

        bootbox.confirm("Are you sure you want to delete?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/delete-document/'+id,
	         		type: "POST",
	         		 data: {id: id,_token:token},
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/list-document';
						}
						if( data.success == false ){
							window.location = path+'admin/list-document';
						}
					},
	         	});
            }
        });
    });
    
   /** Change Ststus **/
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to  '+(status == true ? "Inactive" : "Active")+' this item ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage('Status changed');
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});
	
	
	TableAjax.init();
	TableAjax.update();
	

});
</script>

@endsection
