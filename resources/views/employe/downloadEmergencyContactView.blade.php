
<?php $id = $data['user_id']; ?>
<a href="{{ route('downloadEmergencyContact',['id'=>".$id.",'download'=>'pdf']) }}"></a>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emergency Contact </title>
</head>

<body>
<div class="main">

  <div style="    width: 100%;
  background: #0b4cc4; text-align: center;    padding: 10px 0;">
    <h1 style="    text-align: center;
    color: #fff;
    font-weight: bold;
    font-family: 'Open Sans', sans-serif;
    font-size: 30px;
    text-transform: uppercase;">Emergency Contact Form</h1>
  </div>
  
    <div style="margin: 40px auto 20px; width: 700px; display: inline-block;">

    	<h1 style="font-size: 20px; margin: 15px 0;width: 100%;    margin-top: 25px;">Emergency Contact 1</h1><br><br>

	    <div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Name :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{ucwords($data['nameOne'])}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Relationship :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{ucwords($data['relationshipOne'])}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Address :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{ucwords($data['addressOne'])}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Phone No :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{$data['CountryCodeOne'].' '.$data['phoneNoOne']}}
		    </div>
		</div>

    </div><br>


    <div style="margin: 40px auto 20px; width: 700px; display: inline-block;">

    	<h1 style="font-size: 20px; margin: 15px 0;width: 100%;    margin-top: 25px;">Emergency Contact 2</h1><br><br>

	    <div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Name :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{ucwords($data['nameSecond'])}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Relationship :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{ucwords($data['relationshipSecond'])}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Address :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{ucwords($data['addressSecond'])}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Phone No :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{$data['CountryCodeSecond'].' '.$data['phoneNoSecond']}}
		    </div>
		</div><br><br>

    </div>
</div>
</body>
</html>

