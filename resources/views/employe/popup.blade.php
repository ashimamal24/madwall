<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Reason For Block
                </h4>
            </div>
                
            <!-- Modal Body -->
        <div class="modal-body">
            <form method="post" action="/admin/block-user" id="block-user-form" class = "form-horizontal" role = "form" >
            
               {{csrf_field()}}
               <input type="hidden" name="userid" id="userid" />
                <div class="form-group reason_for_block_usr">
                    <div class="col-sm-12">
                        <textarea class="form-control description" id="reason_for_block_usrId" maxlength="500" placeholder="Reason" name="reason_for_block_usr" cols="50" rows="10"></textarea>
                    </div>
                <div style="margin-left:15px;">
                    <label class="help-block"></label>
                </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-8 col-sm-10">
                        <button type="button" class="btn btn-primary" id="block-user-reason">Submit</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
          </form>
        </div>  
        </div>
    </div>
</div>
