<!--<iframe src="https://tokbox.com/embed/embed/ot-embed.js?embedId=3e287457-a76c-4729-891b-23b4d785469c&room=DEFAULT_ROOM&iframe=true" width="800px" height="640px" scrolling="auto" allow="microphone; camera" ></iframe>-->

<html>
  <style type="text/css">
    
  #overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.5);
    z-index: 2;
    cursor: pointer;
  }

  #data{
    position: absolute;
    top: 50%;
    left: 50%;
    color: white;
    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
  }

  #discardOverlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 2;
    cursor: pointer;
  }

  #discard{
    position: absolute;
    top: 50%;
    left: 50%;
    padding-top: 35%;
    width: auto;
    height: 9%;
    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
  }

  @media (max-width: 1200px){
    #discard{
      padding-top: 143%;
    }
  }

  </style>

  <head>
      <title> OpenTok Getting Started </title>
      <link href="{{ asset( 'public/opentok/css/opentok.css' ) }}" type="text/css" rel="stylesheet"/>
      <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
  </head>

  <body>

      <div class='container' id='container'>

          <div class="imgcalling"><img class="user" src={{url('/public/opentok/images/default.png')}}>
          <!--<video id="video" autoplay></video>-->
          </div>

          <input type='hidden' id='declineRoute' data-route="{{ route('declineOpentokCall',[$data['emp_id']]) }}">
          <input type='hidden' id='emp_id' name='emp_id' value="{{$data['emp_id']}}">
          <p class='content'>{{$data['name']}}</p>
          <p class='content'>Calling ...</p>
          <a href="#" onclick="decline()"  ><img class='call' src={{url('/public/opentok/images/redcall2.gif')}}></a>

      </div>


       <div class='screen' style="display: none;"> 
          <div id="videos">

              <input type='hidden' name='apiKey' id='apiKey' value='{{$data["apiKey"]}}'>
              <input type='hidden' name='sessionId' id='sessionId' value='{{$data["sessionId"]}}'>
              <input type='hidden' name='token' id='token' value='{{$data["token"]}}'>

              <div id="subscriber">
                <div id="discardOverlay">
                <a href="#" onclick="disconnect()"><img id='discard' src={{url('/public/opentok/images/redcall4.png')}}></a>
                </div>
              </div>

              <div id="publisher" ></div>
          </div>
      </div>


      <!-- Calling  -->
      <!--<div class="calling" style="display: block;">

          <video id="video" width='100%'  height='100%' autoplay></video>

          <div id="overlay" style="display: block;">
               <div id="data">
                  <div class='container' id='container'>
                      <p  class='content'>{{$data['name']}}</p>
                      <p  class='content'>Calling ...</p>
                      <a href="#" onclick="disconnect()" ><img class='call' src={{url('/public/opentok/images/redcall2.gif')}}></a>
                    
                  </div>
                </div>
          </div>
      </div>-->

  </body>

</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{ asset( 'public/opentok/js/opentok.js') }}"></script>

<script type="text/javascript">
  var path = '<?php echo url('/'); ?>';
  var user_id = $('#emp_id').val();

    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            //video.src = window.URL.createObjectURL(stream);
            video.srcObject = stream;
            video.play();
        });
    }


    // Decline Connection by admin
    function decline() {
      
        var route = $('#declineRoute').data('route');
        console.log(route);
        $.get(route, function(data, status){
        console.log("Data: " + data + "\nStatus: " + status);
        //disconnect();

        console.log('call disconnected');
        $(".screen").css("display", "none");

        session.disconnect();
        //window.history.back();
        window.history.go(-2);

        });
  
    }

</script>