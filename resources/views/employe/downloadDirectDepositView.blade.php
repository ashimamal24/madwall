
<!--<a href="{{ route('downloadDirectDeposit',['id'=>'5910610142997575ee131321','download'=>'pdf']) }}">Download PDF</a>-->
<?php $id = $data['user_id']; ?>
<a href="{{ route('downloadDirectDeposit',['id'=>".$id.",'download'=>'pdf']) }}"></a>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Direct Deposit </title>
</head>

<body>
<div class="main">

  <div style="    width: 100%;
  background: #0b4cc4; text-align: center;    padding: 10px 0;">
    <h1 style="    text-align: center;
    color: #fff;
    font-weight: bold;
    font-family: 'Open Sans', sans-serif;
    font-size: 30px;
    text-transform: uppercase;">Direct Deposit Form</h1>
  </div>
  
    <div style="margin: 40px auto 20px; width: 700px; display: inline-block;">

	    <div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Institution No :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{$data['institutionNo']}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Transit No :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{$data['transitNo']}}
		    </div>
		</div><br><br>

		<div style="width:100%; display: inline-block; border-bottom: 1px solid #e6e6e6">
	    	<div style="width: 30%; display: inline-block; font-size: 16px; font-weight: 500;">
	        Account No :
	    	</div>
		    <div style="width: 70%; display: inline-block; font-size: 16px; font-weight: 500;   ">
		        {{$data['accountNo']}}
		    </div>
		</div><br><br>

    </div>
</div>
</body>
</html>


