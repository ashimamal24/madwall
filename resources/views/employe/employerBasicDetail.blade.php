@extends('admin.layout')
@section('title')
	Approved or Decline
@endsection
@section('css')

<style>

.table-responsive {
    max-height:169px;
    border: 1px solid black;
    border-collapse: collapse;
}
.company_description{
    border-top-style: solid;
    border-top-width: 1px;
    border-bottom-style: solid;
    border-bottom-width: 1px; 
}
.sec-3{
    padding:5px;
}
</style>
@endsection
@section('content')
<h3 class="block" style="text-align:center">
<b><u>View Details</u></b>
</h3>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue ">
            
                
                <div class="caption" style="float:right">
                    <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/list-employerwaitlist')}}"> Back </a>
                </div>
            
            <div class="portlet-body util-btn-margin-bottom-5">
                <div class="tab-pane" id="tab_1">
                    <div class="portlet-body form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="well col-md-3">
                                    <span><b>Company : </b>{{ $userDetail['company_name']}}</span><br>
                                    <span><b>Email : </b> {{ $userDetail['email'] }} </span><br>
                                    <span><b>Employer Mobile : </b> 
                                    
                                    <?php
                                    /******* 16 aug 2017, shivani - to display employer contact number ******/
                                    
                                    $phone = 'N/A';

									if( isset($userDetail['company_code'] ) && !empty($userDetail['company_contact']) && $userDetail['company_contact'] != null ) {
										$phone =$userDetail['company_code'] ." ". $userDetail['company_contact'];
									}
									if( isset($userDetail['country_code'] ) &&  !empty($userDetail['phone'])) {
										$phone = $userDetail['country_code'] ." ".$userDetail['phone'];
									}
                                    ?>
                                    
                                    {{ $phone }}
                                    
                                    </span><br>
                                        <?php foreach( $userDetail['industry'] as $indusrty ){
                                            $indusrty_arry[] = $indusrty['name'];
                                        }?>
                                    <span><b>Industries : </b> <?php echo $industries = implode(" ,", $indusrty_arry);?></span><br>
                        
                                    <span><b>No. of Workers : </b>
                                    
                                    @if(isset($userDetail['number_worker']) && !empty($userDetail['number_worker']))
										{{ $userDetail['number_worker']}}
									@else
										N/A
									@endif
                                    
                                    
                                    
                                    </span><br>
                                     <span><b>Address : </b> # {{ $userDetail['location']}}</span><br> 
                                    <span><b>Name : </b>{{ $userDetail['first_name']}} {{ $userDetail['last_name']}}</span>
                                </div>
                                <div id="industry-cover">
                                    <div class="table-heading">
                                        <div class="industry-head"><span>Industry</span></div>
                                        <div class="width66company_info">
                                            <div class="sub_data">
                                                <div class="category-head">
                                                   Category
                                                </div>
                                                <div class="category-head">
                                                    Commission
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @foreach($allindustries as $industry) 
                                    <div class="heading_row">
                                        @foreach($industry as $key => $indust)
                                            @if($key == 'name')
                                                <div class="width33company_info company-job-catrgory-name bg_grey" style="height: 70px;"><span>{{ $indust }}</span></div>
                                            @endif
                                            @if($key == 'relatedcategory')
                                            <div class="width66company_info bg_grey">
                                                @foreach( $indust as $ind )
                                                    <div class="sub_data">
                                                        @if(isset($ind['name']))
                                                            <div class="width50company_info ">
                                                                {{ $ind['name'] }}
                                                            </div>
                                                        @endif
                                                        @if(isset($ind['commision']))
                                                            <div class="width50company_info">
                                                                {{ $ind['commision']}} %
                                                            </div>
                                                        @endif
                                                    </div>
                                                @endforeach
                                             </div>
                                            @endif
                                        @endforeach
                                    </div>
                                @endforeach
                                </div>
                                
                                
                                <div id="profile-pic" class="col-md-2">
                                    @if(isset($userDetail['image']) )
                                        <img src="{{ $userDetail['image']}}" class="img-rounded" width="135" height="135" class="" alt="User Image" />  
                                    @else
                                        {{ Html::image('public/admin/img/user_male2-512.png', 'a picture', array( "height"=>"120", "width"=>"140" )) }}
                                    @endif
                                </div>
                                <div class="col-md-12 company_description">
                                     <h4><b><u>Company Description</b></u></h4>  
                                    @if(isset($userDetail['company_description']) )
                                        {{ $userDetail['company_description'] }}
                                    @else
                                        No information provided yet.
                                    @endif
                                </div>
                                <div class="col-md-offset-2 col-md-12 sec-3">
                                 {{ Form::open(array( 'method' => 'POST','url' => '/admin/approve-waiting-employer','id'=>'approve-waitlisting-employer',)) }}
                                    <div class="box-footer">
                                        <div class="col-sm-6" style="float:right">
                                            {{ Form::button( 'Approve', [ 'id'=>'approve-waiting-employer','class' => 'btn green']) }}
                                            {{ Form::button( 'Decline', [ 'id'=>'decline-employer','class' => 'btn red']) }}
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    {{ Form::hidden( 'userid', $userDetail['_id'],['id'=>'userid'] ) }}
                                {{ Form::close() }} 
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div> 
</div>

@endsection	
@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>

<script>

jQuery(document).ready(function() {
    /* Approve Waiting Jobseeker*/
    $('#approve-waiting-employer').click(function(){        
            $("#approve-waitlisting-employer").ajaxSubmit(
                {
                    type: 'post',
                    beforeSend  : function() {
                        addLoader();
                    },
                    url : path+'admin/approve-waiting-employer',
                    success     : function(data) {
                        window.location = path+'admin/list-approvedemployer';
                    },
                    error       : function(xhr, ajaxOptions, thrownError) {
                    removeLoader();
                }
            }
        )
    });

        /* Decline User */
    $('#decline-employer').click(function(){
        var token = "{{ csrf_token() }}";
        var id = $('#userid').val();
        $.ajax({
            dataType    : 'json',
            method      : 'post',
            url : path+'admin/decline-waiting-employer',
            data: { id:id, _token: token },
            beforeSend  : function() {
                addLoader();
            },
            success     : function(data) {
                window.location = path+'admin/list-employerwaitlist';
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
            }
        });
    });

});
</script>

@endsection	
