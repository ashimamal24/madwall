@extends('admin.layout')

@section('title')
	Waitlist
@endsection

@section('content')
<h3 class="page-title">
Waitlist Employer
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-employerwaitlist' ) }}">Waitlist Employer</a>

		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-body">
					<div class="table-container">
						<input type="hidden" name="action" value="filter-employerwaitlists"/>
						<!-- <div class="table-actions-wrapper">
							<span>
							</span>
						<a href="{{url('admin/add-commission')}}" class="btn blue btn-sm pull-right">Add Commission</a>
						</div> -->
						<div class="table-custom table-responsive">
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax_for_employer">
							<thead>
							<tr role="row" class="heading">
								<th width="5%">No.</th>
								<th width="10%">Company Name</th>
								<th width="10%">Contact Name</th>
								<th width="10%">Type of Industry</th>
								<th width="10%">Email</th>
								<th width="10%">Mobile Number</th>
								<th width="20%">Registered Date</th>
								<!-- <th width="20%">Status</th> -->
								<th width="10%">Action</th>
							</tr>
							<tr role="row" class="filter">
								<td></td>
								<td><input type="text" class="form-control form-filter input-sm" name="name" id="approved_planname" autocomplete="off"></td>
								<td></td>
								<td></td>
								<td></td>
								<td><button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button></td>
								<td>
									<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd" id="registered_start_date">
										<input type="text" class="form-control form-filter input-sm" readonly name="requested_at_from" placeholder="From">
										<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd" id="plan_end_date">
										<input type="text" class="form-control form-filter input-sm" readonly name="requested_at_to" placeholder="To">
										<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>

								</td>
								<td>
									<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
									<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
								</td>
								<!-- <td>{{ Form::select('status', array(''=>'--select--','1' => 'Active', '0' => 'Inactive' ),null,['class' => 'form-control form-filter input-sm']) }}</td> -->
								
							</tr>
							</thead>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End: life time stats -->
	</div><!-- END PAGE CONTENT-->
	
@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwwaitingemployer.js') }}"></script>
<script src="{{ asset( 'public/admin/js/bootstrap-datepicker.js' ) }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {
 
 /***********user ajax view *******/
	$(document).on("click", "#view", function () {
        var url_for_user_view = adminname+'/view-commission';
        var commission = $(this).attr("commissionid");
        var token ="{{csrf_token()}}";
        $.ajax({
            url: path+url_for_user_view,
            type: "POST",
            data: {id: commission,_token:token},
            dataType: "JSON",
			success: function (result) {
				if ( result.status == 'error' ) {
				    bootbox.alert('some problem occur try again.....');
				} else {                   
				  
					if(result.reslutset.category_name){
						$('#category_name').text(result.reslutset.category_name);
				   	} else{
				   		$('#category_name').text('NA');
				   	}

					if(result.reslutset.commission_amount){
						$('#commission_amount').text(result.reslutset.commission_amount);
					} else{
				    	$('#commission_amount').text('NA');
				   	}
				   $('#myModal').modal('show'); 	
				}
			}
        });
    });
    
    
    
    
    
    $(document).on('click','.delete-user', function(){
		var $this = $(this);
		var url = $(this).attr('data-url');
		bootbox.confirm('Are you sure you want to delete this user ?', function (result) {
            if (result) {
               window.location.href = url;
            }
        });
	});
   $(document).on('click','.verify-user', function(){
		var $this = $(this);
		var url = $(this).attr('data-url');
		bootbox.confirm('Are you sure you want to mark this user email verified ?', function (result) {
            if (result) {
               window.location.href = url;
            }
        });
	});
    
    
    
    
	

	$(document).on( "click", ".proved-improved", function () {
        var id = $(this).attr('data-id');
        var pendingstatus= $(this).attr('pending-status');
        var token ="{{csrf_token()}}";

        bootbox.confirm("Are you sure you want to improve?", function (result) {
            if (result) {
	         	$.ajax({
	         		url: path+'admin/improve-disimprove-user/'+id,
	         		type: "POST",
	         		 data: {id: id,_token:token, pendingstatus: pendingstatus },
	         		success  : function(data) {
						if( data.success == true ){
							window.location = path+'admin/list-jobwaitlists';
						}
						if( data.success == false ){
							window.location = path+'admin/list-jobwaitlists';
						}
					},
	         	});
            }
        });
    });
$('.start_date').datepicker( {
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months",
      onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            
        }
        
        
});

 $('start_date').datepicker()
      .on('changeDate', function(ev){
		   $('.datepicker').hide();
	  });



	  /////////////////////    code for datepickers start    ////////////////////////////////
			
	function formatDate(date) {
	    var d = new Date(date),
		month = '' + (d.getMonth() + 1),
	    day = '' + d.getDate(),
	    year = d.getFullYear();
	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	    return [year,month,day].join('-');
	}
	
	$('#registered_start_date').datepicker().on('changeDate', function(ev){
		var selected_date = new Date(ev.dates);
		var date = formatDate(selected_date);
		$('#plan_end_date').datepicker('setStartDate',date);
		$('.datepicker').hide();
		$('.filter-submit').click();  
    });
      
	$('#plan_end_date').datepicker().on('changeDate', function(ev){
		var selected_date = new Date(ev.dates);
		var date = formatDate(selected_date);
		$('#registered_start_date').datepicker('setEndDate', date);
		$('.datepicker').hide();
		$('.filter-submit').click();  
	});		
             
   ////////////////////    code for timepicker end    ////////////////////////////////////


   /** Change Ststus **/
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		bootbox.confirm('Are you sure you want to change the status to '+(status == true ? "Inactive" : "Active")+' ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage('Status changed');
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});
	
	TableAjax.init();
	TableAjax.update();
});
</script>

@endsection
