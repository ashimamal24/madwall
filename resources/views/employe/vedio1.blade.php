<!--<iframe src="https://tokbox.com/embed/embed/ot-embed.js?embedId=3e287457-a76c-4729-891b-23b4d785469c&room=DEFAULT_ROOM&iframe=true" width="800px" height="640px" scrolling="auto" allow="microphone; camera" ></iframe>-->

<html>

  <head>
      <title> OpenTok Getting Started </title>
  
      <link href="{{ asset( 'public/opentok/css/opentok.css' ) }}" type="text/css" rel="stylesheet"/>

      <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

  </head>

  <body>


      <div class='container' id='container'>
        <div class="imgcalling"><img class="user" src={{url('/public/opentok/images/default.png')}}></div>
        <p class='content'>{{$data['name']}}</p>
        <p class='content'>Calling ...</p>
       <a href="javascript:history.go(-1)" ><img class='call' src={{url('/public/opentok/images/redcall2.gif')}}></a>
      </div>




<div class='screen' style="display: none;">
        <input type='submit' onclick="disconnect()" value="Disconnect">

        <div id="videos" style="">

            <input type='hidden' name='apiKey' id='apiKey' value='{{$data["apiKey"]}}'>
            <input type='hidden' name='sessionId' id='sessionId' value='{{$data["sessionId"]}}'>
            <input type='hidden' name='token' id='token' value='{{$data["token"]}}'>
            <div id="subscriber"></div>
            <div id="publisher" ></div>
        </div>
</div>
    
  </body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{ asset( 'public/opentok/js/opentok.js') }}"></script>


