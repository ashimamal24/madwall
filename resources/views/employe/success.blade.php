<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<style>
.main

{
    width:100%;
    float: left;
    text-align:center;
    padding: 100px 0;
}
.image-success
{
    display:inline-block;
    text-align: center;
}
.successmsg {
    width: 100%;
    font-size: 50px;
    text-align: center;
    font-weight: bold;
    padding: 10px 0;
}
p.successpara {
    text-align: center;
    font-size: 20px;
}
.image{
    width: 80px;
}

</style>
</head>

<body>
<div class="main">
    <div class="image-success">
<img class='image' src="{{url("/public/img//checked.png")}}">
<div class="successmsg">Success!</div>
<p class="successpara">Saved Successfully. </p>
    </div>
</div>
</body>
</html>
