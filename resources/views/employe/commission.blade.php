@extends('admin.layout')
@section('title')
    Edit Commission
@endsection
@section('css')
<style type="text/css">
    
.extra-discount-css{
    margin-top: 20px;    
}
</style>>
@endsession
@section('heading')
    Edit Commission
@endsection
@section('content')
    <!-- --------- Display category chart here ----------- -->
<div class="row">
	<div class="col-md-12">
    <div class="portlet box blue">  
        <div class="caption" style="float:right">
            <a style="background-color:#C23F44" class="btn btn-primary" href="{{ url('admin/list-approvedemployer')}}"> Back </a>
        </div>
       
        
        <div class="portlet-body form">
            @include('errors.user_error')
            @include('flash::message')
            @if (count($errors) > 0)
            @endif
            {{ Form::open(array( 'method' => 'POST','url' => '/admin/edit-approved-employer','id'=>'edit-approved-employer')) }}
             
                <div class="form-body">
                    <div class="row">
						
						<!-- ------------ select category for which commision is to be updated ----------- -->
						<div class="col-md-offset-2 col-md-2">
							<div class="form-group">
								{{ Form::label('Select Category', 'Select Category: ',['class' => 'control-label'] ) }} <span class="star">*</span> 								
								{{ Form::select('category' ,$categoryDetails,null,['id'=>'selectCategory','class' => 'form-control extra-discount-css']) }}
								<label class="help-block"></label>
							</div>
						</div>	
							
							
						<!-- --------------- select Extra/Discount  -------------- -->
						<div class="col-md-2">
							<div class="form-group">
								{{ Form::label('extra_discount', 'Commision type:',['class' => 'control-label'] ) }}
								<span class="star">*</span> 
								{{ Form::select('extra_discount' ,array('extra' => 'Extra', 'discount' => 'Discount'),null,['class' => 'form-control extra-discount-css']) }}
								<label class="help-block"></label>
							</div>
						</div>
						
						<!-- ------------------ Enter commision ------------------ -->	
						<div class="col-md-2">
							<div class="form-group commission">
								{{ Form::label('commission', 'Commission(%): ',['class' => 'control-label'] ) }} <span class="star">*</span> 
								{{ Form::text( 'commission' , null, ['class' => 'form-control extra-discount-css', 'maxlength'=> '3', 'id' => 'commission' ] ) }}
								<label class="help-block"></label>
							</div>
						</div>
                    </div>
                </div>
                
                
                
                <div class="box-footer">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        {{ Form::button( 'Save', ['id'=>'give-commission','class' => 'btn btn-primary']) }}
                        {{ Html::link( 'admin/list-approvedemployer', 'Cancel', array( 'class' => 'btn btn-primary' ))}}
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
                {{ Form::hidden( 'userid' , $id, ['id'=> 'userid' ] ) }}
            {{ Form::close() }}
            <br><br>
        </div>  
    </div>
    
    
    
    

		<!-- Begin: life time stats -->
		<div class="portlet">
			<div class="portlet-body">
				<div class="table-container" id="ReloadListing" style="overflow-x: scroll;"> 
					<h3>Category Commision Details</h3> 
					<table class="table table-striped table-bordered table-hover" id="datatable_ajax">
						<thead>
							<tr role="row" class="heading">
								<th width="5%">Sr.No.</th>
								<th width="10%">Category Name</th>
								<th width="10%">Category Commision</th>
								<th width="10%">Extra</th>
								<th width="10%">Discount</th>
								<th width="10%">Total</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							@foreach( $industries as $industry )
								<tr>
									<td width="5%">{{$i}}</td>
									<td width="10%">{{ucfirst(  $industry['name'] )}}</td>
									<td width="10%">
										{{$industry['commision']}}
									</td>
									<td width="10%">
										
										@if(isset($user['category_details']) && !empty($user['category_details']))
											@foreach($user['category_details'] as $catDtl)
												@if($catDtl['_id'] == $industry['_id'])
													{{ $catDtl['extra'] }}
												@endif
											@endforeach
										@else
											0
										@endif
										
									</td>
									
									<td width="10%">
										
										@if(isset($user['category_details']) && !empty($user['category_details']))
											@foreach($user['category_details'] as $catDtl)
												@if($catDtl['_id'] == $industry['_id'])
													{{ $catDtl['discount'] }}
												@endif
											@endforeach
										@else
											0
										@endif
									
									</td>
									<td>
										<?php
											$commision = $industry['commision'];
										?>
										@if(isset($user['category_details']) && !empty($user['category_details']))
											@foreach($user['category_details'] as $catDtl)									
												@if($catDtl['_id'] == $industry['_id'])
													@if(!empty($catDtl['extra']))
														<?php $commision += $catDtl['extra']; ?>
													@elseif(!empty($catDtl['discount']))
													<?php  $commision -= $catDtl['discount']; ?>
													@else
													<?php $commision += 0; ?>
													@endif
												@endif
											@endforeach
										@endif
										
										{{ $commision }}%
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>	
    
    
    
    
    

@endsection 
@section('js')
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>
<script>
$(document).ready(function() {   
	
	//when the category is selected
	$(document).on('change','#selectCategory,#extra_discount',function(){
		var cat_id = $('#selectCategory option:selected').val();
		var user_id = "{{$user['_id']}}";
		$.ajax({
			url: path+'admin/check-category-commision',
			type: 'get',
			dataType: 'json',
			data: 'cat_id='+cat_id+'&user_id='+user_id,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			beforeSend:function(){
				addLoader();
			},
			success: function(data) {
				if(data.success == true){
					var amountType = $('#extra_discount option:selected').val();
					var amount = '0';
					if(amountType != '' && amountType == 'extra' && data.extra != 0){
						amount = data.extra;
					}
					if(amountType != '' && amountType == 'discount' && data.discount != 0){
						amount = data.discount;
					}
					$('#commission').val(amount);
				}else{
					//there's an error
				}
			},
			error: function(error) { 
				
			},
			complete: function() { removeLoader(); }
		  });
	});
	
	/*
	when assign commision form is submitted.
	*/
    $('#give-commission').click(function(){
        $("#edit-approved-employer").ajaxSubmit(
            {
                type: 'post',
                beforeSend  : function() {
                    addLoader();
                },
                url : path+'admin/edit-approved-employer',
                success     : function(data) {
                    //window.location = path+'admin/list-approvedemployer';
                    window.location.reload(true);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $("#editcategory .form-group").removeClass("has-error");
                $(".help-block").hide();
                $.each(xhr.responseJSON, function(i, obj) {
                    $('input[name="'+i+'"]').closest('.form-group').addClass('has-error');
                    $('input[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
                    $('textarea[name="'+i+'"]').closest('.form-group').addClass('has-error');
                    $('textarea[name="'+i+'"]').closest('.form-group').find('label.help-block').slideDown(400).html(obj);
                    
                    if(i=='commission'){
                        $('.commission').addClass('has-error');
                        $('.commission').find('label.help-block').slideDown(400).html(obj);
                    }
                }); 
            } 
        }
        )
    });
    
    $('#edit-approved-employer').submit(function(e){
		e.preventDefault();
		$('#give-commission').click();
	});
    
    $(document).on('change', '#extra_discount', function() {
        if( $('#extra_discount').val() =='null'){
            $('#commission').val(0);
            $("#commission").attr("disabled", "disabled"); 
        } else{
            $("#commission").removeAttr("disabled"); 
        }
    });
    if( $('#extra_discount').val() =='null'){
        $('#commission').val(0);
        $("#commission").attr("disabled", "disabled"); 
    }
    
    
    $('#extra_discount').change();
    
    
});
</script>

@endsection 
