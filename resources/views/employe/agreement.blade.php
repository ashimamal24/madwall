
<?php // print_R($agreement);die; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Madwall - Agreement</title>
    <link href="{{ asset( 'public/agreement/css/style.css' ) }}" type="text/css" rel="stylesheet" />

    <link rel="stylesheet" href="//code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />

    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="//code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.js"></script>

    <style type="text/css">

      #div_signcontract{ width: 99%; }
      .popupHeader{ margin: 10px; }
      .error {
         border-bottom: 3px solid #f00 !important;
      }
      /*.name,.date{
         font-weight:bold;
      }*/
      .paraDiv li{
        text-align: justify;
      }
      .mrgnbtm{
        text-align: left !important;
      }
      #btnSubmitSign {
        background: #0b4cc4;
        border: none;
        padding: 10px 15px;
        font-size: 14px;
        color: #fff;
        margin: 0 6px;
        border-radius: 6px;

    }
    input[type="button"]{
        border-radius: 6px
    }

    #divPopUpSignContract{
        float: none !important;
         text-align: center;
        margin: 0;
        display: inline-block;
    }
    #divPopUpSignContract-popup {
        display: inline-block;
        text-align: center;
        width: 100%;
        max-width: 400px !important;
        left: 0 !important;
    }
    .ui-btn-hover-b {
        background: none;
        background-image: none;
        text-shadow: none;
        box-shadow: none !important;
        border: none;
    }
    div.ui-input-text input.ui-input-text, div.ui-input-text textarea.ui-input-text, .ui-input-search input.ui-input-text {
     padding: 0;
    }
    .btnSubmitSign{
        margin-bottom: 20px !important;
    }

    .save,.discard {
        background: #0b4cc4;
        border: none;
        padding: 10px 15px;
        font-size: 14px;
        color: #fff;
        margin: 0 6px;
        border-radius: 6px;

    }
    .modal{
        max-width: 272px !important;
    }
    .modal a.close-modal{
        top: 2.5px !important;
        right: 4.5px !important;

    }
    h4{
        text-align: center  !important;
        line-height: 1.5  !important;
    }
    .back{
        position: fixed;
    }
    input[type=text] {
        margin-top: 20px !important; 
    }
    .company{
    margin-left: 28px;
    margin-top: 3px;
    }



    </style>

    <style>
  /* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: visible;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}

</style>

  <link href="{{ asset( 'public/agreement/css/bootstrap.css' ) }}" type="text/css" rel="stylesheet"/>
  <script src="{{ asset('public/agreement/js/bootstrap.js') }}" type="text/javascript"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">




</head>

<body>

<div id="ex1" class="modal">
    <h4>Are you sure  you want to leave ?</h4> <br>
    <center><a href="#close-modal" rel="modal:close"><input type='submit' class='save' id='save' value='Save'>  <input class='discard' id='discard' type='submit' value='Discard'></a></center>
</div>
<div class="loading" hidden>Loading&#8230;</div>

<!-- Link to open the modal 
<p><a href="#ex1" rel="modal:open">Open Modal</a></p>-->
 



  <div class="main">
    <!--<div class='backDiv'><a href="#ex1" rel="modal:open"><img class='back' id='back' src="{{ asset('public/img/back.png') }}"></a></div>-->
    <form action='' method='post' class='myform' id='myform' data-route="{{ route('postData') }}" data-image="{{ route('imageRoute') }}">
      <div class="clear"></div>
        <div class="container">
          <div class="pdfhtml">
          <h1>AGREEMENT</h1>
            <!--<div class="agreeDateTime">THIS AGREEMENT is made in <input type="text" name='made_in' id='made_in' value=''>, Ontario  on the <input type="text" id='made_on_day' name='made_on_day' value=''> day of<br> <input type="text" name='made_on_month' id='made_on_month' value=''>, 20 <input type="text" name='made_on_year' id='made_on_year' value=''>.</div>--->

            <br>
            <br>

            <span class="mrgnbtm">THIS AGREEMENT is <br>made in  <input type="text" autofocus name='made_in' id='made_in' placeholder='City' value=''>, <br><br> Ontario on the  <u> {{$data['day']}} </u> day of <u>{{$data['month']}} </u> 20 <u> {{$data['year']}} </u>.


             <input type="hidden" class='madeOn'  id='made_on_day' name='made_on_day' placeholder='Day' value='{{$data['day']}}' disabled> 
             <input type="hidden" class='madeOn'  name='made_on_month' id='made_on_month' placeholder='Month' value='{{$data['month']}}' disabled>
             <input type="hidden" class='madeOn'  name='made_on_year' id='made_on_year' maxlength="2" placeholder='Year' value='{{$data['year']}}' disabled>


            <!--<span class="mrgnbtm">THIS AGREEMENT is <br>made in  <input type="text" name='made_in' id='made_in' placeholder='City' value='{{$agreement['madeIn']}}'>, <br> Ontario on the 
              <select name='made_on_day' id='made_on_day' placeholder='Day'>
                @foreach($days as $days)
                 <option value="{{$days}}">{{$days}}</option>
                @endforeach
            </select></span><br>

            


             of <select name='made_on_month' id='made_on_month' placeholder='Month'>
                @for ($i = 1; $i <= 12; $i++)
                     <option value="{{date('F', mktime(0, 0, 0, $i, 1, 2019))}}">{{date('F', mktime(0, 0, 0, $i, 1, 2019))}}</option>
                @endfor
            </select><br>


             20 <select name='made_on_year' id='made_on_year' placeholder='Year'>
                @for ($i = 19; $i <= 99; $i++)
              <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>.-->

            <br>

            <div class="betweenand">
                <b>BETWEEN: </b> MADWALL INCORPORATED, a corporation incorporated pursuant the laws of Canada <br> (hereinafter referred to as the <b>“Employer”</b>)
            </div>
            <div class="betweenand">
               <b>AND: </b> <input type="text" name='between' id='between' value='{{$data['name']}}' placeholder='Employee Name' disabled> <br>
               (hereinafter referred to as the <b>“Employee”</b>)
            </div>
            <div class="emplDiv">1. <u>EMPLOYMENT</u></div>
            <ul class="paraDiv"><li>The Employer hereby employs the Employee and the Employee hereby agrees to
            serve the Employer as an Assignment Employee and agrees to perform his duties
            and exercise his powers in this position to and in accordance with the policies and
            directions given from time to time by the President of the Employer or its
            designated officers. These policies include but are not limited to the Employers:
            <div class="clearfix"></div>
<span class="DocDiv">MadWall Policies and Documentation</span> 
<!--<span class="DocDiv">Employee Signature </span> <span class="DocDiv">Date</span> -->
<div class="madwallpolicy">
    <ul>
        <li><b>a.</b> Workplace harassment policy and program 
            <!--<br>
            <div id="page1" class='page1' data-role="content">
            <input type="text" class='pad1' name='pad1' data-value ='1' placeholder=''>
            </div>
            Signature of Employee 
            
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>b.</b> Workplace violence policy and program
            <!--<br>
             <div id="page2"  class='page2' data-role="content">
            <input type="text" class='pad2' name='pad2' data-value ='2' placeholder=''>
           </div>
           Signature of Employee 
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>c.</b> Absenteeism and lateness policy
            <!--<br>
             <div id="page3" class='page3' data-role="content">
            <input type="text" class='pad3' name='pad3' data-value ='3' placeholder=''>
           </div>
           Signature of Employee 
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>d.</b> Substance abuse policy
           <!-- <br>
            <div id="page4" class='page4' data-role="content">
            <input type="text" class='pad4' name='pad4' data-value ='4' placeholder=''>
           </div>
           Signature of Employee 
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>e.</b> Hygiene and safety standards policy
           <!-- <br>
             <div id="page5" class='page5' data-role="content">
            <input type="text" class='pad5' name='pad5' data-value ='5' placeholder=''>
           </div>
           Signature of Employee
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>f.</b> Return to work policy
            <!--<br>
             <div id="page6" class='page6' data-role="content">
            <input type="text" class='pad6' name='pad6' data-value ='6' placeholder=''>
           </div>
           Signature of Employee 
          <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>g.</b> Rating policy and rating rules
            <!--<br>
             <div id="page7" class='page7' data-role="content">
            <input type="text" class='pad7' name='pad7' data-value ='7' placeholder=''>
           </div>
           Signature of Employee 
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>h.</b> Privacy Policy Statement
            <!--<br>
             <div id="page8" class='page8' data-role="content">
            <input type="text" class='pad8' name='pad8' data-value ='8' placeholder=''>
           </div>
           Signature of Employee 
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>i.</b> MadWall Employee Handbook
            <!--<br>
             <div id="page9" class='page9' data-role="content">
            <input type="text" class='pad9' name='pad9' data-value ='9' placeholder=''>
           </div>
           Signature of Employee 
            <br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date-->
        </li>
        <li><b>j.</b> MadWall Terms and Conditions
            <br>
             <div id="page10" class='page10' data-role="content">
            <input type="text" class='pad10'  name='pad10' data-value ='10' placeholder=''>
           </div>
           Signature of Employee 
            <!--<br>
            <input type="text" value='{{$data["currentDate"]}}' class='date' disabled>
            <br>Date--->
        </li>
    </ul>
</div>
<div class="clearfix">
    The Employee understands that signing above, beside each of the MadWall
policies states that they have read, understood, and acknowledge each MadWall
policy. If an Employee does not agree to any of the policies, they are asked to not
continue to review or sign the rest of this Agreement.
</div>
</li> 
<li>The Employee understands that their written authorization is necessary for the
completion of the MadWall application process. It is understood by the Employee
that although not every Assignment found on the MadWall App may require a
background check, some Clients may request one to be completed. Please read
and sign this form in the space provided below:
<div class="clearfix"></div><br>
I,<b> {{$data["name"]}} </b> hereby authorize MadWall to investigate my
background and qualifications for purposes of evaluating whether I am qualified
for any position which I may apply for through the MadWall App. I understand that
MadWall will utilize an outside firm or firms to assist it in checking such
information, and I specifically authorize such an investigation by information
services and outside entities of the company's choice. I also understand that I may
withhold my permission and that in such a case, no investigation will be done, and
my application for employment may not be processed further.
<div class="signatureemploye">
  <div id="page11" class='page11' data-role="content">
           <input type="text" class='pad11' name='pad11' data-value ='11'>
    </div>
    Signature of Employee </div>
<!--<div class="signatureemploye"><input type="text" value='{{$data['currentDate']}}' class='date' disabled><br>Date</div>
<div class="signatureemploye"><input type="text" value='{{$data['name']}}' class='name' disabled><br>Employee's Name </div>-->
</li>

<li>The Employee agrees that although not every Assignment found on the MadWall
    App may require specific Personal Protective Equipment, some Assignments may.
    The Employee understands that MadWall is not liable to provide any required
    Personal Protective Equipment, but MadWall may provide such Personal Protective
    Equipment at the cost to the Employee. The cost would be deducted from the
    Employee’s first pay
<div class="signatureemploye">
  <div id="page12" class='page12' data-role="content">
           <input type="text" class='pad12' name='pad12'  data-value ='12' placeholder=''>
    </div>
    Signature of Employee </div>
<!--<div class="signatureemploye"><input type="text" value='{{$data['currentDate']}}' class='date' disabled><br>Date</div>
<div class="signatureemploye"><input type="text" value='{{$data['name']}}' class='name' disabled><br>Employee's Name </div>-->
</li>

<li>The Employee understands that he is an Assignment Employee and that the
    Employer is a Temporary Help Agency pursuant to the <i>Employment Standards Act,
    2000, S.O. 2000. C.41.</i> The Employee understands that the he will be assigned by
    the Employer to perform work on a temporary basis for clients of the Employer.
</li>

<li>The Employee understands that as their employer, MadWall requires the
    Employee’s SIN, as per the Employment Insurance Regulations. MadWall uses the
    SIN to administer government benefits under the Income Tax Act, the Canadian
    Pension Plan Act, and the Employment Insurance Act. MadWall ensures that the
    Employee’s SIN will only be used for income-related information. The Employee is
    to also provide their name, as shown on their SIN card. The Employee understands
    that failure to provide accurate information related to their SIN may result in
    wrongful government benefits, and that by signing below, they agree that the
    information provided is accurate.
<div class="signatureemploye"><input type="text" name='employe_sin' id='employe_sin' placeholder='' value='{{$data['sin_number']}}' ><br>Employee SIN  </div>
<div class="signatureemploye"><input type="text" value='{{$data['name']}}' name='sin_name' id='sin_name' placeholder=""><br>Employee's Name on SIN </div>
<!--<div class="signatureemploye"><input type="text" value='{{$data['currentDate']}}' class='date' disabled><br>Date</div>-->
<div class="signatureemploye">
  <div id="page13" class='page13' data-role="content">
           <input type="text" class='pad13' name='pad13' data-value ='13' placeholder="">
    </div>
  Signature of Employee</div>
</li>
<li>The terms and conditions of this Agreement shall govern the parties regardless of
    the length of employment or any changes to the Employee’s position,
    compensation, title and regardless of whether such changes is material or
    otherwise.
</li>

</ul>
<div class="emplDiv">2. <u> TERM </u></div>
<ul class="paraDiv"><li> The employment of the Employee pursuant to the terms and conditions of this
    Agreement in the position of Assignment Employee shall commence on
    <b> {{$data['currentDate']}} </b> and shall continue until such time as it is terminated pursuant to
    the provisions of this Agreement or any amendment thereto.
    </li>
    </ul>
    <div class="emplDiv">3. <u> DUTIES </u></div>
    <ul class="paraDiv"><li>The Employee shall perform those functions, which are normally the functions of an
        Assignment Employee and shall further perform those functions, which shall be
        reasonably determined from time to time by the President of the Employer or its
        designated officers
        </li>
        <li>The Employee shall, during the Term of his employment devote his entire working
            time, attention and energies to the business of the Employer. The Employee agrees
            that his work will be scheduled by following the terms and conditions of the
            Employer’s app. The app is called MadWall Employment App and the Employee
            understands that he will be solely responsible for scheduling his own shifts and
            accepting work by selecting a position that has been posted by the Employer on
            the MadWall Employment App. The Employee will be entitled to sign up for a
            posted position and to accept work on a first come first serve basis, provided the
            employee has the correct skills and qualifications for the posted position. The
            Employee further acknowledges and agrees that it is within the sole discretion of
            the Employer to determine whether the Employee has the correct skills and
            qualifications to apply for a posted position. The Employee also acknowledges and
            agrees that from time to time his hours of work will vary and may be irregular and
            that the Employer does not guarantee the Employee a fixed and/or minimum
            amount of work or compensation. 
            </li>
            <li>
                . The Employee acknowledges and agrees for the Employer to rate him based on his
            performance, attendance, and client review. The Employee will be evaluated
            individually through the Employer’s electronic interface and the Employer reserves
            the right to modify each employee’s score at its discretion. The Employee agrees
            and acknowledges that he will start his employment with a five (5.0) rating score
            and that this rate may fluctuate throughout the term of his employment. The
            Employee agrees to the following rating policies:
            </li>
            <div class="madwallpolicy">
            <ul >
                <li>Some metrics of the Rating Rules (as seen below in paragraph 3(iii)(e) have a
                    maximum allowable point allocation within a given month. The maximum
                    allowable point allocation resets at the beginning of each month. The Employer
                    will conduct the Employee’s rating assessments at the end of each month. The
                    Employee may be eligible to earn two percent (2%) of their gross pay as a bonus
                    if the Employee ends the rating assessment with a five (5.0) star rating. The
                    Employee acknowledges and agrees that any bonus payment in this regard is
                    discretionary and that the Employer reserves the right not to pay any such
                    bonus. </li>
                <li>Each employee is to retain a minimal score of four (4.0), called the Datum Score.
                    If an employee is under the Datum Score (ie. 4.0), the Employee’s ability to put
                    out an application for any assignment and to select a posted position within the
                    MadWall Employment App will be restricted because of the Employee’s poor
                    rating. The Employee agrees and acknowledges that the Employer reserves the
                    right to the final decisions in regard to any reassessment of the Employee’s
                    Datum Score. </li>
                <li>In the event that the Employer decides to reassess the Employee’s Datum score
                    the Employee will be required to watch a mandatory video. The Employee may
                    file for reassessment up to three (3) times, after which they may no longer file for
                    reassessment and may be terminated pursuant to paragraph 6.2(f) below. The
                    Employee agrees that in order to support his request for reassessment he will
                    provide the Employee with his position in writing outlining why he believes
                    reassessment is necessary, together with any documentation that may assist in
                    his request for reassessment.</li>
                <li>The Employee agrees and acknowledges that if he drops below the Datum Score
                    (ie. 4.0 rating score) he will still be obligated to finish any assignments he has
                    signed up for that are scheduled to take place within the 48 hours immediately
                    following him becoming informed of his low Datum Score. All other assignments
                    selected by the Employee will be placed back in the assignment pool and may
                    be selected by other Employees.</li>
                   <!-- <li>The Employee agrees to be rated based on the following Rating Rules:

                                    <div class="table_rating ">
                                        <div class="table-responsive">
                                        <table >
                                            <tr>
                                            <th>Metric </th>
                                            <th>Description  </th>
                                            <th>Value ** </th>
                                            <th>Max Allowable
                                                Per Month ** </th>
                                            </tr>
                                            <tr>
                                                <td>Finishing
                                                        Jobs</td>
                                                        <td>Successfully completing 5 Jobs</td>
                                                        <td>+0.2 </td>
                                                        <td>+0.4</td>
                                            </tr>
                                            <tr>
                                                    <td>Employer
                                                            Rating</td>
                                                            <td>5 Star Rating <br>
                                                                    4 Star Rating <br>
                                                                    3 Star Rating<br>
                                                                    2 Star Rating<br>
                                                                    1 Star Rating</td>
                                                            <td>+ 0.1<br>
                                                                    + 0.05<br>
                                                                    ± 0<br>
                                                                    - 0.05<br>
                                                                    - 0.1 </td>
                                                            <td>+ 0.2 <br>
                                                                    + 0.1 <br>
                                                                    - <br>
                                                                    No Limit <br>
                                                                    No Limit</td>
                                                </tr>
                                                <tr>
                                                        <td>Monthly
                                                                Earnings</td>
                                                                <td>
                                                                        Earnings
                                                                        $0 ≤ Gross Monthly Earnings ≤ $1499 <br>
                                                                        $1500 ≤ Gross Monthly Earning ≤ $1999<br>
                                                                        $2000 ≤ Gross Monthly Earning
                                                                    </td>
                                                                <td>± 0 <br>
                                                                        + 0.2<br>
                                                                        + 0.4 
                                                                    </td>
                                                                <td>± 0 <br>
                                                                        + 0.2<br>
                                                                        + 0.4</td>
                                                    </tr>
                                                    <tr>
                                                        <td>  5 Star Score</td>
                                                        <td>e 2% Bonus on Monthly Gross Earnings</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                            <td>  No Show </td>
                                                            <td>Employee did not show up for approved
                                                                    assignment</td>
                                                            <td>-1.0</td>
                                                            <td>No Limit</td>
                                                        </tr>
                                                        <tr>
                                                                <td>Late to Job </td>
                                                                <td>Employee did not meet the requirements
                                                                        of the job assignment start time</td>
                                                                <td>-0.2</td>
                                                                <td>No Limit</td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Left Job
                                                                            Early</td>
                                                                    <td>Employee did not stay for the duration of
                                                                            the job assignment</td>
                                                                    <td>-0.2</td>
                                                                    <td>No Limit</td>
                                                                </tr>
                                                                <tr>
                                                                        <td>Withdrawing
                                                                                from an
                                                                                approved
                                                                                Job</td>
                                                                        <td>Withdrawing from an approved
                                                                                assignment:<br>
                                                                                Before 48 hours from the start time<br>
                                                                                48 hours ≤ Start Time ≤ 25 hours<br>
                                                                                24 hours ≤ Start Time ≤ 12 hours<br>
                                                                                12 hours ≤ Start Time </td>
                                                                        <td>- <br>
                                                                                ± 0 <br>
                                                                                - 0.2<br>
                                                                                - 0.4<br>
                                                                                - 1.0</td>
                                                                        <td>-<br>
                                                                                ± 0 <br>
                                                                                No Limit<br>
                                                                                No Limit<br>
                                                                                No Limit</td>
                                                                    </tr>
                                        </table>
                                    </div>
                                    </div>
                                </li>-->
            </ul>
        </div> 
        <li>The Employee agrees that his compensation as described in this Agreement fully
                compensates him for all of his hours of work and, subject to any applicable
                statutory limitations, he shall not be entitled to any additional compensation or
                time in lieu thereof on account of any such additional hours worked by him except
                as provided for in this Agreement.</li>
                <li>
                        The Employee represents and warrants that he has the required skills and
                        experience to perform the duties and exercise the responsibilities required of him
                        when scheduling himself for shifts/positions posted on the MadWall Employment
                        App.
                </li>
                <li>
                        The Employee shall not invest his personal assets in any business other than
                        Non-Competing Businesses, provided that should he invest in a Non-Competing
                        Business, no services shall be required to be performed by him in the operations or
                        businesses of such Non-Competing Business, and his participation therein will be
                        restricted solely to that of an investor. <div class="clearfix"></div>
                        Notwithstanding the foregoing, the Employee shall be entitled to purchase
                        securities of any corporation whose securities are regularly traded on a recognised
                        securities market. Moreover, the Employee shall not knowingly assist any spouse
                        or family member of his to make any investment which the Employee is not
                        permitted to make by Section 3(v) of this Agreement. For the purposes of
                        Subsection 3(v) of this Agreement, the expression Non-Competing Businesses shall
                        mean any businesses which do not compete with any of the business activities
                        which are carried on by the Employer nor have any access, directly or indirectly, to 
                        Revision 10
                        Page 7 of 19
                        any of the confidential or proprietary information or to the property of the
                        Employer.
                </li>
    
    
    </ul>
    <div class="emplDiv">4. <u>WAGES AND BENEFITS </u></div>
    <ul class="paraDiv">

        <li>In consideration for the faithful performance of the services to be rendered by the
            Employee to the Employer as herein provided, the Employer shall provide the
            Employee with compensation based on the rate of pay indicated by the MadWall
            Employment App. The Employee agrees and acknowledges that each posting on
            the MadWall Employment App will notify the Employee of the compensation being
            provided for that job. The Employee understands that all pay will be subject to
            statutory deductions and that the rate of pay will fluctuate up and down
            depending on the position the Employee is applying for through the Madwall
            Employment App. The Employee further understands and acknowledges that there
            is no consistent rate of pay from one job to the next and that the Employer in no
            way guarantees that the Employee will obtain the higher paying postings.
        </li>

        <li>
            In any case, the Employer and Employee agree that at no time will the Employee be
            provided with less than what is provided for by the Employment Standards
            Act, 2000, S.O. 2000, c. 41, or any other applicable legislation
        </li>

        <li><u>Overtime:</u> The Employer will compensate the Employee for overtime in accordance with the Employment Standards Act, 2000, S.O. 2000. C.41.
        </li>

        <li><u>Holidays:</u>The Employer will pay the Employee public holiday pay in accordance
            with the Employment Standards Act, 2000, S.O. 2000. C.41
        </li>

        <li>The Employee understands that he will only be paid for time worked. The
          Employer’s customer will provide the Employer with a detailed summary of the
          time the Employee begins work and the time the Employee finishes work. If the
          Employee shows up late to the scheduled shift or leaves early from the scheduled
          shift, the Employee will only receive compensation from time actually worked and
          not compensation for the entirety of the scheduled shift
        </li>

        <li>The Employee shall not be required to incur any expenses personally, regardless of the potential for reimbursement, without the prior written consent of MadWall, provided 
        </li>

    </ul>
    <div class="emplDiv">5. <u>VACATION</u></div>
        <ul class="paraDiv">
                <li>During the Term of his employment the Employee shall be entitled to vacation time
                        of two (2) weeks annually, which shall constitute his annual vacation. Vacation pay
                        will be paid out on an ongoing basis at a rate of 4% on each pay cheque</li>
                     
                        <li>Such vacation shall be taken at such times as will interfere as little as possible with
                                the performance of his functions and shall be at a time or times acceptable to the
                                Employer having regard to its operations. The Employee agrees that if he intends
                                to take vacation time, he may not schedule any assignment or apply for any posted
                                position during that period. Vacations will not be carried over from one calendar
                                year to another. In the event that the Employee does not take the full vacation
                                time to which he is entitled in any year of employment the Employee will lose the
                                entitlement to the unused vacation except any payments in respect of vacation pay
                                as required by the Employment Standards Act, 2000, S.O. 2000, c. 41 or any
                                applicable legislation that may replace it in the future. It is the intention that the
                                Employee shall on no account be given any less vacation than as is required as a
                                minimum pursuant to the Employment Standards Act, 2000, S.0. 2000, c. 41.</li>
        
                </ul>
                <div class="emplDiv">6. <u>PROBATIONARY PERIOD AND TERMINATION OF EMPLOYMENT </u></div>
                <ul class="paraDiv">
                        <li><u>Probationary Period.</u> The Employee’s employ- ment will be subject to a probationary
                                period of three (3) months in order to assess his suitability for the position. During
                                this period, the Employer may, notwithstanding any other provision in this
                                Agreement, terminate the Employee’s employment, at any time, with or without
                                cause, and without any notice or pay in lieu of notice.</li>
                             
                                <li><u>Termination for Cause. </u> Notwithstanding any other provision contained herein, the
                                    Employer may immediately terminate the employment of the Employee for cause.
                                    For the purposes of this Agreement, just cause includes but is not limited to:
                                
                                    <div class="madwallpolicy">
                                            <ul>
                                                <li>the wilful misconduct, fraud, misappropriation of funds, theft with respect to the Employer or gross negligence of the Employee; </li>
                                                            <li>

                                                                the commission of any criminal act by the Employee against the Employer, or any
                                                                of the Employer’s clients, involving material harm (whether or not charges are
                                                                filed in connection therewith); the commission of any criminal act or of any act of
                                                                moral turpitude which may bring the Employer into disrepute;
                                                        </li>
                                                        <li>

                                                                the wilful insubordination by the Employee to any directive of the President of
                                                                the Employer or its designated officers, provided reasonable prior notice of such
                                                                directive is given;
                                                        </li>
                                                        <li>
                                                                a material breach of this Agreement or the Employer’s employment policies;
                                                        </li>
                                                        <li> incompetence or substandard performance </li>
                                                        <li>a situation where the Employee’s Datum Score drops below 4.0 and the Employee is required to apply for reassessment more than three (3) times pursuant to paragraph 3(iii)(c) herein;</li>
                                                            <li>breach of duty of good faith and fidelity, including disclosing confidential information;</li>
                                                                        <li>improper use of or abuse of company property;</li>
                                                                        <li>for any other reason for which termination for cause is justified at common law</li>
                                                                        Any such termination for cause shall be without notice or payment of any compensation, damages or amounts in lieu of notice to the Employee.

                                                 
                                            </ul>
                                        </div>
                                </li>

                                <li><u>Termination upon Death.</u> Notwithstanding any other provision contained herein,
                                        this agreement shall terminate automatically, without notice or indemnity in lieu
                                        thereof, if the Employee dies.</li>
                                        <li><u>Termination without Cause. </u> The Employer may at any time during the Term of this
                                            Agreement or any renewal or extension thereof terminate the Employee’s
                                            employment without cause upon providing to him, his minimum entitlements as
                                            required by the Employment Standards Act, 2000, S.O.2000, c. 41, or any applicable
                                            legislation that may replace such statute in the future for a not for cause
                                            termination. The Employee agrees that these entitlements constitute his maximum
                                            entitlement upon the termination of his employment without cause and that the
                                            amount of these entitlements satisfies any and all common law requirements for
                                            reasonable notice of termination of his employment or compensation in lieu
                                            thereof. </li>
                                            <li><u>Termination Upon Disability.  </u> The Employer may terminate this Agreement by
                                                sending a notice in writing to the Employee with immediate effect and without
                                                further obligation to the Employee other than those obligations pursuant to
                                                Section 6.4 if:
                                                <div class="madwallpolicy">
                                                    <ul>
                                                        <li>the President of the Employer determines that the Employee has been unable
                                                                due to illness, disease, injury, mental or physical disability or similar cause, to fulfil
                                                                his obligations pursuant to this Agreement for any consecutive six (6) month
                                                                period or for any twelve (12) months in any consecutive twenty-four (24) month
                                                                period; or</li>
                                                                <li>a court of competent jurisdiction has declared the Employee to be mentally
                                                                        incompetent or incapable of managing his affairs.</li>
                                                    </ul>
                                                    </div>
                                                    <li><u>Termination by The Employee. </u>The Employee may terminate his employment, at
                                                            any time, upon giving at least two (2) week’s written notice to the Employer of his
                                                            resignation date.</li>
                                            </li>
                
                        </ul>
                        <div class="emplDiv">7. <u>CONFIDENTIAL INFORMATION AND RETURN OF PROPERTY</u></div>
                        <ul class="paraDiv">
                            <li>
                                    <b>Confidentiality Obligation. </b>The Employee covenants and agrees that he shall
                                    not, at any time during his employment with the Employer or any time thereafter,
                                    without the prior written consent of the Employer, directly or indirectly,
                                    communicate, reveal or disclose, in any manner, to anyone, or use for any purpose
                                    other than in carrying out the Employee’s duties under this Agreement in
                                    furtherance of the Employer’s business interests, any confidential or proprietary
                                    information concerning, or learned as a result of his employment with the
                                    Employer or any of its Affiliates or any of their respective predecessors, successors
                                    or Affiliates, including information concerning: their business plan or model,
                                    sources of revenue, assets, businesses, affairs, pricing, costs, technical information,
                                    financial information, plans or opportunities, processes, sale and distribution,
                                    marketing, research and development, suppliers, partnerships or employees.
                                    Provided that this section does not apply to information which is in the public
                                    domain without breach of this Agreement and provided that the Employee may
                                    make such disclosure as is required by law.
                            </li>
                            <li>
                                    <b>Return of Property. </b>Upon ceasing to be employed by the Employer or upon
                                    request of the Employer at any time, the Employee shall return to the Employer all
                                    business-related property belonging to the Employer including, but not limited to,
                                    all documents in any format whatsoever including electronic format, that are in his
                                    possession or control, and the Employee agrees not to retain any copies of such
                                    property in any format whatsoever including, but not limited to, electronic format.
                            </li>
                        </ul>
                        <div class="emplDiv">8. <u>NON-SOLICITATION </u></div>
                        <ul class="paraDiv">
                                    <li>The Employee shall not, either during his employment with the Employer or for a
                                    period of one (1) year thereafter, directly or indirectly, initiate any contact with or
                                    solicit any designated customers or clients of the Employer or any of its
                                    Subsidiaries or related corporations for the purpose of selling or providing to the
                                    designated customers or clients, any products or services which are the same as or
                                    substantially similar to, or in any way competitive with, the products or services
                                    provided by the Employer or any of its Subsidiaries or related corporations during
                                    the term of his employment with the Employer or at the end thereof, as the case
                                    may be. For the purpose of this section, a “designated customer or client” means a
                                    person, corporation, or entity who was a customer or client or was about to
                                    become a customer or client of the Employer or any Subsidiary or related
                                    corporations and with whom the Employee had dealings during his employment by
                                    the Employer.</li>
                                    <li>
                                    The Employee shall not, either during his employment with the Employer or for a
                                    period of one (1) year thereafter, directly or indirectly, employ or retain as an
                                    independent contractor any employee of the Employer or any of its Subsidiaries or
                                    related corporations or induce or solicit, or attempt to induce, any such person to
                                    leave that person’s employment with the Employer.
                                    </li>
                                    <li>
                                    The Employee represents and warrants that none of the negotiation, entering into
                                    or performance of this Agreement has resulted in or may result in a breach by him
                                    of any agreement, duty or other obligation with or to any other person,
                                    corporation, or entity, including, without limitation, any agreement, duty or
                                    obligation not to compete with any such person, corporation, or other entity or to
                                    keep any confidential information of any such person, corporation, or other entity
                                    or not to solicit or contact any customers or clients of such person, corporation, or
                                    other entity. The Employee further agrees to indemnify and hold harmless the
                                    Employer and any Subsidiaries or related corporations from and against any and all
                                    damages, expenses, losses, costs (including but not limited to legal fees and
                                    disbursements) which may be paid or are found to be payable by the Employer on
                                    account of any breach of this provision by the Employee.
                                    </li>
                                    <li>
                                    In the event of a breach or threatened breach by the Employee of the provisions of
                                    Paragraphs 8.1, 8.2 and/or 8.3 of this Agreement, the Employer shall be entitled to
                                    an injunction restraining the Employee from further violation. Nothing herein shall
                                    be construed as prohibiting the Employer from pursuing any other remedies 
                                    Revision 10
                                    Page 12 of 19
                                    available to it for such breach or threatened breach, including recovery of damages
                                    and reasonable legal expenses for the Employer.
                                    </li>

                        </ul>
                        <div class="emplDiv">9. <u>PROPRIETARY AND MORAL RIGHTS</u></div>
                        <ul class="paraDiv">
                            <li>
                                    <b>Proprietary Rights.</b> The Employee recognizes the Employer’s proprietary rights in
                                    the tangible and intangible property of the Employer and acknowledges that he
                                    has not obtained or acquired and shall not obtain or acquire any rights, ownership,
                                    title or interest, in any of the property of any member of the Employer or any of its
                                    predecessors, successors or Affiliates, including any writing, business plans, sources
                                    of revenue, communications, manuals, documents, instruments, contracts,
                                    agreements, files, literature, data, technical information, know-how, secrets,
                                    formulas, products, methods, procedures, processes, devices, apparatuses,
                                    trademarks, trade names, trade styles, service marks, logos, copyrights, patents,
                                    inventions, or discoveries, whether or not patented or copyrighted or patentable or
                                    copyrightable, which the Employee may have learned or may have conceived or
                                    made, or may learn, conceive or make, either alone or in conjunction with others,
                                    and related to the business of the Employer (collectively, the “Materials”). The
                                    Employee agrees that during his employment with the Employer and any time
                                    afterwards all Materials shall be the sole and exclusive property of the Employer.
                            </li>

                            <li>
                                    <b>Moral Rights. </b> The Employee irrevocably waives to the greatest extent permitted
                                    by law, for the benefit the Employer, all of the Employee’s moral rights whatsoever
                                    in the Materials, including any right to the integrity of any Materials, any rights to
                                    be associated with any Materials and any right to restrict or prevent the
                                    modification or use of any Materials in any way whatsoever. The Employee
                                    irrevocably transfers to the Employer all rights to restrict any violations of moral
                                    rights in any of the Materials, including any distortion, mutilation or other
                                    modification.
                            </li>
                            <li>
                                    <b>Assignment of Rights.  </b> If the Employee has acquired or does acquire, by any
                                    means, any right, title or interest in any of the Materials or in any intellectual
                                    property rights relating to the Materials, the Employee irrevocably assigns all such
                                    right, title and interest throughout the world exclusively to the Employer, including
                                    any renewals, extensions or reversions relating thereto and any right to bring an
                                    action or to collect compensation for past or future infringements.
                            </li>

                            <li>
                                    <b>Registration.  </b> The Employer will have the exclusive right to obtain copyright
                                    registrations, patents, industrial design registrations, trade-mark registrations or 
                                    Revision 10
                                    Page 13 of 19
                                    any other protection in respect of the Materials and the intellectual property rights
                                    relating to the Materials anywhere in the world. At the request of the Employer, the
                                    Employee shall, both during and after the Employee’s employment with the
                                    Employer, execute all documents and do all other acts necessary in order to enable
                                    the Employer to protect its rights in any of the Materials and the intellectual
                                    property rights relating to the Materials.
                            </li>
                            <li>
                                    All documents, records, software programs, working papers, notes, financial
                                    statements, personal tax returns, business plans, sources of revenue, corporate tax
                                    returns, memoranda, files and other records of or containers of confidential or
                                    proprietary information or Materials made or compiled by the Employee at any
                                    time or made available to the Employee at any time in the course of providing
                                    services under this Agreement and any renewal or extension thereof, including all
                                    copies thereof, shall be the property of the Employer and belong solely to it, and
                                    shall be held by the Employee solely for the benefit of the Employer and shall be
                                    delivered to the Employer by the Employee on the termination of this Agreement
                                    or at any other time on request by the Employer.
                            </li>
                            <li>
                                    The Employee shall, during the Term of this Agreement and any renewal or
                                    extension thereof, promptly disclose to the Employer in writing all ideas, business
                                    models or plans, inventions, formulae and discoveries relating to the business of
                                    the Employer, whether or not conceived or developed on the premises of the
                                    Employer. The Employee specifically acknowledges that those ideas, business
                                    models or plans, inventions, formulae and discoveries shall be the property of the
                                    Employer, which shall have the exclusive right to any and all patents, trademarks,
                                    copyrights, licences or any other protection which is issued on or which may arise
                                    with respect to those ideas, business models or plans, inventions, formulae and
                                    discoveries. The Employee assigns to the Employer all of the right, title and
                                    interest of the Employee in those ideas, business models or plans, inventions,
                                    formulae and discoveries and in any patent, trademark, copyright, licence or any
                                    other protection which may be issued on or which may arise in respect of those
                                    ideas, inventions, formulae and discoveries.
                            </li>
                            <li>
                                    The obligations of The Employee under the Sections 7.1, 7.2, 9.1, 9.2, 9.3, 9.4, 9.5
                                    and 9.6 are to remain in effect in perpetuity and shall exist and continue in full
                                    force and effect notwithstanding any breach or repudiation, or alleged breach or
                                    repudiation, by the Employer of this Agreement. These obligations are not in
                                    substitution for any obligation which the Employee may now or hereafter owe to
                                    the Employer and which exist apart from these Sections and do not replace any
                                    rights of the Employer with respect to any such obligations.
                            </li>
                          

                        </ul>
                        <div class="emplDiv">10. <u>REASONABLENESS AND REMEDIES</u></div>
                        <ul class="paraDiv">
                            <li>
                                    The Employee agrees that all the conditions and restrictions established in this
                                    Agreement are reasonable taking into account the circumstances surrounding this
                                    Agreement.
                            </li>
                            <li>
                                    The Employee recognises that in view of the serious and irreparable harm which a
                                    violation hereof would have on the Employer it is essential to the effective
                                    enforcement of this Agreement that in addition to any other remedies to which the
                                    Employer may be entitled, the Employer shall be entitled to seek and obtain, in
                                    summary manner, from any court having jurisdiction, interim, interlocutory, and
                                    permanent injunctive relief without showing irreparable harm, specific
                                    performance, and other equitable remedies.
                            </li>
                        </ul>
                        <div class="emplDiv">11. <u>ADMENDMENTS</u></div>
                        <ul class="paraDiv">
                            <li>
                                    This Agreement may be amended only by written instrument duly executed by all
                                    the parties hereto
                            </li>
                        </ul>
                        <div class="emplDiv">12. <u>ASSIGNMENT</u></div>
                        <ul class="paraDiv">
                            <li>
                                    The Employer may assign any of its rights or obligations under this Agreement to
                                    any affiliate of the Employer or to any purchaser of all or a material part of the
                                    business of the Employer any time and from time to time without the consent of
                                    the Employee. This Agreement ensures to the benefit of the Employer, its
                                    successors and assigns. The Employee may not assign any of his rights or
                                    obligations under this Agreement without the express written consent of the
                                    Employer. 
                            </li>
                        </ul>
                        <div class="emplDiv">13. <u>NO WAIVER</u></div>
                        <ul class="paraDiv">
                            <li>
                                    No waiver by any party of any breach of the obligations of any other party
                                    hereunder shall be a waiver of any subsequent breach or of any other obligation,
                                    nor shall any forbearance to seek a remedy for any breach be a waiver of any rights
                                    and remedies with respect to any subsequent breach.
                            </li>
                        </ul>
                        <div class="emplDiv">14. <u>SEVERABILITY</u></div>
                        <ul class="paraDiv">
                            <li>
                                    The invalidity of one of the provisions of this Agreement shall not invalidate or
                                    otherwise affect any of the other provisions of this Agreement and all such other
                                    provisions shall remain in full force and effect.
                            </li>
                        </ul>
                        <div class="emplDiv">15. <u>CURRENCY</u></div>
                        <ul class="paraDiv">
                            <li>
                                    All references in this Agreement to dollars or $ means lawful currency of Canada
                            </li>
                        </ul>

                        <div class="emplDiv">16. <u>GOVERNING LAWS</u></div>
                        <ul class="paraDiv">
                            <li>
                                    This Agreement shall be construed, interpreted and enforced in accordance with
                                    the laws of the Province of Ontario and the laws of Canada applicable therein.
                            </li>
                            <li>
                                    The parties agree to submit to the exclusive jurisdiction of the Courts of Ontario
                                    and the Courts of Ontario shall have the exclusive jurisdiction to entertain any
                                    action arising under this Agreement.
                            </li>
                            <li>
                                    Where the entitlements under this Agreement are less than what is provided for by
                                    the Employment Standards Act, 2000, S.O. 2000, c. 41, or any other applicable
                                    legislation, the minimums in the applicable legislation shall be substituted for the
                                    contractual provisions therein.
                            </li>
                        </ul>

                        <div class="emplDiv">17. <u>ENTIRE AGREEMENT</u></div>
                        <ul class="paraDiv">
                            <li>
                                    This Agreement embodies the entire agreement between the parties hereto concerning the subject matters mentioned herein and supersedes all previous discussions, correspondence, understandings or agreements, whether written or oral, with respect to such matters.
                            </li>
                            <li>
                                    The parties agree that this Agreement may be delivered by facsimile or PDF email
                                    transmission bearing the signature of a party to this Agreement, in which case the
                                    facsimile or PDF email of the Agreement shall be deemed to be the original and of
                                    full force and effect.
                            </li>
                            <li>
                                    The parties further agree that each party may sign by electronic signature and such
                                    signature will be accepted as a valid original signature in accordance with the
                                    Electronic Commerce Act, 2000.
                            </li>
                        </ul>
                        <div class="emplDiv">18. <u>NOTICES</u></div>
                        <ul class="paraDiv">
                            <li>
                                    All notices and other communications necessary or contemplated under this
                                    Agreement or any renewal or extension thereof shall be in writing and shall be
                                    delivered in the manner specified herein or, in the absence of such specification,
                                    shall be deemed to have been duly given three (3) business days after mailing by
                                    registered mail, or at the time of delivery when delivered by hand, or when
                                    delivered by facsimile or electronically by email or other means upon confirmation
                                    of receipt, or one (1) day after sending by overnight delivery service, to the
                                    respective addresses of the parties set forth below:
                            </li>
                            <div class="madwallpolicy">
                              <ul>
                                  <li>For notices and communications to the Employee (if no address is provided,
                                      communications to the Employee will be sent via the phone number or email
                                      address provided by the Employee while registering to MadWall via the
                                      App/Website: <br><br>
                                     <span>Attention:</span>  Employee Name<br>
                                      Address
                                  </li>
                                  <li>For notices and communications to the Employer:<br><br>
                                        <span>Attention:</span>  MadWall Incorporated <br>
                                        <span>Address:</span>  info@MadWall.ca
                                  </li>      
                              </ul>
                            </div>
                        </ul>
                        <div class="emplDiv">19.<u>INDEPENDENT LEGAL ADVICE</u></div>
                        <ul class="paraDiv">
                            <li>The Employee expressly declares that he has been given sufficient time to consider
                                    the terms of this Agreement and he voluntarily accepts the said terms and
                                    expressly declares that he has been given sufficient time to seek such independent
                                    or legal or other advice that he deems appropriate with respect to his employment
                                    and the terms of this Agreement. He further acknowledges that no representation 
                                    Revision 10
                                    Page 17 of 19
                                    of fact or opinion, threat or improper inducement has been made or given by the
                                    Employer to induce the signing of this Agreement. <br>
                                   <b class="mrgnbtm">IN WITNESS WHEREOF </b> the undersigned has executed this Agreement <br>this <u>{{$data['day']}} </u> day of <u>{{$data['month']}} </u> , 20 <u>{{$data['year']}} </u> .
                                    <br>

                                    <input type="hidden" name='agreement_on_day' class='agreementOn' id='agreement_on_day' value='{{$data['day']}}' placeholder="Day" disabled>
                                    <input type="hidden" class='agreementOn' name='agreement_on_month' id='agreement_on_month' value='{{$data['month']}}' placeholder="Month" disabled>
                                    <input type="hidden"  class='agreementOn' name='agreement_on_year' id='agreement_on_year' maxlength="2" value='{{$data['year']}}' placeholder="Year" disabled>






                                    <b class="mrgnbtm">
                                            SIGNED, SEALED & DELIVERED
                                            in the presence of 
                                    </b>
                                    <div class="signatureemploye">
                                      <div id="page21" class='page21' data-role="content">
                                          <input type="text" class='pad21' name='pad21' data-value ='21' placeholder=''>
                                      </div>

                                    <b>Witness Signature (Optional):</b></div>
                                    <div class="signatureemploye">
                                     
                                    <div class="signatureemploye"><input type="text" name='witness_name' id='witness_name' value='' placeholder=''><br><b>Witness Name (Optional):</b></div>

                                    <div class="signatureemploye">
                                         <div id="page14" class='page14' data-role="content">
                                         <input type="text" class='pad14' name='pad14'  data-value ='14' placeholder=''>
                                    </div><b>Employee Signature: </b></div>

                                      
                                      <input type="hidden" value='{{$data["currentDate"]}}' name='agreementDate' id='agreementDate' class='date' disabled><input type="hidden" value='{{$data["emp_id"]}}' name='emp_id' id='emp_id' class='date' disabled>

                                      <!--<input type="text" value='{{$data['name']}}' name='employee_name' id='employee_name' class='name' disabled placeholder=''>
                                      <br><b>Employee Name: </b>--></div>

                                    <br>
                                    <!--<b class="mrgnbtm">IN WITNESS WHEREOF </b> the undersigned has executed this Agreement <br>this <input type="text" name='agreement_on_day' id='agreement_on_day' value=''> <br> of <input type="text" name='agreement_on_month' id='agreement_on_month' value=''><br> 20 <input type="text"  name='agreement_on_year' id='agreement_on_year' value=''> .
                                    <br>-->
                                    <b class="mrgnbtm"> MADWALL INCORPORATED</b>
                                    <br><br>
                                    Per:  <img  height='75px' width='100px' src='{{url("/public/img/sign.jpg")}}'><br>
                                    <p class='company'>(Nav Madesha, CEO)</p>
                                    <br>
                                    <b class="mrgnbtm"> <i>I have authority to bind the corporation</i></b>
                                    <br>
                                    <b><i>Date: August 23, 2017</i></b>
                            </li>
                        </ul>

    </ul>

    <!--<input id='submit' type='submit' name='ubmit' value='Submit'>-->
    <input id="btnSubmitSign" class='btnSubmitSign' type="submit" data-inline="true" data-mini="true" data-theme="b" value="Submit" />
</div>
  
</div>
</div>

</form>
</div>


  <!---  SIGN PAD POPUP START --->
    <div data-role="popup" id="divPopUpSignContract">
      <div data-role="header" data-theme="b">
        <a data-role="button" data-rel="back" data-transition="slide" class="ui-btn-right" > X </a>
        <p class="popupHeader">Sign Pad</p>
      </div>
      <!-- SIGN ALERT -->
      <div class="ui-content popUpHeight" id='signAlert'>
         <div class="alert alert-danger">
           <center><strong>Please sign here.</strong> </center>
          </div>
      </div>

        <div id="div_signcontract">
          <canvas id="canvas">Canvas is not supported</canvas>
          <div class="signbtn">
            <input type='hidden' name='name' id='name' value=''>
            <input id="btnSubmitSign" type="button" data-inline="true" data-mini="true" data-theme="b" value="Submit" onclick="fun_submit()" />
            <input id="btnClearSign" type="button" data-inline="true" data-mini="true" data-theme="b" value="Clear" onclick="init_Sign_Canvas()" />
          </div>
        </div>  
      </div>
    </div>
 <!---  SIGN PAD POPUP END --->

  <!---  SIGN PAD POPUP START --->
    <div data-role="popup" id="divPopUpSignContract">
      <div data-role="header" data-theme="b">
        <a data-role="button" data-rel="back" data-transition="slide" class="ui-btn-right" > X </a>
        <p class="popupHeader">Sign Pad</p>
      </div>
      <!-- SIGN ALERT -->
      <div class="ui-content popUpHeight" id='signAlert'>
         <div class="alert alert-danger">
           <center><strong>Please sign here.</strong> </center>
          </div>
      </div>

        <div id="div_signcontract">
          <canvas id="canvas">Canvas is not supported</canvas>
          <div class="signbtn">
            <input type='hidden' name='name' id='name' value=''>
            <input id="btnSubmitSign" type="button" data-inline="true" data-mini="true" data-theme="b" value="Submit" onclick="fun_submit()" />
            <input id="btnClearSign" type="button" data-inline="true" data-mini="true" data-theme="b" value="Clear" onclick="init_Sign_Canvas()" />
          </div>
        </div>  
      </div>
    </div>
 <!---  SIGN PAD POPUP END --->



</body>
</html>

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>




<script type="text/javascript">


$('#signAlert').hide(); 
    var isSign = false;
    var leftMButtonDown = false;

    var preImage = '';
    var employeeImage ='';
    var witnessImage ='';
    
    jQuery(function(){
      //Initialize sign pad
      init_Sign_Canvas();
    });
    
    function fun_submit() {

      $('#signAlert').hide(); 
      var value = $('#name').val();
      console.log(value);
      if(isSign) {

        var canvas = $("#canvas").get(0);


        var imgData = canvas.toDataURL();

        var route = $('#myform').data('image');
        var fd = new FormData(); 
        fd.append('image', imgData);

         if(value == 21){

                witnessImage = imgData;

            }else{

               employeeImage = imgData;

            }
    
            
        $.ajax({
           url:route,
           type:'POST',
           data: fd,
           processData: false,
           contentType: false,
           dataType :'json',
           beforeSend : function() {
               //  addLoader();
               $('.loading').show();
            },
           success:function(data){
            $('.loading').hide();
            //console.log(data);

           // jQuery('.page'+value).empty();<br>
            $('.pad'+value).hide();
            jQuery('.page'+value).find('img').remove();
            jQuery('.page'+value).append($('<img id="img'+value+'"  data-image="'+data.image+'" class="img'+value+'" data-value='+value+' >').attr('src',data.fullurl).css({'width' : '100px' , 'height' : '75px'}));
//console.log(imgData);


            if(value != 21){

                     $('.img10,.img11,.img12,.img13,.img14').attr("src",data.fullurl);

            }
         
                 
          },
          error: function() { 
           //alert("Fail");
           console.log('fail');
                  
           }
        });
        
    
        closePopUp();

      } else {
        //alert('Please sign');
       $('#signAlert').show(); 
      }
    }
    
    function closePopUp() {
      $('#divPopUpSignContract').popup('close');
      //jQuery('#divPopUpSignContract').popup('close');
    }
    
    function init_Sign_Canvas() {

      isSign = false;
      leftMButtonDown = false;
      
      //Set Canvas width
      var sizedWindowWidth = $(window).width();
      if(sizedWindowWidth > 700)
        sizedWindowWidth = $(window).width() / 2;
      else if(sizedWindowWidth > 400)
        sizedWindowWidth = sizedWindowWidth - 100;
      else
        sizedWindowWidth = sizedWindowWidth - 50;
       
       $("#canvas").width(sizedWindowWidth);
       $("#canvas").height(200);
       $("#canvas").css("border","1px solid #000");
      
       //var canvas = $("#canvas").get(0);
      // canvas
       var canvas = document.getElementById("canvas"),
       //console.log(canvas);
      
       canvasContext = canvas.getContext('2d');

      if(isSign == false){

        var background = new Image();
        // The image needs to be in your domain.
        background.src = preImage;

        // Make sure the image is loaded first otherwise nothing will draw.
        background.onload = function() {
           canvasContext.drawImage(background, 0, 0);
            isSign = true;
            preImage ='';

        };

      }
        


       if(canvasContext)
       {
         canvasContext.canvas.width  = sizedWindowWidth;
         canvasContext.canvas.height = 200;

         canvasContext.fillStyle = "#fff";
         canvasContext.fillRect(0,0,sizedWindowWidth,200);
         
         canvasContext.moveTo(50,150);
         //canvasContext.lineTo(sizedWindowWidth-50,150);
         canvasContext.stroke();
        
         canvasContext.fillStyle = "#000";
         canvasContext.font="20px Arial";
         //canvasContext.fillText("x",40,155);
       }
       // Bind Mouse events
       $(canvas).on('mousedown', function (e) {
         if(e.which === 1) { 
           leftMButtonDown = true;
           canvasContext.fillStyle = "#000";
           var x = e.pageX - $(e.target).offset().left;
           var y = e.pageY - $(e.target).offset().top;
           canvasContext.moveTo(x, y);
         }
         e.preventDefault();
         return false;
       });
      
       $(canvas).on('mouseup', function (e) {
         if(leftMButtonDown && e.which === 1) {
           leftMButtonDown = false;
           isSign = true;
         }
         e.preventDefault();
         return false;
       });
      
       // draw a line from the last point to this one
       $(canvas).on('mousemove', function (e) {
         if(leftMButtonDown == true) {
           canvasContext.fillStyle = "#000";
           var x = e.pageX - $(e.target).offset().left;
           var y = e.pageY - $(e.target).offset().top;
           canvasContext.lineTo(x,y);
           canvasContext.stroke();
         }
         e.preventDefault();
         return false;
       });
       
       //bind touch events
       $(canvas).on('touchstart', function (e) {
        leftMButtonDown = true;
        canvasContext.fillStyle = "#000";
        var t = e.originalEvent.touches[0];
        var x = t.pageX - $(e.target).offset().left;
        var y = t.pageY - $(e.target).offset().top;
        canvasContext.moveTo(x, y);
        
        e.preventDefault();
        return false;
       });
       
       $(canvas).on('touchmove', function (e) {
        canvasContext.fillStyle = "#000";
        var t = e.originalEvent.touches[0];
        var x = t.pageX - $(e.target).offset().left;
        var y = t.pageY - $(e.target).offset().top;
        canvasContext.lineTo(x,y);
        canvasContext.stroke();
        
        e.preventDefault();
        return false;
       });
       
       $(canvas).on('touchend', function (e) {
        if(leftMButtonDown) {
          leftMButtonDown = false;
          isSign = true;
        }
       
       });
    }
    
  $("input").hover(function(){
        $(this).focus();   
    });


/*

    $('.page1,.page2,.page3,.page4,.page5,.page6,.page7,.page8,.page9,.page10').click(function() {

        $('#divPopUpSignContract').popup('open');
        $('#signAlert').hide(); 
        var value = $(this).find("input[type='text']").data('value');
        $('#name').val(value);
    });*/

  /*
    $('.pad1,.pad2,.pad3,.pad4,.pad5,.pad6,.pad7,.pad8,.pad9,.pad10').click(function() {

        if(isSign == false){

            $('#divPopUpSignContract').popup('open');

         }else{
             fun_submit();
          }
    $('#signAlert').hide(); 
     var value = $(this).data('value');
    $('#name').val(value);
    });
  */
    

   // $('.pad11,.pad12,.pad13,.pad14').click(function() {
   /* $('.page10,.page11,.page12,.page13,.page14').click(function() {
      $('#divPopUpSignContract').popup('open');
      $('#signAlert').hide(); 
     // var value = $(this).data('value');
      var value = $(this).find("input[type='text']").data('value');
      $('#name').val(value);
    });*/

     /* $('.page10,.page11,.page12,.page13,.page14').click(function() {
      $('#divPopUpSignContract').popup('open');
      $('#signAlert').hide(); 
     // var value = $(this).data('value');
      var value = $(this).find("input[type='text']").data('value');
      $('#name').val(value);
    });*/

        $('.page10,.page11,.page12,.page13,.page14').click(function() {

          preImage =  employeeImage;
          init_Sign_Canvas();
          //console.log(preImage)

          $('#divPopUpSignContract').popup('open');
          $('#signAlert').hide(); 
          var value = $(this).find("input[type='text']").data('value');
          $('#name').val(value);

   /*

      $('#divPopUpSignContract').popup('open');
      $('#signAlert').hide(); 
     // var value = $(this).data('value');
     var value = $(this).find("input[type='text']").data('value');
    $('#name').val(value);*/
    });

    //$('.pad21').click(function() {
      $('.page21').click(function() {

      preImage =  witnessImage;
      init_Sign_Canvas();
      $('#divPopUpSignContract').popup('open');
      $('#signAlert').hide(); 
      //var value = $(this).data('value');
      var value = $(this).find("input[type='text']").data('value');
      $('#name').val(value);
    });


       var status = 1;


      jQuery.validator.addMethod("letterswithspace", function(value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
      }, "Only alphabetical characters"); 

      jQuery.validator.addMethod("alphanumericspace", function(value, element) {
              return this.optional(element) || /^[a-zA-Z0-9\s]*$/.test(value);
      }); 


      $( "#myform" ).submit(function() {  
         status = 1;
       });  

  $('#myform').validate({ 

      rules: {

       made_in: {
            required: true,
            //lettersonly: true,
          //  regex: "[a-zA-Z']",
          //letterswithspace : true,
          //alphabetsnspace: true,
          letterswithspace: true,
    
            minlength: 3,
            maxlength: 50,
         },
        made_on_day: {
            required: true,
            //minlength: 2
         },
        made_on_month: {
            required: true,
            minlength: 3
         },
        made_on_year: {
            required: true,
            minlength: 2,
         },
        
        between: {
            required: true,
            alphanumericspace: true,
            minlength: 3,
            maxlength: 100
         },

        employe_sin: {
            required: true,
            number: true,
            minlength: 2,
         },
        sin_name: {
            required: true,
            letterswithspace: true,
            minlength: 3,
            maxlength: 50,
         },

       /* pad1: 'required',
        pad2: 'required',
        pad3: 'required',
        pad4: 'required',
        pad5: 'required',
        pad6: 'required',
        pad7: 'required',
        pad8: 'required',
        pad9: 'required',*/
        pad10: 'required',

        pad11: 'required',
        pad12: 'required',
        pad13: 'required',
        pad14: 'required',

        //pad21: 'required',

        agreement_on_day: {
            required: true,
            minlength: 2,
         },
        agreement_on_month: {
            required: true,
            minlength: 3,
         },
        agreement_on_year: {
            required: true,
            minlength: 2,
         },

        witness_name: {
           // required: true,
            letterswithspace: true,
            minlength: 3,
         },
        employee_name: 'required',
        agreementDate: 'required',
        emp_id: 'required',
        
      },
      errorPlacement: function(error, element){

          if(status == 1){

             toastr.error('Please enter all required fields', 'Unsuccessful !');
        

             status = 2;
          }

          return false;
      },
      submitHandler: function (form) { 
        
      var fd = new FormData(); 

      fd.append('madeIn', $('#made_in').val());
      fd.append('madeOnDay', $('#made_on_day').val()); 
      fd.append('madeOnMonth', $('#made_on_month').val()); 
      fd.append('madeOnYear', $('#made_on_year').val());

      fd.append('between', $('#between').val()); 

      fd.append('employeSin', $('#employe_sin').val()); 
      fd.append('sinName', $('#sin_name').val()); 

      fd.append('harassmentPolicy', $('.page1 img').data('image')); 
      fd.append('violencePolicy', $('.page2 img').data('image')); 
      fd.append('latenessPolicy', $('.page3 img').data('image')); 
      fd.append('abusePolicy', $('.page4 img').data('image')); 
      fd.append('standardsPolicy', $('.page5 img').data('image'));
      fd.append('workPolicy', $('.page6 img').data('image')); 
      fd.append('reatingPolicy', $('.page7 img').data('image')); 
      fd.append('privacyPolicy', $('.page8 img').data('image')); 
      fd.append('employeeHandbook', $('.page9 img').data('image')); 
      fd.append('termsAndConditions', $('.page10 img').data('image'));  

      fd.append('madWallPoliciesState', $('.page11 img').data('image'));  
      fd.append('personalProtectiveEquipment', $('.page12 img').data('image'));  
      fd.append('employmentStandards', $('.page13 img').data('image'));  
      fd.append('independentLegalAdvice', $('.page14 img').data('image'));  

      fd.append('witnessSignature', $('.page21 img').data('image'));  

      fd.append('agreementOnDay', $('#agreement_on_day').val());
      fd.append('agreementOnMonth', $('#agreement_on_month').val()); 
      fd.append('agreementOnYear', $('#agreement_on_year').val()); 

      fd.append('witnessName', $('#witness_name').val());
      fd.append('employeeName', $('#employee_name').val());
      fd.append('agreementDate', $('#agreementDate').val());
      fd.append('employeeId', $('#emp_id').val());

      var route = $('#myform').data('route');
        
        $.ajax({
           url:route,
           type:'POST',
           data: fd,
           processData: false,
           contentType: false,
           dataType :'json',
           beforeSend : function() {
               //  addLoader();
               $('.loading').show();
            },
           success:function(data){
            console.log(data);
            //swal("Success!", "Saved Successfully!", "success");
            //toastr.success('Saved Successfully.', 'Success !!')
           /* webkit.messageHandlers.formAction.postMessage(
                "Submit"
            );*/
            window.location.href = data.url;

          },
          error: function() { 

            //alert("Fail");
            console.log('fail');
                  
           }
        });

    }
  });

$(".discard").on("click", function(){

    /*webkit.messageHandlers.formAction.postMessage(
    "Discard"
    );*/

});

$(".save").on("click", function(){

    
      var fd = new FormData(); 

      fd.append('madeIn', $('#made_in').val());
      fd.append('madeOnDay', $('#made_on_day').val()); 
      fd.append('madeOnMonth', $('#made_on_month').val()); 
      fd.append('madeOnYear', $('#made_on_year').val());

      fd.append('between', $('#between').val()); 

      fd.append('employeSin', $('#employe_sin').val()); 
      fd.append('sinName', $('#sin_name').val()); 

      fd.append('harassmentPolicy', $('.page1 img').data('image')); 
      fd.append('violencePolicy', $('.page2 img').data('image')); 
      fd.append('latenessPolicy', $('.page3 img').data('image')); 
      fd.append('abusePolicy', $('.page4 img').data('image')); 
      fd.append('standardsPolicy', $('.page5 img').data('image'));
      fd.append('workPolicy', $('.page6 img').data('image')); 
      fd.append('reatingPolicy', $('.page7 img').data('image')); 
      fd.append('privacyPolicy', $('.page8 img').data('image')); 
      fd.append('employeeHandbook', $('.page9 img').data('image')); 
      fd.append('termsAndConditions', $('.page10 img').data('image'));  

      fd.append('madWallPoliciesState', $('.page11 img').data('image'));  
      fd.append('personalProtectiveEquipment', $('.page12 img').data('image'));  
      fd.append('employmentStandards', $('.page13 img').data('image'));  
      fd.append('independentLegalAdvice', $('.page14 img').data('image'));  

      fd.append('witnessSignature', $('.page21 img').data('image'));  

      fd.append('agreementOnDay', $('#agreement_on_day').val());
      fd.append('agreementOnMonth', $('#agreement_on_month').val()); 
      fd.append('agreementOnYear', $('#agreement_on_year').val()); 

      fd.append('witnessName', $('#witness_name').val());
      fd.append('employeeName', $('#employee_name').val());
      fd.append('agreementDate', $('#agreementDate').val());
      fd.append('employeeId', $('#emp_id').val());

      var route = $('#myform').data('route');
        
        $.ajax({
           url:route,
           type:'POST',
           data: fd,
           processData: false,
           contentType: false,
           dataType :'json',
           beforeSend : function() {
               //  addLoader();
            },
           success:function(data){
            console.log(data);
            //swal("Success!", "Saved Successfully!", "success");
            toastr.success('Saved Successfully.', 'Success !!');


            /*webkit.messageHandlers.formAction.postMessage(
             "Save"
            );*/
           

          },
          error: function() { 

            //alert("Fail");
            console.log('fail');
                  
           }
        });
  
});

</script>

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

  var $j = jQuery.noConflict();
  $j( function() {
     /*$j(".madeOn").datepicker({
      dateFormat: 'dd-MM-y',
            onSelect: function (dateText, inst) {
                var date = $j(this).val().split('-');

                var day =  getOrdinalNum(parseInt(date[0]));
                var month = date[1];
                var year = date[2];

                $j('#made_on_day').val(day);
                $j('#made_on_month').val(month);
                $j('#made_on_year').val(year);    
        }
        });*/
  });

  $j( function() {
     /*$j(".agreementOn").datepicker({
      dateFormat: 'dd-MM-y',
            onSelect: function (dateText, inst) {
                var date = $j(this).val().split('-');

                var day =  getOrdinalNum(parseInt(date[0]));
                var month = date[1];
                var year = date[2];

                $j('#agreement_on_day').val(day);
                $j('#agreement_on_month').val(month);
                $j('#agreement_on_year').val(year);    
        }
        });*/
  });

  function getOrdinalNum(n) {
    return n + (n > 0 ? ['th', 'st', 'nd', 'rd'][(n > 3 && n < 21) || n % 10 > 3 ? 0 : n % 10] : '');
  }


  
</script>
