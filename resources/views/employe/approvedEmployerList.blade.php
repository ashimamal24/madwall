@extends('admin.layout')

@section('title')
	Approved	
@endsection 
@section('content')
<h3 class="page-title">
	Approved Employer
</h3><br>
<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url( 'admin/dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ url( 'admin/list-approvedemployer' ) }}"> Approved Employer</a>
			
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
@include('flash::message')
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-body">
					<div class="table-container">
						<input type="hidden" name="action" value="filter-approvedemployer"/>
						<div class="table-custom table-responsive">
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax_for_approved_employer">
							<thead>
							<tr role="row" class="heading">
								<th width="5%">No.</th>
								<th width="15%">Employer Id</th>
								<th width="15%">Company Name</th>
								<th width="15%">Contact Name</th>
								<th width="10%">Type of Industry</th>
								<th width="10%">Location</th>
								<th width="10%">Email</th>
								<th width="10%">Mobile Number</th>
								<th width="25%">Status</th>
								<th width="15%">Action</th>
							</tr>
							<tr role="row" class="filter">
								<td></td>
								<td></td>
								<td><input type="text" class="form-control form-filter input-sm" name="name" id="skillname" autocomplete="off"></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>{{ Form::select('status', array(''=>'--select--','1' => 'Unblock', '0' => 'Block' ),null,['class' => 'form-control form-filter input-sm']) }}</td>
								<td>
									<button style="display:none;" class="btn btn-sm yellow filter-submit margin-bottom"></button>
									<button title="Clear" class="btn btn-sm red filter-cancel">Clear</button>	
								</td>
							</tr>
							</thead>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End: life time stats -->
	</div><!-- END PAGE CONTENT-->
	@include('employe.popup')

@endsection
@section('js')
<script src="{{ asset( 'public/admin/js/mwapprovedemployer.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.form.js') }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {
 
 /***********user ajax view *******/
 
 $('#myModalHorizontal').on("hidden.bs.modal", function () {
		$('#reason_for_block_usrId').val('');
	});
	
	/** Change Status **/
   $(document).on('click','#change-common-status', function(){
		var $this = $(this);
		var table = $this.data('table');
		var token = $('meta[name=csrf-token]').attr("content");
		var id = $this.data('id');
		var status = $this.data('status');
		var action = $this.data('action');
		var approved = $this.data('approved');
		if(approved == 1 ){
            $('input#userid').val(id);
            $('#myModalHorizontal').modal('show');
        	return false;
        }
		bootbox.confirm('Are you sure you want to '+(status == true ? "Block" : "Unblock")+' this user ?', function (result) {
            if (result) {
                $.ajax({
					url: path+'admin/employer-change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status+'&action='+action+'&_token='+token+'&approved='+approved,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					success: function(json){
						if ( json.success ) {
							showSuccessMessage('Employer has been enabled now.');
							TableAjax.refresh();
						} else if (json.exception_message) {
							showErrorMessage('Something went wrong!!');
							TableAjax.refresh();
						}else{
							window.location.reload(true);
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						showErrorMessage('Something went wrong!!');
					}
				});
            }
        });
	});
	
	
   $(document).on('click','.delete-user', function(){
		var $this = $(this);
		var url = $(this).attr('data-url');
		bootbox.confirm('Are you sure you want to delete this user ?', function (result) {
            if (result) {
               window.location.href = url;
            }
        });
	});
   $(document).on('click','.verify-user', function(){
		var $this = $(this);
		var url = $(this).attr('data-url');
		bootbox.confirm('Are you sure you want to mark this user email verified ?', function (result) {
            if (result) {
              window.location.href = url;
            }
        });
	});
	


	$('#block-user-reason ').click(function(){  
        //var token = "{{ csrf_token() }}";
        $("#block-user-form").ajaxSubmit({
        	method      : 'post',
        	url : path+'admin/block-employer',
        	beforeSend  : function() {
                addLoader();
            },
            success     : function(data) {
				$('input#userid').val('');
				$('#myModalHorizontal').modal('hide');
                removeLoader();
                if ( data.success == true ) {
					showSuccessMessage(data.message);
					TableAjax.refresh();
				} else if (data.success == false) {
					showErrorMessage(data.message);
					TableAjax.refresh();
				}else{
					window.location.reload(true);
				}
            },
            error       : function(xhr, ajaxOptions, thrownError) {
                removeLoader();
                $( "#block-user-form .form-group" ).removeClass( "has-error" );
                $( ".help-block" ).hide();
                var j=0; 
                $.each( xhr.responseJSON, function( i, obj ) {
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'input[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').addClass('has-error');
                    $( 'textarea[name="'+i+'"]' ).closest( '.form-group').find('label.help-block').slideDown(400).html(obj);

                    if( i=='reason_for_block_usr' ){
                        $('.reason_for_block_usr').addClass('has-error');
                        $('.reason_for_block_usr' ).find('label.help-block').slideDown(400).html(obj);
                    }
                });
            }
        });
    });


	TableAjax.init();
	TableAjax.update();
});
</script>

@endsection
