<?php

return [

    /*
    |--------------------------------------------------------------------------
    | error Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
     
    //commons
    'error'=>'Something went wrong',

    //blogs Category
    'add_blog_category' => 'Blog category added successfully.',
    'update_blog_category' => 'Blog Category updated successfully.',
    'delete_blog_category' => 'Blog Category deleted successfully.',
    'block_blog_category' => 'Status Changed successfully.',
    'block_message_category' => 'Category cannot be blocked as it is associated with some blog(s).',
    'not_delete_category' => 'Category cannot be deleted as it is associated with some blog(s).',

    //Blogs 
    'add_blog' => 'Blog added successfully.',
    'update_blog' => 'Blog updated successfully.',
    'delete_blog' => 'Blog deleted successfully.',
    'block_blog' => 'Status Changed successfully.',
   
   
    //Comments
    'delete_comment'=>"Comment deleted successfully.",

    //industry_content
    'add_industry_content' => 'Industry Content added successfully.',
    'update_industry_content' => 'Industry Content updated successfully.',
    'delete_industry_content' => 'Industry Content deleted successfully.',
    'status_industry_content' => 'Status changed.',

    //Reply
    'add_reply'=>'Comment replied successfully.',
    'reply_limit_exceeded'=>'Sorry, maximum limit for the replies has been reached.',

    //title
    'add_title' => 'Title added successfully.',
    'update_title' => 'Title updated successfully.',
    'delete_title' => 'Title deleted successfully.',
    'status_title' => 'Status changed.',


    //_highlights
    'add_highlight' => 'Highlight added successfully.',
    'update_highlight' => 'Highlight updated successfully.',
    'delete_highlight' => 'Highlight deleted successfully.',
    'status_highlight' => 'Status changed.',
  
];
