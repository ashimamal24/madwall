<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Breadcrumbs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
     
     //blog
     'blog' => ' Manage Blogs',
     'add_blog' =>'Add Blog',
     'edit_blog' =>'Edit Blog',
     
     
     //blog_category
     'blog_category'=>'Manage Blog Category',
     'add_blog_category'=>'Add Blog Category',
     'edit_blog_category'=>'Edit Blog Category',


     //comments
     'comments'=>'Manage Comments',
      'add_reply'=>'Add Reply',
   
     //industry content
     'industry_content'=>'Manage Industry Content',
     'add_industry_content'=>'Add Industry Content',
     'edit_industry_content'=>'Edit Industry Content',

      //Title
     'title'=>'Manage Title',
     'add_title'=>'Add Title',
     'edit_title'=>'Edit Title',

     //Highlight
     'highlight'=>'Manage Highlight',
     'add_highlight'=>'Add Highlight',
     'edit_highlight'=>'Edit Highlight',

];
