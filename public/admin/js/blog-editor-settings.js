// text editor settings
$("#description1").Editor({

'l_align':true,

'r_align':true,

'c_align':true,
'justify':true,
'indent':true,

'outdent':false,
'block_quote':false,
'undo':true,

'redo':true,

'insert_link':true,

'unlink':false,

'insert_img':false,
'insert_table':true,
'strikeout':false,
'splchars':true,
'hr_line':true,
'print':false,

'rm_format':false,
'select_all':false,
'source':false,
'togglescreen':true
});


$("#description2").Editor({

'l_align':true,

'r_align':true,

'c_align':true,
'justify':true,
'indent':true,

'outdent':false,
'block_quote':false,
'undo':true,

'redo':true,

'insert_link':true,

'unlink':false,

'insert_img':false,
'insert_table':true,
'strikeout':false,
'splchars':true,
'hr_line':true,
'print':false,

'rm_format':false,
'select_all':false,
'source':false,
'togglescreen':true
});

// set text in text editor
	var editor_data = $( "#description1" ).val();
	$( "#description1" ).Editor( "setText",editor_data );
	
	$("#addblog").click(function() {
		$('#description1').val($('#description1').Editor("getText") );
	});


	var editor_data = $( "#description2" ).val();
	$( "#description2" ).Editor( "setText",editor_data );
	
	$("#addblog").click(function() {
		$('#description2').val($('#description2').Editor("getText") );
	});

