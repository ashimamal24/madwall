var TableAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true
        });
    }
    
    var handleRecords = function () {
		
		var action = $('input[name="action"]').val();
		
        var grid = new Datatable();

        grid.init({
            src: $("#datatable_ajax_for_industry_content"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            "processing": true,
			"serverSide": true,
            dataTable: { 
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": path+adminname+'/'+action, // ajax source
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
                },
                "aoColumnDefs" : [
				 {
				   'bSortable' : false,
				   'aTargets' : [ 0,1,2,3,4,5,6,7]
				 }],
                "order": [
                    [0, "desc"]
                ],
                "language": {
                  "emptyTable": "No results found"
                },
                "bInfo":false,
            }
        });

     
    }
    
   var customFunction = function () {
		$("#datatable_ajax_for_industry_content input").not('input[type="checkbox"]').on('keyup',function (e) {
			$('.filter-submit').click();  
		});
		$("#datatable_ajax_for_industry_content select").not('input[type="checkbox"]').on('keyup change',function (e) {
			$('.filter-submit').click();  
	   });
   }
   var refreshRecords = function () {
	   var $myTable = $("#datatable_ajax_for_industry_content").dataTable( { bRetrieve : true } );
		$myTable.fnDraw();
   }

    return {

        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecords();
        },
         update: function () {
            customFunction();
        },
        refresh: function () {
            refreshRecords();
        }

    };

}();
