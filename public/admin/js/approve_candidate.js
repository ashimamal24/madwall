$(document).ready(function(){
	/*
        DESC : Autoload categories when industry is selected.
        */
        $(document).on('change','#industry_id',function(){
			//var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			var industry_id = $(this).val();
			$.ajax({
				url:path+'admin/industry-category',
				type:'get',
				data:'industry_id='+industry_id,
				dataType:'json',
				beforeSend:function(){
					addLoader();
				},
				success:function(data){
					removeLoader();
					$('#subcategory').html('').trigger('chosen:updated');
					$('#skills').html('').trigger('chosen:updated');
					$('#category_id').html('');
					var data1 = '<option value="">Please select category</option>';
					var optgroup = [];
					if(data.category)
					{
						
						$.each(data.category, function(index, element) {
							
							if(optgroup.indexOf(element.industry_name) == -1){
								
								if(optgroup.length){
									data1 +='</optgroup><optgroup label="'+element.industry_name+'">';
									
								}else{
									data1 +='<optgroup label="'+element.industry_name+'">';
								}
							}
						   optgroup.push(element.industry_name);
						   data1 +='<option value="'+element._id+'">'+element.name+'</option>';						   
						});
					}
					
					$('#category_id').html(data1);
					//jcf.getInstance($('#category_id')).refresh();
					//$('#multiselect').html('');
					$('#category_id').trigger("chosen:updated");
				},
				error:function(errors){
					removeLoader();
				},
				complete:function(){
					removeLoader();
				}
			});
			
		});
        
        //~ $('#category_id').on('change', function(){
            //~ $('#category_hidden_name').val($('option:selected',this).text());
        //~ });
		
		/*
		Auto load subcategories when categories are selected
		*/
        $('.category-ajax').on('change', function(){
            $('#subcategory').val('').trigger('chosen:updated');
            $('#skills').val('').trigger('chosen:updated');
			var catid = $(this).val();
			if(catid) {
				$.ajax({
					url : path+'admin/select-subcategory/'+catid,
					type: "GET",
					dataType: "json",
					success:function(data) {
						var optgroup = [];
						var subcategory ='';
						$.each(data, function(key, value) {
							if(optgroup.indexOf(value.category_name) == -1){
								
								if(optgroup.length){
									subcategory +='</optgroup><optgroup label="'+value.category_name+'">';
									
								}else{
									subcategory +='<optgroup label="'+value.category_name+'">';
								}
							}
						   optgroup.push(value.category_name);
							subcategory+='<option value="' + value._id + '">' + value.name + '</option>';
						});
						$("#subcategory").html(subcategory);
						$("#subcategory").trigger("chosen:updated");
						$('#subcategory').chosen({ width: "95%" });
					}
				});
			}else{
				$('#subcategory').val('').trigger('chosen:updated');
			}
		});

    $('.subcategory-ajax').on('change', function(){
        var subCategory_id = $(this).val();
        if(subCategory_id) {
            $.ajax({
                url : path+'admin/select-skills/'+subCategory_id,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    var skills ='';
                    //~ data.forEach(function(data) {
                        //~ skills+='<option value="' + data.id + '">' + data.name + '</option>';
                    //~ });
                    
                    
                    
                    if(data.skills)
					{
						$.each(data.skills, function(index, element) {
							
							skills +='<option value="'+element._id+'"';
							if(data.mandatory){
								
								if($.inArray(element._id, data.mandatory) != -1){
									skills +='selected';
								}
							}
							
							skills += '>'+element.name+'</option>';
						});
					}
                    
                    
                    
                    
                    
                    
                    $("#skills").html(skills);
                    //$('#skills option').prop('selected', true);
                    $("#skills").trigger("chosen:updated");
                    $('#skills').chosen({ width: "95%" });              
                }
            });
        }else{
            $('#skills').val('').trigger('chosen:updated');
        }
    });
});
