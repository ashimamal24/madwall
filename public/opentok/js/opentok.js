var apiKey = $('#apiKey').val();
var sessionId = $('#sessionId').val();
var token = $('#token').val();
var subscriberLoginStatus = 1;

//Play Ring tone
var  audioUrl = window.location.origin+'/beta/public/opentok/ringtone.wav';
//console.log(audioUrl);
var audio = new Audio(audioUrl);

/*
var apiKey = $('#apiKey').val();
var sessionId = '1_MX40NjQ0NjEyMn5-MTU3MzExNjM4Nzc4OX5LbzRFcTNpVE93bXZHekhtK0JwdEp4dUR-UH4';
var token = 'T1==cGFydG5lcl9pZD00NjQ0NjEyMiZzaWc9MjNiNzU2ZDMyMGI5OGZkNDhiYmMyYzI4NTI0NjM1OWI4MDU4NGRjMzpzZXNzaW9uX2lkPTFfTVg0ME5qUTBOakV5TW41LU1UVTNNekV4TmpNNE56YzRPWDVMYnpSRmNUTnBWRTkzYlhaSGVraHRLMEp3ZEVwNGRVUi1VSDQmY3JlYXRlX3RpbWU9MTU3MzExNjM4NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNTczMTE2Mzg3Ljk0NDUxNjI0MDk5MzkzJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9';
*/
console.log('apiKey: '+apiKey);
console.log('sessionId: '+sessionId);
console.log('token: '+token);



// (optional) add server code here
/*

var session = OT.initSession(apiKey, sessionId);
session.connect(token, function(error) {
  if (error) {
    console.log("Error connecting: ", error.name, error.message);
  } else {
    console.log("Connected to the session.");
  }
});


session.on("sessionDisconnected", function (event) {
  // The event is defined by the SessionDisconnectEvent class
  if (event.reason == "networkDisconnected") {
    alert("Your network connection terminated.")
  }
});
session.connect(token);




initializeSession();



function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);


  session.on("sessionDisconnected", function (event) {
  // The event is defined by the SessionDisconnectEvent class
  if (event.reason == "networkDisconnected") {
    alert("Your network connection terminated.")
  }
});
session.connect(token);

  // Subscribe to a newly created stream
  // Create a publisher
  var publisher = OT.initPublisher('publisher', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);

  // Connect to the session
  session.connect(token, function(error) {
    // If the connection is successful, publish to the session
    if (error) {
      handleError(error);
    } else {
      session.publish(publisher, handleError);
    }
  });

  // Subscribe to a newly created stream
  // Create a subscriber
  session.on('streamCreated', function(event) {
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    }, handleError);
  });

}*/


// Handling all of our errors here by alerting them
function handleError(error) {
  if (error) {
    alert(error.message);
  }
}

// Initialization
var session = OT.initSession(apiKey, sessionId);




// Create Connection
session.connect(token, function (error) {

  if (error) {
    console.log("Failed to connect.");
  } else {
    console.log('You have connected to the session.');


    audio.play();

    setTimeout(function() {

        if(subscriberLoginStatus == 1){
          console.log('subscriber not picking the call');
          timeoutDisconnect();
        }

      }, 30000);

  }
});


// Disconnect Connection
function disconnect() {
  //alert('A client disconnected.');
  console.log('call disconnected');
  $(".screen").css("display", "none");

  session.disconnect();
  //window.history.back();
  //window.history.go(-2);
  ///admin/list-waitingemployee?callStatus=1&id=1234
   var  url = path+'/admin/list-waitingemployee?callStatus=1&id='+user_id;
   window.location.href = url;
}

// Disconnect Connection
function timeoutDisconnect() {
  //alert('A client disconnected.');
  console.log('call disconnected');
  $(".screen").css("display", "none");

  session.disconnect();
  window.history.back();
  //window.history.go(-2);
  ///admin/list-waitingemployee?callStatus=1&id=1234
  // var  url = path+'/admin/list-waitingemployee?callStatus=1&id='+user_id;
  // window.location.href = url;
}





var connectionCount = 0;

session.on({

  // Call Start
  connectionCreated: function (event) {


    connectionCount++;
    if (event.connection.connectionId != session.connection.connectionId) {
      console.log('Another client connected. ' + connectionCount + ' total.');
      //audio.stop();
      audio.pause();
      subscriberLoginStatus = 2;

      $(".screen").css("display", "block");
      $("#container").css("display", "none");
      $("#discardOverlay").css("display", "block");
       //document.getElementById("discardOverlay").style.display = "block";
      
      //$("#videos").css("display", "block");
      

      var publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      }, handleError);

      session.publish(publisher, handleError);
    }
  },

  // Call End
  connectionDestroyed: function connectionDestroyedHandler(event) {
    connectionCount--;
    console.log('A client disconnected. ' + connectionCount + ' total.');

    disconnect();
    // $(".screen").css("display", "none");
    //alert('A client disconnected.')
   // session.disconnect();
  }

});


 // Create a subscriber
  session.on('streamCreated', function(event) {
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    }, handleError);
  });







/*
var session;
var connectionCount = 0;

function connect() {
  // Replace apiKey and sessionId with your own values:
  session = OT.initSession(apiKey, sessionId);
  session.on({
    connectionCreated: function (event) {
      connectionCount++;
      console.log(connectionCount + ' connections.');
    },
    connectionDestroyed: function (event) {
      connectionCount--;
      console.log(connectionCount + ' connections.');
    },
    sessionDisconnected: function sessionDisconnectHandler(event) {
      // The event is defined by the SessionDisconnectEvent class
      console.log('Disconnected from the session.');
      document.getElementById('disconnectBtn').style.display = 'none';
      if (event.reason == 'networkDisconnected') {
        alert('Your network connection terminated.')
      }
    }
  });
  // Replace token with your own value:
session.connect(token, function (error) {

  var publisher = OT.initPublisher('publisher', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);


  if (error) {
    console.log("Failed to connect.");
  } else {
    console.log('You have connected to the session.');
    session.publish(publisher, handleError);
  }
});
}

function disconnect() {
  session.disconnect();
}

connect();

*/