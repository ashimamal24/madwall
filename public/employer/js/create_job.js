
$('#physical_requirement').richText();
$('#safety_hazards').richText();
$('#description').richText();

$(document).ready(function(e) {
		
		/*
		DESC : to implement chosen
		*/
		$(".chosen-select").chosen();
		$('#multiselect_chosen').removeAttr('style');		
		$('.default').removeAttr( 'style' );
		$('#dates').val('');
		$('#job_published_type').val('');
		var date = new Date();
		var ser_date = new Date();
		var s_date = new Date(ser_date);
		var currentMonth = date.getMonth();
		var currentDate = date.getDate();
		var currentYear = date.getFullYear();
		
		/**
		DESC : to display datepicker and to validate that only 30 days job can be posted.
		*/
		$('#datepicker').datepicker({
			startDate: s_date, 
			multidate:true,
			changeMonth:true,
			changeYear:false,
			endDate: new Date(currentYear, currentMonth, currentDate+90),
			maxViewMode: 0
		}).on('changeDate', function(e){
			date_val = Array();	//to collect all the dates when job post form will be submitted.		
			date_Formatval = Array();	//to collect all the dates when job post form will be submitted.		
			for(var i = 0;i<e.dates.length;i++)
			{
				e.dates.sort(function(a,b)
				{
					a = new Date(a);					
					b = new Date(b);
					return a-b;
				});
				var new_date = myDateFormatter(e.dates[i]);
				var formatted_dates = format_date_month_day(e.dates[i]);
				date_val.push(new_date);
				date_Formatval.push(formatted_dates);
			}
			if(date_val.length<=30){
				$('span.errormsge').hide();
			}
			$('#dates').val(date_val);//populate all the dates to a hidden field to be submitted to the server
			$('#formatted_dates').val(date_Formatval);//populate all the dates to a hidden field to be shown into popup (formatted)
			//update or remove job shift date from temp process details 
			remove_temp_details(date_val);
			if(date_val.length>30){
				$('.errormsge').show();
				$('.errormsge').html('Maximum job length can be 30 days only');
			}	
		});
		
		/*
		DESC : when user enters job address, show suggestions using google api
		*/
		$("#location").geocomplete({
          details: ".pickLocation",
          types: ["geocode", "establishment"],
        }).bind("geocode:result", function(event, result){
            var coor = result.geometry.location.lat();
            $('#key').val(1);
            $('.pickLocation').find('span.error_msgg').hide();  
        }).bind("blur", function(event, results){
            setTimeout(function(){
              if($('#key').val() == '')
              {
              	if($('#location').val())
              	{
	                $('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid location for the job.');
	                $('#lat').val('');
	                $('#lng').val('');
              	}
              	else
              	{
              		$('.pickLocation').find('span.error_msgg').hide();
              	}
              }
              $('#key').val('');
            },1000);
        });
        
        
        /*
		 * DESC : to check shift duration when lunch time is updated
		 * */
		$(document).on('change','#lunch_hour',function(){
			if($('#lunch_hour :selected').val() != ''){
				//check duration
				save_shifts_with_same_time();
			}
		});
        
        
        /*
		DESC : to populate current time in start and end time fields.
		*/
		var Time = new Date();
		var minutesTime = 30;
		var hourTime = Time.getHours();
		if(Time.getMinutes() > 30)
		{
			hourTime = hourTime+1;
			minutesTime = '00';
		}
		//~ pickuptime =(("0" + (hourTime)).slice(-2))+':'+minutesTime;
		//~ $('#start_date').val(pickuptime);
		//~ $('#end_date').val(pickuptime);
		//~ var pick = "{{$startTime}}";
		//~ var drop = "{{$startTime}}";
		//~ if(pick != '' && drop != '')
		//~ {
			//~ $('#start_date').val("{{$startTime}}");
			//~ $('#end_date').val("{{$endTime}}");	
		//~ }

		$('#start_date').timepicker({timeFormat: "H:i",'step': '15'});
		$('#end_date').timepicker({timeFormat: "H:i",'step': '15'});
		
		/*
		DESC : to reset end time when start time is updated
		*/
		$(document).on('change','#start_date',function(){
			var pair = [ ['00:00',$('#start_date').val()]]
			$('#end_date').val(($('#start_date').val()));
		});
		
		/*
		 * DESC : to update start/end time for all the selected dates when end time is selected
		 * */
		$(document).on('change','#end_date',function(){
			var end_time = $(this).val();
			var start_time = $('#start_date').val();
			if(start_time != '' && end_time != '' && start_time != end_time){
				//call same_job_time to save temporary data for this job post
				save_shifts_with_same_time();
			}
			
		});
		
		
		/*
		DESC : to display set shift time modal
		*/
		$(document).on('click','#set_jobTime',function(e){
			e.preventDefault();
			$('#show_err').empty().removeClass('alert-danger').hide();
			//check selected dates
			var selectedDates = $('#dates').val();
			var formatted_dates = $('#formatted_dates').val();
			var date_arr = selectedDates.split(',');
			var format_arr = formatted_dates.split(',');
			//if no date is selected, show error message
			if(selectedDates == ""){
				$('.errormsge').show();
				$('.errormsge').html('Please specify job days for the job.');
				return false;
			}else{
				var date_fields = '';
				if($('#process_id').val() != ''){
					process_details();
				}else{
					var start_time = $('#start_date').val();
					var end_time = $('#end_date').val();
					for (var i = 0; i < date_arr.length; i++) {
						//Do something				
						date_fields += '<div class="row"><div class="col-md-4"><label class="center_align_label">'+format_arr[i]+'</label></div><div class="col-md-4"><div class="field_forms"><div class="label_form"><label>Start Time</label></div><div class="form_inputs"><input class="setTime_job form-control" type="text" name="start[time_'+date_arr[i]+']" value="'+start_time+'" onkeydown="return false"><span class="error_msgg" style="display:none;"></span><span class="error_msgg" style="display:none;"></span></div></div></div>';
						date_fields += '<div class="col-md-4"><div class="field_forms"><div class="label_form"><label>End Time</label></div><div class="form_inputs"><input class="setTime_job form-control" type="text" name="end[time_'+date_arr[i]+']" value="'+end_time+'" onkeydown="return false"><span class="error_msgg" style="display:none;"></span></div></div></div></div>';
					}
					$('#append_shiftFieldHere').html(date_fields);
					//add timepicker for all the fields.
					$('.setTime_job').each(function(){
						$(this).timepicker({timeFormat: "H:i",'step': '15'});
					});
					
					$('#set_shiftTime').modal('show');
				}
			}
		});
		
		/*
		DESC : when OK button is clicked from set shift popup
		*/
		$(document).on('click','#time_selected',function(){
			//send an ajax request to server
			$('#show_err').empty().hide();
			var data_check = true;
			//check if time is selected for all the selected dates
			$('.setTime_job').each(function(){
				if($(this).val() == ''){
					data_check = false;
				}
			});
				
			if(data_check == false){
				$('#show_err').empty().addClass('alert-danger').html("Please select start and end time for all the selected dates").fadeIn();
				return false;
			}
			
			var lunch_hour = 0;
			if($('#unpaid_lunch').is(':checked')){
				lunch_hour = $('#lunch_hour').val();
			}
			
			$.ajax({
				url: path+adminname+'/save-shifts',
				type: 'post',
				dataType: 'json',
				data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&'+$('.setTime_job').serialize()+'&lunch_hour='+lunch_hour,
				beforeSend:function(){
					Loader();
				},
				success: function(data) {
					if(data.success == true){
						$('#show_selectedDates_here').html(data.shift_html);
						$('#process_id').val(data.process_id);
						$('#set_shiftTime').modal('hide');
					}else{
						//show error message
						$('#show_err').empty().addClass('alert-danger').html(data.err_message).fadeIn();
					}
					RemoveLoader();
				},
				error : function(err){
					RemoveLoader();
					//show error message
					$('#show_err').empty().addClass('alert-danger').html(err.err_message).fadeIn();
				}
			});
			
		});
	
	

		/*
		DESC : to hide modal popup in sfari and internet explorer
		*/
		$("#rehire_fr_job,#manul_hiring,#automatic_hiring").on("hidden.bs.modal", function () {
			$('.modal-backdrop').removeClass('in');
		});
		$("#manul_hiring_be_advc").on("shown.bs.modal", function () {
			$('.modal-backdrop').addClass('in');
		}); 
		
		/*
		DESC : validate job post form
		*/
		jQuery("form#addjob").validate({
			rules: {
			  "title":{
				 required:true 
			  },
			  "description":{
				required:true 
			  },
			  "start_date":{
				required:true
			  },
			  "end_date":{
				required:true
			  },
			  "salary_per_hour":{
				required:true
			  },	      
			  "address":{
				required:true
			  }
			},
			messages: {
				"title":{
				  required:"Please specify Job Name."
				},	        
				"description":{
				  required:"Please specify Job Description."
				},
				"start_date":{
				  required:"Please specify start time of the job."
				},
				"end_date":{
				  required:"Please specify end time of the job."
				},
				"salary_per_hour":{
				  required:"Please specify Salary per hour of the job in dollars."
				},
				"address":{
				  required:"Please specify job location for the job."
				}
			},
			errorElement:'span',
			errorClass:'error_msg errormsges',
			submitHandler: function(form) {				
				$('span.error_msgg').hide();
				//check if dates are selected or not.(3-aug-2017, shivani)
				if($('#dates').val() != '' && $('.check_skills_selected option:selected').length >= 1 && $('#subcategory').val() != '' && $('#category').val() != ''){
					if($('#job_published_type').val()==1 || $('#job_published_type').val()==0){
						//automatic/manual job has been posted
						var job_typeTitle = 'Automatic';
						if($('#job_published_type').val()==0){
							job_typeTitle = 'Manual';
						}
						//
						$('#jobPost_confirmMessage').html('<p>Are you sure you want to post this  job  as <b>'+ job_typeTitle +' hiring?</b></p>');
						$('#job_postConfirm').modal('show');
					}else{
						 //rehire case
						//call function to display users to be rehired.
						rehire_user_check();
						
					}
				}		    		
			}
		});

		
		$("#DisableJobAppAction").on("hidden.bs.modal", function () {
			$('#disable_message_here').empty();
		});

		/*
		DESC : when submit button is clicked from rehire user modal.	
		*/
		$(document).on('click','#selectedUser',function(){
			
			var count = 0;
			
			$('.rehire_checkbox').each(function(){
				
				if($(this).prop('checked') == true){
					count = count + 1;
				}
				
			});
			
			
			if(count==0){			
				$('#rehire_error_messages').addClass('alert-danger').html('Please select atleast 1 employee').fadeIn('slow').delay(3000).fadeOut(); 
				return false;
			}
			
			var jobType_selected = $('input[name="rehire_type"]:checked').length;
			
			if(jobType_selected==0){			
				$('#rehire_error_messages').addClass('alert-danger').html('Please select Job type to hire remaining employees').fadeIn('slow').delay(3000).fadeOut(); 
				return false;
			}
			
			
			
			$('#automatic_hiring').modal('hide');
			$('.manul_hiring').modal('hide');	  
			close_modals();
			var d1 = $('#dates').val();
			var post =0;
			var cur_date = d1.split(',')
			var CurrentDate = moment().format('DD-MM-YYYY');
			var f_date = moment(cur_date[0],'DD-MM-YYYY').format('YYYY-MM-DD');
			//check job duration i.e.. it can not be less than 3 hours.
			var douration = moment.duration(moment(f_date+' '+$('input[name="start_date"]').val(),'YYYY-MM-DD HH:mm').diff(moment()));
			//serialize job post form
			var formatdata = $('form#addjob').serialize();
			var value = ''; 
			$('#rehirevalue').val('');
			postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
			//~ if(CurrentDate==cur_date[0])
			//~ {
				//~ $('.rehire_fr_job').modal('hide');
				//~ if(douration.hours()>=0 &&douration.hours()<=3){
				   //~ $('#dur_days').val(douration.days());
				   //~ $('#dur_hours').val(douration.hours());
				   //~ $('#dur_cur_date').val(CurrentDate);
				   //~ $('#dur_current_date').val(cur_date[0]);
				   //~ $('.gurrntte').modal('show');
				//~ }
				//~ else
					//~ postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
			//~ }
			//~ else
				//~ postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
		});
		
		/*
		DESC : when manual/automatic job is posted.
		*/
		$(document).on('click','#hiring, #mhiring',function(){		
			$('#automatic_hiring').modal('hide');
			$('.manul_hiring').modal('hide');	  
			close_modals();
			var d1 = $('#dates').val();
			var post =0;
			var cur_date = d1.split(',')
			var CurrentDate = moment().format('DD-MM-YYYY');
			var f_date = moment(cur_date[0],'DD-MM-YYYY').format('YYYY-MM-DD');
			//check duration for a job , it should not be less than 3 hours
			var douration = moment.duration(moment(f_date+' '+$('input[name="start_date"]').val(),'YYYY-MM-DD HH:mm').diff(moment()));
			//serialize job post form
			var formatdata = $('form#addjob').serialize();
			var value = ''; 
			$('#rehirevalue').val('');
			postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
			//~ if(CurrentDate==cur_date[0])
			//~ {
				//~ $('.rehire_fr_job').modal('hide');
				//~ if(douration.hours()>=0 && douration.hours()<=3){
				   //~ $('#dur_days').val(douration.days());
				   //~ $('#dur_hours').val(douration.hours());
				   //~ $('#dur_cur_date').val(CurrentDate);
				   //~ $('#dur_current_date').val(cur_date[0]);
				   //~ $('.gurrntte').modal('show');
				//~ }
				//~ else
					//~ postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
			//~ }
			//~ else
				//~ postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
			
		});
        
	});
	
	/*
	DESC : to remove/update dates from temporary process details at server
	*/
	function remove_temp_details(new_date){
		var check_process = $('#process_id').val();
		var start_time = $('#start_date').val();
		var end_time = $('#end_date').val();
		var lunch_hour = $('#lunch_hour :selected').val();
		if(check_process != ''){
			$.ajax({
				url: path+adminname+'/remove-shifts',
				type: 'post',
				dataType: 'json',
				data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&check_date='+new_date+'&process_id='+check_process+'&start_time='+start_time+'&end_time='+end_time+'&lunch_hour='+lunch_hour,
				beforeSend:function(){
					Loader();
				},
				success: function(data) {
					if(data.success == true){
						$('#show_selectedDates_here').html(data.shift_html);
						$('#process_id').val(data.process_id);
						$('#set_shiftTime').modal('hide');
					}else{
						//show error message
						//$('#show_err').empty().addClass('alert-danger').html(data.err_message).fadeIn();
						if(data.process == 0){
							$('#process_id').val('');
							$('#show_selectedDates_here').html('');
						}
						$('.errormsge').show();
						$('.errormsge').slideDown(400).html(data.err_message);
					}
					RemoveLoader();
				},
				error : function(err){
					RemoveLoader();
					//$('#show_err').empty().addClass('alert-danger').html(err.err_message).fadeIn();
					$('.errormsge').show();
					$('.errormsge').slideDown(400).html(err.err_message);
				}
			});
		}else{
			if(start_time != '' && end_time != '' && start_time != end_time){
				save_shifts_with_same_time();
			}
		}	
	}
	
	
	function postRequest(day,hours,curdate,cur_date)
	{
		postJob();
	}

	function checkUser()
	{
		var count = $("[type='checkbox']:checked").length;
		var requireuser = $('#number_of_worker_required').val();
			close_modals();
		if(count<requireuser){
			if($("[type='checkbox']:checked").length==0){
				$('.rehireuserselect').modal('show');
			}
			else
			{
				$('#rehire_err_msg').html("Please select the required number of employees for this job");
				$('.rehireanothermethod').modal('show');
				$('#rehireModal').modal('hide');
			}
		}
		else
			postJob();	
	}

	/*
	updated : 3 aug 2017, shivani - to handle not approved user error message
	*/
	function postJob(){
		
		//hide all modals
		close_modals();
		var formatdata = $('form#addjob').serialize();
		if($('#job_published_type').val()==2){
			formatdata += '&'+$('input[name="offer[]"]:checked').serialize();
			formatdata += '&rehire_type='+$('input[name="rehire_type"]:checked').val();
		}
		
		$.ajax({
			url: $('form#addjob').attr('action'),
			type: 'post',
			dataType: 'json',
			data: formatdata,
			beforeSend:function(){
				Loader();
			},
			success: function(data) {
				//====updated
				if(data.status==false)
				{
					RemoveLoader();
					window.location.reload();
				}
				//=======end
				
				if(data.status==1)
				{
					$('#rehireModal').modal('hide');
					window.location.href=data.url;

				}
				if(data.status==0)
				{
					RemoveLoader() ;
					window.location.reload();
				}
			},
			error: function(error) { 
			  $('span.error_msg').hide();
			  $('span.error_msgg').hide();
			  $('.cat_error').hide();
			  $('.subcat_error').hide();
			  $('.skill_error').hide();
			  RemoveLoader();
			  var result_errors = error.responseJSON;
			  $('#rehireModal').modal('hide');
			  if(result_errors)
			  {

				 $.each(result_errors, function(i,obj)
				 {
					$('input[name='+i+']').parent('.form_inputs').find('span.error_msgg').slideDown(400).html(obj);
					if(i=='dates')
						$('.calender_div').find('span.error_msgg').slideDown(400).html(obj);

					if(i=='job_category')
						$('.job_category').find('span.error_msgg').slideDown(400).html(obj);

					if(i=='job_subcategory')
						$('.job_subcategory').find('span.error_msgg').slideDown(400).html(obj);

					if(i=='skills')
						$('.skills').find('span.error_msgg').slideDown(400).html(obj);

					if(i=='description')
						$('.description').find('span.error_msgg').slideDown(400).html(obj);
						
						
					if(i=='rehire_error'){
						$('#rehire_err_msg').html(obj);
						$('.rehireanothermethod').modal('show');
					}	
					
					if(i=='lunch_hour'){
						$('.lunch_hr_err').show();
						$('.lunch_hr_err').slideDown(400).html(obj);
					}	
					
					if(i=='process_id'){					
						$('.errormsge').show();
						$('.errormsge').slideDown(400).html(obj);
					}	
						
				 }) 
			  }
			  close_modals();
			},
			complete: function() { //RemoveLoader();
			}
		  });
	}
	
	function close_modals(){
		$('#automatic_hiring,#manul_hiring,#rehire_fr_job,#manul_hiring_be_advc,#job_postConfirm').modal('hide');
	}
	
	/*
	DESC : to check data from process id
	*/
	function process_details(){
		var process_id = $('#process_id').val();
		var selectedDates = $('#dates').val();
		var formattedDates = $('#formatted_dates').val();
		var format_arr = formattedDates.split(',');
		var date_arr = selectedDates.split(',');
		//send ajax request, to get the data selected
		$.ajax({
			url: path+adminname+'/process-data',
			type: 'post',
			dataType: 'json',
			data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&process_id='+process_id,
			beforeSend:function(){
				Loader();
			},
			success: function(data) {		
				var shift_detail = data.shifts;
				var dates_detail = data.selected_dates;	
				var date_fields = '';
				for (var i = 0; i < date_arr.length; i++) {
					//if date is already selected, then display its time.
					if($.inArray(date_arr[i], dates_detail) != -1){					
						//loop through the shift details array
						Object.keys(shift_detail).forEach(function(key) {	
							if(shift_detail[key].start_date == date_arr[i]){
								date_fields += '<div class="row"><div class="col-md-4"><label class="center_align_label">'+format_arr[i]+'</label></div><div class="col-md-4"><div class="field_forms"><div class="label_form"><label>Start Time</label></div><div class="form_inputs"><input class="setTime_job" type="text" name="start[time_'+shift_detail[key].start_date+']" value="'+shift_detail[key].start_time+'" onkeydown="return false"><span class="error_msgg" style="display:none;"></span><span class="error_msgg" style="display:none;"></span></div></div></div>';
								date_fields += '<div class="col-md-4"><div class="field_forms"><div class="label_form"><label>End Time</label></div><div class="form_inputs"><input class="setTime_job" type="text" name="end[time_'+shift_detail[key].start_date+']" value="'+shift_detail[key].end_time+'" onkeydown="return false"><span class="error_msgg" style="display:none;"></span></div></div></div></div>';
							}							
						});
					}else{
						//display blank fields		
						date_fields += '<div class="row"><div class="col-md-4"><label class="center_align_label">'+format_arr[i]+'</label></div><div class="col-md-4"><div class="field_forms"><div class="label_form"><label>Start Time</label></div><div class="form_inputs"><input class="setTime_job" type="text" name="start[time_'+date_arr[i]+']" value="" onkeydown="return false"><span class="error_msgg" style="display:none;"></span><span class="error_msgg" style="display:none;"></span></div></div></div>';
						date_fields += '<div class="col-md-4"><div class="field_forms"><div class="label_form"><label>End Time</label></div><div class="form_inputs"><input class="setTime_job" type="text" name="end[time_'+date_arr[i]+']" value="" onkeydown="return false"><span class="error_msgg" style="display:none;"></span></div></div></div></div>';
					}
				}
				$('#append_shiftFieldHere').html(date_fields);
				//add timepicker for all the fields.
				$('.setTime_job').each(function(){
					$(this).timepicker({timeFormat: "H:i",'step': '15'});
				});
				//display modal - to set shift timings
				$('#set_shiftTime').modal('show'); 
				RemoveLoader();
			},
			error : function(err){
				RemoveLoader();
				//show error message
				$('#show_err').empty().addClass('alert-danger').html(err.err_message).fadeIn();
			}
		});
	}
	
	/*
	 * DESC: to display users in rehire user modal
	 * */
	function rehire_user_check(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		var employer_id = 0;
		var skills = $('#multiselect').serialize();
		if($('#employer_id :selected').val() != ''){
			employer_id = $('#employer_id :selected').val();
		}
		$.ajax({
			url:path+adminname+'/user-for-rehire',
			type:'post',
			data:skills+'&employer_id='+employer_id+'&_token='+CSRF_TOKEN,
			dataType:'json',
			beforeSend:function(){
				Loader();
			},
			success:function(data){
				$('#rehire_fr_job').empty().html(data.user_list);
				$('#rehire_fr_job').modal('show');
				RemoveLoader();
			},
			error:function(errors){
				RemoveLoader();
			}
		});
	}
	
	/*
	 * DESC : to save shift timings with same start/end time
	 * Added : 29 dec 2017, shivani debut infotech
	 * */
	function save_shifts_with_same_time(){
		var check_process = ''
		if($('#process_id').val() != ''){
			check_process = $('#process_id').val();
		}
		var start_time = $('#start_date').val();
		var end_time = $('#end_date').val();
		new_date = $('#dates').val();
		var lunch_hour = $('#lunch_hour :selected').val();
		if(new_date != ''){
			$.ajax({
				url: path+adminname+'/same-job',
				type: 'post',
				dataType: 'json',
				data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&check_date='+new_date+'&process_id='+check_process+'&start_time='+start_time+'&end_time='+end_time+'&lunch_hour='+lunch_hour,
				beforeSend:function(){
					Loader();
				},
				success: function(data) {
					$('.errormsge').empty().hide();
					if(data.success == true){
						$('#show_selectedDates_here').html(data.shift_html);
						$('#process_id').val(data.process_id);
						$('#set_shiftTime').modal('hide');
					}else{
						//show error message
						//$('#show_err').empty().addClass('alert-danger').html(data.err_message).fadeIn();
						$('.errormsge').show();
						$('.errormsge').slideDown(400).html(data.err_message);
					}
					RemoveLoader();
				},
				error : function(err){
					RemoveLoader();
					//$('#show_err').empty().addClass('alert-danger').html(err.err_message).fadeIn();
					$('.errormsge').show();
					$('.errormsge').slideDown(400).html(err.err_message);
				}
			});
		}
	}

