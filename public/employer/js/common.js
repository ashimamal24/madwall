
//to display message to select employer (if it has not been selected)
$(document).ready(function(){
	if($('#employer_id').length && $('#employer_id :selected').val() == ''){
		$('#employer_id').parent('.form_inputs').find('.error_msgg').empty().html('Please select an employer.').show();
	}
	
	update_skills_strength();
});

//populate subcategory when category is selected
$(document).on('change','.subcategory',function(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var employer_id = 0;
	if($('#employer_id :selected').val() != ''){
		employer_id = $('#employer_id :selected').val();
	}
	$.ajax({
		url:path+adminname+'/subcategory',
		type:'post',
		data:'value='+$('.subcategory').val()+'&employer_id='+employer_id+'&_token='+CSRF_TOKEN,
		dataType:'json',
		beforeSend:function(){
			Loader();
		},
		success:function(data){
			
			RemoveLoader();
			var data1 = '<option value="">Please select Subcategory</option>';
			if(data.sub_cat)
			{
				$.each(data.sub_cat, function(index, element) {
		           data1 +='<option value="'+element._id+'"><b>'+element.name+'</b></option>';
		        });
			}
			$('.commission').show();
			$('#commision').html(data.commision);
			$('#subcategory').html(data1);
			if(adminname == 'employer'){
				jcf.getInstance($('#subcategory')).refresh();
			}else{
				$('#subcategory').trigger("chosen:updated");
			}
		    
		    $('#multiselect').html('');
		    
		    $('#multiselect').trigger("chosen:updated");
		    $('#multiselect').parent('.form_inputs').find('.error_msgg').empty().hide();
		    $('.cat_error').empty().hide();
		    //hide error message if already showing
		    if($('#employer_id').length && $('#employer_id :selected').val() == ''){
				$('#employer_id').parent('.form_inputs').find('.error_msgg').empty().hide();
			}
		},
		error:function(errors){
			RemoveLoader();
		},
		complete:function(){
			RemoveLoader();
		}
	});

});

//auto populate skills when subcategory is selected
$(document).on('change','#subcategory',function(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

	$.ajax({
		url:path+adminname+'/skills',
		type:'post',
		data:'value='+$('#subcategory').val()+'&_token='+CSRF_TOKEN,
		dataType:'json',
		beforeSend:function(){
			Loader();
		},
		success:function(data){
			RemoveLoader();
			var data1 ='';
			if(data.skills)
			{
				$.each(data.skills, function(index, element) {
					data1 +='<option value="'+element._id+'"';
					if(data.mandatory){
						if($.inArray(element._id, data.mandatory) != -1){							
							data1 +='selected';
							$('.skill_error').empty().hide();
						}
					}
		            data1 += '>'+element.name+'</option>';
		        });
			}
			$('#multiselect').html(data1);
			//$('#multiselect option').prop('selected', true);
		    $('#multiselect').trigger("chosen:updated");
		    //update strength indicator
		    update_skills_strength();
		    $('.subcat_error').empty().hide();
		},
		error:function(errors){
			RemoveLoader();
		},
		complete:function(){
			RemoveLoader();
		}
	});

});

//hide error for skills when any of the skill is selected
$(document).on('change','#multiselect',function(){
	if($(this).val() != ''){
		$('.skill_error').empty().hide();
		update_skills_strength();
	}
});

$(document).on('change','#title,#salary_per_hour,#number_of_worker_required,#location',function(){
	if($(this).val() != ''){
		$(this).parent('.form_inputs').find('.errormsges,.error_msgg').empty().hide();
	}
});

$(document).on('click','#automatichiring, #manulhiring, #rehirejob, #edit_job',function(){
	$('.skill_error').hide();
	$('.cat_error').hide();
	$('.subcat_error').hide();
	$('.errormsge').hide();
	if($('#dates').val()=='')
	{
		$('.errormsge').html('Please specify job days for the job.');
		$('.errormsge').show();
	}

	if($('.subcategory').val()=='')
	{
		$('.cat_error').html('Please specify the job category of the job.');
		$('.cat_error').show();
	}

	if($('#subcategory').val()=='')
	{
		$('.subcat_error').html('Please specify the job sub category of the job.');
		$('.subcat_error').show();
	}
	if($('select[name="skills[]"]').val()==null)
	{
		$('.skill_error').html('Please specify skills required for the job.');
		$('.skill_error').show();
	}
$('#message').val($(this).data('confirm-message'));
	$('#job_published_type').val(($(this).attr('data-status')));

	//if($('#job_published_type').val()=='rehire')
		
	
	$('form#addjob').submit();

});


	/*
	 * Added : 27 nov 2017, shivani - Debut infotech
	 * DESC : to export data to csv from various pages i.e.. active jobs, candidate listing, job history, candidates from history details
	 * */
	function export_csv(url,form_id){		
		var filter_data = $('#'+form_id).serialize();
		Loader();
		$.ajax({
			type : 'get',
			url : url,
			data : filter_data,
			dataType : 'json',
			beforesend:function(){
				Loader();
			},
			success : function(data){
				RemoveLoader();				
				if(data.status == true){					
					 window.location.href = path+data.csv_url;
				}else{
					//display error popup
					$('#disable_message_here').empty().html(data.message);
					$('#DisableJobAppAction').modal('show');
				}
			},
			error : function(data,ajaxOptions,thrownError){
				RemoveLoader();
				var errors = jQuery.parseJSON(data.responseText);
				if(errors.success==false){
					new PNotify({
						type: 'error',
						title: 'Error',
						text: 'Something went wrong!!!'
					});
				}
				
			}
		});
	}
	
	/*
	 * Added : 2 jan 2018
	 * DESC : to update strength indicatory when skills are selected/deselected
	 * */
	function update_skills_strength(){
		var total_skills = $('#multiselect option').length;
		var selected_skills = $('#multiselect option:selected').length;
		//check %age
		var strength = (selected_skills/total_skills)*100;
		
		if(strength == 0){
			$('.strength_indicatorDiv').hide();
			$('.strength_indicator').empty().hide();
		}else if(strength > 0 && strength < 50){
			$('.strength_indicatorDiv').show();
			//too broad
			$('.strength_indicator').empty().html('<span style="color:#549C35;"><b>Broad</b></span>').show();
		}else if(strength >= 50 && strength < 75){
			$('.strength_indicatorDiv').show();
			//just right
			$('.strength_indicator').empty().html('<span style="color:#FF7919;"><b>Just Right</b></span>').show();
		}else if(strength >=75){
			$('.strength_indicatorDiv').show();
			//too specific
			$('.strength_indicator').empty().html('<span style="color:#C23F44;"><b>Specific</b></span>').show();
		}else{
			$('.strength_indicatorDiv').hide();
			//hide indicator
			$('.strength_indicator').empty().hide();
		}
	}


