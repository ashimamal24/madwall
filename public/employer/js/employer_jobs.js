$(document).ready(function(){
	/*
	DESC : to reset search text box
	*/
	$('#reset_search').click(function(){
		$('#job_name').val('');
		listjobApply($('#filter_form').attr('action'));
	});
		
	/*
	DESC : to export candidate listing to csv
	*/
	$('#export_candidate_list').click(function(){
		export_csv(path+adminname+'/export-employees','filter_form');
	});
	
	
	//when a keyword is entered into search field
	$("#job_name").keyup(function(){
		//reload listing
		listjobApply($('#filter_form').attr('action'));
	});
	
	//when search button is clicked
	$(document).on('click','.search',function(){
		listjobApply($('#filter_form').attr('action'));
	});
	
	//when any record is selected from jobseeker suggestions
	$(document).on('change','.selectrecords',function(){
		var url = $('#filter_form').attr('action');
		listjobApply(url);
	});
	
	//when pagination links are clicked
	$(document).on('click', '.pagination a', function (e) 
	{
		e.preventDefault();		
		var url=$(this).attr('href');
		listjobApply(url);
	});
		
	/*
	DESC : when check docs button is clicked - to display jobseeker certification/degree docs.
	*/
	$(document).on('click','.check_jobseeker_docs',function(e){
		//serialize cancel employee form
		var user_id = $(this).data('id');
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			url:path+adminname+'/check-docs',
			type:'get',
			data:'user_id='+user_id+'&_token='+CSRF_TOKEN,
			dataType:'html',
			beforeSend:function(){
				Loader();
			},
			success:function(data){
				$('#display_docs_here').empty().html(data);
				$('#jobseekerDocsPopup').modal('show');			
			},
			error:function(errors){
				RemoveLoader();
				
			},
			complete:function(){
				RemoveLoader();
			}
		});
	});
	
	/*
	DESC : to display shift details of an employee
	*/
	$(document).on('click','.check_shift_details',function(){

		$('#show_shift_err').empty().removeClass('alert-danger').hide();
		var job_id = $(this).data('job');
		var jobseeker_id = $(this).data('jobseeker');
		$('#displayShiftHours').empty();
		Loader();
		$.ajax({
			type : 'get',
			url :path+adminname+'/employee-shifts',
			data : 'job_id='+job_id+'&employee='+jobseeker_id,
			dataType : 'json',
			beforesend:function(){
				
			},
			success : function(data){
				RemoveLoader();
				total_val ="N/A";
				$('#displayShiftHours').empty().html(data.shift_html);
/////////////////////

///
				var table = $('#tbl').dataTable( {
				  searching: false, bFilter: false,bInfo:false,"ordering": false,
				   "searching": false,"bAutoWidth": false ,"bLengthChange": false,"autoWidth": false,"pagingType": "simple","pageLength": 7,drawCallback: function(){
          $('.paginate_button.next', this.api().table().container())          
             .on('click', function(){
                $('.work_hours').each(function(){
						$(this).timepicker({timeFormat: "H:i"});
					});
               	total_week_val = 0;
				$.each($('.total_hour_column'),function(k,v){
					total_week_val += Number.parseFloat($(v).attr('data-total'));
					//console.log(v);
				});
				$('#total_week_hour').text(total_week_val);
             });
             $('.paginate_button.previous', this.api().table().container())          
             .on('click', function(){
         
               	total_week_val = 0;
				$.each($('.total_hour_column'),function(k,v){
					total_week_val += Number.parseFloat($(v).attr('data-total'));
					//console.log(v);
				});
				$('#total_week_hour').text(total_week_val);
             });       
       }
				} );


//////////////////////
				

  
    //var data = ["Friday - July 06, 2018","System Architect","Edinburgh","5421","2011/04/25","$320,800"];
   	if (typeof $('input[name="current_date"]').val() != "undefined") {
		table = $('#tbl').DataTable();
    	table.page.jumpToData($('input[name="current_date"]').val(), 0);
	}
  	
 ///////////////
				$(".dataTables_paginate").parent().siblings().remove();
				$(".dataTables_paginate").parent().removeClass('col-sm-7').addClass('col-md-12');
				//var total_val;
				//alert($('.total_hours').val());
				var total_week_val = 0;
				$.each($('.total_hour_column'),function(k,v){
					total_week_val += Number.parseFloat($(v).attr('data-total'));
					//console.log(v);
				});
				if($('.total_hours').val() == null){
					total_val ="N/A";
				}else{
					total_val = $('.total_hours').val();
				}
				if($('.total_hours').val() === "undefined"){
					$('.total_hours').val()=0;
				}
				$(".dataTables_paginate").parent().parent().prepend('<div class="col-md-12"><table class="table table-bordered"><tr><td colspan="5" align="right"><strong>Total Weekly Hours: </strong><span id="total_week_hour">'+total_week_val+'</td></tr><tr><td colspan="5" align="right"><strong>Total Hours: </strong><span id="total_week">'+total_val+'</td></tr></table></div>');
				if(data.success == true && data.err_status == false){
					//add timepicker for all the fields.
					$('.work_hours').each(function(){
						$(this).timepicker({timeFormat: "H:i"});
					});
					
					$('.displayshftdetail').show();
					$('.ok_close').hide();
				}else{
					$('.displayshftdetail').hide();
					$('.ok_close').show();
				}
				$('#employee_shiftDetail').modal('show');
			},
			error : function(data,ajaxOptions, thrownError){
				RemoveLoader();
				var errors = jQuery.parseJSON(data.responseText);
				if(errors.success==false){
					$('#show_shift_err').empty().addClass('alert-danger').html("Something went wrong!!!").fadeIn();			
				}
			}
		});
	});
	
	/*
	 * DESC : when start/end time is updated, display hours change in real time too
	 * */
	$(document).on('change','.work_hours',function(){
		
		var id = $(this).data('id');
		
		var start = $('.start_'+id).val();
		var end = $('.end_'+id).val();
		
		//lunch hour value
		var lunch_hour = $('#lunchHour_'+id).val();
		
		$('.hidden_div').append('<input size="4" class="form-control work_hours start_'+id+' ui-timepicker-input" name="shiftstart['+id+']" data-id="'+id+'" value="'+start+'" type="text" autocomplete="off"><input size="4" class="form-control work_hours end_'+id+' ui-timepicker-input" name="shiftend['+id+']" data-id="'+id+'" value="'+end+'" type="text" autocomplete="off">')
		//create date format          
		var timeStart = new Date("01/01/2007 " + start).getHours();
		var timeEnd = new Date("01/01/2007 " + end).getHours();

		var hourDiff = timeEnd - timeStart;//difference in hours
		var total_hours_worked = hourDiff - lunch_hour; //subtract lunch hours from total hours
		if(total_hours_worked < 0 && start > end){
			fromDate = parseInt(new Date("01/01/2007 " + start).getTime()/1000); 
			toDate = parseInt(new Date("01/02/2007 " + end).getTime()/1000);
			var timeDiff = (toDate - fromDate)/3600;  // will give difference in hrs
			var total_hours_worked = timeDiff - lunch_hour; //subtract lunch hours from total hours
		}
		
		
		if(total_hours_worked < 0 ){
			$('#show_shift_err').empty().addClass('alert-danger').html("Total hours for a shift can not be negative").fadeIn();	
			return false;
		}else{
			$('#show_shift_err').empty().removeClass('alert-danger').fadeOut();	
		}
		
		
		//update total hours worked(display only)
		$('.total_'+id).html(total_hours_worked);
		
	});
	
	$(document).on('change','.work_hours_up,.lunch_hour',function(){
		
		var d = $(this).data('date');
		var start =$(this).parent().parent().children('.start_shift').children('.work_hours_up').val();
		var end =$(this).parent().parent().children('.end_shift').children('.work_hours_up').val();
		var lunch =$(this).parent().parent().children('.lunch_time').children('.lunch_hour').val();
		console.log(start+end+lunch);
		$('.hidden_div').find("[data-date='" + d + "']").remove();
		$('.hidden_div').append('<input size="4" class="form-control work_hours_up" value="'+start+'" name="start_shift['+d+']" data-date="'+d+'" /><input size="4" class="form-control work_hours_up" value="'+end+'" name="end_shift['+d+']" data-date="'+d+'" /><input data-date="'+d+'" value="'+lunch+'" class="form-control lunch_hour" name="lunch['+d+']" />');
	});
	
	/*
	 * DESC : to update employee work hours for a particular shift
	 * */
	$(document).on('click','#updateHours',function(){
		$('#show_shift_err').empty().removeClass('alert-danger').hide();
		//check if any of the hours field is empty or have 0 value
		var updateData = true;
		var NumericData = true;
		/*$('.work_hours').each(function(){
			if($(this).val() == '' || $(this).val() == 0){
				updateData = false;
			}
			
			var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
			var str = $(this).val();
			if(numberRegex.test(str)) {
			   	   
			} else{
				NumericData = false;
			}
			
		});
		
		if(updateData == false){
			$('#show_shift_err').empty().addClass('alert-danger').html("Hours value can not be empty or 0").fadeIn();
			return false;
		}
		if(NumericData == false){
			$('#show_shift_err').empty().addClass('alert-danger').html("Hours value can have numeric values only").fadeIn();
			return false;
		}*/
		
		
		Loader();
		//return false;
		//~ if(hours_data != ''){
			$.ajax({
				type : 'get',
				url : path+adminname+'/update-hours',
				data : $('.work_hours').serialize()+'&'+$('.work_hours_up').serialize()+'&'+$('.lunch_hour').serialize()+'&job_id='+$('input[name="job_id"]').val()+'&app_id='+$('input[name="app_id"]').val()+'&jobseeker_id='+$('input[name="jobseeker_id"]').val(),
				dataType : 'json',
				beforesend:function(){
					
				},
				success : function(data){
					if(data.success == true){
						window.location.reload();
					}else{
						$('#show_shift_err').addClass('alert-danger').empty().html(data.message).fadeIn();
					}
					RemoveLoader();
						
				},
				error : function(data,ajaxOptions, thrownError){
					RemoveLoader();

					var errors = jQuery.parseJSON(data.responseText);
					if(errors.success==false){
						new PNotify({
							type: 'error',
							title: Error,
							text: 'Something went wrong!!!'
						});
					}
					
				}
			});
		//}
	});
	
	/*
	DESC : to display lunch start/end time fields
	*/
	$(document).on('change','#unpaid_lunch',function(){
		if($(this).is(":checked")){
			//show lunch hours div
			$('#lunch_hours_div').fadeIn('slow');
		}else{
			$('#lunch_hours_div').fadeOut('slow');
			$('.lunch_hr_err').empty().hide();
			$('#lunch_hour').val('');
			jcf.getInstance($('#lunch_hour')).refresh();
		}
	});
});

//to display jobseeker list and to filter jobseeker data
function listjobApply(url)
{
	var record =  '';
	if($('.selectrecords').val()!='' && $('.selectrecords').val() != undefined)
	{
		record = $('.selectrecords').val();
	}
	var filter_data = $('#filter_form').serialize();
	//Loader();
	$.ajax({
		type : 'post',
		url : url,
		data : filter_data,
		dataType : 'html',
		beforesend:function(){
			//Loader();
		},
		success : function(data){
			//RemoveLoader();
			$('.jobdata').empty().html(data);
			$('.msg').html($('#message').val());
		},
		error : function(data,ajaxOptions, thrownError){
			//RemoveLoader();
			//console.log(data.responseText);
			var errors = jQuery.parseJSON(data.responseText);
			if(errors.success==false){
				new PNotify({
					type: 'error',
					title: Error,
					text: 'Something went wrong!!!'
				});
			}
			
		}
	});
}

/*
DESC : to return a formatted date i.e.. to be populated in hidden date field.
*/
function myDateFormatter (dateObject) {
	var d = new Date(dateObject);
	var day = d.getDate();
	var month = d.getMonth()+1;
	var year = d.getFullYear();
	if (day < 10) {
		day = "0" + day;
	}
	if (month < 10) {
		month = "0" + month;
	}
	var date = day + "-" + month + "-" + year;
	return date;
}

function format_date_month_day (dateObject) {
			var d = new Date(dateObject);
			//var d = new Date();
			var weekday = new Array(7);
			weekday[0] = "Sunday";
			weekday[1] = "Monday";
			weekday[2] = "Tuesday";
			weekday[3] = "Wednesday";
			weekday[4] = "Thursday";
			weekday[5] = "Friday";
			weekday[6] = "Saturday";
			
			var month = new Array();
			month[0] = "Jan";
			month[1] = "Feb";
			month[2] = "Mar";
			month[3] = "Apr";
			month[4] = "May";
			month[5] = "Jun";
			month[6] = "Jul";
			month[7] = "Aug";
			month[8] = "Sep";
			month[9] = "Oct";
			month[10] = "Nov";
			month[11] = "Dec";
			
			var day = d.getDate();
			var month = month[d.getMonth()];
			var day_name = weekday[d.getDay()];
			if (day < 10) {
				day = "0" + day;
			}
			
			var date = day + "-" + month +" "+ day_name;
			return date;
		}
var i=0;
$(document).on('click','.edit-job',function(){
	i++;
	var d = $(this).parent().siblings('.date_time').attr('date');
	$(this).parent().siblings('.start_shift').html('<input size="4" class="form-control work_hours_up" value="00:00" name="start_shift['+d+']" data-date="'+d+'" />');
	$(this).parent().siblings('.end_shift').html('<input size="4" class="form-control work_hours_up" value="00:00" name="end_shift['+d+']" data-date="'+d+'" />');
	$(this).parent().siblings('.lunch_time').html('<select data-date="'+d+'" class="form-control lunch_hour" name="lunch['+d+']"><option value="0">0 hour</option><option value="0.25">15 minutes</option><option value="0.50">30 minutes</option><option value="1">1 hour</option><option value="1.5">1 and half hour</option><option value="2">2 hours</option></select>');
	$(this).parent().empty().html("<a class='cancel_edit' href = '#' data-date='"+d+"'>Cancel</a>");
	$('.work_hours_up').each(function(){
		$(this).timepicker({timeFormat: "H:i"});
	});
	
});

$(document).on('click','.cancel_edit',function(){
	$(this).parent().siblings('.start_shift').html('N/A');
	$(this).parent().siblings('.end_shift').html('N/A');
	$(this).parent().siblings('.lunch_time').html('N/A');
	var d =$(this).data('date');
	$('.hidden_div').find("[data-date='" + d + "']").remove();
	$(this).parent().empty().html('<a class="edit-job" href="#">Add Shift</a>');
});

$(document).on('click','.delete-shift',function(){
	
});