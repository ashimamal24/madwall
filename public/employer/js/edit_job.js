$('#description').richText();
$('#safety_hazards').richText();
$('#physical_requirement').richText();
$(document).ready(function(){
	/*
	DESC : to display set shift time modal
	*/
	$(document).on('click','#set_jobTime',function(e){
		e.preventDefault();
		$('#show_err').empty().removeClass('alert-danger').hide();
		//check selected dates
		var selectedDates = $('#dates').val();
		var date_arr = selectedDates.split(',');
		//if no date is selected, show error message
		if(selectedDates == ""){
			$('.errormsge').show();
			$('.errormsge').html('Please specify job days for the job.');
			return false;
		}else{
			if($('#process_id').val() != ''){
				process_details();
			}else{	
				generate_date_fields(shift_detail,dates_detail);					
			}
		}
	});
	
	/*
	 * DESC : to check shift duration when lunch time is updated
	 * */
	$(document).on('change','#lunch_hour',function(){
		if($('#lunch_hour :selected').val() != ''){
			//check duration
			save_shifts_with_same_time();
		}
	});
	
	
	
	/**
	DESC : when OK button is clicked from set shift popup
	*/
	$(document).on('click','#time_selected',function(){
		//send an ajax request to server
		$('#show_err').empty().hide();
		var data_check = true;
		//check if time is selected for all the selected dates
		$('.setTime_job').each(function(){
			if($(this).val() == ''){
				data_check = false;
			}
		});
			
		if(data_check == false){
			$('#show_err').empty().addClass('alert-danger').html("Please select start and end time for all the selected dates").fadeIn();
			return false;
		}
		var job_id = $('input[name="jobValue"]').val();
		
		var lunch_hour = 0;
		if($('#unpaid_lunch').is(':checked')){
			lunch_hour = $('#lunch_hour').val();
		}
		
		
		$.ajax({
			url: path+adminname+'/save-shifts',
			type: 'post',
			dataType: 'json',
			data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&job_id='+job_id+'&'+$('.setTime_job').serialize()+'&lunch_hour='+lunch_hour,
			beforeSend:function(){
				Loader();
			},
			success: function(data) {
				if(data.success == true){
					$('#show_selectedDates_here').html(data.shift_html);
					$('#process_id').val(data.process_id);
					$('#dates_updates').val('yes');
					$('#set_shiftTime').modal('hide');
				}else{
					//show error message
					$('#show_err').empty().addClass('alert-danger').html(data.err_message).fadeIn();
				}
				RemoveLoader();
			},
			error : function(err){
				RemoveLoader();
				//show error message
				$('#show_err').empty().addClass('alert-danger').html(err.err_message).fadeIn();
			}
		});
		
	});
	
	$('#dates').val('');
	$('#job_published_type').val('');
	var c_date = new Date(); 
	
	date = new Date(c_date);
	
	var currentMonth = date.getMonth();
	var currentDate = date.getDate();
	var currentYear = date.getFullYear();
	$('#datepicker').datepicker({
		startDate: date,
		multidate:true,
		endDate: new Date(currentYear, currentMonth, currentDate+90),
		maxViewMode: 0
	}).on('changeDate', function(e){
		date_val = Array();
		format_val = Array();
		
		for(var i = 0;i<e.dates.length;i++)
		{
			e.dates.sort(function(a,b)
			{
				a = new Date(a);				
				b = new Date(b);
				return a-b;
			});
			date_val.push(myDateFormatter(e.dates[i]));
			format_val.push(format_date_month_day(e.dates[i]));
		}
		if(date_val.length<=30){
			$('span.errormsge').hide();
		}
		$('#dates').val(date_val);
		$('#formatted_dates').val(format_val);
		
		
		save_shifts_with_same_time();
		
		
		if(date_val.length>30){
			$('.errormsge').show();
			$('.errormsge').html('Maximum job length can be 30 days only.'); 
		}

			
	});
	
	var d = rateArray.split(',');		 
	var manualDates = [];
	for(i = 0; i<d.length; i++)
	{
		//alert(moment.utc(d[i]).local().format('YYYY-MM-DD hh:mm A'));
		manualDates.push(new Date(d[i]));
	}
	$('#datepicker').datepicker('setDate',manualDates);
	
	$("#location").geocomplete({
          details: ".pickLocation",
          types: ["geocode", "establishment"],
	}).bind("geocode:result", function(event, result){
	    var coor = result.geometry.location.lat();
	    $('#key').val(1);
	    $('.pickLocation').find('span.error_msgg').hide();  
	}).bind("blur", function(event, results){
	    setTimeout(function(){
			if($('#key').val() == '')
			{
				$('.pickLocation').find('span.error_msgg').slideDown(400).html('Please specify a valid location for the job.');
				$('#lat').val('');
				$('#lng').val('');
			}
			$('#key').val('');
	    },1000);	    
	});
	
	$(document).on('click','#deletejobs',function(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			url:path+adminname+'/deletejob',
			type:'post',
			data:'value='+$('.jobdelete').data('job')+'&_token='+CSRF_TOKEN,
			dataType:'json',
			beforeSend:function(){
				Loader();
			},
			success:function(data){
				if(data.status)
					window.location.href=data.url;
			},
			error:function(errors){
				console.log(errors);
			},
			complete:function(){
				RemoveLoader();
			}
		});
	});
	
	jQuery("form#addjob").validate({
	    rules: {
	      "title":{
	         required:true 
	      },
	      "description":{
	        required:true 
	      },
	      "start_date":{
	      	required:true
	      },
	      "end_date":{
	      	required:true
	      },
	      'salary_per_hour':{
	      	required:true
	      },
	      "number_of_worker_required":{
	      	required:true
	      },
	      "address":{
	      	required:true
	      }
	    },
	    messages: {
	        "title":{
	          required:"Please specify Job Name."
	        },
	        "description":{
	          required:"Please specify Job Description."
	        },
	        "start_date":{
	          required:"Please specify start time of the job."
	        },
	        "end_date":{
	          required:"Please specify end time of the job."
	        },
	        "salary_per_hour":{
	          required:"Please specify Salary per hour of the job in dollars."
	        },
	        "number_of_worker_required":{
	          required:"Please specify number of persons required for the job.."
	        },
	        "address":{
	          required:"Please specify job location for the job."
	        }
	    },
	    errorElement:'span',
	    errorClass:'error_msg errormsges',
	    submitHandler: function(form) {
	    	$('.editjobpopup').modal('show');
	    /*bootbox.confirm($('#message').val(), function(result){ 
	    	if(result)
		    	{
		    		var d1 = $('#dates').val();
		    		var cur_date = d1.split(',')
		    		var CurrentDate = moment().format('DD-MM-YYYY');
		    		var f_date = moment(cur_date[0],'DD-MM-YYYY').format('YYYY-MM-DD');
					var douration = moment.duration(moment(f_date+' '+$('input[name="start_date"]').val(),'YYYY-MM-DD HH:mm').diff(moment()));
	    			var formatdata = $('form#addjob').serialize();
			    	var value = ''; 
			    	$('#rehirevalue').val('');
			    	if(CurrentDate==cur_date[0])
					{
						$('#rehire_fr_job').modal('hide');
						if(douration.hours()>=0 &&douration.hours()<=3){
						   $('#dur_days').val(douration.days());
					       $('#dur_hours').val(douration.hours());
					       $('#dur_cur_date').val(CurrentDate);
					       $('#dur_current_date').val(cur_date[0]);
					       $('.gurrntte').modal('show');
						}
						else
							postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
					}
					else
						postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
	    		}
		     });*/	
	    }
	});
	
	$(document).on('click','#editJobs',function(){
		$('.editjobpopup').modal('hide');
		var d1 = $('#dates').val();
		var cur_date = d1.split(',');
		var CurrentDate = moment().format('DD-MM-YYYY');
		var f_date = moment(cur_date[0],'DD-MM-YYYY').format('YYYY-MM-DD');
		var douration = moment.duration(moment(f_date+' '+$('input[name="start_date"]').val(),'YYYY-MM-DD HH:mm').diff(moment()));
		var formatdata = $('form#addjob').serialize();
		var value = ''; 
		$('#rehirevalue').val('');
		
		postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
		
		//~ if(CurrentDate==cur_date[0])
		//~ {
			//~ $('#rehire_fr_job').modal('hide');
			//~ if(douration.hours()>=0 &&douration.hours()<=3){
			   //~ $('#dur_days').val(douration.days());
			   //~ $('#dur_hours').val(douration.hours());
			   //~ $('#dur_cur_date').val(CurrentDate);
			   //~ $('#dur_current_date').val(cur_date[0]);
			   //~ $('.gurrntte').modal('show');
			//~ }
			//~ else
				//~ postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
		//~ }
		//~ else
			//~ postRequest(douration.days(),douration.hours(),CurrentDate,cur_date[0]);
	});
	
	$(document).on('click','#confirm_dur_message',function(){
		// alert($('#dur_days').val());
		postJob();
		//postRequest($('#dur_days').val(),$('#dur_hours').val(),$('#dur_cur_date').val(),$('#dur_current_date').val());
	});
	
	
	
	
	$('#start_date').timepicker({timeFormat: "H:i",'step': '15'});
	$('#end_date').timepicker({timeFormat: "H:i",'step': '15'});
	
	/*
	DESC : to reset end time when start time is updated
	*/
	$(document).on('change','#start_date',function(){
		var pair = [ ['00:00',$('#start_date').val()]];
		$('#end_date').val(($('#start_date').val()));
	});
	
	/*
	 * DESC : to update start/end time for all the selected dates when end time is selected
	 * */
	$(document).on('change','#end_date',function(){
		var end_time = $(this).val();
		var start_time = $('#start_date').val();
		if(start_time != '' && end_time != '' && start_time != end_time){
			//call same_job_time to save temporary data for this job post
			save_shifts_with_same_time();
		}
		
	});
	
	
	

});

/*
DESC : to check data from process id
*/
function process_details(){
	var process_id = $('#process_id').val();
	var job_id = $('input[name="jobValue"]').val();
	//send ajax request, to get the data selected
	$.ajax({
		url: path+adminname+'/process-data',
		type: 'post',
		dataType: 'json',
		data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&process_id='+process_id+'&job_id='+job_id,
		beforeSend:function(){
			Loader();
		},
		success: function(data) {		
			var shift_detailData = data.shifts;
			var dates_detailData = data.selected_dates;	
			var format_detailData = data.format_dates;	
			
			//generate date fields on behalf of the data passed.		
			generate_date_fields(shift_detailData,dates_detailData);
			RemoveLoader();
		},
		error : function(err){
			RemoveLoader();
			//show error message
			$('#show_err').empty().addClass('alert-danger').html(err.err_message).fadeIn();
		}
	});
}

/*
DESC : to generate date time fields for the data variable passed.
*/
function generate_date_fields(shift_details,dates_details){
	var selectedDates = $('#dates').val();
	var formattedDates = $('#formatted_dates').val();
	var date_arr = selectedDates.split(',');
	var format_arr = formattedDates.split(',');
	var date_fields = '';
	for (var i = 0; i < date_arr.length; i++) {
		//if date is already selected, then display its time.
		if($.inArray(date_arr[i], dates_details) != -1){		
			//loop through the shift details array
			Object.keys(shift_details).forEach(function(key) {	
				if(shift_details[key].start_date == date_arr[i]){					
					date_fields += '<div class="row"><div class="col-md-4"><label class="center_align_label">'+format_arr[i]+'</label></div><div class="col-md-4"><div class="field_forms"><div class="label_form"><label>Start Time</label></div><div class="form_inputs"><input class="setTime_job form-control" type="text" name="start[time_'+shift_details[key].start_date+']" value="'+shift_details[key].start_time+'" onkeydown="return false"><span class="error_msgg" style="display:none;"></span><span class="error_msgg" style="display:none;"></span></div></div></div>';
					date_fields += '<div class="col-md-4"><div class="field_forms"><div class="label_form"><label>End Time</label></div><div class="form_inputs"><input class="setTime_job form-control" type="text" name="end[time_'+shift_details[key].start_date+']" value="'+shift_details[key].end_time+'" onkeydown="return false"><span class="error_msgg" style="display:none;"></span></div></div></div></div>';
				}							
			});
		}else{
			//display blank fields		
			date_fields += '<div class="row"><div class="col-md-4"><label class="center_align_label">'+format_arr[i]+'</label></div><div class="col-md-4"><div class="field_forms"><div class="label_form"><label>Start Time</label></div><div class="form_inputs"><input class="setTime_job form-control" type="text" name="start[time_'+date_arr[i]+']" value="" onkeydown="return false"><span class="error_msgg" style="display:none;"></span><span class="error_msgg" style="display:none;"></span></div></div></div>';
			date_fields += '<div class="col-md-4"><div class="field_forms"><div class="label_form"><label>End Time</label></div><div class="form_inputs"><input class="setTime_job form-control" type="text" name="end[time_'+date_arr[i]+']" value="" onkeydown="return false"><span class="error_msgg" style="display:none;"></span></div></div></div></div>';
		}
	}
	$('#append_shiftFieldHere').html(date_fields);
	//add timepicker for all the fields.
	$('.setTime_job').each(function(){
		$(this).timepicker({timeFormat: "H:i",'step': '15'});
	});
	//display modal - to set shift timings
	$('#set_shiftTime').modal('show'); 
}


function close_modals(){
	$('#automatic_hiring,#manul_hiring,#rehire_fr_job,#manul_hiring_be_advc').modal('hide');
}


function postJob(){
	
	//hide all modals
	close_modals();
	var formatdata = $('form#addjob').serialize();
	if($('#job_published_type').val()==2)
		formatdata += $('input[name="offer"]:checked').serialize();
	$.ajax({
        url: $('form#addjob').attr('action'),
        type: 'post',
        dataType: 'json',
        data: formatdata,
        beforeSend:function(){
            Loader();
        },
        success: function(data) {
          console.log('sudccess');
          if(data.status==1)
          {
          	$('#rehireModal').modal('hide');
            window.location.href=data.url;

          }
          if(data.status==0)
          {
          	RemoveLoader();
            window.location.reload();
          }
        },
        error: function(error) { 
          $('span.error_msg').hide();
          $('span.error_msgg').hide();
          $('.cat_error').hide();
          $('.subcat_error').hide();
          $('.skill_error').hide();
          RemoveLoader() ;
          var result_errors = error.responseJSON;
          
          if(result_errors)
          {

             $.each(result_errors, function(i,obj)
             {
                $('input[name='+i+']').parent('.form_inputs').find('span.error_msgg').slideDown(400).html(obj);
                if(i=='dates')
                {
                	$('.formfieldset').find('span.error_msgg').slideDown(400).html(obj);
                }
                if(i=='job_category')
                {
                	$('.job_category').find('span.error_msgg').slideDown(400).html(obj);
                }
                if(i=='job_subcategory')
                {
                	$('.job_subcategory').find('span.error_msgg').slideDown(400).html(obj);
                }
                if(i=='skills')
                {
                	$('.skills').find('span.error_msgg').slideDown(400).html(obj);
                }
                if(i=='description'){
                	$('.description').find('span.error_msgg').slideDown(400).html(obj);
                }
                
                if(i=='rehire_error'){
					$('#rehire_err_msg').html(obj);
					$('.rehireanothermethod').modal('show');
				}
					
                if(i=='dates'){					
					$('.errormsge').show();
					$('.errormsge').html(obj);
				}	
				
				if(i=='lunch_hour'){
					$('.lunch_hr_err').show();
					$('.lunch_hr_err').slideDown(400).html(obj);
				}	
				if(i=='update_job'){
					window.location.reload();
				}	
             }) 
          }

        },
        complete: function() { //RemoveLoader() ;
        }
      });
}

	function postRequest(day,hours,curdate,cur_date)
	{
		postJob();	
	}

	/*
	 * DESC : to save shift timings with same start/end time
	 * Added : 29 dec 2017, shivani debut infotech
	 * */
	function save_shifts_with_same_time(){
		var check_process = ''
		if($('#process_id').val() != ''){
			check_process = $('#process_id').val();
		}
		var start_time = $('#start_date').val();
		var end_time = $('#end_date').val();
		new_date = $('#dates').val();
		var job_id = $('input[name="jobValue"]').val();
		var lunch_hour = $('#lunch_hour :selected').val();
		//if(new_date != ''){
			$.ajax({
				url: path+adminname+'/edit-same-job',
				type: 'post',
				dataType: 'json',
				data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&check_date='+new_date+'&process_id='+check_process+'&start_time='+start_time+'&end_time='+end_time+'&job_id='+job_id+'&lunch_hour='+lunch_hour,
				beforeSend:function(){
					Loader();
				},
				success: function(data) {
					$('.errormsge').empty().hide();
					if(data.success == true){
						if(data.dates == false){
							$('#process_id').val('');
							$('#show_selectedDates_here').html('');
						}else{
							$('#show_selectedDates_here').html(data.shift_html);
							$('#process_id').val(data.process_id);
							$('#set_shiftTime').modal('hide');
						}
							
					}else{
						//show error message
						//$('#show_err').empty().addClass('alert-danger').html(data.err_message).fadeIn();
						if(data.process == 0){
							$('#process_id').val('');
							$('#show_selectedDates_here').html('');
						}
						$('.errormsge').show();
						$('.errormsge').slideDown(400).html(data.err_message);
					}
					RemoveLoader();
				},
				error : function(err){
					RemoveLoader();
					//$('#show_err').empty().addClass('alert-danger').html(err.err_message).fadeIn();
					$('.errormsge').show();
					$('.errormsge').slideDown(400).html(err.err_message);
				}
			});
		
	}

	
