$(document).ready(function(){
	
	$('.display_intrvwtime_slot').hide();
	
	
	//display interview timeslots when checkbox is checked.
	$(document).on('change','#assign_time_slot',function(){
		if($(this).is(':checked')){
			$('.display_intrvwtime_slot').show();
		}else{
			$('.display_intrvwtime_slot').hide();
		}
	});
	
	/*
	 * DESC : to assign interview timeslots to an employee
	 * */
	$(document).on('click','#hire_employee',function(){
		if($('#assign_time_slot').is(':checked')){ //if timeslots has been assigned, then
			//check if any of the timeslot is empty
			var empty_time = false;
			$('.intervw_time').each(function(){
				if($(this).val() == ''){
					empty_time = true;
				}
			});
			
			if(empty_time == true){
				//display error - please select timeslots
				$('#show_intrvw_err').empty().addClass('alert-danger').html("Please select all the timeslots").fadeIn();			
			}else{
				//send ajax request to assign timeslot to an employee for a job.
				var start_one = $('#intrvw_one_start').val();
				var end_one = $('#intrvw_one_end').val();
				var start_two = $('#intrvw_two_start').val();
				var end_two = $('#intrvw_two_end').val();
				var start_three = $('#intrvw_three_start').val();
				var end_three = $('#intrvw_three_end').val();
				var application_id = $('#application_itrvw_id').val();
				$.ajax({
					url: path+adminname+"/interview-assign",
					type: 'post',
					dataType: 'json',
					data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&start_one='+start_one+"&end_one="+end_one+"&start_two="+start_two+"&end_two="+end_two+"&start_three="+start_three+"&end_three="+end_three+"&application_id="+application_id,
					beforeSend:function(){
						Loader();
					},
					success: function(data) {
						if(data.status == false){
							$('#show_intrvw_err').empty().addClass('alert-danger').html(data.message).fadeIn();
						}else{
							window.location.reload();
						}
						RemoveLoader() ;
					},
					error: function(error) { 
						RemoveLoader() ;
						$('#show_intrvw_err').empty().addClass('alert-danger').html(error.message).fadeIn();
					},
					complete: function() { //RemoveLoader() 
					}
			  });
			}
			
		}else{ //else if no interview timeslots found, then directly hire the employee.
			//accept employee directly.
		}
	});
	
	
	
	//display rating modal for admin
	$(document).on('click','.update_applicant_rating',function(){
		//applicant id 
		var applicant_id = $(this).data('applicant');
		//rating
		var rating = $(this).data('value');
		
		$('#applicant_rating').val(rating);
		$('#applicant_id').val(applicant_id);
		$('#updateApplicantRating').modal('show');
		
	});
	
	
	//update rating from admin
	$(document).on('click','#updateRatingjob',function(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		//applicant id 
		var applicant_id = $('#applicant_id').val();
		//rating
		var rating = $('#applicant_rating').val();
		if(rating == ''){
			$('#rating_err').addClass('alert-danger').empty().html('Rating can not be empty').show();
			return false;
		}
		$.ajax({
			url:path+adminname+'/edit-rating',
			type:'post',
			data:'rating='+rating+'&jobappid='+applicant_id+'&_token='+CSRF_TOKEN,
			dataType:'json',
			beforeSend:function(){
				Loader();
			},
			success:function(data){
				window.location.reload(true);
			},
			error:function(errors){
				console.log(errors);
			},
			complete:function(){
				RemoveLoader()();
			}
		});
	});
	
	
	
	//when disabled accept/decline buttons are clicked
	$(document).on('click','.declinedDisable,.acceptDisable,.accept_max_hired,.declined_max_hired',function(){
		var message = $(this).data('confirm-message');
		$('#disable_message_here').html(message);
		$('#DisableJobAppAction').modal('show');
	});
	
	/*
	DESC : when cancel employee button is clicked.
	*/
	$(document).on('click','.cancel_employee',function(e){		
		var message = $(this).data('confirm-message');
		var cancel_emp_id = $(this).data('apply_value');
		
		$('#cancel_employee_message_here').html(message);
		$('#cancel_employee_id').val(cancel_emp_id);
		$('#cancelEmployeeModal').modal('show');
	});
	
	/*
	DESC : when yes button is clicked to delete employee
	*/
	$(document).on('click','#cancelEmployeeYes',function(e){
		$('#cancelEmployeeModal').modal('hide');
		//serialize cancel employee form
		var cancel_data = $('#cancelEmployeeForm').serialize();
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			url:path+adminname+'/cancel-employee',
			type:'get',
			data:cancel_data+'&_token='+CSRF_TOKEN,
			dataType:'json',
			beforeSend:function(){
				Loader();
			},
			success:function(data){
				if(data.success == false){
					var message = data.message;
					$('#disable_message_here').html(message);
					$('#DisableJobAppAction').modal('show');
				}
				if(data.success == true){
					//reload the page
					window.location.reload();
				}				
			},
			error:function(errors){
				RemoveLoader();
				
			},
			complete:function(){
				RemoveLoader();
			}
		});
	});
	
	//to accept reject jobseeker application
	$(document).on('click','.accept, .declined',function(){
		var type = $(this).attr('data-type');
		var apply= $(this).attr('data-apply_value');
		//~ $('#set_intrvwTime').modal('show');
		
		//~ $('#application_itrvw_id').val(apply);
		
		bootbox.confirm($(this).data('confirm-message'),function(result){ 
			if(result)
			{
				$.ajax({
					url: path+adminname+"/acceptreject",
					type: 'post',
					dataType: 'json',
					data: '_token='+$('meta[name="csrf-token"]').attr('content')+'&apply_value='+apply+"&status="+type,
					beforeSend:function(){
						Loader();
					},
					success: function(data) {
						if(data.status == false){
							$('#disable_message_here').html(data.message);
							$('#DisableJobAppAction').modal('show');	
						}else{
							window.location.reload();
						}
						RemoveLoader() ;
					},
					error: function(error) { 
						RemoveLoader() ;
					},
					complete: function() { //RemoveLoader() 
					}
			  });
			}
		});
	});
	
	//$('.intervw_time').timepicker({timeFormat: "H:i"});
	
	//~ $('.intervw_time').datetimepicker({
		//~ useCurrent: false,
		//~ showClose: true,
		//~ minDate : moment(),
		//~ format: 'MM/DD/YYYY HH:mm',
	//~ });
	
	/*
	 * DESC : when timeslots are assigned, auto populate end time for next 30 minutes.
	 * */
	$(document).on('blur','#intrvw_one_start',function(){
		var start = $(this).val();
		if(start != ''){
			set_end_time(start,'intrvw_one_end');
		}else{
			$('#intrvw_one_end').val('');
		}
	});
	$(document).on('blur','#intrvw_two_start',function(){
		var start = $(this).val();
		
		if(start != ''){
			set_end_time(start,'intrvw_two_end');
		}else{
			$('#intrvw_two_end').val('');
		}
	});
	$(document).on('blur','#intrvw_three_start',function(){
		var start = $(this).val();
		if(start != ''){
			set_end_time(start,'intrvw_three_end');
		}else{
			$('#intrvw_three_end').val('');
		}		
	});
	
	
	
	/*
	DESC : when invite jobseekers button is clicked.
	29 aug 2017, shivani
	*/
	$('#display_jobseeker_list').click(function(){
		$("body").scrollTop()
		//start loader
		//$('#rehire_fr_job').modal('show');
		//send ajax request to 
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		var employer_id = 0;
		var job_id = $(this).data('id');
		
		$.ajax({
			url:path+adminname+'/user-for-rehire',
			type:'post',
			data:'job_id='+job_id+'&employer_id='+employer_id+'&_token='+CSRF_TOKEN,
			dataType:'json',
			beforeSend:function(){
				Loader();
			},
			success:function(data){
				
				$('#rehire_fr_job').empty().html(data.user_list);
				$('#rehire_remaining').hide();
				$('#rehire_fr_job').modal('show');
				RemoveLoader();
			},
			error:function(errors){
				RemoveLoader();
			}
		});
	});
	
	
	/*
	DESC : to add/remove jobseekers for rehired job
	*/
	$(document).on('click','#selectedUser',function(){
		//check if any jobseeker is selected or not
		var count = $("input[type='checkbox']:checked").length;
		if(count==0){
			$('#rehire_error_messages').addClass('alert-danger').html('Please select atleast 1 employee').fadeIn('slow').delay(3000).fadeOut(); 
			return false;
		}else{
			var token = $('meta[name="csrf-token"]').attr('content');
			Loader();
			var jobId = $('#rehire_job_id').val();
			var jobseekers = $('input[name="offer[]"]:checked').serialize();
			//send ajax request and send invitaion to selected users
			$.ajax({
				type    : 'get',
				url		:	path+adminname+'/send-rehire-invite',
				data	:'_token='+token+'&job_id='+jobId+'&'+jobseekers,
				datatype: 'json',
				success : function(data) 
				{
					RemoveLoader();
					//reload the page
					window.location.reload();
				},
				error: function(data) {
					RemoveLoader();
				}
			});	
		}		
	});
	
	
	$("#DisableJobAppAction").on("hidden.bs.modal", function () {
		$('#disable_message_here').empty();
	});
});

function set_end_time(start,id){
	//create date format          
	var timeStart = new Date(start);
	var timeEnd = new Date(timeStart.getTime() + (30*60000));
	
	var minutes  = timeEnd.getMinutes();
	if(minutes.toString().length == 1){
		minutes = '0'+minutes;
	}
	var month  = (timeEnd.getMonth()+1);
	if(month.toString().length == 1){
		month = '0'+month;
	}
	var hours  = timeEnd.getHours();
	if(hours.toString().length == 1){
		hours = '0'+hours;
	}
	var date_str  = timeEnd.getDate();
	if(date_str.toString().length == 1){
		date_str = '0'+date_str;
	}
	var end_time = month + "/" + date_str + "/" + timeEnd.getFullYear() + " " + hours + ":" +minutes;
	$('#'+id).val(end_time);
}

