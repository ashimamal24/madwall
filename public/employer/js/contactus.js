$(document).on('click','.contact-us',function(){
	$("#usercontactus").validate({	
		rules: {
	      	name:{
	        	required	: true,
	        	 maxlength	: 25 
	      	},
	      	email:{
	        	required	: true,
	        	email 		: true
	      	},
	      	content:{
	        	required	: true,
	        	maxlength	: 1000
	      	},
	    },
	    messages: {
	        name:{
	        	required	: "Please enter your name.",
	        	maxlength	: "Name can not exceed more than 25 characters."
	        },
	        email:{
	        	required	: "Please enter your email.",
	        	email 		: "Please enter valid emailid."
	      	},
	      	content:{
	        	required	: "Please specify a message for the admin.",
	        	maxlength	: "Please enter the message between 50 to 1000 characters only"
	      	}
	    },
	    errorElement:'span',
	    errorClass:'error_msg errormsges',
		submitHandler: function(form) {
		    Loader();
		    $.ajax({
	            url: $('form#usercontactus').attr('action'),
	            type: 'post',
	            dataType: 'json',
	            data: $('form#usercontactus').serialize(),
	            beforeSend:function(){
	                Loader();
	            },
	            success: function(data) {
					RemoveLoader();
					$('form#usercontactus')[0].reset();
					if(data.status==true){
						$('#contactmsg').html('<div class="alert alert-success"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</strong>.</div>');
					}

					if(data.status==false){
						$('#contactmsg').html('<div class="alert alert-error"><strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</strong>.</div>');
					}

					setTimeout(function(){
					window.location.reload();
					},3000);
	            },
				error: function(error) { 
					RemoveLoader();
					$('span.error_msg').hide();
					$('span.error_msgg').hide();
					var result_errors = error.responseJSON;
					if(result_errors) {
						$.each(result_errors, function(i,obj) {
							$('input[name='+i+'],textarea[name='+i+']').parent('.form_grp').find('span.error_msgg').slideDown(400).html(obj);
							if(i == 'token_mismatch')
							window.location.reload();
						}) 
					}
				},
	            complete: function() { //alert('b nn'); 
	            }
	        });
    	}
	});
});