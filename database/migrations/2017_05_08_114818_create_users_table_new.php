<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTableNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('password_reset_requested');
            $table->string('phone');
            $table->string('image');
            $table->string('referred_by');
            $table->string('address');
            $table->string('refferal_code');
            $table->string('dob');
            $table->string('sin');
            $table->string('where_heared_about_us');
            $table->string('documents');
            $table->string('cv');
            $table->string('otp_status');
            $table->string('otp');
            $table->string('otp_expire');
            $table->string('madwall_quiz');
            $table->string('madwall_quiz_blocked');
            $table->string('madwall_quiz_attemp');
            $table->string('madwall_health_first_quiz');
            $table->string('madfwall_quiz_first_percentage');
            $table->string('status');
            $table->string('is_deleted');
            $table->string('role');
            $table->rememberToken();
            $table->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
