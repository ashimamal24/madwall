<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Model\NotificationModel;
use Blade;
use Auth;
use Flash;
use Carbon\Carbon;
use App\Model\PasswordReset;
use App\Model\Documentation;
use App\Model\Document;
use App\Model\Industry;
use App\Model\User;
use App\Model\JobsapplicationModel;
use App\Model\UserRatings;

class Helper
{
    public static function shout(string $string)
    {
        return strtoupper($string);
    }

    public static function getAllNotification()
    {
      $notification = NotificationModel::where(array('status'=>true,'to'=>Auth::user()->_id))->get();
      
      $not_count= count($notification);
     // $content =  view('employer.promo.ajaxnotification',compact('notification','not_count'))->render();
      $result = ['not_count'=>$not_count,'notification'=>$notification];
      return $result;
    }

    public static function checkUser()
    {
    	if(Auth::user()->role == 3)
        {
            if(Auth::user()->approved==2){
                flash()->error('Your profile was declined by administrator.');
               return redirect('employer/logout');
            }
        }
    }
    
    /*
     * Added on : 8 aug 2017
     * Added by : shivani - to display error message if reset token has been expired
     * */
    public static function check_reset_token($token,$email){
		if(PasswordReset::where('token','=',$token)->where('email',$email)->where('created_at','>',\Carbon\Carbon::now()->subMinutes(30))->first()){
			return ['success'=>true,'valid'=>1];
		}else{			
			return ['success'=>true,'valid'=>0];
		}
		return ['success'=>false];
	}
	
	
	/*
	 * 
	 * */
	
	public static function check_difference_in_hours($startTime){
		$currentTime = Carbon::now();
		$start_time = Carbon::parse($startTime);
		//return $start_time->diff($currentTime)->format('%H');
		return $start_time->diffInHours($currentTime);
		//return $start_time->diff($currentTime)->format('%H');
	}
	
	/*
	 * Added on : 16 aug 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to return documentation details to be shown in promo footer
	 * */
	public static function doc_promo_footer(){
		return $document=Document::where(array('status'=>true,'is_deleted' => false))->get();
	}
	/*
	 * Added on : 16 aug 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to return documentation details to be shown in promo footer
	 * */
	public static function indstry_promo_footer(){
		return $industries =Industry::where(array('status'=>true,'is_deleted'=>false))->get()->toArray();
	}

	/*public static function referby($user_id,$start_date,$end_date,$refer_id){
        $jobapplications = JobsapplicationModel::with(array('userjobapplied'))->where(array('jobseeker_id' =>$user_id ))->get()->toArray();
        $hours = 0;
        $calcutate_hour = 0;
        foreach ($jobapplications as $key => $jobapplication) {
        	if($jobapplication['job_status']==5){
        		$hours+=$jobapplication['total_hours_worked'];
        	} else{
        		$calcutate_hour = 1;
        	}
        }
        if($calcutate_hour = 1){
        	$basearray = JobsapplicationModel::with(array('job','employerjobs','userjobapplied','job_shift'=>function($q) use($start_date,$end_date ){$q->where('start_date','>=',$start_date)->where('end_date','<=', $end_date)->get();}))->where(array('jobseeker_id' =>$user_id))->where('job_status' ,'!=', 5 )->get()->toArray();
	        	foreach($basearray as $key=>$base){
	        		foreach ($base['job_shift'] as $jobshift){
		                $hours+=$jobshift['shift_time']/60;
	            	}
	        	}
        }
        if( $hours >=8 ){
    		$refer_code = User::where(array('_id'=>$refer_id ))->value('refferal_code');
    		return $refer_code;
        }
	}*/
	public static function referby($job_status,$user_id,$refer_id){
		if($job_status == 5){
            $jobapplications = JobsapplicationModel::where(array('jobseeker_id' =>$user_id ))->get()->toArray();
            $hours = 0;
            $calcutate_hour = 0;
            foreach ($jobapplications as $key => $jobapplication) {
                $hours+=$jobapplication['total_hours_worked'];
            }
            if($hours>=8){
                return $flag = 1;
              //echo $refercode = User::where(array('_id' =>$refer_id ))->value('refferal_code');
            }
        } else{

        }
	}

    public static function refferal_code($refer_id){
        return $refercode = User::where(array('_id' =>$refer_id ))->value('refferal_code');
    }
    
    /*
     * Added on : 13 sep 2017, shivani
     * To return user rating
     * */
    public static function user_rating($rating_num){
		$user['rating'] = $rating_num;
		$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"."&nbsp;"."<b>(".$user['rating'].")</b>";
		if( isset($user['rating']) ) {
			if( ( $user['rating'] >= 1 ) && ( $user['rating'] < 1.5 ) ){
				//1 star
				$rating = "<i class='fa fa-star'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}
			elseif( ( $user['rating'] >= 1.5 ) && ( $user['rating'] < 2) ){
				//1.5 star
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}
			elseif( ( $user['rating'] >= 2 ) && ( $user['rating'] < 2.5 ) ){
				//2 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}
			elseif( ( $user['rating'] >= 2.5 )&& ( $user['rating'] < 3 ) ){
				//2.5 star
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}elseif( ( $user['rating'] >= 3 ) && ( $user['rating'] < 3.5 ) ){
				//3 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}
			elseif( ( $user['rating'] >= 3.5 ) && ( $user['rating'] < 4) ){
				//3.5 star
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}elseif( ( $user['rating'] >= 4 ) && ( $user['rating'] < 4.5 ) ){
				//4 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}
			elseif( ( $user['rating'] >= 4.5 ) && ( $user['rating'] < 5) ){
				//4.5 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}
			else {
				//5 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>"."<br>"."<b>(".$user['rating'].")</b>";
			}
		}
		return $rating;
	}
	
	public static function job_rating_for_user($jobseeker_id,$job_id){
		return UserRatings::where(['type'=>'job_rating','type_id'=>$jobseeker_id,'job_id'=>$job_id])->value('rating');	
	}
	
	/*
	 * Added on : 16 oct 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to check referals of a jobseeker and calculate their referal amount
	 * */
	public static function check_referals($jobseeker_id){
		//check all user referred by current(logged) user
        $jobseekerApplication = User::where("reffered_by",$jobseeker_id)->with(['jobseekerjobs'=>function($q){
                $q->where('total_hours_worked','!=',null)->where('total_hours_worked','!=',0)->whereIn('job_status',[4,5,7])->with('job_shift');
        }])->get()->toArray();
        $workMinutes = $earnedAmount = $workHours = 0;
        if(!empty($jobseekerApplication)){
            //loop through all users and check job shift  hours completed
            foreach ($jobseekerApplication as $job) {
                if(!empty($job['jobseekerjobs'])){
                    //loop through all jobs found
                    foreach ($job['jobseekerjobs'] as $application) {
                         $workHours += $application['total_hours_worked'];
                    }//endforeach (jobseeker jobs)
                }
                //if work minutes are greater than 0, then convert to hours
                if($workHours >= 8){
					$earnedAmount += 20;
				}
				$workMinutes = $workHours = 0;
            }
        }
        return ['earned_amount' => $earnedAmount];
	}
	
	/*
	 * Added : 22 nov 2017, shivani debut infotech
	 * DESC : to return job shift details while creating and updating jobs.
	 * */
	public static function getShiftDates($dates)
	{
		$data = [];
		$data2 = [];
		$data3 = [];
		$curTime = Carbon::now();
		foreach($dates AS $k => $row)
		{
			$start_date = date('d-m-Y',strtotime($row['start_date']));
			$formatted_date = date('d-M l',strtotime($row['start_date']));
			$start_time = date('H:i',strtotime($row['start_date']));
			$end_time   = date('H:i',strtotime($row['end_date']));
			if($row['start_date'] > $curTime){
				
				$data[]     = ['start_date'=>$start_date,'start_time'=>$start_time,'end_time'=>$end_time];				
			}
			$data2[]    = $start_date;
			$data3[]    = $formatted_date;
		}
		return ['shifts'=>$data,'dates'=>$data2,'format_dates'=>$data3];
	}
	
	public static function getDates($dates)
	{
		$data = [];
		foreach($dates AS $k => $row)
		{
			$data[] = date('Y-m-d H:i:s',strtotime($row['start_date']));

		}
		return $data;
	}
	
}
