<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmailModel extends Eloquent
{

	// Specify the table name.
    protected $table = 'email_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'subject',
        'mailcontent',
        'status',
        'is_deleted',
        'created_at',
        'updated_at'
    ];
}
