<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Industry extends Eloquent
{
    //
    protected $table = 'industry';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'image', 'file_name',
        'status',
        'is_deleted',
        'created_at',
        'updated_at'
    ];

    /*============================
        Relation With jobs model
    ==============================
     */
    public function relatedcategory()
    {
        return $this->HasMany('App\Model\CategoryModel', 'industry_id', '_id');
    }

}
