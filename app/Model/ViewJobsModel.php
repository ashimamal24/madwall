<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ViewJobsModel extends Eloquent
{
    //
    protected $table = 'job_viewed';

    protected $fillable = ['jobseeker_id', 'ma_job_id', 'rehire_job_id', 'status'];
}
