<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Wage extends Eloquent
{
    // Specify the table name.
    protected $table = 'manage_wage';

    /*=========================================
     * The attributes that are mass assignable.
     * @var array
     *========================================*/
    protected $fillable = ['min_amount', 'status', 'is_deleted', 'created_at', 'updated_at'];
}
