<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TimeSlot extends Eloquent
{
    protected $table = 'user_timeslots';
    protected $fillable = ['user_id', 'status', 'accepted'];
    protected $dates = ["start_time", "end_time"];

}
