<?php 
namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Quiz extends Eloquent
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'question',
		'answer',
		'options',
		'status',
		'type',
		'image',
		'serial',
		'is_deleted'
	];
	public function getImageAttribute($value)
	{
		return env('APP_URL') . '/public/health_quiz/' . $value;
	}





}
