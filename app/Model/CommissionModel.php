<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CommissionModel extends Eloquent
{
    // Specify the table name.
    protected $table = 'commission';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'category_name',
        'commission_amount',
        'status',
        'is_deleted',
        'created_at',
        'updated_at'
    ];
}
