<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Carbon\Carbon;

class ApplicantShift extends Eloquent
{
    //

    protected $table = 'applicant_shift';

    protected $fillable = ['app_id', 'job_id', 'start_date', 'end_date', 'total_hours_worked', 'lunch_hour'];

    protected $dates = ['start_date', 'end_date'];

    protected $appends = ['shift_time'];

    public function getShiftTimeAttribute()
    {
        $start = Carbon::parse($this->start_date);
        $end = Carbon::parse($this->end_date);

        $current_time = Carbon::now();
        if ($current_time >= $start && $current_time < $end) {
            $duration = $current_time->diffinMinutes($start);
            if ($duration < 60) {
                $duration = 0;
            }
        } else if ($current_time < $start && $current_time < $end) {
            $duration = 0;
        } else {
            $duration = $end->diffinMinutes($start);
        }

        return $duration;
    }

    public function job_detail()
    {
        return $this->belongsTo('App\Model\JobsModel', 'job_id', '_id');
    }

    public function job_application()
    {
        return $this->HasMany('App\Model\JobsapplicationModel', 'app_id', '_id');
    }
}
