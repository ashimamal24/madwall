<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TempJobdelete extends Eloquent
{
    // Specify the table name.
    protected $table = 'temp_job_delete';

    protected $dates = ['delete_at'];

    protected $fillable = ['job_id', 'delete_at'];
}
