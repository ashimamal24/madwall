<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AboutModel extends Eloquent
{
    //
    protected $table = 'aboutus';

    protected $fillable = ['message', 'status', 'is_deleted', 'created_at', 'updated_at', 'user_id'];
}
