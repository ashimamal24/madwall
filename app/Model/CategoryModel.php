<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CategoryModel extends Eloquent
{
    
    // Specify the table name.
    protected $table = 'category';


    public function categoryrelation()
    {
        return $this->hasOne('App\Model\CategoryModel', '_id', 'parent_id');
    }

   /*====================
        Specify Column
    ======================
     */



    protected $fillable = [
        'name', 'description', 'type', 'commision', 'industry_name',
        'industry_id', 'status', 'used', 'parent_id', 'category_id', 'category_name', 'skills', 'skill_object', 'is_deleted', 'created_at', 'updated_at', 'category_object', 'mandatory_skills', 'mandatory_object'
    ];

}
