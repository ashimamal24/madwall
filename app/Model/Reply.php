<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Reply extends Eloquent
{
  

    protected $table = 'replies';
    
    protected $fillable = [
        'comment_id', 'reply_id', 'first_name', 'last_name', 'replay', 'is_deleted', 'created_at', 'updated_at'
    ];

}
