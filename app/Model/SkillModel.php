<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SkillModel extends Eloquent
{
    // Specify the table name.
    protected $table = 'master_skill';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'status',
        'is_deleted',
        'created_at',
        'used',
        'updated_at'
    ];
}
