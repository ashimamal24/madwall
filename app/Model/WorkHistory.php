<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WorkHistory extends Eloquent
{
    // Specify the table name.
    protected $table = 'work_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['job_id', 'jobseeker_id', 'start_time', 'end_time', 'completed_on', 'leaving_on', 'is_deleted', 'working_hour', 'created_at', 'updated_at'];
}
