<?php

namespace App\Model;

//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmergencyContact extends Eloquent
{
    protected $table = 'emergency_contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','nameOne','relationshipOne','addressOne','CountryCodeOne','phoneNoOne','nameSecond','relationshipSecond','addressSecond','CountryCodeSecond','phoneNoSecond','created_at', 'updated_at'];

}
