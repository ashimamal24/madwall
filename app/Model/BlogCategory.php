<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BlogCategory extends Eloquent
{
  
    protected $table = 'blog_category';  

    protected $fillable = ['name', 'status', 'used', 'is_deleted', 'created_at', 'updated_at'];


   /* public function blog_category()
	{
		return $this->belongsTo('App\Model\BlogCategory');
	}
*/
}
