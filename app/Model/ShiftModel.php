<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Carbon\Carbon;

class ShiftModel extends Eloquent
{
    //

    protected $table = 'job_shift';

    protected $fillable = ['job_id', 'start_date', 'end_date', 'status', 'job_id_object'];

    protected $dates = ['start_date', 'end_date'];
    protected $appends = ['shift_time'];
    public function getShiftTimeAttribute()
    {
        $start = Carbon::parse($this->start_date);
        $end = Carbon::parse($this->end_date);

        $current_time = Carbon::now();
        if ($current_time >= $start && $current_time < $end) {
            $duration = $current_time->diffinMinutes($start);
            if ($duration < 60) {
                $duration = 0;
            }
        } else if ($current_time < $start && $current_time < $end) {
            $duration = 0;
        } else {
            $duration = $end->diffinMinutes($start);
        }

        return $duration;
    }
}
