<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TempJobs extends Eloquent
{
    // Specify the table name.
    protected $table = 'temp_job_data';

    /*=========================================
     * The attributes that are mass assignable.
     * @var array
     *========================================*/
    protected $fillable = ['start_date', 'end_date', 'user_id', 'process_id', 'created_at', 'updated_at'];

    protected $dates = ['start_date', 'end_date'];
}
