<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CmsPages extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'cms_pages';

    protected $fillable = [
        'name', 'status', 'content', 'alias', 'type', 'is_deleted'
    ];





}
