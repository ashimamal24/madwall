<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class JobsapplicationModel extends Eloquent
{
    //
    protected $table = 'job_application';
    
    
    /*===============
     * Job Application status codes
     * 0 - offered/applied
     * 1 - accepted from employer side
     * 2 - accepted from jobseeker app
     * 3 - declined by jobseeker app
     * 4 - complete
     * 5 - cancel by jobseeker app
     * 6 - declined by employer 
     * 7 - fired by employer
     * ===============*/



    protected $fillable = ['jobseeker_id', 'job_id', 'employer_id', 'job_status', 'job_type', 'rating', 'new_job', 'is_applied', 'is_deleted', 'applied_date', 'is_viewed', 'total_hours_worked', 'completed_at', 'withdraw_date', 'shifts'];

    protected $dates = ['applied_date', 'completed_at', 'withdraw_date'];

    public function applyjobuser()
    {
        return $this->HasOne('App\Model\User', '_id', 'jobseeker_id');
    }

    /*=========================================================================
	Function for getting job detail with job application
    ===========================================================================*/
    public function job()
    {
        return $this->HasOne('App\Model\JobsModel', '_id', 'job_id');
    }

    /*=============================================
    Function for getting job detail with ShiftModel
    ==============================================*/
    public function job_shift()
    {
        return $this->HasMany('App\Model\ShiftModel', 'job_id', 'job_id');
    }


    public function app_shift()
    {
        return $this->HasMany('App\Model\ApplicantShift', 'app_id', '_id');
    }

    public function userjobapplied()
    {
        return $this->belongsTo('App\Model\User', 'jobseeker_id', '_id');
    }

    public function employerjobs()
    {
        return $this->belongsTo('App\Model\User', 'employer_id', '_id');
    }

    /*public function getTagTranslatedAttribute() {
        return $this->is_deleted;
    }*/


    public function job_detail()
    {
        return $this->belongsTo('App\Model\JobsModel', 'job_id', '_id');
    }

    protected $appends = ['job_app_status'];
    public function getJobAppStatusAttribute()
    {
        return $this->job_status;
    }
}
