<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Document extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'documents';

    protected $fillable = [
        'name', 'general_file_name', 'general_file_url', 'status', 'is_deleted'
    ];
}
