<?php

namespace App\Model;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class MasterHearAboutAndIdProof extends Eloquent
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'master_hear_about_us_and__id_proof';
    protected $fillable = ['name', 'slug', 'status', 'type'];



}
