<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PasswordReset extends Eloquent
{
    //
    protected $collection = 'password_resets';
}
