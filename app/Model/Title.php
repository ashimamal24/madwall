<?php

namespace App\Model;

//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Title extends Eloquent
{
    protected $table = 'titles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','created_at', 'updated_at'];
}
