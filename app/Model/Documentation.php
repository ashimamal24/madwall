<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Documentation extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'documentation';

    protected $fillable = [
        'doc_name'
    ];
    protected $appends = ['extension'];
    //function creating a extension for each file
    public function getExtensionAttribute()
    {
        $ext = explode('.', $this->doc_name);
        if ($ext[1] == 'pdf') {
            $class = 'fa fa-file-pdf-o';
        } else {
            $class = 'fa fa-file-word-o';
        }
        return $class;
    }
}
