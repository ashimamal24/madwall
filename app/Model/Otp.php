<?php 
namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Otp extends Eloquent
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'otp',
		'type',
		'expired_at'

	];



}
