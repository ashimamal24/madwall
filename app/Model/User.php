<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Notifications\UserResetPasswordNotification;

class User extends Eloquent implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Notifiable, Authenticatable, Authorizable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'email', 'password', 'first_name', 'last_name', 'phone', 'image', 'password_reset_requested', 'otp', 'otp_expire', 'rating', 'status', 'is_deleted', 'device_token', "device_type", 'role', 'profile_complete', 'address', 'sin_number', 'dob', 'source', 'id_proofs', 'cv_url', 'cv_name', 'madwall_quiz_percentage', 'health_quiz_attempt', 'health_quiz_percentage', 'healt_quiz_blocked_until', 'country_code', 'approved', 'slot_requested_additional', 'madwall_quiz_answer', 'madwall_health_answer', 'user_slot_accepted', 'category_id', 'skills', 'notification', 'refferal_code', 'reffered_by', 'user_email_confirmed', 'additional_documents', 'category_name', 'image_name', 'phone_verified', 'subcategories', 'reason_for_block_usr', 'company_name', 'contact_name', 'industry', 'industry_type', 'location', 'commission', 'extra_discount', 'number_worker', 'company_contact', 'company_description', 'email_verification_code', 'category_object', 'company_code', 'skills_ids', 'other_documents', 'company_address', 'lat', 'lng', 'key', 'mwuserid', 'coordinate', 'approved_date', 'link_expire_at', 'new_email', 'monthly_bonus', 'category_details', 'new_cv', 'new_cv_name', 'industry_id','agreement_status','emergency_contact','direct_deposit','agreement_send','pushkit_token','opentokSessionId'];

    protected $casts = [
        'rating' => 'float'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['otp_expire', 'dob', 'healt_quiz_blocked_until', 'approved_date', 'link_expire_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*==========================
        Relation With timeslots
    ============================
     */
    public function timeslots()
    {
        return $this->HasMany('App\Model\TimeSlot', 'user_id', '_id');
    }

    /*============================
        Relation With jobs model
    ==============================
     */
    public function userjobs()
    {
        return $this->HasMany('App\Model\JobsModel', 'user_id', '_id');
    }




    /*============================
        Relation With jobsApplication model (for jobseeker)
    ==============================
     */
    public function jobseekerjobs()
    {
        return $this->HasMany('App\Model\JobsapplicationModel', 'jobseeker_id', '_id');
    }





    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->{$this->getRememberTokenName()};
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->{$this->getRememberTokenName()} = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    /*public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
    }*/
}
