<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Faqs extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'faqs';

    protected $fillable = [
        'faqs', 'answer', 'type', 'is_deleted', 'status'
    ];
}
