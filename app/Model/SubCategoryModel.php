<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SubCategoryModel extends Eloquent
{
    
    // Specify the table name.
    protected $table = 'subcategory';

    /*====================
        Specify Column
    ======================
     */
    protected $fillable = [
        'name',
        'description',
        'category_id',
        'category_name',
        'skills',
        'status',
        'is_deleted',
    ];
}
