<?php


namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserRatings extends Eloquent
{
	// Specify the table name.
    protected $table = 'user_ratings';

    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'type_id',
        'rated_by_id',
        'job_id',
        'rating',
        'points',
        'created_at',
        'updated_at'
    ];
}
