<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Blog extends Eloquent
{
  
    protected $fillable = ['blog_cat_id', 'title', 'blog_description', 'author_name','author_description','blog_image','author_image','comments',
    'status', 'is_deleted','created_at', 'updated_at']; 



    public function blog_category(){
	 
	 return $this->belongsTo('App\Model\BlogCategory','blog_cat_id','_id');
	}


	public function comments(){
	  
	  return $this->hasMany('App\Model\Comment');
	}

	public function comments_count(){
	  
	  return $this->hasMany('App\Model\Comment')->whereUserId($this->blog_id)->where('is_deleted',false)->count();
	}

   /* public function replies()
	{
		return $this->hasMany('App\Model\Replay');
	}*/
   



}
