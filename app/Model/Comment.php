<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Comment extends Eloquent
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  
    protected $fillable = [
        'post_id', 'first_name', 'last_name', 'comment', 'is_deleted','created_at', 'updated_at'
    ];

    
    public function replies()
	{
		return $this->hasMany('App\Model\Reply');
	}
}
