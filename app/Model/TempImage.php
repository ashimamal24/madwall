<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TempImage extends Eloquent
{
    // Specify the table name.
    protected $table = 'temp_images';

    protected $fillable = ['type', 'image_name'];
}
