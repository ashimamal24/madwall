<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class IndustryContent extends Eloquent
{

    protected $table = 'industry_contents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_name', 'user_image', 
                           'description', 'image', 'status',
                           'is_deleted', 'created_at', 'updated_at'];

}
