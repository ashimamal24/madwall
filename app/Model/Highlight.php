<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Highlight extends Eloquent
{
    
    protected $table = 'highlights';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'descriptions','created_at', 'updated_at'];
}
