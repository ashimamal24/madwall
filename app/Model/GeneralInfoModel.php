<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GeneralInfoModel extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'general_info';

    protected $fillable = [
        'name', 'general_file_name', 'general_file_url', 'status', 'is_deleted'
    ];
}
