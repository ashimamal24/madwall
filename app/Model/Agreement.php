<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Agreement extends Eloquent
{
    protected $table = 'agreement';


   	protected $fillable = [

    	'madeIn',
    	'madeOnDay',
    	'madeOnMonth',
    	'madeOnYear',

    	'between',

    	'employeSin',
    	'sinName',

    	'harassmentPolicy',
    	'violencePolicy',
    	'latenessPolicy',
    	'abusePolicy',
    	'standardsPolicy',
    	'workPolicy',
    	'reatingPolicy',
    	'privacyPolicy',
    	'employeeHandbook',
    	'termsAndConditions',

    	'madWallPoliciesState',
    	'personalProtectiveEquipment',
    	'employmentStandards',
    	'independentLegalAdvice',

    	'witnessSignature',

    	'agreementOnDay',
    	'agreementOnMonth',
    	'agreementOnYear',

    	'witnessName',
    	'employeeName',
    	'agreementDate',
    	'employeeId',

    	'created_at',
    	'updated_at'
    ]; 

}
