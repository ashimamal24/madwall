<?php 
namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmailTemplate extends Eloquent
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'subject',
		'content',
		'language',
		'status',
		'is_deleted'
	];

	/**  Has Many Relationship with EmailTemplateAttribute Model  **/
	public function template_attributes()
	{
		return $this->hasMany('App\Models\EmailTemplateAttribute', 'email_template_id');
	}

}
