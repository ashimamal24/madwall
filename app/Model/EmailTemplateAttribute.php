<?php 
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class EmailTemplateAttribute extends Eloquent
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'email_template_id',
		'variable'
	];

	/**  Belongs To Relationship with EmailTemplate Model  **/
	public function email_template()
	{
		return $this->belongsTo('App\Models\EmailTemplate');
	}
}
