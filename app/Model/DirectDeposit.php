<?php

namespace App\Model;

//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DirectDeposit extends Eloquent
{
    protected $table = 'direct_deposit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','institutionNo','transitNo','accountNo','created_at', 'updated_at'];


}
