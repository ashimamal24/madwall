<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Model\ShiftModel;

class JobInterview extends Eloquent
{
    //
    protected $collection = "job_interviews";
    protected $fillable = ['application_id', 'job_id', 'status'];

    protected $dates = ['start_date', 'end_date'];


}
