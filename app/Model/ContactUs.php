<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ContactUs extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'contact_us';

    protected $fillable = [
        'email', 'status', 'content', 'type', 'user_id', 'is_deleted', 'name', 'subject', 'file_name', 'file_url', 'request_status'
    ];
}
