<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Model\ShiftModel;

class JobsModel extends Eloquent
{
    //
    protected $collection = "jobs";
    protected $fillable = ['title', 'job_category', 'job_subcategory', 'skills', 'description', 'salary_per_hour', 'number_of_worker_required', 'address', 'job_published_type', 'category', 'is_deleted', 'subcategory', 'skill', 'shifts', 'total_applied', 'total_hired', 'status', 'location', 'job_status', 'user_id', 'rating', 'start_date', 'end_date', 'physical_requirement', 'safety_hazards', 'contact_name', 'meeting_location', 'job_id', 'publisher_status', 'total_work_hours', 'category_commision', 'lunch_hour', 'rehire_type', 'start_time', 'end_time'];
	
	
	//0->pending, 1->active,2->filled,3->inprogress,4->completed
    protected $dates = ['start_date', 'end_date'];

    /*============================
        Relation With jobs model
    =============================*/
    public function userjobs()
    {
        return $this->HasMany('App\Model\User', '_id', 'user_id');
    }

    public function jobapplied()
    {
        return $this->HasMany('App\Model\JobsapplicationModel', 'job_id', '_id');
    }

    public function jobshifts()
    {
        return $this->HasMany('App\Model\ShiftModel', 'job_id', '_id');
    }

    public function jobcancel()
    {
        return $this->HasMany('App\Model\WorkHistory', 'job_id', '_id');
    }

    public function application()
    {
        return $this->HasMany('App\Model\JobsapplicationModel', '_id', 'job_id');
    }

    public function jobwithuser()
    {
        return $this->belongsTo('App\Model\User', 'user_id', '_id');
    }


    protected $appends = ['shifts'];
    public function getShiftsAttribute()
    {
        return ShiftModel::where('job_id', $this->_id)->orderBy('start_date', 'asc')->get()->toArray();
    }

    public function app_shifts()
    {
        return $this->hasMany('App\Model\ApplicantShift', 'job_id', '_id');
    }
}
