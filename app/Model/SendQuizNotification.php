<?php

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SendQuizNotification extends Eloquent
{
    // Specify the table name.
    protected $table = 'quiz_notification';


    protected $dates = ['created_at', 'updated_at', 'send_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['to', 'notification_sent', 'type', 'title', 'lng', 'lat', 'job_id'];
}
