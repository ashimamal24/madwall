<?php

namespace App\Http\Controllers\Webpage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Blog;
use App\Model\BlogCategory;
use App\Model\Industry;
use App\Model\Documentation;
use Carbon\Carbon;
use Auth;
class BlogController extends Controller
{
  
    public function __construct()
    {
      
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }

    }

    /*============================
        Display all blogs.
    =============================*/
    public function index(Request $request){
      
      if (Auth::check()){
       return redirect('employer/dashboard');
      }
	    $query = Blog::query();

	    if (request('category')) {
	     $query->where('blog_cat_id', request('category'));
	    }

	      $blog = $query->where('status',true)->where('is_deleted',false)->orderBy('created_at','DESC')->paginate(3);

      //$blogCategory = Blog::with('blog_category')->where('status',true)->groupBy('blog_cat_id')->get();
        $blogCategory = BlogCategory::where('status',true)->where('is_deleted',false)->get();

        $industry = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        $industries = Industry::where(array('status' => true, 'is_deleted' => false))->get()->toArray();
        $documetation = Documentation::All();
        $openDocument = false;


       if ($request->ajax()) {
        return view('blogs.blog', compact('blog','industry', 'documetation', 'industries', 'openDocument'));
       }else{
        return view('blogs.index',compact('blogCategory','blog','industry', 'documetation', 'industries', 'openDocument'));
       }
      
    }

    /*============================
        Get specific blog details.
    =============================*/
    public function details(Request $request,$id){
      
      if (Auth::check()){
       return redirect('employer/dashboard');
      }
      
      $industry = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
      $industries = Industry::where(array('status' => true, 'is_deleted' => false))->get()->toArray();
      $documetation = Documentation::All();
      $openDocument = false;

      $blogDetail = Blog::where('_id',$id)->where('status',true)
                          ->where('is_deleted',false)
                          ->with(['comments' => function($q){
                             $q->paginate(2);
                             $q->where('is_deleted',false);
                             $q->orderBy('created_at','DESC');
                          }])->first();
    


      if ($request->ajax()) {
        $view = view('blogs.comments',compact('blogDetail'))->render();
            return response()->json(['html'=>$view]);
      }

      if($blogDetail){
       return view('blogs.detail',compact('blogDetail','industry', 'documetation', 'industries', 'openDocument')); 
      }else{
        abort(404);
      }
     

    }

    /*============================
        Get load more Blogs.
    =============================*/
    public function loadBlog(Request $request){
       
        $output = '';
        $id = $request->id;
       
        $query = Blog::query();

        if (request('category')) {
          $query->where('blog_cat_id', request('category'));
        }

        $blog = $query->where('_id','<',$id)->where('status',true)
                      ->where('is_deleted',false)
                      ->orderBy('created_at','DESC')->paginate(3);
     
        if(!$blog->isEmpty())
        {
            foreach($blog as $val)
            {
               $title = str_word_count(strip_tags($val->title)) >5? substr(strip_tags($val->title),0,30)."....":$val->title;

                $description = $val->blog_description;
                $url = url('/blog/details').'/'.$val->_id;

                if(!empty($val->blog_image)){
                  $image = url('public/blog_images').'/'.$val->blog_image;
                }else{
                  $image = url('public/blog_images/default.png');
                } 
               // $body .= strlen(strip_tags($val->body))>500?"...":"";
                  $description = str_word_count(strip_tags($val->blog_description)) >50? substr(strip_tags($val->blog_description),0,200)."....":$val->blog_description;   

                   $createdAt = Carbon::parse($val->created_at)->format('dS') . ' ' . 
                                   Carbon::parse($val->created_at)->format('M') . ' ' . 
                                    Carbon::parse($val->created_at)->format('Y');
  
                   $output .= '<div class="blog-class">
                              <div class="blog-box">';
                          
                   $output .= '<div class="blog-page blogi-img" style="background:url('.$image.') no-repeat center;">
                                    <img src="'.$image.'">
                                </div>';
     
                   $output .= '<div class="blogi-info">
                                    <h4><a href="'.$url.'">'.$title.'</a></h4>
                                    <h6> <i class="fa fa-calendar"></i>&nbsp; '.$createdAt.'</h6>
                                    <p>'.$description.'</p>
                                    <a class="btn btn-primary" href="'.$url.'">Read More</a>
                                </div>
                            </div>
                            </div>';
            }
            
            $output .= ' <div class="clearfix"></div><div id="remove-row" class="text-center">
                            <button id="btn-more" data-id="'.$val->id.'" class="btn btn-primary nounderline mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Load More </button>
                        </div>';
            echo $output;
         }
    }

}
