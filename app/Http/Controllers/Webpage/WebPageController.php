<?php

namespace App\Http\Controllers\Webpage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CmsPages;
use App\Model\Documentation;
use Auth; //29july2017, shivami

class WebPageController extends Controller
{
	//updated : 29 july 2017, to return different view for logged users
    public function termsCondtions()
    {
        $cmspage = CmsPages::select('name', 'content')->where(array('alias' => 'terms_conditions', 'is_deleted' => false, 'type' => 'web'))->get()->toArray();
        $title = "Terms And Conditions";
        $documetation = Documentation::All();
        if (Auth::check()) {
            return view('employer/promo/terms', compact('cmspage', 'title', 'documetation'));
        } else {
            return view('webPages/content', compact('cmspage', 'documetation'));
        }
    }
	//updated : 29 july 2017, to return different view for logged users
    public function aboutUs()
    {
        $cmspage = CmsPages::select('name', 'content')->where(array('alias' => 'about_us', 'is_deleted' => false, ))->get()->toArray();
        $title = "About MadWall";
        $documetation = Documentation::All();
        if (Auth::check()) {
            return view('employer/promo/terms', compact('cmspage', 'title', 'documetation'));
        } else {
            return view('webPages/content', compact('cmspage', 'documetation'));
        }
    }
	
	//updated : 29 july 2017, to return different view for logged users
    public function privacyPolicy()
    {
        $cmspage = CmsPages::select('name', 'content')->where(array('alias' => 'privacy_policy', 'is_deleted' => false, 'type' => 'web'))->get()->toArray();
        $title = "Privacy Policy";
        $documetation = Documentation::All();
        if (Auth::check()) {
            return view('employer/promo/terms', compact('cmspage', 'title', 'documetation'));
        } else {
            return view('webPages/content', compact('cmspage', 'documetation'));
        }
    }

    public function apitermsCondtions()
    {
        $cmspage = CmsPages::where(array('alias' => 'terms_conditions', 'type' => 'mobile'))->first()->toArray();
        return view('apiPages/content', compact('cmspage'));
    }

    public function apiaboutUs()
    {
        $cmspage = CmsPages::where(array('alias' => 'about_us'))->first()->toArray();
        return view('apiPages/content', compact('cmspage'));

    }

    public function apiprivacyPolicy()
    {
        $cmspage = CmsPages::where(array('alias' => 'privacy_policy', 'type' => 'mobile'))->first()->toArray();
        return view('apiPages/content', compact('cmspage'));
    }

}
