<?php

namespace App\Http\Controllers\Webpage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositary\CommonRepositary;
use App\Http\Requests\CommentRequest;
use App\Model\Blog;
use App\Model\BlogCategory;
use App\Model\Comment;
use App\Model\Reply;
use Carbon\Carbon;

class CommentController extends Controller
{


    public function __construct()
    {
        
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }

    }

    /*============================
        Post a comment.
    =============================*/
    public function postComment(CommentRequest $request,$blogId){

       $blog = Blog::where(array('_id' => $blogId, 'is_deleted' => false))->first();
       if($blog){
             $comment = new Comment();
             $comment['blog_id'] = $blogId;
             $comment['first_name'] = $request->first_name;
             $comment['last_name'] = $request->last_name;
             $comment['comment'] = $request->comment;
             $comment['is_deleted'] = false;
       
            if($comment->save()){
                $comment['_id'] = $comment->_id;
                    $comment['date'] = Carbon::parse($comment->created_at)->format('d') . ' ' . 
                    Carbon::parse($comment->created_at)->format('M') . ' ' . 
                    Carbon::parse($comment->created_at)->format('Y') .' | '.
                    Carbon::parse($comment->created_at)->format('g:i A');
               
             return response()->json(['status' => true, 'message' => 'Success.','data'=>$comment]);
            }else{
             return response()->json(['status' => false, 'message' => 'Opps! Something went wrong please try again.']);  
            }
        }else{
           return response()->json(['status' => false, 'message' => 'Opps! blog not found.']);  
        }         
    }


    /*============================
        Reply a comment.
    =============================*/
    public function commentReply(CommentRequest $request,$commentId){
        
        $replyCount = Reply::where('comment_id',$commentId)->count();

        if($replyCount < 5)
        {
            $reply = new Reply();
            $reply->comment_id = $commentId;
            $reply->reply = $request->comment;
            $reply->first_name  = $request->first_name;
            $reply->last_name  = $request->last_name;
    		
    		if ($reply->save()) {

                 $reply->date = Carbon::parse($reply->created_at)->format('d') . ' ' . 
                        Carbon::parse($reply->created_at)->format('M') . ' ' . 
                        Carbon::parse($reply->created_at)->format('Y') .' | '.
                        Carbon::parse($reply->created_at)->format('g:i A');

    		   return response()->json(['status' => true, 'message' => 'Success','data'=>$reply]);

    		} else {
    		   return response()->json(['status' => false, 'message' => 'Opps! Something went wrong please try again.']);	
    		}
        }else{
             return response()->json(['status' => true, 'message' => 'You can not send message.']);    
        }
	           
    }



}
