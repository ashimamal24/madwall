<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
//use repositories\CommonRepositoryInterface;
use Illuminate\Http\Request;
use App\Model\CategoryModel;
use App\Model\User;
use App\Model\Documentation;
use App\Model\EmailVerification;
use Auth;
use DB;
use App\Model\Industry;
use App\Model\IndustryContent;
use Validator;
use Session;
use App\Model\Title;
use App\Model\Highlight;



class HomeController extends Controller
{
    /**
     * Create a new controller instance. added by pankaj
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        //$this->middleware('test');
        $this->middleware('admin', ['except' => ['indexEmployer', 'getConfirmEmail', 'getDashboard', 'jobshare_deeplink', 'test_user']]);
        $this->middleware('CheckRedirection', ['only' => ['indexEmployer']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try {
            $waitlist = User::where(array('role' => 2, 'approved' => 0))->count();
            $approved = User::where(array('role' => 2, 'approved' => 1))->count();
            $waitingEmployer = User::where(array('role' => 3, 'approved' => 0))->count();
            $approvedEmployer = User::where(array('role' => 3, 'approved' => 1))->count();

            return view('admin/dashboard', ['active' => 'dashboard', 'waitlist' => $waitlist, 'approved' => $approved, 'waitingEmployer' => $waitingEmployer, 'approvedEmployer' => $approvedEmployer]);

        } catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage(),
                'active' => 'dashboard'
            ];
            return view('errors.error', $result);
        }
    }


    public function changeStatus(Request $request)
    {
        $id = $request->id;

        $status = DB::table($request['table'])->where('_id', $id);
        if ($request['status'] == '1') {
            $status->update(['status' => false]);
        } else {
            $status->update(['status' => true]);
        }
        return response()->json(['success' => true, 'action' => $request['action']]);


    }


    /*==============================================================================================
    Function for showing home page of employer section
    =================================================================================================*/
    public function indexEmployer(Request $request)
    {
        if (Auth::check())
            return redirect('employer/dashboard');
        $industry = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        $industries = Industry::where(array('status' => true, 'is_deleted' => false))->get()->toArray();
        $documetation = Documentation::All();
        $openDocument = false;
        if (isset($request->app) && !empty($request->app)) {
            $openDocument = true;
        }
        
        $industryContent = IndustryContent::where(array('status' => true, 'is_deleted' => false))->get();

        $titles = Title::first()->toarray();
        $highlights = Highlight::limit(3)->get()->toarray();
        $highlight = Highlight::offset(3)->limit(3)->get()->toarray();
        
        //print_R($highlights->toarray());die;
        return view('employer.promo.index', compact('industry', 'documetation', 'industries', 'openDocument', 'industryContent','titles','highlights','highlight'));
    }

    public function getDashboard($value = '')
    {
        die('cv ');
    }

    /*================================================================================================
    Function for verify email of a user
    ==================================================================================================*/

    public function getConfirmEmail(Request $request, $code)
    {
        //update user's confirm status.
        if ($verification = EmailVerification::where(array('status' => true, 'code' => $code))->first()) {
            if (User::where('email', $verification->email)->update(array('user_email_confirmed' => true)) && $verification->update(array('status' => false))) {
                $status = 1;
            } else {
                $status = 2;
            }
        } else {
            $status = 3;
        }

        return view('emails.email_verification', compact('status'));
    }
    
    
    /*
     * Added on : 7 oct 2017, shivani debut infotech
     * to implement deeplink for jobshare.
     * */
    public function jobshare_deeplink($job_id)
    {
        return view('employer.deeplink.schedule_deeplink', compact('job_id'));
    }

    public function test_user()
    {
        return User::where('email', 'shivani.shivu6@gmail.com')->first();
    }
}
