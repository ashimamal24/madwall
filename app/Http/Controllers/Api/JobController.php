<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterHearAboutAndIdProof;
use App\Model\User;
use App\Model\JobsModel;
use App\Model\CategoryModel;
use App\Model\Token;
use App\Model\JobsapplicationModel;
use App\Model\ShiftModel;
use App\Model\WorkHistory;
use App\Model\Setting;
use App\Model\UserRatings;
use Validator;
use Carbon\Carbon;
use JWTAuth;
use App\Http\Repositary\CommonRepositary;
use Auth;
use App\Model\SendQuizNotification;
use DB;
use MongoDB\BSON\UTCDateTime as MongoDate;
use DateTime;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwtcustom');
    }
    
    
    /*
     * DESC : to find jobs according to candidate location
     * */
    static function getGeoNear($request)
    {
        $max_dstance = ($request->has('max_distance')) ? $request->input('max_distance') * 1000 : 100000;
        $geoNear = array(
            '$geoNear' => array(
                'near' => array(
                    "type" => "Point",
                    "coordinates" => $request->input('coordinates')
                ),
                "spherical" => true,
                "distanceField" => "dis",
                "distanceMultiplier" => 0.001,
                "maxDistance" => $max_dstance,
                "num" => 5000
            )
        );
        $project = array('$project' => array('_id' => true, 'title' => true));
        $date = new DateTime();
        $mdate = new MongoDate($date->getTimestamp() * 1000);

        $match = array('$match' =>
            array(
            'start_date' => array('$gte' => $mdate),
						//'end_date'=>array('$lte'=>$mdate),

        ));
        $query1 = array($geoNear, $match, $project);
        $cursor = DB::collection('jobs')->raw(function ($collection) use ($query1) {
            return $collection->aggregate($query1);
        });

        $res = [];
        foreach ($cursor as $key => $doc) {
            $res[$key] = $doc;

        }

        return $res;
    }
    
        
    /*=============================================
    Function for getting Mannual and Automatic jobs
    ===============================================*/
    public function getMannualAutomaticJobs(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'start' => 'required',
                'end' => 'required',
                'catId' => 'exists:category,_id'
            ]
        );
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
			
			//~ $nearByJobs = self::getGeoNear($request);
            $res = [];
			//~ foreach($nearByJobs as $key=>$doc){
				//~ $res[$key]= $doc->_id;
			//~ }	

            $user = JWTAuth::parseToken()->authenticate();
            //candidate categories 
            $category_ids = [];
            if (is_array($user->category_id)) {
                $category_ids = $user->category_id;
            } else {
                $category_ids = [$user->category_id];
            }

            //check jobs applied by the user
            $applied_job_ids = JobsapplicationModel::where(array('jobseeker_id' => $user->_id))->pluck('job_id')->toArray();
            //current date
            $date = Carbon::now();
            //->where('job_published_type','!=',2)->whereIn('_id',$res)

            $jobobject = JobsModel::orderBy('start_date', 'asc')
                ->where(array('is_deleted' => false))
                ->where('start_date', '>=', $date)
                ->where('end_date', '>=', $date)
                ->whereIn('job_status', [0, 1])
                ->whereNotIn('_id', $applied_job_ids)
                ->whereIn('job_category', $category_ids);
            
           // $jobobject = JobsModel::orderBy('start_date','asc')->where(array('is_deleted'=>false ) )->where('end_date','>=',$date)->whereRaw(array('$where' => 'this.total_hired < this.number_of_worker_required'))->whereNotIn('_id',$applied_job_ids)->where('job_published_type','!=',2);

            if ($request->has('catId')) {
                $jobobject->where('job_category', $request->input('catId'));
            }
            
            //to return jobs according to user's current location
            if (!empty($res)) {
                $jobobject->whereIn('_id', $res);
            }

            $finalObject = $jobobject->with([
                'jobshifts',
                'jobapplied' => function ($r) use ($user) {
                    $r->where('jobseeker_id', $user->_id);
                }
            ])
                ->take(intval($request->Input('end')))
                ->skip(intval($request->Input('start')));

            //Assigning simple jobs here
            $response['response'] = $finalObject->get()->toArray();

            //Assigning offered jobs here            
            $response['offered_jobs'] = JobsapplicationModel::where(array('jobseeker_id' => $user->_id, 'job_type' => 2))
                ->whereIn('job_status', [0])
                ->with([
                    'job' => function ($q) use ($date) {
                        $q->where('start_date', '>=', $date)->where('end_date', '>=', $date)->whereIn('job_status', [0, 1]);
                    },
                    'job_shift'
                ])->get()
                ->toArray();

            $http_status = 200;
            $response['status'] = 1;
        }

        return response()->json($response, $http_status);
    }

    /*============================================
    Function for getting Applied and Acceptedjobs
    ==============================================*/
    public function getAppliedAcceptedJobs(Request $request)
    {
        $validator = Validator::make($request->all(), ['start' => 'required', 'end' => 'required', ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            $user = JWTAuth::parseToken()->authenticate();
            $date = Carbon::now();
			//applied job
            $response['applied'] = JobsapplicationModel::where(array('jobseeker_id' => $user->_id))->with(['job' =>
                function ($q) use ($date) {
                $q->where('is_deleted', false)->where('end_date', '>=', $date)->get();
            }, 'job_shift'])->where('job_status', 0)->whereIn('job_type', [0, 1])->get();
			//accepted job
            $response['accepted'] = JobsapplicationModel::where(array('jobseeker_id' => $user->_id))->with(['job' => function ($p) use ($date) {
                $p->where('is_deleted', false)->where('end_date', '>=', $date)->get();
            }, 'job_shift'])->whereIn('job_status', [1, 2])->get();
            $response['status'] = 1;
        }
        $http_status = 200;
        return response()->json($response, $http_status);
    }

    /*=======================================
    Function for getting category for search
    =========================================*/
    public function getCategoriesForSearch()
    {
        if ($response['response'] = CategoryModel::where(array('is_deleted' => false, 'type' => 'category', 'status' => true))->get()) {
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['errors'] = 'Something went wrong';
            $response['status'] = 0;
            $http_status = 400;
        }
        return response()->json($response, $http_status);
    }
    
    

    /*===========================================
    Function for searching category to apply job
    =============================================*/
    public function postJobsByCategory(Request $request)
    {
        //~ $catId = $request->input('catId');
        //~ $user_ids = User::where(array( 'role' => 3, 'approved' => 1 ))->pluck('_id')->toArray();
        //~ $data=JobsModel::with('userjobs')->whereIn('user_id',$user_ids)->where(array('is_deleted'=>false, 'job_category' => $catId ) )->whereIn('job_status' ,[0,1])->get()->toArray();

        $user = JWTAuth::parseToken()->authenticate();
        //check jobs applied by the user
        $applied_job_ids = JobsapplicationModel::where(array('jobseeker_id' => $user->_id))->pluck('job_id')->toArray();
		//current date
        $date = Carbon::now();
        $jobobject = JobsModel::orderBy('start_date', 'asc')->where(array('is_deleted' => false))->where('start_date', '>=', $date)->where('end_date', '>=', $date)->whereIn('job_status', [0, 1])->whereNotIn('_id', $applied_job_ids);
        if ($request->has('catId')) {
            $jobobject->where('job_category', $request->input('catId'));
        }
        $finalObject = $jobobject->with(array('jobshifts', 'jobapplied' => function ($r) use ($user) {
            $r->where('jobseeker_id', $user->_id);
        }))->take(intval($request->Input('end')))->skip(intval($request->Input('start')));
		//Assigning simple jobs here
        $data = $finalObject->get()->toArray();

        if (count($data)) {
            $response['response'] = $data;
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['response'] = $data;
            $response['status'] = 1;
            $http_status = 200;
        }
        return response()->json($response, $http_status);
    }

    /*==============================
    Function for getting job detail
    ================================*/
    public function getJobDetail(Request $request)
    {
        $id = $request['id'];
        $user_ids = User::where(array('approved' => 1, 'role' => 3))->pluck('_id')->toArray();
        $data = JobsModel::with(['userjobs', 'jobshifts'])->whereIn('user_id', $user_ids)->where(array('is_deleted' => false, '_id' => $id))->first();
        if ($data) {
            $jobApp = JobsapplicationModel::where(['job_id' => $id, 'jobseeker_id' => JWTAuth::parseToken()->authenticate()->_id]);
            if ($jobApp->count()) {
                $appData = $jobApp->select('job_status', '_id', 'job_type')->first();
                $data['job_app_id'] = $job_app_id = $appData->_id;
                $data['job_type'] = $job_app_id = $appData->job_type;

                if (!empty($data['job_app_id'])) {
                    $data['job_app_status'] = $appData->job_status;
                }
				
				//check if job is rehire type job, then send invited user's ids.
                if (JobsModel::where('_id', $id)->value('job_published_type') == 2) {
                    $data['rehire_user_ids'] = JobsapplicationModel::where(['job_id' => $id])->select('jobseeker_id', 'job_status')->get()->toArray();
                }
            }
            $response['response'] = [$data];
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['errors'] = 'No Job Found';
            $response['status'] = 0;
            $http_status = 400;
        }
        return response()->json($response, $http_status);
    }
    
    /*===============================================
    Function for applying Mannual and Automatic Jobs
     * updated : 30 aug 2017, shivani - to check jobseeker availablity
     * updated : 22 dec 2017, shivani - to allow employes to be able to apply to a rehire job
    ================================================*/
    public function postApplyJob(Request $request, CommonRepositary $common)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'jobId' => 'required|exists:jobs,_id|is_approved|is_jobstarted|rating|filled_custom|deleted|email_confirmed|applied|'
            ]
        );
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            date_default_timezone_set($request->header('app_timezone'));
            //check jobseeker availablity
            $jobseekrAvailablity = $common->check_jobseeker_availablity(JWTAuth::parseToken()->authenticate()->_id, $request->input('jobId'));
            if ($jobseekrAvailablity['available'] == false) {
                return response()->json(['errors' => "You cannot apply to this job as you have already been accepted to another job. Please see your Schedule for details.", 'status' => 0]);
            } else {
				//check job details, updated by shivani - to track if jobseeker is working for same employer or not and to check week dates according to that particular job.
                $job_detail = JobsModel::where('_id', $request->input('jobId'))->first();
                $jobseeker_hours = $common->getWorkingHourOfJobseeker($job_detail->start_date, $job_detail->user_id, JWTAuth::parseToken()->authenticate()->_id);
                $week_hour = $jobseeker_hours + $common->getHourOfJobBeingApplied($request->input('jobId'));

                if ($jobseeker_hours > 0 && $week_hour >= 44) {
                    return response()->json(['errors' => "Sorry, the maximum working hours for an employee has already been reached.", 'status' => 0]);
                }

                $data['jobseeker_id'] = JWTAuth::parseToken()->authenticate()->_id;
                $data['job_id'] = $request->input('jobId');
				//$data['applied_date'] 	= Carbon::parse($request->header('current_date'));
                $data['applied_date'] = Carbon::parse($job_detail->start_date);
                $data['employer_id'] = $job_detail->user_id;
                $data['job_type'] = ($job_detail->job_published_type == 2) ? 0 : intval($job_detail->job_published_type);
                $data['is_viewed'] = false;
                $data['is_deleted'] = false;
                $data['is_applied'] = true;
                $data['job_status'] = 0;
                $data['rating'] = false;
				//if job is automatic and jobseekers skills matched, then it will be auto accepted - shivani (23 aug 2017)			 
                $userSkill = [];
                foreach (JWTAuth::parseToken()->authenticate()->skills as $skl) {
                    $userSkill[] = $skl['_id'];
                }
				//$match_skills = array_diff($userSkill,$job_detail->skills);
                $notify = false;
				//@array_intersect('search_parameter','other array from which first one will be searched')
				//if job type = rehire and rehire_type = automatic (method to hire the remaining employees)
                if (count(array_intersect($job_detail->skills, $userSkill)) == count($job_detail->skills)) {
                    if ($data['job_type'] == 1 || ($job_detail->job_published_type == 2 && $job_detail->rehire_type == 1)) {
                        $data['job_status'] = 1;
                    }

                    $notify = true;
                } else {
                    if ($data['job_type'] == 1 || ($job_detail->job_published_type == 2 && $job_detail->rehire_type == 1)) {
                        return response()->json(['errors' => "Sorry! you do not meet the job requirements.", 'status' => 0]);
                    }
                }
				//==========================================================
                $job = new JobsapplicationModel($data);
                if ($job->save()) {

                    JobsModel::where('_id', $request->input('jobId'))->increment('total_applied', 1);
					//if job is automatic and jobseeker's skills has been matched then increment total hired value for the job.
                    if ($data['job_status'] == 1) {
                        JobsModel::where('_id', $request->input('jobId'))->increment('total_hired', 1);
						//save applicant shifts
                        $common->save_app_shifts($job->_id, $request->input('jobId'));
                    }

                    $adminData = $common->getAdminDetail();
                    $title = JWTAuth::parseToken()->authenticate()->first_name . " " . JWTAuth::parseToken()->authenticate()->last_name . " has applied on " . $job_detail->title . ". Please take further action";	
					//send notification to employer
                    if ($data['job_type'] == 1) {
						//automatic job - skills matched.
                        if ($notify == true) {
                            $title = JWTAuth::parseToken()->authenticate()->first_name . " " . JWTAuth::parseToken()->authenticate()->last_name . " has been hired on " . $job_detail->title . ". Click here to check details";
                            $common->saveNotification($from = JWTAuth::parseToken()->authenticate()->_id, $to = $job_detail->user_id, $type = 2, $target = 10, $target_id = $job_detail->_id, $title);
							//save temporary entry to send notification before 12 and 1 hour of job start time
                            $start_date = Carbon::parse($job_detail->start_date);
                            $befor12_hour = $start_date->subHours(12)->parse($start_date);
                            $start_date2 = Carbon::parse($job_detail->start_date);
                            $befor1_hour = $start_date2->subHours(1)->parse($start_date2);
                            $common->notify_before_twelve_hour($job_detail->_id, $befor12_hour, $befor1_hour, JWTAuth::parseToken()->authenticate()->_id);
                        }

                    } else {
						//this notification will work for all job type except automatic job - notification will be sent to only those candidated whose skills match
                        $common->saveNotification($from = JWTAuth::parseToken()->authenticate()->_id, $to = $job_detail->user_id, $type = 2, $target = 10, $target_id = $job_detail->_id, $title);
                    }

                    $response['message'] = "Successfully Applied to the Job";
                    $response['skills_matched'] = 0;
                    $response['status'] = 1;
                    $http_status = 200;
                } else {
                    $response['errors'] = "Something went wrong";
                    $response['status'] = 0;
                    $http_status = 200;
                }

            }
        }
        return response()->json($response, $http_status);
    }
    
    
    

    /*=================================================
    Function for Accept Offered Jobs in case of Rehire
     * updated at : 1 sep 2017 shivani - to notify employer
    ===================================================*/
    public function postAcceptJob(Request $request, CommonRepositary $common)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'jobId' => 'required|exists:jobs,_id|is_approved|is_jobstarted|rating|filled_custom|deleted',
                'application_id' => 'required|exists:job_application,_id'
            ],
            ['application_id.exists' => 'Sorry, but you have been removed from the job by employer']
        );
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            date_default_timezone_set($request->header('app_timezone'));
				
            //check jobseeker availablity
            $jobseekrAvailablity = $common->check_jobseeker_availablity(JWTAuth::parseToken()->authenticate()->_id, $request->input('jobId'));
            if ($jobseekrAvailablity['available'] == false) {
                return response()->json(['errors' => "Sorry! You have already accepted another job for this time slot.", 'status' => 0]);
            } else {
				//check job details, updated by shivani - to track if jobseeker is working for same employer or not and to check week dates according to that particular job.
                $jobDetail = JobsModel::where('_id', $request->input('jobId'))->first();
                $jobseeker_hours = $common->getWorkingHourOfJobseeker($jobDetail->start_date, $jobDetail->user_id, JWTAuth::parseToken()->authenticate()->_id);

                $week_hour = $jobseeker_hours + $common->getHourOfJobBeingApplied($request->input('jobId'));
                if ($jobseeker_hours > 0 && $week_hour >= 44) {
                    return response()->json(['errors' => "Sorry, the maximum working hours for an employee has already been reached.", 'status' => 0]);
                }
                $job_appl = JobsapplicationModel::where('_id', $request->input('application_id'))->first();
                $job_appl->is_viewed = false;
                $job_appl->is_applied = true;
                $job_appl->job_status = 2;
				  // $job_appl->applied_date=Carbon::parse($request->header('current_date'));

                if ($job_appl->save()) {
					//save applicant shift details @application_id, @job_id
                    $common->save_app_shifts($request->input('application_id'), $request->input('jobId'));
                    JobsModel::where('_id', $request->input('jobId'))->increment('total_hired', 1);					
					//notify employer====== 1 sep 2017
                    $job_detail = JobsModel::where('_id', $request->input('jobId'))->first();
                    $adminData = $common->getAdminDetail();
                    $title = JWTAuth::parseToken()->authenticate()->first_name . " " . JWTAuth::parseToken()->authenticate()->last_name . " has accepted the invitation for " . $job_detail->title . " job";
                    $common->saveNotification($from = JWTAuth::parseToken()->authenticate()->_id, $to = $job_detail->user_id, $type = 2, $target = 10, $target_id = $job_detail->_id, $title);
					//================================
					
					
					//save temporary entry to send notification before 12 and 1 hour of job start time
                    $start_date = Carbon::parse($job_detail->start_date);
                    $befor12_hour = $start_date->subHours(12)->parse($start_date);
                    $start_date2 = Carbon::parse($job_detail->start_date);
                    $befor1_hour = $start_date2->subHours(1)->parse($start_date2);
                    $common->notify_before_twelve_hour($job_appl->job_id, $befor12_hour, $befor1_hour, JWTAuth::parseToken()->authenticate()->_id);


                    $response['message'] = "Job Accepted successfully";
                    $response['status'] = 1;
                    $http_status = 200;
                } else {
                    $response['errors'] = "Something went wrong";
                    $response['status'] = 0;
                    $http_status = 400;
                }
            }
        }

        return response()->json($response, $http_status);
    }

    /*============================================
    Function for Deline Offered Jobs by jobseeker
    ===============================================*/
    public function postDeclineJob(Request $request, CommonRepositary $common)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'jobId' => 'required|exists:jobs,_id|is_approved|is_jobstarted|rating|filled_custom|deleted',
                'application_id' => 'required|exists:job_application,_id'
            ],
            ['application_id.exists' => 'Sorry, but you have been removed from the job by employer']
        );
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            if (JobsapplicationModel::where('_id', $request->input('application_id'))->update(array('job_status' => 3))) {
                $jobshifts = JobsapplicationModel::where('_id', $request->input('application_id'))->with(['job_detail' => function ($e) {
                    $e->select('start_date', 'title', '_id', 'user_id')->first();
                }])->first();	
					
				//update total_applied count for the job
                $job_id = JobsapplicationModel::where('_id', $request->input('application_id'))->value('job_id');
                $total_applied = JobsModel::where('_id', $job_id)->value('total_applied');
                $applied_count = intval($total_applied - 1);
                if ($applied_count < 0)
                    $applied_count = 0;

                JobsModel::where('_id', $job_id)->update(['total_applied' => $applied_count]);

                $title = JWTAuth::parseToken()->authenticate()->first_name . " " . JWTAuth::parseToken()->authenticate()->last_name . " has declined " . $jobshifts->job_detail->title . " job.";
                $common->saveNotification($from = JWTAuth::parseToken()->authenticate()->_id, $to = $jobshifts->job_detail->user_id, $type = 2, $target = 10, $target_id = $jobshifts->job_detail->_id, $title);
                $response['message'] = "Job declined successfully";
                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['errors'] = "Something went wrong";
                $response['status'] = 0;
                $http_status = 200;
            }
        }
        return response()->json($response, $http_status);
    }
        
    

    /*=======================
    Function for Cancel Jobs
    updated : 11 aug 2017, shivani - to update total hours worked
    =========================*/
    public function postCancelJob(Request $request, CommonRepositary $common)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'application_id' => 'required|exists:job_application,_id'
            ],
            ['application_id.exists' => 'Sorry, but you have been removed from the job by employer']
        );
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            //calculate total hours worked before cancel
            $workMinutes = $workHours = 0;
            $jobshifts = JobsapplicationModel::where('_id', $request->input('application_id'))->with([
                'job_detail' => function ($e) {
                    $e->select('start_date', 'title', '_id', 'user_id')->first();
                },
                'job_shift' => function ($q) {
                    $q->where('start_date', '<=', Carbon::now());
                }
            ])->first();
            $jobApplication_data = JobsapplicationModel::where('_id', $request->input('application_id'))->first();
            $jobApplication_data->withdraw_date = Carbon::now();
            $jobApplication_data->job_status = 5;
            /*********** end 11 aug 2017 **********/
            if ($jobApplication_data->save()) {	
				//update hours
				//if jobshifts found, then calculate hours worked upto now
                $app_hours = $common->calculate_job_app_hours($request->input('application_id'));
				//Quit an On-Going Job (If current time is greater than job's start time)
                if (Carbon::now() > Carbon::parse($jobshifts->job_detail->start_date)) {

                    $rating = Setting::where('alias', 'quit_job')->value('value');

                } else {
					//update total hired value for the job, so another candidate can get hired for the job
                    $total_hired = JobsModel::where('_id', $jobshifts->job_id)->value('total_hired');
                    $total_hired -= 1;
                    if ($total_hired < 0)
                        $total_hired = 0;

                    JobsModel::where('_id', $jobshifts->job_id)->update(['total_hired' => $total_hired, 'job_status' => 1]);	
					
					
					//Get difference between current time and job start time in hours
                    $hours = $common->check_difference_in_hours($jobshifts->job_detail->start_date);

                    switch ($hours) {
						//After 48 hours from the start time
                        case ($hours > 48):
                            $rating = Setting::where('alias', 'withdraw_before_48hours')->value('value');
                            break;
						//25 hours ≤ Start Time ≤ 48 hours
                        case ($hours > 25 && $hours <= 48):
                            $rating = Setting::where('alias', 'withdraw_between_48to25hours')->value('value');
                            break;
						//12 hours ≤ Start Time ≤ 24 hours
                        case ($hours > 12 && $hours <= 25):
                            $rating = Setting::where('alias', 'withdraw_between_24to12hours')->value('value');
                            break;
						//12 hours >= Start Time
                        case ($hours <= 12):
                            $rating = Setting::where('alias', 'withdraw_after_12hours')->value('value');
                            break;
                        default:
                            break;
                    }
                }
				
				//save details to user_ratings
                $save_user_rating = UserRatings::create(['type' => 'jobseeker', 'type_id' => JWTAuth::parseToken()->authenticate()->_id, 'points' => $rating]);
                if ($save_user_rating) {
					//Update avg rating of user
                    $common->updateRating($rating, JWTAuth::parseToken()->authenticate()->_id);
                }
				//notify employer ===== 12 sep 2017
                $adminData = $common->getAdminDetail();
                $title = JWTAuth::parseToken()->authenticate()->first_name . " " . JWTAuth::parseToken()->authenticate()->last_name . " has cancelled " . $jobshifts->job_detail->title . " job.";
                $common->saveNotification($from = JWTAuth::parseToken()->authenticate()->_id, $to = $jobshifts->job_detail->user_id, $type = 2, $target = 10, $target_id = $jobshifts->job_detail->_id, $title);
				//================
                $response['message'] = "Job cancelled successfully.";
                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['errors'] = "Something went wrong";
                $response['status'] = 0;
                $http_status = 200;
            }
        }
        return response()->json($response, $http_status);
    }

    /*=========================
    Function for View New Jobs 
    ==========================*/
    public function postViewNewJobs(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'jobId' => 'required|exists:job_application,_id'
            ]
        );
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            if (JobsapplicationModel::where(array('_id' => $request->input('jobId')))->update(array('is_viewed' => true))) {
                $response['message'] = 'success';
                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['message'] = 'Something went wrong';
                $response['status'] = 0;
                $http_status = 200;
            }
        }
        return response()->json($response, $http_status);
        /*$job_id = $request['jobId'];
        $user_ids = User::where(array( 'approved' => 1, 'role' => 3 ))->pluck('_id')->toArray();
        $job_detail=JobsModel::with('userjobs')->whereIn('user_id',$user_ids)->where(array( '_id' => $job_id ,'job_published_type'=>2 ) )->whereIn('job_status' ,[0,1,2,3])->first()->toArray();
        $data['job_id'] = $job_id;
        $data['new_job'] = false;
        
        if( $is_already_viewed > 0 ){
            $job_update = JobsapplicationModel::where( array( 'job_id' => $job_id, 'jobseeker_id' => JWTAuth::parseToken()->authenticate()->_id ) )->update( array('new_job' => false ));
        } else{
            $job = new JobsapplicationModel($data);
            $job_save = $job->save();
        }
        
        if( isset($job_save) || isset( $job_update ) ){
            $response['message'] = "Job is viewed";
            $response['status']=1;
            $http_status=200;
        } else {
            $response['errors']='Something went wrong';
            $response['status']=0;
            $http_status=400;
        }*/


    }


    /*================================
    Function for Getting Job Schedule 
    ==================================*/


    public function getJobSchedule(Request $request)
    {
        $end = intval($request->Input('end'));
        $start = intval($request->Input('start'));
        $jobs = JobsapplicationModel::where(array('jobseeker_id' => JWTAuth::parseToken()->authenticate()->_id))->whereIn('job_status', [1, 2])->get()->toArray();
        $job_ids = array();
        foreach ($jobs as $job) {
            $job_ids[] = $job['job_id']; // new \MongoDB\BSON\ObjectId($job['job_id']);
        }

        $job_data = JobsModel::with('userjobs')->with(['jobapplied', 'jobshifts'])->where(array('is_deleted' => false, 'publisher_status' => true))->whereIn('_id', $job_ids)->whereIn('job_status', [0, 1, 2, 3])->take($end)->skip($start)->get()->toArray();
        foreach ($job_data as $job) {
            $job_schedule[] = $job;
        }
        if (isset($job_schedule)) {
            $response['data'] = $job_schedule;
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['data'] = [];
               // $response['errors']='No job found';
            $response['status'] = 1;
            $http_status = 200;
        }

        return response()->json($response, $http_status);
    }

    /*===============================
    Function for Getting My Earnings 
    =================================*/
    public function getMyEarnings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_date' => 'required|date'
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            Carbon::setWeekStartsAt(Carbon::SUNDAY);
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            $current_date = $request->input('current_date');
            $date = Carbon::parse($current_date);
            $end = Carbon::parse($current_date);
            $start_time = $date->startOfWeek()->startOfDay();

            if ($response['data'] = JobsapplicationModel::with([
                'job_shift',
                'app_shift' => function ($q) use ($start_time, $end) {
                    $q->where('start_date', '>=', $start_time)->get();
                },
                'job'
            ])
                ->where(array('jobseeker_id' => JWTAuth::parseToken()->authenticate()->_id))
                ->where('total_hours_worked', '!=', null)
                ->where('total_hours_worked', '!=', 0)
                ->get()
                ->toArray()) {				
                //$response['data'] = $applied_job_detail;
                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['data'] = [];
               // $response['errors']='No job found';
                $response['status'] = 1;
                $http_status = 200;
            }
        }
        return response()->json($response);
    }
}
