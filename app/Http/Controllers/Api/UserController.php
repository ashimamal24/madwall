<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterHearAboutAndIdProof;
use App\Model\User;
use App\Model\ContactUs;
use Validator;
use JWTAuth;
use App\Http\Repositary\CommonRepositary;
use App\Model\EmailTemplate;
use App\Model\EmailVerification;
use App\Jobs\SendOtpEmail;
use App\Model\JobsapplicationModel;
use App\Model\JobsModel;
use App\Model\ShiftModel;
use App\Model\NotificationModel;
use App\Model\Setting;
use App\Model\EmergencyContact;
use App\Model\DirectDeposit;
use Carbon\Carbon;

class UserController extends Controller
{

    public function __construct()
    {

        //print_R('sdgdfg');die;
        $this->middleware('jwtcustom', ['except' => ['getHearAboutIdProof', 'getAppVersion']]);


    }

    /*=============================================================================================
    Function for get master hear about us and id proof data
     * updated : 26 april 2018, shivani debut Infotech
     * DESC : to return app version for android and ios both
    ===============================================================================================
     */
    public function getHearAboutIdProof()
    {
        if ($response['data']['hear_about_us'] = MasterHearAboutAndIdProof::where(array('status' => true, 'type' => 1))->get()) {
            $response['data']['id_proof'] = MasterHearAboutAndIdProof::where(array('status' => true, 'type' => 2))->get();

            $iosVersion = Setting::where('key', 'app_ios')->value('value');
            $androidVersion = Setting::where('key', 'app_android')->value('value');

            $response['app_ios'] = empty($iosVersion) ? null : $iosVersion;
            $response['app_android'] = empty($androidVersion) ? null : $androidVersion;
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['errors'] = 'No Data found!!!';
            $response['status'] = 0;
            $http_status = 200;
        }
        return response()->json($response, $http_status);
    }
    /*=============================================================================================
    Function for save general information
    ===============================================================================================
     */
    public function postGeneralInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
           
			//'address' 			=> 'required',
			//'sin_number' 		=> 'required',
			//'dob' 		        => 'required',
			//'source' 		    => 'required',
            'id_proofs' => 'required|array',
        ]);
        if ($validator->fails()) {

            $response['errors'] = $validator->errors();
            $http_status = 400;
            $response['status'] = 0;
        } else {

            $user = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
            $data = $request->all();
            //$data['coordinate'] = [$request->input('lng'), $request->input('lat')]; 
	    	//$data['dob']=\Carbon\Carbon::createFromFormat('d-m-Y',$request->input('dob'));
            $data['profile_complete'] = 4;
            if ($user && $user->update($data)) {
                $response['message'] = 'Info saved successfully.';
                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['message'] = 'No such user found.';
                $response['status'] = 0;
                $http_status = 200;
            }
        }
        return response()->json($response, $http_status);
    }


    /*=============================================================================================
    Function for get general information
    ===============================================================================================
     */
    public function getGeneralInfo()
    {
        $response['data'] = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
        $response['status'] = 1;
        return response()->json($response, 200);
    }

    /*=============================================================================================
    Function for get that user is approved or not
    ===============================================================================================
     */
    public function getApprovedInfo()
    {
        //$response['approved'] = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->value('approved');
        $user = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
        $response['profile_complete'] = $user['profile_complete'];
        $response['approved'] = $user['approved'];
        $response['status'] = 1;
        return response()->json($response, 200);
    }

    /*=============================================================================================
    Function for edit Profile
    ===============================================================================================
     */
    public function EditProfile(Request $request, CommonRepositary $common)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'phone_update' => 'unique:users,phone,' . JWTAuth::parseToken()->authenticate()->_id . ',_id',
                'email' => 'email|unique:users,email,' . JWTAuth::parseToken()->authenticate()->_id . ',_id'
            ],
            [
                'email.email' => 'Please fill valid email',
                'email.unique' => 'Sorry, this email is already taken',
                'phone_update.unique' => 'Sorry, this mobile number is already taken',
                'phone_update.required' => 'Mobile number is required'
            ]

        );
        if ($validator->fails()) {

            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {

            $user = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
            $user_ref = $user;
            $data = $request->all();
            if ($request->has('dob')) {
                $data['dob'] = \Carbon\Carbon::createFromFormat('d-m-Y', $request->input('dob'));
            }

            if ($request->has('email')) {
                $code = str_random(30);
                $emal_verification = new EmailVerification(array('code' => $code, 'status' => true, 'email' => $request->input('email')));
                if ($emal_verification->save()) {
                    $template = EmailTemplate::find('596866ddb098f0348a7995e7');
                    $link = '<a href=' . url('/email_verification/' . $code) . '>Click Here</a>';
                    $find = array('@name@', '@company@', '@link@');
                    $values = array($user['first_name'], env('MAIL_COMPANY'), $link);
                    $body = str_replace($find, $values, $template->content);
                    $this->dispatch(new SendOtpEmail($body, $request->input('email'), $template));
                }
            }

            if ($request->has('phone_update') && $request->has('country_code')) {
                $data['otp'] = $common->randomGenerator();
                $response['otp'] = $data['otp'];
                $common->sendText($request->input('country_code') . $request->input('phone_update'), 'Hello! Welcome to MadWall. Here is the code: ' . $data['otp'] . '. Please confirm it in the app. Thanks!');
            }

            $new_cv = false;
            if ($request->has('cv_url')) {
                if (JWTAuth::parseToken()->authenticate()->cv_url != $request->input('cv_url')) {
                    $data['new_cv'] = $request->input('cv_url');
                    $data['new_cv_name'] = $request->input('cv_name');
                    $data['cv_url'] = JWTAuth::parseToken()->authenticate()->cv_url;
                    $data['cv_name'] = JWTAuth::parseToken()->authenticate()->cv_name;
                    $new_cv = true;
                } else {
                    $data['cv_url'] = $request->input('cv_url');
                }
            } else {
                $data['cv_url'] = null;
                $data['cv_name'] = null;
            }

            if ($request->has('lat') && $request->has('lng')) {
                $data['coordinate'] = [floatval($request->input('lng')), floatval($request->input('lat'))];
            }


            if ($user->update($data)) {
                if ($new_cv == true) {
					//shivani - 14 oct 2017 to update notification text
                    $adminData = $common->getAdminDetail();
                    $common->saveNotification(JWTAuth::parseToken()->authenticate()->_id, $adminData->_id, '1', '12', null, JWTAuth::parseToken()->authenticate()->first_name . ' ' . JWTAuth::parseToken()->authenticate()->last_name . " has uploaded a new CV. Please click to review");
                }
                $response['message'] = 'Profile updated successfully';

                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['errors'] = 'Something went wrong';
                $response['status'] = 0;
                $http_status = 400;
            }
        }
        return response()->json($response, $http_status);
    }



    /*=============================================================================================
    Function for verify mobile if updated
    ===============================================================================================
     */
    public function updateMobile(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'phone_update' => 'required',
            'otp' => 'required',

        ]);
        if ($validator->fails()) {

            $response['errors'] = $validator->errors();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            $user = User::where(array("otp" => $request->input('otp')))->first();
            if ($user && $user->update(array('phone' => $request->input('phone_update')))) {

                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['errors'] = 'Invalid OTP';
                $response['status'] = 0;
                $http_status = 200;
            }
        }
        return response()->json($response, $http_status);
    }
    /*=============================================================================================
    Function for resend otp on profile screen
    ===============================================================================================
     */
    public function updateMobileOtp(Request $request, CommonRepositary $common)
    {
        $validator = Validator::make($request->all(), [

            'phone_update' => 'required',
            'country_code' => 'required',


        ]);
        if ($validator->fails()) {

            $response['errors'] = $validator->errors();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            $user = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
            $otp = $common->randomGenerator();
            $common->sendText($request->input('country_code') . $request->input('phone_update'), 'Hello! Welcome to MadWall. Here is the code: ' . $otp . '. Please confirm it in the app. Thanks!');
            if ($user && $user->update(array('otp' => $otp))) {
                $response['otp'] = $otp;
                $response['status'] = 1;
                $http_status = 200;
            } else {

                $response['status'] = 0;
                $http_status = 400;
            }
        }
        return response()->json($response, $http_status);
    }


    /*=============================================================================================
    Function for get user  Profile
    ===============================================================================================*/
    public function getProfile(Request $request)
    {
        if ($response['response'] = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first()) {
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['errors'] = 'Something went wrong';
            $response['status'] = 0;
            $http_status = 400;
        }
        return response()->json($response, $http_status);
    }
    
    /*=============================================================================================
    Function for get dashboard info
    ===============================================================================================
     */
    public function DashboardInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_date' => 'required|date'

        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            Carbon::setWeekStartsAt(Carbon::SUNDAY);
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            $user = JWTAuth::parseToken()->authenticate();
            $current_date = $request->input('current_date');
            $date = Carbon::parse($current_date);
            $end = Carbon::parse($current_date);
            $start_time = $date->startOfWeek()->startOfDay();
                
	            //$start_time 	= $date->startOfWeek()->subDays(1)->startOfDay();
	            // $job_application = JobsapplicationModel::with(array('job_shift'=>function($q) use($start_time,$end) { $q->where('start_date','>=',$start_time)->where('end_date','<=',$end->endofDay())->get();}, 'job'=>function($q) {
             //        $q->select(['_id','salary_per_hour']) ;              }))->where( array( 'jobseeker_id' => $user->_id,"job_status" => 1 ) )->get()->toArray();    

                //updated : 12 sep 2017, shivani  - to return earnings for completed jobs.  
            $complete_application = JobsapplicationModel::with(array('job_shift', 'app_shift' => function ($q) use ($start_time, $end) {
                $q->where('start_date', '>=', $start_time)->get();
            }, 'job'))->where(array('jobseeker_id' => JWTAuth::parseToken()->authenticate()->_id))->where('total_hours_worked', '!=', null)->where('total_hours_worked', '!=', 0)->get()->toArray();  

                //JobsapplicationModel::with(array('job'=>function($q) {$q->select(['_id','salary_per_hour']) ;}))->where( array('jobseeker_id'=>$user->_id) )->where('total_hours_worked','!=',null)->where('total_hours_worked','!=',0)->get()->toArray();	   

            foreach ($complete_application as $applications) {
                $salary = $applications['job']['salary_per_hour'];
                if (!empty($applications['app_shift'])) {
                    foreach ($applications['app_shift'] as $appShift) {
                        $total[] = !empty($appShift['total_hours_worked']) ? ($appShift['total_hours_worked'] * $salary) : 0;
                    }
                }

            }
            $response['earnings'] = isset($total) ? array_sum($total) : 0;
            $response['jobs_offered'] = JobsapplicationModel::where(array('jobseeker_id' => $user->_id))->where('job_type', 2)->whereIn('job_status', [0])->count();   
                //offered accepted count             
            $offered_count = JobsapplicationModel::where(array('jobseeker_id' => $user->_id))->where('job_type', 2)->whereNotIn('job_status', [0])->count();
            $response['applications'] = JobsapplicationModel::where(array('jobseeker_id' => $user->_id))->where('job_type', '!=', 2)->whereNotIn('job_status', [4, 5, 6, 7])->count() + $offered_count;
            $response['user'] = $user;
                //========= app versions for android and ios
            $iosVersion = Setting::where('key', 'app_ios')->value('value');
            $androidVersion = Setting::where('key', 'app_android')->value('value');

            $response['app_ios'] = empty($iosVersion) ? null : $iosVersion;
            $response['app_android'] = empty($androidVersion) ? null : $androidVersion;

            $response['status'] = 1;
            $http_status = 200;
        }



        return response()->json($response, $http_status);
    }
    /*=================================================================================
    function for contact us api
    ===================================================================================*/
    public function saveContactUs(Request $request, CommonRepositary $common)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'content' => 'required',
            'type' => 'required'

        ]);
        if ($validator->fails()) {

            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            $data = $request->all();
            $data['user_id'] = JWTAuth::parseToken()->authenticate()->_id;
            $data['email'] = JWTAuth::parseToken()->authenticate()->email;
            $data['name'] = JWTAuth::parseToken()->authenticate()->first_name . ' ' . JWTAuth::parseToken()->authenticate()->last_name;
            $data['is_deleted'] = false;
            $data['subject'] = $request->input('subject');
            $data['file_name'] = $request->input('file_name');
            $data['file_url'] = $request->input('file_url');
            $data['request_status'] = 'pending';
            $contactus = new ContactUs($data);
            if ($contactus->save()) {
                $response['message'] = "Message saved successfully";
                $common->saveNotification(JWTAuth::parseToken()->authenticate()->_id, '5910610142997575ee131321', '1', '3', null, JWTAuth::parseToken()->authenticate()->first_name . ' ' . JWTAuth::parseToken()->authenticate()->last_name . " has sent a contact request.");
                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['errors'] = "Something went wrong";
                $response['status'] = 0;
                $http_status = 400;
            }
        }
        return response()->json($response, $http_status);
    }
    /*=================================================================================
    enable disable notification
    ===================================================================================*/
    public function manageNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'notification' => 'required'


        ]);
        if ($validator->fails()) {

            $response['errors'] = $validator->errors()->first();
            $http_status = 400;
            $response['status'] = 0;
        } else {
            $data = $request->all();
            if (User::where(array('_id' => JWTAuth::parseToken()->authenticate()->_id))->update($data)) {
                $response['message'] = "Changes saved successfully";
                $response['status'] = 1;
                $http_status = 200;
            } else {
                $response['errors'] = "Something went wrong";
                $response['status'] = 0;
                $http_status = 400;
            }
        }
        return response()->json($response, $http_status);
    }
    /*=======================================================================================
    Function checking that tutoraila is watched or not by user
    =========================================================================================*/
    public function tutorialWatched()
    {
        if (User::where(array('_id' => JWTAuth::parseToken()->authenticate()->_id))->update(array('profile_complete' => 8))) {
            $response['message'] = 'Data updated successfully';
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['message'] = 'Something went wrong';
            $response['status'] = 0;
            $http_status = 400;
        }
        return response()->json($response, $http_status);
    }

    /*==============================
    Function for Email Verification 
    ================================*/
    public function postEmailVerification(Request $request)
    {
        $code = str_random(30);
        $jobseeker = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first()->toArray();
        $emal_verification = new EmailVerification(array('code' => $code, 'status' => true, 'email' => $jobseeker['email']));
        if ($emal_verification->save()) {
            $template = EmailTemplate::find('596866ddb098f0348a7995e7');
            $link = '<a href=' . url('/email_verification/' . $code) . '>Click Here</a>';
            $find = array('@name@', '@company@', '@link@');
            $values = array($jobseeker['first_name'], env('MAIL_COMPANY', 'MadWall'), $link);
            $body = str_replace($find, $values, $template->content);
            $this->dispatch(new SendOtpEmail($body, $jobseeker['email'], $template));
           // $this->dispatch(new SendOtpEmail($body,'debutinfo12@gmail.com',$template));
            $response['message'] = "Email sent successfully";
            $response['status'] = 1;
            $http_status = 200;
        } else {
            $response['errors'] = 'Something went wrong';
            $response['status'] = 0;
            $http_status = 400;
        }
        return response()->json($response, $http_status);
    }

    /*=====================================
    Function for Uploading other Documents 
     * updated : 14 oct 2017, shivani 
    =======================================*/
    public function uploadOtherDocuments(Request $request, CommonRepositary $common)
    {
        $jobseeker = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
        $document = array();
        if ($request->has('doc_name') && $request->has('doc_url')) {
            if ($jobseeker['other_documents']) {
                $other_documents = $jobseeker['other_documents'];
                $document['name'] = $request->input('doc_name');
                $document['url'] = $request->input('doc_url');
                $other_documents[] = $document;
            } else {
                $document['name'] = $request->input('doc_name');
                $document['url'] = $request->input('doc_url');
                $other_documents[] = $document;

            }
            if ($jobseeker->update(array('other_documents' => $other_documents))) {
                $response['message'] = "Documents uploaded successfully";
                //shivani - 14 oct 2017 to update notification text
                $adminData = $common->getAdminDetail();
                $common->saveNotification(JWTAuth::parseToken()->authenticate()->_id, $adminData->_id, '1', '1', null, JWTAuth::parseToken()->authenticate()->first_name . ' ' . JWTAuth::parseToken()->authenticate()->last_name . " has uploaded a new document. Click to review");
                $response['status'] = 1;
                $http_status = 200;
            }
        } else {
            $response['errors'] = 'Something went wrong';
            $response['status'] = 0;
            $http_status = 400;
        }
        return response()->json($response, $http_status);
    }

    /*=========================================
    Function for get earnings through reference 
    ===========================================*/
    public function getEarningWithReference(Request $request)
    {
        
        //check all user referred by current(logged) user
        $jobseekerApplication = User::where("reffered_by", JWTAuth::parseToken()->authenticate()->_id)->with(['jobseekerjobs' => function ($q) {
            $q->with('job_shift');
        }])->get()->toArray();

        $workMinutes = $earnedAmount = $workHours = 0;
        if (!empty($jobseekerApplication)) {
            //loop through all users and check job shift  hours completed
            foreach ($jobseekerApplication as $job) {
                //cancelled jobs will not consider in the following loop
                if (!empty($job['jobseekerjobs'])) {
                    //loop through all jobs found
                    foreach ($job['jobseekerjobs'] as $application) {
                        //if job application was cancelled in between, then check total hours worked :
                        if ($application['job_status'] == 5) {
                            $workHours += $application['total_hours_worked'];
                        }
                        //if job status is complete (4), calculate minutes completed.
                        if (!empty($application['job_shift']) && $application['job_status'] == 4 && $application['job_status'] != 5) {
                            foreach ($application['job_shift'] as $shift) {
                               //finally calculate total minutes worked.
                                $workMinutes += $shift['shift_time'];
                            }//end foreach (jobshift) 
                        }
                    }//endforeach (jobseeker jobs)
                }
                //if work minutes are greater than 0, then convert to hours
                if ($workMinutes > 0) {
                    $workHours += intval($workMinutes / 60);
                    if ($workHours >= 8) {
                        $earnedAmount += 20;
                    }
                    $workMinutes = $workHours = 0;
                }
            }
        }

        $response['data'] = $earnedAmount;
        $response['status'] = 1;
        $http_status = 200;
        return response()->json($response, $http_status);
    }
    
    /*
     * added : 23 april 2018, shivani Debut infotech
     * DESC : to check App version
     * */
    public function getAppVersion(Request $request)
    {
        $app_version = Setting::where('key', 'app_version_key')->value('value');
        if (!empty($app_version)) {
            $response['data'] = $app_version;
            $response['status'] = 1;
            $http_status = 200;
            return response()->json($response, $http_status);
        } else {
            $response['status'] = 0;
            $http_status = 200;
            return response()->json($response, $http_status);
        }
    }


     /*
     * added : 23 april 2018, Debut infotech
     * DESC : to check App version
     * Method Add emergency Contact
     * */
    public function emergency_contact(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'nameOne' => 'required',
            'relationshipOne' => 'required',
            'addressOne' => 'required',
            'CountryCodeOne' => 'required',
            'phoneNoOne' => 'required',
            
        ]);
   
        if ($validator->fails()) {
                
            $response['errors'] = $validator->errors();
            $http_status = 400;
            $response['status'] = 0;

        } else {

            $emergencyContacts = EmergencyContact::where(array("user_id" => JWTAuth::parseToken()->authenticate()->_id))->first();

            if($emergencyContacts){

                $emergencyContacts->update($request->all());
                $response['message'] = "Update Successfully.";    
            }else{

                $request['user_id'] =  JWTAuth::parseToken()->authenticate()->_id;
                $emergencyContact = new EmergencyContact($request->all());
                $emergencyContact->save();
                $response['message'] = "Saved Successfully.";
            }

            $user = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
            $data = array('emergency_contact'=>'2');
            $user->update($data);
             
            
            $response['status'] = 1;
            $http_status = 200;       
            
        }
        return response()->json($response, $http_status);
    }


     /*
     * added : 23 april 2018, Debut infotech
     * DESC : to check App version
     * Method Add direct deposit 
     * */
    public function direct_deposit(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'institutionNo' => 'required',
            'transitNo' => 'required',
            'accountNo' => 'required',

        ]);
   
        if ($validator->fails()) {
                
            $response['errors'] = $validator->errors();
            $http_status = 400;
            $response['status'] = 0;

        } else {

            $directDeposits = DirectDeposit::where(array("user_id" => JWTAuth::parseToken()->authenticate()->_id))->first();

            if($directDeposits){

                $directDeposits->update($request->all());
                $response['message'] = "Update Successfully."; 
                     
            }else{

                $request['user_id'] =  JWTAuth::parseToken()->authenticate()->_id;
                $directDeposit = new DirectDeposit($request->all());
                $directDeposit->save();
                $response['message'] = "Saved Successfully.";

            }

            $user = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
            $data = array('direct_deposit'=>'2');
            $user->update($data);
             
            
            $response['status'] = 1;
            $http_status = 200;

        }

        return response()->json($response, $http_status);
    }

    /*
     * added : 23 april 2018, shivani Debut infotech
     * DESC : to check App version
     * */
    public function getEmergencyContact()
    {
        $emergencyContact = EmergencyContact::where(array("user_id" => JWTAuth::parseToken()->authenticate()->_id))->first();

        if (!empty($emergencyContact)) {

            $response['data'] = $emergencyContact;

            if($emergencyContact['nameSecond'] == null){
                    $emergencyContact['nameSecond']="";
            }
            if($emergencyContact['relationshipSecond'] == null){
                $emergencyContact['relationshipSecond']="";
            }
            if($emergencyContact['addressSecond'] == null){
                $emergencyContact['addressSecond']="";
            }
            if($emergencyContact['phoneNoSecond'] == null){
                $emergencyContact['phoneNoSecond']="";
            }
             if($emergencyContact['CountryCodeSecond'] == null){
                $emergencyContact['CountryCodeSecond']="";
            }

        } else {
            $response['data'] = new \stdClass;
        }

        $response['status'] = 1;
        $http_status = 200;
        return response()->json($response, $http_status);
    }



/*
     * added : 23 april 2018, shivani Debut infotech
     * DESC : to check App version
     * */
    public function getDirectDeposit(Request $request)
    {
        $directDeposit = DirectDeposit::where(array("user_id" => JWTAuth::parseToken()->authenticate()->_id))->first();

        if (!empty($directDeposit)) {
            $response['data'] = $directDeposit;
        } else {
            $response['data'] = new \stdClass;
        }

       
        $response['status'] = 1;
        $http_status = 200;
        return response()->json($response, $http_status);
    }

    # updateIosPushKitToken 

    function updateIosPushKitToken(Request $request){

      /* $user = User::where(array("__id" => JWTAuth::parseToken()->authenticate()->_id))->first();

      // if($user){
          $user->update($request->all());
       //}else{
       //   Agreement::create($request->all());
     //  }
*/
      
  $user = User::where(array("_id" => JWTAuth::parseToken()->authenticate()->_id))->first();
    $data = array('pushkit_token'=>$request->input('pushkit_token'));
  $user->update($data);


    $response['status'] = 1;
    $http_status = 200;
    return response()->json($response, $http_status);

  }

}


 