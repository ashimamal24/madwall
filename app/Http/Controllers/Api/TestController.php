<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterHearAboutAndIdProof;
use App\Model\User;
use App\Model\TimeSlot;
use App\Model\Quiz;
use Validator;
use JWTAuth;
use App\Model\SendQuizNotification;//5 aug 2017, shivani
use App\Http\Repositary\CommonRepositary;//5 aug 2017, shivani

class TestController extends Controller
{
    public function notificationTest($type = 21, $job_id = null)
    {
		//$device_token = User::where( array('_id' => $user_id) )->value('device_token');
        //$device_token = 'cUMsXMR-xr0:APA91bFzpdbFCtWzrelf9Sh4yrq-FTL9BrvjAhEiaUTCgxL57cousiuRht8HyFss2xl44wDlXvnSfuxmkdIib9x5adwZH1sILCanQaJN5Ob0FAOIUWwxmqIRbbSfpVyZY75VAKTPmkZA';

        //Tarun Device
        $device_token = 'fzAi8cyzp7Y:APA91bGz1Nso6bksYrC6GCTp9hQ9qHWhNQdh22ijm5ekiUr-5jgoPOeZPAAe-Tu_NruuPACdpmBP3-c3HnIvrAdFB3ypanjS50nqkY4knhBZNU_2raM729HplEWYN2eChcj-ATG6oqgm';

        if ($device_token) {
            //notification message body
            $fields['data']['title'] = $msg['title'] = 'Madwall';
            $fields['to'] = $device_token;
            $fields['priority'] = 'high';
            $fields['data']['fcm_status'] = 'OK';
            $msg['sound'] = 'default';
            if ($type == 1) {//when jobseeker is approved by admin
                $msg['body'] = $fields['data']['body'] = "Your profile has been approved by MadWall.";
                $fields['notification'] = $msg;
            } else if ($type == 2) {//when jobseeker is declined by admin
                $msg['body'] = $fields['data']['body'] = "Your profile has been declined by MadWall.";
                $fields['notification'] = $msg;
            } else if ($type == 3) {//when jobseeker is blocked by admin
                $msg['body'] = $fields['data']['body'] = "Your profile has been blocked by MadWall.";
                $fields['notification'] = $msg;
            } else if ($type == 4) { //when time slots are assigned
                $msg['body'] = $fields['data']['body'] = "3 time slots have been assigned. Please select one of them.";
                $fields['notification'] = $msg;
            } else if ($type == 5) {
                //when jobseeker is rehired for a job
                $msg['body'] = $fields['data']['body'] = 'Hi! You have a new offered Job';
                $fields['data']['job_type'] = '1';
                $fields['data']['is_from_deeplink'] = true;
                $fields['data']['job_id'] = $job_id;
                $fields['notification'] = $msg;
            } else if ($type == 6) {
                //when jobseeker application is accepted by employer
                $msg['body'] = $fields['data']['body'] = 'Hi! your job application has been accepted by the employer';
                $fields['data']['job_type'] = '2';
                $fields['data']['is_from_deeplink'] = true;
                $fields['data']['job_id'] = $job_id;
                $fields['data']['sound'] = 'default';
                $fields['notification'] = $msg;
            } else if ($type == 21) {//when jobseeker is declined by admin
                $msg['body'] = $fields['data']['body'] = "opentok test notification";
                $fields['notification'] = $msg;
            } else {
                //send notification to jobseeker after 48 hours of failed health quiz attempt ($type == 7)
                $msg['body'] = $fields['data']['body'] = 'Hi, you can re-attempt to pass madwall health quiz';
                $fields['notification'] = $msg;
            }
            
            $fields['data']['notificationtype'] = $type;

            //send notification code
            $headers = array(
                'Authorization: key=AAAAvBXXWQY:APA91bEyUOlAZ8N0--HrsfumQJ98ryyLl2VfgmE-3DdftykJjDyQWAOLldovEeOgmg3a6sZNcOvJpG66SddmFT3E1zrjlAurQEY-ooydwUx3yJtgRGA4GsO90o1TLc-teDXYJARquKtp',
                'Content-Type: application/json'
            );
            #Send Reponse To FireBase Server    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            #Echo Result Of FireBase Server
            return $result;
        } else {
            //no device token found
            return ['success' => false, 'message' => 'No device token found'];
        }

    }


    public function test_quiz_notification()
    {
        $notification['to'] = JWTAuth::parseToken()->authenticate()->_id;
        $notification['notification_sent'] = false;
        SendQuizNotification::create($notification);
    }
	
	
	/*
     * Added on : 5 aug 2017, shivani - to send notification to jobseeker after 48 hours of failed health quiz attempt
     * */
    public function send_quiz_notification(CommonRepositary $common)
    {
        $notifications = SendQuizNotification::where('notification_sent', false)->get()->toArray();
        if (!empty($notifications)) {
            foreach ($notifications as $notify) {
				//send notification after 48 hours
                if ($notify['created_at'] <= \Carbon\Carbon::now()->subMinutes(10)) {
					//call send_notification from common repository
                    $notifyJobseeker = $common->send_notification($notify['to'], 7, $job_id = null);
                    if ($notifyJobseeker) {
						//update notification_sent flag 
                        SendQuizNotification::where("_id", $notify['_id'])->update(['notification_sent' => true]);
                    }
                }
            }//end foreach
        }
    }

}
