<?php

namespace App\Http\Controllers\Employer;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Model\NotificationModel;
use App\Model\JobsModel;
use App\Model\User;
use Flash;
use Auth;
use Request as AjaxRequest;
use DB;
use Cookie;
use Session;
use Carbon\Carbon;
use App\Http\Repositary\CommonRepositary;
use DateTime;
use DateTimeZone;
use Crypt;


/*
 * Added : 27 Nov 2017, Debut infotech
 * DESC : to write code for job history related functionalities. 
 * */

class NotificationController extends Controller
{
	//
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth', ['except' => ['postContactus']]);
		$this->middleware('backbutton', ['except' => ['postContactus']]);
		if (isset($_COOKIE['client_timezone'])) {
			date_default_timezone_set($_COOKIE['client_timezone']);
		}
	}


	public function getAllNotification()
	{
		$thrtyDay_earlier = Carbon::now()->subDays(30);
		$notification = NotificationModel::where(array('status' => true, 'to' => $this->auth->user()->_id))->where('created_at', '>=', $thrtyDay_earlier)->orderBy('created_at', 'DESC')->limit(50)->get();
		$not_count = count($notification);
		$content = view('employer.promo.ajaxnotification', compact('notification', 'not_count'))->render();
		$result = ['status' => true, 'content' => $content];
		return response()->json($result, 200);
	}

	public function postUpdateNotify(Request $request)
	{
		$data = $request->all();
		$notification = NotificationModel::where(array('status' => true, '_id' => $data['notify']))->first();
		if ($notification) {
			$url = '/';
			if ($notification->target == 4 || $notification->target == 5 || $notification->target == 6) {
				$url = 'employer/company-profile';
			} elseif ($notification->target == 10) {
				$url = 'employer/jobdetail/' . Crypt::encrypt($notification->target_id);
			} else {
				$url = '/';
			}
			NotificationModel::where(['_id' => $data['notify']])->update(['status' => false]);
			return response()->json(['status' => true, 'redirect_uri' => $url]);
		}
	}
}
