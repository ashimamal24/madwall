<?php

namespace App\Http\Controllers\Employer;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Model\JobsapplicationModel;
use App\Model\SkillModel;
use App\Model\ShiftModel;
use App\Model\JobsModel;
use App\Model\User;
use Flash;
use Auth;
use Request as AjaxRequest;
use DB;
use Cookie;
use Session;
use Carbon\Carbon;
use App\Model\EmailTemplate;
use App\Http\Repositary\CommonRepositary;
use App\Http\Repositary\EmployerRepositary;
use DateTime;
use DateTimeZone;
use Crypt;

class EmployeeController extends Controller
{

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth', ['except' => ['postContactus']]);
		$this->middleware('backbutton', ['except' => ['postContactus']]);
		if (isset($_COOKIE['client_timezone'])) {
			date_default_timezone_set($_COOKIE['client_timezone']);
		}
	}
    
    
    /*
	 * updated : 10 aug 2017, shivani - to check if jobseeker application can be accepted or declined (24 hours check)
	 * */
	public function exportEmployees(Request $request)
	{
		$data = $request->all();
		$name = '';
		$jobId = \Crypt::decrypt($data['jobvalue']);
		$now = Carbon::now();
        //only export the records whoes shift end time has been passed.
		$job_applied = JobsapplicationModel::with(['job_shift', 'app_shift' => function ($q) use ($now) {
			$q->where('end_date', '<', $now);
		}, 'applyjobuser' => function ($q) {
			$q->select('first_name', 'last_name', 'cv_url', 'rating', 'image', 'skills', 'additional_documents', '_id');
		}, 'job' => function ($q) {
			$q->select('start_date', 'total_hired', 'number_of_worker_required', 'title');
		}])->where(array('employer_id' => $this->auth->user()->_id, 'job_id' => $jobId, 'is_deleted' => false))->whereIn('job_status', [0, 1, 2, 5, 7, 4]);

		if (AjaxRequest::ajax()) {
			if ($data['job_name']) {
				//search user by first and last name both.
				$user_ids = User::where('first_name', 'like', $data['job_name'] . '%')->pluck('_id');
				if (count($user_ids) == 0) {
					$user_ids = User::where('last_name', 'like', $data['job_name'] . '%')->pluck('_id');
				}
				$job_applied->whereIn('jobseeker_id', $user_ids);
			}
		}
		$order = 'asc';
		$jobCount = $job_applied->count();
		$jobData = $job_applied->orderBy('_id', $order)->get();
		if (!empty($jobData->toArray())) {
			$filename = "uploads/EmployeeList.csv";
			$handle = fopen($filename, 'w');
			fputcsv($handle, array('Candidate Name', 'Job Name', 'Status', 'Total Hours Worked', 'Lunch Hours', 'Start Date', 'Start Time', 'End Date', 'End Time'));
			foreach ($jobData as $key => $value) {
				if (!empty($value->applyjobuser)) {
					$candidate_name = $value->applyjobuser->first_name . ' ' . $value->applyjobuser->last_name;
					$job_name = $value->job->title;
					$status = 'N/A';
					if ($value->job_status == 0) {
						$status = 'Pending';
					} else if ($value->job_status == 1 || $value->job_status == 2) {
						$status = 'Hired';
					} else if ($value->job_status == 5) {
						$status = 'Cancelled';
					} else if ($value->job_status == 7) {
						$status = 'Removed';
					} else {
						$status = 'Complete';
					}

					$totalHoursWorked = 0;
					if (!empty($value->total_hours_worked)) {
						$totalHoursWorked = $value->total_hours_worked;
					}

					$lunchHour = 0;
					$start_date = $end_date = 'N/A';
					$start_time = $end_time = 'N/A';

					if (!empty($value->app_shift->toArray())) {
						foreach ($value->app_shift as $shft) {
							$start_date = Carbon::parse($shft['start_date'])->format('d,M Y');
							$start_time = Carbon::parse($shft['start_date'])->format('H:i');
							$end_date = Carbon::parse($shft['end_date'])->format('d,M Y');
							$end_time = Carbon::parse($shft['end_date'])->format('H:i');
							$totalHoursWorked = $shft['total_hours_worked'];
							if (!empty($shft->lunch_hour)) {
								$lunchHour = $shft->lunch_hour;
							}
							fputcsv($handle, array($candidate_name, $job_name, $status, $totalHoursWorked, $lunchHour, $start_date, $start_time, $end_date, $end_time));
						}
					} else {
						foreach ($value->job_shift as $shft) {
							$start_date = Carbon::parse($shft['start_date'])->format('d,M Y');
							$start_time = Carbon::parse($shft['start_date'])->format('H:i');
							$end_date = Carbon::parse($shft['end_date'])->format('d,M Y');
							$end_time = Carbon::parse($shft['end_date'])->format('H:i');
							$totalHoursWorked = 0;
							fputcsv($handle, array($candidate_name, $job_name, $status, $totalHoursWorked, $lunchHour, $start_date, $start_time, $end_date, $end_time));
						}
						//fputcsv($handle, array($candidate_name, $job_name,$status,$totalHoursWorked,$start_date,$start_time,$end_date,$end_time));
					}



				}
			}

			fclose($handle);
			$headers = array(
				"Content-type" => "text/csv"
			);
			return response()->json(array('csv_url' => $filename, 'status' => true));
		} else {
			return response()->json(array('message' => 'Sorry, No results to export', 'status' => false));
		}
	}
	
	/*
	 * updated : 10 aug 2017, shivani - to check if jobseeker application can be accepted or declined (24 hours check)
	 * */
	public function postUserAppliedJob(Request $request)
	{
		$data = $request->all();
		$records = 10;
		$name = '';
		$jobId = \Crypt::decrypt($data['jobvalue']);
		//query updated
		$job_applied = JobsapplicationModel::with(['applyjobuser' => function ($q) {
			$q->select('first_name', 'last_name', 'cv_url', 'rating', 'image', 'skills', 'additional_documents', '_id');
		}, 'job' => function ($q) {
			$q->select('start_date', 'total_hired', 'number_of_worker_required');
		}, 'app_shift'])->where(array('employer_id' => $this->auth->user()->_id, 'job_id' => $jobId, 'is_deleted' => false))->whereIn('job_status', [0, 1, 2, 4, 5, 7]);

		if (AjaxRequest::ajax()) {
			if ($data['job_name']) {
				//search user by first and last name both.
				$user_ids = User::where('first_name', 'like', $data['job_name'] . '%')->pluck('_id');
				if (count($user_ids) == 0) {
					$user_ids = User::where('last_name', 'like', $data['job_name'] . '%')->pluck('_id');
				}
				$job_applied->whereIn('jobseeker_id', $user_ids);
			}
			if ($data['records']) {
				$records = intval($data['records']);
			}
		}
		$order = 'asc';
		$jobCount = $job_applied->count();
		$jobData = $job_applied->orderBy('_id', $order)->paginate($records);
		$jobData = $jobData->setPath('jobapplylisting');
		return view('employer.promo.ajaxjobuserlist', compact('jobData', 'jobCount', 'records'))->render();
	}


	public function postJobsUsers(Request $request)
	{
		$data = $request->all();
		$location = [];
		$jobId = \Crypt::decrypt($data['jobId']);
		$job_applied = JobsapplicationModel::with(['applyjobuser'])->where(array('employer_id' => $this->auth->user()->_id, 'job_id' => $jobId, 'is_deleted' => false))->first();

		if ($job_applied['applyjobuser']) {
			$location[] = $job_applied['applyjobuser']->first_name;
		}
		return $result = ['success' => true, 'location' => $location];
	}
}
