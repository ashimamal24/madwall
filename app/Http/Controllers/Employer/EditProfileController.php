<?php

namespace App\Http\Controllers\Employer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Model\CategoryModel;
use App\Model\Industry;
use App\Http\Requests\Employer\ChangeEmployerPassword;
use App\Http\Requests\Employer\ChangeEmployerEmail;//24 july 2017, shivani
use App\Http\Requests\UpdateProfileRequest;//24 july 2017, shivani
use App\Http\Requests\Employer\ChangeEmployerLogo;//24 july 2017, shivani
use App\Model\Documentation;//24 july 2017, shivani
use App\Jobs\SendOtpEmail;//24 july 2017, shivani
use App\Model\EmailTemplate;
use App\Model\User;
use Carbon\Carbon;
use Hash;
use Auth;
use Mail;
use App\Model\EmailVerification;


class EditProfileController extends Controller
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('auth');
        $this->middleware('backbutton');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*===============================
        Show The Form for Editing.
    ===============================*/
    public function index()
    {
        try {
            $active = "company-profile";
            $documetation = Documentation::All();
            return view('employer/profile/edit', compact('active', 'documetation'));
        } catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage(),
                'active' => 'dashboard'
            ];
            return view('errors.error', $result);
        }
    }

    /*=================================
        Load View for Change Password
      ================================*/
    public function ViewChangePassword()
    {
        try {
            $active = "company-profile";
            $documetation = Documentation::All();
            return view('employer/profile/change-password', compact('active', 'documetation'));
        } catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage(),
                'active' => 'dashboard'
            ];
            return view('errors.error', $result);
        }
    }

    /*====================================================
     * Change Employer Password
     * @param App\Http\Requests\Auth\ChangeAdminPassword;
     * Updated by : shivani, debut infotech
     * Date : 24 july 2017
    ====================================================*/
    public function ChangePassword(ChangeEmployerPassword $request)
    {
        try {
            $user = Auth::user();
            if (Hash::check($request->old_password, Auth::user()->password)) {
                if (Hash::check($request->password, Auth::user()->password)) {
					//old password and new password can not be same
                    return response()->json(['success' => false, 'message' => 'Sorry, Please choose different password']);
                } else {
                    $data['password'] = bcrypt($request->password);
                    //update password
                    if ($user->update($data)) {
                        return response()->json(['success' => true, 'message' => 'Password Updated successfully']);
                    } else {
                        return response()->json(['success' => false, 'message' => 'Sorry, Password can not be Changed']);
                    }
                }
            } else {
				//old password not correct
                return response()->json(['success' => false, 'message' => 'Please enter correct old password']);
            }
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
    
    
    /*
     * Added on : 24 july 2017
     * Added by : shivani, debut infotech
     * DESC : to change email (Company profile)
     * */
    public function ChangeEmail(ChangeEmployerEmail $request)
    {
        try {
            $user = Auth::user();
            if ($request->new_email == Auth::user()->email) {
				//old email and new email can not be same
                return response()->json(['success' => false, 'message' => 'Oops! looks like this is your current email id']);
            } else {
				//check email should be unique
                if (User::where('email', $request->new_email)->count()) {
                    return response()->json(['success' => false, 'message' => 'This email already exists. Please specify any other email address']);
                }
                $data['new_email'] = $request->new_email;
                $data['email_verification_code'] = str_random(30);
                $current_date = Carbon::now();
                $data['link_expire_at'] = $current_date->addMinutes(30)->parse($current_date);
				//update email
                if ($user->update($data)) {
					//send verification email to new email updated,
                    $template = EmailTemplate::find('59115c8bb098f07a8f7594c2');
                    $link = "<a style='text-decoration:none;' href='" . url('register/verify/' . $data['email_verification_code']) . "'>confirm account</a>";
                    $find = array('@name@', '@Company@', '@link@');
                    $values = array($user->first_name . ' ' . $user->last_name, env('MAIL_COMPANY'), $link);
                    $body = str_replace($find, $values, $template->content);

                    $subject = 'Madwall : email update request';
                    $email_user = $request->new_email;
					//new SendOtpEmail($body,$request->new_email,$template);
                    Mail::send('emails.verify', ['content' => $body], function ($message) use ($subject, $email_user) {


                        $message->to($email_user)
                            ->subject($subject);

                    });

                    return response()->json(['success' => true, 'message' => 'We have just sent you an verification link on your updated email.Please click on the link for the update process to complete.']);
                } else {
                    return response()->json(['success' => false, 'message' => 'Sorry, Email can not be Changed']);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
    
    /*
     * Added on : 26 july 2017
     * Added by : shivani, debut infotech
     * DESC : to change logo (Company logo)
     * */
    public function ChangeLogo(ChangeEmployerLogo $request)
    {
        try {
            $user = Auth::user();
            $data = $request->all();
			//update logo
            if ($user->update($data)) {
                return response()->json(['success' => true, 'message' => 'Company logo updated successfully']);
            } else {
                return response()->json(['success' => false, 'message' => 'Sorry, company logo can not be updated']);
            }
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
    
    /*
     * Added on : 25 july 2017
     * Added by : shivani, Debut infotech
     * DESC : to update company profile.
     * */
    public function UpdateProfile(UpdateProfileRequest $request)
    {
        try {
            $user = User::where("_id", Auth::user()->_id);

            if ($request->new_email == Auth::user()->email) {
				//old email and new email can not be same
                return response()->json(['success' => false, 'message' => 'Oops! looks like this is your current email id']);
            } else {
                $data = $request->all();
				//check user phone number is unique or not (18aug2017-shivani)
                if (User::where(['country_code' => $data['country_code'], 'phone' => $data['phone']])->where('_id', '!=', Auth::user()->_id)->count()) {
                    return response()->json(['phone' => 'Phone number already exists'], 422);
                }

                if ($request->has('industry_type') && count($data['industry_type']) && is_array($data['industry_type'])) {
                    $i = 1;
                    $data1 = [];
                    foreach ($data['industry_type'] as $k => $row) {
                        $data1[$i] = new \MongoDB\BSON\ObjectID($row);
                        $i += 1;
                    }
                    $data['industry_object'] = $data1;
                    $data['industry'] = Industry::whereIn('_id', $data['industry_type'])->get()->toArray();

                    $category_details = CategoryModel::where(array('status' => true, 'type' => 'category', 'is_deleted' => false))->whereIn('industry_id', $data['industry_type'])->select('name', '_id', 'industry_name')->get()->toArray();
                    $catDetail = [];
                    if (!empty($category_details)) {
                        foreach ($category_details as $catDtl) {
                            $catDetail[] = ['_id' => $catDtl['_id'], 'discount' => 0, 'extra' => 0, 'name' => $catDtl['name'], 'industry_name' => $catDtl['industry_name']];
                        }
                    }
                    $data['category_details'] = (!empty($catDetail)) ? $catDetail : null;
                }
                $data['location'] = $data['company_address'];
				//update email
                if ($user->update($data)) {
                    return response()->json(['success' => true, 'message' => 'Email Updated successfully']);
                } else {
                    return response()->json(['success' => false, 'message' => 'Sorry, please try again later']);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
    
    
    /*
     * Updated on : 25 july 2017
     * Updated by : shivani, Debut infotech
     * */
    public function ViewCompanyProfile()
    {
        $user = Auth::user()->toArray();


        $title = 'Company Profile';
       //check user's industry types selected
        $indstryIds = [];
        if (isset($user['industry']) && !empty($user['industry'])) {
            foreach ($user['industry'] as $indstry) {
                $indstryIds[] = $indstry['_id'];
            }
        }
        $industries = CategoryModel::where(array('status' => true, 'is_deleted' => false))->whereIn('industry_id', $indstryIds)->whereNotIn('type', ['subcategory'])->get()->toArray();

        usort($industries, function ($i, $j) {
            $a = $i['industry_name'];
            $b = $j['industry_name'];
            return strcmp($a, $b);
        });



        $industry = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();

        $active = "company-profile";
        $documetation = Documentation::All();
        return view('employer/profile/companyProfile', ['user' => $user, 'active' => $active, 'industries' => $industries, 'indstry' => $industry, 'selected_indstry' => $indstryIds, 'title' => $title, 'documetation' => $documetation]);
    }

    public function postVeficationLink(Request $request)
    {
        $id = Auth::user()->_id;
        $template = EmailTemplate::find('596866ddb098f0348a7995e7');
        $link = "<a style='text-decoration:none;' href='" . url('register/verify/' . Auth::user()->email_verification_code) . "'>confirm account</a>";
        $find = array('@name@', '@Company@', '@link@');
        $values = array(Auth::user()->first_name . ' ' . Auth::user()->last_name, env('MAIL_COMPANY'), $link);
        $body = str_replace($find, $values, $template->content);
        $this->dispatch(new SendOtpEmail($body, Auth::user()->email, $template));
        flash()->success('We have just sent you an email. Please click on the link in the email to verify your account.');
        $url = 'dashboard';
        $result = [
            'status' => true,
            'url' => $url
        ];
        return response()->json($result);
    }

}
