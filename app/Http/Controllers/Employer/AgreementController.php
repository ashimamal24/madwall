<?php

namespace App\Http\Controllers\Employer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Agreement;
use App\Model\User;
use PDF;
use App\Model\EmergencyContact;
use App\Model\DirectDeposit;

use Config;

use JWTAuth;

#Opentok
use OpenTok\OpenTok;
use OpenTok\Role;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\OutputMode;
use \Firebase\JWT\JWT;


use App\Http\Repositary\CommonRepositary;


use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;




class AgreementController extends Controller
{

#================================================================================#


  function vedio($id){

    $user = User::where(array("_id" => $id))->first();

    require('public/opentok/opentok.phar');

    $opentok_key = Config::get('app.opentok_key');
    $opentok_secret = Config::get('app.opentok_secret');

    #Create Opentok Connection
    $opentok = new OpenTok($opentok_key,$opentok_secret);
/*
    #Genrate Opentok SesiionId 
    $session = $opentok->createSession();     
    $sessionId  = $session->getSessionId();

    #Genrate Opentok Token
    $token = $opentok->generateToken($sessionId,array('role' => Role::PUBLISHER));
*/


     $opentok_key = '46446122';
    $sessionId = "1_MX40NjQ0NjEyMn5-MTU3NDMxNTMyNTQ4MH5rNzROa0xnN1NJRFVISUNRYUlBV2c0Yll-QX4";
    $token = "T1==cGFydG5lcl9pZD00NjQ0NjEyMiZzaWc9OTA0NTEwNmFlOWEwY2E3MDg2YTg1NjY1YTcwMjlhN2VjM2Q4YmM5NzpzZXNzaW9uX2lkPTFfTVg0ME5qUTBOakV5TW41LU1UVTNORE14TlRNeU5UUTRNSDVyTnpST2EweG5OMU5KUkZWSVNVTlJZVWxCVjJjMFlsbC1RWDQmY3JlYXRlX3RpbWU9MTU3NDMxNTMyNSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNTc0MzE1MzI1LjUxMzQxNDYxNzYxNTQ5JmV4cGlyZV90aW1lPTE1NzQ5MjAxMjUmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0=";

//print_R(url('/'));die;
    $data = array(
      'apiKey'=>$opentok_key,
      'sessionId'=>$sessionId,
      'token'=>$token,
      'name'=>ucwords($user->first_name.' '.$user->last_name),
      'emp_id'=>$user->_id,
    );


    return view('employe/vedio', ['data' => $data]);

  }

#================================================================================#


  function opentok($id, CommonRepositary $common){

    $user = User::where(array("_id" => $id))->first();

    require('public/opentok/opentok.phar');


    # Fatch api key and secret
    #==========================
    $opentok_key = Config::get('app.opentok_key');
    $opentok_secret = Config::get('app.opentok_secret');



    #Create Opentok Connection
    #==========================
    $opentok = new OpenTok($opentok_key,$opentok_secret);



    #Genrate Opentok SesiionId 
    #==========================
    $sessionOptions = array(
      'archiveMode' => ArchiveMode::ALWAYS,
      'mediaMode' => MediaMode::ROUTED
    );
    $session = $opentok->createSession($sessionOptions);
    $sessionId  = $session->getSessionId();



    #Genrate Opentok Token
    #=====================
    $token = $opentok->generateToken($sessionId, array('role' => Role::PUBLISHER, 'expireTime' => time()+(7 * 24 * 60 * 60)));

    /*
   $opentok_key = '46446122';
    $sessionId = "1_MX40NjQ0NjEyMn5-MTU3NTM1NDk5NTg5MH5RZkNoK1JoVUJOWEw5Smdybi9tcjltMjF-QX4";
    $token = "T1==cGFydG5lcl9pZD00NjQ0NjEyMiZzaWc9ZDQ3Zjg4YmE0YTRkMjMyYzMwMTBhODk5ZjZkZGM2OWY1OTg2NDc5YjpzZXNzaW9uX2lkPTFfTVg0ME5qUTBOakV5TW41LU1UVTNOVE0xTkRrNU5UZzVNSDVSWmtOb0sxSm9WVUpPV0V3NVNtZHliaTl0Y2psdE1qRi1RWDQmY3JlYXRlX3RpbWU9MTU3NTM1NDk5NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNTc1MzU0OTk2LjAzNjI0ODQwMTEyMDAmZXhwaXJlX3RpbWU9MTU3NTk1OTc5NiZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==";
    */



    # Save Opentok SessionId in database
    #===================================
    $data = array('opentokSessionId'=>$sessionId);
    $user->update($data);

    //$sessionId ='2_MX40NjQ0NjEyMn5-MTU3NDgzNjc4NTAxMH53Y2EveG14T2ZPb1FKU0NLV1NNMkJJSlR-QX4';

    #Download Opentok Video
    #=====================
    //AgreementController::downloadOpentokVideo($opentok_key,$opentok_secret,$sessionId);



    #Send Notification
    #=====================
    $message = array(
        'message'=>'success',
        'username'=>'MadWall',
        'appkey'=>$opentok_key,
        'sessionId'=>$sessionId,
        'token'=>$token,
    );
     
   if($user['device_type'] == 'iOS'){
        $deviceIds = $user['pushkit_token'];
        AgreementController::iphone_send_notification($deviceIds,$message);
    }
  

    if($user['device_type'] == 'android'){
        $common->send_notifications($id, $type = 21, $message); 
    }



    # Create SIP User
    #======================
    /*$sipUsername ='testing23';
    $sipEmail ='testing1@gmail.com';
    $sipPassword ='aA@welcome01';
    $creareSipUser = AgreementController::creareSipUser($sipUsername,$sipEmail,$sipPassword);*/
    //print_r($creareSipUser);die;



    # Initiating a SIP call
    #======================
    //$initiatingSipCall = AgreementController::InitiatingSipCall($opentok_key,$opentok_secret,$sessionId,$token);
    //print_r($initiatingSipCall);die;

    

    $data = array(
      'apiKey'=>$opentok_key,
      'sessionId'=>$sessionId,
      'token'=>$token,
      'name'=>ucwords($user->first_name.' '.$user->last_name),
      'emp_id'=>$user->_id
    );

    return view('employe/opentok', ['data' => $data,'message'=> $message]);

  }



#================================================================================#


  function createSipSession(){

      # API URL
      $url = 'https://api.onsip.com/api';

      # Dynamic Values
      $username ='tarun@debut.onsip.com';
      $password ='aA@welcome01';

      $postData = array(
        'Action'=>'SessionCreate',
        'Username'=>$username,
        'Password'=>$password,
        'Output'=>'json',
      );


      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec ($ch);
      curl_close ($ch);
      $result = json_decode($result);
      //print_r($result);die;
      //print_r($result->Response->Context->Session->SessionId);die;
      return $result->Response->Context->Session->SessionId;
    
  }


#================================================================================#


  function creareSipUser($username,$email,$password){

      
      # API URL
      $url = 'https://api.onsip.com/api';

      # Fix Values
      $sipSessionId = AgreementController::createSipSession();
      $OrganizationId ='145406';
      $domain = 'debut.onsip.com';

      # Dynamic Values
      /*$username ='testing23';
      $email ='testing1@gmail.com';
      $password ='aA@welcome01';*/


      $postData = array(
        'Action'=>'UserAdd',
        'SessionId'=>$sipSessionId,
        'OrganizationId'=>$OrganizationId,
        'Username'=>$username,
        'Domain'=>$domain,
        'Name'=>$username,
        'Email'=>$email,
        'AuthUsername'=>$username,
        'Password'=>$password,
        'PasswordConfirm'=>$password,
        'Output'=>'json',
      );


      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec ($ch);
      curl_close ($ch);
      $result = json_decode($result);
      print_r($result);die;
      return  $result;

  }


#================================================================================#

  # Satisfied With Call 

  function satisfiedWithCall($id, CommonRepositary $common){

       $user = User::where(array("_id" => $id))->first();

        if($user){

            $data = array('profile_complete'=>11);
            $user->update($data);

            $common->send_notifications($id, $type = 23, $message = null);
            $result = array('status'=>'1','message'=>'success');

       }else{
            $result = array('status'=>'0','message'=>'user not found');   
       }
   
    echo json_encode($result);

  }



#================================================================================#

function downloadOpentokVideo($opentok_key,$opentok_secret,$sessionId){

    #Genrate JWT token with opentok API
    #==================================
        $tokens = array(
            'ist' => 'project',
            'iss' => $opentok_key,
            'iat' => time(),
            'exp' => time()+1800,
            'jti' => uniqid(),
        );

        $jwtOpentokToken = JWT::encode($tokens, $opentok_secret);


           $header[]='Accept:application/json';
        $header[]='Content-Type:application/json';
        $header[]='X-OPENTOK-AUTH:'.$jwtOpentokToken;


   # Get Video URL
   #======================

    $url = 'https://api.opentok.com/v2/project/'.$opentok_key.'/archive?sessionId='.$sessionId;

    $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $result = curl_exec ($ch);
    curl_close ($ch);
    $result = json_decode($result);
    //$result->items[0]->id;
    //print_r($result->items[0]->url); 
    //print_r($result->items[0]->id);die;
    //return $result->items[0]->url;

    $archiveId = $result->items[0]->id;

    $path = $opentok_key.'/'.$archiveId.'/archive.mp4';

    $awsKey ='AKIAJPDJ2BHK7OGXEA4A';
    $awsSecret='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';

    $s3Client = new \Aws\S3\S3Client([
    'version'     => 'latest',
        'region'      => 'ca-central-1',
        'credentials' => [
            'key'    => $awsKey,
            'secret' => $awsSecret
        ]
    ]);


    $cmd = $s3Client->getCommand('GetObject', [
    'Bucket' => 'madwall',
    'Key' => $path,
    ]);


    try
    {
    $request = $s3Client->createPresignedRequest($cmd, '+5 minutes');
    $presignedUrl = (string)$request->getUri();
    echo $presignedUrl; die;
    }
    catch(S3Exception $e)
    {
    $e->getMessage();
    die();
    }

}


#================================================================================#

  public function iphone_send_notification($deviceIds,$message){
    

    $result='';

    $body['aps'] = array(
        'alert' => $message['message'],
        'noti_for'=>$message,
        'sound' => 'default',
          //'mediaUrl' => $message['image'],
          //'mediaType' => 'image',
        'badge' => 0,
    );


    // Encode the payload as JSON
    $payload = json_encode($body);

    $ctx = stream_context_create();
  
    $passphrase = '1234'; 

    stream_context_set_option($ctx, 'ssl', 'local_cert', 'public/certs/VoIPMadwall.pem'); 
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);  
  
    $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 4, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
    //$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 4, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
  
    if($fp){

    $message = chr(0) . pack('n', 32) . pack('H*', $deviceIds) . pack('n', strlen($payload)) . $payload;
    $result = fwrite($fp, $message, strlen($message));
   
    fclose($fp);
    
    }
        echo $result;

  } 

  #================================================================================#

  # Decline Opentok Call 

  function decline($id, CommonRepositary $common){

       $user = User::where(array("_id" => $id))->first();

        if($user){

            $common->send_notifications($id, $type = 22, $message = null);
            $result = array('status'=>'1','message'=>'success');

       }else{
            $result = array('status'=>'0','message'=>'user not found');   
       }
   

    echo json_encode($result);

  }

#================================================================================#



    # Get Agreement Form

    function InitiatingSipCall($opentok_key,$opentok_secret,$sessionId,$token){

    
       #Genrate JWT token with opentok API
       #==================================
        $tokens = array(
            'ist' => 'project',
            'iss' => $opentok_key,
            'iat' => time(),
            'exp' => time()+1800,
            'jti' => uniqid(),
        );

        $jwtOpentokToken = JWT::encode($tokens, $opentok_secret);

    

        # Initiating a SIP call
        #======================
        $url = 'https://api.opentok.com/v2/project/'.$opentok_key.'/dial';
       // $sipUri = 'sip:user@sip.partner.com';
        $sipUri = 'sip:tarun@debut.onsip.com';
        

        $header[]='Accept:application/json';
        $header[]='Content-Type:application/json';
        $header[]='X-OPENTOK-AUTH:'.$jwtOpentokToken;

          $options = array(
            'sessionId'=>$sessionId,
            'token'=>$token,
            'sip'=> array(
            'uri'=>$sipUri,
            'secure'=>false
            )
          );

         $postData =  json_encode($options);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
          $result = curl_exec ($ch);
          curl_close ($ch);
         // $result = json_decode($result);
          //print_r($result);die;
          return  $result;
    }

#================================================================================#
    # Get Agreement Form

    function agreementForm($id){

    $user = User::where(array("_id" => $id))->first();

    if($user){

      $data = array(
        'emp_id'=>$id,
        'name'=>$user->first_name.' '.$user->last_name,
        'sin_number'=>$user->sin_number, 
        'currentDate'=>date("m-d-Y"),
        'day'=>AgreementController::getOrdinalNum(date("d")),
        'month'=>date("F"),
        'year'=>date("y"),
      );
      $agreement = array();
      $agreement = Agreement::where(array("employeeId" => $id))->first();


      for ($number = 1; $number <= 31; $number++){
            $ends = array('th','st','nd','rd','th','th','th','th','th','th');
            if (($number %100) >= 11 && ($number%100) <= 13)
               $days[] = $number. 'th';
            else
               $days[] = $number. $ends[$number % 10];
      }

      return view('employe/agreement', ['data' => $data,'agreement' => $agreement,'days'=>$days]);

    }else{

      abort(404);
    }
  }


  function getOrdinalNum($number){

     $ends = array('th','st','nd','rd','th','th','th','th','th','th');

    if (($number %100) >= 11 && ($number%100) <= 13){
        $days = $number. 'th';
    }else{
        $days = $number. $ends[$number % 10];
    }   
    return $days;
  }

#================================================================================#

  # Save Agreement 

    function saveAgreementForm(Request $request){

       $agreement = Agreement::where(array("employeeId" => $request->input('employeeId')))->first();

       if($agreement){
          $agreement->update($request->all());
       }else{
          Agreement::create($request->all());
       }

      
      $user = User::where(array("_id" => $request->input('employeeId')))->first();
      $data = array('agreement_status'=>'2','profile_complete'=>9,'sin_number'=>$request->input('employeSin'));
      $user->update($data);

      $url = url('success');

    $result = array('status'=>$request->all(),'url'=>$url);   
    echo json_encode($result);

  }


#================================================================================#

  # convert base 64 to png file

    function base64ToPng(Request $request){

        $base64image = $request->input('image');

       $path = $_SERVER['DOCUMENT_ROOT'].'/beta/public/img/';

        $image_parts = explode(";base64,", $base64image);

          $image_base64 = base64_decode($image_parts[1]);
          $name = 'img'.uniqid() . '.png';
          $file = $path . $name;
          file_put_contents($file, $image_base64);

          $fullurl = url('/public/img').'/'.$name;
          $result = array('status'=>'1','image'=>$name,'fullurl'=>$fullurl);    
      echo json_encode($result);
  }

#================================================================================#

   public function pdfview(Request $request, $id)
    {

      //$data = Agreement::where(array("employeeId" => $id))->get()->toarray();
      //print_R($id);die;
      $data = Agreement::where(array('employeeId' => $id))->first()->toarray();;

      view()->share('data',$data);

       if($request->has('download')){


          // Set extra option
          //PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
          // pass view file
            $pdf = PDF::loadView('employe/pdfview');
            // download pdf
            return $pdf->download('agreement.pdf');
      }
        return view('employe/pdfview');
    }

#================================================================================#


    public function downloadEmergencyContact(Request $request, $id)
    {
      $data = EmergencyContact::where(array('user_id' => $id))->first()->toarray();;

      view()->share('data',$data);

        //if($request->has('download')){

            $pdf = PDF::loadView('employe/downloadEmergencyContactView');
            // download pdf
            //return $pdf->download('EmergencyContact.pdf');
            return $pdf->stream();
        //}
        //return view('employe/downloadEmergencyContactView');
    }

#================================================================================#

    public function downloadDirectDeposit(Request $request, $id)
    {
      $data = DirectDeposit::where(array('user_id' => $id))->first()->toarray();;

      view()->share('data',$data);

        //if($request->has('download')){

            $pdf = PDF::loadView('employe/downloadDirectDepositView');
            // download pdf
            //return $pdf->download('DirectDeposit.pdf');
            return $pdf->stream();
        //}
        //return view('employe/downloadDirectDepositView');
    }

#================================================================================#

    public function downloadEmplyoeeContract(Request $request, $id)
    {
      $data = Agreement::where(array('employeeId' => $id))->first()->toarray();;

      view()->share('data',$data);

     //   if($request->has('download')){

            $pdf = PDF::loadView('employe/downloadEmplyoeeContractView');
            // download pdf
            //return $pdf->download('EmplyoeeContract.pdf');
            return $pdf->stream();
      //  }
       // return view('employe/downloadEmplyoeeContractView');
    } 

#================================================================================#

     # Get success 

    function success(){

      return view('employe/success');
    }

#================================================================================#


}



