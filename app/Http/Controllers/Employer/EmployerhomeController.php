<?php

namespace App\Http\Controllers\employer;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Model\CategoryModel;
use App\Model\JobsapplicationModel;
use App\Model\SkillModel;
use App\Model\ShiftModel;
use App\Model\Documentation;
use App\Model\AboutModel;
use App\Model\NotificationModel;
use App\Model\JobsModel;
use App\Model\ContactUs;
use App\Model\User;
use App\Http\Requests\JobsRequest;
use Flash;
use Auth;
use Request as AjaxRequest;
use DB;
use Cookie;
use Session;
use Carbon\Carbon;
use App\Model\EmailTemplate;
use App\Model\TempJobdelete;
use App\Jobs\SendOtpEmail;
use App\Model\EmailVerification;
use App\Http\Requests\ContactusRequest;
use App\Http\Requests\AboutusRequest;
use App\Http\Repositary\CommonRepositary;
use App\Http\Repositary\EmployerRepositary;
use DateTime;
use DateTimeZone;
use Crypt;
use App\Model\Wage;
use App\Model\SendQuizNotification;
use App\Model\TempJobs;
use App\Model\ApplicantShift;


class EmployerhomeController extends Controller
{
    //
	public function __construct(Guard $auth, EmployerRepositary $employer)
	{
		$this->auth = $auth;
		$this->employer = $employer;
		$this->middleware('auth', ['except' => ['postContactus']]);
		$this->middleware('backbutton', ['except' => ['postContactus']]);
		if (isset($_COOKIE['client_timezone'])) {
			date_default_timezone_set($_COOKIE['client_timezone']);
		}
	}
	
	/*
	 * Updated on : 1 aug 2017
	 * Updated by : shivani, to set page title 
	 * */
	public function index()
	{
		$title = 'Dashboard';
		$active = 'dashboard';
       // $category = CategoryModel::where(array('status'=>true,'type'=>'category','is_deleted'=>false))->pluck('name','_id')->toArray();

		$industIds = User::where(array('status' => true, '_id' => $this->auth->user()->_id))->first();
		$inIds = [];
        /*print_r($industIds);*/
		if ($industIds) {
			foreach ($industIds->industry as $k => $indust) {
				$inIds[] = $indust['_id'];
			}
		}
		$category = CategoryModel::where(array('type' => 'category', 'is_deleted' => false))->whereIn('industry_id', $inIds)->pluck('name', '_id')->toArray();

		$documetation = Documentation::All();
		return view('employer.promo.dashboard', compact('category', 'active', 'title', 'documetation'));
	}

	public function getJobpost(request $request)
	{
		$data = $request->all();
		$utc = $request->input('timezone');
		$records = 10;
		$date = Carbon::now();
		$jobs = JobsModel::where('user_id', $this->auth->user()->_id)->where(array('status' => true, 'is_deleted' => false))->where('end_date', '>=', $date);
        //$order = 'desc';
		$order = 'asc';
		$msg = 'No Jobs.';
		if (AjaxRequest::ajax()) {

			if (!empty($data['job_name']))
				$jobs->where('title', 'LIKE', '%' . $data['job_name'] . '%');
			if (!empty($data['category']))
				$jobs->where('job_category', $data['category']);
			if (!empty($data['status']))
				$jobs->where('job_status', intval($data['status']));
			if (isset($data['order']) && !empty($data['order']))
				$order = $data['order'];
			if (!empty($data['records']))
				$records = intval($data['records']);

			if (!empty($data['job_name']) || !empty($data['category']) || !empty($data['status']) || isset($data['order']) && !empty($data['order']) || !empty($data['records']))
				$msg = 'No Jobs.';
		}

		$jobCount = $jobs->count();
		$jobData = $jobs->orderBy('start_date', $order)->paginate($records);
		$jobData = $jobData->setPath('jobdata');
		return view('employer.promo.ajaxjobdata', compact('jobData', 'jobCount', 'records', 'utc', 'msg'))->render();
	}
	
	
	/*
	 * Updated on : 1 aug 2017
	 * Updated by : shivani, to set page title 
	 * */
	public function createPost($id = null, EmployerRepositary $employer)
	{
		$active = '';
		if (!empty($id)) {
			$title = 'Repost a Job';
		} else {
			$title = 'Post a job';
		}         
        //check employer (approved,blocked,verified)
		$checkEmployer = $this->postCheckEmployer(new Request);		
		//do allow to post job if employer is either blocked or has a non verified email.
		if ($checkEmployer['block_type'] != 'approved') {
			flash()->error('You can not perform this action as you have been blocked by the madwall.');
			return redirect('employer/dashboard');
		}
		$viewData = $this->employer->create_post_data($id);  
		//if categories are not returned by repositary
		if (empty($viewData['category'])) {
			$industIds = User::where(array('status' => true, '_id' => $this->auth->user()->_id))->first();
			$inIds = [];
			if ($industIds) {
				foreach ($industIds->industry as $k => $indust) {
					$inIds[] = $indust['_id'];
				}
			}
			$viewData['category'] = CategoryModel::where(array('status' => true, 'type' => 'category', 'is_deleted' => false))->whereIn('industry_id', $inIds)->pluck('name', '_id')->toArray();
		}
		$viewData['active'] = $active;
		$viewData['title'] = $title;
		//render view for a new job to be posted
		return view('employer.promo.createpost', $viewData);
	}

	public function Subcategory(Request $request)
	{
		$cat_id = $request->value;
		$subcategory = CategoryModel::where(array('status' => true, 'is_deleted' => false, 'type' => 'subcategory', 'category_id' => $cat_id))->select('name', '_id')->get()->toArray();
		$commission = $this->employer->get_commision($cat_id);//CategoryModel::where(array('_id'=>$cat_id))->value('commision');

		return response()->json(['sub_cat' => $subcategory, 'commision' => $commission['commision']]);
	}

	public function Skills(Request $request)
	{
		$subcat_id = $request->value;
		$skilldata = $this->employer->get_skills($subcat_id);
		$skills = CategoryModel::where(array('status' => true, '_id' => $subcat_id, 'type' => 'subcategory'))->select('mandatory_skills')->get()->toArray();
		$mId = [];
		foreach ($skills as $k => $value) {
			if (isset($value['mandatory_skills']) && !empty($value['mandatory_skills'])) {
				$mId = empty($value['mandatory_skills']) ? [] : $value['mandatory_skills'];
			}
		}
		return response()->json(['skills' => $skilldata, 'mandatory' => $mId]);
	}
    
    
	/*
	 * DESC : to save job details.
	 * published type : 0->manual
	 * published type : 1->automatic
	 * published type : 2->rehire
	 * JobsRequest
	 * */
	public function postSavejob(JobsRequest $request, CommonRepositary $common, EmployerRepositary $employer)
	{		
		//check employer (approved,blocked,verified)
		$checkEmployer = $this->postCheckEmployer(new Request);		
		//do allow to post job if employer is either blocked or has a non verified email.
		if ($checkEmployer['block_type'] != 'approved') {
			flash()->error('You can not perform this action as you have been blocked by the madwall.');
			$url = url('employer/dashboard');
			$result = ['status' => false, 'message' => 'Sorry, you can not perform this action'];
			return response()->json($result);
		}
		/*****************End here******************/
		$saveJob = $this->employer->save_job($request);
		if ($saveJob['success'] == true) {
			$saveJob['url'] = url('admin/post-new-job');
			return response()->json($saveJob, 200);
		} else {
			return response()->json($saveJob, 422);
		}
	}  
	
	/*
	 * Updated on : 1 aug 2017
	 * Updated by : shivani, to set page title 
	 * */
	public function getJobdetail($id, CommonRepositary $common)
	{

		$active = '';
		$title = 'Job Details';
		$jobid = \Crypt::decrypt($id); 
        //$common->calculate_total_job_hours($jobid);         
		$job_post = JobsModel::with(['jobapplied' => function ($q) {
			$q->where('is_deleted', false);
		}, 'jobshifts'])
		->where('_id', $jobid)->where('is_deleted', false)->first();


         /*echo "<pre/>";
         print_r($job_post);
         die('ssss');*/

		if ($job_post) {
			$date = Carbon::now();
            //add one hour to current time
			$current_date = $date->addHours(1)->parse($date)->format('Y-m-d H:i:s');
			//check if job can be edited or not == shivani (29 august 2017)
			$job_edit = $this->employer->check_job($job_post, $current_date);
			//======= end 29 aug 2017            
			$documetation = Documentation::All();
			$user_ids = $accepted_users = [];
            //selected jobseekers
			if ($job_post->job_published_type == 2) {
				$jobPost_arr = $job_post->toArray();
				if (!empty($jobPost_arr['jobapplied'])) {
					foreach ($jobPost_arr['jobapplied'] as $jobseeker) {
						if ($jobseeker['job_status'] == 1) {
							$accepted_users[] = $jobseeker['jobseeker_id'];
						} else {
							$user_ids[] = $jobseeker['jobseeker_id'];
						}
					}
				}
			}
			//rehire candidate list
			$rehire_user = $this->employer->getRehireList($accepted_users);
			$users = $rehire_user['users'];
			return view('employer.promo.job_detail', compact('job_post', 'job_edit', 'job_dates', 'current_date', 'active', 'title', 'documetation', 'users', 'user_ids', 'accepted_users'));
		} else {
			flash()->error('Invalid Id');
			return redirect('employer/dashboard');
		}
	}
	
	/*
	 * Updated : 24 aug 2017, shivani - To send notification to jobseeker when job is deleted.
	 * 19 sep 2017, to update total hours worked for the job
	 * */
	public function postDeletejob(Request $request, CommonRepositary $common, EmployerRepositary $employer)
	{
		$jobId = \Crypt::decrypt($request->value);
		$deleteJob = $this->employer->delete_job($jobId);
		$deleteJob['url'] = url('employer/dashboard');
		return response()->json($deleteJob);
	}




	public function getEditjob($id, EmployerRepositary $employer)
	{
		$active = '';
		$title = 'Edit Job';
		if (Auth::user()->approved == 3 || (Auth::user()->approved == 1 && !empty(Auth::user()->email_verification_code)) || Auth::user()->approved == 0)
			return redirect('employer/dashboard');

		$id = \Crypt::decrypt($id);
		$editJobData = $this->employer->get_edit_job_data($id);
		if ($editJobData['success'] == true) {
			return view('employer.promo.editjob', $editJobData);
		} else {
			$encryptedId = \Crypt::encrypt($id);
			//redirect back to job listing page, with error message.
			return redirect('employer/jobdetail/' . $encryptedId);
			//return false;
		}
	}
	
	
	/*
	 * DESC : to update job
	 * */
	public function postEditjob(JobsRequest $request, $id, CommonRepositary $common, EmployerRepositary $employer)
	{

		$id = \Crypt::decrypt($id);
		$updateJob = $this->employer->update_job($id, $request);
		if ($updateJob['success'] == false) {
			//return error response 
			return response()->json($updateJob, 422);
		} else {
			return response()->json($updateJob, 200);
		}
	}

	public function postJobRating(Request $request)
	{

		$data = $request->all();
		$job_id = \Crypt::decrypt($data['value']);
		$jobs = JobsModel::where(array('_id' => $job_id, 'is_deleted' => false, 'user_id' => $this->auth->user()->_id))->first();

		if ($jobs->update($data)) {
			flash()->success('Rating apply sucessfully');
			$result = ['status' => true, 'message' => 'Rating apply sucessfully'];
		} else {
			flash()->error('Rating not apply sucessfully');
			$result = ['status' => true, 'message' => 'Rating not apply sucessfully'];
		}

		return response()->json($result, 200);
	}
	
	
	
	/*
	 * Updated on : 1 aug 2017
	 * updated by : shivani, debut infotech
	 * desc : to return correct message and update status in job applications as well.
	 * updated : 10 aug 2017, to check if max workers requirement is fulfilled or not
	 * */
	public function postAcceptjob(Request $request, CommonRepositary $common)
	{
		$acceptDeclineEmployee = $this->employer->accept_decline_candidate($request);
		return response()->json($acceptDeclineEmployee, 200);
	}

	/*
	 * Updated on : 1 aug 2017
	 * Updated by : shivani, to set page title 
	 * */
	public function getAbout()
	{
		$title = 'Contact MadWall';
		$active = 'about';
		$user = User::where('_id', Auth::user()->_id)->first();
		$documetation = Documentation::All();
		return view('employer.promo.about', compact('active', 'user', 'title', 'documetation'));
	}
	
	/*
	 * DESC : to return jobs name while searching for a job (using typeahead at employer dashboard.)
	 * */
	public function EmployerJobsName()
	{
		$location = JobsModel::where(array('is_deleted' => false, 'user_id' => $this->auth->user()->id))->pluck('title');
		return $result = ['success' => true, 'location' => $location];
	}


	public function postContactus(ContactusRequest $request, CommonRepositary $common)
	{
		$data = $request->all();
		$contact = new ContactUs();
		$data['type'] = 'guest';
		$data['status'] = true;
		$data['is_deleted'] = false;
		$data['request_status'] = 'pending';
		$from = '';
		if ($this->auth->user()) {
			$data['user_id'] = $from = $this->auth->user()->_id;
			$data['email'] = $this->auth->user()->email;
			$name = $this->auth->user()->first_name . ' ' . $this->auth->user()->last_name;
			$data['name'] = $name;
			$data['type'] = 'website';
		} else {
			$name = $request->name;
		}
		if ($contact->create($data)) {
			$template = EmailTemplate::find('596c9f85b098f02dd03c2153');
			$find = array('@name@');
			$values = array($data['name']);
			if ($template) {
				$body = str_replace($find, $values, $template->content);
				//$this->dispatch(new SendOtpEmail($body, $data['email'], $template));
			}
			$template1 = EmailTemplate::find('596c9fdeb098f01a7173a607');
			if ($template1) {
				$find1 = array('@name@', '@message@', '@username@');
				$admindata = User::where(array('role' => 1))->first();				
                //$values1=array($admindata->first_name.' '.$admindata->last_name,$data['content'],$data['name']);
				$values1 = array('Admin', $data['content'], $data['name']);
				$body1 = str_replace($find1, $values1, $template1->content);
				//$this->dispatch(new SendOtpEmail($body1,$admindata->email,$template1));
				////$this->dispatch(new SendOtpEmail($body1, 'info@madwall.ca', $template1));
				//$this->dispatch(new SendOtpEmail($body1,'debutinfo11@gmail.com',$template1));				
				//send notification to admin - shivani (24 aug 2017)
				$adminData = $common->getAdminDetail();
				$common->saveNotification($from, $to = $adminData->_id, $type = 2, $target = 3, null, $title = $name . ' has sent a query.');
			}
			$result['status'] = true;
			$result['message'] = 'Your message has been sent successfully!';
			if (isset($request->req_frm) && $request->req_frm == 'employer') {
				flash()->success($result['message']);
			}
		} else {
			$result['status'] = true;
			$result['message'] = 'Your message has not been sent successfully!';
			if (isset($request->req_frm) && $request->req_frm == 'employer') {
				flash()->error($result['message']);
			}
		}
		return response()->json($result, 200);
	}

	public function postEmployerUserRating(Request $request, CommonRepositary $common)
	{
		$data = $request->all();
		//job_id
		$job_id = $data['value'];
		$errorCount = 0;
		if (isset($data['u_ids'])) {
			foreach ($data['u_ids'] as $k => $users) {
				if ($data['rating'][$k] > 0) {
					$ratvalue = $common->AdjustRating($data['rating'][$k], $users, $job_id);
				}
			}
			flash()->success('Rating assigned successfully');
			$result = ['status' => true];
		}
		return response()->json($result, 200);
	}

	public function postCheckUser(Request $request)
	{
		$user = User::where(['_id' => $this->auth->user('_id'), 'status' => true, 'email_verification_code' => null, 'approved' => 1])->first();

		if ($user) {
			return true;
		} else {
			return false;
		}

	}


	public function postCheckEmployer(Request $request)
	{
		$user = User::where(['_id' => $this->auth->user()->_id])->first();


		if ($user) {
			$blockUser = false;
			$block_type = 'approved';
			$url = '';
			//if user profile is approved from admin.then
			if ($user->status == false && $user->approved == 3) {
				$block_type = 'blockuser';
				$blockUser = true;
				$url = '';
			} else {
				if ($user->approved == 1 && empty($user->email_verification_code)) {
					$url = url('employer/post_job');
				} elseif ($user->approved == 1 && !empty($user->email_verification_code)) {
					$block_type = 'verifyemail';
					$blockUser = true;
					$url = '';
				} else {
					$block_type = 'waitlistuser';
					$blockUser = true;
					$url = '';
				}
			}
			//if function is called via ajax, then return json and otherwise simply return array
			if ($request->ajax()) {
				return response()->json(['success' => true, 'block_user' => $blockUser, 'block_type' => $block_type, 'url' => $url]);
			} else {
				return ['success' => true, 'block_user' => $blockUser, 'block_type' => $block_type];
			}
		} else {
			if ($request->ajax()) {
				return response()->json(['success' => false, 'message' => 'Sorry, user not found'], 422);
			} else {
				return ['success' => false, 'message' => 'Sorry, user not found'];
			}
		}
	}
    
    
	
	
	/*
	 * Added on : 29 aug 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to send rehire invitation to the jobseekers.
	 * */
	public function getRehireInvite(Request $request, CommonRepositary $common)
	{
		$inviteJobseekers = $this->employer->rehire_invitation($request);
		return response()->json($inviteJobseekers);
	}
	
	/*
	 * Added on : 18 sep 2017, shivani - Debut infotech
	 * DESC : to cancel and calculate job hours for an employee
	 * */
	public function getCancelEmployee(Request $request, CommonRepositary $common)
	{
		$jobApp_id = \Crypt::decrypt($request->cancel_employee_id);
		$cancelEmployee = $this->employer->cancel_employee($jobApp_id);
		return response()->json($cancelEmployee);
	}
	
	/*
	 * Added on : 20 sep 2017
	 * DESC  :to return certificates/degree details of jobseeker
	 * */
	public function getCandidateDocuments(Request $request)
	{
		$user_id = $request->user_id;
		$candidate_details = User::where("_id", $user_id)->first();
		return view('employer.promo.jobseeker_certificates', compact('candidate_details'));
	}
	
	
	
	/*
	 * Added on : 17 nov 2017, shivani Debut infotech
	 * DESC : to save job shift dates to a temporary table
	 * */
	public function save_shift_time(Request $request)
	{
		$shiftTime = $request->all();
		$saveTempData = $this->employer->save_temp_process($shiftTime);
		return response()->json($saveTempData);
	}
	
	
	/*
	 * Added on : 2 jan 2018, shivani Debut infotech
	 * DESC : to save job shift dates to a temporary table
	 * */
	public function same_job_time(Request $request)
	{
		//$shiftTime = $request->all();		
		$requestedDates = explode(',', $request->check_date);
		$shiftTime = [];
		$shiftTime['lunch_hour'] = $request->lunch_hour;
		foreach ($requestedDates as $dte) {
			$shiftTime['start']['time_' . $dte] = $request->start_time;
			$shiftTime['end']['time_' . $dte] = $request->end_time;
		}
		if (!empty($request->process_id)) {
			TempJobs::where('process_id', $request->process_id)->delete();
		}
		$saveTempData = $this->employer->save_temp_process($shiftTime);
		return response()->json($saveTempData);
	}
	
	/*
	 * Added on : 2 jan 2018, shivani Debut infotech
	 * DESC : to save job shift dates to a temporary table
	 * */
	public function edit_same_job_time(Request $request)
	{
		$saveTempData = $this->employer->edit_same_job_time($request);
		return response()->json($saveTempData);
	}
	
	/*
	 * Added on : 17 nov 2017, shivani Debut infotech
	 * DESC : to remove, update shift timings for a job (from temporary values only)
	 * */
	public function remove_shift_time(Request $request)
	{
		$removeData = $this->employer->remove_temp_process_data($request);
		return response()->json($removeData);
	}
	
	
	
	/*
	 * Added : 23 nov 2017, shivani Debut infotech
	 * DESC : to get details on behalf of process id
	 * */
	public function process_details(Request $request)
	{
		$process_details = $this->employer->get_temp_process_details($request);
		return response()->json($process_details);
	}
	
	/*===========================
        Export CSV of Payable Data
    ==============================*/
	public function exportActiveJobs(request $request)
	{
		$data = $request->all();
		$utc = $request->input('timezone');
		$records = 10;
		$date = Carbon::now();
		$jobs = JobsModel::where('user_id', $this->auth->user()->_id)->where(array('status' => true, 'is_deleted' => false))->where('end_date', '>=', $date);
        //$order = 'desc';
		$order = 'asc';
		$msg = 'No Jobs.';
		if (AjaxRequest::ajax()) {

			if (!empty($data['job_name']))
				$jobs->where('title', 'LIKE', '%' . $data['job_name'] . '%');
			if (!empty($data['category']))
				$jobs->where('job_category', $data['category']);
			if (!empty($data['status']))
				$jobs->where('job_status', intval($data['status']));
			if (isset($data['order']) && !empty($data['order']))
				$order = $data['order'];
			if (!empty($data['records']))
				$records = intval($data['records']);

			if (!empty($data['job_name']) || !empty($data['category']) || !empty($data['status']) || isset($data['order']) && !empty($data['order']) || !empty($data['records']))
				$msg = 'No Jobs.';
		}

		$jobCount = $jobs->count();
		$jobData = $jobs->orderBy('start_date', $order)->get();

		$all = array();
		if (count($jobData->toArray()) > 0) {
			$filename = "uploads/ActiveJobs.csv";
			$handle = fopen($filename, 'w');
			fputcsv($handle, array('Job Id', 'Job Name', 'Hiring Method', 'Category', 'Status', 'Total Applicants', 'Total Hired', 'Start date', 'End date'));
			foreach ($jobData as $k => $value) {
				$jobId = Crypt::encrypt($value->_id);
				$job_type = '';
				if ($value->job_published_type == 0) {
					$job_type = 'Manual';
				} elseif ($value->job_published_type == 1) {
					$job_type = 'Automatic';
				} else {
					$job_type = 'Rehire';
				}

				$status = '';
				if ($value->job_status == 1) {
					$status = 'Active';
				} elseif ($value->job_status == 2) {
					$status = 'Filled';
				} elseif ($value->job_status == 3) {
					$status = 'In Progress';
				} elseif ($value->job_status == 4) {
					$status = 'Completed';
				}

				$total_applied = 0;
				if ($value->total_applied) {
					$total_applied = $value->total_applied;
				}

				$total_hired = 0;
				if ($value->total_hired) {
					$total_hired = $value->total_hired;
				}

				fputcsv($handle, array($value->job_id, ucfirst($value->title), $job_type, ucfirst($value['category'][0]['name']), $status, $total_applied, $total_hired, date('F d, Y', strtotime($value->start_date)), date('F d, Y', strtotime($value->end_date))));
			}
			fclose($handle);
			$headers = array(
				"Content-type" => "text/csv"
			);
			return response()->json(array('csv_url' => $filename, 'status' => true));
		} else {
			return response()->json(array('message' => 'Sorry, No results to export', 'status' => false));
		}
	}
    
    /*
	 * Added : 14 dec 2017, shivani debut infotech
	 * DESC : to return users to be shown in rehire user modal.
	 * */
	public function user_for_rehire(Request $request)
	{
		$getUsers = $this->employer->get_rehire_users($this->auth->user()->_id, true, $request);
		return response()->json($getUsers);
	}
	
	/*
	 * Added : 23 jan 2018, shivani Debut infotech
	 * DESC : to assign interview timeslots to an employee for a job.
	 * */
	public function interview_timeslot(Request $request)
	{
		$app_id = \Crypt::decrypt($request->application_id);
		//assign interview timeslots 
		$assignTime = $this->employer->interview_slots($request, $app_id);
		return response()->json($assignTime);
	}
}
