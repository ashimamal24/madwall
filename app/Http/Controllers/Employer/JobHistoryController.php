<?php

namespace App\Http\Controllers\Employer;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Model\JobsapplicationModel;
use App\Model\Documentation;
use App\Model\JobsModel;
use App\Model\ApplicantShift;
use App\Model\User;
use Flash;
use Auth;
use Request as AjaxRequest;
use DB;
use Cookie;
use Session;
use Carbon\Carbon;
use App\Http\Repositary\CommonRepositary;
use App\Http\Repositary\EmployerRepositary;
use DateTime;
use DateTimeZone;
use Crypt;


/*
 * Added : 27 Nov 2017, Debut infotech
 * DESC : to write code for job history related functionalities. 
 * */


class JobHistoryController extends Controller
{
	//
	public function __construct(Guard $auth, EmployerRepositary $employer)
	{
		$this->auth = $auth;
		$this->employer = $employer;
		$this->middleware('auth', ['except' => ['postContactus']]);
		$this->middleware('backbutton', ['except' => ['postContactus']]);
		if (isset($_COOKIE['client_timezone'])) {
			date_default_timezone_set($_COOKIE['client_timezone']);
		}
	}
    
    /*
	 * Updated on : 1 aug 2017
	 * Updated by : shivani, to set page title 
	 * */
	public function getJobhistory()
	{
		$active = 'history';
		$title = 'Job History';
		$documetation = Documentation::All();
		return view('employer.promo.jobhistory', compact('active', 'title', 'documetation'));
	}
	
	/*
	 * updated : 14 sep 2017, shivani - to sort history listing with latest complete jobs
	 * */
	public function jobHistoryListing(Request $request)
	{
		$data = $request->all();
		$records = 10;
		$date = Carbon::now();
		$jobs = JobsModel::where('user_id', $this->auth->user()->_id)->where(function ($query) use ($date) {
			$query->orWhere('is_deleted', true);
			$query->orWhere('end_date', '<=', $date);
		})->orderBy('updated_at', 'desc');       
        //$order = 'desc';
		$order = 'asc';
		$msg = 'No Jobs.';
		if (AjaxRequest::ajax()) {
			if (!empty($data['records']))
				$records = intval($data['records']);

			if (!empty($data['search']))
				$jobs->where('title', 'LIKE', $data['search'] . '%');

			if (!empty($data['search']) || !empty($data['records']))
				$msg = 'No records to display.';
		}
		$jobCount = $jobs->count();
		$jobData = $jobs->paginate($records);
		$jobData = $jobData->setPath('history-listing');
		return view('employer.promo.ajaxjobhistory', compact('jobData', 'jobCount', 'records', 'msg'))->render();
	}
    
    /*
	 * Updated : 24 aug 2017, shivani - to update job post query to return details of deleted jobs as well.
	 * */
	public function getJobHistoryDetail($id, CommonRepositary $common)
	{
		$id = \Crypt::decrypt($id);
		$title = 'Job Detail';
		$users = [];
		$job_post = JobsModel::with(['jobapplied' => function ($q) {
			$q->where('job_status', '!=', 3)->where('total_hours_worked', '!=', null)->where('total_hours_worked', '!=', 0)->with(['applyjobuser' => function ($jb_usr) {
				$jb_usr->select('first_name', 'image', 'email', 'rating', '_id')->orderBy('first_name', 'DESC')->first();
			}]);
		}, 'jobshifts'])->where('_id', $id)->first();

		$applicants = JobsapplicationModel::with(['applyjobuser' => function ($q) {
			$q->select('first_name', 'last_name', 'image', 'email', 'rating', '_id')->orderBy('first_name', 'DESC');
		}])->where(array('employer_id' => $this->auth->user()->_id, 'job_id' => $id, 'is_deleted' => false))->where('job_status', '!=', 3)->where('total_hours_worked', '!=', null)->where('total_hours_worked', '!=', 0)->get()->toArray();

		$ids = [];
		$rIds = [];
		$ratingUser = [];

		$rateUserCount = JobsapplicationModel::where('job_id', $id)->where('job_status', '!=', 3)->where('job_status', '!=', 6)->where('job_status', '!=', 5)->where('rating', false)->count();
		if ($job_post) {
			$documetation = Documentation::All();
			$commission = $this->employer->get_commision($job_post->category[0]['_id'], $job_post->user_id);
			return view('employer.promo.jobhistorydetail', compact('job_post', 'users', 'ids', 'ratingUser', 'rateUserCount', 'title', 'documetation', 'applicants', 'commission'));
		} else {
			flash()->error('Sorry, Job not found');
			return redirect('employer/dashboard');
		}
	}
    
    /*
	 * Added : 27 nov 2017, shivani - 
	 * */
	public function exportJobHistory(Request $request)
	{
		$data = $request->all();
		$records = 10;
		$date = Carbon::now();
		$jobs = JobsModel::where('user_id', $this->auth->user()->_id)->where(function ($query) use ($date) {
			$query->orWhere('is_deleted', true);
			$query->orWhere('end_date', '<=', $date);
		})->orderBy('updated_at', 'desc');       
        //$order = 'desc';
		$order = 'asc';
		$msg = 'No Jobs.';
		if (AjaxRequest::ajax()) {
			if (!empty($data['records']))
				$records = intval($data['records']);

			if (!empty($data['search']))
				$jobs->where('title', 'LIKE', $data['search'] . '%');

		}
		$jobData = $jobs->get();
		if (!empty($jobData->toArray())) {
			$filename = "uploads/JobHistory.csv";
			$handle = fopen($filename, 'w');
			fputcsv($handle, array('Job Id', 'Job Name', 'Hiring Method', 'Applicants', 'Wage', 'Commision(%)', 'Sub-Total', 'Total', 'Status', 'Start Date', 'End Date'));
			foreach ($jobData as $key => $value) {
				$job_id = $value->job_id;
				$job_name = $value->title;
				$job_type = '';
				if ($value->job_published_type == 0) {
					$job_type = 'Manual';
				} elseif ($value->job_published_type == 1) {
					$job_type = 'Automatic';
				} else {
					$job_type = 'Rehire';
				}

				$total_hired = 0;
				if ($value->total_hired) {
					$total_hired = $value->total_hired;
				}
				$wage = number_format($value->salary_per_hour, 2);
				$price = 0;
				if (!empty($value->total_work_hours)) {
					$price = ($value->salary_per_hour * $value->total_work_hours);
				}

				$totalCommision = $value->category[0]['commision'];
				if (isset($value->category_commision) && !empty($value->category_commision)) {
					$totalCommision = $value->category_commision;
				}

				$commision = (($price * $totalCommision) / 100);

				$total = '$0.00';
				$status = '';
				if ($value->is_deleted == true) {
					$status = 'Deleted';
				} else {
					if ($value->job_status == 1) {
						$status = 'Active';
					} elseif ($value->job_status == 2) {
						$status = 'Filled';
					} elseif ($value->job_status == 3) {
						$status = 'Progress';
					} elseif ($value->job_status == 4) {
						$status = 'Completed';
					}
				}

				$total = '$' . number_format($price + $commision, 2);
				$start_date = date('F d, Y', strtotime($value->start_date));
				$end_date = date('F d, Y', strtotime($value->end_date));
				fputcsv($handle, array($job_id, ucfirst($job_name), $job_type, $total_hired, '$' . $wage, $totalCommision, '$' . $price, $total, $status, $start_date, $end_date));
			}
			fclose($handle);
			$headers = array(
				"Content-type" => "text/csv"
			);
			return response()->json(array('csv_url' => $filename, 'status' => true));
		} else {
			return response()->json(array('message' => 'Sorry, No results to export', 'status' => false));
		}
	}
	
	
	/*
	 * Added : 29 nov 2017, shivani
	 * DESC : to list users who has worked on the job (history detail page.)
	 * */
	public function postEmployeeHistory(Request $request)
	{
		$data = $request->all();
		$records = 10;
		$name = '';
		$jobId = \Crypt::decrypt($data['jobvalue']);
		//query updated
		$job_applied = JobsapplicationModel::with(['applyjobuser' => function ($q) {
			$q->select('first_name', 'last_name', 'cv_url', 'rating', 'image', 'skills', 'additional_documents', '_id');
		}, 'job' => function ($q) {
			$q->select('start_date', 'total_hired', 'number_of_worker_required');
		}])->where(array('employer_id' => $this->auth->user()->_id, 'job_id' => $jobId, 'is_deleted' => false))->whereIn('job_status', [4, 5, 7]);

		if (AjaxRequest::ajax()) {
			if ($data['job_name']) {
				//search user by first and last name both.
				$user_ids = User::where('first_name', 'like', $data['job_name'] . '%')->pluck('_id');
				if (count($user_ids) == 0) {
					$user_ids = User::where('last_name', 'like', $data['job_name'] . '%')->pluck('_id');
				}
				$job_applied->whereIn('jobseeker_id', $user_ids);
			}
			if ($data['records']) {
				$records = intval($data['records']);
			}
		}
		$order = 'asc';
		$jobCount = $job_applied->count();
		$jobData = $job_applied->orderBy('_id', $order)->paginate($records);
		$jobData = $jobData->setPath('jobapplylisting');
		return view('employer.promo.history_employeelist', compact('jobData', 'jobCount', 'records'))->render();
	}
	
	/*
	 * Added : 29 Nov 2017
	 * DESC : to return employee shift details
	 * */
	public function employeeShifts(Request $request)
	{
		$employeeShift = $this->employer->employee_shift_details($request);
		return response()->json($employeeShift);
	}
	
	/*
	 * Added : 30 Nov 2017
	 * DESC : to update employee work hours for a particular shift
	 * */
	public function update_work_hours(Request $request)
	{

		$updateHours = $this->employer->update_work_hours($request);
		$createHours = $this->employer->create_work_hours($request);
		if ($createHours['success'] === 0 || $updateHours['success'] === 0) {
			if ($createHours['success'] == 0) {
				return response()->json($createHours);
			} else {
				if ($updateHours['success'] == 0) {
					return response()->json($updateHours);
				}
			}
		}

		flash()->success('Timesheet updated successfully');
		return response()->json(["success" => true]);
	}

	/*
	delete job
	 */
	public function deleteJob($id)
	{
		$this->employer->deleteShiftModel($id);
		return response()->json(true);
	}
}
