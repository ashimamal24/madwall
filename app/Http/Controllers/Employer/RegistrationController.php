<?php namespace App\Http\Controllers\employer;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Http\Request;
use App\EmailTemplate;
use App\Jobs\SendEmail;
use Auth;//18 aug 2017, shivani - to check if there is any user already logged in 
use Carbon\Carbon;

class RegistrationController extends Controller
{
	
	/*
	|--------------------------------------------------------------------------
	| Registration Controller
	|--------------------------------------------------------------------------
	|
	| This controller activates the user account and completes the Registration process.
	| Created on: 11/8/2015
	| Updated on: 31/8/2015
	 */

	public function confirm($confirmation_code, Request $request)
	{
		if (Auth::user()) {
			//check if confirmation link belongs to logged user otherwise an error will show.
			$user = User::where('_id', Auth::user()->_id)->where('email_verification_code', $confirmation_code)->count();
			if ($user == 0) {
				//This page can not be available to a logged user
				return view('errors.error', ['message' => 'Sorry, You can not access that page']);
			}
		}
		// Check whether confirmation code exists or not.			
		if (!$confirmation_code) {
			throw new \Exception("Whoops, this is an invalid confirmation code....please try again");
		}
		//current date
		$current_date_time = Carbon::now();
		$expiry_date = '';
		$user = User::where('email_verification_code', $confirmation_code)->first();		
		// Check whether user exists or not.
		if (!$user) {
			flash()->error('Link has been expired');
			return redirect('/');
		} else {
			if (isset($user->link_expire_at) && !empty($user->link_expire_at)) {
				$expiry_date = Carbon::parse($user->link_expire_at);
				if ($current_date_time >= $expiry_date) {
					flash()->error('Link has been expired');
					return redirect('/');
				} else {
					$user->email = $user->new_email;
					$user->new_email = null;
					$user->link_expire_at = null;
				}
			}
		}
		$user->status = true;
		$user->email_verification_code = null;
		$user->save();
		$data = $request->all();
		if (isset($data['verify']) && !empty($data['verify'])) {
			$url = url('employer/dashboard');
			$result = ['url' => $url];
			return response()->json($result, 200);
		} else {
			if (!empty($expiry_date)) {
				flash()->success('Email updated successfully!');
			} else {
				flash()->success('Your account has been confirmed');
			}
			if (Auth::user()) {
				return redirect('/employer/dashboard');
			}
			return redirect('/');
		}
	}

}
