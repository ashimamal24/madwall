<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\JobsModel;
use App\Model\JobsapplicationModel;
use Carbon\Carbon;
use App\Model\SendQuizNotification;
use App\Model\User;
use App\Model\TempJobdelete;
use App\Model\UserRatings;
use App\Model\Setting;
use App\Model\ApplicantShift;
use App\Http\Repositary\CommonRepositary;

class CronController extends Controller
{
	/*************
		Function for complete tasks after end date of jobs
		updated : 12 aug 2017, shivani - to add total hours worked in job applications.
	 * 19 sep 2017, to update total hours worked for the job
	 ***************/
	public function complateJobs(CommonRepositary $common)
	{
		$date = Carbon::now();
		$start_date = $data['job_status'] = 4;
		$jobs = JobsModel::where('end_date', '<=', $date)->where('job_status', '!=', 4)->where(array('status' => true, 'is_deleted' => false))->with(['jobapplied' => function ($q) {
			$q->with('job_shift');
		}])->orderBy('_id', 'Desc')->get()->toArray();
    	//if jobshifts found, then calculate hours worked upto now
		if (!empty($jobs)) {
			foreach ($jobs as $job) {
				if (!empty($job['jobapplied'])) {
					$workHours = $workMinutes = 0;//initial values for work minutes and hours
					foreach ($job['jobapplied'] as $job_applctn) {
						//check job shifts, update total hours if applicaytion status is not canceled
						if (!empty($job_applctn['job_shift']) && $job_applctn['job_status'] != 5 && $job_applctn['job_status'] != 3 && $job_applctn['job_status'] != 7 && $job_applctn['job_status'] != 6 && $job_applctn['job_status'] != 0) {
							//$app_hours = $common->calculate_job_app_hours($job_applctn['_id']);
							//$workHours = floatval($app_hours['job_app_hours']);							
							//update total hours worked for each job application
							$job_app = JobsapplicationModel::where('_id', $job_applctn['_id'])->first();
							//$job_app->total_hours_worked = $workHours;
							$job_app->completed_at = $date;
							$job_app->job_status = 4;
							$job_app->save();
							//check if jobseeker has completed 5 jobs or not
							$this->check_jobseeker_jobs($job_applctn['jobseeker_id']);
						} elseif ($job_applctn['job_status'] == 0) {
							//update total hours worked for each job application
							$job_app = JobsapplicationModel::where('_id', $job_applctn['_id'])->first();
							$job_app->completed_at = $date;
							$job_app->job_status = 6;//decline those users who were not accepted for this job.
							//$job_app->total_hours_worked = 0;
							$job_app->save();
						}
					}
				}
				//update job status
				//~ $job_hours = $common->calculate_total_job_hours($job['_id']);
				//~ $data['total_work_hours'] = floatval($job_hours['job_hours']);
				JobsModel::where('_id', $job['_id'])->update($data);
			}
		}
	}

    /*
	 * Added on : 14 sep 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to update user rating on completion of 5 jobs
	 * */
	function check_jobseeker_jobs($jobseeker_id)
	{
		$currentDate = Carbon::now();
		$monthStart = $currentDate->copy()->startOfMonth();
		$monthEnd = $currentDate->copy()->endOfMonth();  
		//check job applications
		$completeJobs = JobsapplicationModel::where('jobseeker_id', $jobseeker_id)->where('job_status', 4)->where('completed_at', '>=', $monthStart)->where('completed_at', '<=', $monthEnd)->count();


		if ($completeJobs >= 5) {
			//check ratign
			$userRating = UserRatings::where('type_id', $jobseeker_id)->where('type', 'jobseeker_complete_jobs')->where('created_at', '>=', $monthStart)->where('created_at', '<=', $monthEnd)->count();
			
			//check rating points to be added
			$rating_val = Setting::where(['type' => 'complete_jobs', 'key' => 'complete_job_rating'])->first();
			if ($userRating < 2) {
				if (UserRatings::create(['type' => 'jobseeker_complete_jobs', 'type_id' => $jobseeker_id, 'rated_by_id' => null, 'rating' => null, 'points' => $rating_val->value])) {
					$user_rate = User::where(array('_id' => $jobseeker_id))->value('rating');
					$user_rate += $rating_val->value;
					if ($user_rate > 5)
						$user_rate = 5;

					if ($user_rate < 0)
						$user_rate = 0;

					User::where('_id', $jobseeker_id)->update(['rating' => floatval(number_format($user_rate, 2))]);
				}

			}
		}
	}

	/*************
		Function for complete tasks after end date of jobs
	 ***************/
	public function ProcessJob()
	{
		$date = Carbon::now();
		//$date->parse($date)->format('Y-m-d H:i:s');
		//$start_date =new \MongoDB\BSON\UTCDateTime(strtotime($date) * 1000);
		$start_date = $data['job_status'] = 3;
		$jobs = JobsModel::where('total_hired', '!=', 0)->where('start_date', '<=', $date)->whereIn('job_status', [1, 2])->where(array('status' => true, 'is_deleted' => false))->orderBy('_id', 'Desc')->update($data);
	}

	/*************
        Function for Rehire job convert to Automatic before 12 hours.
        updated : 28 aug 2017, shivani - only the jobs where total hired are less than total required will be converted to automatic from rehire.
	 ***************/
	public function AutomaticJob()
	{
		$date = Carbon::now();
		$start_date = $date->addHours(12);
		$data['job_published_type'] = 1;
    	//$jobs = JobsModel::where('start_date','<=',$start_date)->where(array('status'=>true,'is_deleted'=>false,'job_published_type'=>2,'job_status'=>1))->orderBy('_id','Desc')->update($data);
		$jobs = JobsModel::where('start_date', '<=', $start_date)->where(array('status' => true, 'is_deleted' => false, 'job_published_type' => 2, 'job_status' => 1))->orderBy('_id', 'Desc')->get()->toArray();

		if (!empty($jobs)) {
			foreach ($jobs as $job) {
				if ($job['total_hired'] == 0 || $job['total_hired'] < $job['number_of_worker_required']) {
					JobsModel::where('_id', $job['_id'])->update($data);
				}
			}
		}

	}
	
	/*
	 * Added on : 5 aug 2017, shivani - to send notification to jobseeker after 72 hours of failed health quiz attempt
	 * */
	public function send_quiz_notification(CommonRepositary $common)
	{
		$notifications = SendQuizNotification::where('notification_sent', false)->get()->toArray();
		if (!empty($notifications)) {
			foreach ($notifications as $notify) {
				//send notification after 72 hours
				if ($notify['created_at'] <= \Carbon\Carbon::now()->subHours(72)) {
					//call send_notification from common repository
					$notifyJobseeker = $common->send_notification($notify['to'], 7, $job_id = null);
					if ($notifyJobseeker) {
                        //update health quiz attempt count
						User::where('_id', $notify['to'])->update(['health_quiz_attempt' => 3]);
						//update notification_sent flag 
						SendQuizNotification::where("_id", $notify['_id'])->delete();
					}
				}
			}//end foreach
		}
	}

	//to test update job hours for job application
	public function test_job_app_hours()
	{
		$date = Carbon::now();
    	//$jobs = JobsModel::where('total_hired','!=',0)->where('end_date','<=',$date)->where('job_status','!=',4)->where(array('status'=>true,'is_deleted'=>false))->orderBy('_id','Desc')->update($data);
		$start_date = $data['job_status'] = 4;
		$jobs = JobsModel::where('total_hired', '!=', 0)->where('end_date', '<=', $date)->where('job_status', '!=', 4)->where(array('status' => true, 'is_deleted' => false))->with(['jobapplied' => function ($q) {
			$q->with('job_shift');
		}])->orderBy('_id', 'Desc')->get()->toArray();
    	 //if jobshifts found, then calculate hours worked upto now
		if (!empty($jobs)) {
			foreach ($jobs as $job) {
				if (!empty($job['jobapplied'])) {
					$workHours = $workMinutes = 0;//initial values for work minutes and hours
					foreach ($job['jobapplied'] as $job_applctn) {
						//check job shifts, update total hours if applicaytion status is not canceled
						if (!empty($job_applctn['job_shift']) && $job_applctn['job_status'] != 5 && $job_applctn['job_status'] != 3) {
							foreach ($job_applctn['job_shift'] as $shift) {
								//finally calculate total minutes worked.
								$workMinutes += $shift['shift_time'];
							}
						}
						//finally calculate total minutes worked.
						$workMinutes += $shift['shift_time'];
						//if work minutes are greater than 0, then convert to hours
						if ($workMinutes > 0) {
							$workHours += intval($workMinutes / 60);
						}
						//update total hours worked for each job application
						if (JobsapplicationModel::where('_id', $job_applctn['_id'])->update(['total_hours_worked' => $workHours])) {
							$workHours = $workMinutes = 0;
						}
					}
				}
				//update job status
				JobsModel::where('_id', $job['_id'])->update($data);
			}
		}
	}
    
    /*
	 * Added on : 30 aug 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to delete jobs (which were supposed to be deleted after current shift end)
	 * */
	public function delete_jobs(CommonRepositary $common)
	{
		//check jobs
		$jobs = TempJobdelete::all()->toArray();
		if (!empty($jobs)) {
			foreach ($jobs as $job) {
				$today = Carbon::now();
				$jobDeleteTime = Carbon::parse($job['delete_at']);
				if ($today >= $jobDeleteTime) {
					//check total hours i.e.. worked by applicants on this job
					//$total_job_hours = $common->calculate_total_job_hours($job['job_id']);
					//,'total_work_hours'=>floatval($total_job_hours['job_hours'])
					//delete job now
					if (JobsModel::where('_id', $job['job_id'])->update(['is_deleted' => true, 'job_status' => 7])) {
						$jobSeekers = JobsapplicationModel::where(array('job_id' => $job['job_id']))->where('job_status', 1)->with(['applyjobuser' => function ($q) {
							$q->select('_id');
						}])->get()->toArray();
						if (!empty($jobSeekers)) {
							foreach ($jobSeekers as $jb_seekr) {
								if (!empty($jb_seekr['applyjobuser'])) {
									//update hours
									$job_app_hours = $common->calculate_job_app_hours($jb_seekr['_id']);
									//$totalJobHours = floatval(number_format($job_app_hours['job_app_hours'],2));
									$applicationJob = JobsapplicationModel::where("_id", $jb_seekr['_id'])->first();
									$applicationJob->job_status = 7;
									//$applicationJob->total_hours_worked = $totalJobHours;
									$applicationJob->withdraw_date = $today;
									if ($applicationJob->save()) {
										$common->send_notification($jb_seekr['jobseeker_id'], $type = 8, $job_id = null);
									}
								}
							}
						}
						//delete entry from temporary table too
						TempJobdelete::where('job_id', $job['job_id'])->delete();
					}
				}
			}
		}
	}

	/*
	 * Added on : 22 sep 2017
	 * DESC : to send notification to jobseekers who are in 15 km of area near to job post location
	 * */
	public function send_job_notification(CommonRepositary $common)
	{
		$notifications = SendQuizNotification::where('notification_sent', false)->where('type', 'job')->get()->toArray();
		if (!empty($notifications)) {
			foreach ($notifications as $notify) {
				//check jobseekers who are in 15 km of area near to job post location
				$users = User::where(['is_deleted' => false, 'status' => true, 'approved' => 1, 'role' => 2])->select('coordinate', '_id')->get()->toArray();

				if (!empty($users)) {
					foreach ($users as $usr) {
						//check distance 
						$userDistance = $common->distance($notify['lat'], $notify['lng'], $usr['coordinate'][1], $usr['coordinate'][0], 'K');
						if ($userDistance <= 15) {
							$common->send_notification($usr['_id'], 10, $notify['job_id']);
						}
					}
				}
				SendQuizNotification::where("_id", $notify['_id'])->delete();
			}//end foreach
		}
 		
 		//check if there is any notification to be sent before 12 or 1 hours of job start time
		$current_time = Carbon::now();
		$notifications = SendQuizNotification::whereIn('type', ['jobStart12Notification', 'jobStart1Notification'])->where('send_at', '<=', $current_time)->get()->toArray();

		if (!empty($notifications)) {
			foreach ($notifications as $notify) {
				//check jobseekers who are in 15 km of area near to job post location
				$user = User::where(['_id' => $notify['to'], 'is_deleted' => false, 'status' => true, 'approved' => 1, 'role' => 2])->first();
				if (!empty($user)) {
					$notify_type = 112;
					if ($notify['type'] == 'jobStart1Notification')
						$notify_type = 111;

					$common->send_notification($notify['to'], $notify_type, $job_id = $notify['job_id']);
				}

				SendQuizNotification::where("_id", $notify['_id'])->delete();
			}//end foreach
		}
	}

	/*
	 * Added on : 25 sep 2017, shivani - Debut infotech
	 * DESC : to check monthly earning and update rating accordingly.
	 * */
	public function check_monthly_earnings()
	{
		$users = User::where(['is_deleted' => false, 'status' => true, 'approved' => 1, 'role' => 2])->select('_id', 'rating')->get()->toArray();
		if (!empty($users)) {
			foreach ($users as $usr) {
				//check job applications and corresponding jobseekers
				$gross_earning = $this->check_user_gross_earning($usr['_id']);
				$applicantEarning = $gross_earning['earning'];
				//now check earning and update rating accordingly
				if ($applicantEarning >= 1500 && $applicantEarning <= 1999) {
					if ($usr['rating'] < 5) {
						$usr['rating'] += 0.2;
					}
				} elseif ($applicantEarning >= 2000) {
					if ($usr['rating'] < 5) {
						$usr['rating'] += 0.4;
					}
				} else {
					$usr['rating'] += 0;
				}

				if ($usr['rating'] > 5)
					$usr['rating'] = 5;

				\Log::info(json_encode(['rating' => 'true', 'user_id' => $usr['_id'], 'rating' => floatval($usr['rating'])]));
				//update user rating
				User::where('_id', $usr['_id'])->update(['rating' => floatval($usr['rating'])]);
			}
		}
	}
	
	/*
	 * Added on : 26 sep 2017, shivani - Debut infotech
	 * DESC : to add bonus on user's gross earning during that month
	 * */
	public function check_monthly_ratings()
	{
		$users = User::where(['is_deleted' => false, 'status' => true, 'approved' => 1, 'role' => 2])->select('_id', 'rating')->get()->toArray();
		if (!empty($users)) {
			foreach ($users as $usr) {
					//check user's gross earning during this month
				$gross_earning = $this->check_user_gross_earning($usr['_id']);
				$monthlyBonus = 0;
				$bonus = 0;
				if ($usr['rating'] >= 4.5 && $usr['rating'] < 5) {
						//bonus will be 2%
					$bonus = 2;
				} else if ($usr['rating'] == 5) {
						//bonus will be 3%
					$bonus = 3;
				} else {
						//no bonus
					$bonus = 0;
				}
				$bonus_amount = number_format((($gross_earning['earning'] * $bonus) / 100), 2);
				$monthlyBonus += floatval($bonus_amount);

				\Log::info(json_encode(['bonus' => 'true', 'user_id' => $usr['_id'], 'rating' => floatval($usr['rating'])]));
					//update user rating
				User::where('_id', $usr['_id'])->update(['monthly_bonus' => floatval($monthlyBonus)]);
			}
		}
	}
	
	/*
	 * DESC : to check candidate's gross earning at the end of each month.
	 * 26 sep 2017, shivani
	 * */
	function check_user_gross_earning($user_id)
	{
		$currentDate = Carbon::now();
		$monthStart = $currentDate->copy()->startOfMonth();
		$monthEnd = $currentDate->copy()->endOfMonth();  
		//check job applications and corresponding jobseekers
		$jobApplicants = JobsapplicationModel::where('jobseeker_id', $user_id)->with(['job_detail' => function ($q) {
			$q->select('start_date', 'end_date', 'salary_per_hour');
		}])->select('job_status', 'jobseeker_id', 'total_hours_worked', 'job_id')->get()->toArray();
		$applicantEarning = 0;
		if (!empty($jobApplicants)) {
			foreach ($jobApplicants as $applcnt) {
				$job_end = Carbon::parse($applcnt['job_detail']['end_date']);
				//if job has completed during this month
				if ($job_end >= $monthStart && $job_end <= $monthEnd) {
					if (isset($applcnt['total_hours_worked']) && !empty($applcnt['total_hours_worked'])) {
						$applicantEarning += ($applcnt['total_hours_worked'] * $applcnt['job_detail']['salary_per_hour']);
					}
				}
			}
		}
		return ['earning' => $applicantEarning];
	}
	
	/*
	 * Added : 28 Nov 2017
	 * DESC : to calculate shift hours on daily basis.
	 * */
	public function update_employee_shifts_hours(Request $request, CommonRepositary $common)
	{
		$currentTime = Carbon::now();
		//check shifts whoes end time has been passed.(whoes work hours has not been updated)
		$jobs = ApplicantShift::where('end_date', '<=', $currentTime)->where('total_hours_worked', 0)->orderBy('_id', 'Desc')->get()->toArray();
		$result = [];
		if (!empty($jobs)) {
			foreach ($jobs as $job) {
				$common->update_job_hours($job['job_id'], $job['app_id'], $job['_id'], $job['shift_time']);
				$result[] = ['app_hours' => JobsapplicationModel::where('_id', $job['app_id'])->value('total_hours_worked'), 'job_hours' => JobsModel::where('_id', $job['job_id'])->value('total_work_hours')];
			}
		}
		//return $result;
	}

	public function process_old_data()
	{
		//check all job application where job status is 4,5 or 7 i.e.. complete cancel and removed 
		$jobApplications = JobsapplicationModel::whereIn('job_status', [4, 5, 7])->with(['job_detail' => function ($q) {
			$q->select('start_date', 'end_date', 'salary_per_hour', 'title');
		}, 'app_shift', 'job_shift'])->get()->toArray();
		if (!empty($jobApplications)) {
			foreach ($jobApplications as $application) {
				$shifts = $application['app_shift'];
				if (empty($application['app_shift'])) {
					$shifts = $application['job_shift'];
				}
				$withdraw_date = empty($application['withdraw_date']) ? 0 : Carbon::parse($application['withdraw_date']);
				foreach ($shifts as $shft) {
					$total_hours_worked = 0;
					if (!isset($shft['total_hours_worked'])) {

						$shiftStart = Carbon::parse($shft['start_date']);
						$shiftEnd = Carbon::parse($shft['end_date']);
						//save this data to applicant shift
						
						//if employee has cancelled or has been removed from the job
						//ApplicantShift::where('app_id',$application['_id'])->delete();	
						$appShift = new ApplicantShift();
						$appShift->app_id = $application['_id'];
						$appShift->job_id = $application['job_id'];
						$appShift->start_date = $shiftStart;
						$appShift->end_date = $shiftEnd;
						if (!empty($withdraw_date)) { //old data format

							if ($shiftStart < $withdraw_date) {
								if ($shiftEnd < $withdraw_date) {
									$workMinutes = $shft['shift_time'];//if job has been cancelled after the shift end time.
								} else {
									$workMinutes = $shiftStart->diffinMinutes($withdraw_date);
								}

								$total_hours_worked = floatval(number_format(($workMinutes / 60), 2));

								$appShift->total_hours_worked = $total_hours_worked;
									
								//$appShift = new ApplicantShift($applicant);
								$appShift->save();
							}
						} else {
							$total_hours_worked = floatval(number_format(($shft['shift_time'] / 60), 2));
							//save this data to applicant shift
							$appShift->total_hours_worked = $total_hours_worked;
							//$appShift = new ApplicantShift($applicant);
							$appShift->save();
						}
					}
				}
			}

		}
	}
	
	/*
	 * Temporary fnunction to reset employee data
	 * */
	//~ public function reset_employee_data(){
		//~ $users = ['test_acc5@gmail.com','test_acc3@gmail.com','test_acc2@gmail.com'];
		//~ $jobseeker_ids = User::whereIn('email',$users)->pluck('_id')->toArray();
		//~ //check applications
		//~ $applications = JobsapplicationModel::whereIn('jobseeker_id',$jobseeker_ids)->pluck('_id')->toArray();
		//~ //delete applications
		//~ JobsapplicationModel::whereIn('jobseeker_id',$jobseeker_ids)->delete();
		//~ //delete applicant shifts
		//~ ApplicantShift::whereIn('app_id',$applications)->delete();
	//~ }
	
	/*
	 * To process old jobseekers data according to new flow i.e.. multiple category and industry
	 * */
	public function reset_jobseekers_category_industry()
	{
		$users = User::where(['is_deleted' => false, 'status' => true, 'approved' => 1, 'role' => 2])->select('_id', 'category_id', 'category_object')->get()->toArray();
		if (!empty($users)) {
			foreach ($users as $usr) {
				$new_data = [];
				if (!is_array($usr['category_id'])) {
					$new_data['category_id'] = [$usr['category_id']];
					$new_data['industry_id'] = [$usr['category_object']['0']['industry_id']];
					User::where('_id', $usr['_id'])->update($new_data);
				}
			}
		}
	}
	
	/*
	 * 1. All old employees category_id field should be an array.
	 * 2. Add industry_id and industry fields based on their category selection.
	 * 3. add industry_name field to all employers category_details field.
	 * 4. all old data job job shifts and job applications have to be saved into app_shifts.
	 * */
	/*
	 * To process old jobseekers data according to new flow i.e.. multiple category and industry
	 * */
	public function reset_joblocation()
	{
		$jobs = JobsModel::select('_id', 'location')->get()->toArray();
		if (!empty($jobs)) {
			$counter = 1;
			$new_data = [];
			foreach ($jobs as $jb) {

				if (is_array($jb['location'])) {
					$lng = floatval($jb['location']['lng']);
					$lat = floatval($jb['location']['lat']);
					$new_data['locaton'] = ['lng' => $lng, 'lat' => $lat];
					JobsModel::where('_id', $jb['_id'])->update($new_data);
					$counter++;
				}
			}
			return $new_data;
		}
	}
	
	/*
	 * To process old jobseekers data according to new flow i.e.. multiple category and industry
	 * */
	public function reset_user_emails(CommonRepositary $common)
	{
		$users = User::where(['is_deleted' => false, 'status' => true, 'approved' => 1, 'role' => 2])->select('device_token', '_id', 'email')->get()->toArray();
		if (!empty($users)) {
			$email = 'madwall_beta_';
			$counter = 1;
			foreach ($users as $usr) {
				//send notification regarding app updation
				if (!empty($usr['device_token'])) {
					$common->send_notification($usr['_id'], 12, null);
				}
			}
		}
	}

	public function reset_user_coordinates()
	{
		$users = User::where(['is_deleted' => false, 'status' => true, 'approved' => 1, 'role' => 2])->select('lng', '_id', 'lat')->get()->toArray();
		if (!empty($users)) {
			foreach ($users as $usr) {
				$lng = floatval($usr['lng']);
				$lat = floatval($usr['lat']);
				$new_data['lng'] = $lng;
				$new_data['lat'] = $lat;
				User::where('_id', $usr['_id'])->update($new_data);
			}
		}
	}
}
