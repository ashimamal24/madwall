<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ContactUs;
use App\Model\User;
use Carbon\Carbon;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*======================================
            Display a listing of contacts.
    ========================================*/
    public function index()
    {
        $contacts = ContactUs::get();
        return view('managecontact/listingcontacts', ['contacts' => $contacts, 'active' => 'contacts']);
    }

    /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterContacts(Request $request)
    {
        $basearray = ContactUs::where(array('is_deleted' => false))->orderBy('created_at', 'desc');
        $totalcount = ContactUs::where(array('is_deleted' => false))->count();

        //~ if( isset( $request->name ) && !empty( $request->name  ) ) {
            //~ $basearray->where( 'name','LIKE',$request->name.'%' );
        //~ }
        
        //~ if( isset( $request->email ) && !empty( $request->email  ) ) {
            //~ $basearray->where( 'email','LIKE',$request->email.'%' );
        //~ }
        
        /*if( isset( $request->status ) && $request->status != '') {
            $basearray->where( 'status' ,(bool)$request->status );
        }*/

        if (isset($request->messagefrom)) {
            $basearray->where('type', $request->messagefrom);
        }

        if (isset($request->request_status)) {
            $basearray->where('request_status', $request->request_status);
        }

        /** If Single set **/
        //~ if( isset($request->requested_at_from ) || isset($request->requested_at_to )  ) {
            //~ $start = Carbon::parse( $request->requested_at_from );
            //~ $start2 = Carbon::parse( $request->requested_at_to );
            //~ $start_day_start = $start->startOfDay();
            //~ $start_day_end = $start2->endOfDay();
            //~ $basearray -> where( 'created_at', '>=', $start_day_start )->where( 'created_at', '<=', $start_day_end );
        //~ }
            //~ /** If Both set **/
        //~ if( isset($request->requested_at_from ) && isset( $request->requested_at_to ) ) {
            //~ $start_both = Carbon::parse( $request->requested_at_from );
            //~ $end_both = Carbon::parse( $request->requested_at_to );
            //~ $start_day_start_both = $start_both->startOfDay();
            //~ $start_day_end_both = $end_both->endOfDay();
            //~ $basearray->where( 'created_at', '>=', $start_day_start_both )->where( 'created_at', '<=', $start_day_end_both );
        //~ }

       /* $order = $request->get('order');
        if($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
                $basearray->orderBy( 'name','asc');
            } elseif($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
                $basearray->orderBy('name','desc');
            }
        else{
                $basearray->orderBy('_id','desc');
        }*/
        $counttotal = ContactUs::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
       // $GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $contacts) {
            $contactId = $contacts['_id'];
            $view_link = '<a contactId="' . $contactId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a>';

            if (isset($contacts['user_id'])) {
                $first_name = User::where(array('_id' => $contacts['user_id']))->value('first_name');
                $last_name = User::where(array('_id' => $contacts['user_id']))->value('last_name');
                $user_name = $first_name . " " . $last_name;
            } else if (isset($contacts['name'])) {
                $user_name = $contacts['name'];
            } else {
                $user_name = 'N/A';
            }


            if (isset($contacts['user_id'])) {
                $email = User::where(array('_id' => $contacts['user_id']))->value('email');
            } else if (isset($contacts['email'])) {
                $email = $contacts['email'];
            } else {
                $email = 'N/A';
            }

            if (isset($contacts['file_url'])) {
                $file = '<a href="' . $contacts['file_url'] . '">' . $contacts['file_name'] . '</a>';
            } else {
                $file = 'N/A';
            }

            if (isset($contacts['created_at'])) {
                $created_at = Carbon::parse($contacts['created_at'])->format('d') . ' ' . Carbon::parse($contacts['created_at'])->format('F') . ', ' . Carbon::parse($contacts['created_at'])->format('Y');
            } else {
                $created_at = 'N/A';
            }
			
			
			/* 16 aug 2017 -- to display employer contact number --- */
            if (isset($contacts['user_id']) && !empty($contacts['user_id'])) {
				//check role 
                $userRole = User::where(array('_id' => $contacts['user_id']))->select('company_code', 'phone', 'country_code', 'company_contact', 'role')->first();
                $mobile_num = User::where(array('_id' => $contacts['user_id']))->value('phone');
                $country_code = User::where(array('_id' => $contacts['user_id']))->value('country_code');
                $phone = $country_code . " " . $mobile_num;


                if (!empty($userRole) && $userRole->role == 3) {
					//for employer
                    if (isset($userRole->company_code) && !empty($userRole->company_contact) && $userRole->company_contact != null) {
                        $phone = $userRole->company_code . " " . $userRole->company_contact;
                    }
                    if (isset($userRole->country_code) && !empty($userRole->phone)) {
                        $phone = $userRole->country_code . " " . $userRole->phone;
                    }
                }

            } else {
                $phone = 'N/A';
            }

            if (isset($contacts->content) && !empty($contacts->content)) {
                $length = strlen($contacts->content);
                if ($length > 100) {
                    $content = substr($contacts->content, 0, 15) . '...';
                } else {
                    $content = substr($contacts->content, 0, 15);
                }
            }

            if (isset($contacts['type']) && !empty($contacts['type'])) {
                switch ($contacts['type']) {
                    case "mobile":
                        $type = 'Employee';
                        break;
                    case "website":
                        $type = 'Employer';
                        break;
                    case "guest":
                        $type = 'General';
                        break;
                    default:
                        $type = 'N/A';
                }
            }
            
            //show subject also,
            $subject = 'N/A';
            if (isset($contacts->subject) && !empty($contacts->subject)) {
                $subject = $contacts->subject;
            }
            $request_status = 'N/A';
            if (isset($contacts->request_status) && !empty($contacts->request_status)) {
                $request_status = $contacts->request_status;
            }


            $GLOBALS['data'][] = array($i, $user_name, $email, $phone, $subject, $content, $file, $created_at, $request_status, $type, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }

    /*===============================
        View Particular Skill Detail 
    =================================*/
    public function viewContact(Request $request)
    {
        $id = $request->id;
        $user_id = ContactUs::where(array('_id' => $id))->value('user_id');
        if (isset($user_id)) {
            $user_detail = User::find($user_id)->toArray();
            $user_name = $user_detail['first_name'] . " " . $user_detail['last_name'];
            $email = $user_detail['email'];
            $data['username'] = $user_name;
            $data['email'] = $email;
            $data['phone'] = $user_detail['country_code'] . " " . $user_detail['phone'];
        } else {
            $data['guest'] = 'guest';
        }


        $data['reslutset'] = ContactUs::where(array('_id' => $id))->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
    
    
    /*
     * Added on : 9 nov 2017
     * Added by : shivani, Debut infotech
     * DESC : to update contact query status as Closed.
     * */
    public function close_query(Request $request)
    {
        $contactQuery = ContactUs::where(array('_id' => $request->id))->first();
        if ($contactQuery) {
            if ($contactQuery->update(['request_status' => 'closed'])) {
                return response()->json(['success' => true, 'message' => 'Status updated successfully']);
            } else {
                return response()->json(['success' => false, 'message' => 'Sorry, something went wrong.']);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'Sorry, contact request not found.']);
        }
    }
}
