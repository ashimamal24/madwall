<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\TimeSlot;
use App\Model\CategoryModel;
use App\Model\SkillModel;
use App\Model\Industry;
use App\Model\Token;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Admin\TimeSlotRequest;
use App\Http\Requests\Admin\BlockUserRequest;
use App\Http\Requests\Admin\ApproveJobseekerRequest;
use App\Model\JobsModel;
use App\Model\JobsapplicationModel;
use Illuminate\Support\Facades\Validator;
use App\Model\EmailTemplate;
use Auth;
use App\Jobs\SendOtpEmail;
use DB;
use Carbon\Carbon;
use Excel;
use PDF;
use JWTAuth;
use Mail;
use App\Http\Repositary\CommonRepositary;//28 july 2017, shivani

class JobSeekerController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }
    
    /*===================================
        Listing of Waitlising Employee
    =====================================*/
    public function getWaitlistEmploye(CommonRepositary $common)
    {
        $waitlisting = User::get();
       /* foreach($waitlisting as $waitlisting){
                $userId = $waitlisting['_id'];
        //print_r($waitlisting['_id']);
           $slot = TimeSlot::where(array('user_id' => $userId,'accepted'=>true))->first();

if($slot){
    $current_date = date("Y-m-d h:i:s");
  //  $start_time = Carbon::parse($slot['start_time']);
   // print_r($slot);
//print_r($slot['start_time']);

    //$startDate = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($slot['start_time'])), $_COOKIE['client_timezone']);
    //$endDate = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($slot['end_time'])), $_COOKIE['client_timezone']);


    $start_time = Carbon::parse($slot['start_time']);
            $current_date = date("Y-m-d h:i:s");
            //if ($picker_date > $current_date || $picker_date == $current_date) {

          //  }

print_r($start_time);
print_r('<br>');

}


        }
        die;


            if($timeslot ){

                print_r($timeslot);
             
            }die;*/
        return view('jobseekers/jobwaitlisting', ['waitlisting' => $waitlisting, 'active' => 'Jobseekerswaitlist']);
    }

    /*===================================
        Listing of Approved Employee
    =====================================*/
    public function getApprovedEmploye()
    {
        $approvedjobseeker = User::get();
        return view('jobseekers/jobApprovedlisting', ['approvedjobseeker' => $approvedjobseeker, 'active' => 'approvedjobseeker']);
    }
    
    
     /*=======================================
        Added on : 11 sep 2017, shivani
     * to hard delete employer (temporary basis - to remove fake accounts)
    =========================================*/
    public function getDeleteEmployee($id)
    {
        $userDetail = User::where(array('_id' => $id))->first();
        if (!empty($userDetail)) {
            User::where(array('_id' => $id))->delete();
            flash()->success('Employee has been deleted');
        }
        return redirect(url()->previous());
    }

     /*=======================================
        Added on : 11 sep 2017, shivani
     * to hard delete employer (permanent basis - to remove fake accounts)
    =========================================*/
    public function getSoftDeleteEmployee($id)
    {
        $userDetail = User::where(array('_id' => $id))->first();
        if (!empty($userDetail)) {
           // User::where(array('_id' => $id))->delete();
            $data=array('is_deleted'=>true);
            $userDetail->update($data);
            flash()->success('Employee has been deleted');
        }
        return redirect(url()->previous());
    }


    /*===================================
        Listing of Declined Employee
    =====================================*/
    public function getBlockedEmploye()
    {
        $declinedjobseeker = User::get();
        $active = 'declinedjobseeker';
        return view('jobseekers/blockEmployeeslist', ['declinedjobseeker' => $declinedjobseeker, 'active' => 'declinedjobseeker']);
    }

    /*=============================================
        View of Assigning Time Slot Containing Tab
    ===============================================*/
    public function assignTimeSlot($id, CommonRepositary $common)
    {
        $detail_waitlist = User::where(array('_id' => $id))->with('timeslots')->first()->toArray();

        if(isset($detail_waitlist['agreement_status']) && $detail_waitlist['agreement_status'] == 2 ){

             $detail_waitlist['downloadEmplyoeeContract'] = url('/agreement/downloadEmplyoeeContract/').'/'.$id;
        }

        if(isset($detail_waitlist['emergency_contact']) && $detail_waitlist['emergency_contact'] == 2 ){

            $detail_waitlist['downloadEmergencyContact'] = url('/agreement/downloadEmergencyContact/').'/'.$id;
        }

        if(isset($detail_waitlist['direct_deposit']) && $detail_waitlist['direct_deposit'] == 2  ){

            $detail_waitlist['downloadDirectDeposit'] = url('/agreement/downloadDirectDeposit/').'/'.$id;
        }
        if(!isset($detail_waitlist['sin_number'])){

            $detail_waitlist['sin_number'] = '';
        }

        //print_R($detail_waitlist['sin_number']);die;
       //if ($detail_waitlist['user_slot_accepted']) {  //"sin_number" : "123456",


        if(isset($detail_waitlist['opentokSessionId'])){

            #Get Video URL
            #=====================
            $detail_waitlist['opentokVideoDownloadUrl'] = $common->downloadOpentokVideo($detail_waitlist['opentokSessionId']);
        }


        if (isset($detail_waitlist['agreement_send']) && $detail_waitlist['agreement_send'] == true && $detail_waitlist['user_slot_accepted'] == true ) {
            
            $categories = CategoryModel::where(array('status' => true, 'is_deleted' => false, 'type' => 'category'))->orderBy('name')->pluck('name', '_id')->toArray();
            $skills = SkillModel::where(array('status' => true, 'is_deleted' => false, ))->orderBy('name')->pluck('name', '_id')->toArray();
            $industries = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();

            return view('jobseekers/ApproveTimeSlot', ['detail' => $detail_waitlist, 'categories' => $categories, 'skills' => $skills, 'industries' => $industries]);
        } else {
            return view('jobseekers/assigntimeslots', ['detail' => $detail_waitlist]);
        }
    }

   /*==========================================
        Assigne Time Slot to Waiting Jobseeker 
        3 aug 2017, to call new function for send notification
    ===========================================*/
    public function saveTimeSlot(Request $request, CommonRepositary $common)
    {
                    
       // print_R($request);die;

        $slot = $request->input('slots')[0];
        $id = $request->input('id');
        $error = 0;
        $slots = $request->input('slots');
        date_default_timezone_set($request->input('timezone'));
        foreach ($slots as $slot) {
            $picker_date = Carbon::parse($slot['start_time']);
            $current_date = date("Y-m-d h:i:s");
            if ($picker_date > $current_date || $picker_date == $current_date) {
                $error = 0;
            } else {
                $error = 1;
            }
        }

        if ($error == 0) {

            /*$count_users = TimeSlot::where(array('user_id' => $id))->count('user_id');
            if ($count_users == 3) {
                TimeSlot::where(array('user_id' => $id))->delete();
            }
               flash()->error('You have already assigned timeslot for this employee');
                return response()->json(['success' => false]);
            } else {*/

                TimeSlot::where(array('user_id' => $id))->delete();

                date_default_timezone_set($request->input('timezone'));
                if ($slots && count($slots)) {
                    foreach ($slots as $slot) {
                        $timeslot = new TimeSlot;
                        $startDate = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($slot['start_time'])), $_COOKIE['client_timezone']);
                        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($slot['end_time'])), $_COOKIE['client_timezone']);
						
                        //$slots_array =array("user_id"=>$id,"status"=>"pending","accepted"=>false,"start_time"=>$startDate,"end_time"=>$endDate,"is_deleted"=>false);

 
                        $timeslot->user_id = $id;
                        $timeslot->status = "pending";
                        $timeslot->accepted = false;
                        $timeslot->start_time = $startDate;
                        $timeslot->end_time = $endDate;
                        $timeslot->is_deleted = false;


                        $timeslot->save();
                    }

                    $user = User::where('_id', $id)->first();
                    if ($user['profile_complete'] == 7) {
                                $user->update(array('profile_complete'=>6));
                    }

                    
                    ///$user_id, $jobid = Null, $time_slot = Null, $approved = 1, $decline = 1, $block = 1  
                    flash()->success('Interview timeslots has been sent successfully to the employee');
                    //$common->sendNotification($id,$jobid = Null,$time_slot = 1,$approved = 0,$decline = 0,$block = 0);
                    //3 aug 2017
                    $common->send_notification($id, $type = 4, $job_id = null);
                    return response()->json(['success' => true]);
                } else {
                    flash()->error('Something went wrong');
                    return response()->json(['success' => false]);
                }
                
                //~ if( TimeSlot::save( $slots_array ) ){
                    
                //~ } else {
                    //~ flash()->error( 'Something went wrong' );
                    //~ return response()->json( ['success'=>false] );
                //~ }
          //  }
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }



    /*===================================
     * Select Subcatergories using ajax
     ====================================*/
    public function selectSubcategory($id)
    {
        $categories = explode(',', $id);
        $subcategories = CategoryModel::where(array('status' => true, 'is_deleted' => false, 'type' => 'subcategory'))->whereIn('category_id', $categories)->orderBy('name')->select("name", "_id", "category_name")->get()->toArray();

        return json_encode($subcategories);
    }

    /*==========================
     * Select Skills using ajax
     ===========================*/
    public function selectSkills($id)
    {
        //$subcat = explode(",",$id);
        $subcat_id = explode(",", $id);
        $skilldata = $this->get_skills($subcat_id);
        $skills = CategoryModel::where(array('status' => true, 'type' => 'subcategory'))->whereIn('_id', $subcat_id)->select('mandatory_skills')->get()->toArray();
        $mId = [];
        foreach ($skills as $k => $value) {
            if (isset($value['mandatory_skills']) && !empty($value['mandatory_skills'])) {
                foreach ($value['mandatory_skills'] as $k => $v) {
                    $mId[] = $v;
                }
            }
        }

        return response()->json(['skills' => $skilldata, 'mandatory' => $mId]);
        //return json_encode($skills);//json_encode($uniqueskills);
    }

    function get_skills($subcat_id)
    {
		//$subcat_id = $request->value;
        $skills = CategoryModel::where(array('status' => true, 'type' => 'subcategory'))->whereIn('_id', $subcat_id)->select('skills', 'mandatory_skills')->get()->toArray();

        $skilldata = [];
        $sId = [];
        $mId = [];
        foreach ($skills as $k => $value) {
			//$sId = $value['skills']; 
            if (isset($value['skills']) && !empty($value['skills'])) {
				//$mId = $value['mandatory_skills'];  
                foreach ($value['skills'] as $k => $v) {
                    $sId[] = $v;
                }

            }
            if (isset($value['mandatory_skills']) && !empty($value['mandatory_skills'])) {
				//$mId = $value['mandatory_skills'];  
                foreach ($value['mandatory_skills'] as $j => $m) {
                    $mId[] = $m;
                }
            }
        }

        if ($skills) {
            if (!empty($mId)) {
                $sId = array_merge($sId, $mId);
            }
            $skilldata = SkillModel::whereIn('_id', $sId)->where('status', true)->select('_id', 'name')->get()->toArray();
        }

        return $skilldata;
    }


     /*================================================
     * Update the specified Aproved User.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     ==================================================*/
    public function editApproveWaitlistingJobseekerData(ApproveJobseekerRequest $request)
    {

        $skills = $request['skills'];
        $subcategories = $request['subcategories'];
        $category_id = $request['category_id'];
        $industry_id = $request['industry_id'];
        $selected_industries = Industry::whereIn('_id', $industry_id)->get()->toArray();
        $selected_category = CategoryModel::whereIn('_id', $category_id)->get()->toArray();


        $selected_subcategories = CategoryModel::whereIn('_id', $subcategories)->get()->toArray();

        $selected_skills = SkillModel::whereIn('_id', $skills)->get()->toArray();

        $proofs_name_array = $request['proofs_name'];
        $proofs_url_array = $request['proofs_url'];
        $proofs_value_array = $request['proofs_value'];
        for ($j = 0; $j <= 1; $j++) {
            if ($proofs_url_array[$j]) {
                $id_proof[$j]['name'] = $proofs_name_array[$j];
                $id_proof[$j]['url'] = $proofs_url_array[$j];
                $id_proof[$j]['value'] = $proofs_value_array[$j];
            }
        }
        $description_array = $request['certificate_description'];
        $url_array = $request['certificate_hidden_url'];
        $certificate_name_array = $request['certificate_hidden_name'];
        if (isset($url_array[0]) || isset($url_array[1]) || isset($url_array[2]) || isset($url_array[3]) || isset($url_array[4])) {
            for ($i = 0; $i <= 4; $i++) {
                if ($url_array[$i]) {
                    $finaldata[$i]['url'] = $url_array[$i];
                    $finaldata[$i]['description'] = $description_array[$i];
                    $finaldata[$i]['name'] = $certificate_name_array[$i];
                }
            }
        } else {
            $finaldata = null;
        }

        $user = User::where('_id', $request['userid'])->first();

        if ($user['approved'] == 3) {
            flash()->error('You have already blocked this employee');
            return response()->json(['success' => false]);
        } else {
            if ($user->update(array('rating' => $request['rating'], 'id_proofs' => $id_proof, 'category_name' => $request['category_name'], 'category_id' => $request['category_id'], 'skills' => $selected_skills, 'additional_documents' => $finaldata, 'cv_name' => $request['hidden_cv_name'], 'cv_url' => $request['hidden_cv_url'], 'subcategories' => $selected_subcategories, 'category_object' => $selected_category, 'skills_ids' => $skills, 'industry' => $selected_industries, 'industry_id' => $industry_id))) {
                flash()->success('Employee updated successfully');
                return response()->json(['success' => true]);
            } else {
                flash()->error('Something went wrong');
                return response()->json(['success' => false]);
            }
        }

    }

    /*============================================================
     * Show the form for editing the specified Approved Jobseeker.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * updated : 14 oct 2017, shivani
     =============================================================*/

    public function updateApprovedJobSeeker(Request $request, $id, CommonRepositary $common)
    {
        $edit_approved_jobseeker = User::where(array('_id' => $id))->first();


        if(isset($edit_approved_jobseeker['agreement_status']) && $edit_approved_jobseeker['agreement_status'] == 2 ){

            $edit_approved_jobseeker['downloadEmplyoeeContract'] = url('/agreement/downloadEmplyoeeContract/').'/'.$id;
        }

        if(isset($edit_approved_jobseeker['emergency_contact']) && $edit_approved_jobseeker['emergency_contact'] == 2 ){

            $edit_approved_jobseeker['downloadEmergencyContact'] = url('/agreement/downloadEmergencyContact/').'/'.$id;
        }

        if(isset($edit_approved_jobseeker['direct_deposit']) && $edit_approved_jobseeker['direct_deposit'] == 2){

              $edit_approved_jobseeker['downloadDirectDeposit'] = url('/agreement/downloadDirectDeposit/').'/'.$id;
        }


         if(isset($edit_approved_jobseeker['opentokSessionId'])){

            #Get Video URL
            #=====================
            $edit_approved_jobseeker['opentokVideoDownloadUrl'] = $common->downloadOpentokVideo($edit_approved_jobseeker['opentokSessionId']);
        }


        $job_application = JobsapplicationModel::with(array('job_shift', 'job'))->where(array('jobseeker_id' => $id))->whereIn("job_status", [4, 5, 7])->where('total_hours_worked', '!=', null)->get()->toArray();
        if (isset($job_application)) {
            $completed_jobs = count($job_application);
        } else {
            $completed_jobs = 0;
        }
        foreach ($job_application as $applications) {
            $salary = $applications['job']['salary_per_hour'];
            $hours[] = $applications['total_hours_worked'];
            $earning[] = $applications['total_hours_worked'] * $salary;
        }
        if (isset($hours)) {
            $total_working_hour = array_sum($hours);
        } else {
            $total_working_hour = 0;
        }
        if (isset($earning)) {
            $total_earning = array_sum($earning);
        } else {
            $total_earning = 0;
        }

        $industries = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        //selected industries 
        $selected_industry = [];
        $indstry_ids = [];
        if (!empty($edit_approved_jobseeker['industry'])) {
            foreach ($edit_approved_jobseeker['industry'] as $indstry) {
                $selected_industry[$indstry['_id']] = $indstry['name'];
                $indstry_ids[] = $indstry['_id'];
            }
        }




        $industries = array_unique(array_merge($industries, $selected_industry));
        
        // Select Category Start
        $usedcategory = '';
        $systemcategories = CategoryModel::where(array('type' => 'category', 'status' => true))->whereIn('industry_id', $indstry_ids)->orderBy('name')->select('name', '_id', 'industry_name')->get()->toArray();

        $usedcategory = [];
        foreach ($edit_approved_jobseeker['category_object'] as $cat) {
            $usedcategory[] = ['_id' => $cat['_id'], 'name' => $cat['name'], 'industry_name' => $cat['industry_name']];
        }


        $allcategories = array_unique(array_merge($systemcategories, $usedcategory), SORT_REGULAR);
		//sort all categories according to industry name
        usort($allcategories, function ($i, $j) {
            $a = $i['industry_name'];
            $b = $j['industry_name'];
            return strcmp($a, $b);
        });
		
        // Select Category End

        $sub_categories = CategoryModel::where(array('type' => 'subcategory'))->whereIn('category_id', $edit_approved_jobseeker['category_id'])->orderBy('name')->select('name', '_id', 'category_name')->get()->toArray();

        // Select Subcategory Start
        $used_subcategories = [];
        foreach ($edit_approved_jobseeker['subcategories'] as $subcategories) {
            $used_subcategories[] = ['_id' => $subcategories['_id'], 'name' => $subcategories['name'], 'category_name' => $subcategories['category_name']];
        }




        if (isset($edit_approved_jobseeker['subcategories'])) {
            foreach ($edit_approved_jobseeker['subcategories'] as $subcategory) {
                $selected_subcategory[] = $subcategory['_id'];
            }
        } else {
            $selected_subcategory = null;
        }


        $allsubcategories = array_unique(array_merge($sub_categories, $used_subcategories), SORT_REGULAR);
        
        //sort all categories according to industry name
        usort($allsubcategories, function ($i, $j) {
            $a = $i['category_name'];
            $b = $j['category_name'];
            return strcmp($a, $b);
        });
		//$allsubcategories;
		
		
        // Select Subcategory End

        
        // Select Skill Start
        $used_skills = [];
        $allskills = [];
        foreach ($edit_approved_jobseeker['skills'] as $skills) {
            $used_skills[$skills['_id']] = $skills['name'];
        }
        
        // Skill from subcategory onject in user model
        foreach ($edit_approved_jobseeker['subcategories'] as $selectedsubcategories) {
            if (!empty($selectedsubcategories['skill_object'])) {
                foreach ($selectedsubcategories['skill_object'] as $skill_object) {
                    $allskills[$skill_object['_id']] = $skill_object['name'];
                }
            }

        }

        foreach ($edit_approved_jobseeker['skills'] as $skill) {
            $selected_skill[] = $skill['_id'];
        }
        $allskills = array_merge($allskills, $used_skills);
        // Select Skill End
        
        //industries
        //=========== ->with(array('relatedcategory'=>function($q){ $q->where( array('status' => true,'is_deleted' => false ) )->get();}))




        return view('jobseekers/editApprovedJobSeeker', ['allcategories' => $allcategories, 'sub_categories' => $allsubcategories, 'selected_subcategory' => $selected_subcategory, 'edit_approved_jobseeker' => $edit_approved_jobseeker, 'allskills' => $allskills, 'selected_skill' => $selected_skill, 'completed_jobs' => $completed_jobs, 'total_working_hour' => $total_working_hour, 'total_earning' => $total_earning, 'industries' => $industries]);
    }

    /*===============================
        Approve Waiting Jobkseeker
    ===============================*/
    public function approveWaitlistingJobseeker(ApproveJobseekerRequest $request, CommonRepositary $common)
    {

        $mwuserid = $common->get_uniq_id('emp');//"EMP".date("y").mt_rand(10000, 99999);
        $skills = $request['skills'];
        $subcategory_id = $request['subcategories'];
        $category_id = $request['category_id'];

        $industry_id = $request['industry_id'];
        $selected_industries = Industry::whereIn('_id', $industry_id)->get()->toArray();
        $selected_category = CategoryModel::whereIn('_id', $category_id)->get()->toArray();



        $selected_skills = SkillModel::whereIn('_id', $skills)->get()->toArray();
        $selected_subcategories = CategoryModel::whereIn('_id', $subcategory_id)->get()->toArray();
        //$selected_category = CategoryModel::where('_id', $category_id )->get()->toArray();
        $proofs_name_array = $request['proofs_name'];
        $proofs_url_array = $request['proofs_url'];
        $proofs_value_array = $request['proofs_value'];
        for ($j = 0; $j <= 1; $j++) {
            if ($proofs_url_array[$j]) {
                $id_proof[$j]['name'] = $proofs_name_array[$j];
                $id_proof[$j]['url'] = $proofs_url_array[$j];
                $id_proof[$j]['value'] = $proofs_value_array[$j];
            }
        }

        $description_array = $request['certificate_description'];
        $url_array = $request['certificate_hidden_url'];
        $certificate_name_array = $request['certificate_hidden_name'];
        if (isset($url_array[0]) || isset($url_array[1]) || isset($url_array[2]) || isset($url_array[3]) || isset($url_array[4])) {
            for ($i = 0; $i <= 4; $i++) {
                if ($url_array[$i]) {
                    $finaldata[$i]['url'] = $url_array[$i];
                    $finaldata[$i]['description'] = $description_array[$i];
                    $finaldata[$i]['name'] = $certificate_name_array[$i];
                }
            }
        } else {
            $finaldata = null;
        }

        $user = User::where('_id', $request['userid'])->first();//->update( array('accepted'=>1 ) );

        $useremail = User::where('_id', $request['userid'])->value('email');
        if ($user->update(array('approved' => 1, 'id_proofs' => $id_proof, 'category_id' => $request['category_id'], 'skills' => $selected_skills, 'additional_documents' => $finaldata, 'cv_name' => $request['hidden_cv_name'], 'cv_url' => $request['hidden_cv_url'], 'subcategories' => $selected_subcategories, 'category_object' => $selected_category, 'skills_ids' => $skills, 'mwuserid' => $mwuserid, 'sin_number' => $request['sin_number'], 'status' => true, 'industry' => $selected_industries, 'industry_id' => $industry_id))) {		
			//3 aug 2017, shivani - to call new function to send notification
            $common->send_notification($request['userid'], $type = 1, $job_id = null);



            $template = EmailTemplate::find('5a097e791a11fe56eb048c1d');

            $body = $template->content;
            $subject = $template->subject;

            Mail::send('emails.verify', ['content' => $body, 'user' => $useremail], function ($m) use ($useremail, $subject) {
                $m->to($useremail, 'name')->subject($subject);
            });

            flash()->success('Employee approved successfully. An email has been sent to the employee regarding profile approval');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
        //~ $isApprove = User::where( '_id', $request['userid'] )->value('approved');
        //~ switch ( $isApprove ) {
            //~ case 1:
                //~ flash()->error( 'You have already approved this employee' );
                //~ return response()->json( ['success' => false ]);
                //~ break;
            //~ case 2:
                //~ flash()->error( 'Employee was already declined earlier' );
                //~ return response()->json( ['success' => false ]);
                //~ break;
            //~ case 3:
                //~ flash()->error( 'You have already blocked this employee' );
                //~ return response()->json( ['success' => false ]);
                //~ break;
            //~ default:
                
        //~ }
    }

    /*===============================
        Decline Waiting Jobkseeker
    ================================*/
    public function declineWaitlistingJobseeker(Request $request, CommonRepositary $common)
    {
        $decline_user = User::where('_id', $request->input('id'))->first();
        $common->getDeclinedToken($request->input('id'));
        if ($decline_user->update(array('status' => false, 'approved' => 2))) { 
			//3 aug 2017, shivani - to call new function to send notification
            $common->send_notification($request->input('id'), $type = 2, $job_id = null);
            flash()->success('Employee declined successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
        //~ switch ( $decline_user['approved'] ) {
            //~ case 1:
                //~ flash()->error( 'You have already approved this employee' );
                //~ return response()->json( ['success' => false ]);
                //~ break;
            //~ case 2:
                //~ flash()->error( 'Employee was already declined earlier' );
                //~ return response()->json( ['success' => false ]);
                //~ break;
            //~ default:
                
        //~ }
    }

    /*===============================
        Block Jobkseeker
    ================================*/
    public function blockUser(BlockUserRequest $request, CommonRepositary $common)
    {
        $block_user = User::where('_id', $request->input('userid'))->first();
        switch ($block_user['approved']) {
            case 2:
                //flash()->error(  );
                return response()->json(['success' => false, 'message' => 'You have already declined this employee']);
                break;
            case 3:
               // flash()->error(  );
                return response()->json(['success' => false, 'message' => 'Employee was already blocked earlier']);
                break;
            default:
                if ($block_user->update(array('status' => false, 'approved' => 3, 'reason_for_block_usr' => $request['reason_for_block_usr']))) { 
                    
                    //$common->sendNotification($request->input('userid'),$jobid = Null,$time_slot = Null,$approved = 0,$decline = 0,$block = 1);


                    //3 aug 2017, shivani - to call new function to send notification
                    $common->send_notification($request->input('userid'), $type = 3, $job_id = null);
                    $template = EmailTemplate::find('59a4f9b44299752faa19f371');
                    $find = array('@name@', '@reason@');
                    $values = array($block_user['first_name'], $request['reason_for_block_usr']);
                    $body = str_replace($find, $values, $template->content);
                    $this->dispatch(new SendOtpEmail($body, $block_user['email'], $template));
                    //flash()->success( 'User blocked successfully' );
                    return response()->json(['success' => true, 'message' => 'Employee blocked successfully']);
                } else {
                    //flash()->error( 'Something went wrong' );
                    return response()->json(['success' => false, 'message' => 'Something went wrong']);
                }
        }
    }

    /*=====================
        Unblock Jobkseeker
    =======================*/
    public function unBlockUser(request $request)
    {
        $unblock_user = User::where('_id', $request->input('userid'))->first();
        switch ($unblock_user['approved']) {
            case 1:
                flash()->error('Employee was already unblocked earlier');
                return response()->json(['success' => false]);
                break;
            case 2:
                flash()->error('You have already declined this employee');
                return response()->json(['success' => false]);
                break;
            default:
                if ($unblock_user->update(array('status' => true, 'approved' => 1))) {
                    flash()->success('User unblocked successfully');
                    return response()->json(['success' => true]);
                } else {
                    flash()->error('Something went wrong');
                    return response()->json(['success' => false]);
                }
        }

    }
    
    /*===============
        Print PDF
    ===============*/
    public function htmltopdfview($id)
    {
        $user = User::where('_id', $id)->first()->toArray();
        $job_application = JobsapplicationModel::with(array('job_shift', 'job'))->where(array('jobseeker_id' => $id))->whereIn("job_status", [4, 5, 7])->where('total_hours_worked', '!=', null)->get()->toArray();
        $completed_jobs = $total_working_hour = $total_earning = 0;
        if (isset($job_application)) {
            $completed_jobs = count($job_application);
            foreach ($job_application as $applications) {
                $salary = $applications['job']['salary_per_hour']; ///60;
                $hours[] = $applications['total_hours_worked'];
                $earning[] = $applications['total_hours_worked'] * $salary;
            }
            if (isset($hours)) {
                $total_working_hour = array_sum($hours);
            }
            if (isset($earning)) {
                $total_earning = array_sum($earning);
            }
        }
        view()->share('users', $user);
        $pdf = PDF::loadView('jobseekers/htmltopdfview', array("users" => $user, 'total_earning' => $total_earning, 'total_working_hour' => $total_working_hour, 'completed_jobs' => $completed_jobs));
        return $pdf->download('Employee Details.pdf');
    }
    
    /*==================================================
        Feltering Waiting Jobkseeker in ajax request
    ===================================================*/
    public function jobSeekerWaitListing(Request $request)
    {
        $basearray = User::where(array('is_deleted' => false, 'role' => 2, 'approved' => 0))->orderBy('created_at', 'desc');
        $totalusercount = User::where(array('is_deleted' => false, 'role' => 2, 'approved' => 0))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('first_name', 'LIKE', $request->name . '%');
        }

        if (isset($request->email) && !empty($request->email)) {
            $basearray->where('email', 'LIKE', $request->email . '%');
        }

        if (isset($request->phone_num) && !empty($request->phone_num)) {
            $basearray->where('phone', 'LIKE', $request->phone_num . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        /** If Single set **/
        if (isset($request->requested_at_from) || isset($request->requested_at_to)) {
            $start = Carbon::parse($request->requested_at_from);
            $start2 = Carbon::parse($request->requested_at_to);
            $start_day_start = $start->startOfDay();
            $start_day_end = $start2->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start)->where('created_at', '<=', $start_day_end);
        }
        /** If Both set **/
        if (isset($request->requested_at_from) && isset($request->requested_at_to)) {
            $start_both = Carbon::parse($request->requested_at_from);
            $end_both = Carbon::parse($request->requested_at_to);
            $start_day_start_both = $start_both->startOfDay();
            $start_day_end_both = $end_both->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start_both)->where('created_at', '<=', $start_day_end_both);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('first_name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('first_name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = User::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );
        foreach ($resultset as $user) {
            $userId = $user['_id'];
/*
            $view_link = '<a id="time-slot" data-toggle="modal" href="assign-time-slot/' . $userId . '" class="btn btn-circle btn-icon-only btn-default"><span class="icon-eye" style="color:blue;"></span></a> &nbsp;';
            
            //added on : 11 sep 2017, shivani
            $delete_link = '<a href="javascript:void(0)" data-url="delete-employee/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked delete-user"><span style="color:red" title="delete" class="icon-trash delete-user" aria-hidden="true"></span></a>&nbsp; ';
            
            //12 sep 2017, shivani
            $verify_link = '<a href="javascript:void(0)" data-url="verify-employee-email/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked verify-user"><span style="color:green" title="verify email" class="fa fa-thumbs-o-up verify-user" aria-hidden="true"></span></a> ';


*/      

        $sendSlotsLink = 'assign-time-slot/' . $userId . '?tabs=3';

        if($user['agreement_send'] == true ){
       
                $sendSlotsTitle ='Approve';
                $icon ='fa-check';

        }else{

                $icon ='fa-clock-o';

             if($user['user_slot_accepted']){

                $sendSlotsTitle ='Resend Time Slots';

             }else{
    
                $sendSlotsTitle ='Send Time Slots';

             }
        }

        $view_link ='<div class="dropdown" style="float:right;">
                        <button class="dropbtn">...</button>
                            <div class="dropdown-content">
                                 <a id="time-slot" data-toggle="modal" href="assign-time-slot/' . $userId . '"><i class="fa fa-eye fa-lg aria-hidden="true"></i> View</a>
                                <a href="javascript:void(0)" data-url="verify-employee-email/' . $userId . '" class="locked verify-user"><i class="fa fa-thumbs-up fa-lg" aria-hidden="true"></i> Verify Email</a>';


       


        if($user['user_slot_accepted'] == true &&  $user['agreement_send'] == false && $user['profile_complete'] != 11){  
       // if($user['user_slot_accepted'] == true &&  $user['agreement_send'] == false ){ 

            $view_link .= '<a href="opentok/' . $userId . '"><i class="fa fa-phone fa-lg" aria-hidden="true"></i> Initiate Call</a>';
        } 


         if($user['agreement_send'] == false && $user['user_slot_accepted'] == true){              

            $view_link .= '<a href="javascript:void(0)" data-url="sent-agreement/' . $userId . '" class="locked agreement-user"><i class="fa fa-file" aria-hidden="true"></i> Send Agreement</a>';

        }


        //if($user['user_slot_accepted'] == false){
        if($user['agreement_send'] == false && $user['profile_complete'] != 11){      

            $view_link .= ' <a href='.$sendSlotsLink.'><i class="fa ' . $icon . ' fa-lg" aria-hidden="true"></i> '.$sendSlotsTitle.'</a>';
            
        }

        if($user['agreement_send'] == true ){
       
                $sendSlotsTitle ='Approve';
                $icon ='fa-check';
                 $view_link .= ' <a href='.$sendSlotsLink.'><i class="fa ' . $icon . ' fa-lg" aria-hidden="true"></i> '.$sendSlotsTitle.'</a>';

        }

       
        //$view_link .= '<a href="vedio/' . $userId . '"><i class="fa fa-phone fa-lg" aria-hidden="true"></i> Sinch Call</a>';

                 
        $view_link .= '<a href="javascript:void(0)" data-url="soft-delete-employee/' . $userId . '" class="locked delete-user"><i class="fa fa-trash fa-lg" aria-hidden="true"></i> Archive </a>';

        $view_link .= '<a href="javascript:void(0)" data-url="delete-employee/' . $userId . '" class="locked delete-user"><i class="fa fa-trash fa-lg" aria-hidden="true"></i> Permanent Delete</a>
                </div>
            </div>';



            $delete_link = '';
            $verify_link = '';
            if (isset($user['first_name']) && !empty($user['first_name'])) {
                $name = '<a id="time-slot" data-toggle="modal" href="assign-time-slot/' . $userId . '">' . $user['first_name'] . ' ' . $user['last_name'] . '</a>';
            } else {
                $name = "N/A";
            }

            if (isset($user['email'])) {
                $email = $user['email'];
            }


            if (isset($user['phone']) && !empty($user['phone'])) {
                $phone = $user['country_code'] . ' ' . $user['phone'];
            }

            if (isset($user['address']) && !empty($user['address'])) {
                $address = $user['address'];
            } else {
                $address = "N/A";
            }

            if (isset($user['sin_number']) && !empty($user['sin_number'])) {
                $sin_number = wordwrap($user['sin_number'], 3, ' ', true);
            } else {
                $sin_number = "N/A";
            }

            //$profilearray = array('1' => 'OTP Not Verified', '2' => 'OTP Verified', '3' => 'Agreement Accepted', '4' => 'Onboarding Step-1', '5' => 'Onboarding Step 2', '6' => 'Onboarding Step-3', '7' => 'Waiting For Approval', '8' => 'Tutorial watch', );

            $profilearray = array('1' => 'OTP Not Verified', '2' => 'OTP Verified', '3' => 'Terms & Conditions Accepted', '4' => 'Onboarding Step-1', '5' => 'Onboarding Step 2', '6' => 'Onboarding Step-3', '7' => 'Waiting for Agreement to Send', '8' => 'Agreement Sent', '9' => 'Waiting For Approval', '10' => 'Tutorial watch', '11' => 'Satisfied & Waiting for Agreement to Send', );

            if (isset($profilearray[$user['profile_complete']])) {
                $profile_complete = $profilearray[$user['profile_complete']];
            }

            if (isset($user['created_at'])) {
                //$created_at = Carbon::parse( $user['created_at'] )->format('d').' '.Carbon::parse( $user['created_at'] )->format('F').', '.Carbon::parse( $user['created_at'] )->format('Y');
                $created_at = Carbon::parse($user['created_at'])->format('M') . '-' . Carbon::parse($user['created_at'])->format('d') . '-' . Carbon::parse($user['created_at'])->format('Y');
            }

            $approved = array('Pending', 'Approved', 'Disapproved');

            $GLOBALS['data'][] = array($i, $name, $email, $phone, $address, $sin_number, $profile_complete, $created_at, $view_link . ' ' . $delete_link . ' ' . $verify_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }
    
    
    /*
     * Added on : 6 nov 2017
     * Added by : shivani, Debut infotech
     * DESC : to return cv details for an employee
     * */
    public function employee_cv_details(Request $request)
    {
        $userDetail = User::where(array('_id' => $request->user_id))->select('cv_url', 'new_cv', 'cv_name', 'new_cv_name')->first()->toArray();
        return view('jobseekers/employee_cv_content', array("userDetail" => $userDetail));
    }
    
    /*
     * Added on : 6 nov 2017
     * Added by : shivani, Debut infotech
     * DESC : to approve new cv for an employee
     * */
    public function approve_new_cv(Request $request)
    {
        $userDetail = User::where(array('_id' => $request->user_id))->select('cv_url', 'new_cv', 'cv_name', 'new_cv_name')->first();

        $userDetail->cv_url = $userDetail->new_cv;
        $userDetail->new_cv = null;
        $userDetail->cv_name = $userDetail->new_cv_name;
        $userDetail->new_cv_name = null;
        if ($userDetail->save()) {
            return response()->json(['success' => true, 'message' => 'cv updated successfully']);
        } else {
            return response()->json(['success' => false, 'message' => 'sorry, something went wrong']);
        }

    }
    
    /*
     * Added on : 6 nov 2017
     * Added by : shivani, Debut infotech
     * DESC : to decline new cv for an employee
     * */
    public function decline_new_cv(Request $request)
    {
        $userDetail = User::where(array('_id' => $request->user_id))->select('cv_url', 'new_cv', 'cv_name', 'new_cv_name')->first();
        $userDetail->new_cv = null;
        $userDetail->new_cv_name = null;
        if ($userDetail->save()) {
            return response()->json(['success' => true, 'message' => 'cv updated successfully']);
        } else {
            return response()->json(['success' => false, 'message' => 'sorry, something went wrong']);
        }
    }
    
    

    /*==================================================
        Feltering Waiting Jobkseeker in ajax request
     * updated on : 28 july 2017, shivani to add uniq employee id to this list
    ====================================================
     */
    public function jobSeekerApprovedListing(Request $request)
    {

        $basearray = User::where(array('is_deleted' => false, 'role' => 2, ))->whereIn('approved', [1, 3])->orderBy('updated_at', 'desc');

       
        $totalusercount = User::where(array('is_deleted' => false, 'role' => 2, ))->whereIn('approved', [1, 3])->count();

        if (isset($request->first_name) && !empty($request->first_name)) {
            $basearray->where('first_name', 'LIKE', $request->first_name . '%');
        }

        if (isset($request->email) && !empty($request->email)) {
            $basearray->where('email', 'LIKE', $request->email . '%');
        }

        if (isset($request->phone_num) && !empty($request->phone_num)) {
            $basearray->where('phone', 'LIKE', $request->phone_num . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');

        if ($order[0]['column'] == 5 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('rating', 'asc');
            $basearray->orderBy('updated_at', 'asc');
        } elseif ($order[0]['column'] == 5 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('rating', 'desc');
            $basearray->orderBy('updated_at', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = User::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
       // $GLOBALS['total'] = count( $resultset );
        foreach ($resultset as $user) {
            $userId = $user['_id'];
            //$view_link  = '<a catId="'.$catId.'" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="'.$user['status'].'" href="edit-category/'.$catId.'" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deletecategory" data-id='.$catId.' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete Category" class="icon-trash" aria-hidden="true"></span></a>';
            $view_link = '<a data-status="' . $user['status'] . '" href="edit-approved-employee/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>&nbsp;';
            //added on : 11 sep 2017, shivani
            $delete_link = '<a href="javascript:void(0)" data-url="delete-employee/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked delete-user"><span style="color:red" title="delete" class="icon-trash delete-user" aria-hidden="true"></span></a> ';
            //12 sep 2017, shivani
            $verify_link = '<a href="javascript:void(0)" data-url="verify-employee-email/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked verify-user"><span style="color:green" title="verify email" class="fa fa-thumbs-o-up verify-user" aria-hidden="true"></span></a> ';
            
            
            //28 july 2017, shivani - to add uniq employee id to this list
            $mw_id = 'N/A';
            if (isset($user['mwuserid'])) {
                $mw_id = $user['mwuserid'];
            }

            if (isset($user['first_name']) && !empty($user['first_name'])) {
                $name = '<a id="time-slot" data-toggle="modal" href="view-detail-approved-employee/' . $userId . '">' . $user['first_name'] . ' ' . $user['last_name'] . '</a>';
            } else {
                $name = "N/A";
            }

            if (isset($user['image']) && !empty($user['image'])) {
                $image = '<img src="' . $user['image'] . '" width="50" height="50" class="img-circle" alt="User Image"/>';
            } else {
                $image = '<img src="' . asset("public/admin/img/user_male2-512.png") . '" width="50" height="50" class="img-circle" alt="User Image"/>';
            }

            if (isset($user['email']) && !empty($user['email'])) {
                $email = $user['email'];
            }

            if (isset($user['phone']) && !empty($user['phone'])) {
                $phone = $user['country_code'] . ' ' . $user['phone'];
            }

            if (isset($user['rating'])) {
                $rating = CommonRepositary::user_rating($user['rating']);
            }

            if (isset($user['sin_number']) && !empty($user['sin_number'])) {
                $sin_number = $user['sin_number'];
            } else {
                $sin_number = "N/A";
            }

            $cv = 'N/A';
            if (isset($user['cv_url']) && !empty($user['cv_url'])) {
                $cv = '<div class="view_cv"><a class="btn btn-circle btn-icon-only btn-default check_user_cv" data-table="users" data-id="' . $user['_id'] . '"><span style="color:#4985CB" title="check CV" class="fa fa-eye" aria-hidden="true"></span><a></div>';
            }

            if (empty($user['cv_url']) && !empty($user['new_cv'])) {
                $cv = '<div class="view_cv"><a class="btn btn-circle btn-icon-only btn-default check_user_cv" data-table="users" data-id="' . $user['_id'] . '"><span style="color:#4985CB" title="check CV" class="fa fa-eye" aria-hidden="true"></span><a></div>';
            }

            $profilearray = array('1' => 'Otp Not Vrified', '2' => 'Otp Verified', '3' => 'Agreement Accepted', '4' => 'Onboarding Step-1', '5' => 'Step 2', '6' => 'Step 3', '7' => 'Time Slots Submitted', '8' => 'Time slot Requested');


            if ($user['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" id="change-common-status" data-table="users" data-id="' . $user['_id'] . '" data-status="' . $user['status'] . '" data-approved="' . $user['approved'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" id="change-common-status" data-table="users" data-id="' . $user['_id'] . '" data-status="' . $user['status'] . '" data-approved="' . $user['approved'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }

        
            $subCategory = 'N/A';
            if ($user['subcategories']) {
              $subCategory = array_column($user['subcategories'] , 'name');
            }

            //27 july 2017, shivani - to add uniq employee id to this list
            $GLOBALS['data'][] = array($i, $mw_id, $image, $name, $email, $subCategory, $phone, $cv, $rating, $status, $view_link . ' ' . $delete_link . ' ' . $verify_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }
    
    
    
     /*=======================================
        Added on : 11 sep 2017, shivani
     * to verify employee email (temporary basis )
    =========================================*/
    public function getVerifyEmployeeEmail($id)
    {
        $userDetail = User::where(array('_id' => $id))->first();
        if (!empty($userDetail)) {
            User::where(array('_id' => $id))->update(['user_email_confirmed' => true]);
            flash()->success('Employee email has been verified');
        }
        return redirect(url()->previous());
    }


    /*=======================================
        Added on : 11 sep 2017, shivani
     * to send employee agreement (temporary basis )
    =========================================*/
    public function getSendEmployeeAgreement($id)
    {
        $userDetail = User::where(array('_id' => $id))->first();
        if (!empty($userDetail)) {
            User::where(array('_id' => $id))->update(['agreement_send' => true, 'profile_complete'=>8]);
            flash()->success('Agreement has been sent successfully.');
        }
        return redirect(url()->previous());
    }
    
   
    /*==================================================
        Feltering Declined Jobkseeker in ajax request
    ===================================================*/
    public function filterDeclinedEmploye(Request $request)
    {
        $basearray = User::where(array('is_deleted' => false, 'role' => 2, 'approved' => 2))->orderBy('created_at', 'desc');
        $totalusercount = User::where(array('is_deleted' => false, 'role' => 2, 'approved' => 2))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('first_name', 'LIKE', $request->name . '%')->where('status', true);
        }

        if (isset($request->email) && !empty($request->email)) {
            $basearray->where('email', 'LIKE', $request->email . '%');
        }

        if (isset($request->phone_num) && !empty($request->phone_num)) {
            $basearray->where('phone', 'LIKE', $request->phone_num . '%');
        }


        /** If Single set **/
        if (isset($request->requested_at_from) || isset($request->requested_at_to)) {
            $start = Carbon::parse($request->requested_at_from);
            $start2 = Carbon::parse($request->requested_at_to);
            $start_day_start = $start->startOfDay();
            $start_day_end = $start2->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start)->where('created_at', '<=', $start_day_end);
        }
        /** If Both set **/
        if (isset($request->requested_at_from) && isset($request->requested_at_to)) {
            $start_both = Carbon::parse($request->requested_at_from);
            $end_both = Carbon::parse($request->requested_at_to);
            $start_day_start_both = $start_both->startOfDay();
            $start_day_end_both = $end_both->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start_both)->where('created_at', '<=', $start_day_end_both);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('first_name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('first_name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = User::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        $GLOBALS['total'] = count($resultset);
        foreach ($resultset as $user) {
            $userId = $user['_id'];
            $view_link = '<a userId="' . $userId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a>';
            if (isset($user['first_name']) && !empty($user['first_name'])) {
                $name = '<a id="view" data-toggle="modal" href="assign-time-slot/' . $userId . '">' . $user['first_name'] . ' ' . $user['last_name'] . '</a>';
            } else {
                $name = "N/A";
            }

            if (isset($user['email'])) {
                $email = $user['email'];
            }


            if (isset($user['phone']) && !empty($user['phone'])) {
                $phone = $user['country_code'] . ' ' . $user['phone'];
            }

            if (isset($user['address']) && !empty($user['address'])) {
                $address = $user['address'];
            } else {
                $address = "N/A";
            }

            if (isset($user['sin_number']) && !empty($user['sin_number'])) {
                $sin_number = wordwrap($user['sin_number'], 3, ' ', true);
            } else {
                $sin_number = "N/A";
            }

           // $profilearray = array('1' => 'OTP Not Verified', '2' => 'OTP Verified', '3' => 'Agreement Accepted', '4' => 'Onboarding Step-1', '5' => 'Onboarding Step 2', '6' => 'Onboarding Step-3', '7' => 'Waiting For Approval', '8' => 'Tutorial watch', );

            $profilearray = array('1' => 'OTP Not Verified', '2' => 'OTP Verified', '3' => 'Terms & Conditions Accepted', '4' => 'Onboarding Step-1', '5' => 'Onboarding Step 2', '6' => 'Onboarding Step-3', '7' => 'Waiting for Agreement to Send', '8' => 'Agreement Sent', '9' => 'Waiting For Approval', '10' => 'Tutorial watch','11' => 'Satisfied & Waiting for Agreement to Send', );

            if (isset($profilearray[$user['profile_complete']])) {
                //$profile_complete = $profilearray[$user['profile_complete']];
            }

            if (isset($user['created_at'])) {
                //$created_at = Carbon::parse( $user['created_at'] )->format('d').' '.Carbon::parse( $user['created_at'] )->format('F').', '.Carbon::parse( $user['created_at'] )->format('Y');
                $created_at = Carbon::parse($user['created_at'])->format('M') . '-' . Carbon::parse($user['created_at'])->format('d') . '-' . Carbon::parse($user['created_at'])->format('Y');
            }
            //~ if($user['status']) {
                //~ $status='<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="user" data-id="'.$user['_id'].'" data-status="'.$user['status'].'" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            //~ } else {
                //~ $status='<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="user" data-id="'.$user['_id'].'" data-status="'.$user['status'].'" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            //~ }

            $approved = array('Pending', 'Approved', 'Disapproved');

            $GLOBALS['data'][] = array($i, $name, $email, $phone, $address, $sin_number, $created_at, $view_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $totalusercount;
        return json_encode($result);
    }

    /*====================
        View Declined User 
    =======================*/

    public function viewDetailApprovedJobseeker(Request $request, $id, CommonRepositary $common)
    {
        $edit_approved_jobseeker = User::where(array('_id' => $id))->first();

        if(isset($edit_approved_jobseeker['agreement_status']) && $edit_approved_jobseeker['agreement_status'] == 2 ){

            $edit_approved_jobseeker['downloadEmplyoeeContract'] = url('/agreement/downloadEmplyoeeContract/').'/'.$id;
        }

        if(isset($edit_approved_jobseeker['emergency_contact']) && $edit_approved_jobseeker['emergency_contact'] == 2 ){

            $edit_approved_jobseeker['downloadEmergencyContact'] = url('/agreement/downloadEmergencyContact/').'/'.$id;
        }

        if(isset($edit_approved_jobseeker['direct_deposit']) && $edit_approved_jobseeker['direct_deposit'] == 2){

              $edit_approved_jobseeker['downloadDirectDeposit'] = url('/agreement/downloadDirectDeposit/').'/'.$id;
        }

        if(isset($edit_approved_jobseeker['opentokSessionId'])){

            #Get Video URL
            #=====================
            $edit_approved_jobseeker['opentokVideoDownloadUrl'] = $common->downloadOpentokVideo($edit_approved_jobseeker['opentokSessionId']);
        }

        $job_application = JobsapplicationModel::with(array('job_shift', 'job'))->where(array('jobseeker_id' => $id, "job_status" => 4))->get()->toArray();
        if (isset($job_application)) {
            $completed_jobs = count($job_application);
        } else {
            $completed_jobs = 0;
        }
        foreach ($job_application as $applications) {
            $salary = $applications['job']['salary_per_hour'] / 60;
            foreach ($applications['job_shift'] as $shift) {
                $earning[] = $shift['shift_time'] * $salary;
                $hours[] = $shift['shift_time'] / 60;
            }
        }
        if (isset($hours)) {
            $total_working_hour = array_sum($hours);
        } else {
            $total_working_hour = 0;
        }
        if (isset($earning)) {
            $total_earning = array_sum($earning);
        } else {
            $total_earning = 0;
        }

        return view('jobseekers/viewApprovedJobSeekerDetail', ['edit_approved_jobseeker' => $edit_approved_jobseeker, 'completed_jobs' => $completed_jobs, 'total_working_hour' => $total_working_hour, 'total_earning' => $total_earning]);
    }

    /*====================
        View Blocked User 
    =======================*/
    public function viewBlockEmployee(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = User::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }

    /*====================
        Undeclined User 
    =======================*/
    public function undelinedEmployee(Request $request)
    {
        $decline_user = User::where('_id', $request->id)->first();
        switch ($decline_user['approved']) {
            case 1:
                flash()->error('You have already approved this employee');
                return response()->json(['success' => false]);
                break;
            default:
                if ($decline_user->update(array('status' => false, 'approved' => 0))) {

                    $token = Token::where('user_id', $request->id)->first();
                    $token->update(array('decline' => false));

                    $template = EmailTemplate::find('59a4f957429975299f441201');
                    $find = array('@name@');
                    $values = array($decline_user['first_name']);
                    $body = str_replace($find, $values, $template->content);
                    $this->dispatch(new SendOtpEmail($body, $decline_user['email'], $template));
                    flash()->success('Employee is in waiting list.Kindly approve the Employee');
                    return response()->json(['success' => true]);
                } else {
                    flash()->error('Something went wrong');
                    return response()->json(['success' => false]);
                }
        }
    }

    /*=========================
        Change Status Of User
    ===========================*/
    public function changeStatus(Request $request, CommonRepositary $common)
    {
        $id = $request->id;
        $unblock_user = User::where('_id', $id)->first();
        if ($request['status'] == '1' && $request['approved'] == 1) {
            $unblock_user->update(['status' => false]);
        } else {
            $unblock_user->update(['status' => true, 'approved' => 1]);
            //3 aug 2017, shivani - to call new function to send notification
            $common->send_notification($id, $type = 'reactivate', $job_id = null);
        }
        return response()->json(['success' => true, 'action' => $request['action']]);
    }

    public function testing(CommonRepositary $common)
    {
        $id = '5da572d8c7069f1cbb6f2d72';

       // print_R('ffff');die;
        
        //$id = '5cb99256c7069f7fd1464751';
       
       $common->send_notification($id, $type = 4, $job_id = null);
    }
}
