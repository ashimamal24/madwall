<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\NotificationModel;
use App\Model\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;

class NotificationController extends Controller
{
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    public function getNotification()
    {
        $thrtyDay_earlier = Carbon::now()->subDays(30);
        $notification = NotificationModel::where(array('status' => true, 'to' => $this->auth->user()->_id))->where('created_at', '>=', $thrtyDay_earlier)->orderBy('created_at', 'DESC')->limit(50)->get();
        $not_count = count($notification);
        $result = ['not_count' => $not_count, 'notification' => $notification];
        return $result;
    }

    public function checkNotification(request $request)
    {
        $notification = NotificationModel::where(array('_id' => $request['id']))->first();
        $notification->update(array('status' => false));
        return response()->json(['msg' => 'hello']);
    }
}
