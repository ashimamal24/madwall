<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CommissionRequest;
use App\Model\CategoryModel;
use App\Model\CommissionModel;
use DB;

// Comiision conteoller

class CommissionController extends Controller
{
    /*===========================================
        listing of Commissions.
    =============================================
     */
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }
    public function index()
    {
        $commission = CommissionModel::get();
        return view('commission/listingcommissions', ['commission' => $commission]);
    }

    /*=========================
        Load Add Commission View
    ===========================
     */
    public function create()
    {
        $categories = CategoryModel::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        return view('commission/createcommission', ['categories' => $categories]);
    }
    
    /*================================================================
        Store item in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     =================================================================
     */
    public function store(CommissionRequest $request)
    {
        $data = $request->except('_token');
        if ($data['status'] == '1') {
            $data['status'] = true;
        }
        if ($data['status'] == '0') {
            $data['status'] = false;
        }
        $data['commission_amount'] = (double)$request['commission_amount'];
        $data['is_deleted'] = false;
        $commissionobj = new CommissionModel($data);
        if ($commissionobj->save()) {
            flash()->success('Commission added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*======================================================
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================
     */
    public function edit(CommissionRequest $request, $id)
    {
        $commission = CommissionModel::where('_id', $id)->first();
        $data = $request->all();
        if ($data['status'] == '1') {
            $data['status'] = true;
        }
        if ($data['status'] == '0') {
            $data['status'] = false;
        }
        if ($commission->update($data)) {
            flash()->success('Commission updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }

    /*================================================
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================
     */
    public function update(Request $request, $id)
    {
        $categories = CategoryModel::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        $edit_commission = CommissionModel::where(array('_id' => $id))->first();
        return view('commission/editcommission', ['categories' => $categories, 'edit_commission' => $edit_commission]);
    }

    /*================================================
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function destroy($id)
    {
        $category = CommissionModel::where('_id', $id)->first();
        $category->update(['is_deleted' => true]);
        if ($category) {
            flash()->success('Commission removed successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*==================================================
         Feltering Data in ajax request
    ====================================================
     */
    public function filterCommissions(Request $request)
    {
        $basearray = CommissionModel::where(array('is_deleted' => false));
        $totalusercount = CommissionModel::where(array('is_deleted' => false))->count();

        if (isset($request->category_name) && !empty($request->category_name)) {
            $basearray->where('category_name', 'LIKE', '%' . $request->category_name . '%');
        }


        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }

        $counttotal = CategoryModel::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $commission) {
            $commissionid = $commission['_id'];
            $view_link = '<a commissionid="' . $commissionid . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $commission['status'] . '" href="edit-commission/' . $commissionid . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deletecommission" data-id=' . $commissionid . ' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>';

            if (isset($commission['category_name']) && !empty($commission['category_name'])) {
                $category_name = $commission['category_name'];
            }

            if (isset($commission['commission_amount']) && !empty($commission['commission_amount'])) {
                $commission_amount = $commission['commission_amount'];
            }

            if ($commission['status']) {
                $status = '<div class="statuscenter"><a  id="change-common-status" data-table="commission" data-id="' . $commission['_id'] . '" data-status="' . $commission['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a  id="change-common-status" data-table="commission" data-id="' . $commission['_id'] . '" data-status="' . $commission['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $category_name, $commission_amount, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }
   
   /*==============================================
        View Particular Category 
    ===============================================
     */
    public function viewCommission(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = CommissionModel::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
}
