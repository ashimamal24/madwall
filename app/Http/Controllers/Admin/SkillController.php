<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SkillRequest;
use App\Model\CategoryModel;
use App\Model\SkillModel;
use DB;

class SkillController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*===========================================
            Display a listing of skills.
    =============================================*/
    public function index()
    {
        $skills = SkillModel::get();
        return view('skills/listingskills', ['skills' => $skills, 'active' => 'skills']);
    }

    /*=========================
        Creating Skill
    ===========================*/
    public function create()
    {
        return view('skills/createskill');
    }

    /*================================================================
        Store Skill in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     =================================================================*/
    public function store(SkillRequest $request)
    {
        $data = $request->except('_token');
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['is_deleted'] = false;
        $data['used'] = false;
        $skillobj = new SkillModel($data);
        if ($skillobj->save()) {
            flash()->success('Skill added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }


    /*======================================================
     * Show the form for editing the specified skill.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function edit(SkillRequest $request, $id)
    {
        $skill = SkillModel::where(array('_id' => $id))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($skill->update($data)) {
            flash()->success('Skill updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function update(Request $request, $id)
    {
        $edit_skill = SkillModel::where(array('_id' => $id))->first();
        return view('skills/editskill', ['edit_skill' => $edit_skill]);
    }

    /*================================================
     * Remove the specified skill from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================*/
    public function destroy($id)
    {
        $skill = SkillModel::where('_id', $id)->first();

        if ($skill->update(['is_deleted' => true])) {
            flash()->success('Skill deleted successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterSkills(Request $request)
    {
        $basearray = SkillModel::where(array('is_deleted' => false));
        $totalusercount = SkillModel::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = SkillModel::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;

        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $realtor) {
            $skillId = $realtor['_id'];
            $view_link = '<a skillid="' . $skillId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $realtor['status'] . '" href="edit-skill/' . $skillId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deleteskill" data-id=' . $skillId . ' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>';
            if (isset($realtor['name']) && !empty($realtor['name'])) {
                $name = $realtor['name'];
            }

            if (isset($realtor->description) && !empty($realtor->description)) {
                $length = strlen($realtor->description);
                if ($length > 100) {
                    $description = substr($realtor->description, 0, 15) . '...';
                } else {
                    $description = substr($realtor->description, 0, 15);
                }
            }

            if ($realtor['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="master_skill" data-id="' . $realtor['_id'] . '" data-status="' . $realtor['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="master_skill" data-id="' . $realtor['_id'] . '" data-status="' . $realtor['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $name, $description, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
   
   /*=====================================
        View Particular Skill Detail 
    ======================================*/
    public function viewSkill(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = SkillModel::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }

}
