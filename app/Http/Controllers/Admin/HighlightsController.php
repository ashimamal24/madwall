<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\HighlightRequest;
use App\Http\Controllers\Controller;
use App\Model\Highlight;
use App\Model\CmsPages;

class HighlightsController extends Controller
{


     /*================================
        Listing of Highlight
    ==================================*/
    public function index()
    {
        $highlights = Highlight::get();
        return view('highlights/listingHighlight', ['highlights' => $highlights]);
    }

     /*=================================
        Load Creating Highlight View
    ===================================*/
    public function create()
    {
        return view('highlights/createHighlight');
    }

    /*================================================================
        Store in database table
    =================================================================*/

    public function store(HighlightRequest $request)
    {
      
        $data['title'] = ucfirst($request->input('title'));
        $data['descriptions'] = ucfirst($request->input('descriptions'));
       
        $data['is_deleted'] = false;
        $indobj = new Highlight($data);
        if ($indobj->save()) {
            flash()->success(trans('Admin/message.add_highlight'));
            return response()->json(['success' => true]);
        } else {
            flash()->error((trans('Admin/message.error')));
            return response()->json(['success' => false]);
        }
    }


/*================================================
     * Update the specified skill in storage.
 =================================================*/
    public function edit($id)
    {
        $edit_highlight = Highlight::where(array('_id' => $id))->first();
        return view('highlights/editHighlight', ['edit_highlight' => $edit_highlight]);
    }

    
    /*======================================================
     * Show the form for editing the specified Category.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function update(HighlightRequest $request, $id)
    {

        $indObj = Highlight::where(array('_id' => $id))->first();

        $data['title'] = ucfirst($request->input('title'));
         $data['descriptions'] = ucfirst($request->input('descriptions'));
       
        if ($indObj->update($data)) {
            flash()->success(trans('Admin/message.update_highlight'));
            return response()->json(['success' => true]);
        } else {
            flash()->error(trans('Admin/message.error'));
            return response()->json(['success' => false]);
        }

    }

     /*================================================
     * Remove the specified highlight from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================*/
    public function destroy($id)
    {

        $highlight = Highlight::where('_id', $id)->first();

        if ($highlight->delete()) {
         flash()->success('Highlight deleted successfully');  
         return response()->json(['success' => true]);
        }else{
         flash()->success('Something went wrong');
         return response()->json(['success' => false]);
        }

    }

     /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterHighlight(Request $request)
    {
    
    	$basearray = Highlight::get();
        $totalusercount = Highlight::get()->count();
       
        $counttotal = Highlight::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray;
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        $GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $resultset) {

                $id = $resultset['_id'];

                $title =$resultset['title'];

                $descriptions =$resultset['descriptions'];

                $view_link = '<a indusd_id="' . $id . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail"><span class="icon-eye" style="color:blue;"></span></a>
                <a  href="edit/' . $id . '" class="btn btn-circle btn-icon-only btn-default locked" title="Edit"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>
                 <!-- <a id="delete" data-id="' . $id . '" class="btn btn-circle btn-icon-only btn-default" title="Delete"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>-->';
          
            $GLOBALS['data'][] = array($i, $title, $descriptions, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }


	/*=====================================
        View Particular  
    ======================================*/
    public function view(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = Highlight::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }




}
