<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BlogCategoryRequest;
use App\Model\CategoryModel;
use App\Model\SkillModel;
use App\Model\Industry;
use App\Model\BlogCategory;
use App\Model\Blog;
use Carbon\Carbon;
use DB;

class BlogCategoryController extends Controller
{

  
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*============================
        Display a listing of blogCategories.
    =============================*/
    public function index()
    {
        $blogCategories = BlogCategory::paginate(10);
         return view('blog-category/listingblogcategories', ['blogCategories' => $blogCategories, 'active' => 'blogcategories']);
  
    }

    /*================
        Creating blog category
    ==================*/
    public function create()
    {
        return view('blog-category/createblogcategory');
    }


    /*================================================================
        Store Skill in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     =================================================================*/
    public function store(BlogCategoryRequest $request)
    {
        $data['name'] = $request->input('name');
        $data['is_deleted'] = false;
        $data['status'] = $request->input('status') > 0 ? true : false;
        $wageobj = new BlogCategory($data);
        if ($wageobj->save()) {
          
            flash()->success(trans('Admin/message.add_blog_category'));
            return response()->json(['success' => true]);
        } else {
            flash()->error(trans('Admin/message.error'));
            return response()->json(['success' => false]);
        }

    }

    
    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function edit(Request $request, $id)
    {
        $editBlogCategory = BlogCategory::where(array('_id' => $id))->first();
        return view('blog-category/editbolgcategory', ['editBlogCategory' => $editBlogCategory]);
    }

    



    /*======================================================
     * Show the form for editing.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function update(BlogCategoryRequest $request)
    {
        
        $wageobj = BlogCategory::where(array('_id' => $request->input('idedit')))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($wageobj->update($data)) {
            flash()->success(trans('Admin/message.update_blog_category'));
            return response()->json(['success' => true]);
        } else {
            flash()->error(trans('Admin/message.error'));
            return response()->json(['success' => false]);
        }

    }

    
    /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterBlogCategories(Request $request)
    {
        $basearray = BlogCategory::where(array('is_deleted' => false));


        $totalusercount = BlogCategory::where(array('is_deleted' => false))->count();
         
        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }
      

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }


        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = BlogCategory::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $val) {
            $bolgId = $val['_id'];
            $view_link ='';
            $view_link .= '<a href="edit/' . $bolgId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>

                <a id="deleteblogcategory" data-id="' . $bolgId . '" class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete Blog Category" class="icon-trash" aria-hidden="true"></span></a>';

            if ($val['status']) {
                $view_link .= '<a class="btn btn-circle btn-icon-only btn-default" title="Unblocked" id="change-common-status" data-table="manage_blog_category" data-id="' . $val['_id'] . '" data-status="' . $val['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a>';
            } else {
                $view_link .= '<a class="btn btn-circle btn-icon-only btn-default" title="Blocked" id="change-common-status" data-table="manage_blog_category" data-id="' . $val['_id'] . '" data-status="' . $val['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a>';
            }



            if (isset($val['name'])) {
                $name = $val['name'];
            }

            if (isset($val['created_at'])) {
                $date = Carbon::parse($val['created_at'])->format('M') . '-' . Carbon::parse($val['created_at'])->format('d') . '-' . Carbon::parse($val['created_at'])->format('Y');

            }


            if ($val['status']) {
                $status = 'Active';
            } else {
                $status = 'Inactive';
            }

            $GLOBALS['data'][] = array($i, $name, $date, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }


    /*================================================
     * Remove the specified Category from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function destroy($id)
    {
        $category = BlogCategory::where('_id', $id)->first();
        
        $blogExists = Blog::where('blog_cat_id', $id)->where('is_deleted',false)->first();
        
       if(empty($blogExists))
       {

        if ($category->update(array('is_deleted' => true))) {
         flash()->success(trans('Admin/message.delete_blog_category'));  
         return response()->json(['success' => true]);
        }else{
         flash()->error(trans('Admin/message.error'));
         return response()->json(['success' => false]);
        }
       }else{
         flash()->error(trans('Admin/message.not_delete_category'));
         return response()->json(['success' => false]);
       }
        
    }



    /*========================
        blokc/Unblock Category 
    ==========================*/

    public function changeStatus(Request $request)
    {
        $id = $request->id;
        $BlogCategory = BlogCategory::where('_id', $id)->first();
        
        $blogExists = Blog::where('blog_cat_id', $id)->where('is_deleted',false)->first();
        
       if(empty($blogExists))
       {
        if ($BlogCategory->update(array('status' => (bool)$request->input('status') ? false : true))) {

           return response()->json(['success' => true,'message'=>trans('Admin/message.block_blog_category')]);
        }else{
           return response()->json(['success' => false,'message'=>trans('Admin/message.error')]); 
        }
       }else{
         
           return response()->json(['success' => false,'message'=>trans('Admin/message.block_message_category')]); 
       }  
        
       
    }

}
