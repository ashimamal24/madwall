<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\TitleRequest;
use App\Http\Controllers\Controller;
use App\Model\Title;
use App\Model\IndustryContent;
use App\Model\Highlight;


class TitlesController extends Controller
{

	public function __construct()
    {
     
        //$this->middleware('admin');
       
    }
    
      /*================================
        Listing of Title
    ==================================*/
    public function index()
    {
    	//print_R('dasd');die;
        $titles = Title::get();
       //print_R($titles);die;
        return view('titles/listingTitle', ['titles' => $titles]);
    }

     /*=================================
        Load Creating titles View
    ===================================*/
    public function create()
    {
    	//print_R('ds');die;
        return view('titles/createTitle');
    }

    /*================================================================
        Store in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
    =================================================================*/
    public function store(TitleRequest $request)
    {
      
        $data['title'] = ucfirst($request->input('title'));
        
        
        //$data['file_name'] = $request->input( 'file_name' );
        $data['is_deleted'] = false;
        $indobj = new Title($data);
        if ($indobj->save()) {
            flash()->success(trans('Admin/message.add_title'));
            return response()->json(['success' => true]);
        } else {
            flash()->error((trans('Admin/message.error')));
            return response()->json(['success' => false]);
        }
    }


/*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function edit($id)
    {
        $edit_title = Title::where(array('_id' => $id))->first();
        return view('titles/editTitle', ['edit_title' => $edit_title]);
    }

    
    /*======================================================
     * Show the form for editing the specified Category.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function update(TitleRequest $request, $id)
    {

    	//print_R('gfd');die;
        $indObj = Title::where(array('_id' => $id))->first();

        $data['title'] = ucfirst($request->input('title'));
       
        if ($indObj->update($data)) {
            flash()->success(trans('Admin/message.update_title'));
            return response()->json(['success' => true]);
        } else {
            flash()->error(trans('Admin/message.error'));
            return response()->json(['success' => false]);
        }

    }

    /*================================================
     * Remove the specified highlight from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================*/
    public function destroy($id)
    {

        $highlight = Title::where('_id', $id)->first();

        if ($highlight->delete()) {
         flash()->success('Title deleted successfully');  
         return response()->json(['success' => true]);
        }else{
         flash()->success('Something went wrong');
         return response()->json(['success' => false]);
        }

    }


     /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterTitle1(Request $request)
    {
        //$basearray = IndustryContent::where(array('is_deleted' => false));
       // $totalusercount = IndustryContent::where(array('is_deleted' => false))->count();

    	 $basearray = Title::get();
        $totalusercount = Title::get()->count();

       /* if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }*/
        $counttotal = Title::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray;
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        $GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $industry) {
            $industry_id = $industry['_id'];

            $view_link = '<a indusd_id="' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail"><span class="icon-eye" style="color:blue;"></span></a>
               <a  href="edit/' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default locked" title="Edit"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>
              <!-- <a id="delete" data-id="' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default" title="Delete"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>-->';
          

       		//$title = 'N/A';
       		$title =$industry['title'];

            $image = '';
 			$userImage = '';
            $userName = '';
            $description = '';
            $status = '';

          

         
            $GLOBALS['data'][] = array($i, $title, $view_link,$description,$image, $userName ,$userImage);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        //print_R($result);die;
        return json_encode($result);
    }


    public function filterTitle(Request $request)
  // public function filterHighlight(Request $request)
    {
    
        $basearray = Title::get();
        $totalusercount = Title::get()->count();
       
        $counttotal = Title::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray;
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        $GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $resultset) {

                $id = $resultset['_id'];

                $title =$resultset['title'];

                $view_link = '<a indusd_id="' . $id . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail"><span class="icon-eye" style="color:blue;"></span></a>
                <a  href="edit/' . $id . '" class="btn btn-circle btn-icon-only btn-default locked" title="Edit"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>
                 <!-- <a id="delete" data-id="' . $id . '" class="btn btn-circle btn-icon-only btn-default" title="Delete"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>-->';
          
            $GLOBALS['data'][] = array($i, $title, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }


    /*=====================================
        View Particular  
    ======================================*/
    public function view(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = Title::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
     


}
