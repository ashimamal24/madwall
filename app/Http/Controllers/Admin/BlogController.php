<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\BlogRequest;
use App\Model\BlogCategory;
use App\Model\Blog;
use App\Model\TempImage;
use Carbon\Carbon;

class BlogController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }


    /*============================
        Display a listing of blog.
    =============================*/
    public function index()
    {
        $blogs = Blog::paginate(10);
         return view('blogs/admin/listingblog', ['blogs' => $blogs, 'active' => 'blogs']);
  
    }



    /*================
        Creating blog 
    ==================*/
    public function create(){   

        $blogCategories = BlogCategory::where('status',true)
                      ->where('is_deleted',false)->pluck('name', '_id')->toArray();
        return view('blogs/admin/createblog',['blogCategories'=>$blogCategories]);
    }



    /*================================================================
        Store Skill in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     =================================================================*/
    public function store(BlogRequest $request)
    {
        
        $data['blog_cat_id'] = $request->input('blog_cat_id');
        $data['title'] = $request->input('title');
        $data['blog_description'] = $request->input('blog_description');
        $data['author_name'] = $request->input('author_name');
        $data['author_description'] = $request->input('author_description');
        $data['author_name'] = $request->input('author_name');
        $data['author_description'] = $request->input('author_description');
       
        if(!empty($request->input('is_blog_image'))){
       
          $data['blog_image'] =  TempImage::where('type','Blog')->pluck('image_name')->first();
        }

        if($request->input('is_author_image')){
         $data['author_image'] =  TempImage::where('type','Author')->pluck('image_name')->first();
        }

        $data['status'] = true;
        $data['is_deleted'] = false;
       
        $blogobj = new Blog($data);
        if ($blogobj->save()) {
            
            if(!empty($request->input('is_blog_image'))){
             TempImage::where('image_name',$data['blog_image'])->delete();
            }


            if(!empty($request->input('is_author_image'))){
             TempImage::where('image_name',$data['author_image'])->delete();
            }
            
            flash()->success(trans('Admin/message.add_blog'));
            return response()->json(['success' => true]);
        } else {
            flash()->error(trans('Admin/message.error'));
            return response()->json(['success' => false]);
        }

    }

    
    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function edit(Request $request, $id)
    {
        $blog = Blog::where(array('_id' => $id))->first();
        $blogCategories = BlogCategory::where('status',true)
                      ->where('is_deleted',false)->pluck('name', '_id')->toArray();

        return view('blogs/admin/editblog', ['editBlog' => $blog,'blogCategories'=>$blogCategories]);
    }

    



    /*======================================================
     * Show the form for editing .
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function update(BlogRequest $request)
    {
        
        $blogobj = Blog::where(array('_id' => $request->input('idedit')))->first();
        $data = $request->all();
        $data['blog_cat_id'] = $request->input('blog_cat_id');
        $data['title'] = $request->input('title');
        $data['blog_description'] = $request->input('blog_description');
        $data['author_name'] = $request->input('author_name');
        $data['author_description'] = $request->input('author_description');
        $data['is_deleted'] = false;


        if(!empty($request->input('is_blog_image'))){
          $data['blog_image'] = TempImage::where('type','Blog')->pluck('image_name')->first();
         }

         if(!empty($request->input('is_author_image'))){
          $data['author_image'] =TempImage::where('type','Author')->pluck('image_name')->first();
         }

        $data['status'] = true;
        $data['is_deleted'] = false;
       

        if ($blogobj->update($data)) {
           
            if(!empty($request->input('is_blog_image'))){
             TempImage::where('image_name',$data['blog_image'])->delete();
            }

            if(!empty($request->input('is_author_image'))){
             TempImage::where('image_name',$data['author_image'])->delete();
            }

            flash()->success(trans('Admin/message.update_blog'));
            return response()->json(['success' => true]);
        } else {
            flash()->error(trans('Admin/message.error'));
            return response()->json(['success' => false]);
        }

    }

    
    /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterBlogs(Request $request)
    {
        $basearray = Blog::where(array('is_deleted' => false));

       
        if (isset($request->title) && !empty($request->title)) {
            $basearray->where('title', 'LIKE', '%' . $request->title . '%');
        }

        if (isset($request->author_name) && !empty($request->author_name)) {
            $basearray->where('author_name', 'LIKE', '%' . $request->author_name . '%');
        }
      

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }


        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }


        $counttotal = Blog::where(array('is_deleted' => false))->count();

        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $val) {
            $bolgId = $val['_id'];
            $view_link ='';
            $view_link .= '<a href="edit/' . $bolgId . '" class="btn btn-circle btn-icon-only btn-default locked" title="Edit"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>

                <a id="delete-blog" data-id="' . $bolgId . '" class="btn btn-circle btn-icon-only btn-default" title="Delete"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>
  
            ';

            if ($val['status']) {
                $view_link .= '<a class="btn btn-circle btn-icon-only btn-default" title="Unblock" id="change-common-status" data-table="manage_blogs" data-id="' . $val['_id'] . '" data-status="' . $val['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a>';
            } else {
                $view_link .= '<a class="btn btn-circle btn-icon-only btn-default" title="Block" id="change-common-status" data-table="manage_blogs" data-id="' . $val['_id'] . '" data-status="' . $val['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a>';
            }
            
            $link = url('admin/blogs/comment-list/').'/'.$bolgId;
            $view_link .='<a href="'.$link.'" class="btn btn-circle btn-icon-only btn-default " title="View Comments"><i class="fa glyphicon glyphicon-envelope text-danger inactive"></i><a>';
       
           $date  = Carbon::parse($val['created_at'])->format('M') . '-' . Carbon::parse($val['created_at'])->format('d') . '-' . Carbon::parse($val['created_at'])->format('Y');
          
            $title = $val['title'];
            $author_name = $val['author_name'];

            $catName = BlogCategory::where('_id',$val['blog_cat_id'])->pluck('name')->first();
           


            if ($val['status']) {
                $status = 'Active';
            } else {
                $status = 'Inactive';
            }
            $GLOBALS['data'][] = array($i, $title, $author_name, $status, $catName, $date,  $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }


    /*================================================
     * Remove the specified blog from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function destroy($id)
    {
        $blog = Blog::where('_id', $id)->first();

        if ($blog->update(array('is_deleted' => true))) {
           //unlink 
          if(!empty($blog->blog_image)){
            $path = public_path(). '/blog_images/'.$blog->blog_image;
            unlink($path);    
          }

          if(!empty($blog->author_image)){
            $path = public_path(). '/author_images/'.$blog->author_image;
            unlink($path);
          }

         flash()->success(trans('Admin/message.delete_blog'));  
         return response()->json(['success' => true]);
        }else{
         flash()->success('Something went wrong');
         return response()->json(['success' => false]);
        }

        
    }



    /*========================
        blokc/Unblock  
    ==========================*/

    public function changeStatus(Request $request)
    {
        $id = $request->id;
        $blog = Blog::where('_id', $id)->first();
       
        if ($blog->update(array('status' => (bool)$request->input('status') ? false : true))) {

           return response()->json(['success' => true,'message'=>trans('Admin/message.block_blog')]);
        }else{
           return response()->json(['success' => false,'message'=>trans('Admin/message.error')]); 
        }
     
        
       
    }


    /*========================
       saveBlogImage
    ==========================*/
    public function saveBlogImage(Request $request){


    $image = $request->file('file');
    $path = public_path(). '/blog_images/';

    if (!Storage::exists($path)) {
        Storage::makeDirectory($path, 0775, true, true);
    }

    $filename = time() . '.' . $image->getClientOriginalExtension();
    $image->move($path, $filename);

    // Assuming that this record does not exist in database
    $TempImage = TempImage::firstOrCreate(['type' => 'Blog']);
    $TempImage->image_name = $filename;
    if($TempImage->save()){
       return response()->json(['status'=>'success']);
    }else{
       return response()->json(['status'=>'error']);
    } 

  }

    /*========================
       saveAuthorImage
    ==========================*/
   public function saveAuthorImage(Request $request){

    $image = $request->file('file');
    $path = public_path(). '/author_images/';
    if (!Storage::exists($path)) {
        Storage::makeDirectory($path, 0775, true, true);
    }
    $filename = time() . '.' . $image->getClientOriginalExtension();
    $image->move($path, $filename);

    // Assuming that this record does not exist in database
    $TempImage = TempImage::firstOrCreate(['type' => 'Author']);
    $TempImage->image_name = $filename;
   
    if($TempImage->save()){
       return response()->json(['status'=>'success']);
    }else{
       return response()->json(['status'=>'error']);
    } 
   
  }

}
