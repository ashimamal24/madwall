<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CmsRequest;
use App\Model\CmsPages;


class CmsController extends Controller
{
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*=====================================
            Display a listing of skills.
    =======================================*/
    public function index()
    {
        $cms = CmsPages::get();
        return view('managecms/listingCms', ['cms' => $cms, 'active' => 'cms']);
    }

    /*=========================
        Creating Skill
    ==========================*/
    public function create()
    {
        return view('managecms/createCms');
    }

    /*================================================================
        Store Skill in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     =================================================================*/
    public function store(CmsRequest $request)
    {
        $data = $request->except('_token');
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['is_deleted'] = false;
        $data['type'] = $request->input('type');
        $data['alias'] = str_replace(' ', '_', strtolower($request->input('alias')));
        $cmsobj = new CmsPages($data);
        if ($cmsobj->save()) {
            flash()->success('CMS Page added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }


    /*======================================================
     * Show the form for editing the specified skill.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function edit(CmsRequest $request, $id)
    {
        $cms = CmsPages::where(array('_id' => $id))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($cms->update($data)) {
            flash()->success('CMS Page updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }

    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function update(Request $request, $id)
    {
        $edit_cms = CmsPages::where(array('_id' => $id))->first();
        return view('managecms/editCms', ['edit_cms' => $edit_cms]);
    }

    /*================================================
     * Remove the specified skill from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================*/
    public function destroy($id)
    {
        $cms = CmsPages::where('_id', $id)->first();
        $cms->update(['is_deleted' => true]);
        if ($cms) {
            flash()->success('CMS Deleted successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterCms(Request $request)
    {
        $basearray = CmsPages::where(array('is_deleted' => false));
        $totalusercount = CmsPages::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = CmsPages::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $cms) {
            $cmsId = $cms['_id'];
            $view_link = '<a cmsId="' . $cmsId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $cms['status'] . '" href="edit-cms/' . $cmsId . '" class="btn btn-circle btn-icon-only btn-default"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>';
            if (isset($cms['name']) && !empty($cms['name'])) {
                $name = $cms['name'];
            }

            if (isset($cms['type']) && !empty($cms['type'])) {
                $type = ucfirst($cms['type']);
            }

            if (isset($cms->content) && !empty($cms->content)) {
                $contentlength = strlen($cms->content);
                if ($contentlength > 100) {
                    $content = substr($cms->content, 0, 50) . '...';
                } else {
                    $content = substr($cms->content, 0, 50);
                }
            }
            $GLOBALS['data'][] = array($i, $name, $content, $type, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
   
   /*=====================================
        View Particular Skill Detail 
    =====================================*/
    public function viewCms(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = CmsPages::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
}
