<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SubCategoryRequest;
use App\Model\CategoryModel;
use App\Model\SkillModel;
use App\Model\Industry;
use DB;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*===========================
        Listing of Subcategories
    =============================
     */
    public function index()
    {
        $subcategories = CategoryModel::where(array('status' => true, 'is_deleted' => false, 'type' => 'subcategories'))->pluck('name', '_id')->toArray();
        return view('subcategory/listingSubcategories', ['subcategories' => $subcategories, 'active' => 'subcategories']);
    }
    
    /*
     * Added on : 7 nov 2017, shivani Debut infotech
     * */
    public function get_categories(Request $request)
    {
        $industry_ids = explode(',', $request->industry_id);
        $categories = CategoryModel::where(['status' => true, 'is_deleted' => false, 'type' => 'category'])->whereIn('industry_id', $industry_ids)->select('name', 'industry_name', '_id')->get()->toArray();
        return response()->json(['category' => $categories]);

    }
    /*===================================
        Load Creating Subateogary View
    ====================================*/

    public function create()
    {
        $skills = SkillModel::where(array('status' => true, 'is_deleted' => false))->orderBy('name')->pluck('name', '_id')->toArray();
        $industries = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        $categories = [];
        return view('subcategory/createSubcategory', ['skills' => $skills, 'categories' => $categories, 'industries' => $industries]);
    }

    /*================================================================
        Store Subcategory in database table
     * @param  \App\Http\Requests\Admin\SubCategoryRequest  $request
     * updated : 7 nov 2017, to save mandatory and optional skills
     =================================================================
     */
    public function addSubcategories(SubCategoryRequest $request)
    {
		
		//check that mandatory and optional fields can not have same values./
        if (!empty($request->input('mandatory_skills')) && !empty($request->input('skills'))) {
            if (array_intersect($request->input('mandatory_skills'), $request->input('skills')) || array_intersect($request->input('skills'), $request->input('mandatory_skills'))) {
                return response()->json(['skills' => 'Skills and mandatory skills can not have same values.'], 422);
            }
        }

        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['name'] = strtolower($request->input('name'));
        $data['description'] = $request->input('description');
        $data['category_id'] = $request->input('category_id');
        $data['industry_id'] = empty($request->input('industry_id')) ? null : $request->input('industry_id');
        $data['category_object'] = CategoryModel::where('_id', $data['category_id'])->get()->toArray();
        $data['category_name'] = $request->input('category_name');
        $data['is_deleted'] = false;
        $data['type'] = 'subcategory';
        $data['used'] = false;
        $data['skills'] = empty($request->input('skills')) ? null : $request->input('skills');
        $data['mandatory_skills'] = empty($request->input('mandatory_skills')) ? null : $request->input('mandatory_skills');
        $selected_skills = empty($request->input('skills')) ? null : SkillModel::whereIn('_id', $data['skills'])->get()->toArray();
        $mandatory_skills = empty($request->input('mandatory_skills')) ? null : SkillModel::whereIn('_id', $data['mandatory_skills'])->get()->toArray();
        $data['skill_object'] = $selected_skills;
        $data['mandatory_object'] = $mandatory_skills;
        $catobj = new CategoryModel($data);
        if ($catobj->save()) {
            flash()->success('Subcategory created successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    public function destroy($id)
    {
        $subcategory = CategoryModel::where(array('_id' => $id))->first();
        if ($subcategory->update(array('is_deleted' => true))) {
            flash()->success('Subcategory deleted successfully');
            return response()->json(['success' => true]);
        }
    }

    /*======================================================
     * Show the form for editing the specified Subcategory.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * updated : 7 nov 2017, to save mandatory and optional skills
     =====================================================*/
    public function editSubcategory(SubCategoryRequest $request, $id)
    {
		//check that mandatory and optional fields can not have same values./
        if (!empty($request->input('mandatory_skills')) && !empty($request->input('skills'))) {
            if (array_intersect($request->input('mandatory_skills'), $request->input('skills')) || array_intersect($request->input('skills'), $request->input('mandatory_skills'))) {
                return response()->json(['skills' => 'Skills and mandatory skills can not have same values.'], 422);
            }
        }
        $subcategory = CategoryModel::where('_id', $id)->first();
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['name'] = strtolower($request->input('name'));
        $data['description'] = $request->input('description');
        $data['category_id'] = $request->input('category_id');
        $data['industry_id'] = empty($request->input('industry_id')) ? null : $request->input('industry_id');
        $data['category_object'] = CategoryModel::where('_id', $data['category_id'])->get()->toArray();
        $data['category_name'] = $request->input('category_name');
        $data['type'] = 'subcategory';
        $data['used'] = false;
        $data['skills'] = empty($request->input('skills')) ? null : $request->input('skills');
        $data['mandatory_skills'] = empty($request->input('mandatory_skills')) ? null : $request->input('mandatory_skills');
        $selected_skills = empty($request->input('skills')) ? null : SkillModel::whereIn('_id', $data['skills'])->get()->toArray();
        $mandatory_skills = empty($request->input('mandatory_skills')) ? null : SkillModel::whereIn('_id', $data['mandatory_skills'])->get()->toArray();
        $data['skill_object'] = $selected_skills;
        $data['mandatory_object'] = $mandatory_skills;
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($subcategory->update($data)) {
            flash()->success('Subcategory updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*================================================
     * Update the specified Category in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     ================================================*/
    public function update(Request $request, $id)
    {
        $edit_subcategories = CategoryModel::where(array('_id' => $id))->first();
        $selected_category = '';

        $selected_category[$edit_subcategories['category_object'][0]['_id']] = $edit_subcategories['category_object'][0]['name'];


        $industries = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();

        $category = CategoryModel::where(array('status' => true, 'is_deleted' => false, 'type' => 'category'));
        if (isset($edit_subcategories->industry_id) && !empty($edit_subcategories->industry_id)) {
            $category->where('industry_id', $edit_subcategories->industry_id);

        }
        $categories = $category->orderBy('name')->pluck('name', '_id')->toArray();
        
       // $categories = CategoryModel::where( array( 'is_deleted'  => false,'type'=>'category' ) )->orderBy('name')->pluck( 'name', '_id' )->toArray();

        $categories = array_merge($categories, $selected_category);
     
        //echo "<pre>";
        $selectedskills = [];
        if (!empty($edit_subcategories['skill_object'])) {
            foreach ($edit_subcategories['skill_object'] as $skills) {
                $selectedskills[$skills['_id']] = $skills['name'];
            }
        }
        
       // print_r($selectedskills);
        $edit_skills = CategoryModel::where(array('_id' => $id))->pluck('skills')->toArray();
        $skills = SkillModel::where(array('is_deleted' => false, 'status' => true))->orderBy('name')->pluck('name', '_id')->toArray();
        
        //$selectedskills = SkillModel::whereIn('_id',$edit_subcategories['skills'])->orderBy('name')->pluck('name','_id')->toArray();
        //print_r($selectedskills);
        //die('here');
        $allskills = array_merge($skills, $selectedskills);


        return view('subcategory/editSubcategory', ['edit_subcategories' => $edit_subcategories, 'categories' => $categories, 'skills' => $allskills, 'industries' => $industries]);
    }

    public function filterSubcategories(Request $request)
    {
        $basearray = CategoryModel::where(array('is_deleted' => false, 'type' => 'subcategory'));
        $totalusercount = CategoryModel::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }

        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } elseif ($order[0]['column'] == 2 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('industry_name', 'asc');
        } elseif ($order[0]['column'] == 2 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('industry_name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = CategoryModel::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $sub_category) {
            $catId = $sub_category['_id'];
            $view_link = '<a catId="' . $catId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $sub_category['status'] . '" href="edit-subcategory/' . $catId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deletesubcategory" data-id=' . $catId . ' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete Category" class="icon-trash" aria-hidden="true"></span></a>';

            if (isset($sub_category['name'])) {
                $name = $sub_category['name'];
            }

            if (isset($sub_category['_id'])) {
                $category_name = $sub_category['category_object'][0]['name'];
            }

            if ($sub_category['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="category" data-id="' . $sub_category['_id'] . '" data-status="' . $sub_category['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="category" data-id="' . $sub_category['_id'] . '" data-status="' . $sub_category['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $name, $category_name, $status, $view_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }

    /*==============================
        View Particular Subcategory 
    ================================*/
    public function viewSubcategories(Request $request)
    {
        $id = $request->id;
        $subcat = CategoryModel::where(array('_id' => $id, ))->pluck('skill_object')->toArray();
        $mndtryskills = $skills = [];
        if (!empty($subcat) && !empty($subcat[0])) {
            foreach ($subcat[0] as $sub) {
                $skills[] = $sub['name'];
            }
        } else {
            $skills[] = 'N/A';
        }
        $mandatory_skills = CategoryModel::where(array('_id' => $id, ))->pluck('mandatory_object')->toArray();
        if (!empty($mandatory_skills) && !empty($mandatory_skills[0])) {
            foreach ($mandatory_skills[0] as $mnd) {
                $mndtryskills[] = $mnd['name'];
            }
        } else {
            $mndtryskills[] = 'N/A';
        }

        $data['skills'] = implode(", ", $skills);
        $data['mandatory_skills'] = implode(", ", $mndtryskills);
        $data['reslutset'] = CategoryModel::where(array('_id' => $id))->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
}
