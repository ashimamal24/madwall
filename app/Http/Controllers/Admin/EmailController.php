<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EmailReguest;
use App\Model\EmailTemplate;
use DB;

class EmailController extends Controller
{
    
    /*===========================
           Listing of emails.
    =============================
     */
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }
    public function index()
    {

        $emails = EmailTemplate::get();
        return view('email/listingemails', ['emails' => $emails, 'active' => 'emails']);
    }

    /*======================================
        Function for Creating Email
    ========================================
     */
    public function create()
    {
        return view('email/createemail');
    }

    /*================================================================
        Store item in database table
     * @param  \App\Http\Requests\Admin\EmailReguest  $request
     =================================================================
     */
    public function store(EmailReguest $request)
    {
        $data = $request->except('_token');
        if ($data['status'] == '1') {
            $data['status'] = true;
        }
        if ($data['status'] == '0') {
            $data['status'] = false;
        }
        $data['is_deleted'] = false;

        $emailobj = new EmailTemplate($data);
        if ($emailobj->save()) {
            flash()->success('Email created successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }


    /*======================================================
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================
     */
    public function edit(EmailReguest $request, $id)
    {
        $data = $request->all();
        $emailobj_update = EmailTemplate::where('_id', $id)->first();
        $data['status'] = $request->input('status') > 0 ? true : false;

        if ($emailobj_update->update($data)) {

            flash()->success('Email updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*================================================
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================
     */
    public function update(Request $request, $id)
    {
        $edit_emails = EmailTemplate::where(array('_id' => $id))->first();
        return view('email/editemail', ['edit_emails' => $edit_emails]);
    }

    /*================================================
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function destroy($id)
    {
        $emailobj = EmailTemplate::where('_id', $id)->first();

        $emailobj->update(['is_deleted' => true]);
        if ($emailobj) {
            flash()->success('Email deleted successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*=====================================
        Feltering Data in ajax request
    =======================================
     */
    public function filterEmails(Request $request)
    {
        $basearray = EmailTemplate::where(array('is_deleted' => false));
        $totalusercount = EmailTemplate::where(array('is_deleted' => false))->count();

        if (isset($request->emailtitle) && !empty($request->emailtitle)) {
            $basearray->where('name', 'LIKE', '%' . $request->emailtitle . '%');
        }

        if (isset($request->emailsubject) && !empty($request->emailsubject)) {
            $basearray->where('subject', 'LIKE', '%' . $request->emailsubject . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }

        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = EmailTemplate::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $start = intval($request->get('start'));
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip($start)->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $emailtemplate) {
            $id = $emailtemplate['_id'];
            $view_link = '<a mailId="' . $id . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a href="edit-email/' . $id . '" class="btn btn-circle btn-icon-only btn-default"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>';

            if (isset($emailtemplate['name']) && !empty($emailtemplate['name'])) {
                $title = $emailtemplate['name'];
            }

            if (isset($emailtemplate->content) && !empty($emailtemplate->content)) {
               //echo $emailtemplate->content;
                $contentlength = strlen($emailtemplate->content);
                if ($contentlength > 100) {
                    $content = substr($emailtemplate->content, 0, 10) . '...';
                } else {
                    $content = substr($emailtemplate->content, 0, 10);
                }
            }

            if (isset($emailtemplate['subject']) && !empty($emailtemplate['subject'])) {
                $subject = $emailtemplate['subject'];
            }

            if ($emailtemplate['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" id="change-common-status" title="Active" data-table="email_templates" data-id="' . $emailtemplate['_id'] . '" data-status="' . $emailtemplate['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" id="change-common-status" title="Inactive" data-table="email_templates" data-id="' . $emailtemplate['_id'] . '" data-status="' . $emailtemplate['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            //$GLOBALS['data'][] = array( $i, $title, $subject, $status, $view_link );
            $GLOBALS['data'][] = array($i, $title, $subject, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }

    /*============================
         View Particular Email 
    ==============================
     */
    public function viewEmail(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = EmailTemplate::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
}
