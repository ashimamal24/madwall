<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Model\CategoryModel;
use App\Model\SkillModel;
use App\Model\Industry;
use DB;
// Category controller
class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*===========================
        Listing of categories
    =============================*/
    public function index()
    {
        $categories = CategoryModel::paginate(10);
        return view('category/listingcategories', ['categories' => $categories, 'active' => 'categories']);
    }

    /*=================================
        Load Creating Cateogary View
    ===================================*/
    public function create()
    {
        $industries = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        /*$categories = CategoryModel::where( array( 'status'   => true, 'is_deleted' => false, 'type' => 'category' ) )->pluck( 'name', '_id' )->toArray();*/
        return view('category/createcategory', ['industries' => $industries]);
    }

    /*================================================================
        Store Category in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
    =================================================================*/
    public function store(CategoryRequest $request)
    {
        $data = $request->except('_token');
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['name'] = strtolower($request->input('name'));
        $data['commision'] = (double)$request->input('commision');
        $data['is_deleted'] = false;
        $data['type'] = 'category';
        $data['used'] = false;
        $catobj = new CategoryModel($data);
        if ($catobj->save()) {
            flash()->success('Category created successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }


    /*======================================================
     * Show the form for editing the specified Category.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function edit(CategoryRequest $request, $id)
    {
        $category = CategoryModel::where(array('_id' => $id))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;

        if ($category->update($data)) {
            $subcatories = CategoryModel::where(array('category_id' => $id, 'type' => 'subcategory'))->pluck('_id')->toArray();
            if (isset($subcatories) && count($subcatories)) {
                foreach ($subcatories as $subcatory) {
                    $subcatories = CategoryModel::where(array('_id' => $subcatory, 'type' => 'subcategory'))->first()->toArray();
                    CategoryModel::where(array('_id' => $subcatories['_id']))->update(array('category_name' => $data['name'], 'status' => $data['status']));
                }
            }
            flash()->success('Category updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }

    /*================================================
     * Update the specified Category in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================
     */
    public function update(Request $request, $id)
    {
        $industries = Industry::where(array('status' => true, 'is_deleted' => false))->pluck('name', '_id')->toArray();
        $edit_categories = CategoryModel::where(array('_id' => $id))->first();
        return view('category/editcategory', ['industries' => $industries, 'edit_categories' => $edit_categories]);
    }

    /*================================================
     * Remove the specified Category from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function destroy($id)
    {
        $category = CategoryModel::where('_id', $id)->first();

        if ($category->update(array('is_deleted' => true))) {
            $subcatories = CategoryModel::where(array('category_id' => $id, 'type' => 'subcategory'))->pluck('_id')->toArray();

            CategoryModel::whereIn('_id', $subcatories)->update(array('is_deleted' => true));
        }

        flash()->success('Category deleted successfully');
        return response()->json(['success' => true]);
    }

    
    /*==================================================
         Feltering Data in ajax request
    ====================================================
     */
    public function filterCategories(Request $request)
    {
        $basearray = CategoryModel::where(array('is_deleted' => false, 'type' => 'category'));
        $totalusercount = CategoryModel::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }
        if (isset($request->industry_name) && !empty($request->industry_name)) {
            $basearray->where('industry_name', 'LIKE', '%' . $request->industry_name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }

        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } elseif ($order[0]['column'] == 2 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('commision', 'asc');
        } elseif ($order[0]['column'] == 2 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('commision', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = CategoryModel::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $category) {
            $catId = $category['_id'];
            $view_link = '<a catId="' . $catId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $category['status'] . '" href="edit-category/' . $catId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deletecategory" data-id=' . $catId . ' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete Category" class="icon-trash" aria-hidden="true"></span></a>';

            if (isset($category['name']) && !empty($category['name'])) {
                $name = $category['name'];
            }

            if (isset($category['commision']) && !empty($category['commision'])) {
                $commision = $category['commision'] . ' %';
            }

            if (isset($category['industry_name']) && !empty($category['industry_name'])) {
                $industry_name = $category['industry_name'];
            }

            if ($category['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="category" data-id="' . $category['_id'] . '" data-status="' . $category['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="category" data-id="' . $category['_id'] . '" data-status="' . $category['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $name, $commision, $industry_name, $status, $view_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }
   
   /*=============================
        View Particular Category 
    ============================*/
    public function viewCategory(Request $request)
    {
        $id = $request->id;
        $subcat = CategoryModel::where(array('category_id' => $id, 'status' => true, 'is_deleted' => false))->pluck('name')->toArray();
        $data['subcategory'] = implode(",", $subcat);
        $data['reslutset'] = CategoryModel::where(array('_id' => $id))->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }

    /*========================
        Lock/Unlock Category 
    ==========================*/

    public function changeStatus(Request $request)
    {
        $id = $request->id;
        $category = CategoryModel::where('_id', $id)->first();
        if ($category->update(array('status' => (bool)$request->input('status') ? false : true))) {
            $subcatories = CategoryModel::where(array('category_id' => $id, 'type' => 'subcategory'))->pluck('_id')->toArray();
        }
        if (isset($subcatories) && count($subcatories)) {
            foreach ($subcatories as $subcatory) {
                $subcatories = CategoryModel::where(array('_id' => $subcatory, 'type' => 'subcategory'))->first()->toArray();
                CategoryModel::where(array('_id' => $subcatories['_id']))->update(array('status' => (bool)$request->input('status') ? false : true));
            }
        }
        flash()->success('Status Changed successfully');
        return response()->json(['success' => true]);

        //$category = CategoryModel::where( array( '_id'=> $id ) )->first();
        /*
        if( $category->update( array('status'=>(bool)$request->input('status') ? false:true))){
            flash()->success( 'Status Changed successfully' );
            return response()->json(['success'=>true ]);
        } else{
            flash()->error( 'Something went wrong' );
            return response()->json(['success'=>true ]);
        }*/

    }
}
