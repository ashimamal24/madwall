<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use Carbon\Carbon;
use App\Model\CategoryModel;
use App\Model\Industry;
use App\Http\Requests\Admin\CommissionRequest;
use App\Http\Requests\Admin\BlockUserRequest;
use App\Http\Repositary\CommonRepositary;//28 july 2017, shivani
use App\Model\EmailTemplate;//28 july 2017, shivani
use App\Jobs\SendOtpEmail;//28 july 2017, shivani
use App\Model\JobsModel;//28 july 2017, shivani

class EmployerController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*================================
        Listing of Waitlising Employe
    =================================*/
    public function getWaitlistEmployer()
    {
        $waitlisting = User::get();
        return view('employe/employerWaitlisting', ['waitlisting' => $waitlisting, 'active' => 'employerwaitlist']);
    }

    /*===================================
        Listing of Approved Employer
    ====================================*/
    public function getApprovedEmployer()
    {
        $approvedemployer = User::get();
        return view('employe/approvedEmployerList', ['approvedemployer' => $approvedemployer, 'active' => 'approvedemployer']);
    }

    /*=====================================
        Display Detail of Waiting Employer
    =======================================*/
    public function viewDetailOfWaitingEmployer($id)
    {
        $userDetail = User::where(array('_id' => $id))->first()->toArray();
        $allindustries = Industry::whereIn('_id', $userDetail['industry_type'])->with(array('relatedcategory' => function ($q) {
            $q->where(array('status' => true, 'is_deleted' => false))->get();
        }))->get()->toArray();   
        //$categories = CategoryModel::whereIn('industry_id',$userDetail['industry_type'])->where(array( 'status' => true, 'is_deleted' => false ) )->get()->toArray();
        return view('employe/employerBasicDetail', ['userDetail' => $userDetail, 'allindustries' => $allindustries]);
    }

    /*=======================================
        Display Detail of Approved Employer
    =========================================*/
    public function detailApprovedEmployer($id)
    {
        $user = $userDetail = User::where(array('_id' => $id))->with('userjobs')->first()->toArray();
        $allindustries = Industry::whereIn('_id', $userDetail['industry_type'])->with(array('relatedcategory' => function ($q) {
            $q->where(array('status' => true, 'is_deleted' => false))->get();
        }))->get()->toArray();
        return view('employe/employerDetailInfo', ['userDetail' => $userDetail, 'allindustries' => $allindustries, 'user' => $user]);
    }

    /*===========================
        Approve Waiting Employer
     * updated on : 28 july 2017, shivani - to send a confirmation mail to employer when admin approves
        updated : 3 aug 2017, shivani. To update employer's job publish status
    ===================================*/
    public function approveWaitingEmployer(request $request, CommonRepositary $common)
    {
        $mwuserid = $common->get_uniq_id('empr');//"EMPR".date("y").mt_rand(10000, 99999);
        $user = User::where('_id', $request['userid'])->first();//->update( array('accepted'=>1 ) );
        switch ($user['approved']) {
            case 1:
                flash()->error('You have already approved this Employer');
                return response()->json(['success' => false]);
                break;
            case 2:
                flash()->error('Employer was already declined earlier');
                return response()->json(['success' => false]);
                break;
            case 3:
                flash()->error('You have already blocked this Employer');
                return response()->json(['success' => false]);
                break;
            default:
                if ($user->update(array('approved' => 1, 'status' => true, 'mwuserid' => $mwuserid, 'approved_date' => Carbon::now()))) {
                    //update job publisher status, when employer is approved - 3 aug
                    if ($userPostedJobs = JobsModel::where("user_id", $request['userid'])->count()) {
                        //if jobs found, then update status
                        JobsModel::where("user_id", $request['userid'])->update(['publisher_status' => true]);
                    }
					//send confirmation email to employer i.e.. employer has been approved by the admin.
                    $template = EmailTemplate::find('5985884fb098f028f527e186');
                    $find = array('@name@');
                    $values = array($user->first_name);
                    $body = str_replace($find, $values, $template->content);
                    $this->dispatch(new SendOtpEmail($body, $user->email, $template));
					//======================== end 28 july 2017 ============================
                    
                    // Send bell notification to employer
                    $common->saveNotification('5910610142997575ee131321', $request['userid'], '2', '4', null, "Congrats! You have been approved by Admin.");
					// End
                    flash()->success('Employer approved successfully');
                    return response()->json(['success' => true]);
                } else {
                    flash()->success('Something went wrong');
                    return response()->json(['success' => false]);
                }
        }
    }

    /*=============================
        Decline Waiting Employer
        updated : 3 aug 2017, shivani. To update employer's job publish status
    ===============================*/
    public function declineWaitingEmployer(Request $request, CommonRepositary $common)
    {
        $decline_user = User::where('_id', $request->input('id'))->first();
        switch ($decline_user['approved']) {
            case 1:
                flash()->error('You have already approved this Employer');
                return response()->json(['success' => false]);
                break;
            case 2:
                flash()->error('Employer was already declined earlier');
                return response()->json(['success' => false]);
                break;
            default:
                if ($decline_user->update(array('status' => false, 'approved' => 2))) {

                    //update job publisher status, when employer is declined
                    if ($userPostedJobs = JobsModel::where("user_id", $request->input('id'))->count()) {
                        //if jobs found, then update status
                        JobsModel::where("user_id", $request->input('id'))->update(['publisher_status' => false]);
                    }

                    // Send bell notification to employer
                    $adminData = $common->getAdminDetail();
                    $common->saveNotification($adminData->_id, $request->input('id'), '2', '4', null, "Sorry, Your profile has been declined by MadWall.");
                    // End

                    flash()->success('Employer declined successfully');
                    return response()->json(['success' => true]);
                } else {
                    flash()->error('Something went wrong');
                    return response()->json(['success' => false]);
                }
        }
    }

    /*=================
        Block Employer
        updated : 3 aug 2017, shivani. To update employer's job publish status
    ==================*/
    public function blockEmployer(BlockUserRequest $request, CommonRepositary $common)
    {

        $block_user = User::where('_id', $request->input('userid'))->first();
        switch ($block_user['approved']) {
            case 2:
                //flash()->error(  );
                return response()->json(['success' => false, 'message' => 'You have already declined this employer']);
                break;
            case 3:
                //flash()->error( 'Employer was already blocked earlier' );
                return response()->json(['success' => false, 'message' => 'Employer was already blocked earlier']);
                break;
            default:
                if ($block_user->update(array('status' => false, 'approved' => 3, 'reason_for_block_usr' => $request['reason_for_block_usr']))) { 
                    //update job publisher status, when employer is declined
                    //~ if($userPostedJobs = JobsModel::where("user_id",$request->input('userid'))->count()){
                        //~ //if jobs found, then update status
                        //~ JobsModel::where("user_id",$request->input('userid'))->update(['publisher_status'=>false]);
                    //~ }
                    $template = EmailTemplate::find('59a4f9b44299752faa19f371');
                    $find = array('@name@', '@reason@');
                    $values = array($block_user['first_name'], $request['reason_for_block_usr']);
                    $body = str_replace($find, $values, $template->content);
                    $this->dispatch(new SendOtpEmail($body, $block_user['email'], $template));
                    
                    // Bell Notification for Employer
                    // Send bell notification to employer
                    $adminData = $common->getAdminDetail();
                    $common->saveNotification($adminData->_id, $request->input('userid'), '2', '6', null, "Sorry, Your profile has been blocked by MadWall.");
                    // End Bell notification
                    //flash()->success(  );
                    return response()->json(['success' => true, 'message' => 'Employer blocked successfully']);
                } else {
                    //flash()->error(  );
                    return response()->json(['success' => false, 'message' => 'Something went wrong']);
                }
        }
    }

    /*===================
        Unblock Employer
    =====================*/
    public function unBlockEmployer(request $request)
    {
        $unblock_user = User::where('_id', $request->input('userid'))->first();
        switch ($unblock_user['approved']) {
            case 1:
                flash()->error('Employer was already unblocked earlier');
                return response()->json(['success' => false]);
                break;
            case 2:
                flash()->error('You have already declined this employer');
                return response()->json(['success' => false]);
                break;
            default:
                if ($unblock_user->update(array('status' => true, 'approved' => 1))) {
                    flash()->success('Employer unblocked successfully');
                    return response()->json(['success' => true]);
                } else {
                    flash()->error('Something went wrong');
                    return response()->json(['success' => false]);
                }
        }
    }

    /*================================
        Load View to give Commission
    =================================*/
    public function updateApprovedEmployer(Request $request, $id)
    {
        $user = User::where(array('_id' => $id))->first()->toArray();
        $approved = $user['approved'];
        $industries = CategoryModel::where(array('status' => true, 'is_deleted' => false))->whereIn('industry_id', $user['industry_type'])->whereNotIn('type', ['subcategory'])->get()->toArray();
        $categoryDetails = CategoryModel::where(array('status' => true, 'is_deleted' => false))->whereIn('industry_id', $user['industry_type'])->whereNotIn('type', ['subcategory'])->pluck('name', '_id');
        return view('employe/commission', ['id' => $id, 'approved' => $approved, 'user' => $user, 'industries' => $industries, 'categoryDetails' => $categoryDetails]);
    }
    
    /*
     * Added on : 28 sep 2017, shivani - Debut Infotech
     * DESC : to return extra/discount given to a particular category for a particular user
     * */
    public function category_commision_value(Request $request)
    {
        $user = User::where(array('_id' => $request->user_id))->first()->toArray();
        if (!empty($user)) {
            if (isset($user['category_details']) && !empty($user['category_details'])) {
                foreach ($user['category_details'] as $catDtl) {
                    if ($catDtl['_id'] == $request->cat_id) {
                        return response()->json(['success' => true, 'extra' => $catDtl['extra'], 'discount' => $catDtl['discount']], 200);
                    }
                }
            }
        }
        return response()->json(['success' => false], 200);
    }
    
    /*===================
        Give Commission
    ====================*/
    public function giveCommisssionToEmployer(CommissionRequest $request)
    {
        $user = User::where('_id', $request['userid'])->first();
        if (isset($request['commission'])) {
            $commission = $request['commission'];
        } else {
            $commission = 0;
        }

        switch ($user['approved']) {
            case 2:
                flash()->error('Employer was already declined earlier');
                return response()->json(['success' => false]);
                break;
            case 3:
                flash()->error('Employer was already blocked earlier');
                return response()->json(['success' => false]);
                break;
            default:

                $category_details = CategoryModel::where(array('status' => true, 'type' => 'category', 'is_deleted' => false))->whereIn('industry_id', $user['industry_type'])->select('name', '_id')->whereNotIn('type', ['subcategory'])->get()->toArray();
                $catDetail = [];
                if (!empty($category_details)) {
                    foreach ($category_details as $catDtl) {
                        if ($catDtl['_id'] == $request->category) {
                            $discAmount = $extraAmount = 0;
                            if ($request->extra_discount == 'discount') {
                                $discAmount = floatval($request->commission);
                            }
                            if ($request->extra_discount == 'extra') {
                                $extraAmount = floatval($request->commission);
                            }
                            $catDetail[] = ['_id' => $catDtl['_id'], 'discount' => $discAmount, 'extra' => $extraAmount, 'name' => $catDtl['name']];
                        } else {
                            $discount = $extra = 0;
                            if (!empty($user['category_details'])) {
                                foreach ($user['category_details'] as $usrCatDtl) {
                                    if ($usrCatDtl['_id'] == $catDtl['_id']) {
                                        $discount = $usrCatDtl['discount'];
                                        $extra = $usrCatDtl['extra'];
                                    }
                                }
                            }

                            $catDetail[] = ['_id' => $catDtl['_id'], 'discount' => $discount, 'extra' => $extra, 'name' => $catDtl['name']];
                        }
                    }
                }
                $category_details = (!empty($catDetail)) ? $catDetail : null;
                if ($user->update(array('category_details' => $category_details))) {
                    flash()->success('Commission updated successfully');
                    return response()->json(['success' => true]);
                } else {
                    flash()->success('Something went wrong');
                    return response()->json(['success' => false]);
                }
        }

    }

    /*================================================
        Feltering Waiting Jobkseeker in ajax request
    ==================================================*/
    public function employerWaitListing(Request $request)
    {
        $basearray = User::where(array('is_deleted' => false, 'role' => 3, 'approved' => 0))->orderBy('created_at', 'desc');
        $totalusercount = User::where(array('is_deleted' => false, 'role' => 3, 'approved' => 0))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('company_name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->contact_name) && !empty($request->contact_name)) {
            $basearray->where('contact_name', 'LIKE', '%' . $request->contact_name . '%');
        }

        if (isset($request->email) && !empty($request->email)) {
            $basearray->where('email', 'LIKE', '%' . $request->email . '%');
        }


        /** If Single set **/
        if (isset($request->requested_at_from) || isset($request->requested_at_to)) {
            $start = Carbon::parse($request->requested_at_from);
            $start2 = Carbon::parse($request->requested_at_to);
            $start_day_start = $start->startOfDay();
            $start_day_end = $start2->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start)->where('created_at', '<=', $start_day_end);
        }
        /** If Both set **/
        if (isset($request->requested_at_from) && isset($request->requested_at_to)) {
            $start_both = Carbon::parse($request->requested_at_from);
            $end_both = Carbon::parse($request->requested_at_to);
            $start_day_start_both = $start_both->startOfDay();
            $start_day_end_both = $end_both->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start_both)->where('created_at', '<=', $start_day_end_both);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('company_name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('company_name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = User::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );
        foreach ($resultset as $user) {
            $userId = $user['_id'];
            $view_link = '<a id="view-detail" data-toggle="modal" href="view-detail/' . $userId . '" class="btn btn-circle btn-icon-only btn-default"><span class="icon-eye" style="color:blue;"></span></a>&nbsp;';
            
             //added on : 11 sep 2017, shivani
            $delete_link = '<a data-status="' . $user['status'] . '"  href="javascript:void(0)" data-url="delete-employer/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked delete-user"><span style="color:red" title="delete" class="icon-trash delete-user" aria-hidden="true"></span></a> &nbsp;';
            
            //12 sep 2017, shivani
            $verify_link = '<a data-status="' . $user['status'] . '" href="javascript:void(0)" data-url="verify-employer-email/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked verify-user"><span style="color:green" title="verify email" class="fa fa-thumbs-o-up verify-user" aria-hidden="true"></span></a> ';

            if (isset($user['company_name']) && !empty($user['company_name'])) {
                $company_name = '<a id="view-detail" data-toggle="modal" href="view-detail/' . $userId . '" >' . $user['company_name'] . '</a>';
            } else {
                $company_name = "N/A";
            }

            if (isset($user['first_name']) && !empty($user['first_name'])) {
                $contact_name = $user['first_name'] . " " . $user['last_name'];
            } else {
                $contact_name = "N/A";
            }
            $industries = '';
            foreach ($user['industry'] as $indusrty) {
                $industries .= $indusrty['name'] . ',';
            }

            if (isset($user['email'])) {
                $email = $user['email'];
            }

           /* if( isset($user['company_code'] ) && !empty( $user['company_contact'] ) ) {
                $phone =$user['company_code'] ." ". $user['company_contact'];
            } else{
                $phone = 'N/A';
            }*/

            //10 aug 2017, shivani
            $phone = 'N/A';

            if (isset($user['company_code']) && !empty($user['company_contact']) && $user['company_contact'] != null) {

                $phone = $user['company_code'] . " " . $user['company_contact'];
            }
            if (isset($user['country_code']) && !empty($user['phone'])) {
                $phone = $user['country_code'] . " " . $user['phone'];
            }

            if (isset($user['created_at'])) {            
                //$created_at = Carbon::parse( $user['created_at'] )->format('d').' '.Carbon::parse( $user['created_at'] )->format('F').', '.Carbon::parse( $user['created_at'] )->format('Y');
                $created_at = Carbon::parse($user['created_at'])->format('M') . '-' . Carbon::parse($user['created_at'])->format('d') . '-' . Carbon::parse($user['created_at'])->format('Y');
            }


            $GLOBALS['data'][] = array($i, $company_name, $contact_name, trim($industries, ','), $email, $phone, $created_at, $view_link . ' ' . $delete_link . ' ' . $verify_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }
    
    
    /*=======================================
        Added on : 11 sep 2017, shivani
     * to hard delete employer (temporary basis - to remove fake accounts)
    =========================================*/
    public function getDeleteEmployer($id)
    {
        $userDetail = User::where(array('_id' => $id))->first();
        if (!empty($userDetail)) {
            User::where(array('_id' => $id))->delete();
            flash()->success('Employer has been deleted');
        }
        return redirect(url()->previous());
    }
    
    
    /*=======================================
        Added on : 11 sep 2017, shivani
     * to verify employer email (temporary basis )
    =========================================*/
    public function getVerifyEmployerEmail($id)
    {
        $userDetail = User::where(array('_id' => $id))->first();
        if (!empty($userDetail)) {
            User::where(array('_id' => $id))->update(['email_verification_code' => null, 'new_email' => null, 'link_expire_at' => null, 'status' => true]);
            flash()->success('Employer email has been verified');
        }
        return redirect(url()->previous());
    }
    
    

    /*==================================================
        Feltering Approved Employer in ajax request
        updated : 10 aug 2017, shivani - phone issue resolved
    ==================================================*/
    public function approvedEmployerList(Request $request)
    {
        $basearray = User::where(array('is_deleted' => false, 'role' => 3, ))->WhereIn('approved', [1, 3])->orderBy('approved_date', 'desc');
        $totalusercount = User::where(array('is_deleted' => false, 'role' => 3, ))->WhereIn('approved', [1, 3])->count();
        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('company_name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('_id', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('_id', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }

        $counttotal = User::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );
        foreach ($resultset as $user) {
            $userId = $user['_id'];
            $view_link = '<a data-status="' . $user['status'] . '" href="edit-approved-employer/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a> &nbsp;';
            //added on : 11 sep 2017, shivani
            $delete_link = '<a href="javascript:void(0)" data-status="' . $user['status'] . '" data-url="delete-employer/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked delete-user"><span style="color:red" title="Edit" class="icon-trash" aria-hidden="true"></span></a> &nbsp;';
            
            //12 sep 2017, shivani
            $verify_link = '<a data-status="' . $user['status'] . '" href="javascript:void(0)" data-url="verify-employer-email/' . $userId . '" class="btn btn-circle btn-icon-only btn-default locked verify-user"><span style="color:green" title="verify email" class="fa fa-thumbs-o-up" aria-hidden="true"></span></a> ';

            if (isset($user['mwuserid'])) {
                $id = $user['mwuserid'];
            }

            if (isset($user['company_name'])) {
                $company_name = '<a data-status="' . $user['status'] . '" href="detail-approved-employer/' . $userId . '">' . $user['company_name'] . '</a>';
            } else {
                $company_name = "N/A";
            }

            if (isset($user['first_name']) && !empty($user['first_name'])) {
                $contact_name = $user['first_name'] . " " . $user['last_name'];
            } else {
                $contact_name = "N/A";
            }

            $industries = '';
            foreach ($user['industry'] as $industry) {
                $industries .= $industry['name'] . ",";
            }


            if (isset($user['location']) && !empty($user['location'])) {
                $location = $user['location'];
            } else {
                $location = "N/A";
            }

            if (isset($user['email'])) {
                $email = $user['email'];
            }

            $phone = 'N/A';

            if (isset($user['company_code']) && !empty($user['company_contact']) && $user['company_contact'] != null) {

                $phone = $user['company_code'] . " " . $user['company_contact'];
            }
            if (isset($user['country_code']) && !empty($user['phone'])) {
                $phone = $user['country_code'] . " " . $user['phone'];
            }

            if ($user['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" id="change-common-status" data-table="users" data-id="' . $user['_id'] . '" data-status="' . $user['status'] . '" data-approved="' . $user['approved'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" id="change-common-status" data-table="users" data-id="' . $user['_id'] . '" data-status="' . $user['status'] . '" data-approved="' . $user['approved'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $id, $company_name, $contact_name, trim($industries, ','), $location, $email, $phone, $status, $view_link . ' ' . $delete_link . ' ' . $verify_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }

    /*=========================
        Change Status Of User
    ===========================*/
    public function changeStatus(Request $request, CommonRepositary $common)
    {
        $id = $request->id;
        $unblock_user = User::where('_id', $id)->first();
        if ($request['status'] == '1' && $request['approved'] == 1) {
            $unblock_user->update(['status' => false, 'approved' => 3]);
        } else {
            $unblock_user->update(['status' => true, 'approved' => 1]);
            $adminData = $common->getAdminDetail();
            $common->saveNotification($adminData->_id, $id, '2', '6', null, "Your profile has been enabled by Madwall.");
        }
        return response()->json(['success' => true, 'action' => $request['action']]);
    }

}
