<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DocumentRequest;
use App\Model\Document;

class DocumentController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }


    # Display a listing of Documents
    # ===============================

    public function index()
    {
        $ginfo = Document::get();
        return view('document/listingDocument', ['document' => $ginfo, 'active' => 'document']);
    }



    # Create Document
    # ===============
    public function create()
    {
        return view('document/createDocument');
    }



    # Store Document in database table
    # =================================
    public function store(DocumentRequest $request)
    {
        //print_r('sss');die;
        $data = $request->except('_token');
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['is_deleted'] = false;
        $ginfoobj = new Document($data);
        if ($ginfoobj->save()) {
            flash()->success('Document Added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }


    # Edit Document
    # =============

    public function edit(DocumentRequest $request, $id)
    {
        $ginfo = Document::where(array('_id' => $id))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($ginfo->update($data)) {
            flash()->success('Document Updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }


    # Update Document
    # ================
    public function update(Request $request, $id)
    {
        $edit_ginfo = Document::where(array('_id' => $id))->first();
        return view('document/editDocument', ['edit_ginfo' => $edit_ginfo]);
    }



    # Destroy Document
    # ================
    public function destroy($id)
    {
        $ginfo = Document::where('_id', $id)->first();
        $ginfo->update(['is_deleted' => true]);
        if ($ginfo) {
            flash()->success('Document deleted successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }



    # Feltering Data in ajax request
    # ==============================

    public function filterDocument(Request $request)
    {
        $basearray = Document::where(array('is_deleted' => false));
        $totalusercount = Document::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = Document::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $ginfo) {
            $ginfoId = $ginfo['_id'];
            $view_link = '<a ginfoId="' . $ginfoId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $ginfo['status'] . '" href="edit-document/' . $ginfoId . '" class="btn btn-circle btn-icon-only btn-default"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deleteginfo" data-id=' . $ginfoId . ' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>';
            if (isset($ginfo['name']) && !empty($ginfo['name'])) {
                $name = $ginfo['name'];
            }

            if (isset($ginfo['general_file_name']) && !empty($ginfo['general_file_name'])) {
                $general_file_name = $ginfo['general_file_name'];
            }

            if ($ginfo['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="documents" data-id="' . $ginfo['_id'] . '" data-status="' . $ginfo['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="documents" data-id="' . $ginfo['_id'] . '" data-status="' . $ginfo['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $name, $general_file_name, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
   

    # View Document
    # =============
    public function viewDocument(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = Document::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            $data['reslutset']['file_url'] = url('public/document/');
            return json_encode($data);
        }
    }
}
