<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\JobsModel;
use App\Model\JobsapplicationModel;
use App\Helpers\Helper;
use Carbon\Carbon;
use Request as AjaxRequest;
use App\Model\User;
use Response;

class PaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*=======================
        Receive Payment View
    ========================*/
    public function receivePaymentView()
    {
        return view('payment/receivePayment');
    }

    /*=====================
        Topay Payment View 
    ========================*/
    public function topayEmployeView()
    {
        return view('payment/toPayEmployePayment');
    }

    /*====================================
        Data to get Payment from Employer 
    ======================================*/
    public function paymentToReceive(request $request)
    {
        $data = $request->all();
        $records = 10;

        if (!empty($data['records'])) {
            $records = intval($data['records']);
        }

        $showing_date = Carbon::parse($request['start_date']);
        $start_date = Carbon::parse($request['start_date'])->startOfDay();
        $end_date = Carbon::parse($request['end_date'])->endOfDay();

        $jobs = JobsModel::whereIn('job_status', [1, 2, 4, 5, 7])
            ->where('total_work_hours', '!=', 0)
            ->where('total_work_hours', '!=', null)
            ->whereHas('jobapplied', function ($q) use ($start_date, $end_date) {
                $q->whereIn('job_status', [1, 2, 4, 5, 7])
                    ->where('total_hours_worked', '!=', null)
                    ->where('total_hours_worked', '!=', 0)
                    ->whereHas('app_shift', function ($q) use ($start_date, $end_date) {
                        $q->where('start_date', '>=', $start_date)
                            ->where('end_date', '<=', $end_date)
                            ->where('total_hours_worked', '!=', 0)
                            ->whereNotNull('total_hours_worked');
                    });
            })
            ->with([
                'jobapplied' => function ($q) use ($start_date, $end_date) {
                    $q->whereIn('job_status', [1, 2, 4, 5, 7])
                        ->where('total_hours_worked', '!=', null)
                        ->where('total_hours_worked', '!=', 0)
                        ->with([
                            'app_shift' => function ($q) use ($start_date, $end_date) {
                                $q->where('start_date', '>=', $start_date)
                                    ->where('end_date', '<=', $end_date)
                                    ->where('total_hours_worked', '!=', 0)
                                    ->whereNotNull('total_hours_worked');
                            }
                        ]);
                }
            ]);

        if ($data['emp_name'] && !empty($data['emp_name'])) {
            $emp_name = $data['emp_name'];
            $user_ids = User::where('first_name', 'LIKE', $emp_name . '%')->pluck('_id');
            $jobs->whereIn('user_id', $user_ids);
        }
        $jobData = $jobs->paginate($records)->setPath('payment-to-receive');
        $jobCount = $jobData->total();
        return view('payment/receivePaymentData', compact('jobData', 'jobCount', 'records', 'showing_date', 'end_date'))->render();
    }

    /*===========================
        Data to pay to Jobseeker 
    ============================*/
    public function paymentToPay(request $request)
    {
        $data = $request->all();
        $records = 10;

        if (!empty($data['records'])) {
            $records = intval($data['records']);
        }

        $showing_date = Carbon::parse($request['start_date']);
        $start_date = Carbon::parse($request['start_date'])->startOfDay();
        $end_date = Carbon::parse($request['end_date'])->endOfDay();

        // find the job applicatin between these dates
        $application_shifts = JobsapplicationModel::whereIn('job_status', [1, 2, 4, 5, 7])
            ->where('total_hours_worked', '!=', null)
            ->where('total_hours_worked', '!=', 0)
            ->whereHas('app_shift', function ($q) use ($start_date, $end_date) {
                $q->where('start_date', '>=', $start_date)
                    ->where('end_date', '<=', $end_date)
                    ->where('total_hours_worked', '!=', 0)
                    ->whereNotNull('total_hours_worked');
            })
            ->with([
                'job',
                'employerjobs',
                'userjobapplied',
                'app_shift' => function ($q) use ($start_date, $end_date) {
                    $q->where('start_date', '>=', $start_date)
                        ->where('end_date', '<=', $end_date)
                        ->where('total_hours_worked', '!=', 0)
                        ->whereNotNull('total_hours_worked');
                }
            ]);

        if ($data['emp_name'] && !empty($data['emp_name'])) {
            $emp_name = $data['emp_name'];
            $jobseekerId = User::where('first_name', 'LIKE', $emp_name . '%')->pluck('_id');
            $application_shifts->whereIn('jobseeker_id', $jobseekerId);
        }

        $jobData = $application_shifts->paginate($records)->setPath('payment-to-pay');
        $jobCount = $jobData->total();
        return view('payment/ayPaymentData', compact('jobData', 'jobCount', 'records', 'showing_date', 'start_date', 'end_date'))->render();
    }

    /*============================
        Export CSV of Recive Data 
    =============================*/
    public function exportReceiveCsv(request $request)
    {
        $data = $request->all();
        if (AjaxRequest::ajax()) {
            $showing_date = Carbon::parse($request['start_date']); 
            //$start_date = Carbon::parse($request['start_date'])->subDays(5);
            $start_date = Carbon::parse($request['start_date'])->startOfDay();
            $end_date = Carbon::parse($request['end_date'])->endOfDay();
            //no. of records to display
            if (!empty($data['records'])) {
                $records = intval($data['records']);
            }

            if (isset($start_date) && isset($end_date)) {
                $basearray = JobsModel::with(['jobwithuser', 'jobshifts'])->where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->whereIn('job_status', [4, 5, 7]);
            }

            if ($data['emp_name'] && !empty($data['emp_name'])) {
                $emp_name = $data['emp_name'];
                $user_ids = User::where('first_name', 'LIKE', $emp_name . '%')->pluck('_id');
                $basearray = JobsModel::with(['jobwithuser', 'jobshifts'])->where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->whereIn('user_id', $user_ids)->whereIn('job_status', [4, 5, 7]);
            }
        }
        $jobCount = $basearray->count(); 
        //$jobData = $basearray->paginate($records);
        $jobData = $basearray->where('total_work_hours', '!=', 0)->where('total_work_hours', '!=', null)->get();
        $commission = 0;
        if (count($jobData->toArray()) > 0) {
            $filename = "uploads/Accounts Receivable for Period " . $start_date->format('d-m-Y') . " - " . $end_date->format('d-m-Y') . ".csv";
            $handle = fopen($filename, 'w');
            fputcsv($handle, array('No.', 'Employer Name', 'Job Name', 'Number of Days', 'Number of Persons', 'Total Hours', 'Salary Per Hour', 'Commision', 'Total Amount', 'MadWall Commission'));
            foreach ($jobData as $key => $base) {
                if (isset($base['jobwithuser']) && count($base['jobwithuser']) > 0) {
                    if (isset($base['jobshifts']) && count($base['jobshifts']) > 0) {
                        foreach ($base['jobshifts'] as $jobshift) {
                            $hours[] = $jobshift['shift_time'] / 60;
                            $total_working_hour = array_sum($hours);
                            $total_sal = $total_working_hour * $base['total_hired'] * count($base['jobshifts']);
                        }
                        $total_sal = ($base['total_work_hours'] * $base['salary_per_hour']);
                        $totalCommision = $base['category'][0]['commision'];
                        if (isset($base['category_commision']) && !empty($base['category_commision'])) {
                            $totalCommision = $base['category_commision'];
                        }
                        $commission = (($total_sal * $totalCommision) / 100);
                        $madwall_commission = $total_sal + $commission;

                        fputcsv($handle, array($base['jobwithuser']['mwuserid'], $base['jobwithuser']['first_name'] . ' ' . $base['jobwithuser']['last_name'], $base['title'], count($base['jobshifts']), $base['total_hired'], $base['total_work_hours'], '$' . $base['salary_per_hour'], $totalCommision . '%', '$' . $total_sal, '$' . $madwall_commission));
                    }
                }
            }
            fclose($handle);

            $headers = array(
                "Content-type" => "text/csv"

            );
            return response()->json(array('url' => $filename, 'status' => true));
        } else {
            return response()->json(array('message' => 'Sorry, No results to export', 'status' => false));
        }
    }

     /*===========================
        Export CSV of Payable Data
    ==============================*/
    public function exportPayableCsv(request $request)
    {
        $data = $request->all();
        if (AjaxRequest::ajax()) {
            $showing_date = Carbon::parse($request['start_date']); 
            //$start_date = Carbon::parse($request['start_date'])->subDays(3);
            $start_date = Carbon::parse($request['start_date'])->startOfDay();
            $end_date = Carbon::parse($request['end_date'])->endOfDay();
            if (isset($start_date) && isset($end_date)) {
				//check job ids that comes within the selected date range
                $job_ids = JobsModel::where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->whereIn('job_status', [4, 5, 7])->pluck('_id');
				//query job applications according to job ids
                $basearray = JobsapplicationModel::with(array('job', 'employerjobs', 'userjobapplied', 'job_shift'))->whereIn('job_status', [4, 5, 7])->where('total_hours_worked', '!=', null)->where('total_hours_worked', '!=', 0)->whereIn('job_id', $job_ids);
            }
            if (!empty($data['records'])) {
                $records = intval($data['records']);
            }
            if ($data['emp_name'] && !empty($data['emp_name'])) {
                $emp_name = $data['emp_name'];
               //check job ids that comes within the selected date range
                $job_ids = JobsModel::where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->whereIn('job_status', [4, 5, 7])->pluck('_id');
				//check jobseekers
                $jobseekerId = User::where('first_name', 'LIKE', $emp_name . '%')->pluck('_id');
				//query job applications according to job ids
                $basearray = JobsapplicationModel::with(array('job', 'employerjobs', 'userjobapplied', 'job_shift'))->whereIn('jobseeker_id', $jobseekerId)->whereIn('job_status', [4, 5, 7])->where('total_hours_worked', '!=', null)->where('total_hours_worked', '!=', 0)->whereIn('job_id', $job_ids);
            }
        }
        $jobCount = $basearray->count(); 
        //$jobData = $basearray->paginate($records);
        $jobData = $basearray->get();

        $referpayment = array();
        $simplepayment = array();
        $all = array();
        if (count($jobData->toArray()) > 0) {
            $filename = "uploads/Accounts Payable for Period " . $start_date->format('d-m-Y') . " - " . $end_date->format('d-m-Y') . ".csv";

            $handle = fopen($filename, 'w');
            fputcsv($handle, array('No.', 'Employee Name', 'Job Name', 'Employer Name', 'Number of Days', 'Total Hours', 'Salary Per Hour', 'Total Amount'));
            foreach ($jobData as $key => $base) {
                if (isset($base['userjobapplied']) && !empty($base['userjobapplied'])) {
                    if (isset($base['job_shift']) && count($base['job_shift']) > 0) {
                        if (isset($base['userjobapplied']['reffered_by'])) {
                            $refer_code = Helper::referby($base['userjobapplied']['_id'], $start_date, $end_date, $base['userjobapplied']['reffered_by']);
                            fputcsv($handle, array($base['userjobapplied']['mwuserid'], $base['userjobapplied']['first_name'] . ' ' . $base['userjobapplied']['last_name'], $refer_code, '--', '--', '--', '--', '--'));
                        }
                        $total_sal = $base['total_hours_worked'] * $base['job']['salary_per_hour'];
                        fputcsv($handle, array($base['userjobapplied']['mwuserid'], $base['userjobapplied']['first_name'] . ' ' . $base['userjobapplied']['last_name'], $base['job']['title'], $base['employerjobs']['first_name'] . ' ' . $base['employerjobs']['last_name'], count($base['job_shift']), $base['total_hours_worked'], '$' . $base['job']['salary_per_hour'], '$' . $total_sal));
                    }
                }
            }
            fclose($handle);
            $headers = array(
                "Content-type" => "text/csv"
            );
            return response()->json(array('url' => $filename, 'status' => true));
        } else {
            return response()->json(array('message' => 'Sorry, No results to export', 'status' => false));
        }
    }
}
