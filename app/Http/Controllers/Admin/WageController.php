<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\WageRequest;
use App\Model\Wage;
use Validator;

class WageController extends Controller
{

    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*============================
        Display a listing of Wage.
    =============================*/
    public function index()
    {
        $wage = Wage::get();
        return view('wage/listingWage', ['wage' => $wage, 'active' => 'wage']);
    }

    /*================
        Creating Wage
    ==================*/
    public function create()
    {
        return view('wage/createWage');
    }

    /*================================================================
        Store Skill in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     =================================================================*/
    public function storeWage(request $request)
    {

        if (doubleval($request->input('min_amount')) == 0) {
            return response()->json(['min_amount' => 'Minimum wage amount must be greater than 0.'], 422);
        }


        $data['min_amount'] = doubleval($request->input('min_amount'));
        $data['is_deleted'] = false;
        $data['status'] = $request->input('status') > 0 ? true : false;
        $wageobj = new Wage($data);
        if ($wageobj->save()) {
            flash()->success('Wage added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }


    /*======================================================
     * Show the form for editing the specified skill.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function edit(WageRequest $request)
    {

        if (doubleval($request->input('min_amount')) == 0) {
            return response()->json(['min_amount' => 'Minimum wage amount must be greater than 0.'], 422);
        }


        $wageobj = Wage::where(array('_id' => $request->input('idedit')))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['min_amount'] = doubleval($request->input('min_amount'));
        if ($wageobj->update($data)) {
            flash()->success('Wage updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }

    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function update(Request $request, $id)
    {
        $edit_wage = Wage::where(array('_id' => $id))->first();
        return view('wage/editWage', ['edit_wage' => $edit_wage]);
    }

      /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterWage(Request $request)
    {
        $basearray = Wage::where(array('is_deleted' => false));
        $totalusercount = Wage::where(array('is_deleted' => false))->count();

        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = Wage::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $realtor) {
            $wageId = $realtor['_id'];
            $view_link = '<a href="edit-wage/' . $wageId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>';
            if (isset($realtor['min_amount'])) {
                $min_amount = $realtor['min_amount'];
            }

            if ($realtor['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="manage_wage" data-id="' . $realtor['_id'] . '" data-status="' . $realtor['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="manage_wage" data-id="' . $realtor['_id'] . '" data-status="' . $realtor['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, '$' . $min_amount, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
}
