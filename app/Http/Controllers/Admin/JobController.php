<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\JobHourRatingRequest;
use App\Http\Requests\JobsRequest;
use App\Http\Controllers\Controller;
use App\Model\JobsModel;
use App\Model\ApplicantShift;
use App\Model\CategoryModel;
use App\Model\JobsapplicationModel;
use App\Model\User;
use App\Model\UserRatings;
use Carbon\Carbon;
use App\Http\Repositary\CommonRepositary;
use App\Http\Repositary\EmployerRepositary;
use Crypt;

class JobController extends Controller
{
    public function __construct(EmployerRepositary $employer)
    {
        $this->employer = $employer;
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*===========================
        Listing of All Jobs
    =============================*/
    public function index()
    {
        $allJobs = JobsModel::get()->toArray();
        $categories = JobsModel::pluck('job_category')->toArray();
        array_unique($categories);
        $user_id = JobsModel::pluck('user_id')->toArray();
        // 'approved'=> 1,
        $employers = User::whereIn('_id', $user_id)->where(array('is_deleted' => false))->get()->toArray();
        $selected_category = CategoryModel::whereIn('_id', $categories)->where(array('is_deleted' => false))->get()->toArray();
        return view('managejobs/listingjobs', ['allJobs' => $allJobs, 'active' => 'allJobs', 'categories' => $selected_category, 'employers' => $employers]);
    }

    /*==================
        View Job Detail
    ====================*/
    public function viewJobDetail($id)
    {
        $job_post = JobsModel::where(array('_id' => $id))->with(['jobapplied' => function ($q) {
            $q->where('is_deleted', false);
        }, 'jobshifts'])->first();
        $job_data = $job_post->toArray();
        $employer = User::where(array('_id' => $job_data['user_id'], 'role' => 3))->first()->toArray();
        $jobapplication = JobsapplicationModel::where(array('job_id' => $id))->get()->toArray();
        //selected jobseekers
        $accepted_users = $user_ids = [];
        $date = Carbon::now();
		//add one hour to current time
        $current_date = $date->addHours(1)->parse($date)->format('Y-m-d H:i:s');
		//check if job can be edited or not == shivani (29 august 2017)
        $job_edit = $this->employer->check_job($job_post, $current_date);
        $commission = $this->employer->get_commision($job_post->category[0]['_id'], $job_post->user_id);
        if ($job_data['job_published_type'] == 2) {
			//$jobPost_arr = $job_post->toArray();				
            if (!empty($job_data['jobapplied'])) {
                foreach ($job_data['jobapplied'] as $jobseeker) {
                    if ($jobseeker['job_status'] == 1) {
                        $accepted_users[] = $jobseeker['jobseeker_id'];
                    } else {
                        $user_ids[] = $jobseeker['jobseeker_id'];
                    }
                }
            }
        }
		//rehire candidate list		
        $rehire_user = $this->employer->getRehireList($accepted_users, $job_data['user_id']);
        $users = $rehire_user['users'];
        $admin = true;
        return view('managejobs/viewJobDetail', ['job_data' => $job_data, 'accepted_users' => $accepted_users, 'users' => $users, 'jobseekers' => $jobapplication, 'employer' => $employer, 'user_ids' => $user_ids, 'admin' => $admin, 'job_edit' => $job_edit, 'commission' => $commission]);
    }

    /*================================
        Feltering Data in ajax request
    ===================================*/
    public function filterJobs(Request $request)
    {
        $basearray = JobsModel::whereIn('is_deleted', [true, false]);
        $totalusercount = JobsModel::whereIn('is_deleted', [true, false])->count();

        if (isset($request->category)) {
            $basearray->where('job_category', $request->category);
        }

        if (isset($request->emp_name)) {
            $basearray->where('user_id', $request->emp_name);
        }

        if (isset($request->title) && !empty($request->title)) {
            $basearray->where('title', 'LIKE', $request->title . '%');
        }

        if (isset($request->jobstatus)) {
            $basearray->where('job_status', intval($request->jobstatus));
        }

        if (isset($request->requested_at_from) || isset($request->requested_at_to)) {
            $start = Carbon::parse($request->requested_at_from);
            $start2 = Carbon::parse($request->requested_at_to);
            $start_day_start = $start->startOfDay();
            $start_day_end = $start2->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start)->where('created_at', '<=', $start_day_end);
        }
        /** If Both set **/
        if (isset($request->requested_at_from) && isset($request->requested_at_to)) {
            $start_both = Carbon::parse($request->requested_at_from);
            $end_both = Carbon::parse($request->requested_at_to);
            $start_day_start_both = $start_both->startOfDay();
            $start_day_end_both = $end_both->endOfDay();
            $basearray->where('created_at', '>=', $start_day_start_both)->where('created_at', '<=', $start_day_end_both);
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }

        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('job_id', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('job_id', 'desc');
        } else {
            $basearray->orderBy('updated_at', 'desc');
        }
        $counttotal = JobsModel::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $jobs) {
            $jobId = $jobs['_id'];
            $view_link = '<a href="view-job-detail/' . $jobId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="View Job" class="icon-pencil" aria-hidden="true"></span></a>';
            //$view_link  = '<a href="#" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>';

            if (isset($jobs['title']) && !empty($jobs['title'])) {
                $title = '<a href="view-job-detail/' . $jobId . '">' . $jobs['title'] . '</a>';
            }

            $job_category = $jobs['category'][0]['name'];

            /*if( isset( $jobs['user_id'] ) ) {
                $first_name = User::where(array( '_id' => $jobs['user_id'] ))->value('first_name');
                $last_name = User::where(array( '_id' => $jobs['user_id'] ))->value('last_name');
                $user_name = ucwords( $first_name ." ". $last_name );
            }*/

            if (isset($jobs['job_id'])) {
                $job_id = $jobs['job_id'];
            } else {
                $job_id = 'N/A';

            }
            $company_name = 'N/A';
            if (isset($jobs['user_id'])) {
                $company_name = User::where(array('_id' => $jobs['user_id']))->value('company_name');
            }

            if (isset($jobs['address']) && !empty($jobs['address'])) {
                $address = $jobs['address'];
            }

            if (isset($jobs['start_date'])) {
                //$created_at = Carbon::parse( $jobs['start_date'] )->format('M').'-'.Carbon::parse( $jobs['start_date'] )->format('d').'-'.Carbon::parse( $jobs['start_date'] )->format('Y');
                $created_at = Carbon::parse($jobs['updated_at'])->format('M') . '-' . Carbon::parse($jobs['updated_at'])->format('d') . '-' . Carbon::parse($jobs['updated_at'])->format('Y');
            }

            if (isset($jobs['start_date'])) {
                $duration = Carbon::parse($jobs['start_date'])->format('h:i A') . '-' . Carbon::parse($jobs['end_date'])->format('h:i A');
            }

            if ($jobs['is_deleted'] == true) {
                $job_status = 'Deleted';
            } else {
                if ($jobs['job_status'] == 1) {
                    $job_status = 'Active';
                } elseif ($jobs['job_status'] == 2) {
                    $job_status = 'Filled';
                } elseif ($jobs['job_status'] == 3) {
                    $job_status = 'Progress';
                } elseif ($jobs['job_status'] == 4) {
                    $job_status = 'Completed';
                }
            }

            $GLOBALS['data'][] = array($i, $job_id, $title, $job_category, $company_name, $address, $created_at, $duration, $job_status, $view_link);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $totalusercount;
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }

    function check_difference_in_hours($startTime)
    {
        $currentTime = Carbon::now();
        $start_time = Carbon::parse($startTime);
		//return $start_time->diff($currentTime)->format('%H');
        return $start_time->diffInHours($currentTime);
		//return $start_time->diff($currentTime)->format('%H');
    }
	
	/*
     * Added : 8 dec 2017, shivani debut infotech
     * DESC : to return action buttons html on behalf of job status
     * */
    function get_action_buttons($job_status, $job_type, $applicationId, $jobseeker_id, $job_id, $start_date, $app_shift, $worker_req, $hired)
    {

        $actiobButton = '';
        $applicationId = Crypt::encrypt($applicationId);
        if (($job_type == 1 && $job_status == 1) || ($job_type == 1 && $job_status == 2) || ($job_type == 2 && $job_status == 2) || ($job_type == 2 && $job_status == 1)) {
			// --------- hired ----------- 
            $actiobButton .= '<a href="javascript:void(0);" class="hired" title="hired"><i class="fa fa-check color_green" aria-hidden="true"></i></a>&nbsp';
			// --------- cancel employee ----------- -->
            $actiobButton .= '<a href="javascript:void(0);" class="cancel_employee rem_emp" data-apply_value="' . $applicationId . '" title="cancel employee" data-confirm-message="Are you sure, you want to cancel this employee?"><i class="fa fa-trash color_red" aria-hidden="true"></i></a>';
        } elseif ($job_type == 2 && $job_status == 0) {
            $actiobButton .= 'N/A';
        } elseif ($job_type == 0) {
            if ($job_status == 0) {
                $timeLeft = $this->check_difference_in_hours($start_date);				
				//----- if max workers are already hired ------ -->
                if ($worker_req == $hired) {
					// ----- accept candidate button (Disabled)  ----- -->
                    $actiobButton .= '<a href="javascript:void(0);" data-apply_value="' . $applicationId . '" class="accept_max_hired" data-type="accepted" data-confirm-message="Sorry, your job vacancies are full">
						<i class="fa fa-thumbs-up color_green" aria-hidden="true"></i></a>';
					// ----- decline candidate button  (Disabled) ----- -->
                    $actiobButton .= '<a href="javascript:void(0);" data-apply_value="' . $applicationId . '" class="declined_max_hired" data-type="rejected" data-confirm-message="Sorry, your requirement for maximum workers has already been fulfilled"><i class="fa fa-thumbs-down color_red" aria-hidden="true"></i></a>';
                } else {
					// ----- accept candidate button ----- -->
                    $actiobButton .= '<a href="javascript:void(0);" data-apply_value="' . $applicationId . '" class="accept" data-type="accepted" data-confirm-message="Are you sure you want to accept the candidate?"><i class="fa fa-thumbs-up color_green" aria-hidden="true"></i></a>';
					// ----- decline candidate button ----- -->
                    $actiobButton .= '<a href="javascript:void(0);" data-apply_value="' . $applicationId . '" class="declined" data-type="rejected" data-confirm-message="Are you sure you want to decline the candidate?"><i class="fa fa-thumbs-down color_red" aria-hidden="true"></i></a>';
                }
            } elseif ($job_status == 1) {
				// --------- hired ----------- -->
                $actiobButton .= '<a href="javascript:void(0);" class="hired" title="hired"><i class="fa fa-check color_green" aria-hidden="true"></i></a>&nbsp;';
				// --------- cancel employee ----------- -->
                $actiobButton .= '<a href="javascript:void(0);" class="cancel_employee rem_emp" data-apply_value="' . $applicationId . '" title="cancel employee" data-confirm-message="Are you sure, you want to cancel this employee?"><i class="fa fa-trash color_red" aria-hidden="true"></i></a>';
            } else {
				//
                if ($job_status == 5 || $job_status == 7) {
                    $actiobButton .= '<a href="javascript:void(0);" class="rem_emp" title="Cancelled/Removed From the job"><i class="fa fa-times color_red" aria-hidden="true"></i></a>';
                }

            }
        }	
		
		// ------ update hours button, if any of the shift has been completed -->
        $actiobButtonTimesheet = '';
        if ($app_shift) {
            $displayShift = false;
            foreach ($app_shift as $shft) {
                if (\Carbon\Carbon::parse($shft['end_date']) < \Carbon\Carbon::now()) {
                    $displayShift = true;
                }
            }

            $actiobButtonTimesheet = '<a href="javascript:void(0);" title="view/update employee work hours" class="check_shift_details" data-jobseeker="' . $jobseeker_id . '" data-job="' . $job_id . '"><i class="fa fa-eye color_blue" aria-hidden="true"></i></a>';

        }

        return [$actiobButton, $actiobButtonTimesheet];
    }
	
	
	/*
     * Updated : 8 dec 2017, shivani debut infotech
     * DESC : to list employee status, cv url, and display view shift, delete employee, accept decline buttons
     * */
    public function filterJobDetail(Request $request, $id)
    {
		//
        $basearray = JobsapplicationModel::where(array('job_id' => $id))->with(['job' => function ($q) {
            $q->select('start_date', 'total_hired', 'number_of_worker_required', 'job_published_type');
        }, 'app_shift'])->whereNotIn('job_status', [6]);
        $totalcount = JobsapplicationModel::where(array('job_id' => $id))->count();

        if (isset($request->requested_at_from)) {
            $startday = Carbon::parse($request->requested_at_from)->startOfDay();
            $endday = Carbon::parse($request->requested_at_from)->endOfDay();
            $basearray = JobsapplicationModel::where('job_id', '=', $id)->with(['job_shift' => function ($q) use ($id, $startday, $endday) {
                $q->where('job_id', '=', $id)->where('start_date', '>=', $startday)->where('end_date', '<=', $endday)->get();
            }]);
        }

        $counttotal = JobsapplicationModel::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
       // $GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $key => $jobs) {
            $jobseeker_id = $jobs['_id'];
            $view_link = 'N/A';
            $job_type = $jobs['job']['job_published_type'];
            $start_date = $jobs['job']['start_date'];
            $app_shift = $jobs['app_shift'];
            $view_link = $this->get_action_buttons($jobs['job_status'], $job_type, $jobs['_id'], $jobs['jobseeker_id'], $jobs['job_id'], $start_date, $app_shift, $jobs['job']['number_of_worker_required'], $jobs['job']['total_hired']);
            
			//~ if($jobs['job_status'] == 4)	{
				//~ $view_link  = '<a href="jobseeker-hours-rating/'.$jobseeker_id.'" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" class="icon-pencil" aria-hidden="true"></span></a>';
			//~ }

            if (isset($jobs['jobseeker_id']) && !empty($jobs['jobseeker_id'])) {

                $first_name = User::where(array('_id' => $jobs['jobseeker_id'], 'role' => 2))->value('first_name');
                $last_name = User::where(array('_id' => $jobs['jobseeker_id'], 'role' => 2))->value('last_name');
                $name = $first_name . " " . $last_name;

            }

            if (isset($jobs['jobseeker_id']) && !empty($jobs['jobseeker_id'])) {
                $user_image = User::where(array('_id' => $jobs['jobseeker_id'], 'role' => 2))->value('image');
                if (isset($user_image) && !empty($jobs['jobseeker_id'])) {
                    $image = '<img src="' . $user_image . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                } else {
                    $image = '<img src="' . asset("public/employer/images/profile_avatar.png") . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                }
            } else {
                $image = '<img src="' . asset("public/employer/images/profile_avatar.png") . '" width="50" height="50" class="img-circle" alt="User Image"/>';
            }
			
			
			//updated : 21 sep 2017, shivani
            if (isset($jobs['rating']) && $jobs['rating'] = true) {
                $jobRating = UserRatings::where(['type' => 'job_rating', 'type_id' => $jobs['jobseeker_id'], 'job_id' => $id])->value('rating');
                if (!empty($jobRating)) {
                    $rating = CommonRepositary::user_rating($jobRating, $jobs['_id']);
                } else {
                    $rating = 'N/A';
                }//$jobs['rating'];
            } else {
                $rating = 'N/A';
            }

            if (isset($jobs['jobseeker_id']) && !empty($jobs['jobseeker_id'])) {
                $country_code = User::where(array('_id' => $jobs['jobseeker_id'], 'role' => 2))->value('country_code');
                $phone = $country_code . ' ' . User::where(array('_id' => $jobs['jobseeker_id'], 'role' => 2))->value('phone');
            }

            if (isset($jobs['total_hours_worked']) && !empty($jobs['total_hours_worked'])) {
                $working_hours = $jobs['total_hours_worked'];
            } else {
                $working_hours = 0;
            }
            
            //certifications
            $additional_documents = User::where(array('_id' => $jobs['jobseeker_id'], 'role' => 2))->value('additional_documents');
            $certification = 'N/A';
            if (!empty($additional_documents)) {
                $certification = '<a href="javascript:void(0);" title="Check Certifications" class="check_jobseeker_docs" data-id="' . $jobs['jobseeker_id'] . '"><i class="fa fa-eye color_blue" aria-hidden="true"></i> </a>';
            }

            $resume = User::where(array('_id' => $jobs['jobseeker_id'], 'role' => 2))->value('cv_url');
            $dwnld_resume = 'N/A';
            if (!empty($resume)) {
                $dwnld_resume = '<a href="' . $resume . '" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>';
            }


            $GLOBALS['data'][] = array($i, $image, $name, $certification, $dwnld_resume, $phone, $working_hours, $rating, $view_link[0], $view_link[1]);
            $i++;
        }

        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $totalcount;
        $result['recordsFiltered'] = $GLOBALS['total'];
        return json_encode($result);
    }

    /*========================================
        Load View For Editing Hours And Rating
    ===========================================*/
    public function viewJobseekerHoursRating($id)
    {
        $jobseekers = JobsapplicationModel::where(array('_id' => $id))->first()->toArray();
        $jobRating = 0;
        if (isset($jobseekers['rating']) && $jobseekers['rating'] == true) {
            $jobRating = UserRatings::where(['type' => 'job_rating', 'type_id' => $jobseekers['jobseeker_id'], 'job_id' => $jobseekers['job_id']])->value('rating');
        }
        return view('managejobs/editHourRating', ['jobseekers' => $jobseekers, 'jobRating' => $jobRating]);
    }

    /*========================
        Edit Hours And Rating
     * updated : 22 sep 2017, shivani. for job hours and rating
    ==========================*/
    public function editHourRating(Request $request, CommonRepositary $common)
    {
        if ($jobseekers = JobsapplicationModel::where(array('_id' => $request['jobappid']))->first()) {
			//update rating
            $user_id = \Crypt::encrypt($jobseekers->jobseeker_id);
            $job_id = \Crypt::encrypt($jobseekers->job_id);
            $ratvalue = $common->AdjustRating($request['rating'], $user_id, $job_id);
            if ($ratvalue) {
                flash()->success('Rating updated successfully');
                return response()->json(['success' => true]);
            } else {
                flash()->error('Sorry, user rating can not be updated');
                return response()->json(['success' => false]);
            }
        } else {
            flash()->error('Sorry, employee application not found');
            return response()->json(['success' => false]);
        }
    }


    public function addhours()
    {
        $jobs = JobsapplicationModel::with('job_shift')->get()->toArray();
        echo "<pre>";
        $ids = array();
        $shifts = array();
        //print_r( $jobs);
        //echo count($jobs['job_shift']);
        //die('here');
        foreach ($jobs as $job) {
            array_push($ids, $job['job_id']);
            foreach ($job['job_shift'] as $jobshitf) {
                array_push($shifts, $jobshitf['shift_time'] / 60);
            }
            //echo $shift_time;
            /*die('here');
            $workMinutes += $shift['shift_time'];
            if($workMinutes > 0){
                $workHours += intval($workMinutes/60);
            }*/
            /*//update total hours worked for each job application
            if(JobsapplicationModel::where('_id',$job_applctn['_id'])->update(['total_hours_worked'=>$workHours])){
                $workHours = $workMinutes = 0;
            }*/
        }
        print_r($ids) . "<br>";
        print_r(array_unique($shifts)) . "<br>";
          //echo $shift_time;
        die('here');
        //if jobshifts found, then calculate hours worked upto now

    }
    
    /*
     * Added : 7 dec 2017, shivani Debut infotech
     * DESC : to return categories on behalf of employer selected 
     * */
    public function getEmployerCategories(Request $request)
    {
        $industIds = User::where(array('status' => true, '_id' => $request->employer_id))->select('industry')->first();
        $inIds = [];
        if ($industIds) {
            foreach ($industIds->industry as $k => $indust) {
                $inIds[] = $indust['_id'];
            }
        }
        $category = CategoryModel::where(array('status' => true, 'type' => 'category', 'is_deleted' => false))->whereIn('industry_id', $inIds)->select('name', '_id')->get()->toArray();
        return response()->json(['success' => true, 'category_data' => $category]);
    }
    
    /*
     * Added : 7 dec 2017, shivani Debut infotech
     * DESC : to return sub-categories on behalf of category selected 
     * */
    public function Subcategory(Request $request)
    {
        $cat_id = $request->value;
        $subcategory = CategoryModel::where(array('status' => true, 'type' => 'subcategory', 'category_id' => $cat_id))->select('name', '_id')->get()->toArray();
        $commission = $this->employer->get_commision($cat_id, $request->employer_id);//CategoryModel::where(array('_id'=>$cat_id))->value('commision');
        return response()->json(['sub_cat' => $subcategory, 'commision' => $commission['commision']]);
    }
	
	/*
     * Added : 7 dec 2017, shivani Debut infotech
     * DESC : to return skills on behalf of subcategory selected 
     * */
    public function Skills(Request $request)
    {
        $subcat_id = $request->value;
        $skilldata = $this->employer->get_skills($subcat_id);
        $skills = CategoryModel::where(array('status' => true, '_id' => $subcat_id, 'type' => 'subcategory'))->select('mandatory_skills')->get()->toArray();
        $mId = [];
        foreach ($skills as $k => $value) {
            if (isset($value['mandatory_skills']) && !empty($value['mandatory_skills'])) {
                $mId = empty($value['mandatory_skills']) ? [] : $value['mandatory_skills'];
            }
        }
        return response()->json(['skills' => $skilldata, 'mandatory' => $mId]);
    }
    
    
    /*
     * Added : 6 dec 2017, shivani Debut infotech
     * DESC : to render post/repost a new job view
     * */
    public function getCreateJob($job_id = null)
    {
        $employers = User::where(array('is_deleted' => false, 'role' => 3, ))->WhereIn('approved', [1])->orderBy('created_at', 'desc')->pluck('email', '_id')->toArray();
        $viewData = $this->employer->create_post_data($job_id);        
       // $viewData['category'] = [];
        $viewData['active'] = 'jobs';
        $viewData['title'] = 'jobs';
        $viewData['employers'] = $employers;
		//render view for a new job to be posted
        return view('managejobs.createjob', $viewData);
    }
    
    /*
     * Added : 6 dec 2017, shivani Debut infotech
     * DESC : to create/repost a new job on behalf of employer
     * */
    public function postNewJob(JobsRequest $request)
    {
        $saveJob = $this->employer->save_job($request);
        if ($saveJob['success'] == true) {
            $saveJob['url'] = url('admin/list-jobs');
            return response()->json($saveJob, 200);
        } else {
            return response()->json($saveJob, 422);
        }
    }
	
	/*
     * Added on : 7 dec 2017, shivani Debut infotech
     * DESC : to save job shift dates to a temporary table
     * */
    public function save_shift_time(Request $request)
    {
        $shiftTime = $request->all();
        $saveTempData = $this->employer->save_temp_process($shiftTime);
        return response()->json($saveTempData);
    }
	
	/*
     * Added on : 7 Dec 2017, shivani Debut infotech
     * DESC : to remove, update shift timings for a job (from temporary values only)
     * */
    public function remove_shift_time(Request $request)
    {
        $removeData = $this->employer->remove_temp_process_data($request);
        return response()->json($removeData);
    }
	
	/*
     * Added : 23 nov 2017, shivani Debut infotech
     * DESC : to get details on behalf of process id
     * */
    public function process_details(Request $request)
    {
        $process_details = $this->employer->get_temp_process_details($request);
        return response()->json($process_details);
    }
	
	/*
     * Added : 7 dec 2017, shivani, Debut infotech
     * DESC : to render edit job view
     * */
    public function get_edit_job($id)
    {
		//$id = \Crypt::decrypt($id); 
        $employers = User::where(array('is_deleted' => false, 'role' => 3, ))->WhereIn('approved', [1])->orderBy('created_at', 'desc')->pluck('email', '_id')->toArray();
        $editJobData = $this->employer->get_edit_job_data($id);
        if ($editJobData['success'] == true) {
            $editJobData['employers'] = $employers;
            return view('managejobs.editjob', $editJobData);
        } else {
			//redirect back to job listing page, with error message.
			//flash()->error('Sorry, something went wrong');
            return redirect('admin/view-job-detail/' . $id);
        }
    }
	
	/*
     * Added : 8 dec 2017, shivani debut infotech
     * DESC : to update job
     * */
    public function post_update_job(JobsRequest $request, $id)
    {
        $id = \Crypt::decrypt($id);
        $updateJob = $this->employer->update_job($id, $request);
        if ($updateJob['success'] == false) {
			//return error response 
            return response()->json($updateJob, 422);
        } else {
            $updateJob['url'] = url('admin/view-job-detail/' . $id);
            return response()->json($updateJob, 200);
        }
    }
	
	/*
     * Added : 8 dec 2017, shivani debut infotech
     * DESC : to delete job
     * */
    public function postDeletejob(Request $request)
    {
        $jobId = $request->value;
        $deleteJob = $this->employer->delete_job($jobId);
        $deleteJob['url'] = url('admin/list-jobs');
        return response()->json($deleteJob);
    }
    
    /*
     * Added : 8 dec 2017, shivani debut infotech
     * DESC : to cancel an employee from a job
     * */
    public function getCancelEmployee(Request $request)
    {
        $jobApp_id = \Crypt::decrypt($request->cancel_employee_id);
        $cancelEmployee = $this->employer->cancel_employee($jobApp_id);
        return response()->json($cancelEmployee);
    }
	
	/*
     * Added : 8 dec 2017, shivani debut infotech
     * DESC : to accept/decline candidate for a job
     * */
    public function postAcceptjob(Request $request)
    {
        $acceptDeclineEmployee = $this->employer->accept_decline_candidate($request);
        return response()->json($acceptDeclineEmployee, 200);
    }
    
    /*
     * Added : 11 dec 2017, shivani debut infotech
     * DESC : to return shift details for an employee
     * */
    public function employeeShifts(Request $request)
    {
        $employeeShift = $this->employer->employee_shift_details($request);
        return response()->json($employeeShift);
    }
	
	/*
     * Added : 13 dec 2017, shivani Debut infotech
     * DESC : to update employee work hours for a particular shift.
     * */
    public function update_work_hours(Request $request)
    {
        $updateHours = $this->employer->update_work_hours($request);
        $createHours = $this->employer->create_work_hours($request);


        if ($createHours['success'] === 0 || $updateHours['success'] === 0) {
            if ($createHours['success'] == 0) {
                return response()->json($createHours);
            } else {
                if ($updateHours['success'] == 0) {
                    return response()->json($updateHours);
                }
            }
        }

        flash()->success('Timesheet updated successfully');
        return response()->json($updateHours);
    }
	
	/*
     * Added : 14 dec 2017, shivani Debut infotech
     * DESC : to send rehire invitation to the jobseekers.
     * */
    public function getRehireInvite(Request $request)
    {
        $inviteJobseekers = $this->employer->rehire_invitation($request);
        return response()->json($inviteJobseekers);
    }
	
	/*
     * Added : 14 dec 2017, shivani debut infotech
     * DESC : to return users to be shown in rehire user modal.
     * */
    public function user_for_rehire(Request $request)
    {
        if (isset($request->job_id)) {
			//get job skills 
            $jobskills = JobsModel::where('_id', $request->job_id)->first();
            $getUsers = $this->employer->get_rehire_users($jobskills->user_id, true, $request, true);
            return response()->json($getUsers);
        } else {
            $getUsers = $this->employer->get_rehire_users($request->employer_id, true, $request, true);
            return response()->json($getUsers);
        }

    }
	
	/*
     * Added on : 18 dec 2017
     * DESC  :to return certificates/degree details of jobseeker
     * */
    public function getCandidateDocuments(Request $request)
    {
        $user_id = $request->user_id;
        $candidate_details = User::where("_id", $user_id)->first();
        return view('employer.promo.jobseeker_certificates', compact('candidate_details'));
    }
	
	/*
     * Added on : 2 jan 2018, shivani Debut infotech
     * DESC : to save job shift dates to a temporary table
     * */
    public function same_job_time(Request $request)
    {
		//$shiftTime = $request->all();		
        $requestedDates = explode(',', $request->check_date);
        $shiftTime = [];
        $shiftTime['lunch_hour'] = $request->lunch_hour;
        foreach ($requestedDates as $dte) {
            $shiftTime['start']['time_' . $dte] = $request->start_time;
            $shiftTime['end']['time_' . $dte] = $request->end_time;
        }
        if (!empty($request->process_id)) {
            TempJobs::where('process_id', $request->process_id)->delete();
        }
        $saveTempData = $this->employer->save_temp_process($shiftTime);
        return response()->json($saveTempData);
    }
	
	/*
     * Added on : 2 jan 2018, shivani Debut infotech
     * DESC : to update job shift dates to a temporary table
     * */
    public function edit_same_job_time(Request $request)
    {
        $saveTempData = $this->employer->edit_same_job_time($request);
        return response()->json($saveTempData);
    }

    /*
    delete job
     */
    public function deleteJob($id)
    {
        $this->employer->deleteShiftModel($id);
        return response()->json(true);
    }

}
