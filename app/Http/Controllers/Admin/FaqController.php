<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FaqRequest;
use App\Model\Faqs;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*===========================================
            Display a listing of skills.
    =============================================
     */
    public function index()
    {
        $faqs = Faqs::get();
        return view('faq/listingFaqs', ['faqs' => $faqs, 'active' => 'faq']);
    }

    /*=========================
        Creating Skill
    ===========================
     */
    public function create()
    {
        return view('faq/createFaq');
    }

    /*================================================================
        Store Skill in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     =================================================================
     */
    public function store(FaqRequest $request)
    {
        $data = $request->except('_token');
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['is_deleted'] = false;
        $faqobj = new Faqs($data);
        if ($faqobj->save()) {
            flash()->success('FAQ added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }


    /*======================================================
     * Show the form for editing the specified faq.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================
     */
    public function edit(FaqRequest $request, $id)
    {
        $faq = Faqs::where(array('_id' => $id))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($faq->update($data)) {
            flash()->success('FAQ updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }

    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================
     */
    public function update(Request $request, $id)
    {
        $edit_faq = Faqs::where(array('_id' => $id))->first();
        return view('faq/editFaq', ['edit_faq' => $edit_faq]);
    }

    /*================================================
     * Remove the specified skill from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function destroy($id)
    {
        $faq = Faqs::where('_id', $id)->first();
        $faq->update(['is_deleted' => true]);
        if ($faq) {
            flash()->success('FAQ deleted successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*====================================
        Feltering Data in ajax request
    ======================================
     */
    public function filterFaqs(Request $request)
    {
        $basearray = Faqs::where(array('is_deleted' => false));
        $totalusercount = Faqs::where(array('is_deleted' => false))->count();

        if (isset($request->faqs) && !empty($request->faqs)) {
            $basearray->where('faqs', 'LIKE', '%' . $request->faqs . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        $basearray->orderBy('updated_at', 'DESC');

        $counttotal = Faqs::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $faq) {
            $faqId = $faq['_id'];
            $view_link = '<a faqId="' . $faqId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $faq['status'] . '" href="edit-faq/' . $faqId . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deletefaq" data-id=' . $faqId . ' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>';
            if (isset($faq['faqs']) && !empty($faq['faqs'])) {
                $faqs = $faq->faqs;
            }

            if (isset($faq->answer) && !empty($faq->answer)) {
                $length = strlen($faq->answer);
                if ($length > 100) {
                    $answer = substr($faq->answer, 0, 50) . '...';
                } else {
                    $answer = substr($faq->answer, 0, 50);
                }
            }
            if ($faq['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="faqs" data-id="' . $faq['_id'] . '" data-status="' . $faq['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="faqs" data-id="' . $faq['_id'] . '" data-status="' . $faq['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $faqs, $answer, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
   
   /*=====================================
        View Particular Skill Detail 
    ======================================
     */
    public function viewFaq(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = Faqs::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
}
