<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\IndustryRequest;
use App\Http\Controllers\Controller;
use App\Model\Industry;


class IndustryController extends Controller
{
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*========================
        Listing of industries
    =========================*/
    public function index()
    {
        $industries = Industry::get();
        return view('industry/listingIndustries', ['industries' => $industries, 'active' => 'industries']);
    }

    /*=================================
        Load Creating Cateogary View
    ===================================*/
    public function create()
    {
        return view('industry/createIndustry');
    }

    /*================================================================
        Store Category in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
    =================================================================*/
    public function store(IndustryRequest $request)
    {
        $img = $request->input('file_name');
        if ($img != '') {
            $data['image'] = $request->input('file_name');
        } else {
            $data['image'] = null;
        }
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['name'] = strtolower($request->input('name'));
        $data['description'] = strip_tags($request->input('description'), '<b><i><u><ul><ol><a>');
        
        //$data['file_name'] = $request->input( 'file_name' );
        $data['is_deleted'] = false;
        $indobj = new Industry($data);
        if ($indobj->save()) {
            flash()->success('Industry added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function update($id)
    {
        $edit_industry = Industry::where(array('_id' => $id))->first();
        return view('industry/editIndustry', ['edit_industry' => $edit_industry]);
    }

    
    /*======================================================
     * Show the form for editing the specified Category.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function edit(IndustryRequest $request, $id)
    {
        $indObj = Industry::where(array('_id' => $id))->first();
        $img = $request->input('file_name');
        if ($img != '') {
            $data['image'] = $request->input('file_name');
        } else {
            $data['image'] = null;
            $data['file_name'] = null;
        }

        $data['name'] = strtolower($request->input('name'));
        $data['description'] = strip_tags($request->input('description'), '<b><i><u><ul><ol><a>');
        $data['is_deleted'] = false;
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($indObj->update($data)) {
            flash()->success('Industry updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }

    
    /*================================================
     * Remove the specified industry from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================*/
    public function destroy($id)
    {
        $indObj = Industry::where(array('_id', $id))->first();
        $indObj->update(['is_deleted' => true]);
        if ($indObj) {
            flash()->success('Industry deleted successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }
    
     /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterIndustries(Request $request)
    {
        $basearray = Industry::where(array('is_deleted' => false));
        $totalusercount = Industry::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = Industry::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $industry) {
            $industry_id = $industry['_id'];
            $view_link = '<a indusd_id="' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $industry['status'] . '" href="edit-industry-view/' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default locked"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>';
            if (isset($industry['name']) && !empty($industry['name'])) {
                $name = $industry['name'];
            }

            if (isset($industry['image']) && !empty($industry['image'])) {
                if (file_exists('uploads/' . $industry['image'])) {
                    $image = '<img src="' . asset('uploads/' . $industry['image']) . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                } else {
                    $image = '<img src="' . $industry['image'] . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                }

            } else {
                if (isset($industry['file_name']) && !empty($industry['file_name'])) {
                    $image = '<img src="' . $industry['file_name'] . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                } else {
                    $image = 'N/A';
                }

            }

            if (isset($industry->description) && !empty($industry->description)) {
                $length = strlen($industry->description);
                if ($length > 100) {
					//
                    $description = substr(strip_tags($industry->description), 0, 15) . '...';
                } else {
                    $description = substr(strip_tags($industry->description), 0, 15);
                }
            } else {
                $description = 'N/A';
            }

            if ($industry['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="industry" data-id="' . $industry['_id'] . '" data-status="' . $industry['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="industry" data-id="' . $industry['_id'] . '" data-status="' . $industry['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $name, $description, $image, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
   
   /*=====================================
        View Particular Skill Detail 
    ======================================*/
    public function viewIndustry(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = Industry::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }

}
