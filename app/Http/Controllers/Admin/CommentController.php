<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CommentRequest;
use App\Model\BlogCategory;
use App\Model\Blog;
use App\Model\Comment;
use App\Model\Reply;
use Carbon\Carbon;
use auth;


class CommentController extends Controller
{

  
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }


    /*============================
        Display a listing of comments.
    =============================*/
    public function index($id){

        $comments = Comment::where('blog_id',$id)->get();

        return view('blogs/admin/listingcomment', ['comments' => $comments, 'active' => 'comments']);
  
    }



  

    /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterComments(Request $request){

        $id = request()->segment(4);
        $basearray = Comment::where('blog_id',$id)->where('is_deleted',false)->orderBy('created_at','DESC');
        $totalcount = Comment::where(array('blog_id'=>$id,'is_deleted' => false))->count();


        $length = intval($request->get('length'));
        $length = $length < 0 ? $totalcount : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $val) {
            $commentId = $val['_id'];
            $view_link ='';
            $view_link .= '<a id="delete-comment" data-id="' . $commentId . '" class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete Comment" class="icon-trash" aria-hidden="true"></span></a>
  
            ';
           
           $replyCount = Reply::where('comment_id',$commentId)->count();
           if($replyCount <5){
            $view_link .='<a data-id="' . $commentId . '" class="btn btn-circle btn-icon-only btn-default reply" title="Reply"><i class="fa glyphicon glyphicon-envelope text-danger inactive"></i><a>';
            }

            $comment = $val['comment'];

            $date = Carbon::parse($val['created_at'])->format('dS') . ' ' . 
                    Carbon::parse($val['created_at'])->format('M') . ' ' . 
                    Carbon::parse($val['created_at'])->format('Y')
                     .','. Carbon::parse($val['created_at'])->format('g:i A');
            $userName =$val['first_name'].' '.$val['last_name'];

            $GLOBALS['data'][] = array($i, $userName, $comment, $date, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];
         

        return json_encode($result);
    }


    /*================================================
     * Remove the specified comment from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function destroy($id)
    {
        $comment = Comment::where('_id', $id)->first();

        if ($comment->update(array('is_deleted' => true))) {
         flash()->success(trans('Admin/message.delete_comment'));  
         return response()->json(['success' => true]);
        }else{
         flash()->success(trans('Admin/message.error'));
         return response()->json(['success' => false]);
        }

        
    }


     /*================================================
     * comment Reply.
     * @param  
     * @return \Illuminate\Http\Response
    ==================================================
     */
    public function commentReply(CommentRequest $request){
        
        $countCmt = Reply::where('comment_id',$request->comment_id)->count();
       
       if($countCmt < 5)
       {
        $reply =  new Reply();
        $reply->first_name =  Auth::user()->first_name;
        $reply->last_name=   Auth::user()->last_name;
        $reply->reply =  $request->reply;
        $reply->comment_id =  $request->comment_id;

        if ($reply->save()) {
    
            flash()->success(trans('Admin/message.add_reply'));  
            return response()->json(['success' => true]);
        }else{
         
            flash()->error(trans('Admin/message.error'));  
            return response()->json(['success' => false]);
        
        }
      }else{

        flash()->error(trans('Admin/message.reply_limit_exceeded'));  
        return response()->json(['success' => false]);
      }

    }



}
