<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\GeneralInfoRequest;
use App\Model\GeneralInfoModel;

class GeneralInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*=================================
        Display a listing of Documents.
    ==================================*/
    public function index()
    {
        $ginfo = GeneralInfoModel::get();
        return view('generalinfo/listingGeneralinfo', ['generalinfo' => $ginfo, 'active' => 'generalinfo']);
    }

    /*=====================
        Creating Document
    =======================*/
    public function create()
    {
        return view('generalinfo/createGeneralinfo');
    }

    /*================================================================
        Store Document in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
    ================================================================*/
    public function store(GeneralInfoRequest $request)
    {
        $data = $request->except('_token');
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['is_deleted'] = false;
        $ginfoobj = new GeneralInfoModel($data);
        if ($ginfoobj->save()) {
            flash()->success('App Document Added successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }


    /*======================================================
     * Show the form for editing the specified skill.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     ======================================================*/
    public function edit(GeneralInfoRequest $request, $id)
    {
        $ginfo = GeneralInfoModel::where(array('_id' => $id))->first();
        $data = $request->all();
        $data['status'] = $request->input('status') > 0 ? true : false;
        if ($ginfo->update($data)) {
            flash()->success('App Document Updated successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }

    }

    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     ================================================*/
    public function update(Request $request, $id)
    {
        $edit_ginfo = GeneralInfoModel::where(array('_id' => $id))->first();
        return view('generalinfo/editGeneralinfo', ['edit_ginfo' => $edit_ginfo]);
    }

    /*================================================
     * Remove the specified skill from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    =================================================*/
    public function destroy($id)
    {
        $ginfo = GeneralInfoModel::where('_id', $id)->first();
        $ginfo->update(['is_deleted' => true]);
        if ($ginfo) {
            flash()->success('App document deleted successfully');
            return response()->json(['success' => true]);
        } else {
            flash()->error('Something went wrong');
            return response()->json(['success' => false]);
        }
    }

    /*==================================
        Feltering Data in ajax request
    ====================================*/
    public function filterGeneralinfo(Request $request)
    {
        $basearray = GeneralInfoModel::where(array('is_deleted' => false));
        $totalusercount = GeneralInfoModel::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = GeneralInfoModel::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $ginfo) {
            $ginfoId = $ginfo['_id'];
            $view_link = '<a ginfoId="' . $ginfoId . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail" ><span class="icon-eye" style="color:blue;"></span></a><a data-status="' . $ginfo['status'] . '" href="edit-generalinfo/' . $ginfoId . '" class="btn btn-circle btn-icon-only btn-default"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a><a id="deleteginfo" data-id=' . $ginfoId . ' class="btn btn-circle btn-icon-only btn-default"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>';
            if (isset($ginfo['name']) && !empty($ginfo['name'])) {
                $name = $ginfo['name'];
            }

            if (isset($ginfo['general_file_name']) && !empty($ginfo['general_file_name'])) {
                $general_file_name = $ginfo['general_file_name'];
            }

            if ($ginfo['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="general_info" data-id="' . $ginfo['_id'] . '" data-status="' . $ginfo['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="general_info" data-id="' . $ginfo['_id'] . '" data-status="' . $ginfo['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $name, $general_file_name, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
   
   /*================================
        View Particular Skill Detail 
    ================================*/
    public function viewGeneralinfo(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = GeneralInfoModel::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }
}
