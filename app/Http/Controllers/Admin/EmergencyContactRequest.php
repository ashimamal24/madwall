<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmergencyContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('action') == 'edit') {


            $valid = array(
        
                'name1'=>'required|max:50',
                
            );
          
        } else {

            $valid = array(
                
                'name1'=>'required|max:50',

            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name1.required' => 'Please Enter name 1.',
            'name1.max' => 'Please enter title between 5-20 characters.',
        ];
    }
}
