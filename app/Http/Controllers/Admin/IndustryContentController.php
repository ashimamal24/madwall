<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\IndustryContentRequest;
use App\Http\Controllers\Controller;
use App\Model\IndustryContent;


class IndustryContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('trimdata');
        $this->middleware('admin');
        if (isset($_COOKIE['client_timezone'])) {
            date_default_timezone_set($_COOKIE['client_timezone']);
        }
    }

    /*================================
        Listing of industries content
    ==================================*/
    public function index()
    {
        $industries = IndustryContent::get();
        return view('industry-content/listingIndustriesContent', ['industries' => $industries]);
    }

    /*=================================
        Load Creating industry content View
    ===================================*/
    public function create()
    {
        return view('industry-content/createIndustryContent');
    }

    /*================================================================
        Store in database table
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
    =================================================================*/
    public function store(IndustryContentRequest $request)
    {
        $data['status'] = $request->input('status') > 0 ? true : false;
        $data['name'] = strtolower($request->input('name'));
        $data['description'] = $request->input('description');

        $img = $request->input('image_file_name');
        if ($img != '') {
            $data['image'] = $request->input('image_file_name');
        } else {
            $data['image'] = null;
        }
        
        $data['user_name'] = $request->input('user_name');
        $img = $request->input('user_file_name');
        if ($img != '') {
            $data['user_image'] = $request->input('user_file_name');
        } else {
            $data['user_image'] = null;
        }
      
        
        //$data['file_name'] = $request->input( 'file_name' );
        $data['is_deleted'] = false;
        $indobj = new IndustryContent($data);
        if ($indobj->save()) {
            flash()->success(trans('Admin/message.add_industry_content'));
            return response()->json(['success' => true]);
        } else {
            flash()->error((trans('Admin/message.error')));
            return response()->json(['success' => false]);
        }
    }

    /*================================================
     * Update the specified skill in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =================================================*/
    public function edit($id)
    {
        $edit_industry = IndustryContent::where(array('_id' => $id))->first();
        return view('industry-content/editIndustryContent', ['edit_industry' => $edit_industry]);
    }

    
    /*======================================================
     * Show the form for editing the specified Category.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     =======================================================*/
    public function update(IndustryContentRequest $request, $id)
    {
        $indObj = IndustryContent::where(array('_id' => $id))->first();

        $data['name'] = strtolower($request->input('name'));
        $data['description'] = $request->input('description');

        $img = $request->input('image_file_name');
        if ($img != '') {
            $data['image'] = $request->input('image_file_name');
        } else {
            $data['image'] = null;
        }
        
        $data['user_name'] = $request->input('user_name');
        $img = $request->input('user_file_name');
        if ($img != '') {
            $data['user_image'] = $request->input('user_file_name');
        } else {
            $data['user_image'] = null;
        }
        $data['is_deleted'] = false;
        $data['status'] = $request->input('status') > 0 ? true : false;
        
        if ($indObj->update($data)) {
            flash()->success(trans('Admin/message.update_industry_content'));
            return response()->json(['success' => true]);
        } else {
            flash()->error(trans('Admin/message.error'));
            return response()->json(['success' => false]);
        }

    }

    
    /*================================================
     * Remove the specified industry from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
    ==================================================*/
    public function destroy($id)
    {

        $industryContent = IndustryContent::where('_id', $id)->first();

        if ($industryContent->delete()) {
         flash()->success('Industries content deleted successfully');  
         return response()->json(['success' => true]);
        }else{
         flash()->success('Something went wrong');
         return response()->json(['success' => false]);
        }

    }
    
     /*====================================
        Feltering Data in ajax request
    ======================================*/
    public function filterIndustriesContent(Request $request)
    {
        $basearray = IndustryContent::where(array('is_deleted' => false));
        $totalusercount = IndustryContent::where(array('is_deleted' => false))->count();

        if (isset($request->name) && !empty($request->name)) {
            $basearray->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if (isset($request->status) && $request->status != '') {
            $basearray->where('status', (bool)$request->status);
        }
        $order = $request->get('order');
        if ($order[0]['column'] == 1 && $order[0]['dir'] == 'asc') {
            $basearray->orderBy('name', 'asc');
        } elseif ($order[0]['column'] == 1 && $order[0]['dir'] == 'desc') {
            $basearray->orderBy('name', 'desc');
        } else {
            $basearray->orderBy('_id', 'desc');
        }
        $counttotal = IndustryContent::get()->count();
        $length = intval($request->get('length'));
        $length = $length < 0 ? $counttotal : $length;
        $GLOBALS['total'] = $basearray->count();
        $resultset = $basearray->skip(intval($request->get('start')))->take($length)->get();
        $i = intval($request->get('start')) + 1;
        $GLOBALS['data'] = array();
        //$GLOBALS['total'] = count( $resultset );

        foreach ($resultset as $industry) {
            $industry_id = $industry['_id'];
            $view_link = '<a indusd_id="' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default" name="view" id="view" title="View Detail"><span class="icon-eye" style="color:blue;"></span></a>
               <a data-status="' . $industry['status'] . '" href="edit/' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default locked" title="Edit"><span style="color:orange" title="Edit" class="icon-pencil" aria-hidden="true"></span></a>
               <a id="delete" data-id="' . $industry_id . '" class="btn btn-circle btn-icon-only btn-default" title="Delete"><span style="color:brown" title="Delete" class="icon-trash" aria-hidden="true"></span></a>';
            if (isset($industry['name']) && !empty($industry['name'])) {
                $name = $industry['name'];
            }

            if (isset($industry['image']) && !empty($industry['image'])) {
                if (file_exists('uploads/' . $industry['image'])) {
                    $image = '<img src="' . asset('uploads/' . $industry['image']) . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                } else {
                    $image = '<img src="' . $industry['image'] . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                }

            } else {
                if (isset($industry['file_name']) && !empty($industry['file_name'])) {
                    $image = '<img src="' . $industry['image_file_name'] . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                } else {
                    $image = 'N/A';
                }

            }

            $userName =$industry['user_name'];

              if (isset($industry['user_image']) && !empty($industry['user_image'])) {
                if (file_exists('uploads/' . $industry['user_image'])) {
                    $userImage = '<img src="' . asset('uploads/' . $industry['user_image']) . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                } else {
                    $userImage = '<img src="' . $industry['user_image'] . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                }

            } else {
                if (isset($industry['file_name']) && !empty($industry['file_name'])) {
                    $userImage = '<img src="' . $industry['user_file_name'] . '" width="50" height="50" class="img-circle" alt="User Image"/>';
                } else {
                    $userImage = 'N/A';
                }

            }

            if (isset($industry->description) && !empty($industry->description)) {
                $length = strlen($industry->description);
                if ($length > 50) {
					//
                    $description = substr(strip_tags($industry->description), 0, 10) . '...';
                } else {
                    $description = substr(strip_tags($industry->description), 0, 10);
                }
            } else {
                $description = 'N/A';
            }

            if ($industry['status']) {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Active" id="change-common-status" data-table="industry" data-id="' . $industry['_id'] . '" data-status="' . $industry['status'] . '" data-action="Plans"><i class="fa fa-circle text-success active"></i><a></div>';
            } else {
                $status = '<div class="statuscenter"><a class="btn btn-circle btn-icon-only btn-default" title="Inactive" id="change-common-status" data-table="industry" data-id="' . $industry['_id'] . '" data-status="' . $industry['status'] . '" data-action="Plans"><i class="fa fa-circle text-danger inactive"></i><a></div>';
            }
            $GLOBALS['data'][] = array($i, $name, $description, $image, $userName ,$userImage, $status, $view_link);
            $i++;
        }
        $result = array();
        $result['data'] = $GLOBALS['data'];
        $result['draw'] = intval($request->get('draw'));
        $result['recordsTotal'] = $GLOBALS['total'];
        $result['recordsFiltered'] = $GLOBALS['total'];

        return json_encode($result);
    }
   
   /*=====================================
        View Particular  
    ======================================*/
    public function view(Request $request)
    {
        $id = $request->id;
        $data['reslutset'] = IndustryContent::where('_id', $id)->first();
        if (!$data['reslutset']) {
            $result = [
                'error' => 'error',
                'exception_message' => 'Record does not exist.',
            ];
            return json_encode($result);
        } else {
            return json_encode($data);
        }
    }




    /*========================
        Change Status 
    ==========================*/

    public function changeStatus(Request $request)
    {
        $id = $request->id;
        $industryContent = IndustryContent::where('_id', $id)->first();

        if ($industryContent->update(array('status' => (bool)$request->input('status') ? false : true))) {
           return response()->json(['success' => true,'message'=>trans('Admin/message.status_industry_content')]);
        }else{
           return response()->json(['success' => false,'message'=>trans('Admin/message.error')]); 
        }

    }

}
