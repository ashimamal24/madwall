<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\Wage;

class JobsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('jobValue');
        $repost_id = $this->get('repost_id');
        switch ($this->get('page')) {
            case 'add':
                return [
            //
                        //'title'						=> 'required|custom_title|unique:jobs',
                    'title' => 'required',
                    'job_category' => 'required',
                    'job_subcategory' => 'required',
                    'skills' => 'required',
                    'description' => 'required|check_desc',
                       // 'start_date'				=> 'required',
                        //'end_date'					=> 'required|custom_min:'.$this->start_date.'|custom_max:'.$this->start_date,
                    'salary_per_hour' => 'required|between:0,99.99|custom_salaryrange|custom_salarymin',
                    'number_of_worker_required' => 'custom_worker',
                    'address' => 'required|custom_location:' . $this->get('lat') . ',' . $this->get('lat'),
                    'dates' => 'required|datesize',
                    'offer' => 'custom_checkrehire:' . $this->get('job_published_type'),
                    'process_id' => 'required|checkprocess',
                    'lunch_hour' => 'required_if:unpaid_lunch,==,1',
                ];
                break;
            case 'edit':
                return [
                        //'title'						=> 'required|custom_title|unique:jobs,_id,'.$id,
                    'title' => 'required',
                    'job_category' => 'required',
                    'job_subcategory' => 'required',
                    'skills' => 'required',
                    'description' => 'required|check_desc',
                        //'start_date'				=> 'required',
                        //'end_date'					=> 'required|custom_min:'.$this->start_date.'|custom_max:'.$this->start_date,
                    'salary_per_hour' => 'required|between:0,99.99|custom_salaryrange|custom_salarymin',
                    'number_of_worker_required' => 'custom_worker',
                    'address' => 'required|custom_location:' . $this->get('lat') . ',' . $this->get('lat'),
                    'dates' => 'required|datesize',
                    'lunch_hour' => 'required_if:unpaid_lunch,==,1',
                ];
                break;
            case 'repost':
                return [
                        //'title'						=> 'required|custom_title|unique:jobs,_id,'.$repost_id,
                    'title' => 'required',
                    'job_category' => 'required',
                    'job_subcategory' => 'required',
                    'skills' => 'required',
                    'description' => 'required|check_desc',
                        //'start_date'				=> 'required',
                        //'end_date'					=> 'required|custom_min:'.$this->start_date.'|custom_max:'.$this->start_date,
                    'salary_per_hour' => 'required|between:0,99.99|custom_salaryrange|custom_salarymin',
                    'number_of_worker_required' => 'custom_worker',
                    'address' => 'required|custom_location:' . $this->get('lat') . ',' . $this->get('lat'),
                    'dates' => 'required|datesize',
                    'offer' => 'custom_checkrehire:' . $this->get('job_published_type'),
                    'process_id' => 'required|checkprocess',
                    'lunch_hour' => 'required_if:unpaid_lunch,==,1',
                ];
                break;
            default:
                # code...
                break;
        }

    }

    public function messages()
    {
        $min_wage = Wage::first();
        return [
            'title.required' => 'Please specify Job Name.',
            'title.unique' => 'Title already exist.',
            'job_category.required' => 'Please specify the job category.',
            'job_subcategory.required' => 'Please specify the job sub category.',
            'skills.required' => 'Please specify skills required for the job.',
            'description.check_desc' => 'Please specify Job Description.',
            'description.required' => 'Please specify Job Description.',
                    //'start_date.required'						=> 'Please specify start time of the job.',
                    //'end_date.required'							=> 'Please specify end time of the job.',
            'salary_per_hour.required' => 'Please specify Salary per hour of the job in dollars.',
            'salary_per_hour.custom_salarymin' => 'Hourly wage can not be less than $' . $min_wage->min_amount,
            'salary_per_hour.numeric' => 'Salary should contain numbers.',
            'number_of_worker_required.numeric' => 'Please specify number of persons required for the job.',
            'number_of_worker_required.required' => 'Number of persons required should contain numbers.',
            'address.required' => 'Please specify job location for the job.',
            'dates.required' => 'Please specify job days for the job.',
            'dates.datesize' => 'Maximum job length can be 30 days only.',
            'title.custom_title' => 'Job name should contain alphabets and numbers only within 5 – 50-character range',
            'description.custom_description' => 'Job Description should contain alphabets, numbers and special characters (“.”,”,”, “(”,”)”, “-”) only upto 20000 character range.',
            'number_of_worker_required.custom_worker' => 'This field should contain numbers only',
            'salary_per_hour.custom_salaryrange' => 'Salary should contain numbers with two decimal point only within 1 - 4-character range.',
                   // 'end_date.custom_min'						=> 'Minimum timeslot should be at least for 3 hour',
                   // 'end_date.custom_max'						=> 'Maximum timeslot cannot be more than 14 hours',
            'address.custom_location' => 'Please specify a valid location.',
            'offer.custom_checkrehire' => 'Please select required number of employees for this job.',
            'process_id.required' => 'Please select start and end time for all the selected dates.',
            'process_id.checkprocess' => 'Please select start and end time for all the selected dates.',
            'lunch_hour.required_if' => 'Please specify lunch hours',
        ];
    }
}
