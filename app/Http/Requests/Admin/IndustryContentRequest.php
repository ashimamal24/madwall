<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class IndustryContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('action') == 'edit') {


            $valid = array(
                'name' => 'required|unique:industry_contents,name,' . $this->request->get('idedit') . ',_id|min:5|max:20',
                'status' => 'required',
                'user_name'=>'required|max:20',
                'description' => 'required|max:100000',
                'image_file_name' => 'required',
                'user_file_name' => 'required',
            );
            if ($this->request->get('image_file_name') == "") {
                $valid = array_merge($valid, ['image' => 'required_if:image_file_name,==,""|image|mimes:jpg,png,jpeg|max:5000']);
            } elseif ($this->request->get('image') != "") {
                $valid = array_merge($valid, ['image' => 'required_if:image_file_name,==,""|image|mimes:jpg,png,jpeg|max:5000']);
            }
             if ($this->request->get('user_file_name') == "") {
                $valid = array_merge($valid, ['user_image' => 'required_if:user_file_name,==,""|image|mimes:jpg,png,jpeg|max:5000']);
            } elseif ($this->request->get('user_image') != "") {
                $valid = array_merge($valid, ['user_image' => 'required_if:user_file_name,==,""|image|mimes:jpg,png,jpeg|max:5000']);
            }
        } else {
            $valid = array(
                'name' => 'required|unique:industry_contents,name|min:5|max:20',
                'image_file_name' => 'required',
                'user_name'=>'required|max:20',
                'status' => 'required',
                'description' => 'required|max:100000',
                'image' => 'required|image|mimes:jpg,png,jpeg|max:5000',
                'user_file_name' => 'required',
                'user_image' => 'required|image|mimes:jpg,png,jpeg|max:5000',

            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Enter name.',
            'name.min' => 'Please enter name between 5-20 characters.',
            'name.max' => 'Please enter name between 5-20 characters.',
            'name.alpha_spaces' => 'Name can only contain alphabets.',
            'name.custom_unique' => 'Name already exists',
            'description.required' => 'Enter Description.',
            'status.required' => 'Select Status.',
            'image.required' => 'Please select logo for the industry.',
            'image.required_if' => 'Please select logo for the industry.',
            'image.max' => 'logo can not greater than 5MB in size',
            'user_image.required' => 'Please select user image.',
            'user_image.required_if' => 'Please select user image.',
            'user_image.max' => 'User Image can not greater than 5MB in size',
            'user_file_name.required'=>'Please select user image.'
            

        ];
    }
}
