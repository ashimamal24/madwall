<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $valid = array(
            'commission' => 'required|numeric|between:0,100',
        );
        if ($this->request->get('extra_discount') == 'null') {
            unset($valid['commission']);
        }
        return $valid;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'commission.required' => 'Commission cannot be empty',
            'commission.numeric' => 'Please specify commission in numbers only within 0 – 3 characters range',
            'commission.digits_between' => 'Commission must be a number between 1-3 digits.',
        ];
    }
}
