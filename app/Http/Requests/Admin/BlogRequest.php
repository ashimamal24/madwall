<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->request->get('action') == 'edit') {
            $valid = array(
                'blog_cat_id'=>'required',
                'title' => 'required|custom_unique:blogs,title,' . $this->request->get('idedit') . ',_id|min:5|max:100',
                'blog_description' => 'required|min:400|max:5000',
                'author_description' => 'required|max:1000',
                'author_name'=>'required|max:100',
                
            );

        } else {
            $valid = array(
                'blog_cat_id'=>'required',
                'title' => 'required|custom_unique:blogs,title|min:5|max:100',
                'blog_description' => 'required|min:400|max:5000',
                'author_description' => 'required|max:1000',
                'author_name'=>'required|max:100',
            );
        }
        return $valid;
    }

}
