<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HighlightRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('action') == 'edit') {


            $valid = array(
        
                'title'=>'required|max:50',
               'descriptions' => 'required|max:100000',
                
            );
          
        } else {

            $valid = array(
                
                'title'=>'required|max:50',
                'descriptions' => 'required|max:100000',
            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Please Enter title.',
            'title.max' => 'Please enter title between 5-20 characters.',
            'descriptions.required' => 'Enter Description.',
        ];
    }
}
