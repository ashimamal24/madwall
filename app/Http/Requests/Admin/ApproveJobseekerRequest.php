<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;


class ApproveJobseekerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('action') == 'edit') {

            $valid = array(
                'category_id' => 'required',
                'industry_id' => 'required',
                'skills' => 'required',
                'rating' => 'required|numeric|between:1,5.00',
                'subcategories' => 'required',
            );

            foreach ($this->request->get('proofs_value') as $key => $option) {
                $valid['proofs_value.' . $key] = 'required|alpha_num|max:25';
            }

            /*foreach($this->request->get('certificate_description') as $key=>$option){
                if( isset($option) ){
                    $valid['certificate_file_url.'.$key] = 'required';   
                }
            }*/

            foreach ($this->request->get('certificate_hidden_url') as $key => $option) {
                if (isset($option)) {
                    $valid['certificate_description.' . $key] = 'required|min:5|max:50';
                    //$valid['certificate_file_url.'.$key] = 'required';           	
                }
            }
        } else {


            $valid = array(
                'category_id' => 'required',
                'skills' => 'required',
                'subcategories' => 'required',
                'sin_number' => 'numeric|digits_between:0,9'
            );

            foreach ($this->request->get('certificate_description') as $key => $option) {
                if (isset($option)) {
                    $valid['certificate_file_url.' . $key] = 'required';
                }
            }
            foreach ($this->request->get('certificate_hidden_url') as $key => $option) {
                if (isset($option)) {
                    $valid['certificate_description.' . $key] = 'required|min:5|max:50';
                }
            }

            foreach ($this->request->get('proofs_value') as $key => $option) {
                $valid['proofs_value.' . $key] = 'required|alpha_num|max:25';
            }

        }

        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array 
     */
    public function messages()
    {
        return [
            'category_id.required' => 'Please select a Job category for the employee',
            'industry_id.required' => 'Please select an Industry for the employee',
            'subcategories.required' => 'Please select a Job subcategory for the employee ',
            'proofs_value.required' => 'All Proof Values are required.',
            'skills.required' => 'Please specify skills for the employee',
            'certificate_description.required' => 'All Certificate details are required.',
            'certificate_description.max' => 'Please specify the description between 5 – 50 characters only',
            'certificate_description.min' => 'Please specify the description between 5 – 50 characters only',
            'rating.required' => 'Please specify average rating of the employee',
            'rating.between' => 'Please specify rating between a scale of 1 – 5.',
            'sin_number.numeric' => 'SIN number must be a number'

        ];
    }
}
