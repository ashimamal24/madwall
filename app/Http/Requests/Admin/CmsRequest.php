<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->request->get('action') == 'edit') {
            $valid = array(
                'name' => 'required|alpha_spaces|custom_unique:cms,name,' . $this->request->get('idedit') . ',_id|min:5|max:40',
                'alias' => 'required',
                'content' => 'required',

            );
        } else {
            $valid = array(
                'name' => 'required|alpha_spaces|custom_unique:cms,name|min:5|max:40',
                'alias' => 'required',
                'content' => 'required',

            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Enter name.',
            'alias.required' => 'Fill alias ',
            'content.required' => 'Fill Content',

        ];
    }
}
