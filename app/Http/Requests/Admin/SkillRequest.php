<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;


class SkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('action') == 'edit') {
            $valid = array(
                'name' => 'required|custom_unique:master_skill,name,' . $this->request->get('idedit') . ',_id|min:5|max:40',
                'description' => 'required|min:1|max:5000',
                'status' => 'required|'
            );
        } else {
            $valid = array(
                'name' => 'required|min:5|max:40|custom_unique:master_skill,name',
                'description' => 'required|Min:1|max:5000',
                'status' => 'required|'
            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Enter skill name',
            'name.custom_unique' => 'Skill name already exists',
            'name.min' => 'Please enter skill name between 5-40 characters.',
            'name.max' => 'Please enter skill name between 5-40 characters.',
            'name.alpha_spaces' => 'Skill name can only contain alphabets',
            'description.required' => 'Please Enter Description',
            'status' => 'Select Status',
        ];
    }
}
