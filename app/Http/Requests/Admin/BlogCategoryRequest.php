<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class BlogCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->request->get('action') == 'edit') {
            $valid = array(
                'name' => 'required|custom_unique:blog_category,name,' . $this->request->get('idedit') . ',_id|min:5|max:40',
                'status' => 'required|',
            );

        } else {
            $valid = array(
                'name' => 'required|custom_unique:blog_category,name|min:5|max:40',
                'status' => 'required|',
            );
        }
        return $valid;
    }

}
