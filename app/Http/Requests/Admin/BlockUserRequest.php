<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;


class BlockUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $valid = array(
            'reason_for_block_usr' => 'required|min:5|max:500',
        );
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'reason_for_block_usr.required' => 'Please specify reason between 5 – 500 characters only',
            'reason_for_block_usr.min' => 'Please specify reason between 5 – 500 characters only',
            'reason_for_block_usr.max' => 'Please specify reason between 5 – 500 characters only',
        ];
    }
}
