<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class IndustryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('action') == 'edit') {
            $valid = array(
                'name' => 'required|custom_unique:industry,name,' . $this->request->get('idedit') . ',_id|min:5|max:50',
                'description' => 'required|min:10|max:5000',
                'status' => 'required',
                'file_name' => 'required',
            );
            if ($this->request->get('file_name') == "") {
                $valid = array_merge($valid, ['image' => 'required_if:file_name,==,""|image|mimes:jpg,png,jpeg|max:5000']);
            } elseif ($this->request->get('image') != "") {
                $valid = array_merge($valid, ['image' => 'required_if:file_name,==,""|image|mimes:jpg,png,jpeg|max:5000']);
            }
        } else {
            $valid = array(
                'name' => 'required|custom_unique:industry,name|min:5|max:50',
                'description' => 'required|min:10|max:5000',
                'file_name' => 'required',
                'status' => 'required',
                'image' => 'required|image|mimes:jpg,png,jpeg|max:5000',

            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Enter Industry name.',
            'name.min' => 'Please enter Industry name between 5-50 characters.',
            'name.max' => 'Please enter Industry name between 5-50 characters.',
            'name.alpha_spaces' => 'Industry name can only contain alphabets.',
            'name.custom_unique' => 'Industry name already exists',
            'description.required' => 'Enter Description.',
            'status.required' => 'Select Status.',
            'image.required' => 'Please select an image for the industry.',
            'image.required_if' => 'Please select an image for the industry.',
            'image.max' => 'Image can not greater than 5MB in size',

        ];
    }
}
