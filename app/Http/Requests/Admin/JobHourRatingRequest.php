<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class JobHourRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total_hours_worked' => 'required|numeric|min:2',
            'rating' => 'required|numeric|between:1,5.00'
        ];
    }
    public function messages()
    {
        return [
            'total_hours_worked.required' => 'Hours field cannot be empty.',
            'total_hours_worked.numeric' => 'Please specify hours in a range of 1 – 2 characters only containing numbers only.',
            'total_hours_worked.min' => 'Please specify hours in a range of 1 – 2 characters only containing numbers only.',
            'rating.required' => 'Rating field cannot be empty.',
            //'rating.numeric'				=> 'Please specify rating in a range of 1 – 5 number only with 2 decimal places allowed',
            'rating.between' => 'Please specify rating in a range of 1 – 5 number only with 2 decimal places allowed',

        ];
    }
}
