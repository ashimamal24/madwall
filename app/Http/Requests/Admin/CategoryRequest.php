<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        if ($this->request->get('action') == 'edit') {
            $valid = array(
                'name' => 'required|custom_unique:category,name,' . $this->request->get('idedit') . ',_id|min:5|max:40',
                'commision' => 'required|numeric|digits_between:1,3',
                'description' => 'required|min:1|max:5000',
                'status' => 'required|',
                'industry_id' => 'required|exists:industry,_id',

            );
            /*if( $this->request->get('type') == 'subcategory'  ){
                unset( $valid['parent_id'] );
            }*/
        } else {

            $valid = array(
                'name' => 'required|custom_unique:category,name|min:5|max:40',
                'commision' => 'required|numeric|digits_between:1,3',
                'description' => 'required|min:1|max:5000',
                'status' => 'required|',
                'industry_id' => 'required|exists:industry,_id',

            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Enter Category name.',
            'name.min' => 'Please enter category name between 5-40 characters.',
            'name.max' => 'Please enter category name between 5-40 characters.',
            'name.alpha_spaces' => 'Category name can only contain alphabets.',
            'name.custom_unique' => 'Category name already exists',
            'commision.required' => 'Enter commission.',
            'commision.numeric' => 'Commission must be a number between 1-3 digits.',
            'commision.digits_between' => 'Commission must be a number between 1-3 digits.',
            'description.required' => 'Enter Description.',
            'status.required' => 'Select Status.',
            'industry_id.required' => 'Select Industry.',

        ];
    }
}
