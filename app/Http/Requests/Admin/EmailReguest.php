<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EmailReguest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    /*public function myCustomUpdate()
    {
        $input = $this->all();

        return $input['content'] = strip_tags($this->request->get('content'));

        
    }*/

    public function rules()
    {
        //echo $this->myCustomUpdate();

        if ($this->request->get('action') == 'edit') {
            $valid = array(
                'name' => 'required|alpha_spaces|custom_unique:email_templates,name,' . $this->request->get('idedit') . ',_id|min:5|max:40',
                'subject' => 'required|alpha_spaces|min:5|max:40',
                'content' => 'required|min:5',
                'status' => 'required',
            );
        } else {
            $valid = array(
                'name' => 'required|alpha_spaces|custom_unique:email_templates,name|min:5|max:40',
                'subject' => 'required|alpha_spaces|min:5|max:40',
                'content' => 'required|min:5',
                'status' => 'required',
            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The title feild is required',
            'name.custom_unique' => 'Email title already exists',
            'subject.required' => 'Enter Subject',
            'title.alpha_spaces' => 'Title may only contain letters.',
            'content.required' => 'Enter Content',
            'content.min' => 'Enter Content',

            'status' => 'Please Select Status',

        ];
    }
}
