<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {


        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        if ($this->request->get('action') == 'edit') {
            $valid = array(
                //'name' => 'required|alpha_spaces|custom_unique:documents,name,' . $this->request->get('idedit') . ',_id|min:5|max:40',
                'name' => 'required|custom_unique:documents,name,' . $this->request->get('idedit') . ',_id|min:3|max:40',
                'status' => 'required',
                'general_file_url' => 'required',
                'file_url' => 'mimes:pdf',
            );
        } else {
            $valid = array(
                //'name' => 'required|alpha_spaces|custom_unique:documents,name|min:5|max:40',
                'name' => 'required|custom_unique:documents,name|min:3|max:40',
                'status' => 'required',
                'general_file_url' => 'required',
                'file_url' => 'mimes:pdf',
            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Please enter file name.',
            'status.required' => 'Select Status.',
            'file_url.mimes' => 'Only PDF format is allowed',
            'general_file_url.required' => 'Attachment feild is required.'
        ];
    }
}
