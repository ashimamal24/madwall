<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TimeSlotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_start_time_slot' => 'required',
            'first_end_time_slot' => 'required',
            'second_start_time_slot' => 'required',
            'second_end_time_slot' => 'required',
            'third_start_time_slot' => 'required',
            'third_end_time_slot' => 'required',

        ];

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_start_time_slot.required' => 'First slot start time is required.',
            'first_end_time_slot.required' => 'First slot end time is required.',
            'second_start_time_slot.required' => 'Second slot start time is required.',
            'second_end_time_slot.required' => 'Second slot end time is required.',
            'third_start_time_slot.required' => 'Third slot start time is required.',
            'third_end_time_slot.required' => 'Third slot end time is required.',

        ];
    }
}
