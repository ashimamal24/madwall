<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if ($this->request->get('action') == 'edit') {
            $valid = array( 
                //'skills'     => 'required|',
                'description' => 'required|min:1|max:5000',
                'status' => 'required|',
                'category_id' => 'required|',
                'industry_id' => 'required|',
                'name' => 'required|custom_subcategory:category,name,' . $this->request->get('idedit') . ',_id,' . $this->request->get('category_id') . ',subcategory|min:5|max:40',
            );
        } else {
            $valid = array(
               // 'skills'        => 'required|',
                'description' => 'required|min:1|max:5000',
                'status' => 'required|',
                'category_id' => 'required|',
                'industry_id' => 'required|',
                'name' => 'required|custom_subcategory:category,name,' . $this->request->get('category_id') . ',subcategory|min:5|max:40',

            );
        }
        return $valid;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Enter Subcategory name.',
            'name.min' => 'Please enter subcategory name between 1-40 characters.',
            'name.max' => 'Please enter subcategory name between 5-40 characters.',
            'name.alpha_spaces' => 'Sub-Category name can only contain alphabets.',
            'name.custom_subcategory' => 'Sub-Category name already exists',
            'skills.required' => 'Select Skill',
            'description.required' => 'Enter Description.',
            'status.required' => 'Select Status.',
            'category_id.required' => 'Select Category.',
            'industry_id.required' => 'Select Industry.',

        ];
    }
}
