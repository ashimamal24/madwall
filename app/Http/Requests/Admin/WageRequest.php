<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class WageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /*======================================================
     * Get the validation rules that apply to the request.
     * @return array
     *=======================================================*/
    public function rules()
    {
        $valid = array(
            'min_amount' => 'required|numeric'
        );

        return $valid;
    }

    /*==========================================================
     * Get the error messages for the defined validation rules
     *=========================================================*/
    public function messages()
    {
        return [
            'min_amount.required' => 'Enter minimum wage',
            'min_amount.numeric' => 'Wage must be numberic',

        ];
    }
}
