<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name' => 'required|alpha_spaces|Max:40',
            'last_name' => 'required|alpha_spaces|Max:40',
            'comment' => 'required|Max:1000',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Please enter first name',
            'first_name.alpha_spaces' => 'Please enter a valid first name',
            'last_name.required' => 'Please enter last name',
            'last_name.alpha_spaces' => 'Please enter a valid last name',
            'comment.required' => 'Please specify a comment',
            'comment.min' => 'Please enter the message between 50 to 1000 characters only',
            'comment.max' => 'Please enter the message between 50 to 1000 characters only',
        ];
    }
}
