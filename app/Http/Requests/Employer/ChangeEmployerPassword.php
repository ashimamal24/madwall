<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;

class ChangeEmployerPassword extends FormRequest
{
    /*****************************************************************
     * Determine if the user is authorized to make this request.
     * @return bool
     *****************************************************************/
    public function authorize()
    {
        return true;
    }

    /******************************************************
     * Get the validation rules that apply to the request.
     * @return array
     ******************************************************/
    public function rules()
    {
        return [
            'old_password' => 'bail|required|',
            'password' => array(
                'required',
                'min:6',
                'password_custom',
            ),
            'password_confirmation' => 'required|same:password',
        ];
    }

    /************************************************************
     * Get the error messages for the defined validation rules.
     * @return array
     ************************************************************/
    public function messages()
    {
        return [
            'old_password' => 'Please Enter Old Password.',
            'password' => 'Please Enter New Password.',
            'password.min' => 'Password should be at least 6 characters long.',
            'password_confirmation' => 'Passwords do not match',
            'password.password_custom' => 'Password must contain at least one uppercase letter, one lowercase letter, a number, a special character, and must be at least six characters long.',
            'password_confirmation.same' => 'New password and confirm password do not match.Please enter both again.',
        ];
    }
}
