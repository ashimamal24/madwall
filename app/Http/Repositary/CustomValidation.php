<?php
namespace App\Http\Repositary;

use Illuminate\Validation\Validator;
use JWTAuth;
use App\Model\User;
use App\Model\JobsModel;
use App\Model\JobsapplicationModel;
use DB;
use Carbon\Carbon;
use App\Model\Wage;
use App\Model\TempJobs;


/*===============================================================================================
	Class for custom validation Created by debut Infotech Pankaj Cheema
=================================================================================================
 */
class CustomValidation extends Validator
{
	
	/*=====================================================================
		Validaton for checking that otp is expired or not 
	=======================================================================
     */
    public function validateOtpExpire($attribute, $value, $parameters)
    {
        $user = User::where(array('otp' => $value))->first();

        if (!$user || $user->otp_expire < \Carbon\Carbon::now()) {
            return false;
        }

        return true;
    }

    /*=======================================================================
        Function for deleting user if he tries to register again
     * updated : 12 aug 2017, shivani - to check if user has already been declined or not.
    ==========================================================================
     */
    public function validateReRegister($attribute, $value, $parameters)
    {
        $user = User::where(array('email' => $value, 'status' => false, 'profile_complete' => 1))->first();
        if ($user) {
            $user->delete();
            return true;
        } else {
            $is_declined = User::where(array('email' => $value))->value('approved');
            if ($is_declined == 2) {
                return false;
            }
            return true;
        }

        return true;
    }


    public function validateOtpVerified($attribute, $value, $parameters)
    {
        $user = User::where(array('email' => $value, 'status' => false))->orWhere('phone', $value)->first();
        if ($user) {
            return false;
        }

        return true;
    }  //attempt

    public function validateAttemptCount($attribute, $value, $parameters)
    {
        $user = User::where(array('_id' => JWTAuth::parseToken()->authenticate()->_id))->first();
        if (!$user->health_quiz_attempt) {
            return false;
        }

        return true;
    }

    /*===========================================================================================
        Custom validation rule added by pankaj cheema debutinfotech 
    =============================================================================================*/

    public function validateCustomUnique($attribute, $value, $parameters)
    {

        if (count($parameters) > 2) {
            $value_exist = DB::table($parameters[0])->where(array($parameters[1] => strtolower($value), 'is_deleted' => false))->where('_id', '!=', $parameters[2])->first();
        } else {
            $value_exist = DB::table($parameters[0])->where(array($parameters[1] => strtolower($value), 'is_deleted' => false))->first();
        }

        if ($value_exist) {
            return false;
        }


        return true;
    }
    
    
    /*
     * Added on : 27 sep 2017, shivani
     * DESC : 
     * */
    public function validateCustomSubcategory($attribute, $value, $parameters)
    {


        if (count($parameters) > 4) {
            $value_exist = DB::table($parameters[0])->where(array($parameters[1] => strtolower($value), 'is_deleted' => false))->where('_id', '!=', $parameters[2])->where('type', $parameters[5])->where('category_id', $parameters[4])->first();
        } else {
            $value_exist = DB::table($parameters[0])->where(array($parameters[1] => strtolower($value), 'is_deleted' => false))->where('type', $parameters[3])->where('category_id', $parameters[2])->first();
        }

        if ($value_exist) {
            return false;
        }


        return true;
    }





    public function validateAlphaSpaces($attribute, $value)
    {
        return preg_match('/^[\pL\s]+$/u', $value);
    }

    public function validateCaptcha($attribute, $value)
    {
        return (new Captcha)->validate($value);
    }

    public function validateAlphaSpacesNumric($attribute, $value)
    {
        return preg_match('/^[\pL\s0-9]+$/u', $value);
    }
    public function validatePlusNumric($attribute, $value)
    {
        return preg_match('/^(?=.*[0-9])[- +()0-9]+$/', $value);
    }
    public function validatePasswordCustom($attribute, $value, $parameters)
    {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d](?=.*?[\W_]).{6,25}$/', $value);
    }


    public function validateDatesize($attribute, $value, $parameters)
    {
        $data = explode(',', $value);

        if (count($data) > 30)
            return false;
        else
            return true;
    }

    public function validateCheckindustry($attribute, $value, $parameters)
    {
        if (count($value) > 3)
            return false;
        else
            return true;
    }

    public function validatecustomTitle($attribute, $value, $parameters)
    {
        return preg_match("/^[a-zA-Z0-9 ' ']{5,50}$/", $value);
    }

    public function validateCustomDescription($attribute, $value, $parameters)
    {
        return preg_match("/^[a-zA-Z0-9 '.,'-()\-]{1,22000}+$/", $value);
    }

    public function validateCustomSalaryrange($attribute, $value, $parameters)
    {
        return preg_match("/^[0-9 '.']{1,5}+$/", $value);
    }
    
	/*
     * Added : 21 nov 2017 - to validate minimum wage set by admin.
     * */
    public function validateCustomSalarymin($attribute, $value, $parameters)
    {
        //check for minimum wage
        $min_wage = Wage::first();
        if (doubleval($value) < $min_wage->min_amount) {
            return false;
        }
        return true;
    }
    
	/*
     * Added : 21 nov 2017 - to validate if there is any user selected for rehire job or not.
     * */
    public function validateCustomCheckrehire($attribute, $value, $parameters)
    {
        if ($parameters[0] == 2 && empty($value)) {
            return false;
        }
        return true;
    }
    
    
	/*
     * Added : 21 nov 2017 - to validate if date time has been selected corresponding to the process id 
     * */
    public function validateCheckprocess($attribute, $value, $parameters)
    {
        $jobDates = TempJobs::where('process_id', $value)->select('start_date', 'end_date')->get()->toArray();
        if (empty($jobDates)) {
            return false;
        }
        return true;
    }
    
    /*
     * DESC : to check if description has some value or not (using strip tags), 21 nov 2017
     * */
    public function validateCheckDesc($attribute, $value, $parameters)
    {
        if (empty(strip_tags($value))) {
            return false;
        }
        return true;
    }


    public function validateCustomWorker($attribute, $value, $parameters)
    {
        return preg_match("/^[0-9]{1,4}+$/", $value);
    }
	
	
	/*
     * updated : 21 aug 2017
     * updated by : shivani, Debut infotech
     * DESC : to check difference using carbon
     * */
    public function validateCustomMin($attribute, $value, $parameters)
    {
        $to = \Carbon\Carbon::createFromFormat('H:i', $value);
        $from = \Carbon\Carbon::createFromFormat('H:i', $parameters[0]);
        $diff_in_hours = $to->diffInHours($from);
        if ($diff_in_hours == 0 || $diff_in_hours < 3)
            return false;
        else
            return true;
    }
	
	/*
     * updated : 21 aug 2017
     * updated by : shivani, Debut infotech
     * DESC : to check difference using carbon
     * */
    public function validateCustomMax($attribute, $value, $parameters)
    {
        $to = \Carbon\Carbon::createFromFormat('H:i', $value);
        $from = \Carbon\Carbon::createFromFormat('H:i', $parameters[0]);
        if ($from > $to) {
			//add one day to end date
            $to = $to->addDays(1);

        }
        $diff_in_hours = $to->diffInHours($from);
        if ($diff_in_hours > 14) {
            return false;
        } else {
            return true;
        }
    }

    public function validateCustomUniqueEmail($attribute, $value, $parameters)
    {
        $value = strtolower($value);
        $user = User::where(array('email' => $value))->first();
        if ($user) {
            return false;
        }

        return true;
    }

    public function validateCustomUser($attribute, $value, $parameters)
    {
        $user = User::where(array('email' => $value))->first();
        if ($user)
            return true;
        else
            return false;

    }

    public function validateCustomLocation($attribute, $value, $parameters)
    {
        if (!empty($value)) {
            if (empty($parameters[0]) || empty($parameters[1]))
                return false;
        }

        return true;
    }

    public function validateCustomUsercheck($attribute, $value, $parameters)
    {

        $user = User::where(array('email' => $value, 'approved' => 2))->first();
        if ($user)
            return false;
        else
            return true;
    }
     //check whether a job has been started or not
    public function validateIsJobstarted($attribute, $value, $parameters)
    {

        $job = JobsModel::where('_id', $value)->value('start_date');
        //$carbondate = \Carbon\Carbon::now()->addHours(1);
        $carbondate = \Carbon\Carbon::now();

        if (\Carbon\Carbon::now() < $job)
            return true;
        else
            return false;
    }

    //check whether a user is approved or not
    public function validateIsApproved($attribute, $value, $parameters)
    {

        if (JWTAuth::parseToken()->authenticate()->approved == 3 || JWTAuth::parseToken()->authenticate()->status == false) {
            return false;
        }
        return true;
    }
    //check whether a user is approved or not
    public function validateRating($attribute, $value, $parameters)
    {

        if (JWTAuth::parseToken()->authenticate()->rating < 3.95) {
            return false;
        }
        return true;
    }
    public function validateFilledCustom($attribute, $value, $parameters)
    {

        $job = JobsModel::where('_id', $value)->first();
        if ($job->total_hired >= $job->number_of_worker_required) {
            return false;
        }
        return true;
    }

    public function validateDeleted($attribute, $value, $parameters)
    {

        $job = JobsModel::where('_id', $value)->first();
        if ($job->is_deleted) {
            return false;
        }
        return true;
    }
    public function validateEmailConfirmed($attribute, $value, $parameters)
    {
        if (JWTAuth::parseToken()->authenticate()->user_email_confirmed) {
            return true;
        }
        return false;
    }
    public function validateApplied($attribute, $value, $parameters)
    {

        if (JobsapplicationModel::where(array('job_id' => $value, 'jobseeker_id' => JWTAuth::parseToken()->authenticate()->_id))->count()) {
            return false;
        }
        return true;
    }

    public function validateApprovedEmail($attribute, $value, $parameters)
    {
        $is_declined = User::where(array('email' => $value))->value('approved');
        if ($is_declined == 2) {
            return false;
        }
        return true;
    }

    public function validateOtpForgetpassword($attribute, $value, $parameters)
    {

        $profile_complete = User::where('email', $value)->value('profile_complete');

        if ($profile_complete == 1) {
            return false;
        }
        return true;
    }
    
    /*
     * Added on : 17 aug 2017
     * Added by : shivani, Debut infotech
     * DESC : to verify if user has verified otp or not
     * */
    public function validatePhoneOtpForgetpassword($attribute, $value, $parameters)
    {
        $profile_complete = User::where('phone', $value)->value('profile_complete');

        if ($profile_complete == 1) {
            return false;
        }
        return true;
    }
}
