<?php
namespace App\Http\Repositary;

use App\Model\User;
use App\Model\JobsapplicationModel;
use App\Model\ShiftModel;
use App\Model\Token;
use JWTAuth;
use Twilio;
use DateTime;
use DateTimeZone;
use App\Jobs\SendOtpEmail;
use Auth;//27july2017, shivani
use App\Model\JobsModel;
use App\Model\NotificationModel;
use App\Model\Setting;
use App\Model\UserRatings;
use Carbon\Carbon;
use App\Model\EmailVerification;
use App\Model\SendQuizNotification;
use App\Model\EmailTemplate;
use Laravel\Lumen\Routing\DispatchesJobs;
use Queue;
use DB;
use App\Model\CategoryModel;
use App\Model\SkillModel;
use App\Model\ApplicantShift;
use App\Model\TempJobs;
use App\Model\Documentation;
use App\Model\JobInterview;
use App\Http\Repositary\CommonRepositary;

class EmployerRepositary
{
	
    /*
	 * Added : 21 nov 2017, shivani debut infotech
	 * DESC : to check if any of the date time conflicts with itself or not
	 * */
	public function check_clashed_times($selected_dateTime, $s_date, $e_date)
	{
		if (!empty($selected_dateTime)) {
			foreach ($selected_dateTime as $slctd) {
				$app_start = $slctd['start_date'];
				$app_end = $slctd['end_date'];
				$job_start = $s_date;
				$job_end = $e_date;
				if ($job_start == $app_start && $job_start < $app_end && $job_end > $app_start && $job_end == $app_end) {
					return ['available' => false];
				} else if ($job_start < $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
					return ['available' => false];
				} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end > $app_end) {
					return ['available' => false];
				} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
					return ['available' => false];
				} else if ($job_start < $app_start && $job_start < $app_end && $job_end > $app_start && $job_end > $app_end) {
					return ['available' => false];
				} else if ($job_start == $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
					return ['available' => false];
				} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end == $app_end) {
					return ['available' => false];
				} else {
					//start/end dates can not be same
					$fgh = $job_start;
					$job_start_date = $job_start->format('Y-m-d');
					$job_end_date = $job_end->format('Y-m-d');
					$app_start_date = $app_start->format('Y-m-d');
					$app_end_date = $app_end->format('Y-m-d');
					if ($job_start_date == $app_start_date && $job_end_date == $app_end_date && $job_end_date == $app_start_date && $job_end_date == $app_end_date) {
						return ['available' => false];
					}
					if ($job_end_date == $app_end_date) {
						return ['available' => false];
					}
				}
			}
			return ['available' => true];
		} else {
			return ['available' => true];
		}

	}
	
	/*
	 * Added on : 29 aug 2017, shivani 
	 * DESC : to check if job can be edited or not
	 * */
	public function check_job($jobs, $current_date)
	{
		$edit = false;
		$start_date = Carbon::parse($jobs->start_date)->format('Y-m-d H:i:s');
		$end_date = Carbon::parse($jobs->end_date)->format('Y-m-d H:i:s');
		if ($jobs->job_published_type == 2 && $jobs->total_hired == 0) {
			$edit = true;
		} else {
			if ($current_date >= $start_date) {
				if (($jobs->total_hired == 0 && $jobs->total_applied == 0)) {
					$edit = true;
				} else {
					$edit = false;
				}
			} else {
				if (($current_date <= $start_date) && ($jobs->total_hired == 0) && ($jobs->total_applied == 0)) {
					$edit = true;
				} elseif (($jobs->total_hired > 0) || ($jobs->total_applied > 0)) {
					$edit = false;
				} else {
					$edit = true;
				}
			}
		}
		return ['edit' => $edit];
	}


	/*
	 * Added on : 30 aug 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to check if job can be deleted immediately or will be deleted on next day
	 * */
	public function check_job_deletion($job)
	{
		$today = Carbon::now();
		//$current_date = $today->addHours(3)->parse($today);
		$current_date = Carbon::now();
		$start_date = Carbon::parse($job->start_date);
		$end_date = Carbon::parse($job->end_date);
		$start_time = $start_date->format('H:i');
		$end_time = $end_date->format('H:i');
		$current_time = $current_date->format('H:i');
		$delete_job = true;
		$delete_time = 0;
		//check if anybody has been hired for this job or not.
		if ($job->total_hired > 0) {
			//check if job in progress 
			if ($current_date > $start_date && $current_date < $end_date) {
				//now compare by time (h:i)
				if ($current_time > $end_time) {
					//delete immediately
					$delete_job = true;
				} else if ($current_time > $start_time && $current_time < $end_time) {
					//delete on next day, current day shift is in progress
					$delete_job = false;
					$delete_hours = $end_time - $current_time;
					$delete_time = $current_date->addHours($delete_hours)->parse($current_date);
				} else {
					//delete immediately, default scenario
					$delete_job = true;
				}
			} else if ($current_date > $end_date) {
				//delete immediately, job has been completed now
				$delete_job = true;
			} else {
				//delete immediately
				$delete_job = true;
			}
		} else {
			//delete immediately, nobody is hired for this job then the job can be deleted immediately
			$delete_job = true;
		}

		return ['delete_job' => $delete_job, 'delete_time' => $delete_time];
	}

	public function get_skills($subcat_id)
	{
		//$subcat_id = $request->value;
		$skills = CategoryModel::where(array('status' => true, '_id' => $subcat_id, 'type' => 'subcategory'))->select('skills', 'mandatory_skills')->get()->toArray();
		$skilldata = [];
		$sId = [];
		$mId = [];
		foreach ($skills as $k => $value) {
			$sId = empty($value['skills']) ? [] : $value['skills'];
			if (isset($value['mandatory_skills']) && !empty($value['mandatory_skills'])) {
				$mId = empty($value['mandatory_skills']) ? [] : $value['mandatory_skills'];
			}
		}

		if ($skills) {
			if (!empty($mId)) {
				$sId = array_merge($sId, $mId);
			}
			$skilldata = SkillModel::whereIn('_id', $sId)->where('status', true)->select('_id', 'name')->get()->toArray();
		}

		return $skilldata;
	}
	
	/*
	 * Added on : 29 aug 2017
	 * Added by : shivani, Debut Infotech
	 * DESC : to return jobseekers list for rehire job
	 * */
	public function getRehireList($accepted_users, $employer_id = null)
	{
		$user_id = Auth::user()->_id;
		if (!empty($employer_id)) {
			$user_id = $employer_id;
		}
		$current_date = Carbon::now();
		$end_date = Carbon::now()->subDays(config('app.rehire_days'));
		$offerlist = JobsModel::with(['jobapplied' => function ($q) {
			$q->where(array('job_status' => 4, 'is_deleted' => false))->get();
		}])->where(array('job_status' => 4, 'is_deleted' => false, 'user_id' => $user_id))->where('end_date', '<=', $current_date)->where('end_date', '>=', $end_date)->select('_id')->get();
		$data = [];
		$i = 1;
		foreach ($offerlist as $k => $user) {
			foreach ($user->jobapplied as $row) {
				if (!in_array($row->jobseeker_id, $accepted_users)) {
					$data[] = $row->jobseeker_id;
				}
			}
		}
		$userIds = array_unique($data);
		$users = User::where(array('is_deleted' => false, 'role' => 2, 'approved' => 1))->whereIn('_id', $userIds)->select('first_name', 'last_name', '_id', 'image', 'rating', 'approved', 'email')->orderBy('rating', 'asc')->get();
		return ['users' => $users];
	}
	
	/*
	 * Added : 28 nov 2017, shivani
	 * DESC : to update applicant shifts when a job shift is updated
	 * @job_id, @applicants = array (Applicant ids)
	 * */
	public function update_applicant_shifts($job_id, $applicants)
	{
		//remove all previous data
		$removePrevious = ApplicantShift::where('job_id', $job_id)->where('total_hours_worked', 0)->delete();
		$lunch_hour = JobsModel::where('_id', $job_id)->value('lunch_hour');
		$lunch_hour = empty($lunch_hour) ? 0 : floatval(number_format(($lunch_hour / 60), 2));
		//save new details
		$details = ShiftModel::where('job_id', $job_id)->where('start_date', '>', Carbon::now())->get()->toArray();
		foreach ($applicants as $app_id) {
			foreach ($details as $dtl) {
				$data['total_hours_worked'] = 0;
				$data['job_id'] = $job_id;
				$data['app_id'] = $app_id;
				$data['start_date'] = Carbon::parse($dtl['start_date']);
				$data['end_date'] = Carbon::parse($dtl['end_date']);
				$data['lunch_hour'] = $lunch_hour;
				$savedta = new ApplicantShift($data);
				$savedta->save();
			}
			//notify candidate about job updation
			$employee_id = JobsapplicationModel::where('_id', $app_id)->value('jobseeker_id');
			$common = new CommonRepositary();
			$common->send_notification($employee_id, 11, $job_id);
		}
	}
	
	/*
	 * Added : 6 dec 2017, shivani debut infotech
	 * DESC : to return data for create/repost job view page
	 * */
	public function create_post_data($id = null)
	{
		$current_date = Carbon::now();
		$end_date = Carbon::now()->subDays(14);
		$jobs = $skill_All = $subcategory_All = $category = $users = [];
		$commission = 0;
        //repost Id
		$repost_id = '';
		$repost = false;
		if ($id) {
			$repost = true;
			$id = $repost_id = \Crypt::decrypt($id);
			//deleted job can also be reposted (24 aug 2017 - shivani)
			$jobs = JobsModel::where(array('_id' => $id))->first();
			if ($jobs) {
				$industIds = User::where(array('status' => true, '_id' => $jobs->user_id))->first();
				$inIds = [];
				if ($industIds) {
					foreach ($industIds->industry as $k => $indust) {
						$inIds[] = $indust['_id'];
					}
				}
				$category = CategoryModel::where(array('status' => true, 'type' => 'category', 'is_deleted' => false))->whereIn('industry_id', $inIds)->pluck('name', '_id')->toArray();   
				//get all subcategories
				$subcategoryAll = CategoryModel::where(array('status' => true, 'type' => 'subcategory', 'category_id' => $jobs->category[0]['_id']))->pluck('name', '_id')->toArray();
				//get all skills
				$skillsAll = $this->get_skills($jobs->subcategory[0]['_id']);

				foreach ($skillsAll as $k => $skl) {
					$skill_All[$skl['_id']] = $skl['name'];
				}
				$commission = $this->get_commision($jobs->category[0]['_id'], $jobs->user_id);
				foreach ($jobs->category as $key => $cat) {
					$selcategory[$cat['_id']] = $cat['name'];
				}

				$category = array_merge($category, $selcategory);
				foreach ($jobs->subcategory as $key => $subcat) {
					$subcategory_All[$subcat['_id']] = $subcat['name'];
				}

				foreach ($jobs->skill as $key => $skil) {
					$skill_All[$skil['_id']] = $skil['name'];
				}
            
				//rehire user details
				//$users = $this->get_rehire_users($jobs->user_id,false,$skill_All);
			} else {
				flash()->error('Invalid Id');
				$result = ['success' => false, 'error' => 'Invalid Id'];
				return $result; //redirect('employer/dashboard');
			}
		}
		$documetation = Documentation::All();

		return ['users' => $users, 'repost' => $repost, 'current_date' => $current_date, 'subcategory_All' => $subcategory_All, 'skill_All' => $skill_All, 'jobs' => $jobs, 'repost_id' => $repost_id, 'documetation' => $documetation, 'commission' => $commission, 'success' => true, 'category' => $category];
	}
	
	/*
	 * Added : 6 dec 2017, shivani debut infotech
	 * DESC : to save create/repost a job on behalf of employer/admin
	 * */
	public function save_job($request)
	{
		$common = new CommonRepositary();
		$data = $request->all();
		if (!empty($request->employer_id)) {
			$data['user_id'] = $request->employer_id;
		} else {
			$data['user_id'] = Auth::user()->_id;
		}
		$data['lunch_hour'] = null;
		if (!empty($request->lunch_hour)) {
			$data['lunch_hour'] = floatval($request->lunch_hour);
		}
		$requestedDates = explode(',', $request->dates);    
		//check job_dates from temp job data
		$jobDates = TempJobs::where('process_id', $request->process_id)->select('start_date', 'end_date')->orderBy('start_date', 'asc')->get()->toArray();
		$cur = Carbon::now();
		$timeSelected = [];
		foreach ($jobDates as $jb_dt) {
			$start_date = Carbon::parse($jb_dt['start_date']);
			$end_date = Carbon::parse($jb_dt['end_date']);			
			//if lunch hours has been selected
			if (!empty($data['lunch_hour'])) {
				//subtract lunch time from end time and check shift validity					
				$shift_time = Carbon::parse($jb_dt['end_date'])->subMinutes($data['lunch_hour']);

				$diff_minutes = $shift_time->diffInMinutes($start_date);
				$diffhours = floatval(($diff_minutes / 60));


				if ($diffhours < 3) {
					//return response()->json([ ],422);
					return ['success' => false, 'status' => false, 'dates' => 'Minimum timeslot for ' . $start_date->format('d-m-Y') . ' should be at least 3 hour. Please either increase the shift time or decrease unpaid lunch time'];
				}
				$totalDuration = $shift_time->diffInHours($start_date);
			} else {
				$totalDuration = $end_date->diffInHours($start_date);
			}

			if ($totalDuration == 0) {
				return ['success' => false, 'status' => false, 'dates' => 'Minimum timeslot for ' . $s_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
			}
			if ($totalDuration < config('app.min_hour')) {
				return ['success' => false, 'status' => false, 'dates' => 'Minimum timeslot for ' . $s_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
			}
			if ($totalDuration > config('app.max_hour')) {
				return ['success' => false, 'status' => false, 'dates' => 'Maximum timeslot for ' . $s_date->format('d-m-Y') . ' cannot be more than ' . config('app.max_hour') . ' hours'];
			}
			//check each selected date i.e.. selected for rehire job.
			/*if($totalDuration<=12 && $data['job_published_type'] == 2){
				//return response()->json([],422);
				return ['success'=>false,'status'=>false,'rehire_error'=>"Rehire cannot be used within 12 hours prior to the job's start time."];
			}*/
			//subtract one hour
			$data2[] = ['start_date' => $start_date, 'end_date' => $end_date];
			$timeSelected[] = $start_date->format('d-m-Y');
		}
		
		//check if time has been selected for all the dates or not.
		if ($requestedDates === array_intersect($requestedDates, $timeSelected) && $timeSelected === array_intersect($timeSelected, $requestedDates)) {
			//continue;
		} else {
			return ['success' => false, 'status' => false, 'dates' => 'Please select start and end time for all the selected dates'];
		}

		$data['category'] = CategoryModel::where('_id', $data['job_category'])->get()->toArray();
		//save commision %age,
		$commision = $this->get_commision($data['job_category'], $data['user_id']);
		$data['category_commision'] = $commision['commision'];
		$data['subcategory'] = CategoryModel::where('_id', $data['job_subcategory'])->get()->toArray();
		$data['skill'] = SkillModel::whereIn('_id', $data['skills'])->get()->toArray();
		$startDate = current($data2);
		$endDate = end($data2);
		$data['start_date'] = $startDate['start_date'];
		$data['end_date'] = $endDate['end_date'];

		$start_time = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse(date('Y-m-d') . ' ' . $request->start_date), $_COOKIE['client_timezone']);
		$end_time = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse(date('Y-m-d') . ' ' . $request->end_date), $_COOKIE['client_timezone']);

		$data['start_time'] = Carbon::parse($start_time)->format('H:i');
		$data['end_time'] = Carbon::parse($end_time)->format('H:i');

		$data['job_published_type'] = intval($data['job_published_type']);
		$data['number_of_worker_required'] = intval($data['number_of_worker_required']);
		$data['salary_per_hour'] = doubleval($data['salary_per_hour']);
		$data['total_applied'] = 0;
		$data['total_hired'] = 0;
		$data['status'] = true;
		$data['job_status'] = 1;
		$data['is_deleted'] = false;
		
		//$data['user_id'] = $this->auth->user()->_id;
		$data['job_id'] = $common->get_uniq_id('job');
		$data['publisher_status'] = true;
		if (empty(strip_tags($data['physical_requirement']))) {
			$data['physical_requirement'] = null;
		}
		if (empty(strip_tags($data['safety_hazards']))) {
			$data['safety_hazards'] = null;
		}
		$data['location'] = ['lng' => doubleval($data['lng']), 'lat' => doubleval($data['lat'])];
		//if rehire job is posted within upcoming 12 hours, it will be automatic 
		if ($totalDuration <= 12 && $data['job_published_type'] == 2) {
			//$data['job_published_type'] = 1;				
			$data['rehire_type'] = intval($data['rehire_type']);
		}

		// CREATE THE JOB
		if ($jobs = JobsModel::create($data)) {
			$common->setShift($data2, $jobs->_id);//set shifts timings for the job
			$hire = $jobs->total_applied;
		   //if rehire job is posted, then create application records.
			if ($data['job_published_type'] == 2) {
				foreach ($data['offer'] as $k => $val) {
					$data1['job_id'] = $jobs->_id;
					$data1['jobseeker_id'] = $val;
					$data1['employer_id'] = $data['user_id'];
					$data1['job_status'] = 0; //for offered job
					$data1['job_type'] = intval($data['job_published_type']);
					$data1['is_deleted'] = false;
					$data1['is_applied'] = true;
					$data1['rating'] = false;
					$data1['is_viewed'] = false;//(to track if jobseeker has checked this job or not)
					//$data1['shifts'] = $common->shift_details($jobs->_id);
					//notify candidate for the offered job                     
					$saveApplication = JobsapplicationModel::create($data1);
					$common->send_notification($data1['jobseeker_id'], $type = 5, $job_id = $data1['job_id']);
					$hire += 1;
				}
			}
			$data2['total_applied'] = $hire;
			$jobs->update($data2);
			//save job details to temporary notification table (notification will be sent later)
			if ($data['job_published_type'] != 2) {
				$notification['title'] = $data['title'];
				$notification['notification_sent'] = false;
				$notification['type'] = 'job';
				$notification['job_id'] = $jobs->_id;
				$notification['lat'] = $data['lat'];
				$notification['lng'] = $data['lng'];
				SendQuizNotification::create($notification);
			}
			//remove temporary process details 
			TempJobs::where('process_id', $request->process_id)->delete();
			flash()->success('Job posted successfully!!');
			$url = url('employer/dashboard');
			$result = ['success' => true, 'status' => 1, 'message' => 'Job posted successfully', 'url' => $url];
		} else {
			flash()->error('Job not posted successfully!!');
			$result = ['success' => false, 'status' => false, 'message' => 'Job posted not successfully!'];
		}
		return $result;//response()->json($result);
	}  
    
    /*
	 * Added on : 28 sep 2017
	 * DESC : to return commision detail for a job
	 * */
	function get_commision($cat_id, $employer_id = null)
	{
		$commission = CategoryModel::where(array('_id' => $cat_id))->value('commision');
		$user = Auth::user();
		if (!empty($employer_id)) {
			$user = User::where('_id', $employer_id)->first();
		}

		if (isset($user->category_details) && !empty($user->category_details)) {
			foreach ($user->category_details as $catDtl) {
				if ($catDtl['_id'] == $cat_id) {
					if (!empty($catDtl['extra']))
						$commission += $catDtl['extra'];

					if (!empty($catDtl['discount']))
						$commission -= $catDtl['discount'];
				}
			}
		}
		return ['commision' => $commission];
	}
	
	/*
	 * Added : 7 dec 2017, shivani Debut infotech
	 * DESC : to save temporary shift details
	 * */
	public function save_temp_process($shiftTime)
	{
		$shiftData = [];
		$common = new CommonRepositary();
		if (!empty($shiftTime)) {
			$selected_dateTime = [];
			$counter = 0;
			if (isset($shiftTime['process_id'])) {
				$process_id = $shiftTime['process_id'];
			} else {
				$process_id = 'PROCESS_' . mt_rand(100000, 999999);
			}

			$current_time = Carbon::now();
			$pastShifts = [];
			if (!empty($shiftTime['job_id'])) {
				//to check if job is updated, then past shifts will not get deleted.
				$job_id = \Crypt::decrypt($shiftTime['job_id']); 
				//check the shifts that has been passed away
				$curTime = Carbon::now();
				$pastShifts = ShiftModel::where('job_id', $job_id)->where('start_date', '<', $curTime)->select('start_date', 'end_date')->get()->toArray();
				if (!empty($pastShifts)) {
					foreach ($pastShifts as $pstDate) {
						$past_startDate = Carbon::parse($pstDate['start_date']);
						$past_endDate = Carbon::parse($pstDate['end_date']);
						$selected_dateTime[] = ['start_date' => $past_startDate, 'end_date' => $past_endDate];
					}
				}
			}
			foreach ($shiftTime['start'] as $date => $time) {
				$start_date = explode('_', $date);
				$selected_time = $time;
				$s_date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($start_date[1] . ' ' . $time)), $_COOKIE['client_timezone']);
				$e_date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($start_date[1] . ' ' . $shiftTime['end'][$date])), $_COOKIE['client_timezone']);
				//validate data , or add one day to end date
				$st_time = Carbon::parse($s_date)->format('H:i');
				$ed_time = Carbon::parse($e_date)->format('H:i');

				$s_time = Carbon::createFromFormat('H:i', $st_time);
				$e_time = Carbon::createFromFormat('H:i', $ed_time);
				if ($s_time > $e_time) {
					$e_date = $e_date->addDays(1);
				}
				//if lunch hours has been selected
				if (!empty($shiftTime['lunch_hour'])) {
					$shiftEndTime = Carbon::createFromFormat('Y-m-d H:i:s', $e_date, $_COOKIE['client_timezone']);
					//subtract lunch time from end time and check shift validity					
					$shift_time = $shiftEndTime->subMinutes($shiftTime['lunch_hour']);
					//$diffhours = $shift_time->diffInHours($s_date);
					$diff_minutes = $shift_time->diffInMinutes($s_date);
					$diffhours = floatval(($diff_minutes / 60));

					if ($diffhours < config('app.min_hour')) {
						return ['success' => false, 'err_message' => 'Minimum timeslot for ' . $s_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour. Please either increase the shift time or decrease unpaid lunch time'];
					}
					$diff_in_hours = $diffhours;
				} else {
					$diff_in_hours = $e_date->diffInHours($s_date);
				}
				
				//shift duration must be between 3 - 14 hours
				//$diff_in_hours = $e_date->diffInHours($s_date);
				if ($diff_in_hours == 0) {
					return ['success' => false, 'err_message' => 'Minimum timeslot for ' . $s_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
				}
				if ($diff_in_hours < config('app.min_hour')) {
					return ['success' => false, 'err_message' => 'Minimum timeslot for ' . $s_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
				}
				if ($diff_in_hours > config('app.max_hour')) {
					return ['success' => false, 'err_message' => 'Maximum timeslot for ' . $s_date->format('d-m-Y') . ' cannot be more than ' . config('app.max_hour') . ' hours'];
				}
				//check if any of the selected dates clash with themseleves or not
				$check_clashedTime = $this->check_clashed_times($selected_dateTime, $s_date, $e_date);
				if ($check_clashedTime['available'] == true) {
					if ($current_time > $s_date) {
						//delete all the data saved earlier
						if (!isset($shiftTime['process_id'])) {
							TempJobs::where('process_id', $process_id)->delete();
						}
						
						// return error, clashed dates are not allowed to be saved.
						return ['success' => false, 'err_message' => 'Please select another time for ' . $s_date->format('d-m-Y') . ' as the time selected has already been passed.'];
					} else {
						$selected_dateTime[] = $shiftData = ['start_date' => $s_date, 'end_date' => $e_date, 'process_id' => $process_id, 'user_id' => Auth::user()->_id];
						$savedta = new TempJobs($shiftData);
						$savedta->save();
						$counter++;
					}
				} else {
					//delete all the data saved earlier
					TempJobs::where('process_id', $process_id)->delete();
					// return error, clashed dates are not allowed to be saved.
					return ['success' => false, 'err_message' => 'Please select another time for ' . $s_date->format('d-m-Y') . ' as it is clashing with another date'];
				}
			}
			
			//shiftData 
			$processed_data = TempJobs::where('process_id', $process_id)->orderBy('start_date', 'asc')->get()->toArray();
			$processData = array_merge($pastShifts, $processed_data);
			
			
			//renderview to be parsed as json
			$shiftHtml = view('employer.promo.shift_data', compact('processData'))->render();
			return ['success' => true, 'process_id' => $process_id, 'shift_html' => $shiftHtml, 'success' => true];
		} else {
			return ['success' => false, 'message' => 'Please select date and time', 'process' => 0];
		}
	}
	
	/*
	 * Added : 7 dec 2017, shivani Debut infotech
	 * DESC : to remove temp data from temp process
	 * */
	public function remove_temp_process_data($request)
	{
		//process details
		$process_id = $request->process_id;
		$processDetail = TempJobs::where('process_id', $process_id)->get()->toArray();
		$requestedDates = explode(',', $request->check_date);
		$existing_dates = [];
		if (!empty($processDetail)) {
			if (!empty(array_filter($requestedDates))) {

				foreach ($processDetail as $dtl) {
					//check start date 
					$prcssDate = Carbon::parse($dtl['start_date'])->format('d-m-Y');
					if (!in_array($prcssDate, $requestedDates)) {
						//delete this data
						TempJobs::where('_id', $dtl['_id'])->delete();
					} else {
						$existing_dates[] = $prcssDate;
					}
				}
				
				//check if new dates has been selected or not
				if ($requestedDates === array_intersect($requestedDates, $existing_dates) && $existing_dates === array_intersect($existing_dates, $requestedDates)) {
					//no new dates has been selected
				} else {
					//fetch the new dates selected
					$newDates = array_diff($requestedDates, $existing_dates);

					if (!empty(array_filter($newDates))) {
						$shiftTime = [];
						foreach ($newDates as $dte) {
							$shiftTime['start']['time_' . $dte] = $request->start_time;
							$shiftTime['end']['time_' . $dte] = $request->end_time;
						}
						$shiftTime['process_id'] = $process_id;
						$shiftTime['lunch_hour'] = floatval($request->lunch_hour);
						$saveTempData = $this->save_temp_process($shiftTime);
						if ($saveTempData['success'] == false) {
							return ['success' => false, 'err_message' => $saveTempData['err_message']];
						}
					}
				}
			} else {
				//delete process
				TempJobs::where('process_id', $process_id)->delete();
				return ['success' => false, 'err_message' => "Please specify job days for the job.", 'process' => 0];
			}
			$processData = TempJobs::where('process_id', $process_id)->orderBy('start_date', 'asc')->get()->toArray();
			$shiftHtml = view('employer.promo.shift_data', compact('processData'))->render();
			return ['success' => true, 'process_id' => $process_id, 'shift_html' => $shiftHtml];
		} else {
			return ['success' => false, 'err_message' => "Please specify job days for the job.", 'process' => 0];
		}
	}
	
	/*
	 * Added : 29 dec 2017, shivani Debut infotech
	 * DESC : to edit job shifts (with same time)
	 * */
	public function edit_same_job_time($request)
	{
		$requestedDates = explode(',', $request->check_date);
		$shiftTime = $existing_dates = [];
		$common = new CommonRepositary();
		$shiftTime['job_id'] = $request->job_id;
		$shiftTime['lunch_hour'] = floatval($request->lunch_hour);
		if (!empty($request->process_id)) {
			//check job details
			$curTime = Carbon::now();
			$jobDays = TempJobs::where('process_id', $request->process_id)->get()->toArray();

			if (!empty($jobDays)) {
				foreach ($jobDays as $jbDate) {
					//check start date 
					$job_startDate = Carbon::parse($jbDate['start_date'])->format('d-m-Y');
					if (!in_array($job_startDate, $requestedDates)) {
						//delete this data
						TempJobs::where('_id', $jbDate['_id'])->delete();
					} else {
						//$job_endDate = Carbon::parse($jbDate['end_date'])->format('d-m-Y');
						$shiftTime['start']['time_' . $job_startDate] = Carbon::parse($jbDate['start_date'])->format('H:i');
						$shiftTime['end']['time_' . $job_startDate] = Carbon::parse($jbDate['end_date'])->format('H:i');
						$existing_dates[] = $job_startDate;
					}
				}
			}
		} else {
			$job_id = \Crypt::decrypt($request->job_id);
			//check job details
			$curTime = Carbon::now();
			$jobDays = ShiftModel::where('job_id', $job_id)->where('start_date', '>', $curTime)->select('start_date', 'end_date')->get()->toArray();


			if (!empty($jobDays)) {
				foreach ($jobDays as $jbDate) {
					$job_startDate = Carbon::parse($jbDate['start_date'])->format('d-m-Y');
					if (in_array($job_startDate, $requestedDates)) {
						//$job_endDate = Carbon::parse($jbDate['end_date'])->format('d-m-Y');
						$shiftTime['start']['time_' . $job_startDate] = Carbon::parse($jbDate['start_date'])->format('H:i');
						$shiftTime['end']['time_' . $job_startDate] = Carbon::parse($jbDate['end_date'])->format('H:i');
						$existing_dates[] = $job_startDate;
					}
				}
			}
		}
		
		//check differences in new and old dates i.e.. either new dates has been selected or not
		if ($requestedDates === array_intersect($requestedDates, $existing_dates) && $existing_dates === array_intersect($existing_dates, $requestedDates)) {
			//no new dates has been selected
		} else {
			//fetch the new dates selected
			$newDates = array_diff($requestedDates, $existing_dates);

			if (!empty(array_filter($newDates))) {

				foreach ($newDates as $dte) {
					//validate if selected time is future time or not
					$newStrtTime = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($dte . ' ' . $request->start_time)), $_COOKIE['client_timezone']);
					if (Carbon::now() < Carbon::parse($newStrtTime)) {
						$shiftTime['start']['time_' . $dte] = $request->start_time;
						$shiftTime['end']['time_' . $dte] = $request->end_time;
					}
				}
			}
		}

		if (!empty($request->process_id)) {
			TempJobs::where('process_id', $request->process_id)->delete();
		}

		if (!empty($shiftTime['start'])) {
			return $saveTempData = $this->save_temp_process($shiftTime);
		} else {
			return ['success' => true, 'dates' => false];
		}

	}
	
	
	
	/*
	 * Added : 7 dec 2017, shivani debut infotech
	 * DESC : to return data on behalf of process id
	 * */
	public function get_temp_process_details($request)
	{
		$process_id = $request->process_id;
		$data = $data2 = $data3 = [];		 
		//to track past shifts i.e.. not to be deleted (their information will remain visible on employer dashboard.)
		if (!empty($request->job_id)) {

			$job_id = \Crypt::decrypt($request->job_id);
			//check the shifts that has been passed away
			$curTime = Carbon::now();
			$pastShifts = ShiftModel::where('job_id', $job_id)->where('start_date', '<', $curTime)->select('start_date')->get()->toArray();
			if (!empty($pastShifts)) {
				foreach ($pastShifts as $pst) {
					$start_date = date('d-m-Y', strtotime($pst['start_date']));
					$data2[] = $start_date;
					$data3[] = date('d-M l', strtotime($pst['start_date']));
				}
			}
		}
		//get process details 
		$process_detail = TempJobs::where('process_id', $process_id)->get()->toArray();
		if (!empty($process_detail)) {
			foreach ($process_detail as $prcs) {
				$start_date = date('d-m-Y', strtotime($prcs['start_date']));
				$start_time = date('H:i', strtotime($prcs['start_date']));
				$end_time = date('H:i', strtotime($prcs['end_date']));
				$data[] = ['start_date' => $start_date, 'start_time' => $start_time, 'end_time' => $end_time];
				$data2[] = $start_date;
				$data3[] = date('d-M l', strtotime($prcs['start_date']));
			}
			return ['shifts' => $data, 'selected_dates' => $data2, 'format_dates' => $data3];
		} else {
			return ['shifts' => '', 'dates' => ''];
		}
	}
	
	/*
	 * Added : 7 dec 2017, shivani debut infotech
	 * DESC : to get data for edit job
	 * */
	public function get_edit_job_data($id)
	{
		$active = '';
		$title = 'Edit Job';
		$jobs = JobsModel::where(array('_id' => $id, 'is_deleted' => false))->first();
		$repost = false;
		if ($jobs) {
			//check if job can be edited or not (within next 5 minutes)
			$date = Carbon::now();
			$current_date = $date->addMinutes(5)->parse($date)->format('Y-m-d H:i:s');
			$checkJob_edited = $this->check_job($jobs, $current_date);
			if ($checkJob_edited['edit'] == false) {
				//redirect back to dashboard with an error message
				flash()->error('Sorry, you can not edit this job as some employees have already taken action on it.');
				return ['success' => false];
				//return redirect();
			}
			$date = Carbon::now();
			$current_date = $date->addMinutes(5)->parse($date)->format('Y-m-d H:i:s');
			$industIds = User::where(array('status' => true, '_id' => $jobs->user_id))->first();
			$inIds = [];
			if ($industIds) {
				foreach ($industIds->industry as $k => $indust) {
					$inIds[] = $indust['_id'];
				}
			}
			$category = CategoryModel::where(array('status' => true, 'type' => 'category', 'is_deleted' => false))->whereIn('industry_id', $inIds)->pluck('name', '_id')->toArray(); 
			//get all subcategories
			$subcategoryAll = CategoryModel::where(array('status' => true, 'type' => 'subcategory', 'category_id' => $jobs->category[0]['_id']))->pluck('name', '_id')->toArray();
			//get all skills
			$skillsAll = $this->get_skills($jobs->subcategory[0]['_id']);

			$subcategory_All = [];
			//~ foreach($subcategoryAll AS $y => $sbcat)
            //~ {
                //~ $subcategory_All[$sbcat['_id']] = $sbcat['name'];
            //~ }
			$skill_All = [];
			foreach ($skillsAll as $k => $skl) {
				$skill_All[$skl['_id']] = $skl['name'];
			}
			$commission = $this->get_commision($jobs->category[0]['_id']);
			foreach ($jobs->category as $key => $cat) {
				$selcategory[$cat['_id']] = $cat['name'];
			}

			$category = array_merge($category, $selcategory);
			foreach ($jobs->subcategory as $key => $subcat) {
				$subcategory_All[$subcat['_id']] = $subcat['name'];
			}
			$subcategory_All = array_merge($subcategory_All, $subcategoryAll);


			foreach ($jobs->skill as $key => $skil) {
				$skill_All[$skil['_id']] = $skil['name'];
			}

			$date = Carbon::now();
            //$current_date = $date->addHours(1)->parse($date)->format('Y-m-d H:i:s');
			$current_date = $date->addMinutes(5)->parse($date)->format('Y-m-d H:i:s');
			$documetation = Documentation::All();

			return ['documetation' => $documetation, 'repost' => $repost, 'jobs' => $jobs, 'category' => $category, 'current_date' => $current_date, 'active' => $active, 'title' => $title, 'skill_All' => $skill_All, 'subcategory_All' => $subcategory_All, 'commission' => $commission, 'success' => true];
		} else {
			return ['success' => false];
		}
	}
	
	/*
	 * Added : 8 dec 2017, shivani debut infotech
	 * DESC : to update job details
	 * */
	public function update_job($id, $request)
	{
		$common = new CommonRepositary();
		$jobs = JobsModel::where(array('_id' => $id, 'is_deleted' => false))->first();
		if ($jobs) {
			$date = Carbon::now();
			$current_date = $date->addMinutes(5)->parse($date)->format('Y-m-d H:i:s');
			$checkJob_edited = $this->check_job($jobs, $current_date);
			if ($checkJob_edited['edit'] == false) {
				//redirect back to dashboard with an error message
				flash()->error('Sorry, you can not edit this job as some employees have already taken action on it.');
				return ['success' => false, 'update_job' => 'you can not update job'];
				//return redirect();
			}
			$data = $request->all();
			if (!empty($request->employer_id)) {
				$data['user_id'] = $request->employer_id;
			} else {
				$data['user_id'] = Auth::user()->_id;
			}
			$data['lunch_hour'] = null;
			if (!empty($request->lunch_hour)) {
				$data['lunch_hour'] = floatval($request->lunch_hour);
			}
			$data2 = [];
			$requestedDates = explode(',', $request->dates);    
            //check the shifts that has been passed away
			$curTime = Carbon::now();
			$pastShifts = ShiftModel::where('job_id', $id)->where('start_date', '<', $curTime)->orderBy('start_date', 'asc')->get()->toArray();
			$timeSelected = [];
			if (!empty($pastShifts)) {
				foreach ($pastShifts as $pst) {
					$start_date = Carbon::parse($pst['start_date']);
					$end_date = Carbon::parse($pst['end_date']);
					$data2[] = ['start_date' => $start_date, 'end_date' => $end_date];
					//add this date to request dates, because past dates are not being sent by the browser(datepicker.)
					$requestedDates[] = $timeSelected[] = $start_date->format('d-m-Y');
				}
			} 
			
			//check job_dates from temp job data
			if (!empty($request->process_id)) {
				$jobDates = TempJobs::where('process_id', $request->process_id)->select('start_date', 'end_date')->orderBy('start_date', 'asc')->get()->toArray();
				$cur = Carbon::now();
				foreach ($jobDates as $jb_dt) {
					$newDate = Carbon::parse($jb_dt['start_date'])->format('d-m-Y');
					if (in_array($newDate, $requestedDates)) {
						$start_date = Carbon::parse($jb_dt['start_date']);
						$end_date = Carbon::parse($jb_dt['end_date']);
						//if lunch hours has been selected
						if (!empty($data['lunch_hour'])) {
							//subtract lunch time from end time and check shift validity					
							$shift_time = Carbon::parse($jb_dt['end_date'])->subMinutes($data['lunch_hour']);

							$diff_minutes = $shift_time->diffInMinutes($start_date);
							$diffhours = floatval(($diff_minutes / 60));

							if ($diffhours < intval(config('app.min_hour'))) {
								return ['dates' => 'Minimum timeslot for ' . $start_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour. Please either increase the shift time or decrease unpaid lunch time', 'success' => false];
							}
							$totalDuration = $shift_time->diffInHours($start_date);
						} else {
							$totalDuration = $end_date->diffInHours($start_date);
						}

						if ($totalDuration == 0) {
							return ['success' => false, 'status' => false, 'dates' => 'Minimum timeslot for ' . $start_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
						}
						if ($totalDuration < config('app.min_hour')) {
							return ['success' => false, 'status' => false, 'dates' => 'Minimum timeslot for ' . $start_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
						}
						if ($totalDuration > config('app.max_hour')) {
							return ['success' => false, 'status' => false, 'dates' => 'Maximum timeslot for ' . $start_date->format('d-m-Y') . ' cannot be more than ' . config('app.max_hour') . ' hours'];
						}
						
						
						//check each selected date i.e.. selected for rehire job.
						/*if($totalDuration<=12 && $data['job_published_type'] == 2){
							return ['rehire_error'=>"Rehire cannot be used within 12 hours prior to the job's start time.",'success'=>false];
						}*/
						$data2[] = ['start_date' => $start_date, 'end_date' => $end_date];
						$timeSelected[] = $start_date->format('d-m-Y');
					}
				}

			} else {
				//date_default_timezone_set($request->input('timezone'));
				$shiftDetail = $jobs->shifts;
				if (!empty($shiftDetail)) {
					foreach ($shiftDetail as $dtl) {
						//check start date 						
						$shftDate = Carbon::parse($dtl['start_date'])->format('d-m-Y');
						if (in_array($shftDate, $requestedDates)) {
							$start_date = Carbon::parse($dtl['start_date']);
							$end_date = Carbon::parse($dtl['end_date']);
							//if lunch hours has been selected
							if (!empty($data['lunch_hour'])) {
								//subtract lunch time from end time and check shift validity					
								$shift_time = Carbon::parse($dtl['end_date'])->subMinutes($data['lunch_hour']);

								$diff_minutes = $shift_time->diffInMinutes($start_date);
								$diffhours = floatval(($diff_minutes / 60));

								if ($diffhours < config('app.min_hour')) {
									return ['dates' => 'Minimum timeslot for ' . $start_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour. Please either increase the shift time or decrease unpaid lunch time', 'success' => false];
								}
								$totalDuration = $shift_time->diffInHours($start_date);
							} else {
								$totalDuration = $end_date->diffInHours($start_date);
							}

							if ($totalDuration == 0) {
								return ['success' => false, 'status' => false, 'err_message' => 'Minimum timeslot for ' . $s_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
							}
							if ($totalDuration < config('app.min_hour')) {
								return ['success' => false, 'status' => false, 'err_message' => 'Minimum timeslot for ' . $s_date->format('d-m-Y') . ' should be at least ' . config('app.min_hour') . ' hour'];
							}
							if ($totalDuration > config('app.max_hour')) {
								return ['success' => false, 'status' => false, 'err_message' => 'Maximum timeslot for ' . $s_date->format('d-m-Y') . ' cannot be more than ' . config('app.max_hour') . ' hours'];
							}
							$data2[] = ['start_date' => $start_date, 'end_date' => $end_date];
							$timeSelected[] = $shftDate;
						} else {
							//any shift can not be deleted before and after 3 hours of job start time
							$start_date = Carbon::parse($dtl['start_date']);
							$startEarlier = $start_date->subHours(config('app.delete_before_after'))->parse($start_date);
							$s_date = Carbon::parse($dtl['start_date']);
							$startLater = $s_date->addHours(3)->parse($s_date);
							//check difference
							if ($curTime >= $startEarlier && $curTime <= $startLater) {
								return ['dates' => 'Shift for ' . $start_date->format('d-m-Y') . ' can not be removed before and after ' . config('app.delete_before_after') . ' hours of job start time', 'success' => false];
							}
						}
					}
				}
			}
			
			
			//check if time has been selected for all the dates or not.
			if ($requestedDates === array_intersect($requestedDates, $timeSelected) && $timeSelected === array_intersect($timeSelected, $requestedDates)) {
				//continue;
			} else {
				return ['dates' => 'Please select start and end time for all the selected dates', 'time' => $timeSelected, 'success' => false];
			}
			
			//check if number of worker required satisfies with total hired value
			if (!empty($jobs->total_hired) && $data['number_of_worker_required'] < $jobs->total_hired) {
				return ['number_of_worker_required' => 'number of worker required can not be less than total hired employees', 'time' => $timeSelected, 'success' => false];
			}

			$i = 1;
			$data['category'] = CategoryModel::where('_id', $data['job_category'])->get()->toArray();
            //save commision %age, shivani - 28 sep 2017
			$commision = $this->get_commision($data['job_category'], $data['user_id']);
			$data['category_commision'] = $commision['commision'];
			$data['subcategory'] = CategoryModel::where('_id', $data['job_subcategory'])->get()->toArray();
			$data['skill'] = SkillModel::whereIn('_id', $data['skills'])->get()->toArray();
			$startDate = current($data2);
			$endDate = end($data2);


			$data['start_date'] = $startDate['start_date'];
			$data['end_date'] = $endDate['end_date'];

			$start_time = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse(date('Y-m-d') . ' ' . $request->start_date), $_COOKIE['client_timezone']);
			$end_time = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse(date('Y-m-d') . ' ' . $request->end_date), $_COOKIE['client_timezone']);

			$data['start_time'] = Carbon::parse($start_time)->format('H:i');
			$data['end_time'] = Carbon::parse($end_time)->format('H:i');

			$data['job_published_type'] = intval($data['job_published_type']);            
            //to automatically convert a rehire job to automatic if it is posted for upcoming 12 hours.
			$check_duration = $common->check_difference_in_hours($data['start_date']);
			if ($check_duration <= 12 && $data['job_published_type'] == 2) { //this will happen only when job is rehire type.
				$data['job_published_type'] = 1;
			}
			$data['number_of_worker_required'] = intval($data['number_of_worker_required']);
			$data['salary_per_hour'] = doubleval($data['salary_per_hour']);
			ShiftModel::where('job_id', $id)->delete();
			if (empty(strip_tags($data['physical_requirement']))) {
				$data['physical_requirement'] = null;
			}
			if (empty(strip_tags($data['safety_hazards']))) {
				$data['safety_hazards'] = null;
			}
			$data['location'] = ['lng' => doubleval($data['lng']), 'lat' => doubleval($data['lat'])];
			//change status to active from filled, if requirement is raised
			if ($data['number_of_worker_required'] > $jobs->number_of_worker_required) {
				$data['job_status'] = 1;
			}

			if ($jobs->update($data)) {
				$common->setShift($data2, $id);
                //check if any of the employee has been hired for this job (update shift details for those users too)
				if (!empty($jobs->total_hired) && $jobs->total_hired > 0) {
					//check applicants 
					$applicants = JobsapplicationModel::where('job_id', $jobs->_id)->whereIn('job_status', [1, 2])->pluck('_id');
					$this->update_applicant_shifts($jobs->_id, $applicants);
				}
				//remove temporary process details 
				if (!empty($request->process_id)) {
					TempJobs::where('process_id', $request->process_id)->delete();
				}

				$url = url('employer/jobdetail/' . \Crypt::encrypt($id));

				$result = ['status' => true, 'success' => true, 'message' => 'Job updated successfully', 'url' => $url];
			} else {
				flash()->error('Job not updated successfully!');
				$result = ['status' => false, 'success' => true, 'url' => $url];
			}
			return $result;
		} else {
			return ['status' => false, 'success' => true, 'url' => $url];
		}
	}
	
	/*
	 * Added : 8 dec 2017, shivani debut infotech
	 * DESC : to delete job
	 * */
	public function delete_job($jobId)
	{

		$common = new CommonRepositary();
		$job = JobsModel::where(array('_id' => $jobId))->first();
		$start_date = Carbon::parse($job->start_date);
		$startEarlier = $start_date->subHours(config('app.delete_before_after'))->parse($start_date);
		$s_date = Carbon::parse($job->start_date);
		$startLater = $s_date->addHours(config('app.delete_before_after'))->parse($s_date);
		$current_time = Carbon::now();
		$url = url('employer/dashboard');
		if ($job->total_hired > 0 && $current_time >= $startEarlier && $current_time <= $startLater) {
			//it means, current time is between 3 hours before start date and 3 hours later than start date,
			//so job can not be fired at this moment
			flash()->error('Job can not be deleted before and until ' . config('app.delete_before_after') . '. hours of job start time.');
			return ['status' => false, 'message' => 'Job can not be deleted', 'url' => $url];
		} 
        //check if job to be deleted immediately or to be deleted by next day.
		$checkJob = $this->check_job_deletion($job);
		if ($checkJob['delete_job'] == false) { //if job to be deleted after current day's shift end
			//save this job detail to temporary table, and process it later after current day shift end.
			$temp_data['job_id'] = $job->_id;
			$temp_data['delete_at'] = $checkJob['delete_time'];
			TempJobdelete::where('job_id', $job->_id)->delete();
			if (TempJobdelete::create($temp_data)) {
				flash()->success('Job is currently in progress, So it will be deleted after today shift ends.');
				$result = ['status' => true, 'message' => 'Job deleted successfully', 'url' => $url];
			} else {
				flash()->error('Job not deleted successfully!');
				$result = ['status' => false, 'message' => 'Job not deleted successfully!', 'url' => $url];
			}
		} else {
			//check total hours i.e.. worked by applicants on this job
			//$total_job_hours = $common->calculate_total_job_hours($jobId);
			if ($job->update(['is_deleted' => true, 'job_status' => 7])) {
				//delete notification if exists
				if (SendQuizNotification::where('job_id', $jobId)->count())
					SendQuizNotification::where('job_id', $jobId)->delete();
				
				//ShiftModel::where('job_id', $jobId)->delete();
				$url = url('employer/dashboard');            
				//send notification
				$jobSeekers = JobsapplicationModel::where(array('job_id' => $jobId))->where('job_status', 1)->with(['applyjobuser' => function ($q) {
					$q->select('_id');
				}])->get()->toArray();

				if (!empty($jobSeekers)) {
					foreach ($jobSeekers as $jb_seekr) {
						if (!empty($jb_seekr['applyjobuser'])) {
							//update hours
							$job_app_hours = $common->calculate_job_app_hours($jb_seekr['_id']);
							//$totalJobHours = floatval(number_format($job_app_hours['job_app_hours'],2));
							//update data in job application.
							$applicationJob = JobsapplicationModel::where("_id", $jb_seekr['_id'])->first();
							$applicationJob->job_status = 7;
							//$applicationJob->total_hours_worked = $totalJobHours;
							$applicationJob->withdraw_date = $current_time;
							if ($applicationJob->save()) {
								$common->send_notification($jb_seekr['applyjobuser']['_id'], $type = 8, $job_id = null);
							}
						}
					}
				}
				flash()->success('Job deleted successfully');
				$result = ['status' => true, 'message' => 'Job deleted successfully', 'url' => $url];
			} else {
				flash()->error('Job not deleted successfully!');
				$result = ['status' => false, 'message' => 'Job not deleted successfully!', 'url' => $url];
			}
		}
		return $result;
	}
	
	/*
	 * Added : 8 dec 2017
	 * DESC : to cancel an employee from a job
	 * */
	public function cancel_employee($jobApp_id)
	{
		$common = new CommonRepositary();
		$jobApplication_data = JobsapplicationModel::where('_id', $jobApp_id)->with(['job' => function ($q) {
			$q->select('job_status', 'start_date', 'end_date', 'total_hired');
		}, 'job_shift' => function ($q) {
			$q->where('start_date', '<=', Carbon::now());
		}])->first();
		if (!empty($jobApplication_data)) {
			$jobApplication_data->job_shift;
			$jobApplication_data->job_status = 7; //when employer fires/cancel an employee from job
			$start_date = Carbon::parse($jobApplication_data->job->start_date);
			$startEarlier = $start_date->subHours(config('app.delete_before_after'))->parse($start_date);
			$s_date = Carbon::parse($jobApplication_data->job->start_date);
			$startLater = $s_date->addHours(config('app.delete_before_after'))->parse($s_date);
			$current_time = Carbon::now();
			//check if job has been started or not			
			if ($current_time >= $startEarlier && $current_time <= $startLater) {
				//it means, current time is between 3 hours before start date and 3 hours later than start date,
				//so candidate can not be fired at this moment
				return ['success' => false, 'message' => 'Sorry, employee can not be removed before and until ' . config('app.delete_before_after') . ' hours of job start time.'];
			} else {
				//it means, current time is before 3 hours of job start time, so no hours calculation
				if ($current_time < $startEarlier && $current_time < $startLater) {
					$jobApplication_data->total_hours_worked = 0;
				} else {
					//($current_time > $startEarlier && $current_time > $startLater)
					//it means , current time is 3 hours later than start date, hours calculations will be done in this case
					//calculate hours worked upto now
					$app_hours = $common->calculate_job_app_hours($jobApp_id);
					//$workHours = floatval($app_hours['job_app_hours']);				
					//$jobApplication_data->total_hours_worked = $workHours;//set total hours that jobseeker has worked					
				}
			}
			//update job application i.e.. cancel jobseeker application and update data accordingly.
			$jobApplication_data->withdraw_date = $current_time;
			if ($jobApplication_data->save()) {
				//if candidate has been cancelled 3 hours before start time, total hired value will be updated.
				if ($current_time < $startEarlier) {
					//update job details i.e.. total hired value
					$total_hired = $jobApplication_data->job->total_hired - 1;
					if ($total_hired < 0)
						$total_hired = 0;

					JobsModel::where('_id', $jobApplication_data->job_id)->update(['total_hired' => intval($total_hired), 'job_status' => 1]);
				}
				//send notification to jobseeker that he has been fired from the job.
				$common->send_notification($jobApplication_data->jobseeker_id, $type = 9, $job_id = $jobApplication_data->job_id);
				//set flash message
				flash()->success('Employee has been removed from the job');
				return ['success' => true];
			} else {
				flash()->error('Sorry, something went wrong!');
				return ['success' => false];
			}
		} else {
			flash()->error('Sorry, employee not found!');
			return ['success' => false];
		}
	}
	
	/*
	 * Added : 8 dec 2017, shivani debut infotech
	 * DESC : to accept/decline a candidate for a job
	 * */
	public function accept_decline_candidate($request)
	{
		$common = new CommonRepositary();
		$data = $request->all();
		$appliedId = \Crypt::decrypt($data['apply_value']);
		$jobs_app = JobsapplicationModel::where(['_id' => $appliedId, 'job_type' => 0])->where('job_status', 0)->with(['job' => function ($q) {
			$q->select('total_hired', 'number_of_worker_required', 'job_status', 'start_date', 'user_id');
		}])->first(); 
        //job applicant not found, return error message.
		if (empty($jobs_app)) {
			$result = ['status' => false, 'message' => 'Sorry, But looks like employee has already hired/withdrawn from this job.'];
		} else {
			//employee can not accepted/hired after the job has been started.
			if (Carbon::now() > Carbon::parse($jobs_app->job->start_date)) {
				return ['status' => false, 'message' => 'You can not hire employees after the job has been started.'];
			} else {
				//check availablity here
				$jobseekrAvailablity = $common->check_jobseeker_availablity($jobs_app->jobseeker_id, $jobs_app->job_id);
				if ($jobseekrAvailablity['available'] == false && $data['status'] != 'rejected') {
					$result = ['status' => false, 'message' => 'Sorry, this employee has already accepted another job for this time slot.'];
				} else {
					
					//check job details, updated by shivani - to track if jobseeker is working for same employer or not and to check week dates according to that particular job.
					$jobDetail = JobsModel::where('_id', $jobs_app->job_id)->first();
					$jobseeker_hours = $common->getWorkingHourOfJobseeker($jobs_app->job->start_date, $jobs_app->job->user_id, $jobs_app->jobseeker_id);
					$week_hour = $jobseeker_hours + $common->getHourOfJobBeingApplied($jobs_app->job_id);

					if ($jobseeker_hours > 0 && $week_hour >= 44) {
						return ['status' => false, 'message' => 'Sorry, the maximum working hours for this employee has already been reached.', 'hours' => $jobseeker_hours, 'week' => $week_hour];
					}

					$data['job_status'] = ($data['status'] == 'rejected') ? 6 : 1;
					//check if employee has already been hired or not
					if (intval($jobs_app->job_status) == intval($data['job_status'])) {
						$message = (intval($jobs_app->job_status) == intval(1)) ? 'HIRED' : 'DECLINED';
						$result = ['status' => false, 'message' => 'Sorry, this employee was already ' . $message . ' earlier.'];
					} else {
						//check, max worker requirement is fulfilled or not
						if ($jobs_app->job->number_of_worker_required > $jobs_app->job->total_hired) {
							if ($jobs_app->update($data)) {
								//update total_hired value for the job (if job application has been accepted)
								if ($data['job_status'] == 1) {
									//save applicant shift details @application_id, @job_id
									$saveShiftDetails = $common->save_app_shifts($appliedId, $jobs_app->job_id);
									$job_status = $jobs_app->job->job_status;
									if (intval($jobs_app->job->number_of_worker_required) == intval($jobs_app->job->total_hired + 1)) {
										$job_status = 2;
										//decline all other applications related to this job
										JobsapplicationModel::where(['job_id' => $jobs_app->job_id, 'job_status' => 0])->update(['job_status' => 6]);
									}
									//update total hired value for the job
									JobsModel::where('_id', $jobs_app->job_id)->update(['job_status' => $job_status, 'total_hired' => intval($jobs_app->job->total_hired + 1)]);
									//send notification
									$common->send_notification($jobs_app->jobseeker_id, $type = 6, $job_id = $jobs_app->job_id);
									//save temporary entry to send notification before 12 and 1 hour of job start time
									$start_date = Carbon::parse($jobs_app->job->start_date);
									$befor12_hour = $start_date->subHours(12)->parse($start_date);
									$start_date2 = Carbon::parse($jobs_app->job->start_date);
									$befor1_hour = $start_date2->subHours(1)->parse($start_date2);
									$common->notify_before_twelve_hour($jobs_app->job_id, $befor12_hour, $befor1_hour, $jobs_app->jobseeker_id);
								}
								flash()->success('Employee application has been ' . $data['status'] . ' sucessfully');//updated : 1 aug 2017
								$result = ['status' => true, 'message' => 'Employee application has been ' . $data['status'] . ' sucessfully'];
							} else {
								$result = ['status' => false, 'message' => 'Sorry, something went wrong'];
							}
						} else { //max workers are already hired
							$result = ['status' => false, 'message' => 'Sorry, your requirement for maximum workers has already been fulfilled'];
						}
					}
				}
			}
		}
		return $result;
	}
	
	/*
	 * Added : 8 dec 2017, shivani debut infotech
	 * DESC : to return employee shift details
	 * */
	public function employee_shift_details($request)
	{
		$job_id = $request->job_id;
		$employee_id = $request->employee;

		$job_app_id = JobsapplicationModel::where(array('jobseeker_id' => $employee_id, 'job_id' => $job_id, 'is_deleted' => false))->value('_id');


		$now = Carbon::now();
		$shiftDetails = JobsapplicationModel::whereHas('app_shift', function ($q) use ($now) {
			$q->where('end_date', '<', $now);
		})->with(['app_shift' => function ($q) use ($now) {
			$q->where('end_date', '<', $now);
		}])->where(array('jobseeker_id' => $employee_id, 'job_id' => $job_id, 'is_deleted' => false))->whereIn('job_status', [1, 2, 4, 5, 7])->get();

		$job_status = JobsModel::where('_id', $job_id)->select('job_status', 'end_date', 'start_date')->first();
		$common = new CommonRepositary();
		$job_endShiftDate = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($job_status->end_date)));
		$job_startShiftDate = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($job_status->start_date)));
		$user = User::where('_id', $employee_id)->first();
		$name = $user->first_name . " " . $user->last_name;
		$err_message = false;
		if ($shiftDetails->count() == 0 || empty($shiftDetails)) {
			$err_message = true;
		}
		$shift_html = view('employer.promo.employee_shifts', compact('shiftDetails', 'err_message', 'name', 'job_status', 'job_endShiftDate', 'job_startShiftDate', 'employee_id', 'job_id', 'job_app_id'))->render();
		return ['success' => true, 'shift_html' => $shift_html, 'err_status' => false];
	}

	
	
	/*
	 OLD LOGIN FOR UPDATE HOURS 
	 
	 if(!empty($workData)){
			foreach($workData as $shift_id => $hours){
				//check shift details 
				$shiftDetail = ApplicantShift::where('_id',$shift_id)->first();
				//maximum shift hours 
				$shift_start_date = Carbon::parse($shiftDetail->start_date);
				if($hours > config('app.max_hour') || $hours < config('app.min_hour')){
					return ['success'=>false,'message'=>'Total hours for '.$shift_start_date->format('d-M Y').' can not be less than '.config('app.min_hour').' and greater '.config('app.max_hour')];
				}
				//shift hours 
				$shift_hours = $shiftDetail->total_hours_worked;
				$shift_end_date = Carbon::parse($shiftDetail->end_date);
				//check if hours to be added/subtracted from end date
				if($shift_hours > $hours){
					$sub_hour = $shift_hours - $hours;
					//subtract
					$shift_end_date = Carbon::parse($shiftDetail->end_date)->subHour($sub_hour);
				}else if($shift_hours < $hours){
					//add
					$add_hour = $hours - $shift_hours;
					$shift_end_date = Carbon::parse($shiftDetail->end_date)->addHour($add_hour);
				}else{
					//no action, both are equal
					$shift_end_date = Carbon::parse($shiftDetail->end_date);
				}
				
				//check if new end date is greater than current time,
				if($shift_end_date > Carbon::now()){
					return ['success'=>false,'message'=>'Sorry, hours for'.$shift_start_date->format('d-M Y').'  can not be updated for future end date.'];
				}
				
				$shiftDetail->end_date = $shift_end_date;
				$shiftDetail->total_hours_worked = $hours;
				$shiftDetail->save();
				//update hours in job application also.
				$total_hours = ApplicantShift::where('app_id',$shiftDetail->app_id)->pluck('total_hours_worked')->toArray();
				$total_hours = array_sum($total_hours);
				JobsapplicationModel::where('_id',$shiftDetail->app_id)->update(['total_hours_worked'=>floatval($total_hours)]);
				//update job hours too
				$job_hours = JobsapplicationModel::where('job_id',$shiftDetail->job_id)->pluck('total_hours_worked')->toArray();
				$job_hours = array_sum($job_hours);
				JobsModel::where('_id',$shiftDetail->job_id)->update(['total_work_hours'=>floatval($job_hours)]);
			}
			flash()->success('Work hours has been updated'); 
			return ['success'=>true];
		}else{
			flash()->error('Sorry, something went wrong'); 
			return ['success'=>false]; 
		}
	 * */


	public function create_work_hours($request)
	{
		$common = new CommonRepositary();
		$workData = $request->all();

		if (!empty($workData['start_shift'])) {
			foreach ($workData['start_shift'] as $shift_date => $start_time) {

				if (!$common->check_jobseeker_availablity_on_update($request->input('jobseeker_id'), $request->input('job_id'), Carbon::parse(Carbon::parse($shift_date)->format('Y-m-d') . $start_time), Carbon::parse(Carbon::parse($shift_date)->format('Y-m-d') . $workData['end_shift'][$shift_date]))) {
					return ['success' => 0, 'message' => 'Sorry, this employee has already accepted another job for ' . Carbon::parse($shift_date)->format('l - F d, Y')];
				}
							//check shift details 
				$shiftDetail = new ApplicantShift;
				$shiftDetail->job_id = $request->input('job_id');
				$shiftDetail->app_id = $request->input('app_id');
				if ($workData['lunch'][$shift_date]) {
					$shiftDetail->lunch_hour = $workData['lunch'][$shift_date];
				} else {
					$shiftDetail->lunch_hour = 0;
				}
				
				//shift start date
				$newShift_start_date = Carbon::parse($shift_date)->format('Y-m-d');
				$newShift_start_time = $start_time;
				$newShiftStart = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($newShift_start_date . ' ' . $newShift_start_time)), $_COOKIE['client_timezone']);
				//shift end date
				$newShift_end_date = Carbon::parse($shift_date)->format('Y-m-d');
				$newShift_end_time = $workData['end_shift'][$shift_date];
				$newShiftEnd = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($newShift_end_date . ' ' . $newShift_end_time)), $_COOKIE['client_timezone']);
				
				//check if end date goes upto next day or not
				$st_time = Carbon::parse($newShiftStart)->format('H:i');
				$ed_time = Carbon::parse($newShiftEnd)->format('H:i');

				$s_time = Carbon::createFromFormat('H:i', $st_time);
				$e_time = Carbon::createFromFormat('H:i', $ed_time);
				if ($s_time >= $e_time) {
					$newShiftEnd = $newShiftEnd->addDays(1);
				}
				
				//check difference, calculate hours
				$diff_minutes = $newShiftEnd->diffInMinutes($newShiftStart);
				$hours = floatval(($diff_minutes / 60));
				//subtract lunch hours from total hours calculated
				if (!empty($shiftDetail->lunch_hour)) {
					$hours -= $shiftDetail->lunch_hour;
				}
				
				 /*if($hours > config('app.max_hour') || $hours < config('app.min_hour')){
					 return ['success'=>false,'message'=>'Total hours for '.$newShiftStart->format('d-M Y').' can not be less than '.config('app.min_hour').' and greater than '.config('app.max_hour')];
				 }*/

				if ($hours < 0) {
					return ['success' => 0, 'message' => 'Total hours for ' . $newShiftStart->format('d-M Y') . ' can not be less than 0'];
				}


				//check if new end date is greater than current time,
				if ($newShiftEnd > Carbon::now()) {
					return ['success' => 0, 'message' => 'Sorry, hours for ' . $newShiftStart->format('d-M Y') . '  can not be updated for future end date.'];
				}

				$shiftDetail->end_date = $newShiftEnd;
				$shiftDetail->start_date = $newShiftStart;
				$shiftDetail->total_hours_worked = $hours;
				$shiftDetail->save();
				//update hours in job application also.
				$total_hours = ApplicantShift::where('app_id', $shiftDetail->app_id)->pluck('total_hours_worked')->toArray();
				$total_hours = array_sum($total_hours);
				JobsapplicationModel::where('_id', $shiftDetail->app_id)->update(['total_hours_worked' => floatval($total_hours)]);
				//update job hours too
				$job_hours = JobsapplicationModel::where('job_id', $shiftDetail->job_id)->pluck('total_hours_worked')->toArray();
				$job_hours = array_sum($job_hours);
				JobsModel::where('_id', $shiftDetail->job_id)->update(['total_work_hours' => floatval($job_hours)]);
			}

		}
	 	//flash()->success('Work hours has been updated'); 
		return ['success' => 1];
	}
	/*
	 * Added : 13 dec 2017, shivani Debut infotech
	 * DESC : to update employee hours for a particular shift
	 * */
	public function update_work_hours($request)
	{
		$workData = $request->all();
		$common = new CommonRepositary();
		if (!empty($workData['shiftstart'])) {
			foreach ($workData['shiftstart'] as $shift_id => $start_time) {

				//check shift details 
				$shiftDetail = ApplicantShift::where('_id', $shift_id)->first();

				if (!$common->check_jobseeker_availablity_on_update($request->input('jobseeker_id'), $request->input('job_id'), Carbon::parse($shiftDetail->start_date), Carbon::parse($shiftDetail->end_date))) {

					return ['success' => false, 'message' => 'Sorry, this employee has already accepted another job for ' . Carbon::parse($shiftDetail->start_date)->format('l - F d, Y')];
				}




				//shift start date
				$newShift_start_date = Carbon::parse($shiftDetail->start_date)->format('Y-m-d');
				$newShift_start_time = $start_time;
				$newShiftStart = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($newShift_start_date . ' ' . $newShift_start_time)), $_COOKIE['client_timezone']);
				//shift end date
				$newShift_end_date = Carbon::parse($shiftDetail->start_date)->format('Y-m-d');
				$newShift_end_time = $workData['shiftend'][$shift_id];
				$newShiftEnd = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($newShift_end_date . ' ' . $newShift_end_time)), $_COOKIE['client_timezone']);
				
				//check if end date goes upto next day or not
				$st_time = Carbon::parse($newShiftStart)->format('H:i');
				$ed_time = Carbon::parse($newShiftEnd)->format('H:i');

				$s_time = Carbon::createFromFormat('H:i', $st_time);
				$e_time = Carbon::createFromFormat('H:i', $ed_time);
				if ($s_time >= $e_time) {
					$newShiftEnd = $newShiftEnd->addDays(1);
				}
				
				//check difference, calculate hours
				$diff_minutes = $newShiftEnd->diffInMinutes($newShiftStart);
				$hours = floatval(($diff_minutes / 60));
				//subtract lunch hours from total hours calculated
				if (!empty($shiftDetail->lunch_hour)) {
					$hours -= $shiftDetail->lunch_hour;
				}
				
				 /*if($hours > config('app.max_hour') || $hours < config('app.min_hour')){
					 return ['success'=>false,'message'=>'Total hours for '.$newShiftStart->format('d-M Y').' can not be less than '.config('app.min_hour').' and greater than '.config('app.max_hour')];
				 }*/

				if ($hours < 0) {
					return ['success' => false, 'message' => 'Total hours for ' . $newShiftStart->format('d-M Y') . ' can not be less than 0'];
				}



				
				//check if new end date is greater than current time,
				if ($newShiftEnd > Carbon::now()) {
					return ['success' => false, 'message' => 'Sorry, hours for' . $newShiftStart->format('d-M Y') . '  can not be updated for future end date.'];
				}

				$shiftDetail->end_date = $newShiftEnd;
				$shiftDetail->start_date = $newShiftStart;
				$shiftDetail->total_hours_worked = $hours;
				$shiftDetail->save();
				//update hours in job application also.
				$total_hours = ApplicantShift::where('app_id', $shiftDetail->app_id)->pluck('total_hours_worked')->toArray();
				$total_hours = array_sum($total_hours);
				JobsapplicationModel::where('_id', $shiftDetail->app_id)->update(['total_hours_worked' => floatval($total_hours)]);
				//update job hours too
				$job_hours = JobsapplicationModel::where('job_id', $shiftDetail->job_id)->pluck('total_hours_worked')->toArray();
				$job_hours = array_sum($job_hours);
				JobsModel::where('_id', $shiftDetail->job_id)->update(['total_work_hours' => floatval($job_hours)]);
			}
			//flash()->success('Work hours has been updated'); 
			return ['success' => true];
		} else {
			//flash()->error('Sorry, something went wrong'); 
			//return ['success'=>false]; 
			return ['success' => true];
		}
	}
	
	/*
	 * Added : 13 dec 2017, shivani Debut infotech
	 * DESC : to return users for rehire job
	 * */
	public function get_rehire_users($employer_id, $view = true, $request, $admin = false)
	{
		$current_date = Carbon::now();
		$end_date = Carbon::now()->subDays(30);
		$offerlist = JobsModel::with(['jobapplied' => function ($q) {
			$q->whereIn('job_status', [4, 5, 7])->where('total_hours_worked', '!=', null)->get();
		}])->where(array('user_id' => $employer_id))->where('end_date', '<=', $current_date)->where('end_date', '>=', $end_date)->select('_id')->get();
		$data = [];
		$i = 1;
		foreach ($offerlist as $k => $user) {
			foreach ($user->jobapplied as $row) {
				$data[] = $row->jobseeker_id;
			}
		}
		$userIds = array_unique($data);

		$users = User::where(array('is_deleted' => false, 'role' => 2, 'approved' => 1))->whereIn('_id', $userIds)->select('first_name', 'last_name', '_id', 'image', 'rating', 'approved', 'email', 'skills')->orderBy('rating', 'asc')->get();
		$invitedUsers = [];
		if ($view == true) {
			$jobSkills = [];
			if (!empty($request->skills)) {

				$jobSkills = $request->skills;
			} else {
				if (!empty($request->job_id)) {
					$jobSkills = JobsModel::where('_id', $request->job_id)->value('skills');
					$invitedUsers = JobsapplicationModel::where('job_id', $request->job_id)->pluck('jobseeker_id')->toArray();
				}
			}
			$userListHtml = view('models.rehireUserList', ['users' => $users, 'invitedUsers' => $invitedUsers, 'admin' => $admin, 'jobSkills' => $jobSkills])->render();
			return ['user_list' => $userListHtml];
		} else {
			return $users;
		}
	}
	
	/*
	 * Added : 13 dec 2017, shivani debut infotech
	 * DESC : to return job details
	 * */
	public function get_job_details($jobid)
	{
		$active = '';
		$title = 'Job Details';
		$job_post = JobsModel::with(['jobapplied' => function ($q) {
			$q->where('is_deleted', false);
		}, 'jobshifts'])->where('_id', $jobid)->first();
		if ($job_post) {
			$date = Carbon::now();
            //add one hour to current time
			$current_date = $date->addHours(1)->parse($date)->format('Y-m-d H:i:s');
			//======= end 29 aug 2017            
			$documetation = Documentation::All();
			$user_ids = $accepted_users = [];
            //selected jobseekers
			if ($job_post->job_published_type == 2) {
				$jobPost_arr = $job_post->toArray();
				if (!empty($jobPost_arr['jobapplied'])) {
					foreach ($jobPost_arr['jobapplied'] as $jobseeker) {
						if ($jobseeker['job_status'] == 1) {
							$accepted_users[] = $jobseeker['jobseeker_id'];
						} else {
							$user_ids[] = $jobseeker['jobseeker_id'];
						}
					}
				}
			}
			//rehire candidate list
			$rehire_user = $this->getRehireList($accepted_users);
			$users = $rehire_user['users'];
			return ['job_post' => $job_post, 'current_date' => $current_date, 'active' => $active, 'title' => $title, 'documetation' => $documetation, 'users' => $users, 'user_ids' => $user_ids, 'accepted_users' => $accepted_users];           
            //return view('employer.promo.job_detail',compact('job_post','job_edit','job_dates','current_date','active','title','documetation','users','user_ids','accepted_users'));
		}
	}
	
	/*
	 * Added : 14 dec 2017, shivani debut infotech
	 * DESC : to send rehire invitation to employees
	 * */
	public function rehire_invitation($request)
	{
		$common = new CommonRepositary();
		$data = $request->all();
		$job_id = \Crypt::decrypt($data['job_id']); 
		//get employer id from job model
		$employer_id = JobsModel::where('_id', $job_id)->value('user_id');
		//remove job applications corresponding to job id received which were not accepted as yet
		$jobApplications = JobsapplicationModel::where('job_id', $job_id)->where('job_status', 0)->delete();
		foreach ($data['offer'] as $k => $val) {
			$data1['job_id'] = $job_id;
			$data1['jobseeker_id'] = $val;
			$data1['employer_id'] = $employer_id;
			$data1['job_status'] = 0; //for offered job
			$data1['job_type'] = 2;
			$data1['is_deleted'] = false;
			$data1['is_applied'] = true;
			$data1['is_viewed'] = false;//(to track if jobseeker has checked this job or not)
			//for send notification   
			if (JobsapplicationModel::where('job_id', $job_id)->where('jobseeker_id', $data1['jobseeker_id'])->count() == 0) {
				JobsapplicationModel::create($data1);
				$common->send_notification($data1['jobseeker_id'], $type = 5, $job_id = $data1['job_id']);
			}
		}
		//total applications for this job
		$totalApplications = JobsapplicationModel::where('job_id', $job_id)->count();
		//update total applied value for the job
		if (JobsModel::where('_id', $job_id)->update(['total_applied' => $totalApplications])) {
			flash()->success('Rehire invitation has been sent successfully!');
			return ['success' => true];
		} else {
			flash()->error('Sorry, something went wrong!');
			return ['success' => false];
		}
	}
	
	/*
	 * Added : 23 jan 2018, shivani Debut infotech
	 * DESC : to assign interview timeslots to an employee for a job.
	 * */
	public function interview_slots($request, $application_id)
	{
		//check job application
		$job_app = JobsapplicationModel::where('_id', $application_id)->first()->toArray();
		$common = new CommonRepositary();
		if (!empty($job_app)) {
			if (JobInterview::where('application_id', $application_id)->count() == 3) {
				return ['status' => false, 'message' => 'you have already assigned the interview timeslots to this employee'];
			} else {
				//first slot
				$slots['start'][1] = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($request->start_one)), $_COOKIE['client_timezone']);
				$slots['end'][1] = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($request->end_one)), $_COOKIE['client_timezone']);
				//second slot
				$slots['start'][2] = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($request->start_two)), $_COOKIE['client_timezone']);
				$slots['end'][2] = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($request->end_two)), $_COOKIE['client_timezone']);
				//third slot
				$slots['start'][3] = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($request->start_three)), $_COOKIE['client_timezone']);
				$slots['end'][3] = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($common->commonDate($request->end_three)), $_COOKIE['client_timezone']);
				$counter = 1;

				foreach ($slots['start'] as $slot) {
					$timeslot = new JobInterview;
					$timeslot->status = "assigned";
					$timeslot->start_date = Carbon::parse($slot);
					$timeslot->end_date = Carbon::parse($slots['end'][$counter]);
					$timeslot->application_id = $application_id;
					$timeslot->save();
					$counter++;
				}
				
				//set timeslot flag = assigned for job application
				if (JobsapplicationModel::where('_id', $application_id)->update(['timeslot_assigned' => true])) {
					//notify employee i.e.. interview timeslots has been assigned.
					$common->send_notification($job_app['jobseeker_id'], $type = 6, $job_id = $job_app['job_id']);
				}
				return ['status' => true, 'message' => 'Interview timeslots assigned successfully'];
			}
		} else {
			return ['status' => false, 'message' => 'Sorry, Application not found.'];
		}
	}

	/*delete shift day in modal*/

	public function deleteShiftModel($id)
	{
		$app = ApplicantShift::where('_id', $id);
		$job = JobsapplicationModel::where('_id', $app->first()->app_id)->first();
		$hrs = $job->total_hours_worked;
		$final_hrs = $hrs - $app->first()->total_hours_worked;
		$job->update(['total_hours_worked' => $final_hrs]);
		JobsModel::where('_id', $app->first()->job_id)->update(['total_work_hours' => $final_hrs]);
		$app->delete();
		flash()->success('Timesheet updated successfully');
	}
}

