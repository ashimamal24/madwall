<?php
namespace App\Http\Repositary;

use App\Model\User;
use App\Model\JobsapplicationModel;
use App\Model\ShiftModel;
use App\Model\Token;
use JWTAuth;
use Twilio;
use DateTime;
use DateTimeZone;
use App\Jobs\SendOtpEmail;
use Auth;//27july2017, shivani
use App\Model\JobsModel;
use App\Model\Blog;
use App\Model\NotificationModel;
use App\Model\Setting;
use App\Model\UserRatings;
use Carbon\Carbon;
use App\Model\EmailVerification;
use App\Model\SendQuizNotification;
use App\Model\EmailTemplate;
use App\Model\ApplicantShift;
use App\Model\Comment;

use Laravel\Lumen\Routing\DispatchesJobs;
use Queue;

use Config;
use \Firebase\JWT\JWT;


class CommonRepositary
{
	
    /*==================================================================================================
    Function for genarating random otp for a user 
    ====================================================================================================
	 */

	public function randomGenerator($length = 6)
	{
		$str = "";
		$characters = array_merge(range('A', 'Z'), range('0', '9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		if (User::where('otp', $str)->exists()) {
			randomGenerator();
		}
		return $str;
	}
	
	/*
	 * Added on : 27 july 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to return unique id i.e.. user readable
	 * */
	public function get_uniq_id($type)
	{
		//random number
		$finalNumber = '';
		$idntfier = 'EMP';//for employee
		if ($type == 'job') {
			//first four letters of employer_name
			$idntfier = strtoupper(substr(Auth::user()->first_name, 0, 4));	
			//check if it already exist in database
			$identifierExist = true;
			while ($identifierExist == true) {
				$nmbr = mt_rand(100000, 999999);
				if (JobsModel::where('job_id', $idntfier . $nmbr)->count() == 0) {
					$identifierExist = false;
					$finalNumber = $idntfier . $nmbr;
				}
			}
		} else {
			if ($type == 'empr') {
				//if type is employer
				$idntfier = 'EMPR';
			}
			//check if it already exist in database
			$identifierExist = true;
			while ($identifierExist == true) {
				$nmbr = mt_rand(100000, 999999);
				if (User::where('user_id', $idntfier . date("y") . $nmbr)->count() == 0) {
					$identifierExist = false;
					$finalNumber = $idntfier . date("y") . $nmbr;
				}
			}
		}
		return $finalNumber;
	}

    /*==================================================================================================
    Function for genarating random otp for a user 
    ====================================================================================================
	 */

	public function randomGeneratorRefferal($length = 10)
	{
		$str = "";
		$characters = array_merge(range('A', 'Z'), range('0', '9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		if (User::where('refferal_code', $str)->exists()) {
			randomGeneratorRefferal();
		}
		return $str;
	}

	/*==================================================================================================
    Function for managing token for a user on one device 
    ====================================================================================================
	 */
	public function getManageToken($token, $user)
	{

		$token_get = Token::where('user_id', $user)->first();

		if ($token_get) {

			try {
				JWTAuth::invalidate($token_get->token);

			} catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			}
			finally {
				$token_get->delete();
				$token_new = new Token(['user_id' => $user, 'token' => $token]);
				return $token_new->save();
			}
		} else {
			$token_new = new Token(['user_id' => $user, 'token' => $token]);
			return $token_new->save();
		}
	}

    /*=============================================================
    function for invalidatetoken on declined of user
    =================================================================*/
	public function getDeclinedToken($userid)
	{

		$token_get = Token::where('user_id', $userid)->first();
		if ($token_get) {
           //JWTAuth::invalidate($token_get->token);
			return $token_get->update(array('decline' => true));
		}
	}

    /*==================================================================================================
    Function for sending text message to anyone
    ====================================================================================================
	 */
	public function sendText($phone, $message)
	{
		try {
			Twilio::message($phone, $message);
		} catch (\Services_Twilio_RestException $e) {
			return true;
		}
	}
    
   
	/*
	 * Updated : 11 sep 2017
	 * Updated by : shivani, To implement ratings logic.
	 * */
	public function AdjustRating($rating, $userId, $job_id)
	{
		$id = \Crypt::decrypt($userId);
		$job_id = \Crypt::decrypt($job_id);
		$userRating = User::where(array('_id' => $id))->value('rating');
		$currentDate = Carbon::now();
		$monthStart = $currentDate->copy()->startOfMonth();
		$monthEnd = $currentDate->copy()->endOfMonth();
		$key = $rating . '_star';
		//rating value from settings table
		$rating_val = Setting::where(['type' => 'employer_rating', 'key' => $key])->first();
		//check how many time this rating has been assigned to the user
		$checkRating = UserRatings::where('type_id', $id)->where('type', 'job_rating')->where('rating', $rating)->where('created_at', '>=', $monthStart)->where('created_at', '<=', $monthEnd)->count();
		//delete record, if already assigned rating by employer
		if ($usrRate = UserRatings::where(['type' => 'job_rating', 'type_id' => $id, 'job_id' => $job_id])->first()) {
			$usrRate->delete();
		}
		if (($rating == 5 || $rating == 4) && $checkRating < 2) {
			//save details to user_ratings and update avg rating of user
			UserRatings::create(['type' => 'job_rating', 'type_id' => $id, 'rated_by_id' => Auth::user()['_id'], 'job_id' => $job_id, 'rating' => intval($rating), 'points' => $rating_val->value]);
			//update avg rating
			if ($userRating < 5) {
				$userRating += $rating_val->value;
				if ($userRating > 5)
					$userRating = 5;

				if ($userRating < 0)
					$userRating = 0;


				User::where('_id', $id)->update(['rating' => floatval(number_format($userRating, 2))]);
			}
			JobsapplicationModel::where('jobseeker_id', $id)->where('job_id', $job_id)->update(['rating' => true]);
			return true;
		} else {
			
			//save details to user_ratings and update avg rating of user
			UserRatings::create(['type' => 'job_rating', 'type_id' => $id, 'rated_by_id' => Auth::user()['_id'], 'job_id' => $job_id, 'rating' => intval($rating), 'points' => $rating_val->value]);
			$userRating -= $rating_val->value;
			if ($rating != 3 || $rating != 5 || $rating != 4) {
				if ($userRating < 0)
					$userRating = 0;

				User::where('_id', $id)->update(['rating' => floatval(number_format($userRating, 2))]);
			}
			JobsapplicationModel::where('jobseeker_id', $id)->where('job_id', $job_id)->update(['rating' => true]);
			return true;
		}
		return false;
	}

    /*
    Updated : 3 aug 2017
    Updated by : shivani
    DESC : to re-write code for send notification
	 */
	public function send_notification($user_id, $type, $job_id = null)
	{

	//	print_R($user_id);die;
		$device_token = User::where(array('_id' => $user_id))->value('device_token');
		$notification = User::where(array('_id' => $user_id))->value('notification');
        //if user's notification setting is turned off,
		if ($notification == false) {
			return false;
		}

		if ($device_token) {
            //notification message body
			$fields['data']['title'] = $msg['title'] = 'MadWall';
			$fields['to'] = $device_token;
			$fields['priority'] = 'high';
			$fields['data']['fcm_status'] = 'OK';
			$msg['sound'] = 'default';
			if ($type == 1) {//when jobseeker is approved by admin
				$msg['body'] = $fields['data']['body'] = "Your profile has been approved by MadWall.";
				$fields['notification'] = $msg;
				$fields['data']['notificationtype'] = '2';
			} else if ($type == 2) {//when jobseeker is declined by admin
				$msg['body'] = $fields['data']['body'] = "Your profile has been declined by MadWall.";
				$fields['notification'] = $msg;
				$fields['data']['notificationtype'] = '3';
			} else if ($type == 3) {//when jobseeker is blocked by admin
				$msg['body'] = $fields['data']['body'] = "Your profile has been blocked by MadWall.";
				$fields['notification'] = $msg;
			} else if ($type == 'reactivate') {//when jobseeker is re-activated  by admin
				$msg['body'] = $fields['data']['body'] = "Your profile has been enabled by MadWall.";
				$fields['notification'] = $msg;
			} else if ($type == 4) { //when time slots are assigned
				$msg['body'] = $fields['data']['body'] = "3 time slots have been assigned. Please select one of them.";
				$fields['notification'] = $msg;
				$fields['data']['notificationtype'] = '1';
			} else if ($type == 5) {
                //when jobseeker is rehired for a job
				$msg['body'] = $fields['data']['body'] = 'Hi! You have a new offered Job';
				$fields['data']['job_type'] = '1';
				$fields['data']['is_from_deeplink'] = true;
				$fields['data']['job_id'] = $job_id;
				$fields['notification'] = $msg;
			} else if ($type == 6) {
                //when jobseeker application is accepted by employer
				$msg['body'] = $fields['data']['body'] = 'Congrats! You have been hired';
				$fields['data']['job_type'] = '2';
				$fields['data']['is_from_deeplink'] = true;
				$fields['data']['job_id'] = $job_id;
				$fields['data']['sound'] = 'default';
				$fields['notification'] = $msg;
			} else if ($type == 8) {
				//send notification to jobseekers when a job is deleted ($type == 8)
				$job_name = JobsModel::where('_id', $job_id)->value('title');
				$msg['body'] = $fields['data']['body'] = $job_name . " job has been deleted from madwall";
				$fields['notification'] = $msg;
			} else if ($type == 9) {
				//send notification to jobseekers when he is fired/cancel from a job by an employer ($type == 8)
				$job_name = JobsModel::where('_id', $job_id)->value('title');
				$msg['body'] = $fields['data']['body'] = "You have been removed from the " . $job_name . " job";
				$fields['notification'] = $msg;
			} else if ($type == 10) {
				//send notification to jobseekers when a new job is posted in his area
				$msg['body'] = $fields['data']['body'] = "A new job has been posted in your area";
				$fields['data']['job_type'] = '2';
				$fields['data']['is_from_deeplink'] = true;
				$fields['data']['job_id'] = $job_id;
				$fields['data']['sound'] = 'default';
				$fields['notification'] = $msg;
			} else if ($type == 112) {
				//send notification to jobseeker before 12 hours of job start time
				$msg['body'] = $fields['data']['body'] = 'Your job is going to start in next 12 hours';
				$fields['data']['job_type'] = '2';
				$fields['data']['is_from_deeplink'] = true;
				$fields['data']['job_id'] = $job_id;
				$fields['data']['sound'] = 'default';
				$fields['notification'] = $msg;
			} else if ($type == 111) {
				//send notification to jobseeker before 1 hours of job start time
				$msg['body'] = $fields['data']['body'] = 'Your job is going to start in next 1 hour';
				$fields['data']['job_type'] = '2';
				$fields['data']['is_from_deeplink'] = true;
				$fields['data']['job_id'] = $job_id;
				$fields['data']['sound'] = 'default';
				$fields['notification'] = $msg;
			} else if ($type == 11) {
				//send notification to jobseeker about job updation
				$msg['body'] = $fields['data']['body'] = 'Your job has been updated, please check details';
				$fields['data']['job_type'] = '2';
				$fields['data']['is_from_deeplink'] = true;
				$fields['data']['job_id'] = $job_id;
				$fields['data']['sound'] = 'default';
				$fields['notification'] = $msg;
			} else if ($type == 12) {
				//send notification regarding app updation
				$msg['body'] = $fields['data']['body'] = "A new & improved version of the MadWall application is available. Update your App now!";
				$fields['notification'] = $msg;
			} else {
                //send notification to jobseeker after 48 hours of failed health quiz attempt ($type == 7)
				$msg['body'] = $fields['data']['body'] = 'Hi, you can re-attempt to pass madwall health quiz';
				$fields['notification'] = $msg;
			}

            //send notification code
			//$headers = array('Authorization: key=' . env('API_ACCESS_KEY'), 'Content-Type: application/json');
			//print_R($headers);die;
			$headers = array('Authorization: key=AAAAvBXXWQY:APA91bEyUOlAZ8N0--HrsfumQJ98ryyLl2VfgmE-3DdftykJjDyQWAOLldovEeOgmg3a6sZNcOvJpG66SddmFT3E1zrjlAurQEY-ooydwUx3yJtgRGA4GsO90o1TLc-teDXYJARquKtp', 'Content-Type: application/json');
            #Send Reponse To FireBase Server    
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
            #Echo Result Of FireBase Server
			return $result;
		} else {
            //no device token found
			return ['success' => false, 'message' => 'No device token found'];
		}
	}


	#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#


	public function send_notifications($user_id, $type, $message)
	{

		$device_token = User::where(array('_id' => $user_id))->value('device_token');
		$notification = User::where(array('_id' => $user_id))->value('notification');

		/*
		$user = User::where(array('_id' => $user_id))->first();

		$notification = $user['notification'];
		$device_type = $user['device_type'];

		if($user['device_type'] ==='android'){

			$device_token = $user['device_token'];

		}else{

			$device_token = $user['pushkit_token'];
		}
		*/
		

        //if user's notification setting is turned off,
		if ($notification == false) {
			return false;
		}

		if ($device_token) {
            //notification message body
			$fields['data']['title'] = $msg['title'] = 'MadWall';
			$fields['to'] = $device_token;
			$fields['priority'] = 'high';
			$fields['data']['fcm_status'] = 'OK';
			$msg['sound'] = 'default';

			if ($type == 21) {
				# Opentok Call
				//print_R($message);die;
				
				$msg['body'] = $fields['data']['body'] = "Incomming Call...";
				$msg['username'] = $message['username'];
				$msg['appkey'] = $message['appkey'];
				$msg['sessionId'] = $message['sessionId'];
				$msg['opentokToken'] = $message['token'];
				$msg['notificationtype'] =  $type;
				$msg['tag'] =  rand(10,99);
				$fields['data'] = $msg;
				$fields['notification'] = $msg;

				//print_R($msg);die;
		
			}else if ($type == 22) {
				# Opentok Call
				//print_R($message);die;
				
				$msg['body'] = $fields['data']['body'] = "Call Declined...";
				$msg['notificationtype'] =  $type;
				$msg['tag'] =  rand(10,99);
				$fields['data'] = $msg;
				$fields['notification'] = $msg;

				//print_R($msg);die;

			}else if ($type == 23) {
				# Opentok Call
				//print_R($message);die;
				
				$msg['body'] = $fields['data']['body'] = "Your call has been successfully completed. Please wait for the Madwall Team to contact you further.";
				$msg['notificationtype'] =  $type;
				$msg['tag'] =  rand(10,99);
				$fields['data'] = $msg;
				$fields['notification'] = $msg;

				//print_R($msg);die;	
		
			} else {
                //send notification to jobseeker after 48 hours of failed health quiz attempt ($type == 7)
				$msg['body'] = $fields['data']['body'] = 'Hi, you can re-attempt to pass madwall health quiz';
				$fields['notification'] = $msg;
			}

            //send notification code
			//$headers = array('Authorization: key=' . env('API_ACCESS_KEY'), 'Content-Type: application/json');
			//print_R($headers);die;
			$headers = array('Authorization: key=AAAAvBXXWQY:APA91bEyUOlAZ8N0--HrsfumQJ98ryyLl2VfgmE-3DdftykJjDyQWAOLldovEeOgmg3a6sZNcOvJpG66SddmFT3E1zrjlAurQEY-ooydwUx3yJtgRGA4GsO90o1TLc-teDXYJARquKtp', 'Content-Type: application/json');
            #Send Reponse To FireBase Server    
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);
            #Echo Result Of FireBase Server
			return $result;
		} else {
            //no device token found
			return ['success' => false, 'message' => 'No device token found'];
		}
	}

	#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#


	public function getAdminDetail()
	{
		return $data = User::where(array('role' => 1))->first();
	}    
    //function for creating a groups of dates with concating time and date

	public function commonDate($value)
	{
        // $date = new \DateTime($value);
		$date = new DateTime($value, new DateTimeZone('UTC'));
		$date->setTimeZone(new DateTimeZone($_COOKIE['client_timezone']));
		$date->setTimezone(new \DateTimeZone('UTC'));
		return $formatted_date = $date->format("Y-m-d\TH:i:s\Z");
        //return $formatted_date = $date->format(DateTime::ATOM);

        //echo "<br />Date2: ". $formatted_date."<br />";
	}


	public function setShift($dates = null, $job_id = null)
	{

		foreach ($dates as $k => $shift) {
			$shifts['job_id'] = $job_id;
			$shifts['job_id_ob'] = new \MongoDB\BSON\ObjectID($job_id);
			$shifts['start_date'] = $shift['start_date'];
			$shifts['end_date'] = $shift['end_date'];
			$shifts['status'] = true;
          //ShiftModel::create($shifts);
			$savedta = new ShiftModel($shifts);
			$savedta->save();
		}
	}


	public function getAllNotification()
	{
		$notification = NotificationModel::where(array('status' => true, 'to' => $this->auth->user()->_id))->get();
		$not_count = count($notification);
		$content = view('employer.promo.ajaxnotification', compact('notification', 'not_count'))->render();
		$result = ['not_count' => $not_count, 'notification' => $notification];
		return $result;
	}
    
    //to save notification to be shown at admin and employer respectively.
	public function saveNotification($from, $to, $type, $target, $target_id = null, $title)
	{
		$notification['from'] = $from;
		$notification['to'] = $to;
		$notification['type'] = intval($type); // 1->Admin 2->employer
		$notification['target'] = intval($target); // 1->Upload Docs,2->Employer Signup, 3-> Contactus, 4->when admin approve employer profile, 5-> when admin declined profile
		$notification['target_id'] = (!empty($target_id)) ? $target_id : null;
		$notification['status'] = true;
		$notification['title'] = $title;
		$notification_obj = new NotificationModel($notification);
		$notification_obj->save();
		return;
	}
	
	/*
	 * Added on : 28 aug 2017, shivani
	 * DESC : to get difference between current time and passed value
	 * */
	public function check_difference_in_hours($startTime)
	{
		$currentTime = Carbon::now();
		$start_time = Carbon::parse($startTime);
		return $start_time->diffInHours($currentTime);
	}
	
    /*
	 * Added on : 30 aug 2017, shivani
	 * DESC : to check jobseeker availablity
	 * */
	public function check_jobseeker_availablity($jobseeker_id, $job_id)
	{
        //check start/end date for the job (for which we are checking availablity)
		$job_detail = JobsModel::where('_id', $job_id)->select('start_date', 'end_date')->with(['jobshifts'])->first()->toArray();    
        //check all jobapplications for jobseeker which are approved/accepted
		$jobApplications = JobsapplicationModel::where('jobseeker_id', $jobseeker_id)->whereIn('job_status', [1, 2, 4])->with(['job_detail' => function ($q) {
			$q->select('start_date', 'end_date');
		}, 'app_shift'])->select('jobseeker_id', 'job_id', 'job_status')->get()->toArray();
        //initial status for availablity
		$available = true;
        //loop through each application, and check all possible conditions and return availabity 
		if (!empty($jobApplications)) {
			foreach ($jobApplications as $job_app) {
				if (!empty($job_app['app_shift'])) {
					foreach ($job_app['app_shift'] as $jobShift) {
						$app_start = Carbon::parse($jobShift['start_date']);
						$app_end = Carbon::parse($jobShift['end_date']);    
                        //compare these dates with each shift of the job (for which we are checking availablity)    
						foreach ($job_detail['jobshifts'] as $shft) {
							$job_start = Carbon::parse($shft['start_date']);
							$job_end = Carbon::parse($shft['end_date']);
							if ($job_start == $app_start && $job_start < $app_end && $job_end > $app_start && $job_end == $app_end) {
								return $available = false;
							} else if ($job_start < $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
								return $available = false;
							} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end > $app_end) {
								return $available = false;
							} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
								return $available = false;
							} else if ($job_start < $app_start && $job_start < $app_end && $job_end > $app_start && $job_end > $app_end) {
								return $available = false;
							} else if ($job_start == $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
								return $available = false;
							} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end == $app_end) {
								return $available = false;
							} else {
								$available = true;
                                //check that start dates can not be same
								$fgh = $job_start;
								$job_start_date = $job_start->format('Y-m-d');
								$job_end_date = $job_end->format('Y-m-d');
								$app_start_date = $app_start->format('Y-m-d');
								$app_end_date = $app_end->format('Y-m-d');
								if ($job_start_date == $app_start_date && $job_end_date == $app_end_date && $job_end_date == $app_start_date && $job_end_date == $app_end_date) {
									return $available = false;
								}

								if ($job_end_date == $app_end_date) {
									return $available = false;
								}

							}
						}
					}
				}
			}
		} else {
			$available = true;
		}

		return ['available' => $available];
	}

    	/*
	 * Added on : 30 aug 2017, shivani
	 * DESC : to check jobseeker availablity
	 * */
	public function check_jobseeker_availablity_on_update($jobseeker_id, $job_id, $start_date, $end_date)
	{
		//check start/end date for the job (for which we are checking availablity)
		$job_detail = JobsModel::where('_id', $job_id)->select('start_date', 'end_date')->with(['jobshifts'])->first()->toArray();	
		//check all jobapplications for jobseeker which are approved/accepted
		$jobApplications = JobsapplicationModel::where('jobseeker_id', $jobseeker_id)->where('job_id', '!=', $job_id)->whereIn('job_status', [1, 2, 4])->with(['job_detail' => function ($q) {
			$q->select('start_date', 'end_date');
		}, 'app_shift'])->select('jobseeker_id', 'job_id', 'job_status')->get()->toArray();
		//initial status for availablity
		$available = true;
		//loop through each application, and check all possible conditions and return availabity 
		if (!empty($jobApplications)) {
			foreach ($jobApplications as $job_app) {
				if (!empty($job_app['app_shift'])) {
					foreach ($job_app['app_shift'] as $jobShift) {
						$app_start = Carbon::parse($jobShift['start_date']);
						$app_end = Carbon::parse($jobShift['end_date']);	
						//compare these dates with each shift of the job (for which we are checking availablity)	

						$job_start = Carbon::parse($start_date);
						$job_end = Carbon::parse($end_date);
						if ($job_start == $app_start && $job_start < $app_end && $job_end > $app_start && $job_end == $app_end) {
							return $available = false;
						} else if ($job_start < $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
							return $available = false;
						} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end > $app_end) {
							return $available = false;
						} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
							return $available = false;
						} else if ($job_start < $app_start && $job_start < $app_end && $job_end > $app_start && $job_end > $app_end) {
							return $available = false;
						} else if ($job_start == $app_start && $job_start < $app_end && $job_end > $app_start && $job_end < $app_end) {
							return $available = false;
						} else if ($job_start > $app_start && $job_start < $app_end && $job_end > $app_start && $job_end == $app_end) {
							return $available = false;
						} else {
							$available = true;
								//check that start dates can not be same
							$fgh = $job_start;
							$job_start_date = $job_start->format('Y-m-d');
							$job_end_date = $job_end->format('Y-m-d');
							$app_start_date = $app_start->format('Y-m-d');
							$app_end_date = $app_end->format('Y-m-d');
							if ($job_start_date == $app_start_date && $job_end_date == $app_end_date && $job_end_date == $app_start_date && $job_end_date == $app_end_date) {
								return $available = false;
							}

							if ($job_end_date == $app_end_date) {
								return $available = false;
							}

						}

					}
				}
			}
		} else {
			$available = true;
		}

		return ['available' => $available];
	}
	
	/*
	 * Added on : 11 Sept 2017
	 * DESC : Update user's average rating
	 * */
	public function updateRating($rating, $user_id)
	{
		//Get user's rating
		$userRating = User::where(array('_id' => $user_id))->value('rating');

		$userRating += $rating;
		if ($userRating > 5)
			$userRating = 5;
		//update avg rating
		User::where('_id', $user_id)->update(['rating' => floatval(number_format($userRating, 2))]);
	}

	public function sendOtpMail($request, $otp)
	{
		$code = str_random(30);
		$emal_verification = new EmailVerification(array('code' => $code, 'status' => true, 'email' => $request->input('email')));
		if ($emal_verification->save()) {
			$template = EmailTemplate::find('59115d57b098f07cae0661c2');
			$link = '<a href=' . url('/email_verification/' . $code) . '>Click Here</a>';
			$find = array('@name@', '@otp@', '@link@', '@company@');
			$values = array($request->input('first_name'), $otp, $link, env('MAIL_COMPANY'));
			$body = str_replace($find, $values, $template->content);
			new SendOtpEmail($body, $request->input('email'), $template);
		}
		
		//Mail::to($request->input('email'))->queue(new OtpSent($body,$template));
	}
    
    /*
	 * Added on : 19 sep 2017
	 * DESC : to calculate total job hours
	 * */
	public function calculate_total_job_hours($job_id)
	{
		$jobs = JobsModel::where('_id', $job_id)->with(['jobapplied' => function ($q) {
			$q->with(['job_shift' => function ($q) {
				$q->where('start_date', '<=', Carbon::now());
			}]);
		}])->orderBy('_id', 'Desc')->get()->toArray();
    	//if jobshifts found, then calculate hours worked upto now
		$totalJobHours = 0;
		if (!empty($jobs)) {
			foreach ($jobs as $job) {
				if (!empty($job['jobapplied'])) {
					$workHours = $workMinutes = 0;//initial values for work minutes and hours
					foreach ($job['jobapplied'] as $job_applctn) {
						//check job shifts, update total hours if applicaytion status is not canceled or pending
						if (!empty($job_applctn['job_shift']) && $job_applctn['job_status'] != 0 && $job_applctn['job_status'] != 5 && $job_applctn['job_status'] != 7 && $job_applctn['job_status'] != 3 && $job_applctn['job_status'] != 6) {
							$job_app_hours = $this->calculate_job_app_hours($job_applctn['_id']);
							$totalJobHours += floatval(number_format($job_app_hours['job_app_hours'], 2));
						} elseif ($job_applctn['job_status'] == 5 || $job_applctn['job_status'] == 7) {
							$totalJobHours += $job_applctn['total_hours_worked'];
						}
					}
				}
			}
		}
		return ['job_hours' => $totalJobHours];
	}
	
	/*
	 * Updated : 28 nov 2017
	 * DESC : job hour calculation procedure updated (job shifts are calculated on daily basis.)
	 * */
	public function calculate_job_app_hours($job_app_id)
	{
		//~ $jobApplication_data = JobsapplicationModel::where('_id',$job_app_id)->with(['job_shift'=>function($q){ $q->where('start_date','<=',Carbon::now()); }])->first();
		//~ $workMinutes = $workHours = 0;
		//~ if(!empty($jobApplication_data->job_shift)){			
			//~ foreach ($jobApplication_data->job_shift as $shift) {
				//~ //finally calculate total minutes worked.
				//~ $workMinutes += $shift['shift_time'];
			//~ }//end foreach (jobshift) 
			//~ //if work minutes are greater than 0, then convert to hours
			//~ if($workMinutes > 0){
				//~ $workHours += floatval(number_format(($workMinutes/60),2));
			//~ }
		//~ }


		$jobApplication_data = ApplicantShift::where('app_id', $job_app_id)->where('start_date', '<=', Carbon::now())->where('total_hours_worked', 0)->get()->toArray();

		if (!empty($jobApplication_data)) {
			foreach ($jobApplication_data as $shift) {
				//update data
				$this->update_job_hours($shift['job_id'], $shift['app_id'], $shift['_id'], $shift['shift_time']);
			}//end foreach (jobshift) 
			//now delete the rest of the shifts which were supposed to be started in future.
		}
		ApplicantShift::where('app_id', $job_app_id)->where('start_date', '>', Carbon::now())->where('total_hours_worked', 0)->delete();
		//~ return ['job_app_hours'=>$workHours];
	}
	
	/*
	 * Added on : 13 sep 2017, shivani
	 * To return user rating
	 * updated : 15 dec 2017, 
	 * */
	public static function user_rating($rating_num, $user_id = null)
	{
		$user['rating'] = $rating_num;
		$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>" . "&nbsp;" . "<b>(5)</b>";
		if (isset($user['rating'])) {
			if (($user['rating'] >= 1) && ($user['rating'] < 1.5)) {
				//1 star
				$rating = "<i class='fa fa-star'></i>";
			} elseif (($user['rating'] >= 1.5) && ($user['rating'] < 2)) {
				//1.5 star
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>";
			} elseif (($user['rating'] >= 2) && ($user['rating'] < 2.5)) {
				//2 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i>";
			} elseif (($user['rating'] >= 2.5) && ($user['rating'] < 3)) {
				//2.5 star
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>";
			} elseif (($user['rating'] >= 3) && ($user['rating'] < 3.5)) {
				//3 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>";
			} elseif (($user['rating'] >= 3.5) && ($user['rating'] < 4)) {
				//3.5 star
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>";
			} elseif (($user['rating'] >= 4) && ($user['rating'] < 4.5)) {
				//4 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>";
			} elseif (($user['rating'] >= 4.5) && ($user['rating'] < 5)) {
				//4.5 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star-half-o'></i>";
			} else {
				//5 stars
				$rating = "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>";
			}
		}
		return $rating . "<br>" . "<b><a href='javascript:void(0)' class='update_applicant_rating' data-applicant='" . $user_id . "' data-value='" . $user['rating'] . "'>(" . $user['rating'] . ")</a></b>";
	}
	
	/*
	 * Added on : 22 sep 2017
	 * Added by : shivani, Debut infotech
	 * DESC : to calculate distance between two coordinates
	 * */
	function distance($lat1, $lon1, $lat2, $lon2, $unit)
	{
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}
	
	//added on : 4 oct 2017, shivani. to save temp records to send notifications
	public function notify_before_twelve_hour($job_id, $befor12_hour, $befor1_hour, $candidate_id)
	{
		date_default_timezone_set('UTC');
		$checkCurrentTime = Carbon::now();
		if ($checkCurrentTime < $befor12_hour) {
			$motify12_before = new SendQuizNotification();
			$motify12_before->job_id = $job_id;
			$motify12_before->send_at = $befor12_hour;
			$motify12_before->to = $candidate_id;
			$motify12_before->type = 'jobStart12Notification';
			$motify12_before->save();
		}

		if ($checkCurrentTime < $befor1_hour) {
			$motify1_before = new SendQuizNotification();
			$motify1_before->job_id = $job_id;
			$motify1_before->send_at = $befor1_hour;
			$motify1_before->to = $candidate_id;
			$motify1_before->type = 'jobStart1Notification';
			$motify1_before->save();
		}

	}
	
	
	/*=====================================
		Getting Working Hour of jobseeker
	 * updated : 27 oct 2017, shivani - to add one day if current date is sunday (Week : sun - sat)
    ======================================*/
	public function getWorkingHourOfJobseeker($currentDate, $employer_id, $jobseeker_id)
	{
		$date = Carbon::parse($currentDate);
		if ($date->format('D') == 'Sun') {
			$date2 = $date->addDays(1)->startOfDay();
			$end = Carbon::parse($date2);
			$start_time = $date2->startOfWeek()->subDays(1)->startOfDay();

			$end_time = $end->endOfWeek()->subDays(1)->startOfDay();
		} else {
			$end = Carbon::parse($date);
			$start_time = $date->startOfWeek()->subDays(1)->startOfDay();

			$end_time = $end->endOfWeek()->subDays(1)->startOfDay();
		}
		
		
		//->where('applied_date','>=',$start_time)->where('applied_date','<=',$end)
		$job_application = JobsapplicationModel::with(array('job_shift' => function ($q) use ($start_time, $end_time) {
			$q->where('start_date', '>=', $start_time)->where('start_date', '<=', $end_time);
		}, 'job'))->where(array('jobseeker_id' => $jobseeker_id))->whereIn('job_status', [1])->get()->toArray();
		
		//check if jobseeker is working for same employer or different
		$employer_ids = [$employer_id];
		foreach ($job_application as $applications) {
			if (!empty($applications['job_shift'])) {
				$employer_ids[] = $applications['employer_id'];
			}
		}

		if (count(array_unique($employer_ids)) > 1) {
			foreach ($job_application as $applications) {
				//if job has been cancelled then add total hours worked
				if ($applications['job_status'] == 5 || $applications['job_status'] == 7) {
					$hours[] = $applications['total_hours_worked'];
				} else {
					foreach ($applications['job_shift'] as $shift) {
						//hours calculation.
						$start = Carbon::parse($shift['start_date']);
						$end = Carbon::parse($shift['end_date']);
						$hours[] = $end->diffinHours($start);
					}
				}
			}
		}

		if (isset($hours)) {
			$total_working_hour = array_sum($hours);
		} else {
			$total_working_hour = 0;
		}

		return $total_working_hour;
	}
    
    /*===================================
		Getting hour of job being applied
    ======================================*/
	public function getHourOfJobBeingApplied($job_id)
	{
    	// getting hour of job being applied
		$saturday = new Carbon('next saturday');
    	//$job_data 	= JobsModel::with(array('jobshifts'=>function($q) use($saturday) { $q->where('end_date','<=',$saturday)->get();}))->where(array('_id'=> $job_id,'is_deleted'=>false))->whereIn('job_status' ,[0,1,2,3])->first()->toArray();


		$currentDate = JobsModel::where(array('_id' => $job_id, 'is_deleted' => false))->value('start_date');

		$date = Carbon::parse($currentDate);
		if ($date->format('D') == 'Sun') {
			$date2 = $date->addDays(1)->startOfDay();
			$end = Carbon::parse($date2);
			$start_time = $date2->startOfWeek()->subDays(1)->startOfDay();

			$end_time = $end->endOfWeek()->subDays(1)->startOfDay();
		} else {
			$end = Carbon::parse($date);
			$start_time = $date->startOfWeek()->subDays(1)->startOfDay();

			$end_time = $end->endOfWeek()->subDays(1)->startOfDay();
		}
    	
    	
		//~ $end  = Carbon::parse($date);
		//~ $start_time = $date->startOfWeek()->subDays(1)->startOfDay();
		//~ $end_time = $end->endOfWeek()->subDays(1)->startOfDay();

		$job_data = JobsModel::with(array('jobshifts' => function ($q) use ($start_time, $end_time) {
			$q->where('start_date', '>=', $start_time)->where('start_date', '<=', $end_time);
		}))->where(array('_id' => $job_id, 'is_deleted' => false))->whereIn('job_status', [0, 1, 2, 3])->first()->toArray();

		foreach ($job_data['jobshifts'] as $job_hours) {
           // $totalhours[]=$job_hours['shift_time']/60;
           //hours calculation.
			$start = Carbon::parse($job_hours['start_date']);
			$end = Carbon::parse($job_hours['end_date']);
			$totalhours[] = $end->diffinHours($start);
		}
		if (isset($totalhours)) {
			$hours = array_sum($totalhours);
		} else {
			$hours = 0;
		}
		return $hours;
	}
    
    /*
	 * Added : 28 nov 2017, shivani
	 * DESC : to return shift details for a job (to be saved for each applicant)
	 * */
	public function save_app_shifts($application_id, $job_id)
	{
		$details = ShiftModel::where('job_id', $job_id)->get()->toArray();
		$lunch_hour = JobsModel::where('_id', $job_id)->value('lunch_hour');
		$lunch_hour = empty($lunch_hour) ? 0 : floatval(number_format(($lunch_hour / 60), 2));
		foreach ($details as $dtl) {
			$data['total_hours_worked'] = 0;
			$data['job_id'] = $job_id;
			$data['app_id'] = $application_id;
			$data['start_date'] = Carbon::parse($dtl['start_date']);
			$data['end_date'] = Carbon::parse($dtl['end_date']);
			$data['lunch_hour'] = $lunch_hour;
			$savedta = new ApplicantShift($data);
			$savedta->save();
		}
	}

	/*
	 * Added : 28 nov 2017, shivani
	 * DESC : to update job application/job hours when job is cancelled/employee fired etc actions are performed.
	 * */
	public function update_job_hours($job_id, $app_id, $shift_id, $shift_time)
	{
		//calculate hours of work
		$workHours = floatval(number_format(($shift_time / 60), 2));
		$appLunchHours = ApplicantShift::where('_id', $shift_id)->value('lunch_hour');
		if (!empty($appLunchHours)) {
			$workHours -= $appLunchHours;
			if ($workHours <= 0) {
				$workHours = 1;
			}
		}
		//update hours
		if (ApplicantShift::where('_id', $shift_id)->update(['total_hours_worked' => $workHours])) {
			
			//check hours for job application
			$appHours = JobsapplicationModel::where('_id', $app_id)->value('total_hours_worked');
			//$jobHours = JobsModel::where('_id', $job_id)->value('total_work_hours');
			if (!empty($appHours)) {
				$appHours += $workHours;
			} else {
				$appHours = $workHours;
			}
			//update hours
			JobsapplicationModel::where('_id', $app_id)->update(['total_hours_worked' => $appHours]);
			//update job work hours for all employees
			$checkappHours = JobsapplicationModel::where('job_id', $job_id)->where('total_hours_worked', '!=', null)->sum('total_hours_worked');
			JobsModel::where('_id', $job_id)->update(['total_work_hours' => $checkappHours]);
		}
	}


	/*
	 * Added : 25 nov 2019, tarun
	 * DESC : to download opentok video
	 * 
	 */

	function downloadOpentokVideo($sessionId){


		 # Fatch api key and secret
        #==========================
        $opentok_key = Config::get('app.opentok_key');
        $opentok_secret = Config::get('app.opentok_secret');



	    #Genrate JWT token with opentok API
	    #==================================
        $tokens = array(
            'ist' => 'project',
            'iss' => $opentok_key,
            'iat' => time(),
            'exp' => time()+1800,
            'jti' => uniqid(),
        );

        $jwtOpentokToken = JWT::encode($tokens, $opentok_secret);


        $header[]='Accept:application/json';
        $header[]='Content-Type:application/json';
        $header[]='X-OPENTOK-AUTH:'.$jwtOpentokToken;


	   # Get Video URL
	   #======================

	    $url = 'https://api.opentok.com/v2/project/'.$opentok_key.'/archive?sessionId='.$sessionId;

	    $ch = curl_init();
	          curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POST, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    $result = curl_exec ($ch);
	    curl_close ($ch);
	    $result = json_decode($result);
	    //$result->items[0]->id;
	    //print_r($result->items[0]->url); 
	    //print_r($result->items[0]->id);die;
	    //return $result->items[0]->url;

        $archiveId = $result->items[0]->id;

	   	$url = CommonRepositary::getAwsUrl($archiveId,$opentok_key);

	   	return $url;
	}


	/*
	 * Added : 27 nov 2019, tarun
	 * DESC : get aws url
	 * 
	 */


	function getAwsUrl($archiveId,$opentok_key){

		$path = $opentok_key.'/'.$archiveId.'/archive.mp4';

	    $awsKey ='AKIAJPDJ2BHK7OGXEA4A';
	    $awsSecret='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';

	    $s3Client = new \Aws\S3\S3Client([
	    'version'     => 'latest',
	        'region'      => 'ca-central-1',
	        'credentials' => [
	            'key'    => $awsKey,
	            'secret' => $awsSecret
	        ]
	    ]);


	    $cmd = $s3Client->getCommand('GetObject', [
	    'Bucket' => 'madwall',
	    'Key' => $path,
	    ]);


	    try
	    {
	    $request = $s3Client->createPresignedRequest($cmd, '+2 hours');
	    $presignedUrl = (string)$request->getUri();
	    //echo $presignedUrl; die;
	    return $presignedUrl;
	    }
	    catch(S3Exception $e)
	    {
	    $e->getMessage();
	    die();
	    }


	}

}
