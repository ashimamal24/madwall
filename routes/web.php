<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::get('/qrcode', function(){
    	return view('qrcode');
    });
Route::group(['prefix' => 'blog'], function () {
  Route::get('/', 'Webpage\BlogController@index'); 
  Route::get('/details/{blog_id}', 'Webpage\BlogController@details'); 
  Route::post('/post_comment/{blog_id}', 'Webpage\CommentController@postComment'); 
  Route::post('/comment_reply/{comment_id}', 'Webpage\CommentController@commentReply'); 
  
});


Route::get('/load-post', 'Webpage\BlogController@loadBlog');
Route::get('/', 'HomeController@indexEmployer');
Route::get('/testUser', 'HomeController@test_user');
Route::get('/deeplink/jobshare/{job_id}', 'HomeController@jobshare_deeplink');
Route::get('/terms-condtion', 'Webpage\WebPageController@termsCondtions'); // View the Admin Profile
Route::get('/about-us', 'Webpage\WebPageController@aboutUs'); // View the Admin Profile
Route::get('/privacy-policy', 'Webpage\WebPageController@privacyPolicy'); // View the Admin Profile
Route::get('/termscondtion', 'Webpage\WebPageController@apitermsCondtions'); // View the Admin Profile
Route::get('/aboutus', 'Webpage\WebPageController@apiaboutUs'); // View the Admin Profile
Route::get('/privacypolicy', 'Webpage\WebPageController@apiprivacyPolicy'); //
Route::post('/auth/employe/register', 'Auth\RegisterController@postEmployeregister');
Route::post('/employer/login', 'Auth\LoginController@postEmployelogin');
Route::get('employer/logout', 'Auth\LoginController@getEmployerLogout');
Route::post('employer/forgotpassword', 'Auth\ForgotPasswordController@postEmployerForgotPassword');
//Route::get('employer/reset/{code}', 'Auth\PasswordController@getReset');
//Route::post('employer/reset', 'Auth\PasswordController@postReset');
Route::get('/email_verification/{code}', 'HomeController@getConfirmEmail');
Route::get('/register/verify/{confirmationCode}', [
	'uses' => 'Employer\RegistrationController@confirm'
]);
Route::get('complete-job', 'CronController@complateJobs');
Route::get('process-job', 'CronController@ProcessJob');
Route::get('automatic-job', 'CronController@AutomaticJob');
Route::get('delete-waitlist-jobs', 'CronController@delete_jobs');//30 aug 2017, shivani
Route::get('complete-daily-shifts', 'CronController@update_employee_shifts_hours');//30 aug 2017, shivani
//5 aug 2017
Route::get('send-quiz-notification', 'CronController@send_quiz_notification');
Route::get('send-job-notification', 'CronController@send_job_notification');
Route::get('monthly-rating', 'CronController@check_monthly_earnings');
Route::get('monthly-bonus', 'CronController@check_monthly_ratings');
Route::get('process-old-data', 'CronController@process_old_data');
Route::get('reset-jobseekers', 'CronController@reset_employee_data');
Route::get('old-jobseekers', 'CronController@reset_jobseekers_category_industry');
Route::get('old-jobLocation', 'CronController@reset_joblocation');
Route::get('send-app-update-notification', 'CronController@reset_user_emails');
Route::get('update-location', 'CronController@reset_user_coordinates');
Auth::routes();
// Employer Route Group
Route::group(['prefix' => 'employer'], function () {
	Route::get('/post_job', 'Employer\EmployerhomeController@createPost');
	Route::get('/export-active-jobs', 'Employer\EmployerhomeController@exportActiveJobs');
	Route::get('/export-employees', 'Employer\EmployeeController@exportEmployees');
	Route::get('/export-job-history', 'Employer\JobHistoryController@exportJobHistory');
	Route::get('/employee-shifts', 'Employer\JobHistoryController@employeeShifts');
	Route::get('/update-hours', 'Employer\JobHistoryController@update_work_hours');
	Route::get('/cancel-employee', 'Employer\EmployerhomeController@getCancelEmployee');
	Route::get('/check-docs', 'Employer\EmployerhomeController@getCandidateDocuments');
	Route::post('/save-shifts', 'Employer\EmployerhomeController@save_shift_time');
	Route::post('/same-job', 'Employer\EmployerhomeController@same_job_time');
	Route::post('/edit-same-job', 'Employer\EmployerhomeController@edit_same_job_time');
	Route::post('/remove-shifts', 'Employer\EmployerhomeController@remove_shift_time');
	Route::post('/process-data', 'Employer\EmployerhomeController@process_details');
	Route::get('post_job/{id}', 'Employer\EmployerhomeController@createPost');
	// Admin Profile Routes
	Route::get('dashboard', 'Employer\EmployerhomeController@index');
	Route::get('view-profile', 'Employer\EditProfileController@index'); // View the Admin Profile
	Route::post('edit-profile', 'Employer\EditProfileController@editProfile'); // Update Admin Profile
	Route::get('view-change-password', 'Employer\EditProfileController@ViewChangePassword'); // View for Change Password 
	Route::post('change-password', 'Employer\EditProfileController@ChangePassword'); // Update company password	
	Route::post('change-email', 'Employer\EditProfileController@ChangeEmail'); // Update company email
	Route::post('change-logo', 'Employer\EditProfileController@ChangeLogo'); // Update company logo
	Route::post('update-company-profile', 'Employer\EditProfileController@UpdateProfile'); // Update  company profile 
	Route::get('company-profile', 'Employer\EditProfileController@ViewCompanyProfile'); // View for Change Password 
	//Route::get('post_job', 'Employer\EmployerhomeController@createPost');
	Route::post('subcategory', 'Employer\EmployerhomeController@Subcategory');
	Route::post('skills', 'Employer\EmployerhomeController@Skills');
	Route::post('savejob', 'Employer\EmployerhomeController@postSavejob');
	Route::get('jobdata', 'Employer\EmployerhomeController@getJobpost');
	Route::get('jobdetail/{id}', 'Employer\EmployerhomeController@getJobdetail');
	Route::post('deletejob', 'Employer\EmployerhomeController@postDeletejob');
	Route::get('editjob/{id}', 'Employer\EmployerhomeController@getEditjob');
	Route::post('editJob/{id}', 'Employer\EmployerhomeController@postEditjob');
	Route::post('subcategory', 'Employer\EmployerhomeController@Subcategory');
	Route::post('skills', 'Employer\EmployerhomeController@Skills');
	Route::post('rating', 'Employer\EmployerhomeController@postJobRating');
	Route::post('jobdetail/jobapplylisting', 'Employer\EmployeeController@postUserAppliedJob');
	Route::post('acceptreject', 'Employer\EmployerhomeController@postAcceptjob');
	Route::get('jobhistory', 'Employer\JobHistoryController@getJobhistory');
	//Route::get('about', 'Employer\EmployerhomeController@getAbout');
	Route::get('contact', 'Employer\EmployerhomeController@getAbout');
	Route::post('employer-jobs', 'Employer\EmployerhomeController@EmployerJobsName');
	Route::post('savejob', 'Employer\EmployerhomeController@postSavejob');
	Route::post('verfiylink', 'Employer\EditProfileController@postVeficationLink');
	Route::post('contact-us', 'Employer\EmployerhomeController@postContactus');
	Route::post('about-us', 'Employer\EmployerhomeController@postAboutUs');
	Route::post('history-listing', 'Employer\JobHistoryController@jobHistoryListing');
	Route::post('historydetail/jobapplylisting', 'Employer\JobHistoryController@postEmployeeHistory');
	Route::post('job-rating', 'Employer\EmployerhomeController@postEmployerUserRating');
	Route::post('all-notification', 'Employer\NotificationController@getAllNotification');
	Route::post('jobs-users', 'Employer\EmployeeController@postJobsUsers');
	Route::post('update-notification', 'Employer\NotificationController@postUpdateNotify');
	Route::post('check-employer', 'Employer\EmployerhomeController@postCheckEmployer');//3 aug 2017, shivani
	Route::post('check-user', 'Employer\EmployerhomeController@postCheckUser');
	Route::get('send-rehire-invite', 'Employer\EmployerhomeController@getRehireInvite');//29 aug 2017, shivani
	Route::post('user-for-rehire', 'Employer\EmployerhomeController@user_for_rehire');
	Route::post('interview-assign', 'Employer\EmployerhomeController@interview_timeslot');//23 jan 2018, to assign interview timeslots to an employee for a job.
	Route::get('history-detail/{id}', 'Employer\JobHistoryController@getJobHistoryDetail');
	Route::post('delete-job/{id}', 'Employer\JobHistoryController@deleteJob');

});

	# Agreement
	Route::get('agreement/{id}','Employer\AgreementController@agreementForm');
	Route::post('agreement/store','Employer\AgreementController@saveAgreementForm')->name('postData');
	//Route::get('agreement/{id}','Employer\AgreementController@agreementForm');
	//Route::get('pdf', 'Employer\AgreementController@pdfview')->name('pdf');
	Route::get('agreements/pdf/{id}', 'Employer\AgreementController@pdfview')->name('pdf');
	Route::get('agreement/downloadEmergencyContact/{id}', 'Employer\AgreementController@downloadEmergencyContact')->name('downloadEmergencyContact');
	Route::get('agreement/downloadDirectDeposit/{id}', 'Employer\AgreementController@downloadDirectDeposit')->name('downloadDirectDeposit');
	Route::get('agreement/downloadEmplyoeeContract/{id}', 'Employer\AgreementController@downloadEmplyoeeContract')->name('downloadEmplyoeeContract');
	
	# Base64 to PNG
	Route::post('base64ToPng','Employer\AgreementController@base64ToPng')->name('imageRoute');

	Route::get('success', 'Employer\AgreementController@success');

	Route::get('admin/vedio/{id}', 'Employer\AgreementController@vedio');

	Route::get('admin/opentok/{id}', 'Employer\AgreementController@opentok');
	//Route::post('admin/decline','Employer\AgreementController@decline')->name('declineOpentokCall');
	Route::get('admin/decline/{id}','Employer\AgreementController@decline')->name('declineOpentokCall');

	//Route::get('admin/opentok/{id}', 'Employer\EditProfileController@opentok');
	

	Route::get('testing', 'Admin\JobSeekerController@testing');

	Route::get('admin/satisfiedWithCall/{id}','Employer\AgreementController@satisfiedWithCall')->name('satisfiedWithCall');


	Route::group(['prefix' => 'admin', 'middleware' => ['backbutton']], function () {
	Route::get('dashboard', 'HomeController@index'); // Trigger After Login
	Route::post('check-employee-cv', 'Admin\JobSeekerController@employee_cv_details');
	Route::post('decline-employee-cv', 'Admin\JobSeekerController@decline_new_cv');
	Route::post('approve-employee-cv', 'Admin\JobSeekerController@approve_new_cv');
	Route::get('mark-closed', 'Admin\ContactController@close_query'); // mark contact query closed	
	// Admin Profile Routes
	Route::get('delete-employer/{id}', 'Admin\EmployerController@getDeleteEmployer');
	Route::get('verify-employer-email/{id}', 'Admin\EmployerController@getVerifyEmployerEmail');
	Route::get('verify-employee-email/{id}', 'Admin\JobSeekerController@getVerifyEmployeeEmail');

	Route::get('sent-agreement/{id}', 'Admin\JobSeekerController@getSendEmployeeAgreement');

	
	Route::get('delete-employee/{id}', 'Admin\JobSeekerController@getDeleteEmployee');
	Route::get('soft-delete-employee/{id}', 'Admin\JobSeekerController@getSoftDeleteEmployee');
	Route::get('industry-category', 'Admin\SubCategoryController@get_categories');//get categories for selected industry
	Route::post('employer-categories', 'Admin\JobController@getEmployerCategories');//get categories for selected employer for job post
	Route::post('subcategory', 'Admin\JobController@Subcategory');//get subcategory for selected category while posting a job
	Route::post('skills', 'Admin\JobController@Skills');//get skills for selected subcategory while posting a job
	Route::post('/save-shifts', 'Admin\JobController@save_shift_time');//save shift details to temporary data
	Route::post('/remove-shifts', 'Admin\JobController@remove_shift_time');//remove shift from temporary data
	Route::post('/process-data', 'Admin\JobController@process_details');//get process details in admin
	Route::post('/same-job', 'Admin\JobController@same_job_time');
	Route::post('/edit-same-job', 'Admin\JobController@edit_same_job_time');
	Route::post('/savejob', 'Admin\JobController@postNewJob');//save job by admin
	Route::get('/editjob/{id}', 'Admin\JobController@get_edit_job');//edit job by admin
	Route::post('editJob/{id}', 'Admin\JobController@post_update_job');//update job by admin
	Route::post('/deletejob', 'Admin\JobController@postDeletejob');//delete job by admin
	Route::get('/cancel-employee', 'Admin\JobController@getCancelEmployee');//cancel employee from a job by admin
	Route::post('acceptreject', 'Admin\JobController@postAcceptjob');//accept reject an employee for a job from admin
	Route::get('/employee-shifts', 'Admin\JobController@employeeShifts');//to get shift details for an employee
	Route::get('/update-hours', 'Admin\JobController@update_work_hours');//to update employee hours for a particular shift
	Route::get('send-rehire-invite', 'Admin\JobController@getRehireInvite');//send rehire invitation from admin
	Route::post('user-for-rehire', 'Admin\JobController@user_for_rehire');//return user list for rehire modal
	Route::get('/check-docs', 'Admin\JobController@getCandidateDocuments');//return document details of a candidate
	Route::post('/interview-assign', 'Admin\JobController@interview_timeslot');//23 jan 2018, to assign interview timeslots to an employee for a job.
	// Admin Profile Routes
	Route::get('view-profile', 'Admin\AdminController@index');
	Route::post('edit-profile', 'Admin\AdminController@editProfile');
	Route::get('view-change-password', 'Admin\AdminController@ViewChangePassword');
	Route::post('change-password', 'Admin\AdminController@ChangePassword');
	/** Category Routes **/
	Route::get('list-categories', 'Admin\CategoryController@index'); // List All Category
	Route::post('filter-data', 'Admin\CategoryController@filterCategories'); // Filter Data Used by ajax
	Route::get('add-category', 'Admin\CategoryController@create'); // Load View to add Category
	Route::post('add-category', 'Admin\CategoryController@store'); // Add New Category
	Route::post('view-category', 'Admin\CategoryController@viewCategory'); // Load View to view Category
	Route::get('edit-category/{cat_id}', 'Admin\CategoryController@update'); // Load View to update Category
	Route::post('edit-category/{cat_id}', 'Admin\CategoryController@edit'); // Update Category
	Route::post('delete/{cat_id}', 'Admin\CategoryController@destroy'); // Delete Category
	//Route::post( 'delete-releted-subcategory/{cat_id}', 'Admin\CategoryController@deleteReletedSubcategory' ); // Delete Category
	Route::post('lock-category', 'Admin\CategoryController@changeStatus');
	/** Subcategory Routes **/
	Route::get('list-subcategories', 'Admin\SubCategoryController@index'); // List All Category
	Route::post('filter-subcategories', 'Admin\SubCategoryController@filterSubcategories'); // Filter Data Used by ajax
	Route::get('add-subcategories', 'Admin\SubCategoryController@create'); // Load View to add Category
	Route::post('store-subcategories', 'Admin\SubCategoryController@addSubcategories'); // Add New Category
	Route::post('view-subcategories', 'Admin\SubCategoryController@viewSubcategories'); // Load View to view Category
	Route::get('edit-subcategory/{subcat_id}', 'Admin\SubCategoryController@update'); // Load View to update Category
	Route::post('edit-subcategory/{subcat_id}', 'Admin\SubCategoryController@editSubcategory'); // Update Category
	Route::post('delete-subcategory/{subcat_id}', 'Admin\SubCategoryController@destroy'); // Delete Category
	Route::post('lock-subcategories', 'Admin\SubCategoryController@changeStatus');
	/** Skill Routes **/
	Route::get('list-skills', 'Admin\SkillController@index'); // List All Skills
	Route::post('filter-skill', 'Admin\SkillController@filterSkills'); // Filter Skills Used by ajax
	Route::get('add-skill', 'Admin\SkillController@create'); // Load View to add Skill
	Route::post('add-skill', 'Admin\SkillController@store'); // Add New Skill
	Route::post('view-skill', 'Admin\SkillController@viewSkill'); // Load View to view Skill
	Route::get('edit-skill/{skill_id}', 'Admin\SkillController@update'); // Load View to update Skill
	Route::post('edit-skill/{skill_id}', 'Admin\SkillController@edit'); // Update Skill
	Route::post('delete-skill/{skill_id}', 'Admin\SkillController@destroy'); // Delete Skill
	/** Email Template Routes **/
	Route::get('list-emails', 'Admin\EmailController@index'); // List All Emails
	Route::post('filter-email', 'Admin\EmailController@filterEmails'); // Filter Data Used by ajax
	//Route::get( 'add-email', 'Admin\EmailController@create' ); // Load View to add Email
	//Route::post( 'add-email', 'Admin\EmailController@store' ); // Add New Email
	Route::post('view-email', 'Admin\EmailController@viewEmail'); // Load View to view Email
	Route::get('edit-email/{email_id}', 'Admin\EmailController@update'); // Load View to update Email
	Route::post('edit-email/{email_id}', 'Admin\EmailController@edit'); // Update Email
	Route::post('delete-email/{email_id}', 'Admin\EmailController@destroy'); // Delete Email
	/** Commission Routes **/
	Route::get('list-commissions', 'Admin\CommissionController@index'); // List All Emails
	Route::post('filter-commission', 'Admin\CommissionController@filterCommissions'); // Filter Data Used by ajax
	Route::get('add-commission', 'Admin\CommissionController@create'); // Load View to add Email
	Route::post('add-commission', 'Admin\CommissionController@store'); // Add New Email
	Route::post('view-commission', 'Admin\CommissionController@viewCommission'); // Load View to view Email
	Route::get('edit-commission/{commission_id}', 'Admin\CommissionController@update'); // Load View to update Email
	Route::post('edit-commission/{commission_id}', 'Admin\CommissionController@edit'); // Update Email
	Route::post('delete-commission/{commission_id}', 'Admin\CommissionController@destroy'); // Delete Email
	/** Jobseeker Routes **/
	Route::get('list-waitingemployee', 'Admin\JobSeekerController@getWaitlistEmploye'); // List of Waiting Jobseekers
	Route::get('list-approvedemployee', 'Admin\JobSeekerController@getApprovedEmploye'); // List of Approved Jobseekers
	Route::get('list-blockedemployee', 'Admin\JobSeekerController@getBlockedEmploye'); // List of Approved Jobseekers
	Route::post('filter-jobseekerwaitlists', 'Admin\JobSeekerController@jobSeekerWaitListing'); // Filter Waiting Jobseekers
	Route::post('filter-declined-user', 'Admin\JobSeekerController@filterDeclinedEmploye'); // Filter Approved Jobseekers
	Route::post('filter-jobseekerapproved', 'Admin\JobSeekerController@jobSeekerApprovedListing'); // Filter Approved Jobseekers
	Route::post('improve-disimprove-user/{user_id}', 'Admin\JobSeekerController@improveDisimproveUser'); // Load View to add Email
	//Route::post( 'view-time-slots', 'Admin\JobSeekerController@viewTimeSlots' ); // Timeslot View
	Route::post('view-block-employe', 'Admin\JobSeekerController@viewBlockEmployee'); // Load View to view Email
	Route::post('view-approved-jobseeker', 'Admin\JobSeekerController@viewApprovedJobseeker'); // Add New Email
	Route::get('assign-time-slot/{user_id}', 'Admin\JobSeekerController@assignTimeSlot'); // Load View to view Email
	Route::post('save-time-slot', 'Admin\JobSeekerController@saveTimeSlot'); // Load View to view Email
	Route::post('approve-waitlisting-jobseeker-data', 'Admin\JobSeekerController@approveWaitlistingJobseeker'); // Load View to view Email
	Route::post('edit-approve-waitlisting-jobseeker-data', 'Admin\JobSeekerController@editApproveWaitlistingJobseekerData'); // Load View to view Email
	Route::post('decline-waitlisting-jobseeker', 'Admin\JobSeekerController@declineWaitlistingJobseeker'); // Load View to view Email
	Route::get('edit-approved-employee/{user_id}', 'Admin\JobSeekerController@updateApprovedJobSeeker'); // Load View to update Email
	Route::get('view-detail-approved-employee/{user_id}', 'Admin\JobSeekerController@viewDetailApprovedJobseeker'); // Load View to update Email
	Route::post('edit-approved-jobseeker/{user_id}', 'Admin\JobSeekerController@editApprovedJobSeeker'); // Update Email
	Route::post('block-employee', 'Admin\JobSeekerController@blockUser'); // Update Email
	//Route::get('htmltopdfview',array('as'=>'htmltopdfview','uses'=>'Admin\JobSeekerController@htmltopdfview'));
	//Route::get( 'htmltopdfview','Admin\JobSeekerController@exportPDF');
	//Route::post('exportPDF', 'Admin\JobSeekerController@exportPDF');
	Route::get('htmltopdfview/{id}', 'Admin\JobSeekerController@htmltopdfview');
	Route::get('select-subcategory/{cat_id}', 'Admin\JobSeekerController@selectSubcategory');
	Route::get('select-skills/{subCategory_id}', 'Admin\JobSeekerController@selectSkills');
	Route::post('unblock-employee', 'Admin\JobSeekerController@unBlockUser'); // Update Email
	Route::post('undelined-employee', 'Admin\JobSeekerController@undelinedEmployee');
	Route::post('employe-change-status', 'Admin\JobSeekerController@changeStatus');
	/** Employer Routes **/
	Route::get('list-employerwaitlist', 'Admin\EmployerController@getWaitlistEmployer'); // List of Waiting Jobseekers
	Route::get('list-approvedemployer', 'Admin\EmployerController@getApprovedEmployer'); // List of Approved Jobseekers
	Route::post('filter-employerwaitlists', 'Admin\EmployerController@employerWaitListing'); // Filter Waiting Jobseekers
	Route::get('view-detail/{user_id}', 'Admin\EmployerController@viewDetailOfWaitingEmployer'); // Filter Waiting Jobseekers
	Route::post('filter-approvedemployer', 'Admin\EmployerController@approvedEmployerList'); // Filter Approved Jobseekers
	Route::post('approve-waiting-employer', 'Admin\EmployerController@approveWaitingEmployer'); // Load View to view Email
	Route::post('decline-waiting-employer', 'Admin\EmployerController@declineWaitingEmployer'); // Load View to view Email
	Route::get('edit-approved-employer/{user_id}', 'Admin\EmployerController@updateApprovedEmployer'); // Load View to update Email
	Route::get('detail-approved-employer/{user_id}', 'Admin\EmployerController@detailApprovedEmployer'); // Load View to update Email
	Route::post('edit-approved-employer', 'Admin\EmployerController@giveCommisssionToEmployer'); // Update Email
	Route::post('block-employer', 'Admin\EmployerController@blockEmployer'); // Update Email
	Route::post('unblock-employer', 'Admin\EmployerController@unBlockEmployer'); // Update Email
	Route::post('employer-change-status', 'Admin\EmployerController@changeStatus');
	Route::get('check-category-commision', 'Admin\EmployerController@category_commision_value');//shivani, 28Sep2017
	/** Cms Routes **/
	Route::get('list-cms', 'Admin\CmsController@index'); // List All cms
	Route::post('filter-cms', 'Admin\CmsController@filterCms'); // Filter cms Used by ajax
	//Route::get( 'add-cms', 'Admin\CmsController@create' ); // Load View to add cms
	//Route::post( 'add-cms', 'Admin\CmsController@store' ); // Add New cms
	Route::post('view-cms', 'Admin\CmsController@viewCms'); // Load View to view cms
	Route::get('edit-cms/{cms_id}', 'Admin\CmsController@update'); // Load View to update cms
	Route::post('edit-cms/{cms_id}', 'Admin\CmsController@edit'); // Update cms
	Route::post('delete-cms/{cms_id}', 'Admin\CmsController@destroy'); // Delete cms
	/** Faq Routes **/
	Route::get('list-faqs', 'Admin\FaqController@index'); // List All cms
	Route::post('filter-faq', 'Admin\FaqController@filterFaqs'); // Filter cms Used by ajax
	Route::get('add-faq', 'Admin\FaqController@create'); // Load View to add cms
	Route::post('add-faq', 'Admin\FaqController@store'); // Add New cms
	Route::post('view-faq', 'Admin\FaqController@viewFaq'); // Load View to view cms
	Route::get('edit-faq/{faq_id}', 'Admin\FaqController@update'); // Load View to update cms
	Route::post('edit-faq/{faq_id}', 'Admin\FaqController@edit'); // Update cms
	Route::post('delete-faq/{faq_id}', 'Admin\FaqController@destroy'); // Delete cms
	/** General Info Routes **/
	Route::get('list-generalinfo', 'Admin\GeneralInfoController@index'); // List All cms
	Route::post('filter-generalinfo', 'Admin\GeneralInfoController@filterGeneralinfo'); // Filter cms Used by ajax

	Route::get('add-generalinfo', 'Admin\GeneralInfoController@create'); // Load View to add cms
	Route::post('add-generalinfo', 'Admin\GeneralInfoController@store'); // Add New cms
	Route::post('view-generalinfo', 'Admin\GeneralInfoController@viewGeneralinfo'); // Load View to view cms
	Route::get('edit-generalinfo/{ginfo_id}', 'Admin\GeneralInfoController@update'); // Load View to update cms
	Route::post('edit-generalinfo/{ginfo_id}', 'Admin\GeneralInfoController@edit'); // Update cms
	Route::post('delete-generalinfo/{ginfo_id}', 'Admin\GeneralInfoController@destroy'); // Delete cms

	Route::get('list-document', 'Admin\DocumentController@index'); // List All cms
	Route::post('filter-document', 'Admin\DocumentController@filterDocument'); // Filter cms Used by ajax

	Route::get('add-document', 'Admin\DocumentController@create'); // Load View to add cms
	Route::post('add-document', 'Admin\DocumentController@store'); // Add New cms
	Route::post('view-document', 'Admin\DocumentController@viewDocument'); // Load View to view cms
	Route::get('edit-document/{ginfo_id}', 'Admin\DocumentController@update'); // Load View to update cms
	Route::post('edit-document/{ginfo_id}', 'Admin\DocumentController@edit'); // Update cms
	Route::post('delete-document/{ginfo_id}', 'Admin\DocumentController@destroy'); // Delete cms

	
	/** Jobs Routes **/
	Route::get('list-jobs', 'Admin\JobController@index'); // List All cms
	Route::post('filter-jobs', 'Admin\JobController@filterJobs'); // Filter cms Used by ajax
	Route::get('view-job-detail/{job_id}', 'Admin\JobController@viewJobDetail'); // Update cms
	Route::post('delete-job/{id}', 'Admin\JobController@deleteJob'); // Update cms
	Route::post('filter-job-detail/{job_id}', 'Admin\JobController@filterJobDetail'); // Update cms
	Route::get('view-job-detail/jobseeker-hours-rating/{id}', 'Admin\JobController@viewJobseekerHoursRating'); // Update cms
	Route::post('edit-rating', 'Admin\JobController@editHourRating'); // Update cms
	Route::get('post-new-job', 'Admin\JobController@getCreateJob'); //post new job from admin
	Route::get('post-new-job/{id}', 'Admin\JobController@getCreateJob');//repost a job from admin
	/** Contact Us **/
	Route::get('list-contacts', 'Admin\ContactController@index'); // List All cms
	Route::post('filter-contacts', 'Admin\ContactController@filterContacts'); // Filter cms Used by ajax
	Route::post('view-contact', 'Admin\ContactController@viewContact'); // Load View to view Skill
	/** Wage Routes **/
	Route::get('list-wage', 'Admin\WageController@index'); // List All Skills
	Route::post('filter-wage', 'Admin\WageController@filterWage'); // Filter Skills Used by ajax
	//Route::get( 'add-wage', 'Admin\WageController@create' ); // Load View to add Wage
	//Route::post( 'add-wage', 'Admin\WageController@storeWage' ); // Add New Wage
	Route::get('edit-wage/{wage_id}', 'Admin\WageController@update'); // Load View to update Wage
	Route::post('edit-wage/{wage_id}', 'Admin\WageController@edit'); // Update Wage
	/** Industry Routes **/
	Route::get('list-industries', 'Admin\IndustryController@index'); // List All Category
	Route::post('filter-industry', 'Admin\IndustryController@filterIndustries'); // Filter Data Used by ajax
	Route::get('add-industry', 'Admin\IndustryController@create'); // Load View to add Category
	Route::post('add-industry', 'Admin\IndustryController@store'); // Add New Category
	Route::post('view-industry', 'Admin\IndustryController@viewIndustry'); // Load View to view Category
	Route::get('edit-industry-view/{indust_id}', 'Admin\IndustryController@update'); // Load View to update Category
	Route::post('edit-industry/{indust_id}', 'Admin\IndustryController@edit'); // Update Category
	Route::post('delete-industry/{cat_id}', 'Admin\IndustryController@destroy'); // Delete Category
	//Route::post( 'delete-releted-subcategory/{cat_id}', 'Admin\CategoryController@deleteReletedSubcategory' ); // Delete Category
	/** Payment Routes **/
	Route::get('receive-payment-view', 'Admin\PaymentController@receivePaymentView'); // Get Notification
	Route::any('payment-to-receive', 'Admin\PaymentController@paymentToReceive'); // Get Notification
	Route::any('export-receive-csv', 'Admin\PaymentController@exportReceiveCsv'); // Get Notification
	Route::any('export-payable-csv', 'Admin\PaymentController@exportPayableCsv'); // Get Notification
	Route::get('topay-employe-view', 'Admin\PaymentController@topayEmployeView'); // Get Notification
	Route::any('payment-to-pay', 'Admin\PaymentController@paymentToPay'); // Get Notification
	Route::post('change-status', 'HomeController@changeStatus');
	/** Notification Routes **/
	Route::get('notification', 'Admin\NotificationController@getNotification'); // Get Notification
	Route::post('check-notification', 'Admin\NotificationController@checkNotification'); // Read Notification
	Route::get('add-hours', 'Admin\JobController@addhours'); // Read Notification

   

    
    //blog category
    Route::group(['prefix' => 'blog-category'], function () {

      Route::get('list','Admin\BlogCategoryController@index');
      Route::post('filter-blogCategories', 'Admin\BlogCategoryController@filterBlogCategories');
      Route::get('add', 'Admin\BlogCategoryController@create');
      Route::post('store', 'Admin\BlogCategoryController@store');
      Route::get('edit/{blog_cat_id}', 'Admin\BlogCategoryController@edit');
      Route::post('update/{blog_cat_id}', 'Admin\BlogCategoryController@update');
      Route::post('delete/{blog_cat_id}', 'Admin\BlogCategoryController@destroy');
      Route::post('block-unblock', 'Admin\BlogCategoryController@changeStatus');

    });


    //blogs
    Route::group(['prefix' => 'blogs'], function () {

      Route::get('list','Admin\BlogController@index');
      Route::post('filter-blogs', 'Admin\BlogController@filterBlogs');
      Route::get('add', 'Admin\BlogController@create');
      Route::post('store', 'Admin\BlogController@store');
      Route::get('edit/{blog_id}', 'Admin\BlogController@edit');
      Route::post('update/{blog_id}', 'Admin\BlogController@update');
      Route::post('delete/{blog_id}', 'Admin\BlogController@destroy');
      Route::post('block-unblock', 'Admin\BlogController@changeStatus');    
      Route::get('comment-list/{blog_id}', 'Admin\CommentController@index');
      Route::post('filter-comments/{id}', 'Admin\CommentController@filterComments');
      Route::post('save_blog_image', 'Admin\BlogController@saveBlogImage');
      Route::post('save_author_image', 'Admin\BlogController@saveAuthorImage');
      Route::post('comment_delete/{id}', 'Admin\CommentController@destroy');
      Route::post('/comment_reply', 'Admin\CommentController@commentReply'); 
    
    });


   //industries-content
     Route::group(['prefix' => 'industries-content'], function () {

      Route::get('list','Admin\IndustryContentController@index');
      Route::post('filter-industry-content', 'Admin\IndustryContentController@filterIndustriesContent');
	  Route::get('add', 'Admin\IndustryContentController@create');
	  Route::post('store', 'Admin\IndustryContentController@store');
	  Route::post('view', 'Admin\IndustryContentController@view');
      Route::get('edit/{id}', 'Admin\IndustryContentController@edit');
      Route::post('update/{id}', 'Admin\IndustryContentController@update');
      Route::post('change-status', 'Admin\IndustryContentController@changeStatus');
      Route::post('delete/{id}', 'Admin\IndustryContentController@destroy');
      
    
    });

     //Highlights
     Route::group(['prefix' => 'highlights'], function () {

      	Route::get('list','Admin\HighlightsController@index');
    	Route::post('filter-highlight', 'Admin\HighlightsController@filterHighlight');
	 	Route::get('add', 'Admin\HighlightsController@create');
		Route::post('store', 'Admin\HighlightsController@store');
		Route::post('view', 'Admin\HighlightsController@view');
   	    Route::get('edit/{id}', 'Admin\HighlightsController@edit');
  	    Route::post('update/{id}', 'Admin\HighlightsController@update');
      	//Route::post('change-status', 'Admin\HighlightsController@changeStatus');
      	Route::post('delete/{id}', 'Admin\HighlightsController@destroy');

    
    
    });


    //Title
    Route::group(['prefix' => 'titles'], function () {

     	Route::get('list','Admin\TitlesController@index');
    	Route::post('filter-title', 'Admin\TitlesController@filterTitle');
	 	Route::get('add', 'Admin\TitlesController@create');
		Route::post('store', 'Admin\TitlesController@store');
		Route::post('view', 'Admin\TitlesController@view');
   	    Route::get('edit/{id}', 'Admin\TitlesController@edit');
  	    Route::post('update/{id}', 'Admin\TitlesController@update');
        //Route::post('change-status', 'Admin\TitlesController@changeStatus');
       Route::post('delete/{id}', 'Admin\TitlesController@destroy');
      
    });




});
